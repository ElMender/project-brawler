using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentSelected : MonoBehaviour
{
    [Header("Armas")]
    public GameObject Daga1;
    public GameObject Daga2, DobleHacha1, DobleHacha2, Guantes1, Guantes2, Escudo, ArmaHoz, Espadadeunamano, EspadaGrande, Pistola1, Pistola2;

    [Header("Skins Cabeza")]
    public GameObject Cabeza1;
    public GameObject Cabeza2;
    public GameObject Cabeza3;
    public GameObject Cabeza4;
    public GameObject Cabeza5;
    public GameObject Cabeza6;

    [Header("Skins Cuerpo")]
    public GameObject Cuerpo1;
    public GameObject Cuerpo2;
    public GameObject Cuerpo3;
    public GameObject Cuerpo4;

    [Header("Armas espalda")]
    public GameObject Daga12;
    public GameObject Daga22, DobleHacha12, DobleHacha22, Guantes12, Guantes22, Escudo2, ArmaHoz2, Espadadeunamano2, EspadaGrande2, Pistola22, Pistola21;

    GameObject currentlyChosen1;

    GameObject currentlyChosen2;

    GameObject TempInv;
    private void Awake()
    {
        #region Hand 1 Select
        if (PlayerPrefs.GetString("Hand1") == "Daga")
        {
            Daga1.SetActive(true);
            Daga2.SetActive(true);
            currentlyChosen1 = Daga1;
        }

        if (PlayerPrefs.GetString("Hand1") == "DobleHacha")
        {
            DobleHacha1.SetActive(true);
            DobleHacha2.SetActive(true);
            currentlyChosen1 = DobleHacha1;
        }

        if (PlayerPrefs.GetString("Hand1") == "Guantes")
        {
            Guantes1.SetActive(true);
            Guantes2.SetActive(true);
            currentlyChosen1 = Guantes1;
        }

        if (PlayerPrefs.GetString("Hand1") == "Escudo")
        {
            Escudo.SetActive(true);
            currentlyChosen1 = Escudo;
        }

        if (PlayerPrefs.GetString("Hand1") == "ArmaHoz")
        {
            ArmaHoz.SetActive(true);
            currentlyChosen1 = ArmaHoz;
        }

        if (PlayerPrefs.GetString("Hand1") == "Espadadeunamano")
        {
            Espadadeunamano.SetActive(true);
            currentlyChosen1 = Espadadeunamano;
        }

        if (PlayerPrefs.GetString("Hand1") == "EspadaGrande")
        {
            EspadaGrande.SetActive(true);
            currentlyChosen1 = EspadaGrande;
        }

        if (PlayerPrefs.GetString("Hand1") == "Pistola")
        {
            Pistola1.SetActive(true);
            Pistola2.SetActive(true);
            currentlyChosen1 = Pistola1;
        }

        #endregion

        #region Hand 2 Select
        if (PlayerPrefs.GetString("Hand2") == "Daga")
        {
            Daga12.SetActive(true);
            Daga22.SetActive(true);
            currentlyChosen2 = Daga2;
        }

        if (PlayerPrefs.GetString("Hand2") == "DobleHacha")
        {
            DobleHacha12.SetActive(true);
            DobleHacha22.SetActive(true);
            currentlyChosen2 = DobleHacha2;
        }

        if (PlayerPrefs.GetString("Hand2") == "Guantes")
        {
            Guantes12.SetActive(true);
            Guantes22.SetActive(true);
            currentlyChosen2 = Guantes2;
        }

        if (PlayerPrefs.GetString("Hand2") == "Escudo")
        {
            Escudo2.SetActive(true);
            currentlyChosen2 = Escudo2;
        }

        if (PlayerPrefs.GetString("Hand2") == "ArmaHoz")
        {
            ArmaHoz2.SetActive(true);
            currentlyChosen2 = ArmaHoz2;
        }

        if (PlayerPrefs.GetString("Hand2") == "Espadadeunamano")
        {
            Espadadeunamano2.SetActive(true);
            currentlyChosen2 = Espadadeunamano2;
        }

        if (PlayerPrefs.GetString("Hand2") == "EspadaGrande")
        {
            EspadaGrande2.SetActive(true);
            currentlyChosen2 = EspadaGrande2;
        }

        if (PlayerPrefs.GetString("Hand2") == "Pistola")
        {
            Pistola22.SetActive(true);
            Pistola21.SetActive(true);
            currentlyChosen2 = Pistola21;
        }
        #endregion

        #region Hat Select
        if (PlayerPrefs.GetInt("Hat") == 0)
        {
            Cabeza1.SetActive(false);
            Cabeza2.SetActive(false);
            Cabeza3.SetActive(false);
            Cabeza4.SetActive(false);
            Cabeza5.SetActive(false);
            Cabeza6.SetActive(false);
        }
        if (PlayerPrefs.GetInt("Hat") == 1)
        {
            Cabeza1.SetActive(true);
        }
        if (PlayerPrefs.GetInt("Hat") == 2)
        {
            Cabeza2.SetActive(true);
        }
        if (PlayerPrefs.GetInt("Hat") == 3)
        {
            Cabeza3.SetActive(true);
        }
        if (PlayerPrefs.GetInt("Hat") == 4)
        {
            Cabeza4.SetActive(true);
        }
        if (PlayerPrefs.GetInt("Hat") == 5)
        {
            Cabeza5.SetActive(true);
        }
        if (PlayerPrefs.GetInt("Hat") == 6)
        {
            Cabeza6.SetActive(true);
        }

        #endregion

        #region Body Select
        if (PlayerPrefs.GetInt("Body") == 0)
        {
            Cuerpo1.SetActive(false);
            Cuerpo2.SetActive(false);
            Cuerpo3.SetActive(false);
            Cuerpo4.SetActive(false);
        }
        if (PlayerPrefs.GetInt("Body") == 1)
        {
            Cuerpo1.SetActive(true);
        }
        if (PlayerPrefs.GetInt("Body") == 2)
        {
            Cuerpo2.SetActive(true);
        }
        if (PlayerPrefs.GetInt("Body") == 3)
        {
            Cuerpo3.SetActive(true);
        }
        if (PlayerPrefs.GetInt("Body") == 4)
        {
            Cuerpo4.SetActive(true);
        }
        #endregion
    }
    public void Swicht()
    {
        currentlyChosen1.SetActive(false);
        currentlyChosen2.SetActive(false);
        TempInv = currentlyChosen1;
        currentlyChosen1 = currentlyChosen2;
        currentlyChosen2 = TempInv;
        currentlyChosen1.SetActive(true);
        currentlyChosen2.SetActive(true);
    }

    public void pressedbuttonCabeza()
    {
        Cabeza1.SetActive(false);
        Cabeza2.SetActive(false);
        Cabeza3.SetActive(false);
        Cabeza4.SetActive(false);
        Cabeza5.SetActive(false);
        Cabeza6.SetActive(false);
        Awake();
    }
    public void pressedbuttonCuerpo()
    {
        Cuerpo1.SetActive(false);
        Cuerpo2.SetActive(false);
        Cuerpo3.SetActive(false);
        Cuerpo4.SetActive(false);
        Awake();
    }
}
