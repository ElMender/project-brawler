using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextUpdaterWeapons : MonoBehaviour
{
    public Text weapon1;
    public Text weapon2;
    void Update()
    {
        weapon1.text = PlayerPrefs.GetString("Hand1");
        weapon2.text = PlayerPrefs.GetString("Hand2");
    }
}
