using System.Numerics;
using UnityEngine;
using Web3Unity.Scripts.Library.ETHEREUEM.EIP;
using UnityEngine.UI;
public class ERC20BalanceOfExample : MonoBehaviour
{
    public BigInteger DogeCount;

    public BigInteger BNBCount;

    public Button DogeButton;
    public Text DogeText;

    async void Start()
    {
        string contract = "0x4985f07c5Ac918376080785f179D9595F972E5cB";
        string account = PlayerPrefs.GetString("Account");

        if (account == "")
        {

        }
        else
        {
            BigInteger balanceOf = await ERC20.BalanceOf(contract, account);
            Debug.Log("Balance Of: " + balanceOf);
            DogeCount = balanceOf;

            contract = "0xEC5dCb5Dbf4B114C9d0F65BcCAb49EC54F6A0867";
            balanceOf = await ERC20.BalanceOf(contract, account);
            Debug.Log("Balance Of: " + balanceOf);
            BNBCount = balanceOf;

            if (DogeCount > 0)
            {
                DogeButton.interactable = true;
                DogeText.text = "";
            }
            else
            {
                DogeButton.interactable = false;
                DogeText.text = "Get Baby Doge Chess To Unlock";
            }
        }
    }
}
