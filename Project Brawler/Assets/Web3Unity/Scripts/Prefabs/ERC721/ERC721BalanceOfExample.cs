using System.Numerics;
using UnityEngine;
using Web3Unity.Scripts.Library.ETHEREUEM.EIP;
using UnityEngine.UI;

public class ERC721BalanceOfExample : MonoBehaviour
{

    BigInteger Cards;

    public Button CardButton;
    public Text CardText;

    async void Start()
    {
        string contract = "0x93F7ee734Be7a726A9322644c166f7033A8Becf5";
        string account = PlayerPrefs.GetString("Account");
        if (account == "")
        {

        }
        else
        {
            BigInteger balance = await ERC721.BalanceOf(contract, account);
            print(balance);

            Cards = balance;

            if (Cards > 0)
            {
                CardButton.interactable = true;
                CardText.text = "";
            }
            else
            {
                CardButton.interactable = true;
                CardText.text = "Get Baby Doge Chess To Unlock";
            }
        }
    }
}
