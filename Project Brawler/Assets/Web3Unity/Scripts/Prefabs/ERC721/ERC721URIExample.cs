using UnityEngine;
using Web3Unity.Scripts.Library.ETHEREUEM.EIP;
using Web3Unity.Scripts.Library.Ethers.Providers;

public class ERC721URIExample : MonoBehaviour
{
    async void Start()
    {
        string contract = "0x93F7ee734Be7a726A9322644c166f7033A8Becf5";
        string tokenId = "29891";

        string uri = await ERC721.URI(contract, tokenId);
        print(uri);
    }
}
