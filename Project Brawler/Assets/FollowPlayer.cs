using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public GameObject playerToFollow;

    void Update()
    {
        if (playerToFollow == null)
        {
            playerToFollow = GameObject.FindGameObjectWithTag("Player");
        }
        else
        {
            transform.position = new Vector3(playerToFollow.transform.position.x, playerToFollow.transform.position.y + 1, playerToFollow.transform.position.z);
        }
    }
}
