using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public static bool Parent = false;
    public GameObject PaseMenuUI;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused && Parent)
            {
                Resume();
            }
            else
            {
                Pausa();
            }
        }
    }
    public void Resume()
    {
        PaseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        Parent=false;
    }
    void Pausa()
    {
        PaseMenuUI.SetActive(true);
        Time.timeScale = 1f;
        GameIsPaused = true;
    }
    public void Salirlobby()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(1);
    }
    public void Exit()
    {
        Application.Quit();
    }
}