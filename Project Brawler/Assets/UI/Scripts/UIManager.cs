using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEditor;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
  public GameObject menuBar;
  public bool showMenuBar = true;
  public GameObject menuBarButton;
  public TMP_Text dateDisplay;
	public TMP_Text timeDisplay;
  public bool showDate = true;
	public bool showTime = true;
  Transform tempParent;
  public Image[] blurs;
    // Start is called before the first frame update
  void Start()
  {
    if(menuBar != null){
		if(!showMenuBar){
			menuBar.gameObject.SetActive(false);
			menuBarButton.gameObject.SetActive(false);
		  }
	  }
        
  }

    // Update is called once per frame
  void Update()
  {
    if(showMenuBar){
		DateTime time = DateTime.Now;
		if(showTime){timeDisplay.text = time.Hour + "hr: " + time.Minute + "min: " + time.Second+"s";}else if(!showTime){timeDisplay.text = "";}
		if(showDate){dateDisplay.text = System.DateTime.Now.ToString("yyyy/MM/dd");}else if(!showDate){dateDisplay.text = "";}
		}
  }
  public void MoveToFront(GameObject currentObj)
  {
		//tempParent = currentObj.transform.parent;
		tempParent = currentObj.transform;
		tempParent.SetAsLastSibling();
	}
  public void Quit()
  {
		Application.Quit();
  }
  public void vs1()
  {
    SceneManager.LoadScene("LV1 - Battlefield");
  }
  public void Equiparse()
  {
	SceneManager.LoadScene("ChooseWeapon");
  }
}
