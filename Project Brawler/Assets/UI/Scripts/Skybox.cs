using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skybox : MonoBehaviour
{
    public float RotaSpeed= 1.3f;
    void Update()
    {
        RenderSettings.skybox.SetFloat("_Rotation",Time.time*RotaSpeed);
    }
}
