using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Quality : MonoBehaviour
{
    public TMP_Dropdown qualityLevel;
    // Start is called before the first frame update
    void Start()
    {
        qualityLevel.value = PlayerPrefs.GetInt("Quality Level");
        QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("Quality Level"), false);
    }
    public void Set_Quality_Level()
	{
		PlayerPrefs.SetInt("Quality Level", qualityLevel.value);

		QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("Quality Level"), false);
	}
}
