using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEditor;
using UnityEngine.UI;
using TMPro;

public class UIScaler : MonoBehaviour
{
    public Slider uiScaleSlider;
    float xScale = 0f;
	float yScale = 0f;
    public CanvasScaler mainCanvas;

    public void UIScalerLarg(){
		xScale = 1920 * uiScaleSlider.value;
		yScale = 1080 * uiScaleSlider.value;
		mainCanvas.referenceResolution = new Vector2 (xScale,yScale);
	}
}
