using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject[] EnemyObj;
    public GameObject playerOBJ;
    public GameObject[] SpawnPoints;
    public GameObject gameOver;

    public GameObject stock3;
    public GameObject stock2;
    public GameObject stock1;
    public GameObject uistock;
    public Text vidaUI;
    public GameObject countdown1;
    public GameObject countdown2;
    public GameObject countdown3;
    public GameObject fightimg;
    public GameObject victoriaUI;

    public string LoadThisSceneNext;
    public PlayerController playerController;

    public Transform playerSpawnPoint;
    public bool GameStarted;

    public int winCondition;
    private int _enemyCount;
    private int _deadEnemyCount;
    public Text contadorEnmeigosUI;
    public GameObject uiContador;

    private void Awake()
    {
        _deadEnemyCount = 0;
        contadorEnmeigosUI.text = "" + (winCondition - _deadEnemyCount);
        GameStarted = false;
        SpawnPlayer();
        StartCoroutine(CountDown());
        gameOver.SetActive(false);
        SpawnPoints = GameObject.FindGameObjectsWithTag("EnemySpawn");

        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        //playerController.Stock
        //playerController.vida
        StockActualizada();
        VidaActualizada();
    }
    public void StockActualizada()
    {
        switch (playerController.Stock)
        {
            case 2:
                stock3.SetActive(false);
                break;
            case 1:
                stock2.SetActive(false);
                break;
            case 0:
                stock1.SetActive(false);
                uistock.SetActive(false);
                uiContador.SetActive(false);
                gameOver.SetActive(true);
                break;
        }
    }
    public void VidaActualizada()
    {
        vidaUI.text = playerController.vida.ToString()+"%";
    }
    private void SpawnPlayer()
    {
        Instantiate(playerOBJ, playerSpawnPoint);
    }
    IEnumerator CountDown()
    {
        countdown3.SetActive(true);
        //countdownTXT.text = "3";
        yield return new WaitForSeconds(1);
        //countdownTXT.text = "2";
        countdown3.SetActive(false);
        countdown2.SetActive(true);
        yield return new WaitForSeconds(1);
        //countdownTXT.text = "1";
        countdown2.SetActive(false);
        countdown1.SetActive(true);
        yield return new WaitForSeconds(1);
        //countdownTXT.text = "Fight";
        countdown1.SetActive(false);
        fightimg.SetActive(true);
        yield return new WaitForSeconds(0.8f);
        fightimg.SetActive(false);
        //countdownTXT.text = "";
        GameStarted = true;
        _enemyCount = GameObject.FindGameObjectsWithTag("Enemy").Length;
        Invoke("SpawnEnemy", 1);
    }
    void SpawnEnemy()
    {
        _enemyCount = GameObject.FindGameObjectsWithTag("Enemy").Length;
        if (_enemyCount <= 6)
        {
            StartCoroutine(SpawnLoop());
        }
        else
        {
            StartCoroutine(WaitForSpawn());
        }
    }

    public void WinnerCheck()
    {
        if (_deadEnemyCount >= winCondition)
        {
            playerController.gameWon();
            victoriaUI.SetActive(true);
            uistock.SetActive(false);
            uiContador.SetActive(false);
        }
    }

    IEnumerator SpawnLoop()
    {
        int rEnemy = Random.Range(0,EnemyObj.Length);
        Vector3 pos = new Vector3 (SpawnPoints[Random.Range(0, SpawnPoints.Length)].transform.position.x, SpawnPoints[Random.Range(0, SpawnPoints.Length)].transform.position.y, SpawnPoints[Random.Range(0, SpawnPoints.Length)].transform.position.z);
        Instantiate(EnemyObj[rEnemy], pos ,Quaternion.identity);
        _enemyCount = GameObject.FindGameObjectsWithTag("Enemy").Length;
        yield return new WaitForSeconds(5);
        Invoke("SpawnEnemy", 0);
    }
    IEnumerator WaitForSpawn()
    {
        yield return new WaitForSeconds(3);
        Invoke("SpawnEnemy", 0);
    }
    public void EnemyDie()
    {
        _deadEnemyCount++;
        contadorEnmeigosUI.text = "" + (winCondition - _deadEnemyCount);
        WinnerCheck();
    }
}
