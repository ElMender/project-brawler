using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBrain : MonoBehaviour
{
    [SerializeField] private float attackRange = 3f;
    [SerializeField] private float attackCooldown = 3f;
    [SerializeField] private float hitCooldown = 4f;
    [SerializeField] private float knockbackForce = 10f;
    [SerializeField] private float life = 0f;

    public LayerMask groundMask;

    private NavMeshAgent agent;
    public Animator animator;
    private GameObject player;
    public GameManager manager;
    public Transform groundCheck;

    private bool isAttacking = false;
    private bool isHit = false;
    private bool isGrounded = false;
    public float groundDistance = 0.1f;
    

    public AudioSource HitSFX;
    public ParticleSystem particle;

    public string ArmaActual;
    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        //animator = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        life = 0f;
    }
    private void Awake()
    {
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        life = 0f;
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        


        if (!isHit && isGrounded && !agent.isOnOffMeshLink)
        {
            animator.SetBool("Grounded", true);
            animator.SetTrigger("Ground");
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            float distance = Vector3.Distance(transform.position, player.transform.position);
            //transform.LookAt(new Vector3(player.transform.position.x, transform.localPosition.y, player.transform.position.z));

            if (distance <= attackRange && !isAttacking)
            {
                agent.isStopped = true;
                Attack();
                transform.LookAt(new Vector3(player.transform.position.x,transform.localPosition.y,player.transform.position.z));
            }
            else if (distance > attackRange)
            {
                agent.isStopped = false;
                agent.SetDestination(player.transform.position);
                animator.SetBool("IsRunning", true);
            }
            else
            {
                agent.isStopped = true;
                animator.SetBool("IsRunning", false);
            }
        }
        
        if (agent.isOnOffMeshLink)
        {
            animator.SetBool("Grounded", false);
            animator.SetTrigger("Jump");
        }
    }

    void Attack()
    {
        isAttacking = true;
        agent.isStopped = true;
        animator.SetTrigger(ArmaActual);

        Invoke("ResetAttack", attackCooldown);
    }

    void ResetAttack()
    {
        isAttacking = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        player = GameObject.FindGameObjectWithTag("Player");

        if ((other.gameObject.tag == "Player" || other.gameObject.tag == "Hazard") && other.gameObject.name == "highF")
        {

            agent.isStopped = true;
            agent.enabled = false;

            life += 1f;
            isHit = true;
            //animator.SetTrigger("hit");
            HitSFX.Play();
            particle.Play();
            //Stop the NavMeshAgent and deactivate it temporarily
            agent.SetDestination(transform.position);
            animator.SetTrigger("ForceJump");
            knockbackForce = life * 2f;

            Vector3 knockbackDirection = (transform.position - player.transform.position).normalized;
            GetComponent<Rigidbody>().AddForce(knockbackDirection * knockbackForce, ForceMode.Impulse);
            GetComponent<Rigidbody>().AddForce(new Vector3(0,knockbackForce,0), ForceMode.Impulse);
            hitCooldown = life * 0.5f;

            Invoke("ResetHit", hitCooldown);
        }

        if (other.gameObject.name == "Death")
        {
            manager.EnemyDie();
            Destroy(this.gameObject);

        }
    }

    void ResetHit()
    {
        
        //animator.ResetTrigger("hit");

        
        isHit = false;

        if (isGrounded)
        {
            agent.enabled = true;
            agent.isStopped = false;
        }
        else
        {
            Invoke("ResetHit", 0.1f);
        }
    }
}
