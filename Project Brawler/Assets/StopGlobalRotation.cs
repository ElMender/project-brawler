using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopGlobalRotation : MonoBehaviour
{
    void Update()
    {
        transform.rotation = new Quaternion(0,0,0,0);
    }
}
