using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class LoadController : MonoBehaviour
{
    public TextMeshProUGUI textProgress;
    public Slider sliderUI;
    float tiempo = 3f;
    float contadorTiempo;
    void Start()
    {
        contadorTiempo = 0;
        int currentSceneIndex = PlayerPrefs.GetInt("TargetSceneNumber", 2); // 1 es el valor predeterminado si no se ha guardado ning�n valor
        StartCoroutine(LoadScene(currentSceneIndex));
    }

    IEnumerator LoadScene(int targetScene)
    {
        textProgress.text = "00%";
        yield return new WaitForSeconds(2.9f);
        AsyncOperation loadAsync = SceneManager.LoadSceneAsync(targetScene);
        //loadAsync.allowSceneActivation = false;
        while (!loadAsync.isDone)
        {
            yield return null;
        }
    }
    void Update()
    {
        if (contadorTiempo <= tiempo)
        {
            contadorTiempo = contadorTiempo + Time.deltaTime;
            sliderUI.value = contadorTiempo / tiempo;
            float valor = contadorTiempo / tiempo;
            textProgress.text = (Mathf.RoundToInt(100 * valor)).ToString() + "%";
        }
    }
}
