using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlcabezasUI : MonoBehaviour
{
    int headskin;
    public GameObject skn1;
    public GameObject skn2;
    public GameObject skn3;
    public GameObject skn4;


    void Start()
    {
        headskin = Random.Range(1, 4);
        switch (headskin)
        {
            case 4:
                skn4.SetActive(true);
                break;
            case 3:
                skn3.SetActive(true);
                break;
            case 2:
                skn2.SetActive(true);
                break;
            case 1:
                skn1.SetActive(true);
                break;
        }
    }
}
