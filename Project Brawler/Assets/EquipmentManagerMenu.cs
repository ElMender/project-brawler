using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentManagerMenu : MonoBehaviour
{
    #region 1rs Hand Pick
    public void EspadaGrandePick1()
    {
        PlayerPrefs.SetString("Hand1", "EspadaGrande");
    }
    public void ArmaHozPick1()
    {
        PlayerPrefs.SetString("Hand1", "ArmaHoz");
    }
    public void DagaPick1()
    {
        PlayerPrefs.SetString("Hand1", "Daga");
    }
    public void DobleHachaPick1()
    {
        PlayerPrefs.SetString("Hand1", "DobleHacha");
    }
    public void EscudoPick1()
    {
        PlayerPrefs.SetString("Hand1", "Escudo");
    }
    public void EspadadeunamanoPick1()
    {
        PlayerPrefs.SetString("Hand1", "Espadadeunamano");
    }
    public void PistolaPick1()
    {
        PlayerPrefs.SetString("Hand1", "Pistola");
    }
    public void GuantesPick1()
    {
        PlayerPrefs.SetString("Hand1", "Guantes");
    }
    #endregion

    #region 2nd Hand Pick
    public void EspadaGrandePick2()
    {
        PlayerPrefs.SetString("Hand2", "EspadaGrande");
    }
    public void ArmaHozPick2()
    {
        PlayerPrefs.SetString("Hand2", "ArmaHoz");
    }
    public void DagaPick2()
    {
        PlayerPrefs.SetString("Hand2", "Daga");
    }
    public void DobleHachaPick2()
    {
        PlayerPrefs.SetString("Hand2", "DobleHacha");
    }
    public void EscudoPick2()
    {
        PlayerPrefs.SetString("Hand2", "Escudo");
    }
    public void EspadadeunamanoPick2()
    {
        PlayerPrefs.SetString("Hand2", "Espadadeunamano");
    }
    public void PistolaPick2()
    {
        PlayerPrefs.SetString("Hand2", "Pistola");
    }
    public void GuantesPick2()
    {
        PlayerPrefs.SetString("Hand2", "Guantes");
    }
    #endregion

    #region HatPick
    public void HatPick0()
    {
        PlayerPrefs.SetInt("Hat", 0);
    }
    public void HatPick1()
    {
        PlayerPrefs.SetInt("Hat", 1);
    }
    public void HatPick2()
    {
        PlayerPrefs.SetInt("Hat", 2);
    }
    public void HatPick3()
    {
        PlayerPrefs.SetInt("Hat", 3);
    }
    public void HatPick4()
    {
        PlayerPrefs.SetInt("Hat", 4);
    }
    public void HatPick5()
    {
        PlayerPrefs.SetInt("Hat", 5);
    }

    public void HatPick6()
    {
        Debug.Log("ManBunxD");
        PlayerPrefs.SetInt("Hat", 6);
    }
    #endregion

    #region BodyPick
    public void BodyPick0()
    {
        PlayerPrefs.SetInt("Body", 0);
    }
    public void BodyPick1()
    {
        PlayerPrefs.SetInt("Body", 1);
    }
    public void BodyPick2()
    {
        PlayerPrefs.SetInt("Body", 2);
    }
    public void BodyPick3()
    {
        PlayerPrefs.SetInt("Body", 3);
    }
    public void BodyPick4()
    {
        PlayerPrefs.SetInt("Body", 4);
    }
    #endregion
}