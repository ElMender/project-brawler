using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuComands : MonoBehaviour
{
    public GameObject PausePanel;
    public int currentSceneIndex;
    void Start()
    {
        // Obtener el indice de la escena actual
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        PlayerPrefs.SetInt("TargetSceneNumber", currentSceneIndex+1);
        PlayerPrefs.Save();
    }
    public void goto1v1()
    {
        SceneManager.LoadScene("Load");
    }

    public void goto1v3()
    {
        SceneManager.LoadScene("1v3Lobby");
    }

    public void gotoMainMenu()
    {
        PlayerPrefs.DeleteKey("TargetSceneNumber");
        Time.timeScale = 1;
        SceneManager.LoadScene("Menu");
    }

    public void gotoEquipScene()
    {
        SceneManager.LoadScene("ChooseWeapon");
    }

    public void gotoArmorScene()
    {
        SceneManager.LoadScene("ChooseArmor");
    }
    public void ResetScena()
    {
        PlayerPrefs.DeleteAll();
        // Obtener el dice de la escena actual
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        // Cargar la misma escena nuevamente
        SceneManager.LoadScene(currentSceneIndex);
    }
    public void nextlevel()
    {
       
        SceneManager.LoadScene("Load");
    }
    public void Pause()
    {
        PausePanel.SetActive(true);
        Time.timeScale = 0;
    }
    public void Resume()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        PausePanel.SetActive(false);
        Time.timeScale = 1;
    }
}
