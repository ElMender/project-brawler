using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.InputSystem;

public class FollowThis : MonoBehaviour
{
    public CinemachineFreeLook camara;
    public GameObject camaraPivot;
    public GameObject look;

    public Controls controlator;

    public GameObject[] ListOfEnemyes;

    int currentTar = 0;

    public void Awake()
    {
        camara = GameObject.Find("PlayerFollowCamera").GetComponent<CinemachineFreeLook>();
        camara.Follow = camaraPivot.transform;
        camara.LookAt = look.transform;
        controlator = new Controls();
    }
    public void Update()
    {


        if (ListOfEnemyes.Length == 0)
        {
            camara.LookAt = camaraPivot.transform;
        }
    }

    private void OnEnable()
    {
        //controlator.Enable();
        //controlator.InWorld.ChangeTar.performed += ChangeTar;
    }

    private void OnDisable()
    {
        //controlator.Disable();
    }

    private void ChangeTar(InputAction.CallbackContext obj)
    {

        ListOfEnemyes = GameObject.FindGameObjectsWithTag("Enemy");
        Debug.Log("Change Target");

        if (ListOfEnemyes.Length == 0)
        {
            camara.LookAt = look.transform;
        }
        else if (ListOfEnemyes.Length > 0 && currentTar < ListOfEnemyes.Length)
        {
            camara.LookAt = ListOfEnemyes[currentTar].transform;
            currentTar++;
        }
        else if (ListOfEnemyes.Length >= currentTar)
        {
            currentTar = 0;
            camara.LookAt = look.transform;
        }
    }
}
