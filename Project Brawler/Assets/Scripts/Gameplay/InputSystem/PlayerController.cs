﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public int PlayerID;
    
    public Controls control;
    public float Vel;
    public float fuerzaSalto;

    public CharacterController controller;
    public Transform frontOfPlayer;
    public GameObject InviPlayer;

    public Animator animador;

    public AudioSource canvasForMusic;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 velocity;
    bool isGrounded;
    Vector3 moveDir;

    Vector3 SpawnPoint;

    public GameManager manager;

    public bool HitCooldown;

    public bool Hab1Cooldown;
    public bool Hab2Cooldown;

    public float vida = 0;

    public int Stock = 3;

    public GameObject FollowCamera;

    bool gameIsWon = false;

    public EquipmentSelected equipmentswich;

    public MenuComands Pausecon;

    public AudioSource HitSFX;
    public ParticleSystem particle;

    private void OnEnable()
    {
        control.Enable();
        control.InWorld.Action.performed += Kicking;
        control.InWorld.Jump.performed += Jump;
        control.InWorld.Skill1.performed += Skill1;
        control.InWorld.Skill2.performed += Skill2;
        control.InWorld.Pause.performed += Pause;
    }

    private void OnDisable()
    {
        control.Disable();
    }

    private void Awake()
    {
        Hab1Cooldown = true;
        Hab2Cooldown = true;
        HitCooldown = true;
        gameIsWon = false;
        FollowCamera = GameObject.Find("PlayerFollowCamera");
        PlayerID = GameObject.FindGameObjectsWithTag("Player").Length;
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        control = new Controls();
        frontOfPlayer = GameObject.Find("MainCamera").GetComponent<Transform>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        Stock = 3;

        SpawnPoint = new Vector3(transform.position.x, transform.position.y, transform.position.z);

        //health = GameObject.Find("Live" + PlayerID).GetComponent<Text>() ;
        //livestock = GameObject.Find("Stock" + PlayerID).GetComponent<Text>();

        canvasForMusic = GameObject.Find("Canvas Player Battle").GetComponent<AudioSource>();
        
        Pausecon = GameObject.Find("Canvas Player Battle").GetComponent<MenuComands>();
        //health.text = vida.ToString() + "%";
        //livestock.text = Stock.ToString();
    }



    private void Update()
    {
        if (manager.GameStarted)
        {
            control.Enable();
        }
        else
        {
            control.Disable();
        }

        if (!gameIsWon)
        {
            isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

            if (isGrounded && velocity.y < 0)
            {
                velocity.y = 0f;
                animador.SetTrigger("Ground");
            }

            animador.SetBool("Grounded",isGrounded);

            float VelX = control.InWorld.MovementX.ReadValue<float>();
            float VelZ = control.InWorld.MovementZ.ReadValue<float>();

            Vector3 direction = new Vector3(VelX, 0, VelZ).normalized;

            if (knockbackCounter <= 0)
            {
                velocity.x = 0;
                velocity.z = 0;
            }

            if (direction.magnitude >= 0.1f && knockbackCounter <= 0)
            {
                controller.Move(Vector3.zero);
                animador.SetBool("IsRunning",true);

                float AngleTar = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + frontOfPlayer.eulerAngles.y;
                transform.rotation = Quaternion.Euler(0, AngleTar, 0);
                moveDir = Quaternion.Euler(0f, AngleTar, 0f) * Vector3.forward;
                controller.Move(moveDir * Vel * Time.deltaTime);
            }
            else
            {
                knockbackCounter -= Time.deltaTime;
                animador.SetBool("IsRunning", false);
            }
            velocity.y += -19 * Time.deltaTime;
            controller.Move(velocity * Time.deltaTime);
        }

    }

    private void Kicking(InputAction.CallbackContext obj)
    {
        if (HitCooldown && isGrounded)
        {
            StartCoroutine("AttackCooldown");
            float VelX = control.InWorld.MovementX.ReadValue<float>();
            float VelZ = control.InWorld.MovementZ.ReadValue<float>();

            Vector3 direction = new Vector3(VelX, 0, VelZ).normalized;
            float AngleTar = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + frontOfPlayer.eulerAngles.y;
            transform.rotation = Quaternion.Euler(0, AngleTar, 0);
            animador.SetTrigger(PlayerPrefs.GetString("Hand1"));
            knockbackCounter = 1f;
        }
    }

    IEnumerator AttackCooldown()
    {

        HitCooldown = false;
        if (PlayerPrefs.GetString("Hand1") == "EspadaGrande")
        {
            yield return new WaitForSecondsRealtime(2);
        }
        if (PlayerPrefs.GetString("Hand1") == "Daga")
        {
            yield return new WaitForSecondsRealtime(1);
        }
        if (PlayerPrefs.GetString("Hand1") == "DobleHacha")
        {
            yield return new WaitForSecondsRealtime(1);
        }
        if (PlayerPrefs.GetString("Hand1") == "Guantes")
        {
            yield return new WaitForSecondsRealtime(1.5f);
        }
        if (PlayerPrefs.GetString("Hand1") == "Escudo")
        {
            yield return new WaitForSecondsRealtime(1);
        }
        if (PlayerPrefs.GetString("Hand1") == "ArmaHoz")
        {
            yield return new WaitForSecondsRealtime(2);
        }
        if (PlayerPrefs.GetString("Hand1") == "Espadadeunamano")
        {
            yield return new WaitForSecondsRealtime(1);
        }
        if (PlayerPrefs.GetString("Hand1") == "Pistola")
        {
            yield return new WaitForSecondsRealtime(1);
        }
        HitCooldown = true;
    }

    IEnumerator H1Cooldown()
    {
        Hab1Cooldown = false;
        if (PlayerPrefs.GetString("Hand1") == "EspadaGrande")
        {
            yield return new WaitForSecondsRealtime(15);
        }
        if (PlayerPrefs.GetString("Hand1") == "Daga")
        {
            Vel = 15f;
            knockbackCounter = 0;
            yield return new WaitForSecondsRealtime(0.5f);
            Vel = 5f;
            yield return new WaitForSecondsRealtime(3);
        }
        if (PlayerPrefs.GetString("Hand1") == "DobleHacha")
        {
            yield return new WaitForSecondsRealtime(10);
        }
        if (PlayerPrefs.GetString("Hand1") == "Guantes")
        {
            yield return new WaitForSecondsRealtime(7);
        }
        if (PlayerPrefs.GetString("Hand1") == "Escudo")
        {
            yield return new WaitForSecondsRealtime(12);
        }
        if (PlayerPrefs.GetString("Hand1") == "ArmaHoz")
        {
            yield return new WaitForSecondsRealtime(15);
        }
        if (PlayerPrefs.GetString("Hand1") == "Espadadeunamano")
        {
            yield return new WaitForSecondsRealtime(8);
        }
        if (PlayerPrefs.GetString("Hand1") == "Pistola")
        {
            yield return new WaitForSecondsRealtime(10);
        }
        Hab1Cooldown = true;
    }
    IEnumerator H2Cooldown()
    {
        //yield return new WaitForSecondsRealtime(0.5f);
        //equipmentswich.Swicht();
        Hab2Cooldown = false;
        if (PlayerPrefs.GetString("Hand2") == "EspadaGrande")
        {
            yield return new WaitForSecondsRealtime(15);
        }
        if (PlayerPrefs.GetString("Hand2") == "Daga")
        {
            Vel = 15f;
            knockbackCounter = 0;
            yield return new WaitForSecondsRealtime(0.5f);
            Vel = 5f;
            yield return new WaitForSecondsRealtime(3);
        }
        if (PlayerPrefs.GetString("Hand2") == "DobleHacha")
        {
            yield return new WaitForSecondsRealtime(10);
        }
        if (PlayerPrefs.GetString("Hand2") == "Guantes")
        {
            yield return new WaitForSecondsRealtime(7);
        }
        if (PlayerPrefs.GetString("Hand2") == "Escudo")
        {
            yield return new WaitForSecondsRealtime(12);
        }
        if (PlayerPrefs.GetString("Hand2") == "ArmaHoz")
        {
            yield return new WaitForSecondsRealtime(15);
        }
        if (PlayerPrefs.GetString("Hand2") == "Espadadeunamano")
        {
            yield return new WaitForSecondsRealtime(8);
        }
        if (PlayerPrefs.GetString("Hand2") == "Pistola")
        {
            yield return new WaitForSecondsRealtime(10);
        }
        Hab2Cooldown = true;
    }

    private void Jump(InputAction.CallbackContext obj)
    {
        if (isGrounded)
        {
            animador.SetTrigger("Jump");
            velocity.y = Mathf.Sqrt(fuerzaSalto * -2f * -19f);
        }
    }
    private void Skill1(InputAction.CallbackContext obj)
    {
        if (Hab1Cooldown)
        {
            //equipmentswich.Swicht();
            StartCoroutine("H1Cooldown");
            animador.SetTrigger(PlayerPrefs.GetString("Hand1") + "H");
        }
    }

    private void Skill2(InputAction.CallbackContext obj)
    {
        if (Hab2Cooldown)
        {
            //equipmentswich.Swicht();
            StartCoroutine("H2Cooldown");
            animador.SetTrigger(PlayerPrefs.GetString("Hand2") + "H");
            
        }
    }

    private void Pause(InputAction.CallbackContext obj)
    {
        Debug.Log("pausa, en teoria");
        Pausecon.Pause();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public float knockbackForce;
    private float knockbackCounter;

    public void Knockback(Vector3 direction)
    {
        knockbackCounter = vida /50;

        velocity = direction * knockbackForce/5;
        velocity.y = knockbackForce/5;
    }

    public void MiniStun()
    {
        knockbackCounter = 0.05f;
    }

    private void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.tag == "Enemy"|| other.gameObject.tag == "Hazard") && other.gameObject.name == "highF" && !gameIsWon)
        {
            HitSFX.Play();
            particle.Play();
            vida += 8;
            knockbackForce = vida;
            Knockback(-other.transform.position + transform.position);
            Debug.Log("heavyHit!");
            animador.SetTrigger("Jump");
        }

        if (other.gameObject.name == "Death") 
        {
            Die();
        }
        //health.text = vida.ToString()+"%";
        manager.VidaActualizada();
    }

    public void Die()
    {
        LooseALive();
        if (Stock == 0)
        {
            Dead();
        }
        transform.position = SpawnPoint;
    }

    public void LooseALive()
    {
        this.Stock -= 1;
        manager.StockActualizada();
        this.vida = 0;
        //livestock.text = Stock.ToString();
    }

    public void Dead()
    {
        if (gameIsWon == false)
        {
            FollowCamera.SetActive(false);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            canvasForMusic.Pause();
            InviPlayer.SetActive(false);
        }
    }
    public void gameWon()
    {
        gameIsWon = true;
        canvasForMusic.Pause();
        FollowCamera.SetActive(false);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        InviPlayer.SetActive(false);
    }
}