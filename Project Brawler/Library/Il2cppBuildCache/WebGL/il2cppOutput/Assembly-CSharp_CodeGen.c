﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.InputSystem.InputActionAsset Controlador::get_asset()
extern void Controlador_get_asset_m365D9F2CF8A3FA0841A4E58BB93A6DA4CFBDFD85 (void);
// 0x00000002 System.Void Controlador::.ctor()
extern void Controlador__ctor_mBB10DB3883C05ACC117451956AF5D14C25395C83 (void);
// 0x00000003 System.Void Controlador::Dispose()
extern void Controlador_Dispose_m9C995625901BD34D10A7A66864D010D7CEAACCC8 (void);
// 0x00000004 System.Nullable`1<UnityEngine.InputSystem.InputBinding> Controlador::get_bindingMask()
extern void Controlador_get_bindingMask_m363DFB8B93372CF90DDFA7A3C62D70EAED2526E8 (void);
// 0x00000005 System.Void Controlador::set_bindingMask(System.Nullable`1<UnityEngine.InputSystem.InputBinding>)
extern void Controlador_set_bindingMask_m9D672F9B52F04A2B1070A5CF6249F9780ECFCD18 (void);
// 0x00000006 System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>> Controlador::get_devices()
extern void Controlador_get_devices_m1A2BE69A9296A7BA7BEFA66432F9240C2D8E2590 (void);
// 0x00000007 System.Void Controlador::set_devices(System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>>)
extern void Controlador_set_devices_mBA1757821B8D1A9EDF3B52EEF9BA533A1354C8A5 (void);
// 0x00000008 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme> Controlador::get_controlSchemes()
extern void Controlador_get_controlSchemes_m11ED401CEC5DFC5CEA039CCB2C47E9B68FD6D93F (void);
// 0x00000009 System.Boolean Controlador::Contains(UnityEngine.InputSystem.InputAction)
extern void Controlador_Contains_mF6DA46E44AE95DC0E94E2DCE9D741991BFFD9D8F (void);
// 0x0000000A System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputAction> Controlador::GetEnumerator()
extern void Controlador_GetEnumerator_mD7B151CAE642A7117B222AAE8BB024D79EE21752 (void);
// 0x0000000B System.Collections.IEnumerator Controlador::System.Collections.IEnumerable.GetEnumerator()
extern void Controlador_System_Collections_IEnumerable_GetEnumerator_m96150C508C728A00C5A59BFA9CD488F3BDB128BE (void);
// 0x0000000C System.Void Controlador::Enable()
extern void Controlador_Enable_mFAFB71F79057ACBB073432770E1AAF2420B6AC89 (void);
// 0x0000000D System.Void Controlador::Disable()
extern void Controlador_Disable_m0A01F119CD8C27AB33FF0D354FAFAD29BF3834D2 (void);
// 0x0000000E Controlador/InWorldActions Controlador::get_InWorld()
extern void Controlador_get_InWorld_m760E4496F0CC16F0A1E3094FBB280A4860A3C504 (void);
// 0x0000000F Controlador/UIControlActions Controlador::get_UIControl()
extern void Controlador_get_UIControl_m9FD019A88B03DEA1397DED3A66758F0B23FB6CD0 (void);
// 0x00000010 System.Void Controlador/InWorldActions::.ctor(Controlador)
extern void InWorldActions__ctor_m377C2A36ACF59A5F3A9480272AD84E80967512B3 (void);
// 0x00000011 UnityEngine.InputSystem.InputAction Controlador/InWorldActions::get_MovementZ()
extern void InWorldActions_get_MovementZ_mA018CC88008FA3D86304ADDE5ED3568FA663DC78 (void);
// 0x00000012 UnityEngine.InputSystem.InputAction Controlador/InWorldActions::get_MovementX()
extern void InWorldActions_get_MovementX_m8015B7B9C4511D1CD73965AED29BBBAA05AE6969 (void);
// 0x00000013 UnityEngine.InputSystem.InputAction Controlador/InWorldActions::get_Action()
extern void InWorldActions_get_Action_m7758A48C330CF9F9B24E2805665841BCC6D1289D (void);
// 0x00000014 UnityEngine.InputSystem.InputAction Controlador/InWorldActions::get_Jump()
extern void InWorldActions_get_Jump_m63BF8D7698C3F5E7263DB94F3B0CAFEB8B1FECED (void);
// 0x00000015 UnityEngine.InputSystem.InputActionMap Controlador/InWorldActions::Get()
extern void InWorldActions_Get_mFFC96166CA3C940F1B367B4071775D67851CDBA8 (void);
// 0x00000016 System.Void Controlador/InWorldActions::Enable()
extern void InWorldActions_Enable_mE24CDC578A2885C807CA6410D7A461FB58905546 (void);
// 0x00000017 System.Void Controlador/InWorldActions::Disable()
extern void InWorldActions_Disable_mB4844B96C11D7E89BF1F6B0C2EE3663158C5C1B0 (void);
// 0x00000018 System.Boolean Controlador/InWorldActions::get_enabled()
extern void InWorldActions_get_enabled_m745C21AC47E3A254D3E20F3238A67B74090EF428 (void);
// 0x00000019 UnityEngine.InputSystem.InputActionMap Controlador/InWorldActions::op_Implicit(Controlador/InWorldActions)
extern void InWorldActions_op_Implicit_m0847FF83016609EDB7566D3706BA15C95393A242 (void);
// 0x0000001A System.Void Controlador/InWorldActions::SetCallbacks(Controlador/IInWorldActions)
extern void InWorldActions_SetCallbacks_mB956BB6C2016CC4D1E9DB40B979D7FD17F51949A (void);
// 0x0000001B System.Void Controlador/UIControlActions::.ctor(Controlador)
extern void UIControlActions__ctor_m66283505DA58059B0C8D6F1129A05F97DB6AA67E (void);
// 0x0000001C UnityEngine.InputSystem.InputAction Controlador/UIControlActions::get_Next()
extern void UIControlActions_get_Next_mD631E5E62E29DB74B742EA6EAE37250CE2BB6215 (void);
// 0x0000001D UnityEngine.InputSystem.InputAction Controlador/UIControlActions::get_Select()
extern void UIControlActions_get_Select_mE930EB65AEEE60A2887E91152C344823AC38C2B4 (void);
// 0x0000001E UnityEngine.InputSystem.InputAction Controlador/UIControlActions::get_Back()
extern void UIControlActions_get_Back_m122706E98ADC48CC78C819577C14D06F41B8DBE2 (void);
// 0x0000001F UnityEngine.InputSystem.InputActionMap Controlador/UIControlActions::Get()
extern void UIControlActions_Get_m2F01A1629782D6AAD26471906B702482D844053A (void);
// 0x00000020 System.Void Controlador/UIControlActions::Enable()
extern void UIControlActions_Enable_mFBA3EFF1773F8EA9957EB87875AC59EA43C9F315 (void);
// 0x00000021 System.Void Controlador/UIControlActions::Disable()
extern void UIControlActions_Disable_mC887C32C55E39858883D1B101220E4739A23343B (void);
// 0x00000022 System.Boolean Controlador/UIControlActions::get_enabled()
extern void UIControlActions_get_enabled_m997E192F447045B825BD73AEA44D37FFA1B6BDA6 (void);
// 0x00000023 UnityEngine.InputSystem.InputActionMap Controlador/UIControlActions::op_Implicit(Controlador/UIControlActions)
extern void UIControlActions_op_Implicit_m29A1BE457836B2706DBF72B9E4CA174646DD4A9A (void);
// 0x00000024 System.Void Controlador/UIControlActions::SetCallbacks(Controlador/IUIControlActions)
extern void UIControlActions_SetCallbacks_m17D84220A7BAED2C0F02D637252AA1352C34E338 (void);
// 0x00000025 System.Void Controlador/IInWorldActions::OnMovementZ(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000026 System.Void Controlador/IInWorldActions::OnMovementX(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000027 System.Void Controlador/IInWorldActions::OnAction(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000028 System.Void Controlador/IInWorldActions::OnJump(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000029 System.Void Controlador/IUIControlActions::OnNext(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x0000002A System.Void Controlador/IUIControlActions::OnSelect(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x0000002B System.Void Controlador/IUIControlActions::OnBack(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x0000002C System.Void PlayerController::OnEnable()
extern void PlayerController_OnEnable_mA03C033E796965E5CE0538A4CF83729D5857F52B (void);
// 0x0000002D System.Void PlayerController::OnDisable()
extern void PlayerController_OnDisable_mE0ED7AD25577BE2048EB6D507C7503BD6462A015 (void);
// 0x0000002E System.Void PlayerController::Awake()
extern void PlayerController_Awake_m23059564DCFDD324EA9DB966473251896647CD23 (void);
// 0x0000002F System.Void PlayerController::Update()
extern void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (void);
// 0x00000030 System.Void PlayerController::Kicking(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerController_Kicking_m1BF85836F21687F1C4AFA3EEEA8A254E06630B99 (void);
// 0x00000031 System.Void PlayerController::Jump(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerController_Jump_m8D4C651984BFEE8F94DE5D6D00759AC316BDC405 (void);
// 0x00000032 System.Void PlayerController::Knockback(UnityEngine.Vector3)
extern void PlayerController_Knockback_m7B06D561D2B9FE751DF3C33828517F17D05050E3 (void);
// 0x00000033 System.Void PlayerController::MiniStun()
extern void PlayerController_MiniStun_m5FC4DD256CDAEA3E2D8CBE5A779796F0AEC057F8 (void);
// 0x00000034 System.Void PlayerController::KnockbackEnd()
extern void PlayerController_KnockbackEnd_m4D6B6C1096281B974DC647BFC1E2F1C6444781B6 (void);
// 0x00000035 System.Void PlayerController::OnTriggerEnter(UnityEngine.Collider)
extern void PlayerController_OnTriggerEnter_mA341783BDD02CF2476E18D5D2504CA3812E0F5B3 (void);
// 0x00000036 System.Void PlayerController::Die()
extern void PlayerController_Die_m2A44CB9CC6A92B0963A0F5D3FA8B6A95F46EA533 (void);
// 0x00000037 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (void);
// 0x00000038 System.Void PlayerController::MirrorProcessed()
extern void PlayerController_MirrorProcessed_m4BCEEBE01DB5E3F80EC6B8DBAA9F3E5AAF917383 (void);
// 0x00000039 System.Single PlayerController::get_Networkvida()
extern void PlayerController_get_Networkvida_m9E4D447F8356BC31A5529D4D1710103FF3316F33 (void);
// 0x0000003A System.Void PlayerController::set_Networkvida(System.Single)
extern void PlayerController_set_Networkvida_m2CB8F66FA6105B23174030D476475B4BE2F8D92E (void);
// 0x0000003B System.Int32 PlayerController::get_NetworkStock()
extern void PlayerController_get_NetworkStock_mED9E4588D3653C199B73F89B9DE2AAFD93A7E506 (void);
// 0x0000003C System.Void PlayerController::set_NetworkStock(System.Int32)
extern void PlayerController_set_NetworkStock_mB974B4B41151AEA08695A50860AF224D4ED2B9B5 (void);
// 0x0000003D System.Boolean PlayerController::get_Networkwinner()
extern void PlayerController_get_Networkwinner_mBD34A4E434BAEEAA663DC763DB29AC07F10874C2 (void);
// 0x0000003E System.Void PlayerController::set_Networkwinner(System.Boolean)
extern void PlayerController_set_Networkwinner_mB5BC41AC0E39B4572712DC5A37E80002CD8C774B (void);
// 0x0000003F System.Boolean PlayerController::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void PlayerController_SerializeSyncVars_m4B46DB71001795E2F6EB4E63A986862549BD58FB (void);
// 0x00000040 System.Void PlayerController::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void PlayerController_DeserializeSyncVars_m4FB4B4221435B0BA8C878BC34B8D643C5B40EB7F (void);
// 0x00000041 System.Void EquipedOnPlayer::.ctor()
extern void EquipedOnPlayer__ctor_m3701FE8BA932C19DD5FAA4AEBA75564EF3B3DC07 (void);
// 0x00000042 System.Void FollowThis::Start()
extern void FollowThis_Start_mC210824AD5040C4631EC70CF1D2C73EEA087F693 (void);
// 0x00000043 System.Void FollowThis::.ctor()
extern void FollowThis__ctor_m40AC3792B1A90995E55B1A291734F26710FF9AE4 (void);
// 0x00000044 System.Void FollowThis::MirrorProcessed()
extern void FollowThis_MirrorProcessed_m5B0BBF96A4D2AD86A9438F2E17EF6FCA92239E69 (void);
// 0x00000045 System.Void MenuComands::goto1v1()
extern void MenuComands_goto1v1_m174B8B2AD7B95FEA6AA4B7B1EBBD5AD76E32CA18 (void);
// 0x00000046 System.Void MenuComands::goto1v3()
extern void MenuComands_goto1v3_mFC946BA513EEADBEF9F87850E8DBBE252A145810 (void);
// 0x00000047 System.Void MenuComands::.ctor()
extern void MenuComands__ctor_m4E8D2F73A0821B1F8B0F85658887AADCB8CFF9CC (void);
// 0x00000048 System.Collections.IEnumerator gameManager::Start()
extern void gameManager_Start_m169616D522718461F2522FC60796972765513C0B (void);
// 0x00000049 System.Void gameManager::StartGame()
extern void gameManager_StartGame_m3C85343E91286389476F3508EDA1BF238C182800 (void);
// 0x0000004A System.Void gameManager::EndGame()
extern void gameManager_EndGame_m5C9698913D66067FB73A5C749A4DCB92C4983D56 (void);
// 0x0000004B System.Collections.IEnumerator gameManager::CountDown()
extern void gameManager_CountDown_mD4C9C89C32375A504A19CDF76CD14CF08E839C5F (void);
// 0x0000004C System.Void gameManager::.ctor()
extern void gameManager__ctor_mEC3390C453AE10969E79C65755A3BECB98211356 (void);
// 0x0000004D System.Void gameManager::MirrorProcessed()
extern void gameManager_MirrorProcessed_mFB1D2DDA8BB16268A3EFC56D53781F23B04490E7 (void);
// 0x0000004E System.Int32 gameManager::get_NetworkTimeToStart()
extern void gameManager_get_NetworkTimeToStart_m5E1ED76E6950C85CFC8995DA89B4C4A8983AD0D8 (void);
// 0x0000004F System.Void gameManager::set_NetworkTimeToStart(System.Int32)
extern void gameManager_set_NetworkTimeToStart_m29853508B64E19F28827F084162C013D132298D3 (void);
// 0x00000050 System.Boolean gameManager::get_NetworkGameStarted()
extern void gameManager_get_NetworkGameStarted_mA3FD2A7B32D0194B5858FEFF7A378D0090F81B0B (void);
// 0x00000051 System.Void gameManager::set_NetworkGameStarted(System.Boolean)
extern void gameManager_set_NetworkGameStarted_m8D94633AF42D193FD5EE7C77616789444C1DCC6E (void);
// 0x00000052 System.Boolean gameManager::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void gameManager_SerializeSyncVars_m323294F60D27D5D2AAD38E3D19C81B79A6BCE921 (void);
// 0x00000053 System.Void gameManager::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void gameManager_DeserializeSyncVars_mE1C4EBDBB31758FFCF37424A29C4B1CC8B686ECB (void);
// 0x00000054 System.Void gameManager/<Start>d__3::.ctor(System.Int32)
extern void U3CStartU3Ed__3__ctor_mB48F4FAA9459FC08D7816D6B374F86776F614EF7 (void);
// 0x00000055 System.Void gameManager/<Start>d__3::System.IDisposable.Dispose()
extern void U3CStartU3Ed__3_System_IDisposable_Dispose_m93BAFC6EC8543E89F44F2387FB1A74F02B9BD27C (void);
// 0x00000056 System.Boolean gameManager/<Start>d__3::MoveNext()
extern void U3CStartU3Ed__3_MoveNext_m879532514C8C170C7530691B8E8A2B61ECFB1D76 (void);
// 0x00000057 System.Object gameManager/<Start>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2B347A5018C26527FE70DD242A8E9AE4FB19ACA5 (void);
// 0x00000058 System.Void gameManager/<Start>d__3::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__3_System_Collections_IEnumerator_Reset_mD0096DF92FFDA797AEEE4E72E2129471B234813D (void);
// 0x00000059 System.Object gameManager/<Start>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__3_System_Collections_IEnumerator_get_Current_mCC8C0917AA7FAC87174852D1E5818DA0DB4416C0 (void);
// 0x0000005A System.Void gameManager/<CountDown>d__6::.ctor(System.Int32)
extern void U3CCountDownU3Ed__6__ctor_mC57A48E6DDD09DA6E118A55129926012973577CA (void);
// 0x0000005B System.Void gameManager/<CountDown>d__6::System.IDisposable.Dispose()
extern void U3CCountDownU3Ed__6_System_IDisposable_Dispose_m2C308B8820BEC4319EBD920D21012B4FD9150C86 (void);
// 0x0000005C System.Boolean gameManager/<CountDown>d__6::MoveNext()
extern void U3CCountDownU3Ed__6_MoveNext_m906A2329B1DED7741219EB15F941F331189C16DB (void);
// 0x0000005D System.Object gameManager/<CountDown>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCountDownU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8389CB86F7B02C0EEA1C0B9AB4153727D26C6DCF (void);
// 0x0000005E System.Void gameManager/<CountDown>d__6::System.Collections.IEnumerator.Reset()
extern void U3CCountDownU3Ed__6_System_Collections_IEnumerator_Reset_mDCCF81548FBBEB13791A835C0C3D5A9DC02DC371 (void);
// 0x0000005F System.Object gameManager/<CountDown>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CCountDownU3Ed__6_System_Collections_IEnumerator_get_Current_m9D45A9FB927FF4F09F7A7D3CF3E0310A8444C439 (void);
// 0x00000060 System.Void ImportNFTTextureExample::Start()
extern void ImportNFTTextureExample_Start_mBBAA36C72E5675E8A026135493CC4EC689D13C28 (void);
// 0x00000061 System.Void ImportNFTTextureExample::.ctor()
extern void ImportNFTTextureExample__ctor_m329242A3D24FDCA956AF62B60DBCD51C08CC2CAB (void);
// 0x00000062 System.Void ImportNFTTextureExample/Response::.ctor()
extern void Response__ctor_mEA1667092CF395117675339A78FBA3827902F38F (void);
// 0x00000063 System.Void ImportNFTTextureExample/<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_m6642927BFC834022BC2708E0866FEEE26E05EFD5 (void);
// 0x00000064 System.Void ImportNFTTextureExample/<Start>d__1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__1_SetStateMachine_m72770C78EEEB2DFDB98D0015DFA3CB7FE724D5E3 (void);
// 0x00000065 System.Threading.Tasks.Task`1<System.Numerics.BigInteger> ERC1155::BalanceOf(System.String,System.String,System.String,System.String,System.String,System.String)
extern void ERC1155_BalanceOf_m6E609734004053402859FA732EB12BD113F5BBC9 (void);
// 0x00000066 System.Threading.Tasks.Task`1<System.Collections.Generic.List`1<System.Numerics.BigInteger>> ERC1155::BalanceOfBatch(System.String,System.String,System.String,System.String[],System.String[],System.String)
extern void ERC1155_BalanceOfBatch_m976C311032DE4CAB615A4BC0FC2F57669A5D04DA (void);
// 0x00000067 System.Threading.Tasks.Task`1<System.String> ERC1155::URI(System.String,System.String,System.String,System.String,System.String)
extern void ERC1155_URI_mA6C8E3B39B71865362F93F5641353858238A7341 (void);
// 0x00000068 System.Void ERC1155::.ctor()
extern void ERC1155__ctor_m2264995D04B372757ED7B1AA5678E8CB478C46D6 (void);
// 0x00000069 System.Void ERC1155::.cctor()
extern void ERC1155__cctor_m1E7EBA5D94BBF702755E5147338F6F99C17F82CC (void);
// 0x0000006A System.Void ERC1155/<BalanceOf>d__1::MoveNext()
extern void U3CBalanceOfU3Ed__1_MoveNext_m827BEB7B02D7F512F3D3272FEE50EB4527603DE0 (void);
// 0x0000006B System.Void ERC1155/<BalanceOf>d__1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CBalanceOfU3Ed__1_SetStateMachine_m6AF4E4155ABC37A3A94695D78B0A266B91662C55 (void);
// 0x0000006C System.Void ERC1155/<BalanceOfBatch>d__2::MoveNext()
extern void U3CBalanceOfBatchU3Ed__2_MoveNext_m6EED73E8199BF4B37338A03EE80469DFC5F7EA60 (void);
// 0x0000006D System.Void ERC1155/<BalanceOfBatch>d__2::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CBalanceOfBatchU3Ed__2_SetStateMachine_m34749E2FCC62C6089CA7D14ECC5008103D62EB8E (void);
// 0x0000006E System.Void ERC1155/<URI>d__3::MoveNext()
extern void U3CURIU3Ed__3_MoveNext_m994ED7A83E78A719CB3A0D3818A730A8C04EB4F1 (void);
// 0x0000006F System.Void ERC1155/<URI>d__3::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CURIU3Ed__3_SetStateMachine_mF1974E58996F7E03BED741B0EE6CB3416B2ACE72 (void);
// 0x00000070 System.Threading.Tasks.Task`1<System.Numerics.BigInteger> ERC20::BalanceOf(System.String,System.String,System.String,System.String,System.String)
extern void ERC20_BalanceOf_mEC315C4821D0098701B261B0BEBC1A2C5B6653D6 (void);
// 0x00000071 System.Threading.Tasks.Task`1<System.String> ERC20::Name(System.String,System.String,System.String,System.String)
extern void ERC20_Name_m5535F0329E289B03EA6922420FCFA008C077831C (void);
// 0x00000072 System.Threading.Tasks.Task`1<System.String> ERC20::Symbol(System.String,System.String,System.String,System.String)
extern void ERC20_Symbol_m1AD978D77261C364EF258C1FB93BE8071F00B026 (void);
// 0x00000073 System.Threading.Tasks.Task`1<System.Numerics.BigInteger> ERC20::Decimals(System.String,System.String,System.String,System.String)
extern void ERC20_Decimals_m3F46DAA1CEDFE86A488FCF1A2BC10AE4E7FFB4FB (void);
// 0x00000074 System.Threading.Tasks.Task`1<System.Numerics.BigInteger> ERC20::TotalSupply(System.String,System.String,System.String,System.String)
extern void ERC20_TotalSupply_mF278916C040D2B71EC160E87AC1853B0EBEB245B (void);
// 0x00000075 System.Void ERC20::.ctor()
extern void ERC20__ctor_m351CB7ACA6A66A62766411BFDBC2ADD36D43F365 (void);
// 0x00000076 System.Void ERC20::.cctor()
extern void ERC20__cctor_m928F4147D85D705E166A5E6349A04247BF300D4D (void);
// 0x00000077 System.Void ERC20/<BalanceOf>d__1::MoveNext()
extern void U3CBalanceOfU3Ed__1_MoveNext_m36D95A3F3A01BEFABEC55A365ED2730403E9456B (void);
// 0x00000078 System.Void ERC20/<BalanceOf>d__1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CBalanceOfU3Ed__1_SetStateMachine_m8699DFD19EEB93AED6E7AA39FDB723211AE5BCB9 (void);
// 0x00000079 System.Void ERC20/<Name>d__2::MoveNext()
extern void U3CNameU3Ed__2_MoveNext_m5D9A4D337DE7457A7E606D536AE1A15B8F9A544B (void);
// 0x0000007A System.Void ERC20/<Name>d__2::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CNameU3Ed__2_SetStateMachine_m49C47D68B3C839B84D14583B1166CE6CF3CCA7B7 (void);
// 0x0000007B System.Void ERC20/<Symbol>d__3::MoveNext()
extern void U3CSymbolU3Ed__3_MoveNext_m7072C6887271018BD4296D3D6B26BE4454E33344 (void);
// 0x0000007C System.Void ERC20/<Symbol>d__3::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSymbolU3Ed__3_SetStateMachine_mB4F3CE1D6541161B57F0FF7DC4AD7695C09BD8E1 (void);
// 0x0000007D System.Void ERC20/<Decimals>d__4::MoveNext()
extern void U3CDecimalsU3Ed__4_MoveNext_m5F53688B23893A6A02A2AD3F3DBA38EB93F27271 (void);
// 0x0000007E System.Void ERC20/<Decimals>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CDecimalsU3Ed__4_SetStateMachine_mE9819F1B7E958708201C635D9DA17E66020F1BC0 (void);
// 0x0000007F System.Void ERC20/<TotalSupply>d__5::MoveNext()
extern void U3CTotalSupplyU3Ed__5_MoveNext_m5991076501DC35DFDCC9BCEBD066050B10B7CD36 (void);
// 0x00000080 System.Void ERC20/<TotalSupply>d__5::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CTotalSupplyU3Ed__5_SetStateMachine_m2B452102301436127D184A33B558C339BE18146B (void);
// 0x00000081 System.Threading.Tasks.Task`1<System.Int32> ERC721::BalanceOf(System.String,System.String,System.String,System.String,System.String)
extern void ERC721_BalanceOf_mF1062240718E051ABFE8F82957509C43F8DFD912 (void);
// 0x00000082 System.Threading.Tasks.Task`1<System.String> ERC721::OwnerOf(System.String,System.String,System.String,System.String,System.String)
extern void ERC721_OwnerOf_m7860416DDCB041D40D7AFB7D06D6CDC1F4841EE1 (void);
// 0x00000083 System.Threading.Tasks.Task`1<System.Collections.Generic.List`1<System.String>> ERC721::OwnerOfBatch(System.String,System.String,System.String,System.String[],System.String,System.String)
extern void ERC721_OwnerOfBatch_mE72D33FAF1E47F507ECDD68E91388EF9031C385C (void);
// 0x00000084 System.Threading.Tasks.Task`1<System.String> ERC721::URI(System.String,System.String,System.String,System.String,System.String)
extern void ERC721_URI_mA39E5DFAE098BA075A61C93AF8FC02BE5E29FB75 (void);
// 0x00000085 System.Void ERC721::.ctor()
extern void ERC721__ctor_m251933444988538E278DEA53D678BE3D7EEF865D (void);
// 0x00000086 System.Void ERC721::.cctor()
extern void ERC721__cctor_m83FEEC760CAD9091959D77867512ECCB4F6620F3 (void);
// 0x00000087 System.Void ERC721/<BalanceOf>d__1::MoveNext()
extern void U3CBalanceOfU3Ed__1_MoveNext_m8015D9AE43B8A99CB9D959E50EAB745432A7F19E (void);
// 0x00000088 System.Void ERC721/<BalanceOf>d__1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CBalanceOfU3Ed__1_SetStateMachine_mD5B12D1AB4C0AE02E3EFC2E56D0E9A69C4A7C0C7 (void);
// 0x00000089 System.Void ERC721/<OwnerOf>d__2::MoveNext()
extern void U3COwnerOfU3Ed__2_MoveNext_m8D8C7B73A05E641AB2DFA344DE4D2FFB5A89E6E9 (void);
// 0x0000008A System.Void ERC721/<OwnerOf>d__2::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COwnerOfU3Ed__2_SetStateMachine_mB9A361356148E8EC7BB543DF7E2F58122EF5685C (void);
// 0x0000008B System.Void ERC721/<OwnerOfBatch>d__3::MoveNext()
extern void U3COwnerOfBatchU3Ed__3_MoveNext_m543097386999349CA5DA05E987905F28052EAC08 (void);
// 0x0000008C System.Void ERC721/<OwnerOfBatch>d__3::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COwnerOfBatchU3Ed__3_SetStateMachine_m94E80236D24084C606B00DAC402B69E5AB05B06A (void);
// 0x0000008D System.Void ERC721/<URI>d__4::MoveNext()
extern void U3CURIU3Ed__4_MoveNext_mA02D956DD4091CF9D6D2962A5836550F95E7F6D0 (void);
// 0x0000008E System.Void ERC721/<URI>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CURIU3Ed__4_SetStateMachine_m83FEB5116327CB0CD5BD2F04AF3E440AA2E52DEA (void);
// 0x0000008F System.Threading.Tasks.Task`1<System.String> EVM::BalanceOf(System.String,System.String,System.String,System.String)
extern void EVM_BalanceOf_m6AF5C0DA67C45C9977B4AAC2C249A36FD84D559E (void);
// 0x00000090 System.Threading.Tasks.Task`1<System.String> EVM::Verify(System.String,System.String)
extern void EVM_Verify_mA8C92E16EF841C6356A849C6E29765534B0EA2CB (void);
// 0x00000091 System.Threading.Tasks.Task`1<System.String> EVM::Call(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern void EVM_Call_m8760C42E4C727A97B7DF290D887E292460F142C4 (void);
// 0x00000092 System.Threading.Tasks.Task`1<System.String> EVM::MultiCall(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern void EVM_MultiCall_m45EA95D0FC82EFF50087446F4ED7C4D407F0B43D (void);
// 0x00000093 System.Threading.Tasks.Task`1<System.String> EVM::TxStatus(System.String,System.String,System.String,System.String)
extern void EVM_TxStatus_mB28FBAF141DDCD343020787DD04064C0C5993E95 (void);
// 0x00000094 System.Threading.Tasks.Task`1<System.Int32> EVM::BlockNumber(System.String,System.String,System.String)
extern void EVM_BlockNumber_m70A74E4A04CAE5B3B50FFA11AFEE0DF6A6A7EC4A (void);
// 0x00000095 System.Threading.Tasks.Task`1<System.String> EVM::Nonce(System.String,System.String,System.String,System.String)
extern void EVM_Nonce_m47224BD603FC6ADE4473C6E4499791AC55A46BFF (void);
// 0x00000096 System.Threading.Tasks.Task`1<System.String> EVM::CreateContractData(System.String,System.String,System.String)
extern void EVM_CreateContractData_m0A69CC06452FBB76AC259D74C32CCC81DBC02A92 (void);
// 0x00000097 System.Threading.Tasks.Task`1<System.String> EVM::AllErc721(System.String,System.String,System.String,System.String,System.Int32,System.Int32)
extern void EVM_AllErc721_m0556274DB1637D6A25E1414A24D33CBA8AD95379 (void);
// 0x00000098 System.Threading.Tasks.Task`1<System.String> EVM::AllErc1155(System.String,System.String,System.String,System.String,System.Int32,System.Int32)
extern void EVM_AllErc1155_mF8CC99C847366BCE65363096D2018749003520D6 (void);
// 0x00000099 System.Threading.Tasks.Task`1<System.String> EVM::GasPrice(System.String,System.String,System.String)
extern void EVM_GasPrice_mFFCC58B0047F8FABEE42963AEB67979F814C23A6 (void);
// 0x0000009A System.Threading.Tasks.Task`1<System.String> EVM::GasLimit(System.String,System.String,System.String,System.String,System.String,System.String)
extern void EVM_GasLimit_mC9C0175B2B65072058C8DA29E1656FD3676E14B3 (void);
// 0x0000009B System.Threading.Tasks.Task`1<System.String> EVM::ChainId(System.String,System.String,System.String)
extern void EVM_ChainId_m5A37B376BE531801A4C4984896B593413935FEA1 (void);
// 0x0000009C System.Threading.Tasks.Task`1<System.String> EVM::CreateTransaction(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern void EVM_CreateTransaction_m9DB2168641491273CE7C782CA1DC042EFD38A6A8 (void);
// 0x0000009D System.Threading.Tasks.Task`1<System.String> EVM::BroadcastTransaction(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern void EVM_BroadcastTransaction_mC3C2718C12BABE0A104C2D9FE68099FF1DD14227 (void);
// 0x0000009E System.Void EVM::.ctor()
extern void EVM__ctor_mC959EDC0C26DF29F2F5C7DA2AC16FAEDEE3EEDAA (void);
// 0x0000009F System.Void EVM::.cctor()
extern void EVM__cctor_mCF0D0C0E582E4773CD57D216964453D8C9A8B302 (void);
// 0x000000A0 System.Void EVM/Response`1::.ctor()
// 0x000000A1 System.Void EVM/<BalanceOf>d__2::MoveNext()
extern void U3CBalanceOfU3Ed__2_MoveNext_m49FF1C68CF752F053B40E31336F0AC117A7B61E8 (void);
// 0x000000A2 System.Void EVM/<BalanceOf>d__2::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CBalanceOfU3Ed__2_SetStateMachine_mD8F5C1681185A6E89A41F1980CE6AABD3050AF10 (void);
// 0x000000A3 System.Void EVM/<Verify>d__3::MoveNext()
extern void U3CVerifyU3Ed__3_MoveNext_mD9331AE8D306DC14F8644E0888C393B1F197956A (void);
// 0x000000A4 System.Void EVM/<Verify>d__3::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CVerifyU3Ed__3_SetStateMachine_m477762C3E935EB66E24F0F2FF4564273024EC780 (void);
// 0x000000A5 System.Void EVM/<Call>d__4::MoveNext()
extern void U3CCallU3Ed__4_MoveNext_m7103426145D023067D0E812AC94242EDC9E96C8E (void);
// 0x000000A6 System.Void EVM/<Call>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCallU3Ed__4_SetStateMachine_m2B4C2DDF94B74ACCA651A9AC53F1CA67853C059F (void);
// 0x000000A7 System.Void EVM/<MultiCall>d__5::MoveNext()
extern void U3CMultiCallU3Ed__5_MoveNext_m59C1E5558D5BD5130431DAD6C5BE122F4259F186 (void);
// 0x000000A8 System.Void EVM/<MultiCall>d__5::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CMultiCallU3Ed__5_SetStateMachine_m77A035AB7DEC5D4342A4617899A02C20AB0E8733 (void);
// 0x000000A9 System.Void EVM/<TxStatus>d__6::MoveNext()
extern void U3CTxStatusU3Ed__6_MoveNext_m21E0D491760958726DCF5290A52E17241AAACFBF (void);
// 0x000000AA System.Void EVM/<TxStatus>d__6::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CTxStatusU3Ed__6_SetStateMachine_m6A59F5AB3D2A6077CF1BA0D1E8353FDEADC0D754 (void);
// 0x000000AB System.Void EVM/<BlockNumber>d__7::MoveNext()
extern void U3CBlockNumberU3Ed__7_MoveNext_m1CA9A036D1F74ECEBBADBEF40970047E3C65B281 (void);
// 0x000000AC System.Void EVM/<BlockNumber>d__7::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CBlockNumberU3Ed__7_SetStateMachine_mBFBF55F1191EB7F3D04C204A5FFC7053EF8C8200 (void);
// 0x000000AD System.Void EVM/<Nonce>d__8::MoveNext()
extern void U3CNonceU3Ed__8_MoveNext_m87AEF2132C7F36BB889C4769E58EF505EE1C5677 (void);
// 0x000000AE System.Void EVM/<Nonce>d__8::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CNonceU3Ed__8_SetStateMachine_m6C0E5BDD185012D9EE78EC7E73F9DB5448B13C1D (void);
// 0x000000AF System.Void EVM/<CreateContractData>d__9::MoveNext()
extern void U3CCreateContractDataU3Ed__9_MoveNext_m3A513FD488EB9248FA899D092048B412F36A2A43 (void);
// 0x000000B0 System.Void EVM/<CreateContractData>d__9::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateContractDataU3Ed__9_SetStateMachine_mC2A74C0432A6E8621E3D510878A7EBB59BA77B96 (void);
// 0x000000B1 System.Void EVM/<AllErc721>d__10::MoveNext()
extern void U3CAllErc721U3Ed__10_MoveNext_mD378DF39149CAF4372C5C6FC3D73DCC66EE1D00B (void);
// 0x000000B2 System.Void EVM/<AllErc721>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAllErc721U3Ed__10_SetStateMachine_mC7F79A8B56C459462E982BFB2007442CB5E77327 (void);
// 0x000000B3 System.Void EVM/<AllErc1155>d__11::MoveNext()
extern void U3CAllErc1155U3Ed__11_MoveNext_m2B1FA74D528BA5675715FEAE5F073E81E1F9AABF (void);
// 0x000000B4 System.Void EVM/<AllErc1155>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAllErc1155U3Ed__11_SetStateMachine_m09EBEC3D74A283E912739821BBC146F9A9306B00 (void);
// 0x000000B5 System.Void EVM/<GasPrice>d__12::MoveNext()
extern void U3CGasPriceU3Ed__12_MoveNext_mD958FF90E9BB9E56039E6FF07C8817FD2D3B0A89 (void);
// 0x000000B6 System.Void EVM/<GasPrice>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGasPriceU3Ed__12_SetStateMachine_mD8A5388148CA31A2177C494B2BF4ACAE993566C7 (void);
// 0x000000B7 System.Void EVM/<GasLimit>d__13::MoveNext()
extern void U3CGasLimitU3Ed__13_MoveNext_m32EDBB43D9022D8AFDD3094B759B3C646F3DF75F (void);
// 0x000000B8 System.Void EVM/<GasLimit>d__13::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGasLimitU3Ed__13_SetStateMachine_mAAB510EC434AE95BC2910728023F47955EA53564 (void);
// 0x000000B9 System.Void EVM/<ChainId>d__14::MoveNext()
extern void U3CChainIdU3Ed__14_MoveNext_m39ABDF10A0AEA824B0A24EA8AC826776E30F1F88 (void);
// 0x000000BA System.Void EVM/<ChainId>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CChainIdU3Ed__14_SetStateMachine_mC3320A62EFF445C636E3E0CBE73AA0C5272DA035 (void);
// 0x000000BB System.Void EVM/<CreateTransaction>d__15::MoveNext()
extern void U3CCreateTransactionU3Ed__15_MoveNext_mC30E9CDA81123979FBD6455141C591E2985C1098 (void);
// 0x000000BC System.Void EVM/<CreateTransaction>d__15::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateTransactionU3Ed__15_SetStateMachine_m07800DBAA5A71F467B51548B88E44168704663BD (void);
// 0x000000BD System.Void EVM/<BroadcastTransaction>d__16::MoveNext()
extern void U3CBroadcastTransactionU3Ed__16_MoveNext_m2912179954FAFC5CD4F687AFBFBC6D35D159A02F (void);
// 0x000000BE System.Void EVM/<BroadcastTransaction>d__16::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CBroadcastTransactionU3Ed__16_SetStateMachine_m6534FEE9D7F78D07C18917E9259F894B414C1E0E (void);
// 0x000000BF System.String Web3PrivateKey::SignTransaction(System.String,System.String,System.String)
extern void Web3PrivateKey_SignTransaction_m1D442D71E0C032375214CD2255613FECEA77B1F7 (void);
// 0x000000C0 System.String Web3PrivateKey::Address(System.String)
extern void Web3PrivateKey_Address_mA98258719769BAC8957BB937616C7C6ADD63A0DB (void);
// 0x000000C1 System.String Web3PrivateKey::Sign(System.String,System.String)
extern void Web3PrivateKey_Sign_m1BDF7691997897152DDE3F2131C36F46635A26FD (void);
// 0x000000C2 System.Void Web3PrivateKey::.ctor()
extern void Web3PrivateKey__ctor_m6433423824F7D6A9F79B29D05E3D421CFCF915DD (void);
// 0x000000C3 System.Threading.Tasks.Task`1<System.String> Web3Wallet::SendTransaction(System.String,System.String,System.String,System.String,System.String,System.String)
extern void Web3Wallet_SendTransaction_mF2426B50D053EF9F8A0D3A6BD8C5A054015D3510 (void);
// 0x000000C4 System.Threading.Tasks.Task`1<System.String> Web3Wallet::Sign(System.String)
extern void Web3Wallet_Sign_m9097546A29A5FBA1F0114EC95EF7097EC0FD8D8A (void);
// 0x000000C5 System.String Web3Wallet::Sha3(System.String)
extern void Web3Wallet_Sha3_m0E052EC4D5241845AFFA6AE980B21E332AA6D21D (void);
// 0x000000C6 System.Void Web3Wallet::.ctor()
extern void Web3Wallet__ctor_mFDE52E181069018DC5683C3D5212D8E7B5D177F8 (void);
// 0x000000C7 System.Void Web3Wallet::.cctor()
extern void Web3Wallet__cctor_mA845968DDB3120128F657BDB3BE458EB18C618B9 (void);
// 0x000000C8 System.Void Web3Wallet/<SendTransaction>d__1::MoveNext()
extern void U3CSendTransactionU3Ed__1_MoveNext_m094A7D4B17DCF83DA8D339C9F0A4AEBA99E81550 (void);
// 0x000000C9 System.Void Web3Wallet/<SendTransaction>d__1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendTransactionU3Ed__1_SetStateMachine_m6C58AE8B37667F289A5318FDC6D296A10C72431B (void);
// 0x000000CA System.Void Web3Wallet/<Sign>d__2::MoveNext()
extern void U3CSignU3Ed__2_MoveNext_mDB3E58221A60D54BD2C7856ACB2E61A9FA18E824 (void);
// 0x000000CB System.Void Web3Wallet/<Sign>d__2::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSignU3Ed__2_SetStateMachine_m80954B22F85344CA5FC5AA3D47EB652446A8DA40 (void);
// 0x000000CC System.Void Web3GL::SendContractJs(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern void Web3GL_SendContractJs_m9F91ED43D46F554F2DCA7B4ED0E43417A2438B24 (void);
// 0x000000CD System.String Web3GL::SendContractResponse()
extern void Web3GL_SendContractResponse_m07C0E7090A2E54F34898D8C9B34DE18D9AB8B61B (void);
// 0x000000CE System.Void Web3GL::SetContractResponse(System.String)
extern void Web3GL_SetContractResponse_m581DED8C3AB4B130CE6A27160134E42064D17897 (void);
// 0x000000CF System.Void Web3GL::SendTransactionJs(System.String,System.String,System.String,System.String)
extern void Web3GL_SendTransactionJs_m4643694BD4CEF40C106AFFD06802F1F7623B6284 (void);
// 0x000000D0 System.String Web3GL::SendTransactionResponse()
extern void Web3GL_SendTransactionResponse_m29FDB8F571CF4646FA2023A88B94E794A9D10A15 (void);
// 0x000000D1 System.Void Web3GL::SetTransactionResponse(System.String)
extern void Web3GL_SetTransactionResponse_m1DEB989EFBBCDDB46687A63F24E47AB2B0B15B9C (void);
// 0x000000D2 System.Void Web3GL::SignMessage(System.String)
extern void Web3GL_SignMessage_mC477404D486FA06FEB49FD1DF69D27694161041C (void);
// 0x000000D3 System.String Web3GL::SignMessageResponse()
extern void Web3GL_SignMessageResponse_mFC3A305C5D857D33399AA53D9D3F8BE7B0753CA5 (void);
// 0x000000D4 System.Void Web3GL::SetSignMessageResponse(System.String)
extern void Web3GL_SetSignMessageResponse_mBB7DBE43E8B7517DD36D3477025F338D72D9FDCF (void);
// 0x000000D5 System.Int32 Web3GL::GetNetwork()
extern void Web3GL_GetNetwork_mC116213F6AD62593B3E6D9F16EFC87A7E336F9A1 (void);
// 0x000000D6 System.Threading.Tasks.Task`1<System.String> Web3GL::SendContract(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern void Web3GL_SendContract_m6675BF158272EAC923D284D5CC10C7F09558E173 (void);
// 0x000000D7 System.Threading.Tasks.Task`1<System.String> Web3GL::SendTransaction(System.String,System.String,System.String,System.String)
extern void Web3GL_SendTransaction_mD40DBCA64188F5159A42BE864CA19D5B61C7F5D9 (void);
// 0x000000D8 System.Threading.Tasks.Task`1<System.String> Web3GL::Sign(System.String)
extern void Web3GL_Sign_mFEAE31488340360AB008F8E3FAE799D1E10871C7 (void);
// 0x000000D9 System.Int32 Web3GL::Network()
extern void Web3GL_Network_m8390042864BF8E5E886B04176B37F6C10C538150 (void);
// 0x000000DA System.Void Web3GL::.ctor()
extern void Web3GL__ctor_m14977243FEA2B06F40615CE85C7522F34B2C6951 (void);
// 0x000000DB System.Void Web3GL/<SendContract>d__10::MoveNext()
extern void U3CSendContractU3Ed__10_MoveNext_mAE0FDF450FEBA139C99D52CECE5D43499DB90708 (void);
// 0x000000DC System.Void Web3GL/<SendContract>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendContractU3Ed__10_SetStateMachine_mDB924150509FEA19525D41521D4EFFBB8CA8C99C (void);
// 0x000000DD System.Void Web3GL/<SendTransaction>d__11::MoveNext()
extern void U3CSendTransactionU3Ed__11_MoveNext_m053E36DD1D85C5856A314131285B48D6F5C070C5 (void);
// 0x000000DE System.Void Web3GL/<SendTransaction>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendTransactionU3Ed__11_SetStateMachine_m324E10096F6699BD39FB4AB1847403016F77A827 (void);
// 0x000000DF System.Void Web3GL/<Sign>d__12::MoveNext()
extern void U3CSignU3Ed__12_MoveNext_m74FB11FDCCAAFD0E2BFF954199891117554D1890 (void);
// 0x000000E0 System.Void Web3GL/<Sign>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSignU3Ed__12_SetStateMachine_mBAA214792AF8C334AC1EDA56E1C0A9B805A5C267 (void);
// 0x000000E1 System.Void ERC1155BalanceOfBatchExample::Start()
extern void ERC1155BalanceOfBatchExample_Start_mB2D18D5D03FFF898BCCD553ED52C25472DDA46A2 (void);
// 0x000000E2 System.Void ERC1155BalanceOfBatchExample::.ctor()
extern void ERC1155BalanceOfBatchExample__ctor_m0236A171447DA49A7761764F9DCE0D7FC4C3B08F (void);
// 0x000000E3 System.Void ERC1155BalanceOfBatchExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_mE94D2F8B51DBFC5B070D07ABF611C7C7D483D016 (void);
// 0x000000E4 System.Void ERC1155BalanceOfBatchExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_m76E05730082FD3FB893CFE4ED257BFD0E1146F2A (void);
// 0x000000E5 System.Void ERC1155BalanceOfExample::Start()
extern void ERC1155BalanceOfExample_Start_mF1D7FC086FAF079516A7463211759E19659E3E19 (void);
// 0x000000E6 System.Void ERC1155BalanceOfExample::.ctor()
extern void ERC1155BalanceOfExample__ctor_m3C08EAD0BA5D101B144908139F5BED8912BF2CE1 (void);
// 0x000000E7 System.Void ERC1155BalanceOfExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_mBE908F669CAC2ABDAC7872701F7C3BED2B4D66C8 (void);
// 0x000000E8 System.Void ERC1155BalanceOfExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_mACD6D05861661B7259297843E6FE635E542144F8 (void);
// 0x000000E9 System.Void ERC1155URIExample::Start()
extern void ERC1155URIExample_Start_m47316F5E9063BA95AB5038A40203C130AE315332 (void);
// 0x000000EA System.Void ERC1155URIExample::.ctor()
extern void ERC1155URIExample__ctor_m1EB728AE1282B0EE260BF3CC994D0745F600E2AE (void);
// 0x000000EB System.Void ERC1155URIExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_mD7E6C3EB57F3683AC190990093F1DF60C5996BDF (void);
// 0x000000EC System.Void ERC1155URIExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_m32F010E483167AED04DE742EB8940EDE54A25941 (void);
// 0x000000ED System.Void ERC20BalanceOfExample::Start()
extern void ERC20BalanceOfExample_Start_mFF41E5445321315891D6684596EFF7494EB4CCEA (void);
// 0x000000EE System.Void ERC20BalanceOfExample::.ctor()
extern void ERC20BalanceOfExample__ctor_m470F363A7E0055B85DD346818056BA00FC77CA0F (void);
// 0x000000EF System.Void ERC20BalanceOfExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m18FDD5025FFAD18D6D5207671EF4439FFBF9117B (void);
// 0x000000F0 System.Void ERC20BalanceOfExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_m8B9F75B4229353D82C1C4D7F5D9B3D0608653F2A (void);
// 0x000000F1 System.Void ERC20DecimalsExample::Start()
extern void ERC20DecimalsExample_Start_mF8A5B405AC67AC0E354341E76FD1674DC451DE85 (void);
// 0x000000F2 System.Void ERC20DecimalsExample::.ctor()
extern void ERC20DecimalsExample__ctor_mD92FAAAD3C3A55AC5B8DFD0C5924F8C93B80345E (void);
// 0x000000F3 System.Void ERC20DecimalsExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_mB8317507C6D1634221D136B56400405740C35941 (void);
// 0x000000F4 System.Void ERC20DecimalsExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_m0E9F22BDB96721E7BD11F85CE5F248ABFF1D4399 (void);
// 0x000000F5 System.Void ERC20NameExample::Start()
extern void ERC20NameExample_Start_m5E0090E513011383498C5799D006C408A0549FFD (void);
// 0x000000F6 System.Void ERC20NameExample::.ctor()
extern void ERC20NameExample__ctor_m2277084D20C11E505CDD500262B45C935905433F (void);
// 0x000000F7 System.Void ERC20NameExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m1009F1D54AA9418573E986DC091E8F6CEA2A152E (void);
// 0x000000F8 System.Void ERC20NameExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_m5C52A6DA998FC196467CC1A8D7DA3F1B2680784A (void);
// 0x000000F9 System.Void ERC20SymbolExample::Start()
extern void ERC20SymbolExample_Start_m44F3237CCD3B3C48CF8216DAADC25FB4C817870A (void);
// 0x000000FA System.Void ERC20SymbolExample::.ctor()
extern void ERC20SymbolExample__ctor_mD300D6C431C733379923241434ED70E78B7998FF (void);
// 0x000000FB System.Void ERC20SymbolExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m7C288E439AA5604812418AC5E663FB9F2463FEF5 (void);
// 0x000000FC System.Void ERC20SymbolExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_m8ECA709670FDBBB8CF6CE3BE68AC77CEE55FEF6B (void);
// 0x000000FD System.Void ERC20TotalSupplyExample::Start()
extern void ERC20TotalSupplyExample_Start_mAD965D99BF2EEA5EF43F8B15CC691404AD553AE2 (void);
// 0x000000FE System.Void ERC20TotalSupplyExample::.ctor()
extern void ERC20TotalSupplyExample__ctor_m0BBB8385A86BD711BB0BECFF421154D3148BD7AC (void);
// 0x000000FF System.Void ERC20TotalSupplyExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m9684BD13AB1E3C45FEF01FB8189CB1CC84CB872B (void);
// 0x00000100 System.Void ERC20TotalSupplyExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_m90AE0209DA8EBB3AE0D980935EAEC3783FB842AD (void);
// 0x00000101 System.Void ERC721BalanceOfExample::Start()
extern void ERC721BalanceOfExample_Start_m5C514F94087DD8AFF72E9D283FF33BD6175B2EF2 (void);
// 0x00000102 System.Void ERC721BalanceOfExample::.ctor()
extern void ERC721BalanceOfExample__ctor_mE3CDB9E379F5A95A8C05061EC3D3397B561F4192 (void);
// 0x00000103 System.Void ERC721BalanceOfExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m7FC8B7D9E9B28572F8FE2AB87C3DF77F8D3DC143 (void);
// 0x00000104 System.Void ERC721BalanceOfExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_mE60DC6B0816FFFA80349E8D40C3CFC426F8001B7 (void);
// 0x00000105 System.Void ERC721OwnerOfBatchExample::Start()
extern void ERC721OwnerOfBatchExample_Start_m8CF7A69B7BFA086DD735B18E5A2395F422414DA6 (void);
// 0x00000106 System.Void ERC721OwnerOfBatchExample::.ctor()
extern void ERC721OwnerOfBatchExample__ctor_m0584E4443C2F44570F67FC5E7349A8B65AEED216 (void);
// 0x00000107 System.Void ERC721OwnerOfBatchExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m2251CA0D2B663CC81ACCA1DF52CF03390A34009A (void);
// 0x00000108 System.Void ERC721OwnerOfBatchExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_mE6B01D55F9D6F1B641203AD045DA10D399ABC770 (void);
// 0x00000109 System.Void ERC721OwnerOfExample::Start()
extern void ERC721OwnerOfExample_Start_m9D7F5468A89DEC19DEF62AD36B527F91BF6792C1 (void);
// 0x0000010A System.Void ERC721OwnerOfExample::.ctor()
extern void ERC721OwnerOfExample__ctor_mBEB0A6B85F70347883731AB42B10D30EB9FC44ED (void);
// 0x0000010B System.Void ERC721OwnerOfExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m4ED7CA82E64E03491B0A4A643ECBE32AD09ADED1 (void);
// 0x0000010C System.Void ERC721OwnerOfExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_m0ECA16AF5C9507944D8F09DD555E373FC01AF5F7 (void);
// 0x0000010D System.Void ERC721URIExample::Start()
extern void ERC721URIExample_Start_m0EACF752E96AE14675BB65815ACEBA99F6494291 (void);
// 0x0000010E System.Void ERC721URIExample::.ctor()
extern void ERC721URIExample__ctor_mC50A975F37511859DA883D76394FC3D18B320C65 (void);
// 0x0000010F System.Void ERC721URIExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m51BDCBDA7FC0D710D01081E177173067F022F3EE (void);
// 0x00000110 System.Void ERC721URIExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_mB281CC760C5BC7236B4D097976C68D5CF60EF754 (void);
// 0x00000111 System.Void AllErc1155Example::Start()
extern void AllErc1155Example_Start_m98450BC383F06E18DD3E997CA40678AA77B3ED45 (void);
// 0x00000112 System.Void AllErc1155Example::.ctor()
extern void AllErc1155Example__ctor_m792ED55745E0341DF0F9C12A8D757B18D452FDD4 (void);
// 0x00000113 System.String AllErc1155Example/NFTs::get_contract()
extern void NFTs_get_contract_m132FBACD650BBB034871A7BEAA4A8A5E69890D65 (void);
// 0x00000114 System.Void AllErc1155Example/NFTs::set_contract(System.String)
extern void NFTs_set_contract_m5806E56C6DAE7193AB5CD81D9F474D07A35B851E (void);
// 0x00000115 System.String AllErc1155Example/NFTs::get_tokenId()
extern void NFTs_get_tokenId_m82B87799B5788DA9B1406347C6E7BE76CC00907F (void);
// 0x00000116 System.Void AllErc1155Example/NFTs::set_tokenId(System.String)
extern void NFTs_set_tokenId_mEED2527C52CB89056919196F37BB93FC91703C8B (void);
// 0x00000117 System.String AllErc1155Example/NFTs::get_uri()
extern void NFTs_get_uri_m24476236E60F7F31C888F8E68D053AF7D9B88C0A (void);
// 0x00000118 System.Void AllErc1155Example/NFTs::set_uri(System.String)
extern void NFTs_set_uri_mA7C27EE09FE391D53EA831A4ECE9B915DAE66CBB (void);
// 0x00000119 System.String AllErc1155Example/NFTs::get_balance()
extern void NFTs_get_balance_mCBFB0194B7ABA4A6B584E176B16282A5F2109424 (void);
// 0x0000011A System.Void AllErc1155Example/NFTs::set_balance(System.String)
extern void NFTs_set_balance_m4045F162A6C0E6616F9AEE022F3204229347A37C (void);
// 0x0000011B System.Void AllErc1155Example/NFTs::.ctor()
extern void NFTs__ctor_m97BE3AED0C12AB78FCCB5F44313F6D8D673787CA (void);
// 0x0000011C System.Void AllErc1155Example/<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_m56F7B4255F3860898DFF3B4A9A4A42827AD9C42F (void);
// 0x0000011D System.Void AllErc1155Example/<Start>d__1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__1_SetStateMachine_mF4BABB9B81366714D470D287C46918A5C6D7EB6A (void);
// 0x0000011E System.Void AllErc721Example::Start()
extern void AllErc721Example_Start_m1173828869CE0CA1368EC96797AB780B26007185 (void);
// 0x0000011F System.Void AllErc721Example::.ctor()
extern void AllErc721Example__ctor_m7ADB3B7A840C832B488C1EFFA54D0F259918BA7D (void);
// 0x00000120 System.String AllErc721Example/NFTs::get_contract()
extern void NFTs_get_contract_mEF0AF77D33D8D1793D68D7BD429C144F2DBECC0D (void);
// 0x00000121 System.Void AllErc721Example/NFTs::set_contract(System.String)
extern void NFTs_set_contract_mD64E897A626ADCEE21998CE15BFF69AD5F762634 (void);
// 0x00000122 System.String AllErc721Example/NFTs::get_tokenId()
extern void NFTs_get_tokenId_m9EFD2AC01B73E801C3BC8B45FB7268099F8979EF (void);
// 0x00000123 System.Void AllErc721Example/NFTs::set_tokenId(System.String)
extern void NFTs_set_tokenId_m24AE780605FE57573BC4ED1FF58B1890B9C98686 (void);
// 0x00000124 System.String AllErc721Example/NFTs::get_uri()
extern void NFTs_get_uri_mC235611E28E2B8ACC01BA87194FC197DB55AD436 (void);
// 0x00000125 System.Void AllErc721Example/NFTs::set_uri(System.String)
extern void NFTs_set_uri_mAD2F72E336F6B548906B4D9A68BB7CF28FDA325B (void);
// 0x00000126 System.String AllErc721Example/NFTs::get_balance()
extern void NFTs_get_balance_m9815F7685CFBDFB187A2EBC45307D1353622B4FA (void);
// 0x00000127 System.Void AllErc721Example/NFTs::set_balance(System.String)
extern void NFTs_set_balance_m343CFEB6DFF4A498F2127634C5EA9041E505AF37 (void);
// 0x00000128 System.Void AllErc721Example/NFTs::.ctor()
extern void NFTs__ctor_m96B5DA690CC6A9867780A5210BE0D6897F744172 (void);
// 0x00000129 System.Void AllErc721Example/<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_mCF92100CA8DD409E5D8F73CBA00D4C85FEC94D1B (void);
// 0x0000012A System.Void AllErc721Example/<Start>d__1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__1_SetStateMachine_mCD7FDC90AA399D1DB295620538CA230DEAB95A1F (void);
// 0x0000012B System.Void BlockNumberExample::Start()
extern void BlockNumberExample_Start_mAE8F8CFCAE848DAA2C9FC0E971741520E7BA3EFD (void);
// 0x0000012C System.Void BlockNumberExample::.ctor()
extern void BlockNumberExample__ctor_m192B9D1EE3FE48031D94F858FEB5365D4976E7C7 (void);
// 0x0000012D System.Void BlockNumberExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m1C1A1991718A66B2A3ED5D056A9B82B28530ADA8 (void);
// 0x0000012E System.Void BlockNumberExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_m1B759B91C18F303ECF8AA29965839045F5FB49C9 (void);
// 0x0000012F System.Void CreateContractDataExample::Start()
extern void CreateContractDataExample_Start_m975FDA0E1478E7283C9B3F95C5D4A825CFCC500E (void);
// 0x00000130 System.Void CreateContractDataExample::.ctor()
extern void CreateContractDataExample__ctor_mCB4953F16A06B18F07E5DD94285EB30FABE5C8DD (void);
// 0x00000131 System.Void CreateContractDataExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m4BC1DF55905C7CAE4ECD9A68314A3A327C5BC727 (void);
// 0x00000132 System.Void CreateContractDataExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_m90FCB1CDB53828A20360A302E41321FA2915A8B7 (void);
// 0x00000133 System.Void CustomCallExample::Mint()
extern void CustomCallExample_Mint_mA9AF3C5B8DC09D051DB0DB7EE46FC8F3F1ECFB34 (void);
// 0x00000134 System.Void CustomCallExample::Awake()
extern void CustomCallExample_Awake_m60E4F5F3D9586022C4A2A86A6A1C6921AD77991F (void);
// 0x00000135 System.Void CustomCallExample::ChangeName()
extern void CustomCallExample_ChangeName_m11A572AD5E78FF9DBC799B37BB3C7DFE0EE95A34 (void);
// 0x00000136 System.Void CustomCallExample::ReLoad()
extern void CustomCallExample_ReLoad_m9ED3FF4E3B3388BC59DD339CF4E2ADE587DB03FF (void);
// 0x00000137 System.Void CustomCallExample::GetNft()
extern void CustomCallExample_GetNft_mB0ABD70FEE9F630BB81163A45BE6C18B72674981 (void);
// 0x00000138 System.Void CustomCallExample::Burn()
extern void CustomCallExample_Burn_mF950FC3007B36EB9AF4C968F7FFCDDF6B31B09C8 (void);
// 0x00000139 System.Void CustomCallExample::.ctor()
extern void CustomCallExample__ctor_m9377564E09D348C8B80520854B781DB031019208 (void);
// 0x0000013A System.String CustomCallExample/NFTs::get_contract()
extern void NFTs_get_contract_mF6232E648620A06789385C55D8180A8CF7464944 (void);
// 0x0000013B System.Void CustomCallExample/NFTs::set_contract(System.String)
extern void NFTs_set_contract_m4F35AAB24C4FD78BF470D1F797C126463E38FB60 (void);
// 0x0000013C System.String CustomCallExample/NFTs::get_tokenId()
extern void NFTs_get_tokenId_mDF28A5F14194E236C2FF36FF1822BB37FA5F1774 (void);
// 0x0000013D System.Void CustomCallExample/NFTs::set_tokenId(System.String)
extern void NFTs_set_tokenId_m240F331C0FD015FEFCD9B9C8AAF3ABBFD9D38D71 (void);
// 0x0000013E System.String CustomCallExample/NFTs::get_uri()
extern void NFTs_get_uri_mFFE0E2FAC27B394D476A8ED8A87D3D75F58EA390 (void);
// 0x0000013F System.Void CustomCallExample/NFTs::set_uri(System.String)
extern void NFTs_set_uri_mF53EB20E483FA1467C8BCC0764A02EC97B64FDED (void);
// 0x00000140 System.String CustomCallExample/NFTs::get_balance()
extern void NFTs_get_balance_m53BCCA62B6C840C1D66DDA2AE8E9D8BB6AFAD2AC (void);
// 0x00000141 System.Void CustomCallExample/NFTs::set_balance(System.String)
extern void NFTs_set_balance_m32CAB14E10B0A94AA9A0093C4133AC20E4775BAA (void);
// 0x00000142 System.Void CustomCallExample/NFTs::.ctor()
extern void NFTs__ctor_mB91B27B80909E3D31BF71B5971902137A417FD76 (void);
// 0x00000143 System.Void CustomCallExample/<Mint>d__6::MoveNext()
extern void U3CMintU3Ed__6_MoveNext_mAA93E837E07DB6EAFAEDEFE955B062A1F5430EB6 (void);
// 0x00000144 System.Void CustomCallExample/<Mint>d__6::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CMintU3Ed__6_SetStateMachine_mE34E1011DA6AE67062B3638D5CA080CB82EE7586 (void);
// 0x00000145 System.Void CustomCallExample/<ChangeName>d__8::MoveNext()
extern void U3CChangeNameU3Ed__8_MoveNext_m8BA79E020BBD8A22B2B2ADE01C887C8673214CA0 (void);
// 0x00000146 System.Void CustomCallExample/<ChangeName>d__8::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CChangeNameU3Ed__8_SetStateMachine_m2F1CDC60D482E2AFCE6FBEA95ECA5648BBE31409 (void);
// 0x00000147 System.Void CustomCallExample/<ReLoad>d__9::MoveNext()
extern void U3CReLoadU3Ed__9_MoveNext_m27927DFB1EEB05FC2A70ED64B9A665BC68F5BF4B (void);
// 0x00000148 System.Void CustomCallExample/<ReLoad>d__9::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CReLoadU3Ed__9_SetStateMachine_m89009A42D7FAC4B90BC171A10997A50E3AFDE867 (void);
// 0x00000149 System.Void CustomCallExample/<GetNft>d__10::MoveNext()
extern void U3CGetNftU3Ed__10_MoveNext_mBDE73D4F04CD780307D74E27B7121D5C5C202DD5 (void);
// 0x0000014A System.Void CustomCallExample/<GetNft>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetNftU3Ed__10_SetStateMachine_m110A5F8CA3599AE1E562A01BAE0B1194F8801D48 (void);
// 0x0000014B System.Void CustomCallExample/<Burn>d__11::MoveNext()
extern void U3CBurnU3Ed__11_MoveNext_m933C2B659AB810835B3B43A240113ABEB2CD4977 (void);
// 0x0000014C System.Void CustomCallExample/<Burn>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CBurnU3Ed__11_SetStateMachine_m1F8C91D9B818F4B52BF56BBC71398F27DFFF3417 (void);
// 0x0000014D System.Void CustomRPCExample::Start()
extern void CustomRPCExample_Start_mA3CCBB955CEA57395864A8F32683C8763BEFDC33 (void);
// 0x0000014E System.Void CustomRPCExample::.ctor()
extern void CustomRPCExample__ctor_m367121EA923134B9133C4F152AFD5C96D9B22D74 (void);
// 0x0000014F System.Void CustomRPCExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m9D1B1A888D55782764A3D7818C3741A2C53ADFD0 (void);
// 0x00000150 System.Void CustomRPCExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_mA01B28EE49B96A3BB28545E7375E2E4823E4F158 (void);
// 0x00000151 System.Void EthBalanceOfExample::Start()
extern void EthBalanceOfExample_Start_m38948E53166841C031BB586EE77C4CC412FAA71F (void);
// 0x00000152 System.Void EthBalanceOfExample::.ctor()
extern void EthBalanceOfExample__ctor_mEF83438420A6B83C6D919D21E60178FCB485E0EE (void);
// 0x00000153 System.Void EthBalanceOfExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m87DC3F4DB835025DF80DEA749A2CD4BFC153321A (void);
// 0x00000154 System.Void EthBalanceOfExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_m8763265A5E48878995143FE2211C1135DD40A431 (void);
// 0x00000155 System.Void NonceExample::Start()
extern void NonceExample_Start_mE7130950BE1D260F4460F2F6DD25E5ADF4209C0D (void);
// 0x00000156 System.Void NonceExample::.ctor()
extern void NonceExample__ctor_m32153CBCD408BC9F3CAE45DD51309D62857D5971 (void);
// 0x00000157 System.Void NonceExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m46792A67B48E13440730A2271EFD9D3F37DCE540 (void);
// 0x00000158 System.Void NonceExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_mE36D9D616EF594199062DEBE0273AC423B4D8347 (void);
// 0x00000159 System.Void TxStatus::Start()
extern void TxStatus_Start_mFA223A713B485F7F2E598A7EB56054D55BB28DB9 (void);
// 0x0000015A System.Void TxStatus::.ctor()
extern void TxStatus__ctor_m03BD513E1882C5FB6BFF2271B82D657E965715D0 (void);
// 0x0000015B System.Void TxStatus/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m0BD1687277E3B971A8A377BD0627EF8FD774A481 (void);
// 0x0000015C System.Void TxStatus/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_mF482F4B6CA65515CF4469D1B6BCD7C43F86BB665 (void);
// 0x0000015D System.Void VerifyExample::Start()
extern void VerifyExample_Start_m00DC30EC005ED55D223D40563C7E5BFA50D78296 (void);
// 0x0000015E System.Void VerifyExample::.ctor()
extern void VerifyExample__ctor_m2B5C4222C3E6D88436ABE99CAAF80C23971A51BC (void);
// 0x0000015F System.Void VerifyExample/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m4C83C0CAB5D8222B58E90AD7B4B1D556C6D19ED1 (void);
// 0x00000160 System.Void VerifyExample/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_m8B1480939675F9433880643CD4EA0C534881209D (void);
// 0x00000161 System.Void Web3PrivateKeyAddressExample::Start()
extern void Web3PrivateKeyAddressExample_Start_mB8AA36F9637A8476D25A029946190EF91966E481 (void);
// 0x00000162 System.Void Web3PrivateKeyAddressExample::.ctor()
extern void Web3PrivateKeyAddressExample__ctor_m708C97875851DBF493A581C399C99B1BF3921E3F (void);
// 0x00000163 System.Void Web3PrivateKeySend1155Example::OnSend1155()
extern void Web3PrivateKeySend1155Example_OnSend1155_m244501F1769E054C6D39622BD3439BFBA32416CD (void);
// 0x00000164 System.Void Web3PrivateKeySend1155Example::.ctor()
extern void Web3PrivateKeySend1155Example__ctor_m2301B78E83C1AE7D4C1D856910722B006CFEC83E (void);
// 0x00000165 System.Void Web3PrivateKeySend1155Example/<OnSend1155>d__0::MoveNext()
extern void U3COnSend1155U3Ed__0_MoveNext_m5CC620F10F35AC4D9113A097C0F0C6999696C126 (void);
// 0x00000166 System.Void Web3PrivateKeySend1155Example/<OnSend1155>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnSend1155U3Ed__0_SetStateMachine_m6044558A9818E8F112CEFE3BF401EDE447831294 (void);
// 0x00000167 System.Void Web3PrivateKeySend20Example::OnSend20()
extern void Web3PrivateKeySend20Example_OnSend20_m25112C041C89E4656FB9F97A75A0F4D65E827540 (void);
// 0x00000168 System.Void Web3PrivateKeySend20Example::.ctor()
extern void Web3PrivateKeySend20Example__ctor_m3223A050C097F9CF2C45ECA72F3A42EEC0372F77 (void);
// 0x00000169 System.Void Web3PrivateKeySend20Example/<OnSend20>d__0::MoveNext()
extern void U3COnSend20U3Ed__0_MoveNext_mE8FEC374CB159E1AC59FB9226035C06D794D6855 (void);
// 0x0000016A System.Void Web3PrivateKeySend20Example/<OnSend20>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnSend20U3Ed__0_SetStateMachine_m2459E52710C5D0C1641F042AA1246E79DB87C834 (void);
// 0x0000016B System.Void Web3PrivateKeySend721Example::OnSend721()
extern void Web3PrivateKeySend721Example_OnSend721_m7DF532A7644D27B2CBBC1EDDF024613064342487 (void);
// 0x0000016C System.Void Web3PrivateKeySend721Example::.ctor()
extern void Web3PrivateKeySend721Example__ctor_mA771D3B9F5BE3ADA021494896231710B7F466E49 (void);
// 0x0000016D System.Void Web3PrivateKeySend721Example/<OnSend721>d__0::MoveNext()
extern void U3COnSend721U3Ed__0_MoveNext_m92930AB8A01B81E9C029494FC9E7885D52300E41 (void);
// 0x0000016E System.Void Web3PrivateKeySend721Example/<OnSend721>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnSend721U3Ed__0_SetStateMachine_m2CDC6C3A888109DBFB4B350C5D3969E5E9805EF3 (void);
// 0x0000016F System.Void Web3PrivateKeySendTransactionExample::OnSendTransaction()
extern void Web3PrivateKeySendTransactionExample_OnSendTransaction_m096857E458B9BFEB768B0BA0088D8C396C14FD63 (void);
// 0x00000170 System.Void Web3PrivateKeySendTransactionExample::.ctor()
extern void Web3PrivateKeySendTransactionExample__ctor_mE80624EACC2E1183E5A3A1E203FF64D6AB3EC02B (void);
// 0x00000171 System.Void Web3PrivateKeySendTransactionExample/<OnSendTransaction>d__0::MoveNext()
extern void U3COnSendTransactionU3Ed__0_MoveNext_m83BE0A4E0DE4D5065D305BEF0E6FD2020BDDA22E (void);
// 0x00000172 System.Void Web3PrivateKeySendTransactionExample/<OnSendTransaction>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnSendTransactionU3Ed__0_SetStateMachine_m75CF9DD43E6B9372BE208D6144B6D3D389EDD257 (void);
// 0x00000173 System.Void Web3PrivateKeySignMessageExample::Start()
extern void Web3PrivateKeySignMessageExample_Start_m781E7927FF493B132E114D69245F899F9FB91DA1 (void);
// 0x00000174 System.Void Web3PrivateKeySignMessageExample::.ctor()
extern void Web3PrivateKeySignMessageExample__ctor_m481CC8024D641087A48BD47F36BDD1B45DE0F5C7 (void);
// 0x00000175 System.Void Web3WalletLogOut::OnLogOut()
extern void Web3WalletLogOut_OnLogOut_mB3E43BB3729C2DBA1781F3B9FB86B5381DD89422 (void);
// 0x00000176 System.Void Web3WalletLogOut::.ctor()
extern void Web3WalletLogOut__ctor_mE293D3C9EA99537F130D3E97E0E8ED8381B816C5 (void);
// 0x00000177 System.Void Web3WalletSendTransactionExample::OnSendTransaction()
extern void Web3WalletSendTransactionExample_OnSendTransaction_m4FA8621476FD0880D88D0F52337D40760ED02D06 (void);
// 0x00000178 System.Void Web3WalletSendTransactionExample::.ctor()
extern void Web3WalletSendTransactionExample__ctor_mA23EB67764399D5BBBEC73C13901C4B64BABF0F6 (void);
// 0x00000179 System.Void Web3WalletSendTransactionExample/<OnSendTransaction>d__0::MoveNext()
extern void U3COnSendTransactionU3Ed__0_MoveNext_mCE4513700F2438755640D4CF601F7B4D7E7A7321 (void);
// 0x0000017A System.Void Web3WalletSendTransactionExample/<OnSendTransaction>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnSendTransactionU3Ed__0_SetStateMachine_mA1354644175F2D58C90730B1FDB4869FA7BDFC48 (void);
// 0x0000017B System.Void Web3WalletSha3Example::Start()
extern void Web3WalletSha3Example_Start_mFEF54A29017F49078D49A977FB22CA1BEFC18741 (void);
// 0x0000017C System.Void Web3WalletSha3Example::.ctor()
extern void Web3WalletSha3Example__ctor_m2ABD3EDDBFF4FB9414E4C50D7BB047377109109D (void);
// 0x0000017D System.Void Web3WalletSignMessageExample::OnSignMessage()
extern void Web3WalletSignMessageExample_OnSignMessage_m6A3BA73D6F59E338F04E01A2E3912198F487BBDA (void);
// 0x0000017E System.Void Web3WalletSignMessageExample::.ctor()
extern void Web3WalletSignMessageExample__ctor_m2689DCC77E5FBDD5A3609C89731909CA2D1F79C0 (void);
// 0x0000017F System.Void Web3WalletSignMessageExample/<OnSignMessage>d__0::MoveNext()
extern void U3COnSignMessageU3Ed__0_MoveNext_m910B0F834534ADBBA4DEA91CF2B62C5EBA533EBE (void);
// 0x00000180 System.Void Web3WalletSignMessageExample/<OnSignMessage>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnSignMessageU3Ed__0_SetStateMachine_m4338BE5F3F9B7C77682650825239AC6C7138A6B1 (void);
// 0x00000181 System.Void Web3WalletTransfer1155Example::OnTransfer1155()
extern void Web3WalletTransfer1155Example_OnTransfer1155_m6CC73312B266A7E1B7974E6CB782503B376758E3 (void);
// 0x00000182 System.Void Web3WalletTransfer1155Example::.ctor()
extern void Web3WalletTransfer1155Example__ctor_m172D22E1D8D9C86AC1772C79274A35C96782BF83 (void);
// 0x00000183 System.Void Web3WalletTransfer1155Example/<OnTransfer1155>d__0::MoveNext()
extern void U3COnTransfer1155U3Ed__0_MoveNext_mA90B1AC56399F741DB2348B2941FDBF72E9AC597 (void);
// 0x00000184 System.Void Web3WalletTransfer1155Example/<OnTransfer1155>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnTransfer1155U3Ed__0_SetStateMachine_mE5364F3E8E033AA906ACE7657884281076A015C5 (void);
// 0x00000185 System.Void Web3WalletTransfer20Example::OnTransfer20()
extern void Web3WalletTransfer20Example_OnTransfer20_mD19680408F257116D802637BCAFA6AE2D4C049D1 (void);
// 0x00000186 System.Void Web3WalletTransfer20Example::.ctor()
extern void Web3WalletTransfer20Example__ctor_m97D5F927B2BF9758CD75C1675BAA53EACA3C2AF8 (void);
// 0x00000187 System.Void Web3WalletTransfer20Example/<OnTransfer20>d__0::MoveNext()
extern void U3COnTransfer20U3Ed__0_MoveNext_mAC06EED0D1221620A17D9BE470EA8130592D27BE (void);
// 0x00000188 System.Void Web3WalletTransfer20Example/<OnTransfer20>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnTransfer20U3Ed__0_SetStateMachine_mF8F73F2742E7641DF16BA8DAD19AB3A74142E61C (void);
// 0x00000189 System.Void Web3WalletTransfer721Example::OnTransfer721()
extern void Web3WalletTransfer721Example_OnTransfer721_mF62FEAA1F0ED77EA1D20ABDDA5F08A19908BD314 (void);
// 0x0000018A System.Void Web3WalletTransfer721Example::.ctor()
extern void Web3WalletTransfer721Example__ctor_mB2368760CF92616211A427427C6BC6B0BB973E98 (void);
// 0x0000018B System.Void Web3WalletTransfer721Example/<OnTransfer721>d__0::MoveNext()
extern void U3COnTransfer721U3Ed__0_MoveNext_m8C1BB1B0A36207ABDD5BB95E54BEA6EE2265F07F (void);
// 0x0000018C System.Void Web3WalletTransfer721Example/<OnTransfer721>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnTransfer721U3Ed__0_SetStateMachine_mF885565CD1B11CCD1E3057612D11154B7629129D (void);
// 0x0000018D System.Void WebGLSendContractExample::OnSendContract()
extern void WebGLSendContractExample_OnSendContract_m1A4B193810A8347EB809FAA076CE10CE962713C2 (void);
// 0x0000018E System.Void WebGLSendContractExample::.ctor()
extern void WebGLSendContractExample__ctor_m13E3954741DC488ACDF798E9FC9AC6B98E89C125 (void);
// 0x0000018F System.Void WebGLSendContractExample/<OnSendContract>d__0::MoveNext()
extern void U3COnSendContractU3Ed__0_MoveNext_m87D0E94C972176E1F9AEA97BCEFD7CBBF79BB814 (void);
// 0x00000190 System.Void WebGLSendContractExample/<OnSendContract>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnSendContractU3Ed__0_SetStateMachine_m7A097F8F1BB131B8A785D2F22D1D28D7527646EA (void);
// 0x00000191 System.Void WebGLSendTransactionExample::OnSendTransaction()
extern void WebGLSendTransactionExample_OnSendTransaction_mE9BD097CBD52F83717726BFBB8B55265A7BEA7F8 (void);
// 0x00000192 System.Void WebGLSendTransactionExample::.ctor()
extern void WebGLSendTransactionExample__ctor_mB33E6B0437A5D91B82624F4CB38AFDE7406424A5 (void);
// 0x00000193 System.Void WebGLSendTransactionExample/<OnSendTransaction>d__0::MoveNext()
extern void U3COnSendTransactionU3Ed__0_MoveNext_mC7CE9060F43BA816093410666933E4EBB204561B (void);
// 0x00000194 System.Void WebGLSendTransactionExample/<OnSendTransaction>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnSendTransactionU3Ed__0_SetStateMachine_mC12443C49C60F7DCAA5CDA5E716B32E16BD74723 (void);
// 0x00000195 System.Void WebGLSignMessageExample::OnSignMessage()
extern void WebGLSignMessageExample_OnSignMessage_mEDA9C904A9601BA4E7A4EBD354297A6DB24A5279 (void);
// 0x00000196 System.Void WebGLSignMessageExample::.ctor()
extern void WebGLSignMessageExample__ctor_m7D4E73C2DF52C0647FE4BC055E1BB411E7AD7D1C (void);
// 0x00000197 System.Void WebGLSignMessageExample/<OnSignMessage>d__0::MoveNext()
extern void U3COnSignMessageU3Ed__0_MoveNext_m3057F7982AC1CB0EDD2664D8ED6B7E5AC4ADE2A1 (void);
// 0x00000198 System.Void WebGLSignMessageExample/<OnSignMessage>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnSignMessageU3Ed__0_SetStateMachine_m3F0DC2AEF8F20669EFA57A92C67E1F9A5F4B6802 (void);
// 0x00000199 System.Void WebGLSignOut::OnSignOut()
extern void WebGLSignOut_OnSignOut_mF873859FF1D5698D45BC012378D6091E809D54A1 (void);
// 0x0000019A System.Void WebGLSignOut::.ctor()
extern void WebGLSignOut__ctor_m41F9C4F86BE31A82BC8C895BB1E1E1D6AD3EE428 (void);
// 0x0000019B System.Void WebGLTransfer1155::SafeTransferFrom()
extern void WebGLTransfer1155_SafeTransferFrom_m60027B3FC9E5908D1DAC6D38F279E525F9E69282 (void);
// 0x0000019C System.Void WebGLTransfer1155::.ctor()
extern void WebGLTransfer1155__ctor_mAD1A671E8D5A6AEDC9416FC5BB6E7B1236722CAD (void);
// 0x0000019D System.Void WebGLTransfer1155/<SafeTransferFrom>d__5::MoveNext()
extern void U3CSafeTransferFromU3Ed__5_MoveNext_m64353C3BCC55943924533B82580063995694E676 (void);
// 0x0000019E System.Void WebGLTransfer1155/<SafeTransferFrom>d__5::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSafeTransferFromU3Ed__5_SetStateMachine_m357DFCF039FEA8B6C3395C25DFC5753549AD9156 (void);
// 0x0000019F System.Void WebGLTransfer20::Transfer()
extern void WebGLTransfer20_Transfer_mC189BF2DC9623CEED6D1285FFF55B985A9F95FB8 (void);
// 0x000001A0 System.Void WebGLTransfer20::.ctor()
extern void WebGLTransfer20__ctor_m2A916DD8676ECCDBD740AD6D27204D8F8987DDC3 (void);
// 0x000001A1 System.Void WebGLTransfer20/<Transfer>d__4::MoveNext()
extern void U3CTransferU3Ed__4_MoveNext_m5655C8D900D8FDBA8EF1BB4A1111791D22525BFD (void);
// 0x000001A2 System.Void WebGLTransfer20/<Transfer>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CTransferU3Ed__4_SetStateMachine_mA32D0A2CF0FC0E65AD5EA5081D9B5860C23936DE (void);
// 0x000001A3 System.Void WebGLTransfer721::SafeTransferFrom()
extern void WebGLTransfer721_SafeTransferFrom_m48A1C85515A84E75A7ADB433BD7D6011D307848B (void);
// 0x000001A4 System.Void WebGLTransfer721::.ctor()
extern void WebGLTransfer721__ctor_m1A7A94EFE2244A7AB1F57EF77E5323EC7BEC1304 (void);
// 0x000001A5 System.Void WebGLTransfer721/<SafeTransferFrom>d__4::MoveNext()
extern void U3CSafeTransferFromU3Ed__4_MoveNext_m967807132732773C90E46AD493832B9237EA807B (void);
// 0x000001A6 System.Void WebGLTransfer721/<SafeTransferFrom>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSafeTransferFromU3Ed__4_SetStateMachine_m72420EB4DE07C850B47609F4910E992BFAA0CC10 (void);
// 0x000001A7 System.Void WalletLogin::Start()
extern void WalletLogin_Start_mEE859A6D79844237B6727D49BF0ABED6E5DABE08 (void);
// 0x000001A8 System.Void WalletLogin::OnLogin()
extern void WalletLogin_OnLogin_mADEC61B177A2617C428988B3EC727ED17679DE7D (void);
// 0x000001A9 System.Void WalletLogin::.ctor()
extern void WalletLogin__ctor_m6F4AF712F9EF7D444679E887D6FBB2A5BD43C4A3 (void);
// 0x000001AA System.Void WalletLogin/<OnLogin>d__2::MoveNext()
extern void U3COnLoginU3Ed__2_MoveNext_mC494AC5CF4F7CB0F482C28FCBC80A560EB1EDAFB (void);
// 0x000001AB System.Void WalletLogin/<OnLogin>d__2::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnLoginU3Ed__2_SetStateMachine_mB0FF2BA206BB13E75568802ED92D9C2A4609EBC1 (void);
// 0x000001AC System.Void WebLogin::Web3Connect()
extern void WebLogin_Web3Connect_m328539A6C6D4351E03FF94C6D7FCAA00F65FFFAD (void);
// 0x000001AD System.String WebLogin::ConnectAccount()
extern void WebLogin_ConnectAccount_mC1E264919C9BEB95A48D4B2F73CF6AE68BE907E7 (void);
// 0x000001AE System.Void WebLogin::SetConnectAccount(System.String)
extern void WebLogin_SetConnectAccount_mFB37E0B21EC829A9BD04821AFD16E75F3522353A (void);
// 0x000001AF System.Void WebLogin::OnLogin()
extern void WebLogin_OnLogin_m1106D41C77BEC6727F80A708A1EBE5EC42D5F932 (void);
// 0x000001B0 System.Void WebLogin::OnConnected()
extern void WebLogin_OnConnected_mBE42B60A0B735E4C2ABB34614D0D194F987BCD8B (void);
// 0x000001B1 System.Void WebLogin::OnSkip()
extern void WebLogin_OnSkip_mFE2DF18530EE3A50913FC96F508BF2C4FE67490A (void);
// 0x000001B2 System.Void WebLogin::.ctor()
extern void WebLogin__ctor_m8F6E9507124A7F34EB5A1E5FA9400CF970488950 (void);
// 0x000001B3 System.Void WebLogin/<OnConnected>d__6::MoveNext()
extern void U3COnConnectedU3Ed__6_MoveNext_m01EC6E60475A256640C35990CAD5131C04AB747D (void);
// 0x000001B4 System.Void WebLogin/<OnConnected>d__6::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnConnectedU3Ed__6_SetStateMachine_mCDC0D60876FA7F6803612569D8BCB444418EC9D9 (void);
// 0x000001B5 Mirror.ReadyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ReadyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ReadyMessage_mE2B956FB18C72DB2AAF97D93CD7A1412D4A6B0BF (void);
// 0x000001B6 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ReadyMessage(Mirror.NetworkWriter,Mirror.ReadyMessage)
extern void GeneratedNetworkCode__Write_Mirror_ReadyMessage_mDEE6F839CBFBC4FA67F9E4AB3E00479144EFE706 (void);
// 0x000001B7 Mirror.NotReadyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NotReadyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NotReadyMessage_mC6EA3B5AED8C4F9906B2F6C0312D1498D5DAA8E0 (void);
// 0x000001B8 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NotReadyMessage(Mirror.NetworkWriter,Mirror.NotReadyMessage)
extern void GeneratedNetworkCode__Write_Mirror_NotReadyMessage_m6C94EAA192195ACB7BCCB7E8A5FB2083FB0C7005 (void);
// 0x000001B9 Mirror.AddPlayerMessage Mirror.GeneratedNetworkCode::_Read_Mirror.AddPlayerMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_AddPlayerMessage_m4C2E06F848D1B94DDC640AA0EFD0CA9C36721502 (void);
// 0x000001BA System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.AddPlayerMessage(Mirror.NetworkWriter,Mirror.AddPlayerMessage)
extern void GeneratedNetworkCode__Write_Mirror_AddPlayerMessage_mAF8C12F87F2F0E51FA42578D82E57D5DE3AF01B9 (void);
// 0x000001BB Mirror.SceneMessage Mirror.GeneratedNetworkCode::_Read_Mirror.SceneMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SceneMessage_m4B63289E3849B01FD601A21B8E29E2F1ED1119B8 (void);
// 0x000001BC Mirror.SceneOperation Mirror.GeneratedNetworkCode::_Read_Mirror.SceneOperation(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SceneOperation_m7DEB9A4BFB06C5A38C93BBAC31D4813AD99FD91D (void);
// 0x000001BD System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SceneMessage(Mirror.NetworkWriter,Mirror.SceneMessage)
extern void GeneratedNetworkCode__Write_Mirror_SceneMessage_mE1645ADD480391AB601EFE6065B1CDDD1E3B00C9 (void);
// 0x000001BE System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SceneOperation(Mirror.NetworkWriter,Mirror.SceneOperation)
extern void GeneratedNetworkCode__Write_Mirror_SceneOperation_m006CA70F6FCA7971B6FEC0A3A0EDE3D7BE1678E6 (void);
// 0x000001BF Mirror.CommandMessage Mirror.GeneratedNetworkCode::_Read_Mirror.CommandMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_CommandMessage_mECC34FB337A5D4B6EB3947644B7E02B8E8B00FC0 (void);
// 0x000001C0 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.CommandMessage(Mirror.NetworkWriter,Mirror.CommandMessage)
extern void GeneratedNetworkCode__Write_Mirror_CommandMessage_mA1685BE9E0633761FE517E4171DE64FFBDC397B1 (void);
// 0x000001C1 Mirror.RpcMessage Mirror.GeneratedNetworkCode::_Read_Mirror.RpcMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_RpcMessage_m4D257364102F49B28E19CFCBF861C74C71C88CF5 (void);
// 0x000001C2 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.RpcMessage(Mirror.NetworkWriter,Mirror.RpcMessage)
extern void GeneratedNetworkCode__Write_Mirror_RpcMessage_m3CFE1651CE89811888902FBB31AB092514F79F60 (void);
// 0x000001C3 Mirror.SpawnMessage Mirror.GeneratedNetworkCode::_Read_Mirror.SpawnMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SpawnMessage_m31EC8220FAEFB8761E8DE1103A248C7BA2DE3783 (void);
// 0x000001C4 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SpawnMessage(Mirror.NetworkWriter,Mirror.SpawnMessage)
extern void GeneratedNetworkCode__Write_Mirror_SpawnMessage_mD4D895651BE0B432E3FEB3132D0C374A30F73F9A (void);
// 0x000001C5 Mirror.ChangeOwnerMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ChangeOwnerMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ChangeOwnerMessage_m04F5E3C464229DD2F2C9539ACC4D2C01C58949F0 (void);
// 0x000001C6 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ChangeOwnerMessage(Mirror.NetworkWriter,Mirror.ChangeOwnerMessage)
extern void GeneratedNetworkCode__Write_Mirror_ChangeOwnerMessage_m021C3B7ADCC887D409AFDF2DAA789DF5AC2A6CC9 (void);
// 0x000001C7 Mirror.ObjectSpawnStartedMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectSpawnStartedMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectSpawnStartedMessage_mFD106C9DC9281FFB5C54E9D7AE09DB9907368A91 (void);
// 0x000001C8 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectSpawnStartedMessage(Mirror.NetworkWriter,Mirror.ObjectSpawnStartedMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectSpawnStartedMessage_m78BBD4AC106546B7F0A36394701C996EB561A462 (void);
// 0x000001C9 Mirror.ObjectSpawnFinishedMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectSpawnFinishedMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectSpawnFinishedMessage_m0B74EA49D498677A02AE9CABCD58AF792F7AA95D (void);
// 0x000001CA System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectSpawnFinishedMessage(Mirror.NetworkWriter,Mirror.ObjectSpawnFinishedMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectSpawnFinishedMessage_m725199CA24E68A0A89E22B5BC1D0AE903671D1A1 (void);
// 0x000001CB Mirror.ObjectDestroyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectDestroyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectDestroyMessage_m48B7D38F474720778F7EF9BCFC37F0B067357B03 (void);
// 0x000001CC System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectDestroyMessage(Mirror.NetworkWriter,Mirror.ObjectDestroyMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectDestroyMessage_m843E69EF0CDF268B178138CDA80905687BAFEADA (void);
// 0x000001CD Mirror.ObjectHideMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectHideMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectHideMessage_m0042FF82016C1FBA7250DA8ECC0BE2724CDC881A (void);
// 0x000001CE System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectHideMessage(Mirror.NetworkWriter,Mirror.ObjectHideMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectHideMessage_m1A8003BDD951D23C981F6F7BAB6F9F1569D0425C (void);
// 0x000001CF Mirror.EntityStateMessage Mirror.GeneratedNetworkCode::_Read_Mirror.EntityStateMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_EntityStateMessage_m7B973026345B93B9E8B733FEA9C341F09CF1EDA1 (void);
// 0x000001D0 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.EntityStateMessage(Mirror.NetworkWriter,Mirror.EntityStateMessage)
extern void GeneratedNetworkCode__Write_Mirror_EntityStateMessage_m41E451CF8A1329EFCE9A3F4EC470B51FC47E3C5C (void);
// 0x000001D1 Mirror.NetworkPingMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NetworkPingMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NetworkPingMessage_mC2B3B4ACDDB56A809D86D120286A8173DBE447A8 (void);
// 0x000001D2 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NetworkPingMessage(Mirror.NetworkWriter,Mirror.NetworkPingMessage)
extern void GeneratedNetworkCode__Write_Mirror_NetworkPingMessage_m6D621F939E48C0B3CA2E6149D71069C358ABDF93 (void);
// 0x000001D3 Mirror.NetworkPongMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NetworkPongMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NetworkPongMessage_m9386EC63A77EFEC9FDDEC75BB13EAFDDDAFC81F8 (void);
// 0x000001D4 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NetworkPongMessage(Mirror.NetworkWriter,Mirror.NetworkPongMessage)
extern void GeneratedNetworkCode__Write_Mirror_NetworkPongMessage_mCAC90209C64184AED8759A6972F1E69D26DDC000 (void);
// 0x000001D5 System.Void Mirror.GeneratedNetworkCode::InitReadWriters()
extern void GeneratedNetworkCode_InitReadWriters_mF3F676E480CD76ED8819C76E79234EED4737891B (void);
static Il2CppMethodPointer s_methodPointers[469] = 
{
	Controlador_get_asset_m365D9F2CF8A3FA0841A4E58BB93A6DA4CFBDFD85,
	Controlador__ctor_mBB10DB3883C05ACC117451956AF5D14C25395C83,
	Controlador_Dispose_m9C995625901BD34D10A7A66864D010D7CEAACCC8,
	Controlador_get_bindingMask_m363DFB8B93372CF90DDFA7A3C62D70EAED2526E8,
	Controlador_set_bindingMask_m9D672F9B52F04A2B1070A5CF6249F9780ECFCD18,
	Controlador_get_devices_m1A2BE69A9296A7BA7BEFA66432F9240C2D8E2590,
	Controlador_set_devices_mBA1757821B8D1A9EDF3B52EEF9BA533A1354C8A5,
	Controlador_get_controlSchemes_m11ED401CEC5DFC5CEA039CCB2C47E9B68FD6D93F,
	Controlador_Contains_mF6DA46E44AE95DC0E94E2DCE9D741991BFFD9D8F,
	Controlador_GetEnumerator_mD7B151CAE642A7117B222AAE8BB024D79EE21752,
	Controlador_System_Collections_IEnumerable_GetEnumerator_m96150C508C728A00C5A59BFA9CD488F3BDB128BE,
	Controlador_Enable_mFAFB71F79057ACBB073432770E1AAF2420B6AC89,
	Controlador_Disable_m0A01F119CD8C27AB33FF0D354FAFAD29BF3834D2,
	Controlador_get_InWorld_m760E4496F0CC16F0A1E3094FBB280A4860A3C504,
	Controlador_get_UIControl_m9FD019A88B03DEA1397DED3A66758F0B23FB6CD0,
	InWorldActions__ctor_m377C2A36ACF59A5F3A9480272AD84E80967512B3,
	InWorldActions_get_MovementZ_mA018CC88008FA3D86304ADDE5ED3568FA663DC78,
	InWorldActions_get_MovementX_m8015B7B9C4511D1CD73965AED29BBBAA05AE6969,
	InWorldActions_get_Action_m7758A48C330CF9F9B24E2805665841BCC6D1289D,
	InWorldActions_get_Jump_m63BF8D7698C3F5E7263DB94F3B0CAFEB8B1FECED,
	InWorldActions_Get_mFFC96166CA3C940F1B367B4071775D67851CDBA8,
	InWorldActions_Enable_mE24CDC578A2885C807CA6410D7A461FB58905546,
	InWorldActions_Disable_mB4844B96C11D7E89BF1F6B0C2EE3663158C5C1B0,
	InWorldActions_get_enabled_m745C21AC47E3A254D3E20F3238A67B74090EF428,
	InWorldActions_op_Implicit_m0847FF83016609EDB7566D3706BA15C95393A242,
	InWorldActions_SetCallbacks_mB956BB6C2016CC4D1E9DB40B979D7FD17F51949A,
	UIControlActions__ctor_m66283505DA58059B0C8D6F1129A05F97DB6AA67E,
	UIControlActions_get_Next_mD631E5E62E29DB74B742EA6EAE37250CE2BB6215,
	UIControlActions_get_Select_mE930EB65AEEE60A2887E91152C344823AC38C2B4,
	UIControlActions_get_Back_m122706E98ADC48CC78C819577C14D06F41B8DBE2,
	UIControlActions_Get_m2F01A1629782D6AAD26471906B702482D844053A,
	UIControlActions_Enable_mFBA3EFF1773F8EA9957EB87875AC59EA43C9F315,
	UIControlActions_Disable_mC887C32C55E39858883D1B101220E4739A23343B,
	UIControlActions_get_enabled_m997E192F447045B825BD73AEA44D37FFA1B6BDA6,
	UIControlActions_op_Implicit_m29A1BE457836B2706DBF72B9E4CA174646DD4A9A,
	UIControlActions_SetCallbacks_m17D84220A7BAED2C0F02D637252AA1352C34E338,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PlayerController_OnEnable_mA03C033E796965E5CE0538A4CF83729D5857F52B,
	PlayerController_OnDisable_mE0ED7AD25577BE2048EB6D507C7503BD6462A015,
	PlayerController_Awake_m23059564DCFDD324EA9DB966473251896647CD23,
	PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794,
	PlayerController_Kicking_m1BF85836F21687F1C4AFA3EEEA8A254E06630B99,
	PlayerController_Jump_m8D4C651984BFEE8F94DE5D6D00759AC316BDC405,
	PlayerController_Knockback_m7B06D561D2B9FE751DF3C33828517F17D05050E3,
	PlayerController_MiniStun_m5FC4DD256CDAEA3E2D8CBE5A779796F0AEC057F8,
	PlayerController_KnockbackEnd_m4D6B6C1096281B974DC647BFC1E2F1C6444781B6,
	PlayerController_OnTriggerEnter_mA341783BDD02CF2476E18D5D2504CA3812E0F5B3,
	PlayerController_Die_m2A44CB9CC6A92B0963A0F5D3FA8B6A95F46EA533,
	PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB,
	PlayerController_MirrorProcessed_m4BCEEBE01DB5E3F80EC6B8DBAA9F3E5AAF917383,
	PlayerController_get_Networkvida_m9E4D447F8356BC31A5529D4D1710103FF3316F33,
	PlayerController_set_Networkvida_m2CB8F66FA6105B23174030D476475B4BE2F8D92E,
	PlayerController_get_NetworkStock_mED9E4588D3653C199B73F89B9DE2AAFD93A7E506,
	PlayerController_set_NetworkStock_mB974B4B41151AEA08695A50860AF224D4ED2B9B5,
	PlayerController_get_Networkwinner_mBD34A4E434BAEEAA663DC763DB29AC07F10874C2,
	PlayerController_set_Networkwinner_mB5BC41AC0E39B4572712DC5A37E80002CD8C774B,
	PlayerController_SerializeSyncVars_m4B46DB71001795E2F6EB4E63A986862549BD58FB,
	PlayerController_DeserializeSyncVars_m4FB4B4221435B0BA8C878BC34B8D643C5B40EB7F,
	EquipedOnPlayer__ctor_m3701FE8BA932C19DD5FAA4AEBA75564EF3B3DC07,
	FollowThis_Start_mC210824AD5040C4631EC70CF1D2C73EEA087F693,
	FollowThis__ctor_m40AC3792B1A90995E55B1A291734F26710FF9AE4,
	FollowThis_MirrorProcessed_m5B0BBF96A4D2AD86A9438F2E17EF6FCA92239E69,
	MenuComands_goto1v1_m174B8B2AD7B95FEA6AA4B7B1EBBD5AD76E32CA18,
	MenuComands_goto1v3_mFC946BA513EEADBEF9F87850E8DBBE252A145810,
	MenuComands__ctor_m4E8D2F73A0821B1F8B0F85658887AADCB8CFF9CC,
	gameManager_Start_m169616D522718461F2522FC60796972765513C0B,
	gameManager_StartGame_m3C85343E91286389476F3508EDA1BF238C182800,
	gameManager_EndGame_m5C9698913D66067FB73A5C749A4DCB92C4983D56,
	gameManager_CountDown_mD4C9C89C32375A504A19CDF76CD14CF08E839C5F,
	gameManager__ctor_mEC3390C453AE10969E79C65755A3BECB98211356,
	gameManager_MirrorProcessed_mFB1D2DDA8BB16268A3EFC56D53781F23B04490E7,
	gameManager_get_NetworkTimeToStart_m5E1ED76E6950C85CFC8995DA89B4C4A8983AD0D8,
	gameManager_set_NetworkTimeToStart_m29853508B64E19F28827F084162C013D132298D3,
	gameManager_get_NetworkGameStarted_mA3FD2A7B32D0194B5858FEFF7A378D0090F81B0B,
	gameManager_set_NetworkGameStarted_m8D94633AF42D193FD5EE7C77616789444C1DCC6E,
	gameManager_SerializeSyncVars_m323294F60D27D5D2AAD38E3D19C81B79A6BCE921,
	gameManager_DeserializeSyncVars_mE1C4EBDBB31758FFCF37424A29C4B1CC8B686ECB,
	U3CStartU3Ed__3__ctor_mB48F4FAA9459FC08D7816D6B374F86776F614EF7,
	U3CStartU3Ed__3_System_IDisposable_Dispose_m93BAFC6EC8543E89F44F2387FB1A74F02B9BD27C,
	U3CStartU3Ed__3_MoveNext_m879532514C8C170C7530691B8E8A2B61ECFB1D76,
	U3CStartU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2B347A5018C26527FE70DD242A8E9AE4FB19ACA5,
	U3CStartU3Ed__3_System_Collections_IEnumerator_Reset_mD0096DF92FFDA797AEEE4E72E2129471B234813D,
	U3CStartU3Ed__3_System_Collections_IEnumerator_get_Current_mCC8C0917AA7FAC87174852D1E5818DA0DB4416C0,
	U3CCountDownU3Ed__6__ctor_mC57A48E6DDD09DA6E118A55129926012973577CA,
	U3CCountDownU3Ed__6_System_IDisposable_Dispose_m2C308B8820BEC4319EBD920D21012B4FD9150C86,
	U3CCountDownU3Ed__6_MoveNext_m906A2329B1DED7741219EB15F941F331189C16DB,
	U3CCountDownU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8389CB86F7B02C0EEA1C0B9AB4153727D26C6DCF,
	U3CCountDownU3Ed__6_System_Collections_IEnumerator_Reset_mDCCF81548FBBEB13791A835C0C3D5A9DC02DC371,
	U3CCountDownU3Ed__6_System_Collections_IEnumerator_get_Current_m9D45A9FB927FF4F09F7A7D3CF3E0310A8444C439,
	ImportNFTTextureExample_Start_mBBAA36C72E5675E8A026135493CC4EC689D13C28,
	ImportNFTTextureExample__ctor_m329242A3D24FDCA956AF62B60DBCD51C08CC2CAB,
	Response__ctor_mEA1667092CF395117675339A78FBA3827902F38F,
	U3CStartU3Ed__1_MoveNext_m6642927BFC834022BC2708E0866FEEE26E05EFD5,
	U3CStartU3Ed__1_SetStateMachine_m72770C78EEEB2DFDB98D0015DFA3CB7FE724D5E3,
	ERC1155_BalanceOf_m6E609734004053402859FA732EB12BD113F5BBC9,
	ERC1155_BalanceOfBatch_m976C311032DE4CAB615A4BC0FC2F57669A5D04DA,
	ERC1155_URI_mA6C8E3B39B71865362F93F5641353858238A7341,
	ERC1155__ctor_m2264995D04B372757ED7B1AA5678E8CB478C46D6,
	ERC1155__cctor_m1E7EBA5D94BBF702755E5147338F6F99C17F82CC,
	U3CBalanceOfU3Ed__1_MoveNext_m827BEB7B02D7F512F3D3272FEE50EB4527603DE0,
	U3CBalanceOfU3Ed__1_SetStateMachine_m6AF4E4155ABC37A3A94695D78B0A266B91662C55,
	U3CBalanceOfBatchU3Ed__2_MoveNext_m6EED73E8199BF4B37338A03EE80469DFC5F7EA60,
	U3CBalanceOfBatchU3Ed__2_SetStateMachine_m34749E2FCC62C6089CA7D14ECC5008103D62EB8E,
	U3CURIU3Ed__3_MoveNext_m994ED7A83E78A719CB3A0D3818A730A8C04EB4F1,
	U3CURIU3Ed__3_SetStateMachine_mF1974E58996F7E03BED741B0EE6CB3416B2ACE72,
	ERC20_BalanceOf_mEC315C4821D0098701B261B0BEBC1A2C5B6653D6,
	ERC20_Name_m5535F0329E289B03EA6922420FCFA008C077831C,
	ERC20_Symbol_m1AD978D77261C364EF258C1FB93BE8071F00B026,
	ERC20_Decimals_m3F46DAA1CEDFE86A488FCF1A2BC10AE4E7FFB4FB,
	ERC20_TotalSupply_mF278916C040D2B71EC160E87AC1853B0EBEB245B,
	ERC20__ctor_m351CB7ACA6A66A62766411BFDBC2ADD36D43F365,
	ERC20__cctor_m928F4147D85D705E166A5E6349A04247BF300D4D,
	U3CBalanceOfU3Ed__1_MoveNext_m36D95A3F3A01BEFABEC55A365ED2730403E9456B,
	U3CBalanceOfU3Ed__1_SetStateMachine_m8699DFD19EEB93AED6E7AA39FDB723211AE5BCB9,
	U3CNameU3Ed__2_MoveNext_m5D9A4D337DE7457A7E606D536AE1A15B8F9A544B,
	U3CNameU3Ed__2_SetStateMachine_m49C47D68B3C839B84D14583B1166CE6CF3CCA7B7,
	U3CSymbolU3Ed__3_MoveNext_m7072C6887271018BD4296D3D6B26BE4454E33344,
	U3CSymbolU3Ed__3_SetStateMachine_mB4F3CE1D6541161B57F0FF7DC4AD7695C09BD8E1,
	U3CDecimalsU3Ed__4_MoveNext_m5F53688B23893A6A02A2AD3F3DBA38EB93F27271,
	U3CDecimalsU3Ed__4_SetStateMachine_mE9819F1B7E958708201C635D9DA17E66020F1BC0,
	U3CTotalSupplyU3Ed__5_MoveNext_m5991076501DC35DFDCC9BCEBD066050B10B7CD36,
	U3CTotalSupplyU3Ed__5_SetStateMachine_m2B452102301436127D184A33B558C339BE18146B,
	ERC721_BalanceOf_mF1062240718E051ABFE8F82957509C43F8DFD912,
	ERC721_OwnerOf_m7860416DDCB041D40D7AFB7D06D6CDC1F4841EE1,
	ERC721_OwnerOfBatch_mE72D33FAF1E47F507ECDD68E91388EF9031C385C,
	ERC721_URI_mA39E5DFAE098BA075A61C93AF8FC02BE5E29FB75,
	ERC721__ctor_m251933444988538E278DEA53D678BE3D7EEF865D,
	ERC721__cctor_m83FEEC760CAD9091959D77867512ECCB4F6620F3,
	U3CBalanceOfU3Ed__1_MoveNext_m8015D9AE43B8A99CB9D959E50EAB745432A7F19E,
	U3CBalanceOfU3Ed__1_SetStateMachine_mD5B12D1AB4C0AE02E3EFC2E56D0E9A69C4A7C0C7,
	U3COwnerOfU3Ed__2_MoveNext_m8D8C7B73A05E641AB2DFA344DE4D2FFB5A89E6E9,
	U3COwnerOfU3Ed__2_SetStateMachine_mB9A361356148E8EC7BB543DF7E2F58122EF5685C,
	U3COwnerOfBatchU3Ed__3_MoveNext_m543097386999349CA5DA05E987905F28052EAC08,
	U3COwnerOfBatchU3Ed__3_SetStateMachine_m94E80236D24084C606B00DAC402B69E5AB05B06A,
	U3CURIU3Ed__4_MoveNext_mA02D956DD4091CF9D6D2962A5836550F95E7F6D0,
	U3CURIU3Ed__4_SetStateMachine_m83FEB5116327CB0CD5BD2F04AF3E440AA2E52DEA,
	EVM_BalanceOf_m6AF5C0DA67C45C9977B4AAC2C249A36FD84D559E,
	EVM_Verify_mA8C92E16EF841C6356A849C6E29765534B0EA2CB,
	EVM_Call_m8760C42E4C727A97B7DF290D887E292460F142C4,
	EVM_MultiCall_m45EA95D0FC82EFF50087446F4ED7C4D407F0B43D,
	EVM_TxStatus_mB28FBAF141DDCD343020787DD04064C0C5993E95,
	EVM_BlockNumber_m70A74E4A04CAE5B3B50FFA11AFEE0DF6A6A7EC4A,
	EVM_Nonce_m47224BD603FC6ADE4473C6E4499791AC55A46BFF,
	EVM_CreateContractData_m0A69CC06452FBB76AC259D74C32CCC81DBC02A92,
	EVM_AllErc721_m0556274DB1637D6A25E1414A24D33CBA8AD95379,
	EVM_AllErc1155_mF8CC99C847366BCE65363096D2018749003520D6,
	EVM_GasPrice_mFFCC58B0047F8FABEE42963AEB67979F814C23A6,
	EVM_GasLimit_mC9C0175B2B65072058C8DA29E1656FD3676E14B3,
	EVM_ChainId_m5A37B376BE531801A4C4984896B593413935FEA1,
	EVM_CreateTransaction_m9DB2168641491273CE7C782CA1DC042EFD38A6A8,
	EVM_BroadcastTransaction_mC3C2718C12BABE0A104C2D9FE68099FF1DD14227,
	EVM__ctor_mC959EDC0C26DF29F2F5C7DA2AC16FAEDEE3EEDAA,
	EVM__cctor_mCF0D0C0E582E4773CD57D216964453D8C9A8B302,
	NULL,
	U3CBalanceOfU3Ed__2_MoveNext_m49FF1C68CF752F053B40E31336F0AC117A7B61E8,
	U3CBalanceOfU3Ed__2_SetStateMachine_mD8F5C1681185A6E89A41F1980CE6AABD3050AF10,
	U3CVerifyU3Ed__3_MoveNext_mD9331AE8D306DC14F8644E0888C393B1F197956A,
	U3CVerifyU3Ed__3_SetStateMachine_m477762C3E935EB66E24F0F2FF4564273024EC780,
	U3CCallU3Ed__4_MoveNext_m7103426145D023067D0E812AC94242EDC9E96C8E,
	U3CCallU3Ed__4_SetStateMachine_m2B4C2DDF94B74ACCA651A9AC53F1CA67853C059F,
	U3CMultiCallU3Ed__5_MoveNext_m59C1E5558D5BD5130431DAD6C5BE122F4259F186,
	U3CMultiCallU3Ed__5_SetStateMachine_m77A035AB7DEC5D4342A4617899A02C20AB0E8733,
	U3CTxStatusU3Ed__6_MoveNext_m21E0D491760958726DCF5290A52E17241AAACFBF,
	U3CTxStatusU3Ed__6_SetStateMachine_m6A59F5AB3D2A6077CF1BA0D1E8353FDEADC0D754,
	U3CBlockNumberU3Ed__7_MoveNext_m1CA9A036D1F74ECEBBADBEF40970047E3C65B281,
	U3CBlockNumberU3Ed__7_SetStateMachine_mBFBF55F1191EB7F3D04C204A5FFC7053EF8C8200,
	U3CNonceU3Ed__8_MoveNext_m87AEF2132C7F36BB889C4769E58EF505EE1C5677,
	U3CNonceU3Ed__8_SetStateMachine_m6C0E5BDD185012D9EE78EC7E73F9DB5448B13C1D,
	U3CCreateContractDataU3Ed__9_MoveNext_m3A513FD488EB9248FA899D092048B412F36A2A43,
	U3CCreateContractDataU3Ed__9_SetStateMachine_mC2A74C0432A6E8621E3D510878A7EBB59BA77B96,
	U3CAllErc721U3Ed__10_MoveNext_mD378DF39149CAF4372C5C6FC3D73DCC66EE1D00B,
	U3CAllErc721U3Ed__10_SetStateMachine_mC7F79A8B56C459462E982BFB2007442CB5E77327,
	U3CAllErc1155U3Ed__11_MoveNext_m2B1FA74D528BA5675715FEAE5F073E81E1F9AABF,
	U3CAllErc1155U3Ed__11_SetStateMachine_m09EBEC3D74A283E912739821BBC146F9A9306B00,
	U3CGasPriceU3Ed__12_MoveNext_mD958FF90E9BB9E56039E6FF07C8817FD2D3B0A89,
	U3CGasPriceU3Ed__12_SetStateMachine_mD8A5388148CA31A2177C494B2BF4ACAE993566C7,
	U3CGasLimitU3Ed__13_MoveNext_m32EDBB43D9022D8AFDD3094B759B3C646F3DF75F,
	U3CGasLimitU3Ed__13_SetStateMachine_mAAB510EC434AE95BC2910728023F47955EA53564,
	U3CChainIdU3Ed__14_MoveNext_m39ABDF10A0AEA824B0A24EA8AC826776E30F1F88,
	U3CChainIdU3Ed__14_SetStateMachine_mC3320A62EFF445C636E3E0CBE73AA0C5272DA035,
	U3CCreateTransactionU3Ed__15_MoveNext_mC30E9CDA81123979FBD6455141C591E2985C1098,
	U3CCreateTransactionU3Ed__15_SetStateMachine_m07800DBAA5A71F467B51548B88E44168704663BD,
	U3CBroadcastTransactionU3Ed__16_MoveNext_m2912179954FAFC5CD4F687AFBFBC6D35D159A02F,
	U3CBroadcastTransactionU3Ed__16_SetStateMachine_m6534FEE9D7F78D07C18917E9259F894B414C1E0E,
	Web3PrivateKey_SignTransaction_m1D442D71E0C032375214CD2255613FECEA77B1F7,
	Web3PrivateKey_Address_mA98258719769BAC8957BB937616C7C6ADD63A0DB,
	Web3PrivateKey_Sign_m1BDF7691997897152DDE3F2131C36F46635A26FD,
	Web3PrivateKey__ctor_m6433423824F7D6A9F79B29D05E3D421CFCF915DD,
	Web3Wallet_SendTransaction_mF2426B50D053EF9F8A0D3A6BD8C5A054015D3510,
	Web3Wallet_Sign_m9097546A29A5FBA1F0114EC95EF7097EC0FD8D8A,
	Web3Wallet_Sha3_m0E052EC4D5241845AFFA6AE980B21E332AA6D21D,
	Web3Wallet__ctor_mFDE52E181069018DC5683C3D5212D8E7B5D177F8,
	Web3Wallet__cctor_mA845968DDB3120128F657BDB3BE458EB18C618B9,
	U3CSendTransactionU3Ed__1_MoveNext_m094A7D4B17DCF83DA8D339C9F0A4AEBA99E81550,
	U3CSendTransactionU3Ed__1_SetStateMachine_m6C58AE8B37667F289A5318FDC6D296A10C72431B,
	U3CSignU3Ed__2_MoveNext_mDB3E58221A60D54BD2C7856ACB2E61A9FA18E824,
	U3CSignU3Ed__2_SetStateMachine_m80954B22F85344CA5FC5AA3D47EB652446A8DA40,
	Web3GL_SendContractJs_m9F91ED43D46F554F2DCA7B4ED0E43417A2438B24,
	Web3GL_SendContractResponse_m07C0E7090A2E54F34898D8C9B34DE18D9AB8B61B,
	Web3GL_SetContractResponse_m581DED8C3AB4B130CE6A27160134E42064D17897,
	Web3GL_SendTransactionJs_m4643694BD4CEF40C106AFFD06802F1F7623B6284,
	Web3GL_SendTransactionResponse_m29FDB8F571CF4646FA2023A88B94E794A9D10A15,
	Web3GL_SetTransactionResponse_m1DEB989EFBBCDDB46687A63F24E47AB2B0B15B9C,
	Web3GL_SignMessage_mC477404D486FA06FEB49FD1DF69D27694161041C,
	Web3GL_SignMessageResponse_mFC3A305C5D857D33399AA53D9D3F8BE7B0753CA5,
	Web3GL_SetSignMessageResponse_mBB7DBE43E8B7517DD36D3477025F338D72D9FDCF,
	Web3GL_GetNetwork_mC116213F6AD62593B3E6D9F16EFC87A7E336F9A1,
	Web3GL_SendContract_m6675BF158272EAC923D284D5CC10C7F09558E173,
	Web3GL_SendTransaction_mD40DBCA64188F5159A42BE864CA19D5B61C7F5D9,
	Web3GL_Sign_mFEAE31488340360AB008F8E3FAE799D1E10871C7,
	Web3GL_Network_m8390042864BF8E5E886B04176B37F6C10C538150,
	Web3GL__ctor_m14977243FEA2B06F40615CE85C7522F34B2C6951,
	U3CSendContractU3Ed__10_MoveNext_mAE0FDF450FEBA139C99D52CECE5D43499DB90708,
	U3CSendContractU3Ed__10_SetStateMachine_mDB924150509FEA19525D41521D4EFFBB8CA8C99C,
	U3CSendTransactionU3Ed__11_MoveNext_m053E36DD1D85C5856A314131285B48D6F5C070C5,
	U3CSendTransactionU3Ed__11_SetStateMachine_m324E10096F6699BD39FB4AB1847403016F77A827,
	U3CSignU3Ed__12_MoveNext_m74FB11FDCCAAFD0E2BFF954199891117554D1890,
	U3CSignU3Ed__12_SetStateMachine_mBAA214792AF8C334AC1EDA56E1C0A9B805A5C267,
	ERC1155BalanceOfBatchExample_Start_mB2D18D5D03FFF898BCCD553ED52C25472DDA46A2,
	ERC1155BalanceOfBatchExample__ctor_m0236A171447DA49A7761764F9DCE0D7FC4C3B08F,
	U3CStartU3Ed__0_MoveNext_mE94D2F8B51DBFC5B070D07ABF611C7C7D483D016,
	U3CStartU3Ed__0_SetStateMachine_m76E05730082FD3FB893CFE4ED257BFD0E1146F2A,
	ERC1155BalanceOfExample_Start_mF1D7FC086FAF079516A7463211759E19659E3E19,
	ERC1155BalanceOfExample__ctor_m3C08EAD0BA5D101B144908139F5BED8912BF2CE1,
	U3CStartU3Ed__0_MoveNext_mBE908F669CAC2ABDAC7872701F7C3BED2B4D66C8,
	U3CStartU3Ed__0_SetStateMachine_mACD6D05861661B7259297843E6FE635E542144F8,
	ERC1155URIExample_Start_m47316F5E9063BA95AB5038A40203C130AE315332,
	ERC1155URIExample__ctor_m1EB728AE1282B0EE260BF3CC994D0745F600E2AE,
	U3CStartU3Ed__0_MoveNext_mD7E6C3EB57F3683AC190990093F1DF60C5996BDF,
	U3CStartU3Ed__0_SetStateMachine_m32F010E483167AED04DE742EB8940EDE54A25941,
	ERC20BalanceOfExample_Start_mFF41E5445321315891D6684596EFF7494EB4CCEA,
	ERC20BalanceOfExample__ctor_m470F363A7E0055B85DD346818056BA00FC77CA0F,
	U3CStartU3Ed__0_MoveNext_m18FDD5025FFAD18D6D5207671EF4439FFBF9117B,
	U3CStartU3Ed__0_SetStateMachine_m8B9F75B4229353D82C1C4D7F5D9B3D0608653F2A,
	ERC20DecimalsExample_Start_mF8A5B405AC67AC0E354341E76FD1674DC451DE85,
	ERC20DecimalsExample__ctor_mD92FAAAD3C3A55AC5B8DFD0C5924F8C93B80345E,
	U3CStartU3Ed__0_MoveNext_mB8317507C6D1634221D136B56400405740C35941,
	U3CStartU3Ed__0_SetStateMachine_m0E9F22BDB96721E7BD11F85CE5F248ABFF1D4399,
	ERC20NameExample_Start_m5E0090E513011383498C5799D006C408A0549FFD,
	ERC20NameExample__ctor_m2277084D20C11E505CDD500262B45C935905433F,
	U3CStartU3Ed__0_MoveNext_m1009F1D54AA9418573E986DC091E8F6CEA2A152E,
	U3CStartU3Ed__0_SetStateMachine_m5C52A6DA998FC196467CC1A8D7DA3F1B2680784A,
	ERC20SymbolExample_Start_m44F3237CCD3B3C48CF8216DAADC25FB4C817870A,
	ERC20SymbolExample__ctor_mD300D6C431C733379923241434ED70E78B7998FF,
	U3CStartU3Ed__0_MoveNext_m7C288E439AA5604812418AC5E663FB9F2463FEF5,
	U3CStartU3Ed__0_SetStateMachine_m8ECA709670FDBBB8CF6CE3BE68AC77CEE55FEF6B,
	ERC20TotalSupplyExample_Start_mAD965D99BF2EEA5EF43F8B15CC691404AD553AE2,
	ERC20TotalSupplyExample__ctor_m0BBB8385A86BD711BB0BECFF421154D3148BD7AC,
	U3CStartU3Ed__0_MoveNext_m9684BD13AB1E3C45FEF01FB8189CB1CC84CB872B,
	U3CStartU3Ed__0_SetStateMachine_m90AE0209DA8EBB3AE0D980935EAEC3783FB842AD,
	ERC721BalanceOfExample_Start_m5C514F94087DD8AFF72E9D283FF33BD6175B2EF2,
	ERC721BalanceOfExample__ctor_mE3CDB9E379F5A95A8C05061EC3D3397B561F4192,
	U3CStartU3Ed__0_MoveNext_m7FC8B7D9E9B28572F8FE2AB87C3DF77F8D3DC143,
	U3CStartU3Ed__0_SetStateMachine_mE60DC6B0816FFFA80349E8D40C3CFC426F8001B7,
	ERC721OwnerOfBatchExample_Start_m8CF7A69B7BFA086DD735B18E5A2395F422414DA6,
	ERC721OwnerOfBatchExample__ctor_m0584E4443C2F44570F67FC5E7349A8B65AEED216,
	U3CStartU3Ed__0_MoveNext_m2251CA0D2B663CC81ACCA1DF52CF03390A34009A,
	U3CStartU3Ed__0_SetStateMachine_mE6B01D55F9D6F1B641203AD045DA10D399ABC770,
	ERC721OwnerOfExample_Start_m9D7F5468A89DEC19DEF62AD36B527F91BF6792C1,
	ERC721OwnerOfExample__ctor_mBEB0A6B85F70347883731AB42B10D30EB9FC44ED,
	U3CStartU3Ed__0_MoveNext_m4ED7CA82E64E03491B0A4A643ECBE32AD09ADED1,
	U3CStartU3Ed__0_SetStateMachine_m0ECA16AF5C9507944D8F09DD555E373FC01AF5F7,
	ERC721URIExample_Start_m0EACF752E96AE14675BB65815ACEBA99F6494291,
	ERC721URIExample__ctor_mC50A975F37511859DA883D76394FC3D18B320C65,
	U3CStartU3Ed__0_MoveNext_m51BDCBDA7FC0D710D01081E177173067F022F3EE,
	U3CStartU3Ed__0_SetStateMachine_mB281CC760C5BC7236B4D097976C68D5CF60EF754,
	AllErc1155Example_Start_m98450BC383F06E18DD3E997CA40678AA77B3ED45,
	AllErc1155Example__ctor_m792ED55745E0341DF0F9C12A8D757B18D452FDD4,
	NFTs_get_contract_m132FBACD650BBB034871A7BEAA4A8A5E69890D65,
	NFTs_set_contract_m5806E56C6DAE7193AB5CD81D9F474D07A35B851E,
	NFTs_get_tokenId_m82B87799B5788DA9B1406347C6E7BE76CC00907F,
	NFTs_set_tokenId_mEED2527C52CB89056919196F37BB93FC91703C8B,
	NFTs_get_uri_m24476236E60F7F31C888F8E68D053AF7D9B88C0A,
	NFTs_set_uri_mA7C27EE09FE391D53EA831A4ECE9B915DAE66CBB,
	NFTs_get_balance_mCBFB0194B7ABA4A6B584E176B16282A5F2109424,
	NFTs_set_balance_m4045F162A6C0E6616F9AEE022F3204229347A37C,
	NFTs__ctor_m97BE3AED0C12AB78FCCB5F44313F6D8D673787CA,
	U3CStartU3Ed__1_MoveNext_m56F7B4255F3860898DFF3B4A9A4A42827AD9C42F,
	U3CStartU3Ed__1_SetStateMachine_mF4BABB9B81366714D470D287C46918A5C6D7EB6A,
	AllErc721Example_Start_m1173828869CE0CA1368EC96797AB780B26007185,
	AllErc721Example__ctor_m7ADB3B7A840C832B488C1EFFA54D0F259918BA7D,
	NFTs_get_contract_mEF0AF77D33D8D1793D68D7BD429C144F2DBECC0D,
	NFTs_set_contract_mD64E897A626ADCEE21998CE15BFF69AD5F762634,
	NFTs_get_tokenId_m9EFD2AC01B73E801C3BC8B45FB7268099F8979EF,
	NFTs_set_tokenId_m24AE780605FE57573BC4ED1FF58B1890B9C98686,
	NFTs_get_uri_mC235611E28E2B8ACC01BA87194FC197DB55AD436,
	NFTs_set_uri_mAD2F72E336F6B548906B4D9A68BB7CF28FDA325B,
	NFTs_get_balance_m9815F7685CFBDFB187A2EBC45307D1353622B4FA,
	NFTs_set_balance_m343CFEB6DFF4A498F2127634C5EA9041E505AF37,
	NFTs__ctor_m96B5DA690CC6A9867780A5210BE0D6897F744172,
	U3CStartU3Ed__1_MoveNext_mCF92100CA8DD409E5D8F73CBA00D4C85FEC94D1B,
	U3CStartU3Ed__1_SetStateMachine_mCD7FDC90AA399D1DB295620538CA230DEAB95A1F,
	BlockNumberExample_Start_mAE8F8CFCAE848DAA2C9FC0E971741520E7BA3EFD,
	BlockNumberExample__ctor_m192B9D1EE3FE48031D94F858FEB5365D4976E7C7,
	U3CStartU3Ed__0_MoveNext_m1C1A1991718A66B2A3ED5D056A9B82B28530ADA8,
	U3CStartU3Ed__0_SetStateMachine_m1B759B91C18F303ECF8AA29965839045F5FB49C9,
	CreateContractDataExample_Start_m975FDA0E1478E7283C9B3F95C5D4A825CFCC500E,
	CreateContractDataExample__ctor_mCB4953F16A06B18F07E5DD94285EB30FABE5C8DD,
	U3CStartU3Ed__0_MoveNext_m4BC1DF55905C7CAE4ECD9A68314A3A327C5BC727,
	U3CStartU3Ed__0_SetStateMachine_m90FCB1CDB53828A20360A302E41321FA2915A8B7,
	CustomCallExample_Mint_mA9AF3C5B8DC09D051DB0DB7EE46FC8F3F1ECFB34,
	CustomCallExample_Awake_m60E4F5F3D9586022C4A2A86A6A1C6921AD77991F,
	CustomCallExample_ChangeName_m11A572AD5E78FF9DBC799B37BB3C7DFE0EE95A34,
	CustomCallExample_ReLoad_m9ED3FF4E3B3388BC59DD339CF4E2ADE587DB03FF,
	CustomCallExample_GetNft_mB0ABD70FEE9F630BB81163A45BE6C18B72674981,
	CustomCallExample_Burn_mF950FC3007B36EB9AF4C968F7FFCDDF6B31B09C8,
	CustomCallExample__ctor_m9377564E09D348C8B80520854B781DB031019208,
	NFTs_get_contract_mF6232E648620A06789385C55D8180A8CF7464944,
	NFTs_set_contract_m4F35AAB24C4FD78BF470D1F797C126463E38FB60,
	NFTs_get_tokenId_mDF28A5F14194E236C2FF36FF1822BB37FA5F1774,
	NFTs_set_tokenId_m240F331C0FD015FEFCD9B9C8AAF3ABBFD9D38D71,
	NFTs_get_uri_mFFE0E2FAC27B394D476A8ED8A87D3D75F58EA390,
	NFTs_set_uri_mF53EB20E483FA1467C8BCC0764A02EC97B64FDED,
	NFTs_get_balance_m53BCCA62B6C840C1D66DDA2AE8E9D8BB6AFAD2AC,
	NFTs_set_balance_m32CAB14E10B0A94AA9A0093C4133AC20E4775BAA,
	NFTs__ctor_mB91B27B80909E3D31BF71B5971902137A417FD76,
	U3CMintU3Ed__6_MoveNext_mAA93E837E07DB6EAFAEDEFE955B062A1F5430EB6,
	U3CMintU3Ed__6_SetStateMachine_mE34E1011DA6AE67062B3638D5CA080CB82EE7586,
	U3CChangeNameU3Ed__8_MoveNext_m8BA79E020BBD8A22B2B2ADE01C887C8673214CA0,
	U3CChangeNameU3Ed__8_SetStateMachine_m2F1CDC60D482E2AFCE6FBEA95ECA5648BBE31409,
	U3CReLoadU3Ed__9_MoveNext_m27927DFB1EEB05FC2A70ED64B9A665BC68F5BF4B,
	U3CReLoadU3Ed__9_SetStateMachine_m89009A42D7FAC4B90BC171A10997A50E3AFDE867,
	U3CGetNftU3Ed__10_MoveNext_mBDE73D4F04CD780307D74E27B7121D5C5C202DD5,
	U3CGetNftU3Ed__10_SetStateMachine_m110A5F8CA3599AE1E562A01BAE0B1194F8801D48,
	U3CBurnU3Ed__11_MoveNext_m933C2B659AB810835B3B43A240113ABEB2CD4977,
	U3CBurnU3Ed__11_SetStateMachine_m1F8C91D9B818F4B52BF56BBC71398F27DFFF3417,
	CustomRPCExample_Start_mA3CCBB955CEA57395864A8F32683C8763BEFDC33,
	CustomRPCExample__ctor_m367121EA923134B9133C4F152AFD5C96D9B22D74,
	U3CStartU3Ed__0_MoveNext_m9D1B1A888D55782764A3D7818C3741A2C53ADFD0,
	U3CStartU3Ed__0_SetStateMachine_mA01B28EE49B96A3BB28545E7375E2E4823E4F158,
	EthBalanceOfExample_Start_m38948E53166841C031BB586EE77C4CC412FAA71F,
	EthBalanceOfExample__ctor_mEF83438420A6B83C6D919D21E60178FCB485E0EE,
	U3CStartU3Ed__0_MoveNext_m87DC3F4DB835025DF80DEA749A2CD4BFC153321A,
	U3CStartU3Ed__0_SetStateMachine_m8763265A5E48878995143FE2211C1135DD40A431,
	NonceExample_Start_mE7130950BE1D260F4460F2F6DD25E5ADF4209C0D,
	NonceExample__ctor_m32153CBCD408BC9F3CAE45DD51309D62857D5971,
	U3CStartU3Ed__0_MoveNext_m46792A67B48E13440730A2271EFD9D3F37DCE540,
	U3CStartU3Ed__0_SetStateMachine_mE36D9D616EF594199062DEBE0273AC423B4D8347,
	TxStatus_Start_mFA223A713B485F7F2E598A7EB56054D55BB28DB9,
	TxStatus__ctor_m03BD513E1882C5FB6BFF2271B82D657E965715D0,
	U3CStartU3Ed__0_MoveNext_m0BD1687277E3B971A8A377BD0627EF8FD774A481,
	U3CStartU3Ed__0_SetStateMachine_mF482F4B6CA65515CF4469D1B6BCD7C43F86BB665,
	VerifyExample_Start_m00DC30EC005ED55D223D40563C7E5BFA50D78296,
	VerifyExample__ctor_m2B5C4222C3E6D88436ABE99CAAF80C23971A51BC,
	U3CStartU3Ed__0_MoveNext_m4C83C0CAB5D8222B58E90AD7B4B1D556C6D19ED1,
	U3CStartU3Ed__0_SetStateMachine_m8B1480939675F9433880643CD4EA0C534881209D,
	Web3PrivateKeyAddressExample_Start_mB8AA36F9637A8476D25A029946190EF91966E481,
	Web3PrivateKeyAddressExample__ctor_m708C97875851DBF493A581C399C99B1BF3921E3F,
	Web3PrivateKeySend1155Example_OnSend1155_m244501F1769E054C6D39622BD3439BFBA32416CD,
	Web3PrivateKeySend1155Example__ctor_m2301B78E83C1AE7D4C1D856910722B006CFEC83E,
	U3COnSend1155U3Ed__0_MoveNext_m5CC620F10F35AC4D9113A097C0F0C6999696C126,
	U3COnSend1155U3Ed__0_SetStateMachine_m6044558A9818E8F112CEFE3BF401EDE447831294,
	Web3PrivateKeySend20Example_OnSend20_m25112C041C89E4656FB9F97A75A0F4D65E827540,
	Web3PrivateKeySend20Example__ctor_m3223A050C097F9CF2C45ECA72F3A42EEC0372F77,
	U3COnSend20U3Ed__0_MoveNext_mE8FEC374CB159E1AC59FB9226035C06D794D6855,
	U3COnSend20U3Ed__0_SetStateMachine_m2459E52710C5D0C1641F042AA1246E79DB87C834,
	Web3PrivateKeySend721Example_OnSend721_m7DF532A7644D27B2CBBC1EDDF024613064342487,
	Web3PrivateKeySend721Example__ctor_mA771D3B9F5BE3ADA021494896231710B7F466E49,
	U3COnSend721U3Ed__0_MoveNext_m92930AB8A01B81E9C029494FC9E7885D52300E41,
	U3COnSend721U3Ed__0_SetStateMachine_m2CDC6C3A888109DBFB4B350C5D3969E5E9805EF3,
	Web3PrivateKeySendTransactionExample_OnSendTransaction_m096857E458B9BFEB768B0BA0088D8C396C14FD63,
	Web3PrivateKeySendTransactionExample__ctor_mE80624EACC2E1183E5A3A1E203FF64D6AB3EC02B,
	U3COnSendTransactionU3Ed__0_MoveNext_m83BE0A4E0DE4D5065D305BEF0E6FD2020BDDA22E,
	U3COnSendTransactionU3Ed__0_SetStateMachine_m75CF9DD43E6B9372BE208D6144B6D3D389EDD257,
	Web3PrivateKeySignMessageExample_Start_m781E7927FF493B132E114D69245F899F9FB91DA1,
	Web3PrivateKeySignMessageExample__ctor_m481CC8024D641087A48BD47F36BDD1B45DE0F5C7,
	Web3WalletLogOut_OnLogOut_mB3E43BB3729C2DBA1781F3B9FB86B5381DD89422,
	Web3WalletLogOut__ctor_mE293D3C9EA99537F130D3E97E0E8ED8381B816C5,
	Web3WalletSendTransactionExample_OnSendTransaction_m4FA8621476FD0880D88D0F52337D40760ED02D06,
	Web3WalletSendTransactionExample__ctor_mA23EB67764399D5BBBEC73C13901C4B64BABF0F6,
	U3COnSendTransactionU3Ed__0_MoveNext_mCE4513700F2438755640D4CF601F7B4D7E7A7321,
	U3COnSendTransactionU3Ed__0_SetStateMachine_mA1354644175F2D58C90730B1FDB4869FA7BDFC48,
	Web3WalletSha3Example_Start_mFEF54A29017F49078D49A977FB22CA1BEFC18741,
	Web3WalletSha3Example__ctor_m2ABD3EDDBFF4FB9414E4C50D7BB047377109109D,
	Web3WalletSignMessageExample_OnSignMessage_m6A3BA73D6F59E338F04E01A2E3912198F487BBDA,
	Web3WalletSignMessageExample__ctor_m2689DCC77E5FBDD5A3609C89731909CA2D1F79C0,
	U3COnSignMessageU3Ed__0_MoveNext_m910B0F834534ADBBA4DEA91CF2B62C5EBA533EBE,
	U3COnSignMessageU3Ed__0_SetStateMachine_m4338BE5F3F9B7C77682650825239AC6C7138A6B1,
	Web3WalletTransfer1155Example_OnTransfer1155_m6CC73312B266A7E1B7974E6CB782503B376758E3,
	Web3WalletTransfer1155Example__ctor_m172D22E1D8D9C86AC1772C79274A35C96782BF83,
	U3COnTransfer1155U3Ed__0_MoveNext_mA90B1AC56399F741DB2348B2941FDBF72E9AC597,
	U3COnTransfer1155U3Ed__0_SetStateMachine_mE5364F3E8E033AA906ACE7657884281076A015C5,
	Web3WalletTransfer20Example_OnTransfer20_mD19680408F257116D802637BCAFA6AE2D4C049D1,
	Web3WalletTransfer20Example__ctor_m97D5F927B2BF9758CD75C1675BAA53EACA3C2AF8,
	U3COnTransfer20U3Ed__0_MoveNext_mAC06EED0D1221620A17D9BE470EA8130592D27BE,
	U3COnTransfer20U3Ed__0_SetStateMachine_mF8F73F2742E7641DF16BA8DAD19AB3A74142E61C,
	Web3WalletTransfer721Example_OnTransfer721_mF62FEAA1F0ED77EA1D20ABDDA5F08A19908BD314,
	Web3WalletTransfer721Example__ctor_mB2368760CF92616211A427427C6BC6B0BB973E98,
	U3COnTransfer721U3Ed__0_MoveNext_m8C1BB1B0A36207ABDD5BB95E54BEA6EE2265F07F,
	U3COnTransfer721U3Ed__0_SetStateMachine_mF885565CD1B11CCD1E3057612D11154B7629129D,
	WebGLSendContractExample_OnSendContract_m1A4B193810A8347EB809FAA076CE10CE962713C2,
	WebGLSendContractExample__ctor_m13E3954741DC488ACDF798E9FC9AC6B98E89C125,
	U3COnSendContractU3Ed__0_MoveNext_m87D0E94C972176E1F9AEA97BCEFD7CBBF79BB814,
	U3COnSendContractU3Ed__0_SetStateMachine_m7A097F8F1BB131B8A785D2F22D1D28D7527646EA,
	WebGLSendTransactionExample_OnSendTransaction_mE9BD097CBD52F83717726BFBB8B55265A7BEA7F8,
	WebGLSendTransactionExample__ctor_mB33E6B0437A5D91B82624F4CB38AFDE7406424A5,
	U3COnSendTransactionU3Ed__0_MoveNext_mC7CE9060F43BA816093410666933E4EBB204561B,
	U3COnSendTransactionU3Ed__0_SetStateMachine_mC12443C49C60F7DCAA5CDA5E716B32E16BD74723,
	WebGLSignMessageExample_OnSignMessage_mEDA9C904A9601BA4E7A4EBD354297A6DB24A5279,
	WebGLSignMessageExample__ctor_m7D4E73C2DF52C0647FE4BC055E1BB411E7AD7D1C,
	U3COnSignMessageU3Ed__0_MoveNext_m3057F7982AC1CB0EDD2664D8ED6B7E5AC4ADE2A1,
	U3COnSignMessageU3Ed__0_SetStateMachine_m3F0DC2AEF8F20669EFA57A92C67E1F9A5F4B6802,
	WebGLSignOut_OnSignOut_mF873859FF1D5698D45BC012378D6091E809D54A1,
	WebGLSignOut__ctor_m41F9C4F86BE31A82BC8C895BB1E1E1D6AD3EE428,
	WebGLTransfer1155_SafeTransferFrom_m60027B3FC9E5908D1DAC6D38F279E525F9E69282,
	WebGLTransfer1155__ctor_mAD1A671E8D5A6AEDC9416FC5BB6E7B1236722CAD,
	U3CSafeTransferFromU3Ed__5_MoveNext_m64353C3BCC55943924533B82580063995694E676,
	U3CSafeTransferFromU3Ed__5_SetStateMachine_m357DFCF039FEA8B6C3395C25DFC5753549AD9156,
	WebGLTransfer20_Transfer_mC189BF2DC9623CEED6D1285FFF55B985A9F95FB8,
	WebGLTransfer20__ctor_m2A916DD8676ECCDBD740AD6D27204D8F8987DDC3,
	U3CTransferU3Ed__4_MoveNext_m5655C8D900D8FDBA8EF1BB4A1111791D22525BFD,
	U3CTransferU3Ed__4_SetStateMachine_mA32D0A2CF0FC0E65AD5EA5081D9B5860C23936DE,
	WebGLTransfer721_SafeTransferFrom_m48A1C85515A84E75A7ADB433BD7D6011D307848B,
	WebGLTransfer721__ctor_m1A7A94EFE2244A7AB1F57EF77E5323EC7BEC1304,
	U3CSafeTransferFromU3Ed__4_MoveNext_m967807132732773C90E46AD493832B9237EA807B,
	U3CSafeTransferFromU3Ed__4_SetStateMachine_m72420EB4DE07C850B47609F4910E992BFAA0CC10,
	WalletLogin_Start_mEE859A6D79844237B6727D49BF0ABED6E5DABE08,
	WalletLogin_OnLogin_mADEC61B177A2617C428988B3EC727ED17679DE7D,
	WalletLogin__ctor_m6F4AF712F9EF7D444679E887D6FBB2A5BD43C4A3,
	U3COnLoginU3Ed__2_MoveNext_mC494AC5CF4F7CB0F482C28FCBC80A560EB1EDAFB,
	U3COnLoginU3Ed__2_SetStateMachine_mB0FF2BA206BB13E75568802ED92D9C2A4609EBC1,
	WebLogin_Web3Connect_m328539A6C6D4351E03FF94C6D7FCAA00F65FFFAD,
	WebLogin_ConnectAccount_mC1E264919C9BEB95A48D4B2F73CF6AE68BE907E7,
	WebLogin_SetConnectAccount_mFB37E0B21EC829A9BD04821AFD16E75F3522353A,
	WebLogin_OnLogin_m1106D41C77BEC6727F80A708A1EBE5EC42D5F932,
	WebLogin_OnConnected_mBE42B60A0B735E4C2ABB34614D0D194F987BCD8B,
	WebLogin_OnSkip_mFE2DF18530EE3A50913FC96F508BF2C4FE67490A,
	WebLogin__ctor_m8F6E9507124A7F34EB5A1E5FA9400CF970488950,
	U3COnConnectedU3Ed__6_MoveNext_m01EC6E60475A256640C35990CAD5131C04AB747D,
	U3COnConnectedU3Ed__6_SetStateMachine_mCDC0D60876FA7F6803612569D8BCB444418EC9D9,
	GeneratedNetworkCode__Read_Mirror_ReadyMessage_mE2B956FB18C72DB2AAF97D93CD7A1412D4A6B0BF,
	GeneratedNetworkCode__Write_Mirror_ReadyMessage_mDEE6F839CBFBC4FA67F9E4AB3E00479144EFE706,
	GeneratedNetworkCode__Read_Mirror_NotReadyMessage_mC6EA3B5AED8C4F9906B2F6C0312D1498D5DAA8E0,
	GeneratedNetworkCode__Write_Mirror_NotReadyMessage_m6C94EAA192195ACB7BCCB7E8A5FB2083FB0C7005,
	GeneratedNetworkCode__Read_Mirror_AddPlayerMessage_m4C2E06F848D1B94DDC640AA0EFD0CA9C36721502,
	GeneratedNetworkCode__Write_Mirror_AddPlayerMessage_mAF8C12F87F2F0E51FA42578D82E57D5DE3AF01B9,
	GeneratedNetworkCode__Read_Mirror_SceneMessage_m4B63289E3849B01FD601A21B8E29E2F1ED1119B8,
	GeneratedNetworkCode__Read_Mirror_SceneOperation_m7DEB9A4BFB06C5A38C93BBAC31D4813AD99FD91D,
	GeneratedNetworkCode__Write_Mirror_SceneMessage_mE1645ADD480391AB601EFE6065B1CDDD1E3B00C9,
	GeneratedNetworkCode__Write_Mirror_SceneOperation_m006CA70F6FCA7971B6FEC0A3A0EDE3D7BE1678E6,
	GeneratedNetworkCode__Read_Mirror_CommandMessage_mECC34FB337A5D4B6EB3947644B7E02B8E8B00FC0,
	GeneratedNetworkCode__Write_Mirror_CommandMessage_mA1685BE9E0633761FE517E4171DE64FFBDC397B1,
	GeneratedNetworkCode__Read_Mirror_RpcMessage_m4D257364102F49B28E19CFCBF861C74C71C88CF5,
	GeneratedNetworkCode__Write_Mirror_RpcMessage_m3CFE1651CE89811888902FBB31AB092514F79F60,
	GeneratedNetworkCode__Read_Mirror_SpawnMessage_m31EC8220FAEFB8761E8DE1103A248C7BA2DE3783,
	GeneratedNetworkCode__Write_Mirror_SpawnMessage_mD4D895651BE0B432E3FEB3132D0C374A30F73F9A,
	GeneratedNetworkCode__Read_Mirror_ChangeOwnerMessage_m04F5E3C464229DD2F2C9539ACC4D2C01C58949F0,
	GeneratedNetworkCode__Write_Mirror_ChangeOwnerMessage_m021C3B7ADCC887D409AFDF2DAA789DF5AC2A6CC9,
	GeneratedNetworkCode__Read_Mirror_ObjectSpawnStartedMessage_mFD106C9DC9281FFB5C54E9D7AE09DB9907368A91,
	GeneratedNetworkCode__Write_Mirror_ObjectSpawnStartedMessage_m78BBD4AC106546B7F0A36394701C996EB561A462,
	GeneratedNetworkCode__Read_Mirror_ObjectSpawnFinishedMessage_m0B74EA49D498677A02AE9CABCD58AF792F7AA95D,
	GeneratedNetworkCode__Write_Mirror_ObjectSpawnFinishedMessage_m725199CA24E68A0A89E22B5BC1D0AE903671D1A1,
	GeneratedNetworkCode__Read_Mirror_ObjectDestroyMessage_m48B7D38F474720778F7EF9BCFC37F0B067357B03,
	GeneratedNetworkCode__Write_Mirror_ObjectDestroyMessage_m843E69EF0CDF268B178138CDA80905687BAFEADA,
	GeneratedNetworkCode__Read_Mirror_ObjectHideMessage_m0042FF82016C1FBA7250DA8ECC0BE2724CDC881A,
	GeneratedNetworkCode__Write_Mirror_ObjectHideMessage_m1A8003BDD951D23C981F6F7BAB6F9F1569D0425C,
	GeneratedNetworkCode__Read_Mirror_EntityStateMessage_m7B973026345B93B9E8B733FEA9C341F09CF1EDA1,
	GeneratedNetworkCode__Write_Mirror_EntityStateMessage_m41E451CF8A1329EFCE9A3F4EC470B51FC47E3C5C,
	GeneratedNetworkCode__Read_Mirror_NetworkPingMessage_mC2B3B4ACDDB56A809D86D120286A8173DBE447A8,
	GeneratedNetworkCode__Write_Mirror_NetworkPingMessage_m6D621F939E48C0B3CA2E6149D71069C358ABDF93,
	GeneratedNetworkCode__Read_Mirror_NetworkPongMessage_m9386EC63A77EFEC9FDDEC75BB13EAFDDDAFC81F8,
	GeneratedNetworkCode__Write_Mirror_NetworkPongMessage_mCAC90209C64184AED8759A6972F1E69D26DDC000,
	GeneratedNetworkCode_InitReadWriters_mF3F676E480CD76ED8819C76E79234EED4737891B,
};
extern void InWorldActions__ctor_m377C2A36ACF59A5F3A9480272AD84E80967512B3_AdjustorThunk (void);
extern void InWorldActions_get_MovementZ_mA018CC88008FA3D86304ADDE5ED3568FA663DC78_AdjustorThunk (void);
extern void InWorldActions_get_MovementX_m8015B7B9C4511D1CD73965AED29BBBAA05AE6969_AdjustorThunk (void);
extern void InWorldActions_get_Action_m7758A48C330CF9F9B24E2805665841BCC6D1289D_AdjustorThunk (void);
extern void InWorldActions_get_Jump_m63BF8D7698C3F5E7263DB94F3B0CAFEB8B1FECED_AdjustorThunk (void);
extern void InWorldActions_Get_mFFC96166CA3C940F1B367B4071775D67851CDBA8_AdjustorThunk (void);
extern void InWorldActions_Enable_mE24CDC578A2885C807CA6410D7A461FB58905546_AdjustorThunk (void);
extern void InWorldActions_Disable_mB4844B96C11D7E89BF1F6B0C2EE3663158C5C1B0_AdjustorThunk (void);
extern void InWorldActions_get_enabled_m745C21AC47E3A254D3E20F3238A67B74090EF428_AdjustorThunk (void);
extern void InWorldActions_SetCallbacks_mB956BB6C2016CC4D1E9DB40B979D7FD17F51949A_AdjustorThunk (void);
extern void UIControlActions__ctor_m66283505DA58059B0C8D6F1129A05F97DB6AA67E_AdjustorThunk (void);
extern void UIControlActions_get_Next_mD631E5E62E29DB74B742EA6EAE37250CE2BB6215_AdjustorThunk (void);
extern void UIControlActions_get_Select_mE930EB65AEEE60A2887E91152C344823AC38C2B4_AdjustorThunk (void);
extern void UIControlActions_get_Back_m122706E98ADC48CC78C819577C14D06F41B8DBE2_AdjustorThunk (void);
extern void UIControlActions_Get_m2F01A1629782D6AAD26471906B702482D844053A_AdjustorThunk (void);
extern void UIControlActions_Enable_mFBA3EFF1773F8EA9957EB87875AC59EA43C9F315_AdjustorThunk (void);
extern void UIControlActions_Disable_mC887C32C55E39858883D1B101220E4739A23343B_AdjustorThunk (void);
extern void UIControlActions_get_enabled_m997E192F447045B825BD73AEA44D37FFA1B6BDA6_AdjustorThunk (void);
extern void UIControlActions_SetCallbacks_m17D84220A7BAED2C0F02D637252AA1352C34E338_AdjustorThunk (void);
extern void U3CStartU3Ed__1_MoveNext_m6642927BFC834022BC2708E0866FEEE26E05EFD5_AdjustorThunk (void);
extern void U3CStartU3Ed__1_SetStateMachine_m72770C78EEEB2DFDB98D0015DFA3CB7FE724D5E3_AdjustorThunk (void);
extern void U3CBalanceOfU3Ed__1_MoveNext_m827BEB7B02D7F512F3D3272FEE50EB4527603DE0_AdjustorThunk (void);
extern void U3CBalanceOfU3Ed__1_SetStateMachine_m6AF4E4155ABC37A3A94695D78B0A266B91662C55_AdjustorThunk (void);
extern void U3CBalanceOfBatchU3Ed__2_MoveNext_m6EED73E8199BF4B37338A03EE80469DFC5F7EA60_AdjustorThunk (void);
extern void U3CBalanceOfBatchU3Ed__2_SetStateMachine_m34749E2FCC62C6089CA7D14ECC5008103D62EB8E_AdjustorThunk (void);
extern void U3CURIU3Ed__3_MoveNext_m994ED7A83E78A719CB3A0D3818A730A8C04EB4F1_AdjustorThunk (void);
extern void U3CURIU3Ed__3_SetStateMachine_mF1974E58996F7E03BED741B0EE6CB3416B2ACE72_AdjustorThunk (void);
extern void U3CBalanceOfU3Ed__1_MoveNext_m36D95A3F3A01BEFABEC55A365ED2730403E9456B_AdjustorThunk (void);
extern void U3CBalanceOfU3Ed__1_SetStateMachine_m8699DFD19EEB93AED6E7AA39FDB723211AE5BCB9_AdjustorThunk (void);
extern void U3CNameU3Ed__2_MoveNext_m5D9A4D337DE7457A7E606D536AE1A15B8F9A544B_AdjustorThunk (void);
extern void U3CNameU3Ed__2_SetStateMachine_m49C47D68B3C839B84D14583B1166CE6CF3CCA7B7_AdjustorThunk (void);
extern void U3CSymbolU3Ed__3_MoveNext_m7072C6887271018BD4296D3D6B26BE4454E33344_AdjustorThunk (void);
extern void U3CSymbolU3Ed__3_SetStateMachine_mB4F3CE1D6541161B57F0FF7DC4AD7695C09BD8E1_AdjustorThunk (void);
extern void U3CDecimalsU3Ed__4_MoveNext_m5F53688B23893A6A02A2AD3F3DBA38EB93F27271_AdjustorThunk (void);
extern void U3CDecimalsU3Ed__4_SetStateMachine_mE9819F1B7E958708201C635D9DA17E66020F1BC0_AdjustorThunk (void);
extern void U3CTotalSupplyU3Ed__5_MoveNext_m5991076501DC35DFDCC9BCEBD066050B10B7CD36_AdjustorThunk (void);
extern void U3CTotalSupplyU3Ed__5_SetStateMachine_m2B452102301436127D184A33B558C339BE18146B_AdjustorThunk (void);
extern void U3CBalanceOfU3Ed__1_MoveNext_m8015D9AE43B8A99CB9D959E50EAB745432A7F19E_AdjustorThunk (void);
extern void U3CBalanceOfU3Ed__1_SetStateMachine_mD5B12D1AB4C0AE02E3EFC2E56D0E9A69C4A7C0C7_AdjustorThunk (void);
extern void U3COwnerOfU3Ed__2_MoveNext_m8D8C7B73A05E641AB2DFA344DE4D2FFB5A89E6E9_AdjustorThunk (void);
extern void U3COwnerOfU3Ed__2_SetStateMachine_mB9A361356148E8EC7BB543DF7E2F58122EF5685C_AdjustorThunk (void);
extern void U3COwnerOfBatchU3Ed__3_MoveNext_m543097386999349CA5DA05E987905F28052EAC08_AdjustorThunk (void);
extern void U3COwnerOfBatchU3Ed__3_SetStateMachine_m94E80236D24084C606B00DAC402B69E5AB05B06A_AdjustorThunk (void);
extern void U3CURIU3Ed__4_MoveNext_mA02D956DD4091CF9D6D2962A5836550F95E7F6D0_AdjustorThunk (void);
extern void U3CURIU3Ed__4_SetStateMachine_m83FEB5116327CB0CD5BD2F04AF3E440AA2E52DEA_AdjustorThunk (void);
extern void U3CBalanceOfU3Ed__2_MoveNext_m49FF1C68CF752F053B40E31336F0AC117A7B61E8_AdjustorThunk (void);
extern void U3CBalanceOfU3Ed__2_SetStateMachine_mD8F5C1681185A6E89A41F1980CE6AABD3050AF10_AdjustorThunk (void);
extern void U3CVerifyU3Ed__3_MoveNext_mD9331AE8D306DC14F8644E0888C393B1F197956A_AdjustorThunk (void);
extern void U3CVerifyU3Ed__3_SetStateMachine_m477762C3E935EB66E24F0F2FF4564273024EC780_AdjustorThunk (void);
extern void U3CCallU3Ed__4_MoveNext_m7103426145D023067D0E812AC94242EDC9E96C8E_AdjustorThunk (void);
extern void U3CCallU3Ed__4_SetStateMachine_m2B4C2DDF94B74ACCA651A9AC53F1CA67853C059F_AdjustorThunk (void);
extern void U3CMultiCallU3Ed__5_MoveNext_m59C1E5558D5BD5130431DAD6C5BE122F4259F186_AdjustorThunk (void);
extern void U3CMultiCallU3Ed__5_SetStateMachine_m77A035AB7DEC5D4342A4617899A02C20AB0E8733_AdjustorThunk (void);
extern void U3CTxStatusU3Ed__6_MoveNext_m21E0D491760958726DCF5290A52E17241AAACFBF_AdjustorThunk (void);
extern void U3CTxStatusU3Ed__6_SetStateMachine_m6A59F5AB3D2A6077CF1BA0D1E8353FDEADC0D754_AdjustorThunk (void);
extern void U3CBlockNumberU3Ed__7_MoveNext_m1CA9A036D1F74ECEBBADBEF40970047E3C65B281_AdjustorThunk (void);
extern void U3CBlockNumberU3Ed__7_SetStateMachine_mBFBF55F1191EB7F3D04C204A5FFC7053EF8C8200_AdjustorThunk (void);
extern void U3CNonceU3Ed__8_MoveNext_m87AEF2132C7F36BB889C4769E58EF505EE1C5677_AdjustorThunk (void);
extern void U3CNonceU3Ed__8_SetStateMachine_m6C0E5BDD185012D9EE78EC7E73F9DB5448B13C1D_AdjustorThunk (void);
extern void U3CCreateContractDataU3Ed__9_MoveNext_m3A513FD488EB9248FA899D092048B412F36A2A43_AdjustorThunk (void);
extern void U3CCreateContractDataU3Ed__9_SetStateMachine_mC2A74C0432A6E8621E3D510878A7EBB59BA77B96_AdjustorThunk (void);
extern void U3CAllErc721U3Ed__10_MoveNext_mD378DF39149CAF4372C5C6FC3D73DCC66EE1D00B_AdjustorThunk (void);
extern void U3CAllErc721U3Ed__10_SetStateMachine_mC7F79A8B56C459462E982BFB2007442CB5E77327_AdjustorThunk (void);
extern void U3CAllErc1155U3Ed__11_MoveNext_m2B1FA74D528BA5675715FEAE5F073E81E1F9AABF_AdjustorThunk (void);
extern void U3CAllErc1155U3Ed__11_SetStateMachine_m09EBEC3D74A283E912739821BBC146F9A9306B00_AdjustorThunk (void);
extern void U3CGasPriceU3Ed__12_MoveNext_mD958FF90E9BB9E56039E6FF07C8817FD2D3B0A89_AdjustorThunk (void);
extern void U3CGasPriceU3Ed__12_SetStateMachine_mD8A5388148CA31A2177C494B2BF4ACAE993566C7_AdjustorThunk (void);
extern void U3CGasLimitU3Ed__13_MoveNext_m32EDBB43D9022D8AFDD3094B759B3C646F3DF75F_AdjustorThunk (void);
extern void U3CGasLimitU3Ed__13_SetStateMachine_mAAB510EC434AE95BC2910728023F47955EA53564_AdjustorThunk (void);
extern void U3CChainIdU3Ed__14_MoveNext_m39ABDF10A0AEA824B0A24EA8AC826776E30F1F88_AdjustorThunk (void);
extern void U3CChainIdU3Ed__14_SetStateMachine_mC3320A62EFF445C636E3E0CBE73AA0C5272DA035_AdjustorThunk (void);
extern void U3CCreateTransactionU3Ed__15_MoveNext_mC30E9CDA81123979FBD6455141C591E2985C1098_AdjustorThunk (void);
extern void U3CCreateTransactionU3Ed__15_SetStateMachine_m07800DBAA5A71F467B51548B88E44168704663BD_AdjustorThunk (void);
extern void U3CBroadcastTransactionU3Ed__16_MoveNext_m2912179954FAFC5CD4F687AFBFBC6D35D159A02F_AdjustorThunk (void);
extern void U3CBroadcastTransactionU3Ed__16_SetStateMachine_m6534FEE9D7F78D07C18917E9259F894B414C1E0E_AdjustorThunk (void);
extern void U3CSendTransactionU3Ed__1_MoveNext_m094A7D4B17DCF83DA8D339C9F0A4AEBA99E81550_AdjustorThunk (void);
extern void U3CSendTransactionU3Ed__1_SetStateMachine_m6C58AE8B37667F289A5318FDC6D296A10C72431B_AdjustorThunk (void);
extern void U3CSignU3Ed__2_MoveNext_mDB3E58221A60D54BD2C7856ACB2E61A9FA18E824_AdjustorThunk (void);
extern void U3CSignU3Ed__2_SetStateMachine_m80954B22F85344CA5FC5AA3D47EB652446A8DA40_AdjustorThunk (void);
extern void U3CSendContractU3Ed__10_MoveNext_mAE0FDF450FEBA139C99D52CECE5D43499DB90708_AdjustorThunk (void);
extern void U3CSendContractU3Ed__10_SetStateMachine_mDB924150509FEA19525D41521D4EFFBB8CA8C99C_AdjustorThunk (void);
extern void U3CSendTransactionU3Ed__11_MoveNext_m053E36DD1D85C5856A314131285B48D6F5C070C5_AdjustorThunk (void);
extern void U3CSendTransactionU3Ed__11_SetStateMachine_m324E10096F6699BD39FB4AB1847403016F77A827_AdjustorThunk (void);
extern void U3CSignU3Ed__12_MoveNext_m74FB11FDCCAAFD0E2BFF954199891117554D1890_AdjustorThunk (void);
extern void U3CSignU3Ed__12_SetStateMachine_mBAA214792AF8C334AC1EDA56E1C0A9B805A5C267_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_mE94D2F8B51DBFC5B070D07ABF611C7C7D483D016_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_m76E05730082FD3FB893CFE4ED257BFD0E1146F2A_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_mBE908F669CAC2ABDAC7872701F7C3BED2B4D66C8_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_mACD6D05861661B7259297843E6FE635E542144F8_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_mD7E6C3EB57F3683AC190990093F1DF60C5996BDF_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_m32F010E483167AED04DE742EB8940EDE54A25941_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_m18FDD5025FFAD18D6D5207671EF4439FFBF9117B_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_m8B9F75B4229353D82C1C4D7F5D9B3D0608653F2A_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_mB8317507C6D1634221D136B56400405740C35941_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_m0E9F22BDB96721E7BD11F85CE5F248ABFF1D4399_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_m1009F1D54AA9418573E986DC091E8F6CEA2A152E_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_m5C52A6DA998FC196467CC1A8D7DA3F1B2680784A_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_m7C288E439AA5604812418AC5E663FB9F2463FEF5_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_m8ECA709670FDBBB8CF6CE3BE68AC77CEE55FEF6B_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_m9684BD13AB1E3C45FEF01FB8189CB1CC84CB872B_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_m90AE0209DA8EBB3AE0D980935EAEC3783FB842AD_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_m7FC8B7D9E9B28572F8FE2AB87C3DF77F8D3DC143_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_mE60DC6B0816FFFA80349E8D40C3CFC426F8001B7_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_m2251CA0D2B663CC81ACCA1DF52CF03390A34009A_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_mE6B01D55F9D6F1B641203AD045DA10D399ABC770_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_m4ED7CA82E64E03491B0A4A643ECBE32AD09ADED1_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_m0ECA16AF5C9507944D8F09DD555E373FC01AF5F7_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_m51BDCBDA7FC0D710D01081E177173067F022F3EE_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_mB281CC760C5BC7236B4D097976C68D5CF60EF754_AdjustorThunk (void);
extern void U3CStartU3Ed__1_MoveNext_m56F7B4255F3860898DFF3B4A9A4A42827AD9C42F_AdjustorThunk (void);
extern void U3CStartU3Ed__1_SetStateMachine_mF4BABB9B81366714D470D287C46918A5C6D7EB6A_AdjustorThunk (void);
extern void U3CStartU3Ed__1_MoveNext_mCF92100CA8DD409E5D8F73CBA00D4C85FEC94D1B_AdjustorThunk (void);
extern void U3CStartU3Ed__1_SetStateMachine_mCD7FDC90AA399D1DB295620538CA230DEAB95A1F_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_m1C1A1991718A66B2A3ED5D056A9B82B28530ADA8_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_m1B759B91C18F303ECF8AA29965839045F5FB49C9_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_m4BC1DF55905C7CAE4ECD9A68314A3A327C5BC727_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_m90FCB1CDB53828A20360A302E41321FA2915A8B7_AdjustorThunk (void);
extern void U3CMintU3Ed__6_MoveNext_mAA93E837E07DB6EAFAEDEFE955B062A1F5430EB6_AdjustorThunk (void);
extern void U3CMintU3Ed__6_SetStateMachine_mE34E1011DA6AE67062B3638D5CA080CB82EE7586_AdjustorThunk (void);
extern void U3CChangeNameU3Ed__8_MoveNext_m8BA79E020BBD8A22B2B2ADE01C887C8673214CA0_AdjustorThunk (void);
extern void U3CChangeNameU3Ed__8_SetStateMachine_m2F1CDC60D482E2AFCE6FBEA95ECA5648BBE31409_AdjustorThunk (void);
extern void U3CReLoadU3Ed__9_MoveNext_m27927DFB1EEB05FC2A70ED64B9A665BC68F5BF4B_AdjustorThunk (void);
extern void U3CReLoadU3Ed__9_SetStateMachine_m89009A42D7FAC4B90BC171A10997A50E3AFDE867_AdjustorThunk (void);
extern void U3CGetNftU3Ed__10_MoveNext_mBDE73D4F04CD780307D74E27B7121D5C5C202DD5_AdjustorThunk (void);
extern void U3CGetNftU3Ed__10_SetStateMachine_m110A5F8CA3599AE1E562A01BAE0B1194F8801D48_AdjustorThunk (void);
extern void U3CBurnU3Ed__11_MoveNext_m933C2B659AB810835B3B43A240113ABEB2CD4977_AdjustorThunk (void);
extern void U3CBurnU3Ed__11_SetStateMachine_m1F8C91D9B818F4B52BF56BBC71398F27DFFF3417_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_m9D1B1A888D55782764A3D7818C3741A2C53ADFD0_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_mA01B28EE49B96A3BB28545E7375E2E4823E4F158_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_m87DC3F4DB835025DF80DEA749A2CD4BFC153321A_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_m8763265A5E48878995143FE2211C1135DD40A431_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_m46792A67B48E13440730A2271EFD9D3F37DCE540_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_mE36D9D616EF594199062DEBE0273AC423B4D8347_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_m0BD1687277E3B971A8A377BD0627EF8FD774A481_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_mF482F4B6CA65515CF4469D1B6BCD7C43F86BB665_AdjustorThunk (void);
extern void U3CStartU3Ed__0_MoveNext_m4C83C0CAB5D8222B58E90AD7B4B1D556C6D19ED1_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_m8B1480939675F9433880643CD4EA0C534881209D_AdjustorThunk (void);
extern void U3COnSend1155U3Ed__0_MoveNext_m5CC620F10F35AC4D9113A097C0F0C6999696C126_AdjustorThunk (void);
extern void U3COnSend1155U3Ed__0_SetStateMachine_m6044558A9818E8F112CEFE3BF401EDE447831294_AdjustorThunk (void);
extern void U3COnSend20U3Ed__0_MoveNext_mE8FEC374CB159E1AC59FB9226035C06D794D6855_AdjustorThunk (void);
extern void U3COnSend20U3Ed__0_SetStateMachine_m2459E52710C5D0C1641F042AA1246E79DB87C834_AdjustorThunk (void);
extern void U3COnSend721U3Ed__0_MoveNext_m92930AB8A01B81E9C029494FC9E7885D52300E41_AdjustorThunk (void);
extern void U3COnSend721U3Ed__0_SetStateMachine_m2CDC6C3A888109DBFB4B350C5D3969E5E9805EF3_AdjustorThunk (void);
extern void U3COnSendTransactionU3Ed__0_MoveNext_m83BE0A4E0DE4D5065D305BEF0E6FD2020BDDA22E_AdjustorThunk (void);
extern void U3COnSendTransactionU3Ed__0_SetStateMachine_m75CF9DD43E6B9372BE208D6144B6D3D389EDD257_AdjustorThunk (void);
extern void U3COnSendTransactionU3Ed__0_MoveNext_mCE4513700F2438755640D4CF601F7B4D7E7A7321_AdjustorThunk (void);
extern void U3COnSendTransactionU3Ed__0_SetStateMachine_mA1354644175F2D58C90730B1FDB4869FA7BDFC48_AdjustorThunk (void);
extern void U3COnSignMessageU3Ed__0_MoveNext_m910B0F834534ADBBA4DEA91CF2B62C5EBA533EBE_AdjustorThunk (void);
extern void U3COnSignMessageU3Ed__0_SetStateMachine_m4338BE5F3F9B7C77682650825239AC6C7138A6B1_AdjustorThunk (void);
extern void U3COnTransfer1155U3Ed__0_MoveNext_mA90B1AC56399F741DB2348B2941FDBF72E9AC597_AdjustorThunk (void);
extern void U3COnTransfer1155U3Ed__0_SetStateMachine_mE5364F3E8E033AA906ACE7657884281076A015C5_AdjustorThunk (void);
extern void U3COnTransfer20U3Ed__0_MoveNext_mAC06EED0D1221620A17D9BE470EA8130592D27BE_AdjustorThunk (void);
extern void U3COnTransfer20U3Ed__0_SetStateMachine_mF8F73F2742E7641DF16BA8DAD19AB3A74142E61C_AdjustorThunk (void);
extern void U3COnTransfer721U3Ed__0_MoveNext_m8C1BB1B0A36207ABDD5BB95E54BEA6EE2265F07F_AdjustorThunk (void);
extern void U3COnTransfer721U3Ed__0_SetStateMachine_mF885565CD1B11CCD1E3057612D11154B7629129D_AdjustorThunk (void);
extern void U3COnSendContractU3Ed__0_MoveNext_m87D0E94C972176E1F9AEA97BCEFD7CBBF79BB814_AdjustorThunk (void);
extern void U3COnSendContractU3Ed__0_SetStateMachine_m7A097F8F1BB131B8A785D2F22D1D28D7527646EA_AdjustorThunk (void);
extern void U3COnSendTransactionU3Ed__0_MoveNext_mC7CE9060F43BA816093410666933E4EBB204561B_AdjustorThunk (void);
extern void U3COnSendTransactionU3Ed__0_SetStateMachine_mC12443C49C60F7DCAA5CDA5E716B32E16BD74723_AdjustorThunk (void);
extern void U3COnSignMessageU3Ed__0_MoveNext_m3057F7982AC1CB0EDD2664D8ED6B7E5AC4ADE2A1_AdjustorThunk (void);
extern void U3COnSignMessageU3Ed__0_SetStateMachine_m3F0DC2AEF8F20669EFA57A92C67E1F9A5F4B6802_AdjustorThunk (void);
extern void U3CSafeTransferFromU3Ed__5_MoveNext_m64353C3BCC55943924533B82580063995694E676_AdjustorThunk (void);
extern void U3CSafeTransferFromU3Ed__5_SetStateMachine_m357DFCF039FEA8B6C3395C25DFC5753549AD9156_AdjustorThunk (void);
extern void U3CTransferU3Ed__4_MoveNext_m5655C8D900D8FDBA8EF1BB4A1111791D22525BFD_AdjustorThunk (void);
extern void U3CTransferU3Ed__4_SetStateMachine_mA32D0A2CF0FC0E65AD5EA5081D9B5860C23936DE_AdjustorThunk (void);
extern void U3CSafeTransferFromU3Ed__4_MoveNext_m967807132732773C90E46AD493832B9237EA807B_AdjustorThunk (void);
extern void U3CSafeTransferFromU3Ed__4_SetStateMachine_m72420EB4DE07C850B47609F4910E992BFAA0CC10_AdjustorThunk (void);
extern void U3COnLoginU3Ed__2_MoveNext_mC494AC5CF4F7CB0F482C28FCBC80A560EB1EDAFB_AdjustorThunk (void);
extern void U3COnLoginU3Ed__2_SetStateMachine_mB0FF2BA206BB13E75568802ED92D9C2A4609EBC1_AdjustorThunk (void);
extern void U3COnConnectedU3Ed__6_MoveNext_m01EC6E60475A256640C35990CAD5131C04AB747D_AdjustorThunk (void);
extern void U3COnConnectedU3Ed__6_SetStateMachine_mCDC0D60876FA7F6803612569D8BCB444418EC9D9_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[171] = 
{
	{ 0x06000010, InWorldActions__ctor_m377C2A36ACF59A5F3A9480272AD84E80967512B3_AdjustorThunk },
	{ 0x06000011, InWorldActions_get_MovementZ_mA018CC88008FA3D86304ADDE5ED3568FA663DC78_AdjustorThunk },
	{ 0x06000012, InWorldActions_get_MovementX_m8015B7B9C4511D1CD73965AED29BBBAA05AE6969_AdjustorThunk },
	{ 0x06000013, InWorldActions_get_Action_m7758A48C330CF9F9B24E2805665841BCC6D1289D_AdjustorThunk },
	{ 0x06000014, InWorldActions_get_Jump_m63BF8D7698C3F5E7263DB94F3B0CAFEB8B1FECED_AdjustorThunk },
	{ 0x06000015, InWorldActions_Get_mFFC96166CA3C940F1B367B4071775D67851CDBA8_AdjustorThunk },
	{ 0x06000016, InWorldActions_Enable_mE24CDC578A2885C807CA6410D7A461FB58905546_AdjustorThunk },
	{ 0x06000017, InWorldActions_Disable_mB4844B96C11D7E89BF1F6B0C2EE3663158C5C1B0_AdjustorThunk },
	{ 0x06000018, InWorldActions_get_enabled_m745C21AC47E3A254D3E20F3238A67B74090EF428_AdjustorThunk },
	{ 0x0600001A, InWorldActions_SetCallbacks_mB956BB6C2016CC4D1E9DB40B979D7FD17F51949A_AdjustorThunk },
	{ 0x0600001B, UIControlActions__ctor_m66283505DA58059B0C8D6F1129A05F97DB6AA67E_AdjustorThunk },
	{ 0x0600001C, UIControlActions_get_Next_mD631E5E62E29DB74B742EA6EAE37250CE2BB6215_AdjustorThunk },
	{ 0x0600001D, UIControlActions_get_Select_mE930EB65AEEE60A2887E91152C344823AC38C2B4_AdjustorThunk },
	{ 0x0600001E, UIControlActions_get_Back_m122706E98ADC48CC78C819577C14D06F41B8DBE2_AdjustorThunk },
	{ 0x0600001F, UIControlActions_Get_m2F01A1629782D6AAD26471906B702482D844053A_AdjustorThunk },
	{ 0x06000020, UIControlActions_Enable_mFBA3EFF1773F8EA9957EB87875AC59EA43C9F315_AdjustorThunk },
	{ 0x06000021, UIControlActions_Disable_mC887C32C55E39858883D1B101220E4739A23343B_AdjustorThunk },
	{ 0x06000022, UIControlActions_get_enabled_m997E192F447045B825BD73AEA44D37FFA1B6BDA6_AdjustorThunk },
	{ 0x06000024, UIControlActions_SetCallbacks_m17D84220A7BAED2C0F02D637252AA1352C34E338_AdjustorThunk },
	{ 0x06000063, U3CStartU3Ed__1_MoveNext_m6642927BFC834022BC2708E0866FEEE26E05EFD5_AdjustorThunk },
	{ 0x06000064, U3CStartU3Ed__1_SetStateMachine_m72770C78EEEB2DFDB98D0015DFA3CB7FE724D5E3_AdjustorThunk },
	{ 0x0600006A, U3CBalanceOfU3Ed__1_MoveNext_m827BEB7B02D7F512F3D3272FEE50EB4527603DE0_AdjustorThunk },
	{ 0x0600006B, U3CBalanceOfU3Ed__1_SetStateMachine_m6AF4E4155ABC37A3A94695D78B0A266B91662C55_AdjustorThunk },
	{ 0x0600006C, U3CBalanceOfBatchU3Ed__2_MoveNext_m6EED73E8199BF4B37338A03EE80469DFC5F7EA60_AdjustorThunk },
	{ 0x0600006D, U3CBalanceOfBatchU3Ed__2_SetStateMachine_m34749E2FCC62C6089CA7D14ECC5008103D62EB8E_AdjustorThunk },
	{ 0x0600006E, U3CURIU3Ed__3_MoveNext_m994ED7A83E78A719CB3A0D3818A730A8C04EB4F1_AdjustorThunk },
	{ 0x0600006F, U3CURIU3Ed__3_SetStateMachine_mF1974E58996F7E03BED741B0EE6CB3416B2ACE72_AdjustorThunk },
	{ 0x06000077, U3CBalanceOfU3Ed__1_MoveNext_m36D95A3F3A01BEFABEC55A365ED2730403E9456B_AdjustorThunk },
	{ 0x06000078, U3CBalanceOfU3Ed__1_SetStateMachine_m8699DFD19EEB93AED6E7AA39FDB723211AE5BCB9_AdjustorThunk },
	{ 0x06000079, U3CNameU3Ed__2_MoveNext_m5D9A4D337DE7457A7E606D536AE1A15B8F9A544B_AdjustorThunk },
	{ 0x0600007A, U3CNameU3Ed__2_SetStateMachine_m49C47D68B3C839B84D14583B1166CE6CF3CCA7B7_AdjustorThunk },
	{ 0x0600007B, U3CSymbolU3Ed__3_MoveNext_m7072C6887271018BD4296D3D6B26BE4454E33344_AdjustorThunk },
	{ 0x0600007C, U3CSymbolU3Ed__3_SetStateMachine_mB4F3CE1D6541161B57F0FF7DC4AD7695C09BD8E1_AdjustorThunk },
	{ 0x0600007D, U3CDecimalsU3Ed__4_MoveNext_m5F53688B23893A6A02A2AD3F3DBA38EB93F27271_AdjustorThunk },
	{ 0x0600007E, U3CDecimalsU3Ed__4_SetStateMachine_mE9819F1B7E958708201C635D9DA17E66020F1BC0_AdjustorThunk },
	{ 0x0600007F, U3CTotalSupplyU3Ed__5_MoveNext_m5991076501DC35DFDCC9BCEBD066050B10B7CD36_AdjustorThunk },
	{ 0x06000080, U3CTotalSupplyU3Ed__5_SetStateMachine_m2B452102301436127D184A33B558C339BE18146B_AdjustorThunk },
	{ 0x06000087, U3CBalanceOfU3Ed__1_MoveNext_m8015D9AE43B8A99CB9D959E50EAB745432A7F19E_AdjustorThunk },
	{ 0x06000088, U3CBalanceOfU3Ed__1_SetStateMachine_mD5B12D1AB4C0AE02E3EFC2E56D0E9A69C4A7C0C7_AdjustorThunk },
	{ 0x06000089, U3COwnerOfU3Ed__2_MoveNext_m8D8C7B73A05E641AB2DFA344DE4D2FFB5A89E6E9_AdjustorThunk },
	{ 0x0600008A, U3COwnerOfU3Ed__2_SetStateMachine_mB9A361356148E8EC7BB543DF7E2F58122EF5685C_AdjustorThunk },
	{ 0x0600008B, U3COwnerOfBatchU3Ed__3_MoveNext_m543097386999349CA5DA05E987905F28052EAC08_AdjustorThunk },
	{ 0x0600008C, U3COwnerOfBatchU3Ed__3_SetStateMachine_m94E80236D24084C606B00DAC402B69E5AB05B06A_AdjustorThunk },
	{ 0x0600008D, U3CURIU3Ed__4_MoveNext_mA02D956DD4091CF9D6D2962A5836550F95E7F6D0_AdjustorThunk },
	{ 0x0600008E, U3CURIU3Ed__4_SetStateMachine_m83FEB5116327CB0CD5BD2F04AF3E440AA2E52DEA_AdjustorThunk },
	{ 0x060000A1, U3CBalanceOfU3Ed__2_MoveNext_m49FF1C68CF752F053B40E31336F0AC117A7B61E8_AdjustorThunk },
	{ 0x060000A2, U3CBalanceOfU3Ed__2_SetStateMachine_mD8F5C1681185A6E89A41F1980CE6AABD3050AF10_AdjustorThunk },
	{ 0x060000A3, U3CVerifyU3Ed__3_MoveNext_mD9331AE8D306DC14F8644E0888C393B1F197956A_AdjustorThunk },
	{ 0x060000A4, U3CVerifyU3Ed__3_SetStateMachine_m477762C3E935EB66E24F0F2FF4564273024EC780_AdjustorThunk },
	{ 0x060000A5, U3CCallU3Ed__4_MoveNext_m7103426145D023067D0E812AC94242EDC9E96C8E_AdjustorThunk },
	{ 0x060000A6, U3CCallU3Ed__4_SetStateMachine_m2B4C2DDF94B74ACCA651A9AC53F1CA67853C059F_AdjustorThunk },
	{ 0x060000A7, U3CMultiCallU3Ed__5_MoveNext_m59C1E5558D5BD5130431DAD6C5BE122F4259F186_AdjustorThunk },
	{ 0x060000A8, U3CMultiCallU3Ed__5_SetStateMachine_m77A035AB7DEC5D4342A4617899A02C20AB0E8733_AdjustorThunk },
	{ 0x060000A9, U3CTxStatusU3Ed__6_MoveNext_m21E0D491760958726DCF5290A52E17241AAACFBF_AdjustorThunk },
	{ 0x060000AA, U3CTxStatusU3Ed__6_SetStateMachine_m6A59F5AB3D2A6077CF1BA0D1E8353FDEADC0D754_AdjustorThunk },
	{ 0x060000AB, U3CBlockNumberU3Ed__7_MoveNext_m1CA9A036D1F74ECEBBADBEF40970047E3C65B281_AdjustorThunk },
	{ 0x060000AC, U3CBlockNumberU3Ed__7_SetStateMachine_mBFBF55F1191EB7F3D04C204A5FFC7053EF8C8200_AdjustorThunk },
	{ 0x060000AD, U3CNonceU3Ed__8_MoveNext_m87AEF2132C7F36BB889C4769E58EF505EE1C5677_AdjustorThunk },
	{ 0x060000AE, U3CNonceU3Ed__8_SetStateMachine_m6C0E5BDD185012D9EE78EC7E73F9DB5448B13C1D_AdjustorThunk },
	{ 0x060000AF, U3CCreateContractDataU3Ed__9_MoveNext_m3A513FD488EB9248FA899D092048B412F36A2A43_AdjustorThunk },
	{ 0x060000B0, U3CCreateContractDataU3Ed__9_SetStateMachine_mC2A74C0432A6E8621E3D510878A7EBB59BA77B96_AdjustorThunk },
	{ 0x060000B1, U3CAllErc721U3Ed__10_MoveNext_mD378DF39149CAF4372C5C6FC3D73DCC66EE1D00B_AdjustorThunk },
	{ 0x060000B2, U3CAllErc721U3Ed__10_SetStateMachine_mC7F79A8B56C459462E982BFB2007442CB5E77327_AdjustorThunk },
	{ 0x060000B3, U3CAllErc1155U3Ed__11_MoveNext_m2B1FA74D528BA5675715FEAE5F073E81E1F9AABF_AdjustorThunk },
	{ 0x060000B4, U3CAllErc1155U3Ed__11_SetStateMachine_m09EBEC3D74A283E912739821BBC146F9A9306B00_AdjustorThunk },
	{ 0x060000B5, U3CGasPriceU3Ed__12_MoveNext_mD958FF90E9BB9E56039E6FF07C8817FD2D3B0A89_AdjustorThunk },
	{ 0x060000B6, U3CGasPriceU3Ed__12_SetStateMachine_mD8A5388148CA31A2177C494B2BF4ACAE993566C7_AdjustorThunk },
	{ 0x060000B7, U3CGasLimitU3Ed__13_MoveNext_m32EDBB43D9022D8AFDD3094B759B3C646F3DF75F_AdjustorThunk },
	{ 0x060000B8, U3CGasLimitU3Ed__13_SetStateMachine_mAAB510EC434AE95BC2910728023F47955EA53564_AdjustorThunk },
	{ 0x060000B9, U3CChainIdU3Ed__14_MoveNext_m39ABDF10A0AEA824B0A24EA8AC826776E30F1F88_AdjustorThunk },
	{ 0x060000BA, U3CChainIdU3Ed__14_SetStateMachine_mC3320A62EFF445C636E3E0CBE73AA0C5272DA035_AdjustorThunk },
	{ 0x060000BB, U3CCreateTransactionU3Ed__15_MoveNext_mC30E9CDA81123979FBD6455141C591E2985C1098_AdjustorThunk },
	{ 0x060000BC, U3CCreateTransactionU3Ed__15_SetStateMachine_m07800DBAA5A71F467B51548B88E44168704663BD_AdjustorThunk },
	{ 0x060000BD, U3CBroadcastTransactionU3Ed__16_MoveNext_m2912179954FAFC5CD4F687AFBFBC6D35D159A02F_AdjustorThunk },
	{ 0x060000BE, U3CBroadcastTransactionU3Ed__16_SetStateMachine_m6534FEE9D7F78D07C18917E9259F894B414C1E0E_AdjustorThunk },
	{ 0x060000C8, U3CSendTransactionU3Ed__1_MoveNext_m094A7D4B17DCF83DA8D339C9F0A4AEBA99E81550_AdjustorThunk },
	{ 0x060000C9, U3CSendTransactionU3Ed__1_SetStateMachine_m6C58AE8B37667F289A5318FDC6D296A10C72431B_AdjustorThunk },
	{ 0x060000CA, U3CSignU3Ed__2_MoveNext_mDB3E58221A60D54BD2C7856ACB2E61A9FA18E824_AdjustorThunk },
	{ 0x060000CB, U3CSignU3Ed__2_SetStateMachine_m80954B22F85344CA5FC5AA3D47EB652446A8DA40_AdjustorThunk },
	{ 0x060000DB, U3CSendContractU3Ed__10_MoveNext_mAE0FDF450FEBA139C99D52CECE5D43499DB90708_AdjustorThunk },
	{ 0x060000DC, U3CSendContractU3Ed__10_SetStateMachine_mDB924150509FEA19525D41521D4EFFBB8CA8C99C_AdjustorThunk },
	{ 0x060000DD, U3CSendTransactionU3Ed__11_MoveNext_m053E36DD1D85C5856A314131285B48D6F5C070C5_AdjustorThunk },
	{ 0x060000DE, U3CSendTransactionU3Ed__11_SetStateMachine_m324E10096F6699BD39FB4AB1847403016F77A827_AdjustorThunk },
	{ 0x060000DF, U3CSignU3Ed__12_MoveNext_m74FB11FDCCAAFD0E2BFF954199891117554D1890_AdjustorThunk },
	{ 0x060000E0, U3CSignU3Ed__12_SetStateMachine_mBAA214792AF8C334AC1EDA56E1C0A9B805A5C267_AdjustorThunk },
	{ 0x060000E3, U3CStartU3Ed__0_MoveNext_mE94D2F8B51DBFC5B070D07ABF611C7C7D483D016_AdjustorThunk },
	{ 0x060000E4, U3CStartU3Ed__0_SetStateMachine_m76E05730082FD3FB893CFE4ED257BFD0E1146F2A_AdjustorThunk },
	{ 0x060000E7, U3CStartU3Ed__0_MoveNext_mBE908F669CAC2ABDAC7872701F7C3BED2B4D66C8_AdjustorThunk },
	{ 0x060000E8, U3CStartU3Ed__0_SetStateMachine_mACD6D05861661B7259297843E6FE635E542144F8_AdjustorThunk },
	{ 0x060000EB, U3CStartU3Ed__0_MoveNext_mD7E6C3EB57F3683AC190990093F1DF60C5996BDF_AdjustorThunk },
	{ 0x060000EC, U3CStartU3Ed__0_SetStateMachine_m32F010E483167AED04DE742EB8940EDE54A25941_AdjustorThunk },
	{ 0x060000EF, U3CStartU3Ed__0_MoveNext_m18FDD5025FFAD18D6D5207671EF4439FFBF9117B_AdjustorThunk },
	{ 0x060000F0, U3CStartU3Ed__0_SetStateMachine_m8B9F75B4229353D82C1C4D7F5D9B3D0608653F2A_AdjustorThunk },
	{ 0x060000F3, U3CStartU3Ed__0_MoveNext_mB8317507C6D1634221D136B56400405740C35941_AdjustorThunk },
	{ 0x060000F4, U3CStartU3Ed__0_SetStateMachine_m0E9F22BDB96721E7BD11F85CE5F248ABFF1D4399_AdjustorThunk },
	{ 0x060000F7, U3CStartU3Ed__0_MoveNext_m1009F1D54AA9418573E986DC091E8F6CEA2A152E_AdjustorThunk },
	{ 0x060000F8, U3CStartU3Ed__0_SetStateMachine_m5C52A6DA998FC196467CC1A8D7DA3F1B2680784A_AdjustorThunk },
	{ 0x060000FB, U3CStartU3Ed__0_MoveNext_m7C288E439AA5604812418AC5E663FB9F2463FEF5_AdjustorThunk },
	{ 0x060000FC, U3CStartU3Ed__0_SetStateMachine_m8ECA709670FDBBB8CF6CE3BE68AC77CEE55FEF6B_AdjustorThunk },
	{ 0x060000FF, U3CStartU3Ed__0_MoveNext_m9684BD13AB1E3C45FEF01FB8189CB1CC84CB872B_AdjustorThunk },
	{ 0x06000100, U3CStartU3Ed__0_SetStateMachine_m90AE0209DA8EBB3AE0D980935EAEC3783FB842AD_AdjustorThunk },
	{ 0x06000103, U3CStartU3Ed__0_MoveNext_m7FC8B7D9E9B28572F8FE2AB87C3DF77F8D3DC143_AdjustorThunk },
	{ 0x06000104, U3CStartU3Ed__0_SetStateMachine_mE60DC6B0816FFFA80349E8D40C3CFC426F8001B7_AdjustorThunk },
	{ 0x06000107, U3CStartU3Ed__0_MoveNext_m2251CA0D2B663CC81ACCA1DF52CF03390A34009A_AdjustorThunk },
	{ 0x06000108, U3CStartU3Ed__0_SetStateMachine_mE6B01D55F9D6F1B641203AD045DA10D399ABC770_AdjustorThunk },
	{ 0x0600010B, U3CStartU3Ed__0_MoveNext_m4ED7CA82E64E03491B0A4A643ECBE32AD09ADED1_AdjustorThunk },
	{ 0x0600010C, U3CStartU3Ed__0_SetStateMachine_m0ECA16AF5C9507944D8F09DD555E373FC01AF5F7_AdjustorThunk },
	{ 0x0600010F, U3CStartU3Ed__0_MoveNext_m51BDCBDA7FC0D710D01081E177173067F022F3EE_AdjustorThunk },
	{ 0x06000110, U3CStartU3Ed__0_SetStateMachine_mB281CC760C5BC7236B4D097976C68D5CF60EF754_AdjustorThunk },
	{ 0x0600011C, U3CStartU3Ed__1_MoveNext_m56F7B4255F3860898DFF3B4A9A4A42827AD9C42F_AdjustorThunk },
	{ 0x0600011D, U3CStartU3Ed__1_SetStateMachine_mF4BABB9B81366714D470D287C46918A5C6D7EB6A_AdjustorThunk },
	{ 0x06000129, U3CStartU3Ed__1_MoveNext_mCF92100CA8DD409E5D8F73CBA00D4C85FEC94D1B_AdjustorThunk },
	{ 0x0600012A, U3CStartU3Ed__1_SetStateMachine_mCD7FDC90AA399D1DB295620538CA230DEAB95A1F_AdjustorThunk },
	{ 0x0600012D, U3CStartU3Ed__0_MoveNext_m1C1A1991718A66B2A3ED5D056A9B82B28530ADA8_AdjustorThunk },
	{ 0x0600012E, U3CStartU3Ed__0_SetStateMachine_m1B759B91C18F303ECF8AA29965839045F5FB49C9_AdjustorThunk },
	{ 0x06000131, U3CStartU3Ed__0_MoveNext_m4BC1DF55905C7CAE4ECD9A68314A3A327C5BC727_AdjustorThunk },
	{ 0x06000132, U3CStartU3Ed__0_SetStateMachine_m90FCB1CDB53828A20360A302E41321FA2915A8B7_AdjustorThunk },
	{ 0x06000143, U3CMintU3Ed__6_MoveNext_mAA93E837E07DB6EAFAEDEFE955B062A1F5430EB6_AdjustorThunk },
	{ 0x06000144, U3CMintU3Ed__6_SetStateMachine_mE34E1011DA6AE67062B3638D5CA080CB82EE7586_AdjustorThunk },
	{ 0x06000145, U3CChangeNameU3Ed__8_MoveNext_m8BA79E020BBD8A22B2B2ADE01C887C8673214CA0_AdjustorThunk },
	{ 0x06000146, U3CChangeNameU3Ed__8_SetStateMachine_m2F1CDC60D482E2AFCE6FBEA95ECA5648BBE31409_AdjustorThunk },
	{ 0x06000147, U3CReLoadU3Ed__9_MoveNext_m27927DFB1EEB05FC2A70ED64B9A665BC68F5BF4B_AdjustorThunk },
	{ 0x06000148, U3CReLoadU3Ed__9_SetStateMachine_m89009A42D7FAC4B90BC171A10997A50E3AFDE867_AdjustorThunk },
	{ 0x06000149, U3CGetNftU3Ed__10_MoveNext_mBDE73D4F04CD780307D74E27B7121D5C5C202DD5_AdjustorThunk },
	{ 0x0600014A, U3CGetNftU3Ed__10_SetStateMachine_m110A5F8CA3599AE1E562A01BAE0B1194F8801D48_AdjustorThunk },
	{ 0x0600014B, U3CBurnU3Ed__11_MoveNext_m933C2B659AB810835B3B43A240113ABEB2CD4977_AdjustorThunk },
	{ 0x0600014C, U3CBurnU3Ed__11_SetStateMachine_m1F8C91D9B818F4B52BF56BBC71398F27DFFF3417_AdjustorThunk },
	{ 0x0600014F, U3CStartU3Ed__0_MoveNext_m9D1B1A888D55782764A3D7818C3741A2C53ADFD0_AdjustorThunk },
	{ 0x06000150, U3CStartU3Ed__0_SetStateMachine_mA01B28EE49B96A3BB28545E7375E2E4823E4F158_AdjustorThunk },
	{ 0x06000153, U3CStartU3Ed__0_MoveNext_m87DC3F4DB835025DF80DEA749A2CD4BFC153321A_AdjustorThunk },
	{ 0x06000154, U3CStartU3Ed__0_SetStateMachine_m8763265A5E48878995143FE2211C1135DD40A431_AdjustorThunk },
	{ 0x06000157, U3CStartU3Ed__0_MoveNext_m46792A67B48E13440730A2271EFD9D3F37DCE540_AdjustorThunk },
	{ 0x06000158, U3CStartU3Ed__0_SetStateMachine_mE36D9D616EF594199062DEBE0273AC423B4D8347_AdjustorThunk },
	{ 0x0600015B, U3CStartU3Ed__0_MoveNext_m0BD1687277E3B971A8A377BD0627EF8FD774A481_AdjustorThunk },
	{ 0x0600015C, U3CStartU3Ed__0_SetStateMachine_mF482F4B6CA65515CF4469D1B6BCD7C43F86BB665_AdjustorThunk },
	{ 0x0600015F, U3CStartU3Ed__0_MoveNext_m4C83C0CAB5D8222B58E90AD7B4B1D556C6D19ED1_AdjustorThunk },
	{ 0x06000160, U3CStartU3Ed__0_SetStateMachine_m8B1480939675F9433880643CD4EA0C534881209D_AdjustorThunk },
	{ 0x06000165, U3COnSend1155U3Ed__0_MoveNext_m5CC620F10F35AC4D9113A097C0F0C6999696C126_AdjustorThunk },
	{ 0x06000166, U3COnSend1155U3Ed__0_SetStateMachine_m6044558A9818E8F112CEFE3BF401EDE447831294_AdjustorThunk },
	{ 0x06000169, U3COnSend20U3Ed__0_MoveNext_mE8FEC374CB159E1AC59FB9226035C06D794D6855_AdjustorThunk },
	{ 0x0600016A, U3COnSend20U3Ed__0_SetStateMachine_m2459E52710C5D0C1641F042AA1246E79DB87C834_AdjustorThunk },
	{ 0x0600016D, U3COnSend721U3Ed__0_MoveNext_m92930AB8A01B81E9C029494FC9E7885D52300E41_AdjustorThunk },
	{ 0x0600016E, U3COnSend721U3Ed__0_SetStateMachine_m2CDC6C3A888109DBFB4B350C5D3969E5E9805EF3_AdjustorThunk },
	{ 0x06000171, U3COnSendTransactionU3Ed__0_MoveNext_m83BE0A4E0DE4D5065D305BEF0E6FD2020BDDA22E_AdjustorThunk },
	{ 0x06000172, U3COnSendTransactionU3Ed__0_SetStateMachine_m75CF9DD43E6B9372BE208D6144B6D3D389EDD257_AdjustorThunk },
	{ 0x06000179, U3COnSendTransactionU3Ed__0_MoveNext_mCE4513700F2438755640D4CF601F7B4D7E7A7321_AdjustorThunk },
	{ 0x0600017A, U3COnSendTransactionU3Ed__0_SetStateMachine_mA1354644175F2D58C90730B1FDB4869FA7BDFC48_AdjustorThunk },
	{ 0x0600017F, U3COnSignMessageU3Ed__0_MoveNext_m910B0F834534ADBBA4DEA91CF2B62C5EBA533EBE_AdjustorThunk },
	{ 0x06000180, U3COnSignMessageU3Ed__0_SetStateMachine_m4338BE5F3F9B7C77682650825239AC6C7138A6B1_AdjustorThunk },
	{ 0x06000183, U3COnTransfer1155U3Ed__0_MoveNext_mA90B1AC56399F741DB2348B2941FDBF72E9AC597_AdjustorThunk },
	{ 0x06000184, U3COnTransfer1155U3Ed__0_SetStateMachine_mE5364F3E8E033AA906ACE7657884281076A015C5_AdjustorThunk },
	{ 0x06000187, U3COnTransfer20U3Ed__0_MoveNext_mAC06EED0D1221620A17D9BE470EA8130592D27BE_AdjustorThunk },
	{ 0x06000188, U3COnTransfer20U3Ed__0_SetStateMachine_mF8F73F2742E7641DF16BA8DAD19AB3A74142E61C_AdjustorThunk },
	{ 0x0600018B, U3COnTransfer721U3Ed__0_MoveNext_m8C1BB1B0A36207ABDD5BB95E54BEA6EE2265F07F_AdjustorThunk },
	{ 0x0600018C, U3COnTransfer721U3Ed__0_SetStateMachine_mF885565CD1B11CCD1E3057612D11154B7629129D_AdjustorThunk },
	{ 0x0600018F, U3COnSendContractU3Ed__0_MoveNext_m87D0E94C972176E1F9AEA97BCEFD7CBBF79BB814_AdjustorThunk },
	{ 0x06000190, U3COnSendContractU3Ed__0_SetStateMachine_m7A097F8F1BB131B8A785D2F22D1D28D7527646EA_AdjustorThunk },
	{ 0x06000193, U3COnSendTransactionU3Ed__0_MoveNext_mC7CE9060F43BA816093410666933E4EBB204561B_AdjustorThunk },
	{ 0x06000194, U3COnSendTransactionU3Ed__0_SetStateMachine_mC12443C49C60F7DCAA5CDA5E716B32E16BD74723_AdjustorThunk },
	{ 0x06000197, U3COnSignMessageU3Ed__0_MoveNext_m3057F7982AC1CB0EDD2664D8ED6B7E5AC4ADE2A1_AdjustorThunk },
	{ 0x06000198, U3COnSignMessageU3Ed__0_SetStateMachine_m3F0DC2AEF8F20669EFA57A92C67E1F9A5F4B6802_AdjustorThunk },
	{ 0x0600019D, U3CSafeTransferFromU3Ed__5_MoveNext_m64353C3BCC55943924533B82580063995694E676_AdjustorThunk },
	{ 0x0600019E, U3CSafeTransferFromU3Ed__5_SetStateMachine_m357DFCF039FEA8B6C3395C25DFC5753549AD9156_AdjustorThunk },
	{ 0x060001A1, U3CTransferU3Ed__4_MoveNext_m5655C8D900D8FDBA8EF1BB4A1111791D22525BFD_AdjustorThunk },
	{ 0x060001A2, U3CTransferU3Ed__4_SetStateMachine_mA32D0A2CF0FC0E65AD5EA5081D9B5860C23936DE_AdjustorThunk },
	{ 0x060001A5, U3CSafeTransferFromU3Ed__4_MoveNext_m967807132732773C90E46AD493832B9237EA807B_AdjustorThunk },
	{ 0x060001A6, U3CSafeTransferFromU3Ed__4_SetStateMachine_m72420EB4DE07C850B47609F4910E992BFAA0CC10_AdjustorThunk },
	{ 0x060001AA, U3COnLoginU3Ed__2_MoveNext_mC494AC5CF4F7CB0F482C28FCBC80A560EB1EDAFB_AdjustorThunk },
	{ 0x060001AB, U3COnLoginU3Ed__2_SetStateMachine_mB0FF2BA206BB13E75568802ED92D9C2A4609EBC1_AdjustorThunk },
	{ 0x060001B3, U3COnConnectedU3Ed__6_MoveNext_m01EC6E60475A256640C35990CAD5131C04AB747D_AdjustorThunk },
	{ 0x060001B4, U3COnConnectedU3Ed__6_SetStateMachine_mCDC0D60876FA7F6803612569D8BCB444418EC9D9_AdjustorThunk },
};
static const int32_t s_InvokerIndices[469] = 
{
	5398,
	5508,
	5508,
	5210,
	4285,
	5200,
	4273,
	5242,
	3842,
	5398,
	5398,
	5508,
	5508,
	5539,
	5540,
	4453,
	5398,
	5398,
	5398,
	5398,
	5398,
	5508,
	5508,
	5442,
	8132,
	4453,
	4453,
	5398,
	5398,
	5398,
	5398,
	5508,
	5508,
	5442,
	8133,
	4453,
	4603,
	4603,
	4603,
	4603,
	4603,
	4603,
	4603,
	5508,
	5508,
	5508,
	5508,
	4603,
	4603,
	4552,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	5454,
	4505,
	5367,
	4423,
	5442,
	4494,
	1968,
	2581,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5398,
	5508,
	5508,
	5398,
	5508,
	5508,
	5367,
	4423,
	5442,
	4494,
	1968,
	2581,
	4423,
	5508,
	5442,
	5398,
	5508,
	5398,
	4423,
	5508,
	5442,
	5398,
	5508,
	5398,
	5508,
	5508,
	5508,
	5508,
	4453,
	5828,
	5828,
	6049,
	5508,
	8500,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	6049,
	6437,
	6437,
	6437,
	6437,
	5508,
	8500,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	6049,
	6049,
	5828,
	6049,
	5508,
	8500,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	6437,
	7263,
	5746,
	5696,
	6437,
	6776,
	6437,
	6776,
	5825,
	5825,
	6776,
	5828,
	6776,
	5668,
	5657,
	5508,
	8500,
	-1,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	6776,
	8116,
	7263,
	5508,
	5828,
	8116,
	8116,
	5508,
	8500,
	5508,
	4453,
	5508,
	4453,
	5779,
	8462,
	8341,
	6596,
	8462,
	8341,
	8341,
	8462,
	8341,
	8455,
	5746,
	6437,
	8116,
	8455,
	5508,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5398,
	4453,
	5398,
	4453,
	5398,
	4453,
	5398,
	4453,
	5508,
	5508,
	4453,
	5508,
	5508,
	5398,
	4453,
	5398,
	4453,
	5398,
	4453,
	5398,
	4453,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5398,
	4453,
	5398,
	4453,
	5398,
	4453,
	5398,
	4453,
	5508,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	5508,
	4453,
	8500,
	8462,
	8341,
	5508,
	5508,
	5508,
	5508,
	5508,
	4453,
	8171,
	7670,
	8038,
	7660,
	7887,
	7634,
	8209,
	8194,
	7677,
	7675,
	7913,
	7639,
	8181,
	7673,
	8232,
	7682,
	7903,
	7635,
	8141,
	7665,
	8140,
	7664,
	8138,
	7662,
	8139,
	7663,
	7951,
	7643,
	8036,
	7658,
	8037,
	7659,
	8500,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	469,
	s_methodPointers,
	171,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
