﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_mCF70C921A0946D15C8335E566858363678B0BE60 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m6B725FC4546904CD5390EE9E91D83A4F492CFC59 (void);
// 0x00000003 System.Void Mirror.LogEntry::.ctor(System.String,UnityEngine.LogType)
extern void LogEntry__ctor_mC9E2A56B134438A361389D9C84CDC08CA73DED4E (void);
// 0x00000004 System.Void Mirror.GUIConsole::Awake()
extern void GUIConsole_Awake_mC3512AD776538E22BA29428F42F047FD0F81D6CC (void);
// 0x00000005 System.Void Mirror.GUIConsole::OnLog(System.String,System.String,UnityEngine.LogType)
extern void GUIConsole_OnLog_mEC3CA6E893079D280D865D5D64090E483C20125D (void);
// 0x00000006 System.Void Mirror.GUIConsole::Update()
extern void GUIConsole_Update_mCCF3087347BF056734F0129273A59EEB3168B3A4 (void);
// 0x00000007 System.Void Mirror.GUIConsole::OnGUI()
extern void GUIConsole_OnGUI_m8363C2506E3F89CA64380834E100D0FA9C72C82C (void);
// 0x00000008 System.Void Mirror.GUIConsole::.ctor()
extern void GUIConsole__ctor_m59F17DF0C5BC380090C3A84BB8AC20651B652A9B (void);
// 0x00000009 System.Int32 Mirror.DistanceInterestManagement::GetVisRange(Mirror.NetworkIdentity)
extern void DistanceInterestManagement_GetVisRange_m8C64D72C9DBB264918083DA9FCDB627CF9429E09 (void);
// 0x0000000A System.Void Mirror.DistanceInterestManagement::Reset()
extern void DistanceInterestManagement_Reset_m520E795163305F499933F27B4534EE37746DF056 (void);
// 0x0000000B System.Boolean Mirror.DistanceInterestManagement::OnCheckObserver(Mirror.NetworkIdentity,Mirror.NetworkConnectionToClient)
extern void DistanceInterestManagement_OnCheckObserver_mD1F0D26816F67C28111F7E659E518E1A2CFD5663 (void);
// 0x0000000C System.Void Mirror.DistanceInterestManagement::OnRebuildObservers(Mirror.NetworkIdentity,System.Collections.Generic.HashSet`1<Mirror.NetworkConnectionToClient>)
extern void DistanceInterestManagement_OnRebuildObservers_mAEADF75D016A0BAC75E886C3FFF3EAE53CC109CB (void);
// 0x0000000D System.Void Mirror.DistanceInterestManagement::Update()
extern void DistanceInterestManagement_Update_m2A4486D3797D1DAEA9B41E609D885D4A65138619 (void);
// 0x0000000E System.Void Mirror.DistanceInterestManagement::.ctor()
extern void DistanceInterestManagement__ctor_mDAB403E9745E5013D8A8E39CA3D0D34771E2FAC6 (void);
// 0x0000000F System.Void Mirror.DistanceInterestManagementCustomRange::.ctor()
extern void DistanceInterestManagementCustomRange__ctor_m7BF2DD4F90D93ED9F47EEAFDB0A832EC2FCE8709 (void);
// 0x00000010 System.Void Mirror.DistanceInterestManagementCustomRange::MirrorProcessed()
extern void DistanceInterestManagementCustomRange_MirrorProcessed_m4A69716E2C739259611876E414B17AA965A9578E (void);
// 0x00000011 System.Void Mirror.MatchInterestManagement::OnSpawned(Mirror.NetworkIdentity)
extern void MatchInterestManagement_OnSpawned_m9A3EEF4BF9A53DE905E516DE08A0138CB3372210 (void);
// 0x00000012 System.Void Mirror.MatchInterestManagement::OnDestroyed(Mirror.NetworkIdentity)
extern void MatchInterestManagement_OnDestroyed_m6C0F714B9B2AC2969F0D52F955682BB0EB0CEDD9 (void);
// 0x00000013 System.Void Mirror.MatchInterestManagement::Update()
extern void MatchInterestManagement_Update_m5F3535A12C2A5AD0B788CC36A02D5AFE8E814922 (void);
// 0x00000014 System.Void Mirror.MatchInterestManagement::UpdateDirtyMatches(System.Guid,System.Guid)
extern void MatchInterestManagement_UpdateDirtyMatches_m67BE3F0DCDB4173C30500552733E000894A63D73 (void);
// 0x00000015 System.Void Mirror.MatchInterestManagement::UpdateMatchObjects(Mirror.NetworkIdentity,System.Guid,System.Guid)
extern void MatchInterestManagement_UpdateMatchObjects_m9413BEA28FCB2B69342E8F7FB845568B0480A888 (void);
// 0x00000016 System.Void Mirror.MatchInterestManagement::RebuildMatchObservers(System.Guid)
extern void MatchInterestManagement_RebuildMatchObservers_m4AD422E720E68AFEE5E11EFA0672E53295A56432 (void);
// 0x00000017 System.Boolean Mirror.MatchInterestManagement::OnCheckObserver(Mirror.NetworkIdentity,Mirror.NetworkConnectionToClient)
extern void MatchInterestManagement_OnCheckObserver_m4D432E025ECE3F0F57E1E5D3B0D3B0780B29AB39 (void);
// 0x00000018 System.Void Mirror.MatchInterestManagement::OnRebuildObservers(Mirror.NetworkIdentity,System.Collections.Generic.HashSet`1<Mirror.NetworkConnectionToClient>)
extern void MatchInterestManagement_OnRebuildObservers_m71DB0C4195F219D63FBD4FC114B77E59C6AE033F (void);
// 0x00000019 System.Void Mirror.MatchInterestManagement::.ctor()
extern void MatchInterestManagement__ctor_m5AB5177D6CB80B62EA570A307E6D7E8564C4C828 (void);
// 0x0000001A System.Void Mirror.NetworkMatch::.ctor()
extern void NetworkMatch__ctor_m2A0B3808A7E997565F240007F6C9F41D7509E76B (void);
// 0x0000001B System.Void Mirror.NetworkMatch::MirrorProcessed()
extern void NetworkMatch_MirrorProcessed_m2E79D571CFDD5D94D9831DEB549477498974F8D6 (void);
// 0x0000001C System.Void Mirror.SceneInterestManagement::OnSpawned(Mirror.NetworkIdentity)
extern void SceneInterestManagement_OnSpawned_mD4EF12F4DAE6FD105A5D405AED0B02EC362074B3 (void);
// 0x0000001D System.Void Mirror.SceneInterestManagement::OnDestroyed(Mirror.NetworkIdentity)
extern void SceneInterestManagement_OnDestroyed_mC649AC1DF0B41598CB9DDFA73E5E36BF96E311F7 (void);
// 0x0000001E System.Void Mirror.SceneInterestManagement::Update()
extern void SceneInterestManagement_Update_mE66F1C6731393D8244055C3A38A732BB6A664BED (void);
// 0x0000001F System.Void Mirror.SceneInterestManagement::RebuildSceneObservers(UnityEngine.SceneManagement.Scene)
extern void SceneInterestManagement_RebuildSceneObservers_m000D9755F31C2389044C842987AB3F2601636F6B (void);
// 0x00000020 System.Boolean Mirror.SceneInterestManagement::OnCheckObserver(Mirror.NetworkIdentity,Mirror.NetworkConnectionToClient)
extern void SceneInterestManagement_OnCheckObserver_mDCBF21FCCEE54ED1D6100F62D166CE47D2BA9D36 (void);
// 0x00000021 System.Void Mirror.SceneInterestManagement::OnRebuildObservers(Mirror.NetworkIdentity,System.Collections.Generic.HashSet`1<Mirror.NetworkConnectionToClient>)
extern void SceneInterestManagement_OnRebuildObservers_m362A747BFD696424F3F84072F9D83B24164AB7E5 (void);
// 0x00000022 System.Void Mirror.SceneInterestManagement::.ctor()
extern void SceneInterestManagement__ctor_m98D2CCF1F61F1B6CF9718413DCFB74377688D89F (void);
// 0x00000023 System.Void Mirror.Grid2D`1::Add(UnityEngine.Vector2Int,T)
// 0x00000024 System.Void Mirror.Grid2D`1::GetAt(UnityEngine.Vector2Int,System.Collections.Generic.HashSet`1<T>)
// 0x00000025 System.Void Mirror.Grid2D`1::GetWithNeighbours(UnityEngine.Vector2Int,System.Collections.Generic.HashSet`1<T>)
// 0x00000026 System.Void Mirror.Grid2D`1::ClearNonAlloc()
// 0x00000027 System.Void Mirror.Grid2D`1::.ctor()
// 0x00000028 System.Int32 Mirror.SpatialHashingInterestManagement::get_resolution()
extern void SpatialHashingInterestManagement_get_resolution_m3DAA86ADF204445CC688E8298FD729D5B7DCE43A (void);
// 0x00000029 UnityEngine.Vector2Int Mirror.SpatialHashingInterestManagement::ProjectToGrid(UnityEngine.Vector3)
extern void SpatialHashingInterestManagement_ProjectToGrid_m7D9665467C2DA736E8016A4A7C53F5F6B878603D (void);
// 0x0000002A System.Boolean Mirror.SpatialHashingInterestManagement::OnCheckObserver(Mirror.NetworkIdentity,Mirror.NetworkConnectionToClient)
extern void SpatialHashingInterestManagement_OnCheckObserver_m4039670B0C9414C2F312B15A89D32F2CB0B3B73B (void);
// 0x0000002B System.Void Mirror.SpatialHashingInterestManagement::OnRebuildObservers(Mirror.NetworkIdentity,System.Collections.Generic.HashSet`1<Mirror.NetworkConnectionToClient>)
extern void SpatialHashingInterestManagement_OnRebuildObservers_m2481DCC78A638FA4B7AF41369A2F114072C997D6 (void);
// 0x0000002C System.Void Mirror.SpatialHashingInterestManagement::Reset()
extern void SpatialHashingInterestManagement_Reset_mA0B7AA296DF93CD8DD9FAAAC176C7FD4900ED1A0 (void);
// 0x0000002D System.Void Mirror.SpatialHashingInterestManagement::Update()
extern void SpatialHashingInterestManagement_Update_mF12FD87C0F298BB62D219B264571215539C1D0B2 (void);
// 0x0000002E System.Void Mirror.SpatialHashingInterestManagement::.ctor()
extern void SpatialHashingInterestManagement__ctor_mBFA9121DF13DC2BCF632AAE63D5A8303D23E6F05 (void);
// 0x0000002F System.Void Mirror.NetworkTeam::.ctor()
extern void NetworkTeam__ctor_m93420EA6C1A9ED83CB18EC77E4203214C1284E01 (void);
// 0x00000030 System.Void Mirror.NetworkTeam::MirrorProcessed()
extern void NetworkTeam_MirrorProcessed_m64D2E315BC91F0A029AE68A2E607A832FE1E986C (void);
// 0x00000031 System.Void Mirror.TeamInterestManagement::OnSpawned(Mirror.NetworkIdentity)
extern void TeamInterestManagement_OnSpawned_mC5CBDE23BE1414DC815EF112DD1A22FD24A194F0 (void);
// 0x00000032 System.Void Mirror.TeamInterestManagement::OnDestroyed(Mirror.NetworkIdentity)
extern void TeamInterestManagement_OnDestroyed_m203064B2B5EB6B40862EF7AD307B0A59A3BB1112 (void);
// 0x00000033 System.Void Mirror.TeamInterestManagement::Update()
extern void TeamInterestManagement_Update_mB174C7DC62DC1902D46C79291FBD7E846CB300BB (void);
// 0x00000034 System.Void Mirror.TeamInterestManagement::UpdateDirtyTeams(System.String,System.String)
extern void TeamInterestManagement_UpdateDirtyTeams_m7CC0E243217188E24BB1789004F9E7FF3A51EEBC (void);
// 0x00000035 System.Void Mirror.TeamInterestManagement::UpdateTeamObjects(Mirror.NetworkIdentity,System.String,System.String)
extern void TeamInterestManagement_UpdateTeamObjects_mCF6E9114D1F31A80F772E8C8825431830B524AE1 (void);
// 0x00000036 System.Void Mirror.TeamInterestManagement::RebuildTeamObservers(System.String)
extern void TeamInterestManagement_RebuildTeamObservers_m01631C1FCB2FCEE92ED530B30663E6725CB6E9F6 (void);
// 0x00000037 System.Boolean Mirror.TeamInterestManagement::OnCheckObserver(Mirror.NetworkIdentity,Mirror.NetworkConnectionToClient)
extern void TeamInterestManagement_OnCheckObserver_mE380EDA5D8F3FC4B2077284A84D1C294C5240B13 (void);
// 0x00000038 System.Void Mirror.TeamInterestManagement::OnRebuildObservers(Mirror.NetworkIdentity,System.Collections.Generic.HashSet`1<Mirror.NetworkConnectionToClient>)
extern void TeamInterestManagement_OnRebuildObservers_m73816746A4D486E5804BFF12039C7B69C7A5442C (void);
// 0x00000039 System.Void Mirror.TeamInterestManagement::AddAllConnections(System.Collections.Generic.HashSet`1<Mirror.NetworkConnectionToClient>)
extern void TeamInterestManagement_AddAllConnections_mBB5F1ACFF64061B722109A699B882D887CB1C607 (void);
// 0x0000003A System.Void Mirror.TeamInterestManagement::.ctor()
extern void TeamInterestManagement__ctor_mCBCC27DC7739402BB5742CCC5E2A8002AFC5497A (void);
// 0x0000003B System.Boolean Mirror.NetworkAnimator::get_SendMessagesAllowed()
extern void NetworkAnimator_get_SendMessagesAllowed_m964ECF1C735DEA178966292C22B84F6398C0B98F (void);
// 0x0000003C System.Void Mirror.NetworkAnimator::Awake()
extern void NetworkAnimator_Awake_m677508D952C2A6D016D2CC9B3D0C4C5B804B7EA5 (void);
// 0x0000003D System.Void Mirror.NetworkAnimator::FixedUpdate()
extern void NetworkAnimator_FixedUpdate_mEF2F13AA36F88DCC725D1D934E1DAED109E64B38 (void);
// 0x0000003E System.Void Mirror.NetworkAnimator::CheckSpeed()
extern void NetworkAnimator_CheckSpeed_mAEE03EA2F3D6DB4D6C646A4168F1CF9142D19641 (void);
// 0x0000003F System.Void Mirror.NetworkAnimator::OnAnimatorSpeedChanged(System.Single,System.Single)
extern void NetworkAnimator_OnAnimatorSpeedChanged_mF9E56E9BC7CA343BB5C7579F28D3B9B7CD61C499 (void);
// 0x00000040 System.Boolean Mirror.NetworkAnimator::CheckAnimStateChanged(System.Int32&,System.Single&,System.Int32)
extern void NetworkAnimator_CheckAnimStateChanged_mEB8BF8E8B7DCD6F4B5EFADE7D8DEDE11EA76C5A2 (void);
// 0x00000041 System.Void Mirror.NetworkAnimator::CheckSendRate()
extern void NetworkAnimator_CheckSendRate_m6340EB66B080616E954BCE6E42765798DCAA5EEC (void);
// 0x00000042 System.Void Mirror.NetworkAnimator::SendAnimationMessage(System.Int32,System.Single,System.Int32,System.Single,System.Byte[])
extern void NetworkAnimator_SendAnimationMessage_m7D050B46814B4F8E4CDDBB3A067B2D48D3CF7A4A (void);
// 0x00000043 System.Void Mirror.NetworkAnimator::SendAnimationParametersMessage(System.Byte[])
extern void NetworkAnimator_SendAnimationParametersMessage_m79225A1856745632B38F24E83958EB263AA3E02E (void);
// 0x00000044 System.Void Mirror.NetworkAnimator::HandleAnimMsg(System.Int32,System.Single,System.Int32,System.Single,Mirror.NetworkReader)
extern void NetworkAnimator_HandleAnimMsg_m2D9D9A60AB59C446A1C964DABB78BDE351ADBEA4 (void);
// 0x00000045 System.Void Mirror.NetworkAnimator::HandleAnimParamsMsg(Mirror.NetworkReader)
extern void NetworkAnimator_HandleAnimParamsMsg_m3FE187D6116A43AEF16716D13A5202C4A10251EC (void);
// 0x00000046 System.Void Mirror.NetworkAnimator::HandleAnimTriggerMsg(System.Int32)
extern void NetworkAnimator_HandleAnimTriggerMsg_mE54CEFD73C52D8A0F52463D113F773D0E197B5B4 (void);
// 0x00000047 System.Void Mirror.NetworkAnimator::HandleAnimResetTriggerMsg(System.Int32)
extern void NetworkAnimator_HandleAnimResetTriggerMsg_mAE618CAFBF9BE2F6DEE93BFE389164160713DBC8 (void);
// 0x00000048 System.UInt64 Mirror.NetworkAnimator::NextDirtyBits()
extern void NetworkAnimator_NextDirtyBits_m1B07C3A52C136825FC8BCF37C272707BB0AAC9D5 (void);
// 0x00000049 System.Boolean Mirror.NetworkAnimator::WriteParameters(Mirror.NetworkWriter,System.Boolean)
extern void NetworkAnimator_WriteParameters_m52E162D9E1E7F1ED7A491C93E54453536EA3267F (void);
// 0x0000004A System.Void Mirror.NetworkAnimator::ReadParameters(Mirror.NetworkReader)
extern void NetworkAnimator_ReadParameters_mED26E56B940334C6903DB03767B3C9DF6CD6D572 (void);
// 0x0000004B System.Boolean Mirror.NetworkAnimator::OnSerialize(Mirror.NetworkWriter,System.Boolean)
extern void NetworkAnimator_OnSerialize_mE6708560293ADC214A4F35ABED00D5287F31D2EF (void);
// 0x0000004C System.Void Mirror.NetworkAnimator::OnDeserialize(Mirror.NetworkReader,System.Boolean)
extern void NetworkAnimator_OnDeserialize_mCF366C8725A6A881360B535542478CA133370997 (void);
// 0x0000004D System.Void Mirror.NetworkAnimator::SetTrigger(System.String)
extern void NetworkAnimator_SetTrigger_m426C845158C1277D17B8A5BFCC7A4F423ABD4F01 (void);
// 0x0000004E System.Void Mirror.NetworkAnimator::SetTrigger(System.Int32)
extern void NetworkAnimator_SetTrigger_m8AC67305FA899AAAE8199A003F0236946D7EBCE8 (void);
// 0x0000004F System.Void Mirror.NetworkAnimator::ResetTrigger(System.String)
extern void NetworkAnimator_ResetTrigger_m7156F4A817502505144310DE18A0C7466D85A27F (void);
// 0x00000050 System.Void Mirror.NetworkAnimator::ResetTrigger(System.Int32)
extern void NetworkAnimator_ResetTrigger_m8DA6AB9DB3DCFCD3DECEDF3F47CF17403C33414D (void);
// 0x00000051 System.Void Mirror.NetworkAnimator::CmdOnAnimationServerMessage(System.Int32,System.Single,System.Int32,System.Single,System.Byte[])
extern void NetworkAnimator_CmdOnAnimationServerMessage_mCD660539BA8B55AF5EDFE443191B301FDBEDFCF3 (void);
// 0x00000052 System.Void Mirror.NetworkAnimator::CmdOnAnimationParametersServerMessage(System.Byte[])
extern void NetworkAnimator_CmdOnAnimationParametersServerMessage_m72C13C9EFC55433FA93892A1E7FAA751DEA0435D (void);
// 0x00000053 System.Void Mirror.NetworkAnimator::CmdOnAnimationTriggerServerMessage(System.Int32)
extern void NetworkAnimator_CmdOnAnimationTriggerServerMessage_m1AD5D53C01F19B3F7CBBCB829496D00CC3BE353D (void);
// 0x00000054 System.Void Mirror.NetworkAnimator::CmdOnAnimationResetTriggerServerMessage(System.Int32)
extern void NetworkAnimator_CmdOnAnimationResetTriggerServerMessage_m0870B4B962B33BDBB560DF4BEA57D1053349EF56 (void);
// 0x00000055 System.Void Mirror.NetworkAnimator::CmdSetAnimatorSpeed(System.Single)
extern void NetworkAnimator_CmdSetAnimatorSpeed_mD3D7D2E1E481E8A564D5B0C02128EE13D5BC5D48 (void);
// 0x00000056 System.Void Mirror.NetworkAnimator::RpcOnAnimationClientMessage(System.Int32,System.Single,System.Int32,System.Single,System.Byte[])
extern void NetworkAnimator_RpcOnAnimationClientMessage_m0A972B48BECC1DC7104A79094E58E7766E10CE71 (void);
// 0x00000057 System.Void Mirror.NetworkAnimator::RpcOnAnimationParametersClientMessage(System.Byte[])
extern void NetworkAnimator_RpcOnAnimationParametersClientMessage_m4BFBCBE9F6DE0DC568293A86D550C16444D99F93 (void);
// 0x00000058 System.Void Mirror.NetworkAnimator::RpcOnAnimationTriggerClientMessage(System.Int32)
extern void NetworkAnimator_RpcOnAnimationTriggerClientMessage_mCA9F57296DAB50792850271172EA6AD7B65C130E (void);
// 0x00000059 System.Void Mirror.NetworkAnimator::RpcOnAnimationResetTriggerClientMessage(System.Int32)
extern void NetworkAnimator_RpcOnAnimationResetTriggerClientMessage_mCAD7F4F3ADC3564CC9F4D1246467870437AAA656 (void);
// 0x0000005A System.Void Mirror.NetworkAnimator::.ctor()
extern void NetworkAnimator__ctor_mF6D012A7EA7A2A9522EB0D673EB8336407DF06E6 (void);
// 0x0000005B System.Boolean Mirror.NetworkAnimator::<Awake>b__14_0(UnityEngine.AnimatorControllerParameter)
extern void NetworkAnimator_U3CAwakeU3Eb__14_0_mA3EE2C53734C4DC6639D9C3AC6F4F58D88BE6D82 (void);
// 0x0000005C System.Void Mirror.NetworkAnimator::MirrorProcessed()
extern void NetworkAnimator_MirrorProcessed_mC7214B5522B5B6B4B4BAD953E4C47800C154E51A (void);
// 0x0000005D System.Single Mirror.NetworkAnimator::get_NetworkanimatorSpeed()
extern void NetworkAnimator_get_NetworkanimatorSpeed_m4170C374FE7EA983C5C3AF6B895837B5BD0FE114 (void);
// 0x0000005E System.Void Mirror.NetworkAnimator::set_NetworkanimatorSpeed(System.Single)
extern void NetworkAnimator_set_NetworkanimatorSpeed_mFC3D6DD561E810C70928FF445D3CACC17ED236EC (void);
// 0x0000005F System.Void Mirror.NetworkAnimator::UserCode_CmdOnAnimationServerMessage__Int32__Single__Int32__Single__Byte[](System.Int32,System.Single,System.Int32,System.Single,System.Byte[])
extern void NetworkAnimator_UserCode_CmdOnAnimationServerMessage__Int32__Single__Int32__Single__ByteU5BU5D_m2B2952090C3AF2C2B73A1E77C99D95254A5E0EE4 (void);
// 0x00000060 System.Void Mirror.NetworkAnimator::InvokeUserCode_CmdOnAnimationServerMessage__Int32__Single__Int32__Single__Byte[](Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_CmdOnAnimationServerMessage__Int32__Single__Int32__Single__ByteU5BU5D_m46E89B2EFBCDCFAA33B66AEF197647ACBF4EF2CB (void);
// 0x00000061 System.Void Mirror.NetworkAnimator::UserCode_CmdOnAnimationParametersServerMessage__Byte[](System.Byte[])
extern void NetworkAnimator_UserCode_CmdOnAnimationParametersServerMessage__ByteU5BU5D_mE2173C484361F7F4BAF57FDCCD3AA47401980FAD (void);
// 0x00000062 System.Void Mirror.NetworkAnimator::InvokeUserCode_CmdOnAnimationParametersServerMessage__Byte[](Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_CmdOnAnimationParametersServerMessage__ByteU5BU5D_m0A1D120AA33E3D053B366F397095ACF84CCC14B2 (void);
// 0x00000063 System.Void Mirror.NetworkAnimator::UserCode_CmdOnAnimationTriggerServerMessage__Int32(System.Int32)
extern void NetworkAnimator_UserCode_CmdOnAnimationTriggerServerMessage__Int32_mCB5EF76E2C034FA5996AF5C15B8A5BE0E9071B94 (void);
// 0x00000064 System.Void Mirror.NetworkAnimator::InvokeUserCode_CmdOnAnimationTriggerServerMessage__Int32(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_CmdOnAnimationTriggerServerMessage__Int32_m8AF9D52EEE303712FC7BC492E959039B831D4282 (void);
// 0x00000065 System.Void Mirror.NetworkAnimator::UserCode_CmdOnAnimationResetTriggerServerMessage__Int32(System.Int32)
extern void NetworkAnimator_UserCode_CmdOnAnimationResetTriggerServerMessage__Int32_mF6AFDFD83AA8D34BF4E93398F9B774F6CD258EC7 (void);
// 0x00000066 System.Void Mirror.NetworkAnimator::InvokeUserCode_CmdOnAnimationResetTriggerServerMessage__Int32(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_CmdOnAnimationResetTriggerServerMessage__Int32_m4AC611F4B55C82D2044EE419155F467E3E68897F (void);
// 0x00000067 System.Void Mirror.NetworkAnimator::UserCode_CmdSetAnimatorSpeed__Single(System.Single)
extern void NetworkAnimator_UserCode_CmdSetAnimatorSpeed__Single_m99230D840D9FFEE217D1CC54D34094D9D398C6B0 (void);
// 0x00000068 System.Void Mirror.NetworkAnimator::InvokeUserCode_CmdSetAnimatorSpeed__Single(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_CmdSetAnimatorSpeed__Single_m6ABD8B043A5A3118E510C0853C15FE1E189DBB18 (void);
// 0x00000069 System.Void Mirror.NetworkAnimator::UserCode_RpcOnAnimationClientMessage__Int32__Single__Int32__Single__Byte[](System.Int32,System.Single,System.Int32,System.Single,System.Byte[])
extern void NetworkAnimator_UserCode_RpcOnAnimationClientMessage__Int32__Single__Int32__Single__ByteU5BU5D_m8B3446A1179AAED1E319DA00C7039E3DEFD99FFF (void);
// 0x0000006A System.Void Mirror.NetworkAnimator::InvokeUserCode_RpcOnAnimationClientMessage__Int32__Single__Int32__Single__Byte[](Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_RpcOnAnimationClientMessage__Int32__Single__Int32__Single__ByteU5BU5D_m285B42B47459CD09E4D2DCE092A5B670DE7A7390 (void);
// 0x0000006B System.Void Mirror.NetworkAnimator::UserCode_RpcOnAnimationParametersClientMessage__Byte[](System.Byte[])
extern void NetworkAnimator_UserCode_RpcOnAnimationParametersClientMessage__ByteU5BU5D_mB2815F45CF82CB7EA3051C356D498B4DF46EF46D (void);
// 0x0000006C System.Void Mirror.NetworkAnimator::InvokeUserCode_RpcOnAnimationParametersClientMessage__Byte[](Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_RpcOnAnimationParametersClientMessage__ByteU5BU5D_mE53029AAC2799FBFC02AA50ED8695868EC12946B (void);
// 0x0000006D System.Void Mirror.NetworkAnimator::UserCode_RpcOnAnimationTriggerClientMessage__Int32(System.Int32)
extern void NetworkAnimator_UserCode_RpcOnAnimationTriggerClientMessage__Int32_m2C88F6A5A9BBDBCE3E703C81763E1BF05A4439D2 (void);
// 0x0000006E System.Void Mirror.NetworkAnimator::InvokeUserCode_RpcOnAnimationTriggerClientMessage__Int32(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_RpcOnAnimationTriggerClientMessage__Int32_m282186885080A032AB1E4032EBD9BA69052A1EA7 (void);
// 0x0000006F System.Void Mirror.NetworkAnimator::UserCode_RpcOnAnimationResetTriggerClientMessage__Int32(System.Int32)
extern void NetworkAnimator_UserCode_RpcOnAnimationResetTriggerClientMessage__Int32_m768334F57BE0BB17E137393194D9F0E03FAB6C55 (void);
// 0x00000070 System.Void Mirror.NetworkAnimator::InvokeUserCode_RpcOnAnimationResetTriggerClientMessage__Int32(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkAnimator_InvokeUserCode_RpcOnAnimationResetTriggerClientMessage__Int32_mD37349C3479A1ABF8D95BC83E88B2B44A825E840 (void);
// 0x00000071 System.Void Mirror.NetworkAnimator::.cctor()
extern void NetworkAnimator__cctor_m00A2F56D627238C1D23186A750E932AA22456EE3 (void);
// 0x00000072 System.Boolean Mirror.NetworkAnimator::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void NetworkAnimator_SerializeSyncVars_m4FFCED159DDDE5E7B8ED4D9AADB41E298E4D1215 (void);
// 0x00000073 System.Void Mirror.NetworkAnimator::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void NetworkAnimator_DeserializeSyncVars_m30FB6245BB40297B31A873B6DC407F9C9B8716B7 (void);
// 0x00000074 System.Void Mirror.NetworkLobbyManager::.ctor()
extern void NetworkLobbyManager__ctor_m930836EBC8A4E4A196543EC9220B45D41437AF09 (void);
// 0x00000075 System.Void Mirror.NetworkLobbyPlayer::.ctor()
extern void NetworkLobbyPlayer__ctor_m24793924037B1980584297A6C625E95128056F09 (void);
// 0x00000076 System.Void Mirror.NetworkLobbyPlayer::MirrorProcessed()
extern void NetworkLobbyPlayer_MirrorProcessed_m57CEB5F9DFC844A1111B5805EDA663A721C39618 (void);
// 0x00000077 System.Void Mirror.NetworkPingDisplay::OnGUI()
extern void NetworkPingDisplay_OnGUI_m9FDBB3DD262C74B96DB76ED90480847B614A7768 (void);
// 0x00000078 System.Void Mirror.NetworkPingDisplay::.ctor()
extern void NetworkPingDisplay__ctor_m4BC68047F85CEE02AF3508B59ED315851ECE2E57 (void);
// 0x00000079 System.Boolean Mirror.NetworkRoomManager::get_allPlayersReady()
extern void NetworkRoomManager_get_allPlayersReady_mD968D13421EC4A191DD3488C7C6579CC6A00FECA (void);
// 0x0000007A System.Void Mirror.NetworkRoomManager::set_allPlayersReady(System.Boolean)
extern void NetworkRoomManager_set_allPlayersReady_m7B9101FDD789C144B418DB00553789CA6B9399E8 (void);
// 0x0000007B System.Void Mirror.NetworkRoomManager::OnValidate()
extern void NetworkRoomManager_OnValidate_m49D223390F13DD3B863245431CC82C86E55F6BCB (void);
// 0x0000007C System.Void Mirror.NetworkRoomManager::ReadyStatusChanged()
extern void NetworkRoomManager_ReadyStatusChanged_m24D1A1CD2A4AB0E02C474862634AF697D4B2EAC7 (void);
// 0x0000007D System.Void Mirror.NetworkRoomManager::OnServerReady(Mirror.NetworkConnectionToClient)
extern void NetworkRoomManager_OnServerReady_mF0FBF6FA30D0A47A80C3492CAA44734DAD436084 (void);
// 0x0000007E System.Void Mirror.NetworkRoomManager::SceneLoadedForPlayer(Mirror.NetworkConnectionToClient,UnityEngine.GameObject)
extern void NetworkRoomManager_SceneLoadedForPlayer_m1744023C21797848BB0556309EF7E6D613246920 (void);
// 0x0000007F System.Void Mirror.NetworkRoomManager::CheckReadyToBegin()
extern void NetworkRoomManager_CheckReadyToBegin_m7A8B570D0CE80E4B7AC858D5C104AA3369CEBE7D (void);
// 0x00000080 System.Void Mirror.NetworkRoomManager::CallOnClientEnterRoom()
extern void NetworkRoomManager_CallOnClientEnterRoom_mC90043AEFA95950600770D593D1CDE2170E3A3BA (void);
// 0x00000081 System.Void Mirror.NetworkRoomManager::CallOnClientExitRoom()
extern void NetworkRoomManager_CallOnClientExitRoom_mD38B742576E682C2B10FDBF1FE61BFD7292E0C0A (void);
// 0x00000082 System.Void Mirror.NetworkRoomManager::OnServerConnect(Mirror.NetworkConnectionToClient)
extern void NetworkRoomManager_OnServerConnect_mFAD1F85CAA1C08ED022477AB188013C861B77F94 (void);
// 0x00000083 System.Void Mirror.NetworkRoomManager::OnServerDisconnect(Mirror.NetworkConnectionToClient)
extern void NetworkRoomManager_OnServerDisconnect_m833A11E23501AD03CE82F35738B0E8760E87EE2F (void);
// 0x00000084 System.Void Mirror.NetworkRoomManager::OnServerAddPlayer(Mirror.NetworkConnectionToClient)
extern void NetworkRoomManager_OnServerAddPlayer_m8AA4B4BAA6FC6319A874936853708DAA6097A4AB (void);
// 0x00000085 System.Void Mirror.NetworkRoomManager::RecalculateRoomPlayerIndices()
extern void NetworkRoomManager_RecalculateRoomPlayerIndices_m648E4BE25F086922B49C435C820B00F2BF32789A (void);
// 0x00000086 System.Void Mirror.NetworkRoomManager::ServerChangeScene(System.String)
extern void NetworkRoomManager_ServerChangeScene_mDC140F8508C7FF45545551B561369B5FA81BCE4A (void);
// 0x00000087 System.Void Mirror.NetworkRoomManager::OnServerSceneChanged(System.String)
extern void NetworkRoomManager_OnServerSceneChanged_m27779E3509132F67C7D3751B18EC9D37E60EF45D (void);
// 0x00000088 System.Void Mirror.NetworkRoomManager::OnStartServer()
extern void NetworkRoomManager_OnStartServer_mCBD4BF292B934B9A9E6A8F2AE481D72E8A2508D0 (void);
// 0x00000089 System.Void Mirror.NetworkRoomManager::OnStartHost()
extern void NetworkRoomManager_OnStartHost_m54F84595857AB041C0426BFF8249C5F03B2F9581 (void);
// 0x0000008A System.Void Mirror.NetworkRoomManager::OnStopServer()
extern void NetworkRoomManager_OnStopServer_m6F72A8C1A5F9C8DF7DF8507D0171BC82892D8424 (void);
// 0x0000008B System.Void Mirror.NetworkRoomManager::OnStopHost()
extern void NetworkRoomManager_OnStopHost_m5EA9B754EF26AF3E95D7142BA270C24E1633C167 (void);
// 0x0000008C System.Void Mirror.NetworkRoomManager::OnStartClient()
extern void NetworkRoomManager_OnStartClient_mFB078D386E2A3D62840EEC6F4CC871ACFDFD78C6 (void);
// 0x0000008D System.Void Mirror.NetworkRoomManager::OnClientConnect()
extern void NetworkRoomManager_OnClientConnect_mBDB9C714DC8C68ECA2730A602777316B351992DB (void);
// 0x0000008E System.Void Mirror.NetworkRoomManager::OnClientDisconnect()
extern void NetworkRoomManager_OnClientDisconnect_m7544DB12B854AAA8A35F37DF7643DED02E9411BA (void);
// 0x0000008F System.Void Mirror.NetworkRoomManager::OnStopClient()
extern void NetworkRoomManager_OnStopClient_m87C2EDE99DD9AB3FAB5B65620F9C529F1A8D0019 (void);
// 0x00000090 System.Void Mirror.NetworkRoomManager::OnClientSceneChanged()
extern void NetworkRoomManager_OnClientSceneChanged_mC374E8777726C3528F1660E6A796587EA2DF47D7 (void);
// 0x00000091 System.Void Mirror.NetworkRoomManager::OnRoomStartHost()
extern void NetworkRoomManager_OnRoomStartHost_m471ABF9C1D5AEF8950437D7DDF93DAFF753A2442 (void);
// 0x00000092 System.Void Mirror.NetworkRoomManager::OnRoomStopHost()
extern void NetworkRoomManager_OnRoomStopHost_m3276C52EC3007EEABD526C398C6E667ED6F560E8 (void);
// 0x00000093 System.Void Mirror.NetworkRoomManager::OnRoomStartServer()
extern void NetworkRoomManager_OnRoomStartServer_m9E26CDC06B296ADD5B5D51BAF15B266513DA9FD7 (void);
// 0x00000094 System.Void Mirror.NetworkRoomManager::OnRoomStopServer()
extern void NetworkRoomManager_OnRoomStopServer_m6F350D2B85CF572DA5CEFA4E379DF1FCB13F82FE (void);
// 0x00000095 System.Void Mirror.NetworkRoomManager::OnRoomServerConnect(Mirror.NetworkConnectionToClient)
extern void NetworkRoomManager_OnRoomServerConnect_mBC656BF31B8D209323D6432028E23B8F2CF2D7B5 (void);
// 0x00000096 System.Void Mirror.NetworkRoomManager::OnRoomServerDisconnect(Mirror.NetworkConnectionToClient)
extern void NetworkRoomManager_OnRoomServerDisconnect_mC8B1F3AAE3F8125112C0C08EBFB2B1E5FD2680BA (void);
// 0x00000097 System.Void Mirror.NetworkRoomManager::OnRoomServerSceneChanged(System.String)
extern void NetworkRoomManager_OnRoomServerSceneChanged_m602FD3E32791A3C8484D9FB440C2CF9BE5A86399 (void);
// 0x00000098 UnityEngine.GameObject Mirror.NetworkRoomManager::OnRoomServerCreateRoomPlayer(Mirror.NetworkConnectionToClient)
extern void NetworkRoomManager_OnRoomServerCreateRoomPlayer_mC12E4A1B71D9FC924E3CEF2CC3769B0A42E5B3B2 (void);
// 0x00000099 UnityEngine.GameObject Mirror.NetworkRoomManager::OnRoomServerCreateGamePlayer(Mirror.NetworkConnectionToClient,UnityEngine.GameObject)
extern void NetworkRoomManager_OnRoomServerCreateGamePlayer_m2B6ED32252D6EC5B21A91F1F17DF5DA65E1CDBB2 (void);
// 0x0000009A System.Void Mirror.NetworkRoomManager::OnRoomServerAddPlayer(Mirror.NetworkConnectionToClient)
extern void NetworkRoomManager_OnRoomServerAddPlayer_mB337CD10CC7034F4FB389CB4D99371F3EEC07EF2 (void);
// 0x0000009B System.Boolean Mirror.NetworkRoomManager::OnRoomServerSceneLoadedForPlayer(Mirror.NetworkConnectionToClient,UnityEngine.GameObject,UnityEngine.GameObject)
extern void NetworkRoomManager_OnRoomServerSceneLoadedForPlayer_mB0544E7321ADDECD6A9C1E9E8C7310D0E517F344 (void);
// 0x0000009C System.Void Mirror.NetworkRoomManager::OnRoomServerPlayersReady()
extern void NetworkRoomManager_OnRoomServerPlayersReady_m301090629E3E54FCF0A7129E0FB5B8FD4EFB8400 (void);
// 0x0000009D System.Void Mirror.NetworkRoomManager::OnRoomServerPlayersNotReady()
extern void NetworkRoomManager_OnRoomServerPlayersNotReady_m0A77903DEBFF2FD17554B6731C3F8A057B6965C4 (void);
// 0x0000009E System.Void Mirror.NetworkRoomManager::OnRoomClientEnter()
extern void NetworkRoomManager_OnRoomClientEnter_mA666F419C195924373F22CC2E7D1BCB98AE6783C (void);
// 0x0000009F System.Void Mirror.NetworkRoomManager::OnRoomClientExit()
extern void NetworkRoomManager_OnRoomClientExit_mA87606F861DEA7147D43477F48F1763260033AF5 (void);
// 0x000000A0 System.Void Mirror.NetworkRoomManager::OnRoomClientConnect()
extern void NetworkRoomManager_OnRoomClientConnect_m084BDB0A5597D148B85B2043A0E925DE7D801ABF (void);
// 0x000000A1 System.Void Mirror.NetworkRoomManager::OnRoomClientConnect(Mirror.NetworkConnection)
extern void NetworkRoomManager_OnRoomClientConnect_m02F038F7D53FCF8D4801909EBEE952EFAC799674 (void);
// 0x000000A2 System.Void Mirror.NetworkRoomManager::OnRoomClientDisconnect()
extern void NetworkRoomManager_OnRoomClientDisconnect_mD00E924009526FF7998181BA50E3733894C24DD2 (void);
// 0x000000A3 System.Void Mirror.NetworkRoomManager::OnRoomClientDisconnect(Mirror.NetworkConnection)
extern void NetworkRoomManager_OnRoomClientDisconnect_mB43136B32E21A2225D8A0115DC62799D31BD18B9 (void);
// 0x000000A4 System.Void Mirror.NetworkRoomManager::OnRoomStartClient()
extern void NetworkRoomManager_OnRoomStartClient_m83910A8CE866C07EB8DDEEB084C355FF12BB1D54 (void);
// 0x000000A5 System.Void Mirror.NetworkRoomManager::OnRoomStopClient()
extern void NetworkRoomManager_OnRoomStopClient_mEDB15D3CAF8BAA160A7254456CABA4DF07EC03C6 (void);
// 0x000000A6 System.Void Mirror.NetworkRoomManager::OnRoomClientSceneChanged()
extern void NetworkRoomManager_OnRoomClientSceneChanged_mBE98879A79CBFDC402B0D3505BC308DBBE5AE505 (void);
// 0x000000A7 System.Void Mirror.NetworkRoomManager::OnRoomClientSceneChanged(Mirror.NetworkConnection)
extern void NetworkRoomManager_OnRoomClientSceneChanged_mE33E5CBF03A68019C4D06CC44CEEC4756CCF11C8 (void);
// 0x000000A8 System.Void Mirror.NetworkRoomManager::OnRoomClientAddPlayerFailed()
extern void NetworkRoomManager_OnRoomClientAddPlayerFailed_m66C6C5AC7252C3B2ECD11280C1AF0D464BCB5657 (void);
// 0x000000A9 System.Void Mirror.NetworkRoomManager::OnGUI()
extern void NetworkRoomManager_OnGUI_mAB4CC46F1EE70E719EC87F8D78B05093E07CBE43 (void);
// 0x000000AA System.Void Mirror.NetworkRoomManager::.ctor()
extern void NetworkRoomManager__ctor_mB2779CD15817C861DCE98C9357276B1EE617EEA0 (void);
// 0x000000AB System.Void Mirror.NetworkRoomManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m61D706EDC3DC5098ADF704030938B70B54AB56F5 (void);
// 0x000000AC System.Void Mirror.NetworkRoomManager/<>c::.ctor()
extern void U3CU3Ec__ctor_m8BA39A59192EDE1BC19CDBDFF284981F93E494C5 (void);
// 0x000000AD System.Boolean Mirror.NetworkRoomManager/<>c::<CheckReadyToBegin>b__17_0(System.Collections.Generic.KeyValuePair`2<System.Int32,Mirror.NetworkConnectionToClient>)
extern void U3CU3Ec_U3CCheckReadyToBeginU3Eb__17_0_mB042B4CB069E4B701CD347434FBAE373B5963D41 (void);
// 0x000000AE System.Void Mirror.NetworkRoomPlayer::Start()
extern void NetworkRoomPlayer_Start_mC124766914B4867375031A861228B5BA44895E4D (void);
// 0x000000AF System.Void Mirror.NetworkRoomPlayer::OnDisable()
extern void NetworkRoomPlayer_OnDisable_m926C4290BC835450EB23EBB4F6DEAD9A49A9EF11 (void);
// 0x000000B0 System.Void Mirror.NetworkRoomPlayer::CmdChangeReadyState(System.Boolean)
extern void NetworkRoomPlayer_CmdChangeReadyState_mFBCD5EEDCA6C85A6D644D8ABC5DD4E0A8F9FA487 (void);
// 0x000000B1 System.Void Mirror.NetworkRoomPlayer::IndexChanged(System.Int32,System.Int32)
extern void NetworkRoomPlayer_IndexChanged_m7979AD16E27E760101D2D37BA15AF88BA60E4C19 (void);
// 0x000000B2 System.Void Mirror.NetworkRoomPlayer::ReadyStateChanged(System.Boolean,System.Boolean)
extern void NetworkRoomPlayer_ReadyStateChanged_mA31F96F4132148CDCCDC3FA8F7E3B15C58F9A8FB (void);
// 0x000000B3 System.Void Mirror.NetworkRoomPlayer::OnClientEnterRoom()
extern void NetworkRoomPlayer_OnClientEnterRoom_m88410F5D47F9A97DD31BC3D8F35D3021BC306599 (void);
// 0x000000B4 System.Void Mirror.NetworkRoomPlayer::OnClientExitRoom()
extern void NetworkRoomPlayer_OnClientExitRoom_m2DCCA5D3C23CAE6CBD3B27BE88FE1B41A50D32E6 (void);
// 0x000000B5 System.Void Mirror.NetworkRoomPlayer::OnGUI()
extern void NetworkRoomPlayer_OnGUI_m825D63B075DC80709C0549AAB940DB7EAA31159B (void);
// 0x000000B6 System.Void Mirror.NetworkRoomPlayer::DrawPlayerReadyState()
extern void NetworkRoomPlayer_DrawPlayerReadyState_mDA8EF69FD5BCFB6A0D1789B0EB347AC8181A392A (void);
// 0x000000B7 System.Void Mirror.NetworkRoomPlayer::DrawPlayerReadyButton()
extern void NetworkRoomPlayer_DrawPlayerReadyButton_mFA114A6AB3DFE41E75D689D7ABE2ED34E612A6F4 (void);
// 0x000000B8 System.Void Mirror.NetworkRoomPlayer::.ctor()
extern void NetworkRoomPlayer__ctor_m022684FAC82553240563C9BCA844841F0F089F62 (void);
// 0x000000B9 System.Void Mirror.NetworkRoomPlayer::MirrorProcessed()
extern void NetworkRoomPlayer_MirrorProcessed_m392DE2C7FEF38E16DDBB195DE2F249345CC52350 (void);
// 0x000000BA System.Boolean Mirror.NetworkRoomPlayer::get_NetworkreadyToBegin()
extern void NetworkRoomPlayer_get_NetworkreadyToBegin_m5ABF9099F3D67F4B17BD0F674A1B9BE25C9FA450 (void);
// 0x000000BB System.Void Mirror.NetworkRoomPlayer::set_NetworkreadyToBegin(System.Boolean)
extern void NetworkRoomPlayer_set_NetworkreadyToBegin_m083F404E9F202196540AC3A22F94B76EA827521B (void);
// 0x000000BC System.Int32 Mirror.NetworkRoomPlayer::get_Networkindex()
extern void NetworkRoomPlayer_get_Networkindex_mDFDEAE60EDEE34FB120AAF402BC448FF54943E1D (void);
// 0x000000BD System.Void Mirror.NetworkRoomPlayer::set_Networkindex(System.Int32)
extern void NetworkRoomPlayer_set_Networkindex_mA4D8D7F31782A79E4290EEC5D0996809EA10EA8B (void);
// 0x000000BE System.Void Mirror.NetworkRoomPlayer::UserCode_CmdChangeReadyState__Boolean(System.Boolean)
extern void NetworkRoomPlayer_UserCode_CmdChangeReadyState__Boolean_m4B98725632A48F6A10FCF339FBF847596C3A985B (void);
// 0x000000BF System.Void Mirror.NetworkRoomPlayer::InvokeUserCode_CmdChangeReadyState__Boolean(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRoomPlayer_InvokeUserCode_CmdChangeReadyState__Boolean_m13C283E6C23D2A253BBD770C662DECD075B0F175 (void);
// 0x000000C0 System.Void Mirror.NetworkRoomPlayer::.cctor()
extern void NetworkRoomPlayer__cctor_m34449F6746B525AA326819ADBAF8E2506DC7789C (void);
// 0x000000C1 System.Boolean Mirror.NetworkRoomPlayer::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void NetworkRoomPlayer_SerializeSyncVars_mBEDEE263B0EE24C748B39401FF6D05DB7712325E (void);
// 0x000000C2 System.Void Mirror.NetworkRoomPlayer::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void NetworkRoomPlayer_DeserializeSyncVars_mB9DE91F52EF5B1EBD41382DD3F6B7A97520472D4 (void);
// 0x000000C3 UnityEngine.Transform Mirror.NetworkTransform::get_targetComponent()
extern void NetworkTransform_get_targetComponent_mA2708512C32A48BD6903CD7D09A060E42BA090C1 (void);
// 0x000000C4 System.Void Mirror.NetworkTransform::.ctor()
extern void NetworkTransform__ctor_m23D7AE0BC1AF69C7F72FC9EB55F26FB1E6161A16 (void);
// 0x000000C5 System.Void Mirror.NetworkTransform::MirrorProcessed()
extern void NetworkTransform_MirrorProcessed_mB8CA216EFB9F2CD4A4BD6EFC416C115836F970B0 (void);
// 0x000000C6 System.Boolean Mirror.NetworkTransformBase::get_IsClientWithAuthority()
extern void NetworkTransformBase_get_IsClientWithAuthority_m85C98D27ABF6374823903858E630FE265A66EC7A (void);
// 0x000000C7 UnityEngine.Transform Mirror.NetworkTransformBase::get_targetComponent()
// 0x000000C8 System.Single Mirror.NetworkTransformBase::get_bufferTime()
extern void NetworkTransformBase_get_bufferTime_m3E7D0A461F5D2326F95223E957422F8BCE322081 (void);
// 0x000000C9 Mirror.NTSnapshot Mirror.NetworkTransformBase::ConstructSnapshot()
extern void NetworkTransformBase_ConstructSnapshot_mECC1F60A100AD41E9BD114E4ABBE0CC1A1453FFB (void);
// 0x000000CA System.Void Mirror.NetworkTransformBase::ApplySnapshot(Mirror.NTSnapshot,Mirror.NTSnapshot,Mirror.NTSnapshot)
extern void NetworkTransformBase_ApplySnapshot_m961DDF9BF22074B12CF77D61CD0AA4796BDD5DF6 (void);
// 0x000000CB System.Boolean Mirror.NetworkTransformBase::CompareSnapshots(Mirror.NTSnapshot)
extern void NetworkTransformBase_CompareSnapshots_m02E52751A42D24EA1C253AB7692533BF82DB545C (void);
// 0x000000CC System.Void Mirror.NetworkTransformBase::CmdClientToServerSync(System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Vector3>)
extern void NetworkTransformBase_CmdClientToServerSync_mEB52452C71BBBBADE695073EEEA3E945B4F4DFA8 (void);
// 0x000000CD System.Void Mirror.NetworkTransformBase::OnClientToServerSync(System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Vector3>)
extern void NetworkTransformBase_OnClientToServerSync_m18C44953C7272069B37650FB6C443D0CE8C07935 (void);
// 0x000000CE System.Void Mirror.NetworkTransformBase::RpcServerToClientSync(System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Vector3>)
extern void NetworkTransformBase_RpcServerToClientSync_m750ADC9FDB53B4415D863662020EED3B2D2DA197 (void);
// 0x000000CF System.Void Mirror.NetworkTransformBase::OnServerToClientSync(System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Vector3>)
extern void NetworkTransformBase_OnServerToClientSync_mECAD39BACE8A3FA0DE29C493B788B13575A1F734 (void);
// 0x000000D0 System.Void Mirror.NetworkTransformBase::UpdateServer()
extern void NetworkTransformBase_UpdateServer_m5AC061003CD2305B8387497186879C2C91E39AE8 (void);
// 0x000000D1 System.Void Mirror.NetworkTransformBase::UpdateClient()
extern void NetworkTransformBase_UpdateClient_m1D707403210A7F29B6D3EC8EEC5E652B9EAC481C (void);
// 0x000000D2 System.Void Mirror.NetworkTransformBase::Update()
extern void NetworkTransformBase_Update_m0A78A380CC025A512798A796092FC70418A777A3 (void);
// 0x000000D3 System.Void Mirror.NetworkTransformBase::OnTeleport(UnityEngine.Vector3)
extern void NetworkTransformBase_OnTeleport_m20263DD6B596675F2977355E6FCBC498E817513E (void);
// 0x000000D4 System.Void Mirror.NetworkTransformBase::OnTeleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_OnTeleport_m229266E4DE6C32F8B3F9A13A89944498C132C52B (void);
// 0x000000D5 System.Void Mirror.NetworkTransformBase::RpcTeleport(UnityEngine.Vector3)
extern void NetworkTransformBase_RpcTeleport_mC4C6A867C5107E61BA92EE37EC3107C7AB7B811D (void);
// 0x000000D6 System.Void Mirror.NetworkTransformBase::RpcTeleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_RpcTeleport_mAE663DAB5399B5C6274CF485FCC8313AE1EFE93B (void);
// 0x000000D7 System.Void Mirror.NetworkTransformBase::RpcTeleportAndRotate(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_RpcTeleportAndRotate_m3AB6EB8AD5721FC1B366FADACAFE7B4E873B5905 (void);
// 0x000000D8 System.Void Mirror.NetworkTransformBase::CmdTeleport(UnityEngine.Vector3)
extern void NetworkTransformBase_CmdTeleport_mE61BCDFB7AF924A81FF79E36D4773EA9D3A4BCCE (void);
// 0x000000D9 System.Void Mirror.NetworkTransformBase::CmdTeleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_CmdTeleport_m3990722908F1BF6492E30933B3501484DC130F16 (void);
// 0x000000DA System.Void Mirror.NetworkTransformBase::CmdTeleportAndRotate(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_CmdTeleportAndRotate_m8251E59451B41729DF37DE85B731980E9B571C62 (void);
// 0x000000DB System.Void Mirror.NetworkTransformBase::Reset()
extern void NetworkTransformBase_Reset_m0EDF6BADC312A763C3418C765702C846605AB623 (void);
// 0x000000DC System.Void Mirror.NetworkTransformBase::OnDisable()
extern void NetworkTransformBase_OnDisable_mD817E33288849CF8383AFA3E5B7021DDA9D9DA07 (void);
// 0x000000DD System.Void Mirror.NetworkTransformBase::OnEnable()
extern void NetworkTransformBase_OnEnable_mFBDF72FD2F54C211AD9C24C9C5C8231A220CD087 (void);
// 0x000000DE System.Void Mirror.NetworkTransformBase::OnValidate()
extern void NetworkTransformBase_OnValidate_m8417DC76482840BF92F4A3F0DD317FE83975DEB5 (void);
// 0x000000DF System.Boolean Mirror.NetworkTransformBase::OnSerialize(Mirror.NetworkWriter,System.Boolean)
extern void NetworkTransformBase_OnSerialize_m1FA68B4D0B1EB10F625C49CFB9B8E315819548BF (void);
// 0x000000E0 System.Void Mirror.NetworkTransformBase::OnDeserialize(Mirror.NetworkReader,System.Boolean)
extern void NetworkTransformBase_OnDeserialize_m6CD7C1843A3D4443CC83BAB5C16312608A3F22EA (void);
// 0x000000E1 System.Void Mirror.NetworkTransformBase::.ctor()
extern void NetworkTransformBase__ctor_mD7C6CDB801C61FDB5A492127CCE5EA539F9E57E5 (void);
// 0x000000E2 System.Void Mirror.NetworkTransformBase::MirrorProcessed()
extern void NetworkTransformBase_MirrorProcessed_mEFE193AFD6ADC6BD0832A873EFE52D75919A33E4 (void);
// 0x000000E3 System.Void Mirror.NetworkTransformBase::UserCode_CmdClientToServerSync__Nullable`1__Nullable`1__Nullable`1(System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Vector3>)
extern void NetworkTransformBase_UserCode_CmdClientToServerSync__Nullable_1__Nullable_1__Nullable_1_mD610102B62B8930F1BF9444319C016562004DADF (void);
// 0x000000E4 System.Void Mirror.NetworkTransformBase::InvokeUserCode_CmdClientToServerSync__Nullable`1__Nullable`1__Nullable`1(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_CmdClientToServerSync__Nullable_1__Nullable_1__Nullable_1_mF85896031356E8C5F85FEBDE76D07FF0F99F2F28 (void);
// 0x000000E5 System.Void Mirror.NetworkTransformBase::UserCode_RpcServerToClientSync__Nullable`1__Nullable`1__Nullable`1(System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Vector3>)
extern void NetworkTransformBase_UserCode_RpcServerToClientSync__Nullable_1__Nullable_1__Nullable_1_mF03F75E60F436FDACB44009C8A15AD3BD6E498A6 (void);
// 0x000000E6 System.Void Mirror.NetworkTransformBase::InvokeUserCode_RpcServerToClientSync__Nullable`1__Nullable`1__Nullable`1(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_RpcServerToClientSync__Nullable_1__Nullable_1__Nullable_1_m2CF39E3A8D2058B538E1465355BBEDC9C8EB957F (void);
// 0x000000E7 System.Void Mirror.NetworkTransformBase::UserCode_RpcTeleport__Vector3(UnityEngine.Vector3)
extern void NetworkTransformBase_UserCode_RpcTeleport__Vector3_m4F1EE0AD6A3B50A9B2C42459E484242EC614B56C (void);
// 0x000000E8 System.Void Mirror.NetworkTransformBase::InvokeUserCode_RpcTeleport__Vector3(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_RpcTeleport__Vector3_mCAADBCD80798CD25EDC46682C8D75F9556B3A719 (void);
// 0x000000E9 System.Void Mirror.NetworkTransformBase::UserCode_RpcTeleport__Vector3__Quaternion(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_UserCode_RpcTeleport__Vector3__Quaternion_m1F76DD98C7A239D198B2DC46391DCEEEEB46C3F3 (void);
// 0x000000EA System.Void Mirror.NetworkTransformBase::InvokeUserCode_RpcTeleport__Vector3__Quaternion(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_RpcTeleport__Vector3__Quaternion_m4450D12FBB1C9BA96AB1876A0310F3B3BFA35E90 (void);
// 0x000000EB System.Void Mirror.NetworkTransformBase::UserCode_RpcTeleportAndRotate__Vector3__Quaternion(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_UserCode_RpcTeleportAndRotate__Vector3__Quaternion_mA89EE6ACC422B9538A3BD7FB4373390A8B4777F2 (void);
// 0x000000EC System.Void Mirror.NetworkTransformBase::InvokeUserCode_RpcTeleportAndRotate__Vector3__Quaternion(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_RpcTeleportAndRotate__Vector3__Quaternion_m8D7F575E535CB4529F186DFCA3164FA8277D31B4 (void);
// 0x000000ED System.Void Mirror.NetworkTransformBase::UserCode_CmdTeleport__Vector3(UnityEngine.Vector3)
extern void NetworkTransformBase_UserCode_CmdTeleport__Vector3_mCAC67731E9F6FD7CB0DFE1BE0E0B33D58C031260 (void);
// 0x000000EE System.Void Mirror.NetworkTransformBase::InvokeUserCode_CmdTeleport__Vector3(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_CmdTeleport__Vector3_m2E5D54979B99ED5AAC6DEC95D0D5E362B9ED5326 (void);
// 0x000000EF System.Void Mirror.NetworkTransformBase::UserCode_CmdTeleport__Vector3__Quaternion(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_UserCode_CmdTeleport__Vector3__Quaternion_mB1AAAC70E826CB10A8C01506CE3A9BBFB636F3E5 (void);
// 0x000000F0 System.Void Mirror.NetworkTransformBase::InvokeUserCode_CmdTeleport__Vector3__Quaternion(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_CmdTeleport__Vector3__Quaternion_mE29B354A75ACF51631CDF57D623FE1DFADD90A5E (void);
// 0x000000F1 System.Void Mirror.NetworkTransformBase::UserCode_CmdTeleportAndRotate__Vector3__Quaternion(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_UserCode_CmdTeleportAndRotate__Vector3__Quaternion_m9AA9396E7342FEA4EA3AFCD1AB56E3A2B8D6BED5 (void);
// 0x000000F2 System.Void Mirror.NetworkTransformBase::InvokeUserCode_CmdTeleportAndRotate__Vector3__Quaternion(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_CmdTeleportAndRotate__Vector3__Quaternion_m7574DACC6AAFCD974F97D1539D00510E84979C7C (void);
// 0x000000F3 System.Void Mirror.NetworkTransformBase::.cctor()
extern void NetworkTransformBase__cctor_mE01354D62DD9ED716C953F135D1B44F1A56C21D4 (void);
// 0x000000F4 UnityEngine.Transform Mirror.NetworkTransformChild::get_targetComponent()
extern void NetworkTransformChild_get_targetComponent_m216A7116DDE37F0558F8B5AA345C6785947B73E8 (void);
// 0x000000F5 System.Void Mirror.NetworkTransformChild::.ctor()
extern void NetworkTransformChild__ctor_m4EEA184777FF3985E28DD252690D046403A00217 (void);
// 0x000000F6 System.Void Mirror.NetworkTransformChild::MirrorProcessed()
extern void NetworkTransformChild_MirrorProcessed_m3C06F1D40BD3B334363BD0708460C9B6F3A94E5A (void);
// 0x000000F7 System.Double Mirror.NTSnapshot::get_remoteTimestamp()
extern void NTSnapshot_get_remoteTimestamp_m84004F6F50E1788176856F60EA4152548B1D1059 (void);
// 0x000000F8 System.Void Mirror.NTSnapshot::set_remoteTimestamp(System.Double)
extern void NTSnapshot_set_remoteTimestamp_m688B88A63C5830FCE38ECFA9C4E6C28EEBF35050 (void);
// 0x000000F9 System.Double Mirror.NTSnapshot::get_localTimestamp()
extern void NTSnapshot_get_localTimestamp_mA92EBFA6E7DD2511FC4E505F1A017930FBC13010 (void);
// 0x000000FA System.Void Mirror.NTSnapshot::set_localTimestamp(System.Double)
extern void NTSnapshot_set_localTimestamp_m0872B0F9AF15F1379212A1F42337FE444D5B487F (void);
// 0x000000FB System.Void Mirror.NTSnapshot::.ctor(System.Double,System.Double,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void NTSnapshot__ctor_mC1D447ECCD119ECE48D2664C9B98ED9D7BD78E48 (void);
// 0x000000FC Mirror.NTSnapshot Mirror.NTSnapshot::Interpolate(Mirror.NTSnapshot,Mirror.NTSnapshot,System.Double)
extern void NTSnapshot_Interpolate_m58F7C7598E26E8FF54232CE54C2EF754A5EFB756 (void);
// 0x000000FD System.Boolean Mirror.Experimental.NetworkLerpRigidbody::get_IgnoreSync()
extern void NetworkLerpRigidbody_get_IgnoreSync_m14CA9768BA48B04B77E1267A6A9D914840834312 (void);
// 0x000000FE System.Boolean Mirror.Experimental.NetworkLerpRigidbody::get_ClientWithAuthority()
extern void NetworkLerpRigidbody_get_ClientWithAuthority_m3AE7699B9A76D7B9FDEB56B499DE22D30EEC788C (void);
// 0x000000FF System.Void Mirror.Experimental.NetworkLerpRigidbody::OnValidate()
extern void NetworkLerpRigidbody_OnValidate_m30B20B406F430FC9B748A2D310DF7F67223CDFB4 (void);
// 0x00000100 System.Void Mirror.Experimental.NetworkLerpRigidbody::Update()
extern void NetworkLerpRigidbody_Update_m696911B7D2A16EC6B93AC5CDDC7C954A75B3AFB9 (void);
// 0x00000101 System.Void Mirror.Experimental.NetworkLerpRigidbody::SyncToClients()
extern void NetworkLerpRigidbody_SyncToClients_mFF5C99EEA8B07EFF8D5B8821E4F27ECED546EACD (void);
// 0x00000102 System.Void Mirror.Experimental.NetworkLerpRigidbody::SendToServer()
extern void NetworkLerpRigidbody_SendToServer_mE9444F0B3B1CA432D601B0C88CDC93121B003C07 (void);
// 0x00000103 System.Void Mirror.Experimental.NetworkLerpRigidbody::CmdSendState(UnityEngine.Vector3,UnityEngine.Vector3)
extern void NetworkLerpRigidbody_CmdSendState_m6DC4D21C9D099952A4F92477EEAEF7FF828608B0 (void);
// 0x00000104 System.Void Mirror.Experimental.NetworkLerpRigidbody::FixedUpdate()
extern void NetworkLerpRigidbody_FixedUpdate_mFC015C2944D746B02B9F8232EB722CB46AF08D67 (void);
// 0x00000105 System.Void Mirror.Experimental.NetworkLerpRigidbody::.ctor()
extern void NetworkLerpRigidbody__ctor_mA9B074AB77CAD36D0D76C1A65688F91B53A02911 (void);
// 0x00000106 System.Void Mirror.Experimental.NetworkLerpRigidbody::MirrorProcessed()
extern void NetworkLerpRigidbody_MirrorProcessed_m8B02C4490E56FB489AEEC26885B82B9200529345 (void);
// 0x00000107 UnityEngine.Vector3 Mirror.Experimental.NetworkLerpRigidbody::get_NetworktargetVelocity()
extern void NetworkLerpRigidbody_get_NetworktargetVelocity_m5305051F55BDFBCD2B114244C2533D524B3C1ADB (void);
// 0x00000108 System.Void Mirror.Experimental.NetworkLerpRigidbody::set_NetworktargetVelocity(UnityEngine.Vector3)
extern void NetworkLerpRigidbody_set_NetworktargetVelocity_m206BC926F2EB85CFAD55DFF1FE913CB835D544BB (void);
// 0x00000109 UnityEngine.Vector3 Mirror.Experimental.NetworkLerpRigidbody::get_NetworktargetPosition()
extern void NetworkLerpRigidbody_get_NetworktargetPosition_m06245F4B9490B61916F8D3706C3B34BEF9FEE580 (void);
// 0x0000010A System.Void Mirror.Experimental.NetworkLerpRigidbody::set_NetworktargetPosition(UnityEngine.Vector3)
extern void NetworkLerpRigidbody_set_NetworktargetPosition_mCC816D3F0B19388221E1F023723810B72FF17CC6 (void);
// 0x0000010B System.Void Mirror.Experimental.NetworkLerpRigidbody::UserCode_CmdSendState__Vector3__Vector3(UnityEngine.Vector3,UnityEngine.Vector3)
extern void NetworkLerpRigidbody_UserCode_CmdSendState__Vector3__Vector3_m47E6C65A799E999C9C6D117395E522BB26656BAE (void);
// 0x0000010C System.Void Mirror.Experimental.NetworkLerpRigidbody::InvokeUserCode_CmdSendState__Vector3__Vector3(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkLerpRigidbody_InvokeUserCode_CmdSendState__Vector3__Vector3_mBF6DD94942E928554C8CA3802D179C2DFE9B2D07 (void);
// 0x0000010D System.Void Mirror.Experimental.NetworkLerpRigidbody::.cctor()
extern void NetworkLerpRigidbody__cctor_m9AEA783BB326597A6A067F61FEDCE71EA1C856F6 (void);
// 0x0000010E System.Boolean Mirror.Experimental.NetworkLerpRigidbody::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void NetworkLerpRigidbody_SerializeSyncVars_mDE1311A42C066B2868A6014B35B0E388F99A4E24 (void);
// 0x0000010F System.Void Mirror.Experimental.NetworkLerpRigidbody::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void NetworkLerpRigidbody_DeserializeSyncVars_m18C676F1848CB062ACCF72808AE932A22499B158 (void);
// 0x00000110 System.Void Mirror.Experimental.NetworkRigidbody::OnValidate()
extern void NetworkRigidbody_OnValidate_m40B71B4F02A9C3447794D2D22013FFE6CBE63B9A (void);
// 0x00000111 System.Boolean Mirror.Experimental.NetworkRigidbody::get_IgnoreSync()
extern void NetworkRigidbody_get_IgnoreSync_mD04E50EBD536D10F375327A2D2541C77C8BE1020 (void);
// 0x00000112 System.Boolean Mirror.Experimental.NetworkRigidbody::get_ClientWithAuthority()
extern void NetworkRigidbody_get_ClientWithAuthority_mAC15E15CA110363B88D160F06EB86964487F4564 (void);
// 0x00000113 System.Void Mirror.Experimental.NetworkRigidbody::OnVelocityChanged(UnityEngine.Vector3,UnityEngine.Vector3)
extern void NetworkRigidbody_OnVelocityChanged_mECEA041EC59A9C87B51E5C180768B00B802EBF94 (void);
// 0x00000114 System.Void Mirror.Experimental.NetworkRigidbody::OnAngularVelocityChanged(UnityEngine.Vector3,UnityEngine.Vector3)
extern void NetworkRigidbody_OnAngularVelocityChanged_mD0CE278A923A793EA84111019C9DE9F4E5D85355 (void);
// 0x00000115 System.Void Mirror.Experimental.NetworkRigidbody::OnIsKinematicChanged(System.Boolean,System.Boolean)
extern void NetworkRigidbody_OnIsKinematicChanged_mA3258FD50BEBFEC65D4BDC3F2E7A15D2702CC13A (void);
// 0x00000116 System.Void Mirror.Experimental.NetworkRigidbody::OnUseGravityChanged(System.Boolean,System.Boolean)
extern void NetworkRigidbody_OnUseGravityChanged_m75BB7FEC9ECCC2D028FD84831A7877AD9DCD5FCE (void);
// 0x00000117 System.Void Mirror.Experimental.NetworkRigidbody::OnuDragChanged(System.Single,System.Single)
extern void NetworkRigidbody_OnuDragChanged_m14202442D0FD99F68BE917482FDEB8466944F6BC (void);
// 0x00000118 System.Void Mirror.Experimental.NetworkRigidbody::OnAngularDragChanged(System.Single,System.Single)
extern void NetworkRigidbody_OnAngularDragChanged_m92D95A47D773C036F71B5F28F6A637DDD2C10A9A (void);
// 0x00000119 System.Void Mirror.Experimental.NetworkRigidbody::Update()
extern void NetworkRigidbody_Update_m41E00C2DD4BC4C6AB2839FB11AEFAF37A54A12D1 (void);
// 0x0000011A System.Void Mirror.Experimental.NetworkRigidbody::FixedUpdate()
extern void NetworkRigidbody_FixedUpdate_mA633EED00083BCDDD38507E4EABFE6116075FB5D (void);
// 0x0000011B System.Void Mirror.Experimental.NetworkRigidbody::SyncToClients()
extern void NetworkRigidbody_SyncToClients_m624B5A1969873B505A76A21F738E640F5B5A0F9B (void);
// 0x0000011C System.Void Mirror.Experimental.NetworkRigidbody::SendToServer()
extern void NetworkRigidbody_SendToServer_mACBA1E99787FD6E558D4F8D485208F38123E49D2 (void);
// 0x0000011D System.Void Mirror.Experimental.NetworkRigidbody::SendVelocity()
extern void NetworkRigidbody_SendVelocity_mE86A133C631CD2BBC0767B2ADAA28B2828304844 (void);
// 0x0000011E System.Void Mirror.Experimental.NetworkRigidbody::SendRigidBodySettings()
extern void NetworkRigidbody_SendRigidBodySettings_mFBC5437125594D31F1D7EA24C50CB980687EC1CA (void);
// 0x0000011F System.Void Mirror.Experimental.NetworkRigidbody::CmdSendVelocity(UnityEngine.Vector3)
extern void NetworkRigidbody_CmdSendVelocity_mF7644E502AE79161CE31F9ED5F235C1335B6EBE8 (void);
// 0x00000120 System.Void Mirror.Experimental.NetworkRigidbody::CmdSendVelocityAndAngular(UnityEngine.Vector3,UnityEngine.Vector3)
extern void NetworkRigidbody_CmdSendVelocityAndAngular_mC17BA6370EA46354F4CF1FDFDC6B83009AB81729 (void);
// 0x00000121 System.Void Mirror.Experimental.NetworkRigidbody::CmdSendIsKinematic(System.Boolean)
extern void NetworkRigidbody_CmdSendIsKinematic_mE1E87D3F8CBF7BB1670AE040695C0AC6C785E0B7 (void);
// 0x00000122 System.Void Mirror.Experimental.NetworkRigidbody::CmdSendUseGravity(System.Boolean)
extern void NetworkRigidbody_CmdSendUseGravity_mA7792A14CEF862C147D57E3AC4BBB0B5F35E37CC (void);
// 0x00000123 System.Void Mirror.Experimental.NetworkRigidbody::CmdSendDrag(System.Single)
extern void NetworkRigidbody_CmdSendDrag_mF9CBC79BD0966744CA3B041C7EC0AB31B75668D6 (void);
// 0x00000124 System.Void Mirror.Experimental.NetworkRigidbody::CmdSendAngularDrag(System.Single)
extern void NetworkRigidbody_CmdSendAngularDrag_m1D8D38192B8E2D60C2AAC74C2741AB4B54080A20 (void);
// 0x00000125 System.Void Mirror.Experimental.NetworkRigidbody::.ctor()
extern void NetworkRigidbody__ctor_m68590B0551893E1FE439288D6743CB42D0653646 (void);
// 0x00000126 System.Void Mirror.Experimental.NetworkRigidbody::MirrorProcessed()
extern void NetworkRigidbody_MirrorProcessed_mBBC3926301A109683E8E313DFC1A049ABAE642F6 (void);
// 0x00000127 UnityEngine.Vector3 Mirror.Experimental.NetworkRigidbody::get_Networkvelocity()
extern void NetworkRigidbody_get_Networkvelocity_m1276832AB6DC2D16572E66E44753D98E8BB6B031 (void);
// 0x00000128 System.Void Mirror.Experimental.NetworkRigidbody::set_Networkvelocity(UnityEngine.Vector3)
extern void NetworkRigidbody_set_Networkvelocity_m6AFEAE1F46B3698F80A4AA90581F4D811657DAE4 (void);
// 0x00000129 UnityEngine.Vector3 Mirror.Experimental.NetworkRigidbody::get_NetworkangularVelocity()
extern void NetworkRigidbody_get_NetworkangularVelocity_m37D304522EC368E3EB07B5E63A1812F98649AA53 (void);
// 0x0000012A System.Void Mirror.Experimental.NetworkRigidbody::set_NetworkangularVelocity(UnityEngine.Vector3)
extern void NetworkRigidbody_set_NetworkangularVelocity_mCCC9446755D03AB4F2319FECE4E15BC4A228A8C4 (void);
// 0x0000012B System.Boolean Mirror.Experimental.NetworkRigidbody::get_NetworkisKinematic()
extern void NetworkRigidbody_get_NetworkisKinematic_m08AA6C0A9504D6ACFA2368D56AE320130338FC9D (void);
// 0x0000012C System.Void Mirror.Experimental.NetworkRigidbody::set_NetworkisKinematic(System.Boolean)
extern void NetworkRigidbody_set_NetworkisKinematic_m37FF2BACE580EAAE5F77B773933D9AF50807E262 (void);
// 0x0000012D System.Boolean Mirror.Experimental.NetworkRigidbody::get_NetworkuseGravity()
extern void NetworkRigidbody_get_NetworkuseGravity_mB9EE895C112215386C64CE2D0CB57EDF55860159 (void);
// 0x0000012E System.Void Mirror.Experimental.NetworkRigidbody::set_NetworkuseGravity(System.Boolean)
extern void NetworkRigidbody_set_NetworkuseGravity_mACA0FBEF181AFF38A289DD202EA24EAE4A642C83 (void);
// 0x0000012F System.Single Mirror.Experimental.NetworkRigidbody::get_Networkdrag()
extern void NetworkRigidbody_get_Networkdrag_m5841CD2C47F070EB5C947E1CA235CA0B1FCBF884 (void);
// 0x00000130 System.Void Mirror.Experimental.NetworkRigidbody::set_Networkdrag(System.Single)
extern void NetworkRigidbody_set_Networkdrag_m9FEF784BD2C3D96435C2A52F55782D16EB3ACB98 (void);
// 0x00000131 System.Single Mirror.Experimental.NetworkRigidbody::get_NetworkangularDrag()
extern void NetworkRigidbody_get_NetworkangularDrag_mF459FFCD27B314D85A7369EDFA0D598F39213113 (void);
// 0x00000132 System.Void Mirror.Experimental.NetworkRigidbody::set_NetworkangularDrag(System.Single)
extern void NetworkRigidbody_set_NetworkangularDrag_mC4D10CF446648EFF2E720370F1F5D6365B031B49 (void);
// 0x00000133 System.Void Mirror.Experimental.NetworkRigidbody::UserCode_CmdSendVelocity__Vector3(UnityEngine.Vector3)
extern void NetworkRigidbody_UserCode_CmdSendVelocity__Vector3_m2504FBC0D69081CF7BAF886FF0BCAE95B5D8092C (void);
// 0x00000134 System.Void Mirror.Experimental.NetworkRigidbody::InvokeUserCode_CmdSendVelocity__Vector3(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody_InvokeUserCode_CmdSendVelocity__Vector3_m96371A52F9D7EE36BF9C7A373CD43159DA184D7B (void);
// 0x00000135 System.Void Mirror.Experimental.NetworkRigidbody::UserCode_CmdSendVelocityAndAngular__Vector3__Vector3(UnityEngine.Vector3,UnityEngine.Vector3)
extern void NetworkRigidbody_UserCode_CmdSendVelocityAndAngular__Vector3__Vector3_m6338D7DD8BA5F91D2ECC811F6E06E1AE894A317D (void);
// 0x00000136 System.Void Mirror.Experimental.NetworkRigidbody::InvokeUserCode_CmdSendVelocityAndAngular__Vector3__Vector3(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody_InvokeUserCode_CmdSendVelocityAndAngular__Vector3__Vector3_m434754402F6BA2DD31FF449210EC418A3BA06FE3 (void);
// 0x00000137 System.Void Mirror.Experimental.NetworkRigidbody::UserCode_CmdSendIsKinematic__Boolean(System.Boolean)
extern void NetworkRigidbody_UserCode_CmdSendIsKinematic__Boolean_m7DCDED83FC305B114ABE7AD3456C543E0D8BF232 (void);
// 0x00000138 System.Void Mirror.Experimental.NetworkRigidbody::InvokeUserCode_CmdSendIsKinematic__Boolean(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody_InvokeUserCode_CmdSendIsKinematic__Boolean_m50A9A61B53AD110B5EE3987F4B30FAE6A1082BA4 (void);
// 0x00000139 System.Void Mirror.Experimental.NetworkRigidbody::UserCode_CmdSendUseGravity__Boolean(System.Boolean)
extern void NetworkRigidbody_UserCode_CmdSendUseGravity__Boolean_m8A2F0C53442EA0702E48C99204A19EC27E1DDCC1 (void);
// 0x0000013A System.Void Mirror.Experimental.NetworkRigidbody::InvokeUserCode_CmdSendUseGravity__Boolean(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody_InvokeUserCode_CmdSendUseGravity__Boolean_m258D1A3ABF24D5FF7AC15A69F89498650DE5EEDE (void);
// 0x0000013B System.Void Mirror.Experimental.NetworkRigidbody::UserCode_CmdSendDrag__Single(System.Single)
extern void NetworkRigidbody_UserCode_CmdSendDrag__Single_m9C6FA4A4E005AFA33E9E46F5EEA85327DE5FBBED (void);
// 0x0000013C System.Void Mirror.Experimental.NetworkRigidbody::InvokeUserCode_CmdSendDrag__Single(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody_InvokeUserCode_CmdSendDrag__Single_m0D1E4D9F6091029AA25B39E1B004ED7A203849BB (void);
// 0x0000013D System.Void Mirror.Experimental.NetworkRigidbody::UserCode_CmdSendAngularDrag__Single(System.Single)
extern void NetworkRigidbody_UserCode_CmdSendAngularDrag__Single_m56E6308CE46D762899AAF8765C8E8280F887A69B (void);
// 0x0000013E System.Void Mirror.Experimental.NetworkRigidbody::InvokeUserCode_CmdSendAngularDrag__Single(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody_InvokeUserCode_CmdSendAngularDrag__Single_m0D3A06DA139F6E78FE99E36907E07ED5AE247A6E (void);
// 0x0000013F System.Void Mirror.Experimental.NetworkRigidbody::.cctor()
extern void NetworkRigidbody__cctor_mA13DEAE6E09138F85B097FBF7DDDB63C01EBCA33 (void);
// 0x00000140 System.Boolean Mirror.Experimental.NetworkRigidbody::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void NetworkRigidbody_SerializeSyncVars_mD32552C3EC9D51170CC83A99E3156A2BA64D82A5 (void);
// 0x00000141 System.Void Mirror.Experimental.NetworkRigidbody::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void NetworkRigidbody_DeserializeSyncVars_m21EED7648E45EF95CAAAD35A6A5C885A16B7CE11 (void);
// 0x00000142 System.Void Mirror.Experimental.NetworkRigidbody/ClientSyncState::.ctor()
extern void ClientSyncState__ctor_mE7E549EE79C27A358936B2A0CA1D57957245CDB4 (void);
// 0x00000143 System.Void Mirror.Experimental.NetworkRigidbody2D::OnValidate()
extern void NetworkRigidbody2D_OnValidate_m1ECC56C6AA8BBC118FC4C690254035ED89111A50 (void);
// 0x00000144 System.Boolean Mirror.Experimental.NetworkRigidbody2D::get_IgnoreSync()
extern void NetworkRigidbody2D_get_IgnoreSync_m004B1391CBE32012519652E2F9C3CFC2F2BEDA6E (void);
// 0x00000145 System.Boolean Mirror.Experimental.NetworkRigidbody2D::get_ClientWithAuthority()
extern void NetworkRigidbody2D_get_ClientWithAuthority_m5D9760AB73F0052032D08DCA562340601034558C (void);
// 0x00000146 System.Void Mirror.Experimental.NetworkRigidbody2D::OnVelocityChanged(UnityEngine.Vector2,UnityEngine.Vector2)
extern void NetworkRigidbody2D_OnVelocityChanged_m2E46E1A9328B450157E23D6C0D37A6F20956C7A6 (void);
// 0x00000147 System.Void Mirror.Experimental.NetworkRigidbody2D::OnAngularVelocityChanged(System.Single,System.Single)
extern void NetworkRigidbody2D_OnAngularVelocityChanged_m81783F017338BCAF5F37D733AFAAFBF78776A106 (void);
// 0x00000148 System.Void Mirror.Experimental.NetworkRigidbody2D::OnIsKinematicChanged(System.Boolean,System.Boolean)
extern void NetworkRigidbody2D_OnIsKinematicChanged_mC588C35A081DE55387657FECB4FD43DADA3D09A4 (void);
// 0x00000149 System.Void Mirror.Experimental.NetworkRigidbody2D::OnGravityScaleChanged(System.Single,System.Single)
extern void NetworkRigidbody2D_OnGravityScaleChanged_mABBF609AAC8430513523BB117A5277505F6CFF18 (void);
// 0x0000014A System.Void Mirror.Experimental.NetworkRigidbody2D::OnuDragChanged(System.Single,System.Single)
extern void NetworkRigidbody2D_OnuDragChanged_mB56B5208B427EEF536E63D33988D22AC3A347936 (void);
// 0x0000014B System.Void Mirror.Experimental.NetworkRigidbody2D::OnAngularDragChanged(System.Single,System.Single)
extern void NetworkRigidbody2D_OnAngularDragChanged_m02430C7F5A59321FFD964E59D99CD50E1CCD3761 (void);
// 0x0000014C System.Void Mirror.Experimental.NetworkRigidbody2D::Update()
extern void NetworkRigidbody2D_Update_m168457634F20035287BDA727CF4724F42BD7C7A8 (void);
// 0x0000014D System.Void Mirror.Experimental.NetworkRigidbody2D::FixedUpdate()
extern void NetworkRigidbody2D_FixedUpdate_mA899AA49A90BEEF31269B249C1AE302DD58173DB (void);
// 0x0000014E System.Void Mirror.Experimental.NetworkRigidbody2D::SyncToClients()
extern void NetworkRigidbody2D_SyncToClients_m834820FB56EB84221CAACCB58F2B92BD6A8E9C6F (void);
// 0x0000014F System.Void Mirror.Experimental.NetworkRigidbody2D::SendToServer()
extern void NetworkRigidbody2D_SendToServer_m0EB09A212CA9BA57B24C2255CC0DA47BD7606BE7 (void);
// 0x00000150 System.Void Mirror.Experimental.NetworkRigidbody2D::SendVelocity()
extern void NetworkRigidbody2D_SendVelocity_m33350DF0364BEFAA368AA118BD86C88B1C5B3689 (void);
// 0x00000151 System.Void Mirror.Experimental.NetworkRigidbody2D::SendRigidBodySettings()
extern void NetworkRigidbody2D_SendRigidBodySettings_mD9C413DB0DB1F3FE9AE14633F9745CCF66986CE1 (void);
// 0x00000152 System.Void Mirror.Experimental.NetworkRigidbody2D::CmdSendVelocity(UnityEngine.Vector2)
extern void NetworkRigidbody2D_CmdSendVelocity_mAD710F5A606D776F2EE3A084065A4A35FFC07119 (void);
// 0x00000153 System.Void Mirror.Experimental.NetworkRigidbody2D::CmdSendVelocityAndAngular(UnityEngine.Vector2,System.Single)
extern void NetworkRigidbody2D_CmdSendVelocityAndAngular_mBCE3ADF90982A68C9A56C1465FCC1AD60973CD5B (void);
// 0x00000154 System.Void Mirror.Experimental.NetworkRigidbody2D::CmdSendIsKinematic(System.Boolean)
extern void NetworkRigidbody2D_CmdSendIsKinematic_m4666FD1B5DE989A50C9D97C8F85BD368B1803131 (void);
// 0x00000155 System.Void Mirror.Experimental.NetworkRigidbody2D::CmdChangeGravityScale(System.Single)
extern void NetworkRigidbody2D_CmdChangeGravityScale_mEBE6E0B54052016BD2112F793FAF7C3C7FD398E0 (void);
// 0x00000156 System.Void Mirror.Experimental.NetworkRigidbody2D::CmdSendDrag(System.Single)
extern void NetworkRigidbody2D_CmdSendDrag_mC7B251CF6B95D8DBBA044AA7ED311EF7803C3D2A (void);
// 0x00000157 System.Void Mirror.Experimental.NetworkRigidbody2D::CmdSendAngularDrag(System.Single)
extern void NetworkRigidbody2D_CmdSendAngularDrag_m65F8FF7828AC313977ED3BB72B4C7E0F23FCD01E (void);
// 0x00000158 System.Void Mirror.Experimental.NetworkRigidbody2D::.ctor()
extern void NetworkRigidbody2D__ctor_mC34216CE64A28AC78CE16578C7950557E712DF42 (void);
// 0x00000159 System.Void Mirror.Experimental.NetworkRigidbody2D::MirrorProcessed()
extern void NetworkRigidbody2D_MirrorProcessed_mC9E160359EDAF7372629371D4CC47BDF06050007 (void);
// 0x0000015A UnityEngine.Vector2 Mirror.Experimental.NetworkRigidbody2D::get_Networkvelocity()
extern void NetworkRigidbody2D_get_Networkvelocity_m6604949BDDADA6F8A096B05B319901252839AC9E (void);
// 0x0000015B System.Void Mirror.Experimental.NetworkRigidbody2D::set_Networkvelocity(UnityEngine.Vector2)
extern void NetworkRigidbody2D_set_Networkvelocity_m6DA882973C958FEF029D3E365C9E5E247E009FC6 (void);
// 0x0000015C System.Single Mirror.Experimental.NetworkRigidbody2D::get_NetworkangularVelocity()
extern void NetworkRigidbody2D_get_NetworkangularVelocity_m29B25B2D939288A11DF056B3EE2F136E3A874F46 (void);
// 0x0000015D System.Void Mirror.Experimental.NetworkRigidbody2D::set_NetworkangularVelocity(System.Single)
extern void NetworkRigidbody2D_set_NetworkangularVelocity_m39E1A26894E0EBCAF312816EBA48EFD2AFD05ED1 (void);
// 0x0000015E System.Boolean Mirror.Experimental.NetworkRigidbody2D::get_NetworkisKinematic()
extern void NetworkRigidbody2D_get_NetworkisKinematic_m865C4FFC009882792397D79B51F5FDF6EDDDCB88 (void);
// 0x0000015F System.Void Mirror.Experimental.NetworkRigidbody2D::set_NetworkisKinematic(System.Boolean)
extern void NetworkRigidbody2D_set_NetworkisKinematic_m00F8F60CB48B86013D7915D3E26DCD9B50EB5A8F (void);
// 0x00000160 System.Single Mirror.Experimental.NetworkRigidbody2D::get_NetworkgravityScale()
extern void NetworkRigidbody2D_get_NetworkgravityScale_m64F7419F54D056D52637344D3036E23AE1AECA6D (void);
// 0x00000161 System.Void Mirror.Experimental.NetworkRigidbody2D::set_NetworkgravityScale(System.Single)
extern void NetworkRigidbody2D_set_NetworkgravityScale_m01BC917023B712E97519FE93FDB26D5D8BE908B0 (void);
// 0x00000162 System.Single Mirror.Experimental.NetworkRigidbody2D::get_Networkdrag()
extern void NetworkRigidbody2D_get_Networkdrag_m40EF2FBEED9D487F067BCEC6693B7C465DD68448 (void);
// 0x00000163 System.Void Mirror.Experimental.NetworkRigidbody2D::set_Networkdrag(System.Single)
extern void NetworkRigidbody2D_set_Networkdrag_m07E5A4AA04852FE886368A007F56D37987E88DDE (void);
// 0x00000164 System.Single Mirror.Experimental.NetworkRigidbody2D::get_NetworkangularDrag()
extern void NetworkRigidbody2D_get_NetworkangularDrag_m486BF042AAC4EE0DBDECEC80FEA24DD5CF9B79FA (void);
// 0x00000165 System.Void Mirror.Experimental.NetworkRigidbody2D::set_NetworkangularDrag(System.Single)
extern void NetworkRigidbody2D_set_NetworkangularDrag_m1F5F5CF8A423DC9BF522EBC12AD90A97949FE24C (void);
// 0x00000166 System.Void Mirror.Experimental.NetworkRigidbody2D::UserCode_CmdSendVelocity__Vector2(UnityEngine.Vector2)
extern void NetworkRigidbody2D_UserCode_CmdSendVelocity__Vector2_m78152BC3FF4CEAF2EC328325CC90B498CDC94331 (void);
// 0x00000167 System.Void Mirror.Experimental.NetworkRigidbody2D::InvokeUserCode_CmdSendVelocity__Vector2(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody2D_InvokeUserCode_CmdSendVelocity__Vector2_m0F4EB3482E4CA25562E03974CB7C2521998CDF59 (void);
// 0x00000168 System.Void Mirror.Experimental.NetworkRigidbody2D::UserCode_CmdSendVelocityAndAngular__Vector2__Single(UnityEngine.Vector2,System.Single)
extern void NetworkRigidbody2D_UserCode_CmdSendVelocityAndAngular__Vector2__Single_m5AADEC6B64222754293AB681CEA0245CED560627 (void);
// 0x00000169 System.Void Mirror.Experimental.NetworkRigidbody2D::InvokeUserCode_CmdSendVelocityAndAngular__Vector2__Single(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody2D_InvokeUserCode_CmdSendVelocityAndAngular__Vector2__Single_m7B7B51027D188DC0E0AC35EA430D9119BB6ABE8D (void);
// 0x0000016A System.Void Mirror.Experimental.NetworkRigidbody2D::UserCode_CmdSendIsKinematic__Boolean(System.Boolean)
extern void NetworkRigidbody2D_UserCode_CmdSendIsKinematic__Boolean_m95AC310DACF64E46DE0181E16BCC252316512A35 (void);
// 0x0000016B System.Void Mirror.Experimental.NetworkRigidbody2D::InvokeUserCode_CmdSendIsKinematic__Boolean(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody2D_InvokeUserCode_CmdSendIsKinematic__Boolean_m197C05A828CD167B7EA5C979EAE79F0AE0FD3AA7 (void);
// 0x0000016C System.Void Mirror.Experimental.NetworkRigidbody2D::UserCode_CmdChangeGravityScale__Single(System.Single)
extern void NetworkRigidbody2D_UserCode_CmdChangeGravityScale__Single_m7AA4B246FCBDEC5938F665B4860ADD523A91D3F3 (void);
// 0x0000016D System.Void Mirror.Experimental.NetworkRigidbody2D::InvokeUserCode_CmdChangeGravityScale__Single(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody2D_InvokeUserCode_CmdChangeGravityScale__Single_mBF61D966111A325B6BF197BBC7F709D0712EFD3D (void);
// 0x0000016E System.Void Mirror.Experimental.NetworkRigidbody2D::UserCode_CmdSendDrag__Single(System.Single)
extern void NetworkRigidbody2D_UserCode_CmdSendDrag__Single_mBFE2FE570DE3B829E5E4761C854D712352123AE8 (void);
// 0x0000016F System.Void Mirror.Experimental.NetworkRigidbody2D::InvokeUserCode_CmdSendDrag__Single(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody2D_InvokeUserCode_CmdSendDrag__Single_mC0AFBD1A7A79CF9C4C6A7F6D9187BDAE672E0F55 (void);
// 0x00000170 System.Void Mirror.Experimental.NetworkRigidbody2D::UserCode_CmdSendAngularDrag__Single(System.Single)
extern void NetworkRigidbody2D_UserCode_CmdSendAngularDrag__Single_m763A18D4CFF682436F703613A610315DC1353519 (void);
// 0x00000171 System.Void Mirror.Experimental.NetworkRigidbody2D::InvokeUserCode_CmdSendAngularDrag__Single(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkRigidbody2D_InvokeUserCode_CmdSendAngularDrag__Single_m0CC726CC47CB0378327000FAFF89CFAB7069185D (void);
// 0x00000172 System.Void Mirror.Experimental.NetworkRigidbody2D::.cctor()
extern void NetworkRigidbody2D__cctor_m3F6E918117CBECEB7425CB6C05EE2B658C6953A9 (void);
// 0x00000173 System.Boolean Mirror.Experimental.NetworkRigidbody2D::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void NetworkRigidbody2D_SerializeSyncVars_m033E882D59C7B36511F9A6111F6100057E9DB7B3 (void);
// 0x00000174 System.Void Mirror.Experimental.NetworkRigidbody2D::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void NetworkRigidbody2D_DeserializeSyncVars_m724AEF58D65825E3B0636FCCD13539CC9ACE0E81 (void);
// 0x00000175 System.Void Mirror.Experimental.NetworkRigidbody2D/ClientSyncState::.ctor()
extern void ClientSyncState__ctor_mED0F6C89D691FDE5EAAE633B579613091108F34A (void);
// 0x00000176 UnityEngine.Transform Mirror.Experimental.NetworkTransform::get_targetTransform()
extern void NetworkTransform_get_targetTransform_mC881691083A745C569A8BA8B07F33058148B0FE5 (void);
// 0x00000177 System.Void Mirror.Experimental.NetworkTransform::.ctor()
extern void NetworkTransform__ctor_mB4AA1104E1900482A1F8A488D0E541437C1054E8 (void);
// 0x00000178 System.Void Mirror.Experimental.NetworkTransform::MirrorProcessed()
extern void NetworkTransform_MirrorProcessed_mC2887596405B426150BA73442FFBDA1EFB38653C (void);
// 0x00000179 UnityEngine.Transform Mirror.Experimental.NetworkTransformBase::get_targetTransform()
// 0x0000017A System.Boolean Mirror.Experimental.NetworkTransformBase::get_IsOwnerWithClientAuthority()
extern void NetworkTransformBase_get_IsOwnerWithClientAuthority_mF4AF1FB4447DA11A7E084011AE96168B3C6AE4E3 (void);
// 0x0000017B System.Void Mirror.Experimental.NetworkTransformBase::FixedUpdate()
extern void NetworkTransformBase_FixedUpdate_mE596F1DA08C1024E780A9037298965360B12FA85 (void);
// 0x0000017C System.Void Mirror.Experimental.NetworkTransformBase::ServerUpdate()
extern void NetworkTransformBase_ServerUpdate_mC67AB2BD5EDD5F5AF3027B0D8991A830D0557D29 (void);
// 0x0000017D System.Void Mirror.Experimental.NetworkTransformBase::ClientAuthorityUpdate()
extern void NetworkTransformBase_ClientAuthorityUpdate_mA9E47C99871609FA001278C09D59494E316AD97D (void);
// 0x0000017E System.Void Mirror.Experimental.NetworkTransformBase::ClientRemoteUpdate()
extern void NetworkTransformBase_ClientRemoteUpdate_m13FD9DA234CF273816B17398380E79BC42CB712F (void);
// 0x0000017F System.Boolean Mirror.Experimental.NetworkTransformBase::HasEitherMovedRotatedScaled()
extern void NetworkTransformBase_HasEitherMovedRotatedScaled_m25AC8AD6F4C0A18C3E7BBB749B9DB551792C61ED (void);
// 0x00000180 System.Boolean Mirror.Experimental.NetworkTransformBase::get_HasMoved()
extern void NetworkTransformBase_get_HasMoved_mA061AFB2FDEC9DED027EA7C3A9CA11034FCEC13F (void);
// 0x00000181 System.Boolean Mirror.Experimental.NetworkTransformBase::get_HasRotated()
extern void NetworkTransformBase_get_HasRotated_m4F9A1B811872B4CD665068677ED381CB198BE810 (void);
// 0x00000182 System.Boolean Mirror.Experimental.NetworkTransformBase::get_HasScaled()
extern void NetworkTransformBase_get_HasScaled_m3210831C922D73AFB5F5FA13D6F8A9A9C310513B (void);
// 0x00000183 System.Boolean Mirror.Experimental.NetworkTransformBase::NeedsTeleport()
extern void NetworkTransformBase_NeedsTeleport_m4999ABAB5041322921782C55FAF74BE09759251E (void);
// 0x00000184 System.Void Mirror.Experimental.NetworkTransformBase::CmdClientToServerSync(UnityEngine.Vector3,System.UInt32,UnityEngine.Vector3)
extern void NetworkTransformBase_CmdClientToServerSync_m4A234A7D5495D36E522B858F4050CC442B55FD4A (void);
// 0x00000185 System.Void Mirror.Experimental.NetworkTransformBase::RpcMove(UnityEngine.Vector3,System.UInt32,UnityEngine.Vector3)
extern void NetworkTransformBase_RpcMove_mC089E102E8C3C1B5C7F316FCF57C7B4486476EE4 (void);
// 0x00000186 System.Void Mirror.Experimental.NetworkTransformBase::SetGoal(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void NetworkTransformBase_SetGoal_mC25E7E30409614EFEE368262A0537524E4B94211 (void);
// 0x00000187 System.Single Mirror.Experimental.NetworkTransformBase::EstimateMovementSpeed(Mirror.Experimental.NetworkTransformBase/DataPoint,Mirror.Experimental.NetworkTransformBase/DataPoint,UnityEngine.Transform,System.Single)
extern void NetworkTransformBase_EstimateMovementSpeed_mF910B9F9B319FBEBD06AC11A27A1919E377018BC (void);
// 0x00000188 System.Void Mirror.Experimental.NetworkTransformBase::ApplyPositionRotationScale(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void NetworkTransformBase_ApplyPositionRotationScale_m37BF4F818B78F7B9A8AFEA603B2B54A5CEF8EA4E (void);
// 0x00000189 UnityEngine.Vector3 Mirror.Experimental.NetworkTransformBase::InterpolatePosition(Mirror.Experimental.NetworkTransformBase/DataPoint,Mirror.Experimental.NetworkTransformBase/DataPoint,UnityEngine.Vector3)
extern void NetworkTransformBase_InterpolatePosition_m0036CB2D94C9BE929025BDC790118135081EDB0B (void);
// 0x0000018A UnityEngine.Quaternion Mirror.Experimental.NetworkTransformBase::InterpolateRotation(Mirror.Experimental.NetworkTransformBase/DataPoint,Mirror.Experimental.NetworkTransformBase/DataPoint,UnityEngine.Quaternion)
extern void NetworkTransformBase_InterpolateRotation_mF2BD9EE260C54BAC30CA8E8F7B1D0C340E9A4AA3 (void);
// 0x0000018B UnityEngine.Vector3 Mirror.Experimental.NetworkTransformBase::InterpolateScale(Mirror.Experimental.NetworkTransformBase/DataPoint,Mirror.Experimental.NetworkTransformBase/DataPoint,UnityEngine.Vector3)
extern void NetworkTransformBase_InterpolateScale_mE7CF32C3C3C82414721490C99C1971877DA04B88 (void);
// 0x0000018C System.Single Mirror.Experimental.NetworkTransformBase::CurrentInterpolationFactor(Mirror.Experimental.NetworkTransformBase/DataPoint,Mirror.Experimental.NetworkTransformBase/DataPoint)
extern void NetworkTransformBase_CurrentInterpolationFactor_m57D042970C468F9859120796244C46E7EB5C939F (void);
// 0x0000018D System.Void Mirror.Experimental.NetworkTransformBase::ServerTeleport(UnityEngine.Vector3)
extern void NetworkTransformBase_ServerTeleport_mEF9ECFA0FBCF2D0448284B511692CB61AF0CCCCE (void);
// 0x0000018E System.Void Mirror.Experimental.NetworkTransformBase::ServerTeleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_ServerTeleport_m4E8F9CA127E8100055D926476AA8C1ACDE46B6B5 (void);
// 0x0000018F System.Void Mirror.Experimental.NetworkTransformBase::DoTeleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void NetworkTransformBase_DoTeleport_m263D5F57F8117BBBBFFFEE420D210B9ECE708021 (void);
// 0x00000190 System.Void Mirror.Experimental.NetworkTransformBase::RpcTeleport(UnityEngine.Vector3,System.UInt32,System.Boolean)
extern void NetworkTransformBase_RpcTeleport_m2E06E42230A147CB253024D7DFBBF3AD357F8F27 (void);
// 0x00000191 System.Void Mirror.Experimental.NetworkTransformBase::CmdTeleportFinished()
extern void NetworkTransformBase_CmdTeleportFinished_m050A5E005BC56812AD1B546651AC723C40BAC482 (void);
// 0x00000192 System.Void Mirror.Experimental.NetworkTransformBase::OnDrawGizmos()
extern void NetworkTransformBase_OnDrawGizmos_m201BD602C906DF0CAB4062EDD55D72F5ECCB04E3 (void);
// 0x00000193 System.Void Mirror.Experimental.NetworkTransformBase::DrawDataPointGizmo(Mirror.Experimental.NetworkTransformBase/DataPoint,UnityEngine.Color)
extern void NetworkTransformBase_DrawDataPointGizmo_mFD807536E36BE9F41548EEE3A54BB47E2DF5D243 (void);
// 0x00000194 System.Void Mirror.Experimental.NetworkTransformBase::DrawLineBetweenDataPoints(Mirror.Experimental.NetworkTransformBase/DataPoint,Mirror.Experimental.NetworkTransformBase/DataPoint,UnityEngine.Color)
extern void NetworkTransformBase_DrawLineBetweenDataPoints_mE3DDE940CB2BE44F00945DEEF485DED039EC6CF1 (void);
// 0x00000195 System.Void Mirror.Experimental.NetworkTransformBase::.ctor()
extern void NetworkTransformBase__ctor_m7F1BEF03D49DB00ED74D85970D5E30A8B14E377E (void);
// 0x00000196 System.Void Mirror.Experimental.NetworkTransformBase::MirrorProcessed()
extern void NetworkTransformBase_MirrorProcessed_m355817808C7A2B0B14201E075702C86BDDD964C6 (void);
// 0x00000197 System.Boolean Mirror.Experimental.NetworkTransformBase::get_NetworkclientAuthority()
extern void NetworkTransformBase_get_NetworkclientAuthority_m9657103DC3F3AE77A3E1A2B8405051D1A9280AAD (void);
// 0x00000198 System.Void Mirror.Experimental.NetworkTransformBase::set_NetworkclientAuthority(System.Boolean)
extern void NetworkTransformBase_set_NetworkclientAuthority_m329724D832B6923D591BC62EF06ADD3DE0CE9BF4 (void);
// 0x00000199 System.Boolean Mirror.Experimental.NetworkTransformBase::get_NetworkexcludeOwnerUpdate()
extern void NetworkTransformBase_get_NetworkexcludeOwnerUpdate_m0BF4BA43B8F9BF3778B24090EE3E466E90B3F790 (void);
// 0x0000019A System.Void Mirror.Experimental.NetworkTransformBase::set_NetworkexcludeOwnerUpdate(System.Boolean)
extern void NetworkTransformBase_set_NetworkexcludeOwnerUpdate_m293E54A64E1605CCF0EAD5AEE835EBD65D5C1355 (void);
// 0x0000019B System.Boolean Mirror.Experimental.NetworkTransformBase::get_NetworksyncPosition()
extern void NetworkTransformBase_get_NetworksyncPosition_m7CB08BAC1947C7E530679774D7CC134291001397 (void);
// 0x0000019C System.Void Mirror.Experimental.NetworkTransformBase::set_NetworksyncPosition(System.Boolean)
extern void NetworkTransformBase_set_NetworksyncPosition_m43212BC29791176F5B832D0256D311E96D608ED1 (void);
// 0x0000019D System.Boolean Mirror.Experimental.NetworkTransformBase::get_NetworksyncRotation()
extern void NetworkTransformBase_get_NetworksyncRotation_m7BD9DAE6B4A679FF60B4749B8EEBE2A23797A418 (void);
// 0x0000019E System.Void Mirror.Experimental.NetworkTransformBase::set_NetworksyncRotation(System.Boolean)
extern void NetworkTransformBase_set_NetworksyncRotation_m50AE877EA7ABEE8E9860568BAFD9E94C08CA1CEE (void);
// 0x0000019F System.Boolean Mirror.Experimental.NetworkTransformBase::get_NetworksyncScale()
extern void NetworkTransformBase_get_NetworksyncScale_m332806A5D48BD3671C71ED9D6D6C9644FBC95A63 (void);
// 0x000001A0 System.Void Mirror.Experimental.NetworkTransformBase::set_NetworksyncScale(System.Boolean)
extern void NetworkTransformBase_set_NetworksyncScale_m22C987C09CBDF8B8EFBD0C0AC52915621ED75ADA (void);
// 0x000001A1 System.Boolean Mirror.Experimental.NetworkTransformBase::get_NetworkinterpolatePosition()
extern void NetworkTransformBase_get_NetworkinterpolatePosition_m9819D816848F20F6B1F4796496912EC5826EA9F1 (void);
// 0x000001A2 System.Void Mirror.Experimental.NetworkTransformBase::set_NetworkinterpolatePosition(System.Boolean)
extern void NetworkTransformBase_set_NetworkinterpolatePosition_mB0A6D4026D0AC9F0BDC3C23A333CB1C7AC49D277 (void);
// 0x000001A3 System.Boolean Mirror.Experimental.NetworkTransformBase::get_NetworkinterpolateRotation()
extern void NetworkTransformBase_get_NetworkinterpolateRotation_mCCE3E9053877A3165C8F66F2B7C540D191AA0473 (void);
// 0x000001A4 System.Void Mirror.Experimental.NetworkTransformBase::set_NetworkinterpolateRotation(System.Boolean)
extern void NetworkTransformBase_set_NetworkinterpolateRotation_m415B821B6054A740529F1F57D5F79764B19FD3E6 (void);
// 0x000001A5 System.Boolean Mirror.Experimental.NetworkTransformBase::get_NetworkinterpolateScale()
extern void NetworkTransformBase_get_NetworkinterpolateScale_m6C81E85950710AD4ADCA563F4A67BCAB89AA2515 (void);
// 0x000001A6 System.Void Mirror.Experimental.NetworkTransformBase::set_NetworkinterpolateScale(System.Boolean)
extern void NetworkTransformBase_set_NetworkinterpolateScale_m325A8BBBB5416AE0AD5A1BD3B2AEF7EB66F192A1 (void);
// 0x000001A7 System.Single Mirror.Experimental.NetworkTransformBase::get_NetworklocalPositionSensitivity()
extern void NetworkTransformBase_get_NetworklocalPositionSensitivity_mB732054AE7D1FC2EB57BFEC50917438EE5F5888E (void);
// 0x000001A8 System.Void Mirror.Experimental.NetworkTransformBase::set_NetworklocalPositionSensitivity(System.Single)
extern void NetworkTransformBase_set_NetworklocalPositionSensitivity_mD0E30BE1A2BB1F78C732AF07B6F548B96B17D3D1 (void);
// 0x000001A9 System.Single Mirror.Experimental.NetworkTransformBase::get_NetworklocalRotationSensitivity()
extern void NetworkTransformBase_get_NetworklocalRotationSensitivity_mC67052F68DE8FB87906A31FF66D9903A0AAB6F7B (void);
// 0x000001AA System.Void Mirror.Experimental.NetworkTransformBase::set_NetworklocalRotationSensitivity(System.Single)
extern void NetworkTransformBase_set_NetworklocalRotationSensitivity_m93017DBC9971A0CF2532C055B1E77B6DCF076290 (void);
// 0x000001AB System.Single Mirror.Experimental.NetworkTransformBase::get_NetworklocalScaleSensitivity()
extern void NetworkTransformBase_get_NetworklocalScaleSensitivity_m79A978CC85991F22D95908A2417A747ACD075050 (void);
// 0x000001AC System.Void Mirror.Experimental.NetworkTransformBase::set_NetworklocalScaleSensitivity(System.Single)
extern void NetworkTransformBase_set_NetworklocalScaleSensitivity_mEC39778F89A07F616CCDC8D8DA673BA0A7FE46E7 (void);
// 0x000001AD System.Void Mirror.Experimental.NetworkTransformBase::UserCode_CmdClientToServerSync__Vector3__UInt32__Vector3(UnityEngine.Vector3,System.UInt32,UnityEngine.Vector3)
extern void NetworkTransformBase_UserCode_CmdClientToServerSync__Vector3__UInt32__Vector3_m6B3CBC07E6BBD2FA67BB803498D9D77C8DF1D667 (void);
// 0x000001AE System.Void Mirror.Experimental.NetworkTransformBase::InvokeUserCode_CmdClientToServerSync__Vector3__UInt32__Vector3(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_CmdClientToServerSync__Vector3__UInt32__Vector3_m38353087C429C28FD201C851FA101BF6148F1731 (void);
// 0x000001AF System.Void Mirror.Experimental.NetworkTransformBase::UserCode_RpcMove__Vector3__UInt32__Vector3(UnityEngine.Vector3,System.UInt32,UnityEngine.Vector3)
extern void NetworkTransformBase_UserCode_RpcMove__Vector3__UInt32__Vector3_m3B1B2B596A04210D4BE04A4D090FEDB888E4BA5F (void);
// 0x000001B0 System.Void Mirror.Experimental.NetworkTransformBase::InvokeUserCode_RpcMove__Vector3__UInt32__Vector3(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_RpcMove__Vector3__UInt32__Vector3_m545822EAC9837C55ACF715DE38CEAE8473A0C769 (void);
// 0x000001B1 System.Void Mirror.Experimental.NetworkTransformBase::UserCode_RpcTeleport__Vector3__UInt32__Boolean(UnityEngine.Vector3,System.UInt32,System.Boolean)
extern void NetworkTransformBase_UserCode_RpcTeleport__Vector3__UInt32__Boolean_mBA38B249F8B595ED0BD57F06677C107D61FD6DC8 (void);
// 0x000001B2 System.Void Mirror.Experimental.NetworkTransformBase::InvokeUserCode_RpcTeleport__Vector3__UInt32__Boolean(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_RpcTeleport__Vector3__UInt32__Boolean_mE89958C01A2AD882B144A1AE65A84617D79478C6 (void);
// 0x000001B3 System.Void Mirror.Experimental.NetworkTransformBase::UserCode_CmdTeleportFinished()
extern void NetworkTransformBase_UserCode_CmdTeleportFinished_m0430E546F2CCA58FF782AD2F02DAD796AE1A4C9B (void);
// 0x000001B4 System.Void Mirror.Experimental.NetworkTransformBase::InvokeUserCode_CmdTeleportFinished(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkTransformBase_InvokeUserCode_CmdTeleportFinished_m26EEA3E6F568579CCB33A52023CBABA036D384B0 (void);
// 0x000001B5 System.Void Mirror.Experimental.NetworkTransformBase::.cctor()
extern void NetworkTransformBase__cctor_mF6F411DFE380188412E290AF41D132A68497C104 (void);
// 0x000001B6 System.Boolean Mirror.Experimental.NetworkTransformBase::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void NetworkTransformBase_SerializeSyncVars_m14AB6A526E35FCDAD98B8826B4194BA380AB2EF3 (void);
// 0x000001B7 System.Void Mirror.Experimental.NetworkTransformBase::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void NetworkTransformBase_DeserializeSyncVars_m8597B9457BE7674B1510C9A39AACC29FC0DF80B5 (void);
// 0x000001B8 System.Boolean Mirror.Experimental.NetworkTransformBase/DataPoint::get_isValid()
extern void DataPoint_get_isValid_m305A8F1E2800210EEF2C12A7532FCF8602E0C020 (void);
// 0x000001B9 UnityEngine.Transform Mirror.Experimental.NetworkTransformChild::get_targetTransform()
extern void NetworkTransformChild_get_targetTransform_mA2551232703D05BBE8BCBA0AA4D93AAF41721C19 (void);
// 0x000001BA System.Void Mirror.Experimental.NetworkTransformChild::.ctor()
extern void NetworkTransformChild__ctor_m251A86F312AD587906D460237502D9DCD3469837 (void);
// 0x000001BB System.Void Mirror.Experimental.NetworkTransformChild::MirrorProcessed()
extern void NetworkTransformChild_MirrorProcessed_mF330A0B9E63FB0D5E6DCB670ED01F3635BE4CA3A (void);
// 0x000001BC System.Void Mirror.Discovery.ServerFoundUnityEvent::.ctor()
extern void ServerFoundUnityEvent__ctor_m98F0934D285A0B199816BC0EF56A9110F7B499A7 (void);
// 0x000001BD System.Int64 Mirror.Discovery.NetworkDiscovery::get_ServerId()
extern void NetworkDiscovery_get_ServerId_m0393F83D7872351E0B51CBD9B006BE12F7DAB0F2 (void);
// 0x000001BE System.Void Mirror.Discovery.NetworkDiscovery::set_ServerId(System.Int64)
extern void NetworkDiscovery_set_ServerId_mCC20225E540A4D49A13E318D99D4315916C9D3EC (void);
// 0x000001BF System.Void Mirror.Discovery.NetworkDiscovery::Start()
extern void NetworkDiscovery_Start_m0BC76A9176B2FFDE299369A1568353EA1112D2FE (void);
// 0x000001C0 Mirror.Discovery.ServerResponse Mirror.Discovery.NetworkDiscovery::ProcessRequest(Mirror.Discovery.ServerRequest,System.Net.IPEndPoint)
extern void NetworkDiscovery_ProcessRequest_m2696E7DB1F2A3B37D35445E47A1159BDA1610804 (void);
// 0x000001C1 Mirror.Discovery.ServerRequest Mirror.Discovery.NetworkDiscovery::GetRequest()
extern void NetworkDiscovery_GetRequest_m5191C16B4967C44C1788414977BE7680B6AD3132 (void);
// 0x000001C2 System.Void Mirror.Discovery.NetworkDiscovery::ProcessResponse(Mirror.Discovery.ServerResponse,System.Net.IPEndPoint)
extern void NetworkDiscovery_ProcessResponse_m56B63DE07F7D1958AC06B0DE2D3526B167717858 (void);
// 0x000001C3 System.Void Mirror.Discovery.NetworkDiscovery::.ctor()
extern void NetworkDiscovery__ctor_mCF2DB5CFEADF8AD55A37AF5A49F1CF8699FC45DF (void);
// 0x000001C4 System.Boolean Mirror.Discovery.NetworkDiscoveryBase`2::get_SupportedOnThisPlatform()
// 0x000001C5 System.Int64 Mirror.Discovery.NetworkDiscoveryBase`2::RandomLong()
// 0x000001C6 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::Start()
// 0x000001C7 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::OnApplicationQuit()
// 0x000001C8 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::OnDisable()
// 0x000001C9 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::OnDestroy()
// 0x000001CA System.Void Mirror.Discovery.NetworkDiscoveryBase`2::Shutdown()
// 0x000001CB System.Void Mirror.Discovery.NetworkDiscoveryBase`2::AdvertiseServer()
// 0x000001CC System.Threading.Tasks.Task Mirror.Discovery.NetworkDiscoveryBase`2::ServerListenAsync()
// 0x000001CD System.Threading.Tasks.Task Mirror.Discovery.NetworkDiscoveryBase`2::ReceiveRequestAsync(System.Net.Sockets.UdpClient)
// 0x000001CE System.Void Mirror.Discovery.NetworkDiscoveryBase`2::ProcessClientRequest(Request,System.Net.IPEndPoint)
// 0x000001CF Response Mirror.Discovery.NetworkDiscoveryBase`2::ProcessRequest(Request,System.Net.IPEndPoint)
// 0x000001D0 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::BeginMulticastLock()
// 0x000001D1 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::EndpMulticastLock()
// 0x000001D2 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::StartDiscovery()
// 0x000001D3 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::StopDiscovery()
// 0x000001D4 System.Threading.Tasks.Task Mirror.Discovery.NetworkDiscoveryBase`2::ClientListenAsync()
// 0x000001D5 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::BroadcastDiscoveryRequest()
// 0x000001D6 Request Mirror.Discovery.NetworkDiscoveryBase`2::GetRequest()
// 0x000001D7 System.Threading.Tasks.Task Mirror.Discovery.NetworkDiscoveryBase`2::ReceiveGameBroadcastAsync(System.Net.Sockets.UdpClient)
// 0x000001D8 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::ProcessResponse(Response,System.Net.IPEndPoint)
// 0x000001D9 System.Void Mirror.Discovery.NetworkDiscoveryBase`2::.ctor()
// 0x000001DA System.Void Mirror.Discovery.NetworkDiscoveryBase`2/<ServerListenAsync>d__15::MoveNext()
// 0x000001DB System.Void Mirror.Discovery.NetworkDiscoveryBase`2/<ServerListenAsync>d__15::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000001DC System.Void Mirror.Discovery.NetworkDiscoveryBase`2/<ReceiveRequestAsync>d__16::MoveNext()
// 0x000001DD System.Void Mirror.Discovery.NetworkDiscoveryBase`2/<ReceiveRequestAsync>d__16::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000001DE System.Void Mirror.Discovery.NetworkDiscoveryBase`2/<ClientListenAsync>d__23::MoveNext()
// 0x000001DF System.Void Mirror.Discovery.NetworkDiscoveryBase`2/<ClientListenAsync>d__23::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000001E0 System.Void Mirror.Discovery.NetworkDiscoveryBase`2/<ReceiveGameBroadcastAsync>d__26::MoveNext()
// 0x000001E1 System.Void Mirror.Discovery.NetworkDiscoveryBase`2/<ReceiveGameBroadcastAsync>d__26::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000001E2 System.Void Mirror.Discovery.NetworkDiscoveryHUD::OnGUI()
extern void NetworkDiscoveryHUD_OnGUI_m8B99F305D5025EEBFA37CC6378421EBB434075C5 (void);
// 0x000001E3 System.Void Mirror.Discovery.NetworkDiscoveryHUD::DrawGUI()
extern void NetworkDiscoveryHUD_DrawGUI_mF090ACD1398D168C60FBDFED821DF47AEEE3230F (void);
// 0x000001E4 System.Void Mirror.Discovery.NetworkDiscoveryHUD::StopButtons()
extern void NetworkDiscoveryHUD_StopButtons_mE0D1594EFFD82FB28D3CAF1080E7C46BEA95DD9F (void);
// 0x000001E5 System.Void Mirror.Discovery.NetworkDiscoveryHUD::Connect(Mirror.Discovery.ServerResponse)
extern void NetworkDiscoveryHUD_Connect_mD5FA85FD6866BA8F180A4A94E37F163EFA9379D6 (void);
// 0x000001E6 System.Void Mirror.Discovery.NetworkDiscoveryHUD::OnDiscoveredServer(Mirror.Discovery.ServerResponse)
extern void NetworkDiscoveryHUD_OnDiscoveredServer_mA1F4D2C18D68A00ECA3980FC66E2239FC269E38A (void);
// 0x000001E7 System.Void Mirror.Discovery.NetworkDiscoveryHUD::.ctor()
extern void NetworkDiscoveryHUD__ctor_mD529124AEEA197386427DFC7013E4481E9FEE668 (void);
// 0x000001E8 System.Net.IPEndPoint Mirror.Discovery.ServerResponse::get_EndPoint()
extern void ServerResponse_get_EndPoint_m6DAD50F1785A1577B00C8DAADB1E2446B934CA0C (void);
// 0x000001E9 System.Void Mirror.Discovery.ServerResponse::set_EndPoint(System.Net.IPEndPoint)
extern void ServerResponse_set_EndPoint_mE2370B1D263800DD50A7463C05FFD93351E9526F (void);
// 0x000001EA Mirror.ReadyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ReadyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ReadyMessage_m048541B81A0BEAA714842B3F8B0F22DCA3E279AD (void);
// 0x000001EB System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ReadyMessage(Mirror.NetworkWriter,Mirror.ReadyMessage)
extern void GeneratedNetworkCode__Write_Mirror_ReadyMessage_m01DC51135AB81BEDB7BED2EC7583182F840A7A09 (void);
// 0x000001EC Mirror.NotReadyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NotReadyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NotReadyMessage_m95725AD95EBC2F4EF2827593C36AF574D365E882 (void);
// 0x000001ED System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NotReadyMessage(Mirror.NetworkWriter,Mirror.NotReadyMessage)
extern void GeneratedNetworkCode__Write_Mirror_NotReadyMessage_mDC8D762F7E74327CDC8E61B81F44966CFB33F007 (void);
// 0x000001EE Mirror.AddPlayerMessage Mirror.GeneratedNetworkCode::_Read_Mirror.AddPlayerMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_AddPlayerMessage_m25FB29225CACDDC16B1DCE207D44A58A6E14965B (void);
// 0x000001EF System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.AddPlayerMessage(Mirror.NetworkWriter,Mirror.AddPlayerMessage)
extern void GeneratedNetworkCode__Write_Mirror_AddPlayerMessage_m35841C4087A1F65F31027390600EA3D20ABD888C (void);
// 0x000001F0 Mirror.SceneMessage Mirror.GeneratedNetworkCode::_Read_Mirror.SceneMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SceneMessage_m5DBBB5CE0FC989769FB20788A1A2939BFF406583 (void);
// 0x000001F1 Mirror.SceneOperation Mirror.GeneratedNetworkCode::_Read_Mirror.SceneOperation(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SceneOperation_mF63BDCDC7391D3FD37A384BDAB94CF80E8AE38BD (void);
// 0x000001F2 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SceneMessage(Mirror.NetworkWriter,Mirror.SceneMessage)
extern void GeneratedNetworkCode__Write_Mirror_SceneMessage_mDC09FE790B84739BD6BB39063A42674B8D20B439 (void);
// 0x000001F3 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SceneOperation(Mirror.NetworkWriter,Mirror.SceneOperation)
extern void GeneratedNetworkCode__Write_Mirror_SceneOperation_m898374C0384EA970E800ABE11A9F3791BFAFD2D2 (void);
// 0x000001F4 Mirror.CommandMessage Mirror.GeneratedNetworkCode::_Read_Mirror.CommandMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_CommandMessage_m9137F757D3CCC1B2724A8FAD5383405026D4E15E (void);
// 0x000001F5 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.CommandMessage(Mirror.NetworkWriter,Mirror.CommandMessage)
extern void GeneratedNetworkCode__Write_Mirror_CommandMessage_mC010353643CA9E4283D1D728CA8B5FBDEC9CFC92 (void);
// 0x000001F6 Mirror.RpcMessage Mirror.GeneratedNetworkCode::_Read_Mirror.RpcMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_RpcMessage_m76904F48A4AE93E3EC3811BC8F7D7222986CBC91 (void);
// 0x000001F7 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.RpcMessage(Mirror.NetworkWriter,Mirror.RpcMessage)
extern void GeneratedNetworkCode__Write_Mirror_RpcMessage_m96E555184BB3884A376BA0E5FE220C0F69997A2A (void);
// 0x000001F8 Mirror.SpawnMessage Mirror.GeneratedNetworkCode::_Read_Mirror.SpawnMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SpawnMessage_mA4DC66896E815468C7F7D1ED0AC1DDD46DCA6BEF (void);
// 0x000001F9 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SpawnMessage(Mirror.NetworkWriter,Mirror.SpawnMessage)
extern void GeneratedNetworkCode__Write_Mirror_SpawnMessage_mCBC229687B354E7CB4C31F44857299BF9AF276B9 (void);
// 0x000001FA Mirror.ChangeOwnerMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ChangeOwnerMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ChangeOwnerMessage_m59235FBB767E6D6787BE24D4305AF42C3F5DD153 (void);
// 0x000001FB System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ChangeOwnerMessage(Mirror.NetworkWriter,Mirror.ChangeOwnerMessage)
extern void GeneratedNetworkCode__Write_Mirror_ChangeOwnerMessage_m6F109C3E92E45BCF85BAC47927A0BF432F65ADD3 (void);
// 0x000001FC Mirror.ObjectSpawnStartedMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectSpawnStartedMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectSpawnStartedMessage_m780E1A3DDDD191BF46EFC0855892B4F4A89693CB (void);
// 0x000001FD System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectSpawnStartedMessage(Mirror.NetworkWriter,Mirror.ObjectSpawnStartedMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectSpawnStartedMessage_m52E2D9BE0E2DF414E34B2E020683E5B5894C52CE (void);
// 0x000001FE Mirror.ObjectSpawnFinishedMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectSpawnFinishedMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectSpawnFinishedMessage_m7A049EC00BF8FF56B643ECD135C828C8B185C00B (void);
// 0x000001FF System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectSpawnFinishedMessage(Mirror.NetworkWriter,Mirror.ObjectSpawnFinishedMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectSpawnFinishedMessage_mAE0290DB02063E7F5176F88D9B089A4E4B073C29 (void);
// 0x00000200 Mirror.ObjectDestroyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectDestroyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectDestroyMessage_m0253F7D176EB837EFE401C21E44861CA9EAA0C9A (void);
// 0x00000201 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectDestroyMessage(Mirror.NetworkWriter,Mirror.ObjectDestroyMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectDestroyMessage_m533403A395951F44DC2902A716CD4350CF6C8A13 (void);
// 0x00000202 Mirror.ObjectHideMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectHideMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectHideMessage_m4FCABC4227993CB9C8DCDA36724031BDC5598575 (void);
// 0x00000203 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectHideMessage(Mirror.NetworkWriter,Mirror.ObjectHideMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectHideMessage_mEFDA269E469770B02ADCBF4BFCBE5E4905D4D9F4 (void);
// 0x00000204 Mirror.EntityStateMessage Mirror.GeneratedNetworkCode::_Read_Mirror.EntityStateMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_EntityStateMessage_m335FEDD5888CDEC91C115AA10A5A9829702E259B (void);
// 0x00000205 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.EntityStateMessage(Mirror.NetworkWriter,Mirror.EntityStateMessage)
extern void GeneratedNetworkCode__Write_Mirror_EntityStateMessage_m2774697DC9F0FEEC721EF000A1721F90385C26ED (void);
// 0x00000206 Mirror.NetworkPingMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NetworkPingMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NetworkPingMessage_m79DF5D2AA749DB6C49943DF2D340EC0F74EA410B (void);
// 0x00000207 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NetworkPingMessage(Mirror.NetworkWriter,Mirror.NetworkPingMessage)
extern void GeneratedNetworkCode__Write_Mirror_NetworkPingMessage_mFA89195274AC46E8EC8F56362EF22E9238363B50 (void);
// 0x00000208 Mirror.NetworkPongMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NetworkPongMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NetworkPongMessage_m8F5A8B068367AE323991AE95DD10ABC62825F3EA (void);
// 0x00000209 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NetworkPongMessage(Mirror.NetworkWriter,Mirror.NetworkPongMessage)
extern void GeneratedNetworkCode__Write_Mirror_NetworkPongMessage_mA43DBF3C08FFB1AEC273A541063BE71BF4D4019B (void);
// 0x0000020A Mirror.Discovery.ServerRequest Mirror.GeneratedNetworkCode::_Read_Mirror.Discovery.ServerRequest(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_Discovery_ServerRequest_m440F755BC5692190A3584036CFC6D6C14BA81C41 (void);
// 0x0000020B System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Discovery.ServerRequest(Mirror.NetworkWriter,Mirror.Discovery.ServerRequest)
extern void GeneratedNetworkCode__Write_Mirror_Discovery_ServerRequest_m7C4ED38FD32CA46F6CF6FEE281405BEEBF470646 (void);
// 0x0000020C Mirror.Discovery.ServerResponse Mirror.GeneratedNetworkCode::_Read_Mirror.Discovery.ServerResponse(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_Discovery_ServerResponse_m54E9E54AF32F787BCA642B8242BD86FC7C5F9968 (void);
// 0x0000020D System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.Discovery.ServerResponse(Mirror.NetworkWriter,Mirror.Discovery.ServerResponse)
extern void GeneratedNetworkCode__Write_Mirror_Discovery_ServerResponse_mC5CA823A98102D4EE3318EA6FC2D9EF989B13133 (void);
// 0x0000020E System.Void Mirror.GeneratedNetworkCode::InitReadWriters()
extern void GeneratedNetworkCode_InitReadWriters_mE5C95525D6231DCC2495AB04E9E14AC26D72BECD (void);
static Il2CppMethodPointer s_methodPointers[526] = 
{
	EmbeddedAttribute__ctor_mCF70C921A0946D15C8335E566858363678B0BE60,
	IsReadOnlyAttribute__ctor_m6B725FC4546904CD5390EE9E91D83A4F492CFC59,
	LogEntry__ctor_mC9E2A56B134438A361389D9C84CDC08CA73DED4E,
	GUIConsole_Awake_mC3512AD776538E22BA29428F42F047FD0F81D6CC,
	GUIConsole_OnLog_mEC3CA6E893079D280D865D5D64090E483C20125D,
	GUIConsole_Update_mCCF3087347BF056734F0129273A59EEB3168B3A4,
	GUIConsole_OnGUI_m8363C2506E3F89CA64380834E100D0FA9C72C82C,
	GUIConsole__ctor_m59F17DF0C5BC380090C3A84BB8AC20651B652A9B,
	DistanceInterestManagement_GetVisRange_m8C64D72C9DBB264918083DA9FCDB627CF9429E09,
	DistanceInterestManagement_Reset_m520E795163305F499933F27B4534EE37746DF056,
	DistanceInterestManagement_OnCheckObserver_mD1F0D26816F67C28111F7E659E518E1A2CFD5663,
	DistanceInterestManagement_OnRebuildObservers_mAEADF75D016A0BAC75E886C3FFF3EAE53CC109CB,
	DistanceInterestManagement_Update_m2A4486D3797D1DAEA9B41E609D885D4A65138619,
	DistanceInterestManagement__ctor_mDAB403E9745E5013D8A8E39CA3D0D34771E2FAC6,
	DistanceInterestManagementCustomRange__ctor_m7BF2DD4F90D93ED9F47EEAFDB0A832EC2FCE8709,
	DistanceInterestManagementCustomRange_MirrorProcessed_m4A69716E2C739259611876E414B17AA965A9578E,
	MatchInterestManagement_OnSpawned_m9A3EEF4BF9A53DE905E516DE08A0138CB3372210,
	MatchInterestManagement_OnDestroyed_m6C0F714B9B2AC2969F0D52F955682BB0EB0CEDD9,
	MatchInterestManagement_Update_m5F3535A12C2A5AD0B788CC36A02D5AFE8E814922,
	MatchInterestManagement_UpdateDirtyMatches_m67BE3F0DCDB4173C30500552733E000894A63D73,
	MatchInterestManagement_UpdateMatchObjects_m9413BEA28FCB2B69342E8F7FB845568B0480A888,
	MatchInterestManagement_RebuildMatchObservers_m4AD422E720E68AFEE5E11EFA0672E53295A56432,
	MatchInterestManagement_OnCheckObserver_m4D432E025ECE3F0F57E1E5D3B0D3B0780B29AB39,
	MatchInterestManagement_OnRebuildObservers_m71DB0C4195F219D63FBD4FC114B77E59C6AE033F,
	MatchInterestManagement__ctor_m5AB5177D6CB80B62EA570A307E6D7E8564C4C828,
	NetworkMatch__ctor_m2A0B3808A7E997565F240007F6C9F41D7509E76B,
	NetworkMatch_MirrorProcessed_m2E79D571CFDD5D94D9831DEB549477498974F8D6,
	SceneInterestManagement_OnSpawned_mD4EF12F4DAE6FD105A5D405AED0B02EC362074B3,
	SceneInterestManagement_OnDestroyed_mC649AC1DF0B41598CB9DDFA73E5E36BF96E311F7,
	SceneInterestManagement_Update_mE66F1C6731393D8244055C3A38A732BB6A664BED,
	SceneInterestManagement_RebuildSceneObservers_m000D9755F31C2389044C842987AB3F2601636F6B,
	SceneInterestManagement_OnCheckObserver_mDCBF21FCCEE54ED1D6100F62D166CE47D2BA9D36,
	SceneInterestManagement_OnRebuildObservers_m362A747BFD696424F3F84072F9D83B24164AB7E5,
	SceneInterestManagement__ctor_m98D2CCF1F61F1B6CF9718413DCFB74377688D89F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SpatialHashingInterestManagement_get_resolution_m3DAA86ADF204445CC688E8298FD729D5B7DCE43A,
	SpatialHashingInterestManagement_ProjectToGrid_m7D9665467C2DA736E8016A4A7C53F5F6B878603D,
	SpatialHashingInterestManagement_OnCheckObserver_m4039670B0C9414C2F312B15A89D32F2CB0B3B73B,
	SpatialHashingInterestManagement_OnRebuildObservers_m2481DCC78A638FA4B7AF41369A2F114072C997D6,
	SpatialHashingInterestManagement_Reset_mA0B7AA296DF93CD8DD9FAAAC176C7FD4900ED1A0,
	SpatialHashingInterestManagement_Update_mF12FD87C0F298BB62D219B264571215539C1D0B2,
	SpatialHashingInterestManagement__ctor_mBFA9121DF13DC2BCF632AAE63D5A8303D23E6F05,
	NetworkTeam__ctor_m93420EA6C1A9ED83CB18EC77E4203214C1284E01,
	NetworkTeam_MirrorProcessed_m64D2E315BC91F0A029AE68A2E607A832FE1E986C,
	TeamInterestManagement_OnSpawned_mC5CBDE23BE1414DC815EF112DD1A22FD24A194F0,
	TeamInterestManagement_OnDestroyed_m203064B2B5EB6B40862EF7AD307B0A59A3BB1112,
	TeamInterestManagement_Update_mB174C7DC62DC1902D46C79291FBD7E846CB300BB,
	TeamInterestManagement_UpdateDirtyTeams_m7CC0E243217188E24BB1789004F9E7FF3A51EEBC,
	TeamInterestManagement_UpdateTeamObjects_mCF6E9114D1F31A80F772E8C8825431830B524AE1,
	TeamInterestManagement_RebuildTeamObservers_m01631C1FCB2FCEE92ED530B30663E6725CB6E9F6,
	TeamInterestManagement_OnCheckObserver_mE380EDA5D8F3FC4B2077284A84D1C294C5240B13,
	TeamInterestManagement_OnRebuildObservers_m73816746A4D486E5804BFF12039C7B69C7A5442C,
	TeamInterestManagement_AddAllConnections_mBB5F1ACFF64061B722109A699B882D887CB1C607,
	TeamInterestManagement__ctor_mCBCC27DC7739402BB5742CCC5E2A8002AFC5497A,
	NetworkAnimator_get_SendMessagesAllowed_m964ECF1C735DEA178966292C22B84F6398C0B98F,
	NetworkAnimator_Awake_m677508D952C2A6D016D2CC9B3D0C4C5B804B7EA5,
	NetworkAnimator_FixedUpdate_mEF2F13AA36F88DCC725D1D934E1DAED109E64B38,
	NetworkAnimator_CheckSpeed_mAEE03EA2F3D6DB4D6C646A4168F1CF9142D19641,
	NetworkAnimator_OnAnimatorSpeedChanged_mF9E56E9BC7CA343BB5C7579F28D3B9B7CD61C499,
	NetworkAnimator_CheckAnimStateChanged_mEB8BF8E8B7DCD6F4B5EFADE7D8DEDE11EA76C5A2,
	NetworkAnimator_CheckSendRate_m6340EB66B080616E954BCE6E42765798DCAA5EEC,
	NetworkAnimator_SendAnimationMessage_m7D050B46814B4F8E4CDDBB3A067B2D48D3CF7A4A,
	NetworkAnimator_SendAnimationParametersMessage_m79225A1856745632B38F24E83958EB263AA3E02E,
	NetworkAnimator_HandleAnimMsg_m2D9D9A60AB59C446A1C964DABB78BDE351ADBEA4,
	NetworkAnimator_HandleAnimParamsMsg_m3FE187D6116A43AEF16716D13A5202C4A10251EC,
	NetworkAnimator_HandleAnimTriggerMsg_mE54CEFD73C52D8A0F52463D113F773D0E197B5B4,
	NetworkAnimator_HandleAnimResetTriggerMsg_mAE618CAFBF9BE2F6DEE93BFE389164160713DBC8,
	NetworkAnimator_NextDirtyBits_m1B07C3A52C136825FC8BCF37C272707BB0AAC9D5,
	NetworkAnimator_WriteParameters_m52E162D9E1E7F1ED7A491C93E54453536EA3267F,
	NetworkAnimator_ReadParameters_mED26E56B940334C6903DB03767B3C9DF6CD6D572,
	NetworkAnimator_OnSerialize_mE6708560293ADC214A4F35ABED00D5287F31D2EF,
	NetworkAnimator_OnDeserialize_mCF366C8725A6A881360B535542478CA133370997,
	NetworkAnimator_SetTrigger_m426C845158C1277D17B8A5BFCC7A4F423ABD4F01,
	NetworkAnimator_SetTrigger_m8AC67305FA899AAAE8199A003F0236946D7EBCE8,
	NetworkAnimator_ResetTrigger_m7156F4A817502505144310DE18A0C7466D85A27F,
	NetworkAnimator_ResetTrigger_m8DA6AB9DB3DCFCD3DECEDF3F47CF17403C33414D,
	NetworkAnimator_CmdOnAnimationServerMessage_mCD660539BA8B55AF5EDFE443191B301FDBEDFCF3,
	NetworkAnimator_CmdOnAnimationParametersServerMessage_m72C13C9EFC55433FA93892A1E7FAA751DEA0435D,
	NetworkAnimator_CmdOnAnimationTriggerServerMessage_m1AD5D53C01F19B3F7CBBCB829496D00CC3BE353D,
	NetworkAnimator_CmdOnAnimationResetTriggerServerMessage_m0870B4B962B33BDBB560DF4BEA57D1053349EF56,
	NetworkAnimator_CmdSetAnimatorSpeed_mD3D7D2E1E481E8A564D5B0C02128EE13D5BC5D48,
	NetworkAnimator_RpcOnAnimationClientMessage_m0A972B48BECC1DC7104A79094E58E7766E10CE71,
	NetworkAnimator_RpcOnAnimationParametersClientMessage_m4BFBCBE9F6DE0DC568293A86D550C16444D99F93,
	NetworkAnimator_RpcOnAnimationTriggerClientMessage_mCA9F57296DAB50792850271172EA6AD7B65C130E,
	NetworkAnimator_RpcOnAnimationResetTriggerClientMessage_mCAD7F4F3ADC3564CC9F4D1246467870437AAA656,
	NetworkAnimator__ctor_mF6D012A7EA7A2A9522EB0D673EB8336407DF06E6,
	NetworkAnimator_U3CAwakeU3Eb__14_0_mA3EE2C53734C4DC6639D9C3AC6F4F58D88BE6D82,
	NetworkAnimator_MirrorProcessed_mC7214B5522B5B6B4B4BAD953E4C47800C154E51A,
	NetworkAnimator_get_NetworkanimatorSpeed_m4170C374FE7EA983C5C3AF6B895837B5BD0FE114,
	NetworkAnimator_set_NetworkanimatorSpeed_mFC3D6DD561E810C70928FF445D3CACC17ED236EC,
	NetworkAnimator_UserCode_CmdOnAnimationServerMessage__Int32__Single__Int32__Single__ByteU5BU5D_m2B2952090C3AF2C2B73A1E77C99D95254A5E0EE4,
	NetworkAnimator_InvokeUserCode_CmdOnAnimationServerMessage__Int32__Single__Int32__Single__ByteU5BU5D_m46E89B2EFBCDCFAA33B66AEF197647ACBF4EF2CB,
	NetworkAnimator_UserCode_CmdOnAnimationParametersServerMessage__ByteU5BU5D_mE2173C484361F7F4BAF57FDCCD3AA47401980FAD,
	NetworkAnimator_InvokeUserCode_CmdOnAnimationParametersServerMessage__ByteU5BU5D_m0A1D120AA33E3D053B366F397095ACF84CCC14B2,
	NetworkAnimator_UserCode_CmdOnAnimationTriggerServerMessage__Int32_mCB5EF76E2C034FA5996AF5C15B8A5BE0E9071B94,
	NetworkAnimator_InvokeUserCode_CmdOnAnimationTriggerServerMessage__Int32_m8AF9D52EEE303712FC7BC492E959039B831D4282,
	NetworkAnimator_UserCode_CmdOnAnimationResetTriggerServerMessage__Int32_mF6AFDFD83AA8D34BF4E93398F9B774F6CD258EC7,
	NetworkAnimator_InvokeUserCode_CmdOnAnimationResetTriggerServerMessage__Int32_m4AC611F4B55C82D2044EE419155F467E3E68897F,
	NetworkAnimator_UserCode_CmdSetAnimatorSpeed__Single_m99230D840D9FFEE217D1CC54D34094D9D398C6B0,
	NetworkAnimator_InvokeUserCode_CmdSetAnimatorSpeed__Single_m6ABD8B043A5A3118E510C0853C15FE1E189DBB18,
	NetworkAnimator_UserCode_RpcOnAnimationClientMessage__Int32__Single__Int32__Single__ByteU5BU5D_m8B3446A1179AAED1E319DA00C7039E3DEFD99FFF,
	NetworkAnimator_InvokeUserCode_RpcOnAnimationClientMessage__Int32__Single__Int32__Single__ByteU5BU5D_m285B42B47459CD09E4D2DCE092A5B670DE7A7390,
	NetworkAnimator_UserCode_RpcOnAnimationParametersClientMessage__ByteU5BU5D_mB2815F45CF82CB7EA3051C356D498B4DF46EF46D,
	NetworkAnimator_InvokeUserCode_RpcOnAnimationParametersClientMessage__ByteU5BU5D_mE53029AAC2799FBFC02AA50ED8695868EC12946B,
	NetworkAnimator_UserCode_RpcOnAnimationTriggerClientMessage__Int32_m2C88F6A5A9BBDBCE3E703C81763E1BF05A4439D2,
	NetworkAnimator_InvokeUserCode_RpcOnAnimationTriggerClientMessage__Int32_m282186885080A032AB1E4032EBD9BA69052A1EA7,
	NetworkAnimator_UserCode_RpcOnAnimationResetTriggerClientMessage__Int32_m768334F57BE0BB17E137393194D9F0E03FAB6C55,
	NetworkAnimator_InvokeUserCode_RpcOnAnimationResetTriggerClientMessage__Int32_mD37349C3479A1ABF8D95BC83E88B2B44A825E840,
	NetworkAnimator__cctor_m00A2F56D627238C1D23186A750E932AA22456EE3,
	NetworkAnimator_SerializeSyncVars_m4FFCED159DDDE5E7B8ED4D9AADB41E298E4D1215,
	NetworkAnimator_DeserializeSyncVars_m30FB6245BB40297B31A873B6DC407F9C9B8716B7,
	NetworkLobbyManager__ctor_m930836EBC8A4E4A196543EC9220B45D41437AF09,
	NetworkLobbyPlayer__ctor_m24793924037B1980584297A6C625E95128056F09,
	NetworkLobbyPlayer_MirrorProcessed_m57CEB5F9DFC844A1111B5805EDA663A721C39618,
	NetworkPingDisplay_OnGUI_m9FDBB3DD262C74B96DB76ED90480847B614A7768,
	NetworkPingDisplay__ctor_m4BC68047F85CEE02AF3508B59ED315851ECE2E57,
	NetworkRoomManager_get_allPlayersReady_mD968D13421EC4A191DD3488C7C6579CC6A00FECA,
	NetworkRoomManager_set_allPlayersReady_m7B9101FDD789C144B418DB00553789CA6B9399E8,
	NetworkRoomManager_OnValidate_m49D223390F13DD3B863245431CC82C86E55F6BCB,
	NetworkRoomManager_ReadyStatusChanged_m24D1A1CD2A4AB0E02C474862634AF697D4B2EAC7,
	NetworkRoomManager_OnServerReady_mF0FBF6FA30D0A47A80C3492CAA44734DAD436084,
	NetworkRoomManager_SceneLoadedForPlayer_m1744023C21797848BB0556309EF7E6D613246920,
	NetworkRoomManager_CheckReadyToBegin_m7A8B570D0CE80E4B7AC858D5C104AA3369CEBE7D,
	NetworkRoomManager_CallOnClientEnterRoom_mC90043AEFA95950600770D593D1CDE2170E3A3BA,
	NetworkRoomManager_CallOnClientExitRoom_mD38B742576E682C2B10FDBF1FE61BFD7292E0C0A,
	NetworkRoomManager_OnServerConnect_mFAD1F85CAA1C08ED022477AB188013C861B77F94,
	NetworkRoomManager_OnServerDisconnect_m833A11E23501AD03CE82F35738B0E8760E87EE2F,
	NetworkRoomManager_OnServerAddPlayer_m8AA4B4BAA6FC6319A874936853708DAA6097A4AB,
	NetworkRoomManager_RecalculateRoomPlayerIndices_m648E4BE25F086922B49C435C820B00F2BF32789A,
	NetworkRoomManager_ServerChangeScene_mDC140F8508C7FF45545551B561369B5FA81BCE4A,
	NetworkRoomManager_OnServerSceneChanged_m27779E3509132F67C7D3751B18EC9D37E60EF45D,
	NetworkRoomManager_OnStartServer_mCBD4BF292B934B9A9E6A8F2AE481D72E8A2508D0,
	NetworkRoomManager_OnStartHost_m54F84595857AB041C0426BFF8249C5F03B2F9581,
	NetworkRoomManager_OnStopServer_m6F72A8C1A5F9C8DF7DF8507D0171BC82892D8424,
	NetworkRoomManager_OnStopHost_m5EA9B754EF26AF3E95D7142BA270C24E1633C167,
	NetworkRoomManager_OnStartClient_mFB078D386E2A3D62840EEC6F4CC871ACFDFD78C6,
	NetworkRoomManager_OnClientConnect_mBDB9C714DC8C68ECA2730A602777316B351992DB,
	NetworkRoomManager_OnClientDisconnect_m7544DB12B854AAA8A35F37DF7643DED02E9411BA,
	NetworkRoomManager_OnStopClient_m87C2EDE99DD9AB3FAB5B65620F9C529F1A8D0019,
	NetworkRoomManager_OnClientSceneChanged_mC374E8777726C3528F1660E6A796587EA2DF47D7,
	NetworkRoomManager_OnRoomStartHost_m471ABF9C1D5AEF8950437D7DDF93DAFF753A2442,
	NetworkRoomManager_OnRoomStopHost_m3276C52EC3007EEABD526C398C6E667ED6F560E8,
	NetworkRoomManager_OnRoomStartServer_m9E26CDC06B296ADD5B5D51BAF15B266513DA9FD7,
	NetworkRoomManager_OnRoomStopServer_m6F350D2B85CF572DA5CEFA4E379DF1FCB13F82FE,
	NetworkRoomManager_OnRoomServerConnect_mBC656BF31B8D209323D6432028E23B8F2CF2D7B5,
	NetworkRoomManager_OnRoomServerDisconnect_mC8B1F3AAE3F8125112C0C08EBFB2B1E5FD2680BA,
	NetworkRoomManager_OnRoomServerSceneChanged_m602FD3E32791A3C8484D9FB440C2CF9BE5A86399,
	NetworkRoomManager_OnRoomServerCreateRoomPlayer_mC12E4A1B71D9FC924E3CEF2CC3769B0A42E5B3B2,
	NetworkRoomManager_OnRoomServerCreateGamePlayer_m2B6ED32252D6EC5B21A91F1F17DF5DA65E1CDBB2,
	NetworkRoomManager_OnRoomServerAddPlayer_mB337CD10CC7034F4FB389CB4D99371F3EEC07EF2,
	NetworkRoomManager_OnRoomServerSceneLoadedForPlayer_mB0544E7321ADDECD6A9C1E9E8C7310D0E517F344,
	NetworkRoomManager_OnRoomServerPlayersReady_m301090629E3E54FCF0A7129E0FB5B8FD4EFB8400,
	NetworkRoomManager_OnRoomServerPlayersNotReady_m0A77903DEBFF2FD17554B6731C3F8A057B6965C4,
	NetworkRoomManager_OnRoomClientEnter_mA666F419C195924373F22CC2E7D1BCB98AE6783C,
	NetworkRoomManager_OnRoomClientExit_mA87606F861DEA7147D43477F48F1763260033AF5,
	NetworkRoomManager_OnRoomClientConnect_m084BDB0A5597D148B85B2043A0E925DE7D801ABF,
	NetworkRoomManager_OnRoomClientConnect_m02F038F7D53FCF8D4801909EBEE952EFAC799674,
	NetworkRoomManager_OnRoomClientDisconnect_mD00E924009526FF7998181BA50E3733894C24DD2,
	NetworkRoomManager_OnRoomClientDisconnect_mB43136B32E21A2225D8A0115DC62799D31BD18B9,
	NetworkRoomManager_OnRoomStartClient_m83910A8CE866C07EB8DDEEB084C355FF12BB1D54,
	NetworkRoomManager_OnRoomStopClient_mEDB15D3CAF8BAA160A7254456CABA4DF07EC03C6,
	NetworkRoomManager_OnRoomClientSceneChanged_mBE98879A79CBFDC402B0D3505BC308DBBE5AE505,
	NetworkRoomManager_OnRoomClientSceneChanged_mE33E5CBF03A68019C4D06CC44CEEC4756CCF11C8,
	NetworkRoomManager_OnRoomClientAddPlayerFailed_m66C6C5AC7252C3B2ECD11280C1AF0D464BCB5657,
	NetworkRoomManager_OnGUI_mAB4CC46F1EE70E719EC87F8D78B05093E07CBE43,
	NetworkRoomManager__ctor_mB2779CD15817C861DCE98C9357276B1EE617EEA0,
	U3CU3Ec__cctor_m61D706EDC3DC5098ADF704030938B70B54AB56F5,
	U3CU3Ec__ctor_m8BA39A59192EDE1BC19CDBDFF284981F93E494C5,
	U3CU3Ec_U3CCheckReadyToBeginU3Eb__17_0_mB042B4CB069E4B701CD347434FBAE373B5963D41,
	NetworkRoomPlayer_Start_mC124766914B4867375031A861228B5BA44895E4D,
	NetworkRoomPlayer_OnDisable_m926C4290BC835450EB23EBB4F6DEAD9A49A9EF11,
	NetworkRoomPlayer_CmdChangeReadyState_mFBCD5EEDCA6C85A6D644D8ABC5DD4E0A8F9FA487,
	NetworkRoomPlayer_IndexChanged_m7979AD16E27E760101D2D37BA15AF88BA60E4C19,
	NetworkRoomPlayer_ReadyStateChanged_mA31F96F4132148CDCCDC3FA8F7E3B15C58F9A8FB,
	NetworkRoomPlayer_OnClientEnterRoom_m88410F5D47F9A97DD31BC3D8F35D3021BC306599,
	NetworkRoomPlayer_OnClientExitRoom_m2DCCA5D3C23CAE6CBD3B27BE88FE1B41A50D32E6,
	NetworkRoomPlayer_OnGUI_m825D63B075DC80709C0549AAB940DB7EAA31159B,
	NetworkRoomPlayer_DrawPlayerReadyState_mDA8EF69FD5BCFB6A0D1789B0EB347AC8181A392A,
	NetworkRoomPlayer_DrawPlayerReadyButton_mFA114A6AB3DFE41E75D689D7ABE2ED34E612A6F4,
	NetworkRoomPlayer__ctor_m022684FAC82553240563C9BCA844841F0F089F62,
	NetworkRoomPlayer_MirrorProcessed_m392DE2C7FEF38E16DDBB195DE2F249345CC52350,
	NetworkRoomPlayer_get_NetworkreadyToBegin_m5ABF9099F3D67F4B17BD0F674A1B9BE25C9FA450,
	NetworkRoomPlayer_set_NetworkreadyToBegin_m083F404E9F202196540AC3A22F94B76EA827521B,
	NetworkRoomPlayer_get_Networkindex_mDFDEAE60EDEE34FB120AAF402BC448FF54943E1D,
	NetworkRoomPlayer_set_Networkindex_mA4D8D7F31782A79E4290EEC5D0996809EA10EA8B,
	NetworkRoomPlayer_UserCode_CmdChangeReadyState__Boolean_m4B98725632A48F6A10FCF339FBF847596C3A985B,
	NetworkRoomPlayer_InvokeUserCode_CmdChangeReadyState__Boolean_m13C283E6C23D2A253BBD770C662DECD075B0F175,
	NetworkRoomPlayer__cctor_m34449F6746B525AA326819ADBAF8E2506DC7789C,
	NetworkRoomPlayer_SerializeSyncVars_mBEDEE263B0EE24C748B39401FF6D05DB7712325E,
	NetworkRoomPlayer_DeserializeSyncVars_mB9DE91F52EF5B1EBD41382DD3F6B7A97520472D4,
	NetworkTransform_get_targetComponent_mA2708512C32A48BD6903CD7D09A060E42BA090C1,
	NetworkTransform__ctor_m23D7AE0BC1AF69C7F72FC9EB55F26FB1E6161A16,
	NetworkTransform_MirrorProcessed_mB8CA216EFB9F2CD4A4BD6EFC416C115836F970B0,
	NetworkTransformBase_get_IsClientWithAuthority_m85C98D27ABF6374823903858E630FE265A66EC7A,
	NULL,
	NetworkTransformBase_get_bufferTime_m3E7D0A461F5D2326F95223E957422F8BCE322081,
	NetworkTransformBase_ConstructSnapshot_mECC1F60A100AD41E9BD114E4ABBE0CC1A1453FFB,
	NetworkTransformBase_ApplySnapshot_m961DDF9BF22074B12CF77D61CD0AA4796BDD5DF6,
	NetworkTransformBase_CompareSnapshots_m02E52751A42D24EA1C253AB7692533BF82DB545C,
	NetworkTransformBase_CmdClientToServerSync_mEB52452C71BBBBADE695073EEEA3E945B4F4DFA8,
	NetworkTransformBase_OnClientToServerSync_m18C44953C7272069B37650FB6C443D0CE8C07935,
	NetworkTransformBase_RpcServerToClientSync_m750ADC9FDB53B4415D863662020EED3B2D2DA197,
	NetworkTransformBase_OnServerToClientSync_mECAD39BACE8A3FA0DE29C493B788B13575A1F734,
	NetworkTransformBase_UpdateServer_m5AC061003CD2305B8387497186879C2C91E39AE8,
	NetworkTransformBase_UpdateClient_m1D707403210A7F29B6D3EC8EEC5E652B9EAC481C,
	NetworkTransformBase_Update_m0A78A380CC025A512798A796092FC70418A777A3,
	NetworkTransformBase_OnTeleport_m20263DD6B596675F2977355E6FCBC498E817513E,
	NetworkTransformBase_OnTeleport_m229266E4DE6C32F8B3F9A13A89944498C132C52B,
	NetworkTransformBase_RpcTeleport_mC4C6A867C5107E61BA92EE37EC3107C7AB7B811D,
	NetworkTransformBase_RpcTeleport_mAE663DAB5399B5C6274CF485FCC8313AE1EFE93B,
	NetworkTransformBase_RpcTeleportAndRotate_m3AB6EB8AD5721FC1B366FADACAFE7B4E873B5905,
	NetworkTransformBase_CmdTeleport_mE61BCDFB7AF924A81FF79E36D4773EA9D3A4BCCE,
	NetworkTransformBase_CmdTeleport_m3990722908F1BF6492E30933B3501484DC130F16,
	NetworkTransformBase_CmdTeleportAndRotate_m8251E59451B41729DF37DE85B731980E9B571C62,
	NetworkTransformBase_Reset_m0EDF6BADC312A763C3418C765702C846605AB623,
	NetworkTransformBase_OnDisable_mD817E33288849CF8383AFA3E5B7021DDA9D9DA07,
	NetworkTransformBase_OnEnable_mFBDF72FD2F54C211AD9C24C9C5C8231A220CD087,
	NetworkTransformBase_OnValidate_m8417DC76482840BF92F4A3F0DD317FE83975DEB5,
	NetworkTransformBase_OnSerialize_m1FA68B4D0B1EB10F625C49CFB9B8E315819548BF,
	NetworkTransformBase_OnDeserialize_m6CD7C1843A3D4443CC83BAB5C16312608A3F22EA,
	NetworkTransformBase__ctor_mD7C6CDB801C61FDB5A492127CCE5EA539F9E57E5,
	NetworkTransformBase_MirrorProcessed_mEFE193AFD6ADC6BD0832A873EFE52D75919A33E4,
	NetworkTransformBase_UserCode_CmdClientToServerSync__Nullable_1__Nullable_1__Nullable_1_mD610102B62B8930F1BF9444319C016562004DADF,
	NetworkTransformBase_InvokeUserCode_CmdClientToServerSync__Nullable_1__Nullable_1__Nullable_1_mF85896031356E8C5F85FEBDE76D07FF0F99F2F28,
	NetworkTransformBase_UserCode_RpcServerToClientSync__Nullable_1__Nullable_1__Nullable_1_mF03F75E60F436FDACB44009C8A15AD3BD6E498A6,
	NetworkTransformBase_InvokeUserCode_RpcServerToClientSync__Nullable_1__Nullable_1__Nullable_1_m2CF39E3A8D2058B538E1465355BBEDC9C8EB957F,
	NetworkTransformBase_UserCode_RpcTeleport__Vector3_m4F1EE0AD6A3B50A9B2C42459E484242EC614B56C,
	NetworkTransformBase_InvokeUserCode_RpcTeleport__Vector3_mCAADBCD80798CD25EDC46682C8D75F9556B3A719,
	NetworkTransformBase_UserCode_RpcTeleport__Vector3__Quaternion_m1F76DD98C7A239D198B2DC46391DCEEEEB46C3F3,
	NetworkTransformBase_InvokeUserCode_RpcTeleport__Vector3__Quaternion_m4450D12FBB1C9BA96AB1876A0310F3B3BFA35E90,
	NetworkTransformBase_UserCode_RpcTeleportAndRotate__Vector3__Quaternion_mA89EE6ACC422B9538A3BD7FB4373390A8B4777F2,
	NetworkTransformBase_InvokeUserCode_RpcTeleportAndRotate__Vector3__Quaternion_m8D7F575E535CB4529F186DFCA3164FA8277D31B4,
	NetworkTransformBase_UserCode_CmdTeleport__Vector3_mCAC67731E9F6FD7CB0DFE1BE0E0B33D58C031260,
	NetworkTransformBase_InvokeUserCode_CmdTeleport__Vector3_m2E5D54979B99ED5AAC6DEC95D0D5E362B9ED5326,
	NetworkTransformBase_UserCode_CmdTeleport__Vector3__Quaternion_mB1AAAC70E826CB10A8C01506CE3A9BBFB636F3E5,
	NetworkTransformBase_InvokeUserCode_CmdTeleport__Vector3__Quaternion_mE29B354A75ACF51631CDF57D623FE1DFADD90A5E,
	NetworkTransformBase_UserCode_CmdTeleportAndRotate__Vector3__Quaternion_m9AA9396E7342FEA4EA3AFCD1AB56E3A2B8D6BED5,
	NetworkTransformBase_InvokeUserCode_CmdTeleportAndRotate__Vector3__Quaternion_m7574DACC6AAFCD974F97D1539D00510E84979C7C,
	NetworkTransformBase__cctor_mE01354D62DD9ED716C953F135D1B44F1A56C21D4,
	NetworkTransformChild_get_targetComponent_m216A7116DDE37F0558F8B5AA345C6785947B73E8,
	NetworkTransformChild__ctor_m4EEA184777FF3985E28DD252690D046403A00217,
	NetworkTransformChild_MirrorProcessed_m3C06F1D40BD3B334363BD0708460C9B6F3A94E5A,
	NTSnapshot_get_remoteTimestamp_m84004F6F50E1788176856F60EA4152548B1D1059,
	NTSnapshot_set_remoteTimestamp_m688B88A63C5830FCE38ECFA9C4E6C28EEBF35050,
	NTSnapshot_get_localTimestamp_mA92EBFA6E7DD2511FC4E505F1A017930FBC13010,
	NTSnapshot_set_localTimestamp_m0872B0F9AF15F1379212A1F42337FE444D5B487F,
	NTSnapshot__ctor_mC1D447ECCD119ECE48D2664C9B98ED9D7BD78E48,
	NTSnapshot_Interpolate_m58F7C7598E26E8FF54232CE54C2EF754A5EFB756,
	NetworkLerpRigidbody_get_IgnoreSync_m14CA9768BA48B04B77E1267A6A9D914840834312,
	NetworkLerpRigidbody_get_ClientWithAuthority_m3AE7699B9A76D7B9FDEB56B499DE22D30EEC788C,
	NetworkLerpRigidbody_OnValidate_m30B20B406F430FC9B748A2D310DF7F67223CDFB4,
	NetworkLerpRigidbody_Update_m696911B7D2A16EC6B93AC5CDDC7C954A75B3AFB9,
	NetworkLerpRigidbody_SyncToClients_mFF5C99EEA8B07EFF8D5B8821E4F27ECED546EACD,
	NetworkLerpRigidbody_SendToServer_mE9444F0B3B1CA432D601B0C88CDC93121B003C07,
	NetworkLerpRigidbody_CmdSendState_m6DC4D21C9D099952A4F92477EEAEF7FF828608B0,
	NetworkLerpRigidbody_FixedUpdate_mFC015C2944D746B02B9F8232EB722CB46AF08D67,
	NetworkLerpRigidbody__ctor_mA9B074AB77CAD36D0D76C1A65688F91B53A02911,
	NetworkLerpRigidbody_MirrorProcessed_m8B02C4490E56FB489AEEC26885B82B9200529345,
	NetworkLerpRigidbody_get_NetworktargetVelocity_m5305051F55BDFBCD2B114244C2533D524B3C1ADB,
	NetworkLerpRigidbody_set_NetworktargetVelocity_m206BC926F2EB85CFAD55DFF1FE913CB835D544BB,
	NetworkLerpRigidbody_get_NetworktargetPosition_m06245F4B9490B61916F8D3706C3B34BEF9FEE580,
	NetworkLerpRigidbody_set_NetworktargetPosition_mCC816D3F0B19388221E1F023723810B72FF17CC6,
	NetworkLerpRigidbody_UserCode_CmdSendState__Vector3__Vector3_m47E6C65A799E999C9C6D117395E522BB26656BAE,
	NetworkLerpRigidbody_InvokeUserCode_CmdSendState__Vector3__Vector3_mBF6DD94942E928554C8CA3802D179C2DFE9B2D07,
	NetworkLerpRigidbody__cctor_m9AEA783BB326597A6A067F61FEDCE71EA1C856F6,
	NetworkLerpRigidbody_SerializeSyncVars_mDE1311A42C066B2868A6014B35B0E388F99A4E24,
	NetworkLerpRigidbody_DeserializeSyncVars_m18C676F1848CB062ACCF72808AE932A22499B158,
	NetworkRigidbody_OnValidate_m40B71B4F02A9C3447794D2D22013FFE6CBE63B9A,
	NetworkRigidbody_get_IgnoreSync_mD04E50EBD536D10F375327A2D2541C77C8BE1020,
	NetworkRigidbody_get_ClientWithAuthority_mAC15E15CA110363B88D160F06EB86964487F4564,
	NetworkRigidbody_OnVelocityChanged_mECEA041EC59A9C87B51E5C180768B00B802EBF94,
	NetworkRigidbody_OnAngularVelocityChanged_mD0CE278A923A793EA84111019C9DE9F4E5D85355,
	NetworkRigidbody_OnIsKinematicChanged_mA3258FD50BEBFEC65D4BDC3F2E7A15D2702CC13A,
	NetworkRigidbody_OnUseGravityChanged_m75BB7FEC9ECCC2D028FD84831A7877AD9DCD5FCE,
	NetworkRigidbody_OnuDragChanged_m14202442D0FD99F68BE917482FDEB8466944F6BC,
	NetworkRigidbody_OnAngularDragChanged_m92D95A47D773C036F71B5F28F6A637DDD2C10A9A,
	NetworkRigidbody_Update_m41E00C2DD4BC4C6AB2839FB11AEFAF37A54A12D1,
	NetworkRigidbody_FixedUpdate_mA633EED00083BCDDD38507E4EABFE6116075FB5D,
	NetworkRigidbody_SyncToClients_m624B5A1969873B505A76A21F738E640F5B5A0F9B,
	NetworkRigidbody_SendToServer_mACBA1E99787FD6E558D4F8D485208F38123E49D2,
	NetworkRigidbody_SendVelocity_mE86A133C631CD2BBC0767B2ADAA28B2828304844,
	NetworkRigidbody_SendRigidBodySettings_mFBC5437125594D31F1D7EA24C50CB980687EC1CA,
	NetworkRigidbody_CmdSendVelocity_mF7644E502AE79161CE31F9ED5F235C1335B6EBE8,
	NetworkRigidbody_CmdSendVelocityAndAngular_mC17BA6370EA46354F4CF1FDFDC6B83009AB81729,
	NetworkRigidbody_CmdSendIsKinematic_mE1E87D3F8CBF7BB1670AE040695C0AC6C785E0B7,
	NetworkRigidbody_CmdSendUseGravity_mA7792A14CEF862C147D57E3AC4BBB0B5F35E37CC,
	NetworkRigidbody_CmdSendDrag_mF9CBC79BD0966744CA3B041C7EC0AB31B75668D6,
	NetworkRigidbody_CmdSendAngularDrag_m1D8D38192B8E2D60C2AAC74C2741AB4B54080A20,
	NetworkRigidbody__ctor_m68590B0551893E1FE439288D6743CB42D0653646,
	NetworkRigidbody_MirrorProcessed_mBBC3926301A109683E8E313DFC1A049ABAE642F6,
	NetworkRigidbody_get_Networkvelocity_m1276832AB6DC2D16572E66E44753D98E8BB6B031,
	NetworkRigidbody_set_Networkvelocity_m6AFEAE1F46B3698F80A4AA90581F4D811657DAE4,
	NetworkRigidbody_get_NetworkangularVelocity_m37D304522EC368E3EB07B5E63A1812F98649AA53,
	NetworkRigidbody_set_NetworkangularVelocity_mCCC9446755D03AB4F2319FECE4E15BC4A228A8C4,
	NetworkRigidbody_get_NetworkisKinematic_m08AA6C0A9504D6ACFA2368D56AE320130338FC9D,
	NetworkRigidbody_set_NetworkisKinematic_m37FF2BACE580EAAE5F77B773933D9AF50807E262,
	NetworkRigidbody_get_NetworkuseGravity_mB9EE895C112215386C64CE2D0CB57EDF55860159,
	NetworkRigidbody_set_NetworkuseGravity_mACA0FBEF181AFF38A289DD202EA24EAE4A642C83,
	NetworkRigidbody_get_Networkdrag_m5841CD2C47F070EB5C947E1CA235CA0B1FCBF884,
	NetworkRigidbody_set_Networkdrag_m9FEF784BD2C3D96435C2A52F55782D16EB3ACB98,
	NetworkRigidbody_get_NetworkangularDrag_mF459FFCD27B314D85A7369EDFA0D598F39213113,
	NetworkRigidbody_set_NetworkangularDrag_mC4D10CF446648EFF2E720370F1F5D6365B031B49,
	NetworkRigidbody_UserCode_CmdSendVelocity__Vector3_m2504FBC0D69081CF7BAF886FF0BCAE95B5D8092C,
	NetworkRigidbody_InvokeUserCode_CmdSendVelocity__Vector3_m96371A52F9D7EE36BF9C7A373CD43159DA184D7B,
	NetworkRigidbody_UserCode_CmdSendVelocityAndAngular__Vector3__Vector3_m6338D7DD8BA5F91D2ECC811F6E06E1AE894A317D,
	NetworkRigidbody_InvokeUserCode_CmdSendVelocityAndAngular__Vector3__Vector3_m434754402F6BA2DD31FF449210EC418A3BA06FE3,
	NetworkRigidbody_UserCode_CmdSendIsKinematic__Boolean_m7DCDED83FC305B114ABE7AD3456C543E0D8BF232,
	NetworkRigidbody_InvokeUserCode_CmdSendIsKinematic__Boolean_m50A9A61B53AD110B5EE3987F4B30FAE6A1082BA4,
	NetworkRigidbody_UserCode_CmdSendUseGravity__Boolean_m8A2F0C53442EA0702E48C99204A19EC27E1DDCC1,
	NetworkRigidbody_InvokeUserCode_CmdSendUseGravity__Boolean_m258D1A3ABF24D5FF7AC15A69F89498650DE5EEDE,
	NetworkRigidbody_UserCode_CmdSendDrag__Single_m9C6FA4A4E005AFA33E9E46F5EEA85327DE5FBBED,
	NetworkRigidbody_InvokeUserCode_CmdSendDrag__Single_m0D1E4D9F6091029AA25B39E1B004ED7A203849BB,
	NetworkRigidbody_UserCode_CmdSendAngularDrag__Single_m56E6308CE46D762899AAF8765C8E8280F887A69B,
	NetworkRigidbody_InvokeUserCode_CmdSendAngularDrag__Single_m0D3A06DA139F6E78FE99E36907E07ED5AE247A6E,
	NetworkRigidbody__cctor_mA13DEAE6E09138F85B097FBF7DDDB63C01EBCA33,
	NetworkRigidbody_SerializeSyncVars_mD32552C3EC9D51170CC83A99E3156A2BA64D82A5,
	NetworkRigidbody_DeserializeSyncVars_m21EED7648E45EF95CAAAD35A6A5C885A16B7CE11,
	ClientSyncState__ctor_mE7E549EE79C27A358936B2A0CA1D57957245CDB4,
	NetworkRigidbody2D_OnValidate_m1ECC56C6AA8BBC118FC4C690254035ED89111A50,
	NetworkRigidbody2D_get_IgnoreSync_m004B1391CBE32012519652E2F9C3CFC2F2BEDA6E,
	NetworkRigidbody2D_get_ClientWithAuthority_m5D9760AB73F0052032D08DCA562340601034558C,
	NetworkRigidbody2D_OnVelocityChanged_m2E46E1A9328B450157E23D6C0D37A6F20956C7A6,
	NetworkRigidbody2D_OnAngularVelocityChanged_m81783F017338BCAF5F37D733AFAAFBF78776A106,
	NetworkRigidbody2D_OnIsKinematicChanged_mC588C35A081DE55387657FECB4FD43DADA3D09A4,
	NetworkRigidbody2D_OnGravityScaleChanged_mABBF609AAC8430513523BB117A5277505F6CFF18,
	NetworkRigidbody2D_OnuDragChanged_mB56B5208B427EEF536E63D33988D22AC3A347936,
	NetworkRigidbody2D_OnAngularDragChanged_m02430C7F5A59321FFD964E59D99CD50E1CCD3761,
	NetworkRigidbody2D_Update_m168457634F20035287BDA727CF4724F42BD7C7A8,
	NetworkRigidbody2D_FixedUpdate_mA899AA49A90BEEF31269B249C1AE302DD58173DB,
	NetworkRigidbody2D_SyncToClients_m834820FB56EB84221CAACCB58F2B92BD6A8E9C6F,
	NetworkRigidbody2D_SendToServer_m0EB09A212CA9BA57B24C2255CC0DA47BD7606BE7,
	NetworkRigidbody2D_SendVelocity_m33350DF0364BEFAA368AA118BD86C88B1C5B3689,
	NetworkRigidbody2D_SendRigidBodySettings_mD9C413DB0DB1F3FE9AE14633F9745CCF66986CE1,
	NetworkRigidbody2D_CmdSendVelocity_mAD710F5A606D776F2EE3A084065A4A35FFC07119,
	NetworkRigidbody2D_CmdSendVelocityAndAngular_mBCE3ADF90982A68C9A56C1465FCC1AD60973CD5B,
	NetworkRigidbody2D_CmdSendIsKinematic_m4666FD1B5DE989A50C9D97C8F85BD368B1803131,
	NetworkRigidbody2D_CmdChangeGravityScale_mEBE6E0B54052016BD2112F793FAF7C3C7FD398E0,
	NetworkRigidbody2D_CmdSendDrag_mC7B251CF6B95D8DBBA044AA7ED311EF7803C3D2A,
	NetworkRigidbody2D_CmdSendAngularDrag_m65F8FF7828AC313977ED3BB72B4C7E0F23FCD01E,
	NetworkRigidbody2D__ctor_mC34216CE64A28AC78CE16578C7950557E712DF42,
	NetworkRigidbody2D_MirrorProcessed_mC9E160359EDAF7372629371D4CC47BDF06050007,
	NetworkRigidbody2D_get_Networkvelocity_m6604949BDDADA6F8A096B05B319901252839AC9E,
	NetworkRigidbody2D_set_Networkvelocity_m6DA882973C958FEF029D3E365C9E5E247E009FC6,
	NetworkRigidbody2D_get_NetworkangularVelocity_m29B25B2D939288A11DF056B3EE2F136E3A874F46,
	NetworkRigidbody2D_set_NetworkangularVelocity_m39E1A26894E0EBCAF312816EBA48EFD2AFD05ED1,
	NetworkRigidbody2D_get_NetworkisKinematic_m865C4FFC009882792397D79B51F5FDF6EDDDCB88,
	NetworkRigidbody2D_set_NetworkisKinematic_m00F8F60CB48B86013D7915D3E26DCD9B50EB5A8F,
	NetworkRigidbody2D_get_NetworkgravityScale_m64F7419F54D056D52637344D3036E23AE1AECA6D,
	NetworkRigidbody2D_set_NetworkgravityScale_m01BC917023B712E97519FE93FDB26D5D8BE908B0,
	NetworkRigidbody2D_get_Networkdrag_m40EF2FBEED9D487F067BCEC6693B7C465DD68448,
	NetworkRigidbody2D_set_Networkdrag_m07E5A4AA04852FE886368A007F56D37987E88DDE,
	NetworkRigidbody2D_get_NetworkangularDrag_m486BF042AAC4EE0DBDECEC80FEA24DD5CF9B79FA,
	NetworkRigidbody2D_set_NetworkangularDrag_m1F5F5CF8A423DC9BF522EBC12AD90A97949FE24C,
	NetworkRigidbody2D_UserCode_CmdSendVelocity__Vector2_m78152BC3FF4CEAF2EC328325CC90B498CDC94331,
	NetworkRigidbody2D_InvokeUserCode_CmdSendVelocity__Vector2_m0F4EB3482E4CA25562E03974CB7C2521998CDF59,
	NetworkRigidbody2D_UserCode_CmdSendVelocityAndAngular__Vector2__Single_m5AADEC6B64222754293AB681CEA0245CED560627,
	NetworkRigidbody2D_InvokeUserCode_CmdSendVelocityAndAngular__Vector2__Single_m7B7B51027D188DC0E0AC35EA430D9119BB6ABE8D,
	NetworkRigidbody2D_UserCode_CmdSendIsKinematic__Boolean_m95AC310DACF64E46DE0181E16BCC252316512A35,
	NetworkRigidbody2D_InvokeUserCode_CmdSendIsKinematic__Boolean_m197C05A828CD167B7EA5C979EAE79F0AE0FD3AA7,
	NetworkRigidbody2D_UserCode_CmdChangeGravityScale__Single_m7AA4B246FCBDEC5938F665B4860ADD523A91D3F3,
	NetworkRigidbody2D_InvokeUserCode_CmdChangeGravityScale__Single_mBF61D966111A325B6BF197BBC7F709D0712EFD3D,
	NetworkRigidbody2D_UserCode_CmdSendDrag__Single_mBFE2FE570DE3B829E5E4761C854D712352123AE8,
	NetworkRigidbody2D_InvokeUserCode_CmdSendDrag__Single_mC0AFBD1A7A79CF9C4C6A7F6D9187BDAE672E0F55,
	NetworkRigidbody2D_UserCode_CmdSendAngularDrag__Single_m763A18D4CFF682436F703613A610315DC1353519,
	NetworkRigidbody2D_InvokeUserCode_CmdSendAngularDrag__Single_m0CC726CC47CB0378327000FAFF89CFAB7069185D,
	NetworkRigidbody2D__cctor_m3F6E918117CBECEB7425CB6C05EE2B658C6953A9,
	NetworkRigidbody2D_SerializeSyncVars_m033E882D59C7B36511F9A6111F6100057E9DB7B3,
	NetworkRigidbody2D_DeserializeSyncVars_m724AEF58D65825E3B0636FCCD13539CC9ACE0E81,
	ClientSyncState__ctor_mED0F6C89D691FDE5EAAE633B579613091108F34A,
	NetworkTransform_get_targetTransform_mC881691083A745C569A8BA8B07F33058148B0FE5,
	NetworkTransform__ctor_mB4AA1104E1900482A1F8A488D0E541437C1054E8,
	NetworkTransform_MirrorProcessed_mC2887596405B426150BA73442FFBDA1EFB38653C,
	NULL,
	NetworkTransformBase_get_IsOwnerWithClientAuthority_mF4AF1FB4447DA11A7E084011AE96168B3C6AE4E3,
	NetworkTransformBase_FixedUpdate_mE596F1DA08C1024E780A9037298965360B12FA85,
	NetworkTransformBase_ServerUpdate_mC67AB2BD5EDD5F5AF3027B0D8991A830D0557D29,
	NetworkTransformBase_ClientAuthorityUpdate_mA9E47C99871609FA001278C09D59494E316AD97D,
	NetworkTransformBase_ClientRemoteUpdate_m13FD9DA234CF273816B17398380E79BC42CB712F,
	NetworkTransformBase_HasEitherMovedRotatedScaled_m25AC8AD6F4C0A18C3E7BBB749B9DB551792C61ED,
	NetworkTransformBase_get_HasMoved_mA061AFB2FDEC9DED027EA7C3A9CA11034FCEC13F,
	NetworkTransformBase_get_HasRotated_m4F9A1B811872B4CD665068677ED381CB198BE810,
	NetworkTransformBase_get_HasScaled_m3210831C922D73AFB5F5FA13D6F8A9A9C310513B,
	NetworkTransformBase_NeedsTeleport_m4999ABAB5041322921782C55FAF74BE09759251E,
	NetworkTransformBase_CmdClientToServerSync_m4A234A7D5495D36E522B858F4050CC442B55FD4A,
	NetworkTransformBase_RpcMove_mC089E102E8C3C1B5C7F316FCF57C7B4486476EE4,
	NetworkTransformBase_SetGoal_mC25E7E30409614EFEE368262A0537524E4B94211,
	NetworkTransformBase_EstimateMovementSpeed_mF910B9F9B319FBEBD06AC11A27A1919E377018BC,
	NetworkTransformBase_ApplyPositionRotationScale_m37BF4F818B78F7B9A8AFEA603B2B54A5CEF8EA4E,
	NetworkTransformBase_InterpolatePosition_m0036CB2D94C9BE929025BDC790118135081EDB0B,
	NetworkTransformBase_InterpolateRotation_mF2BD9EE260C54BAC30CA8E8F7B1D0C340E9A4AA3,
	NetworkTransformBase_InterpolateScale_mE7CF32C3C3C82414721490C99C1971877DA04B88,
	NetworkTransformBase_CurrentInterpolationFactor_m57D042970C468F9859120796244C46E7EB5C939F,
	NetworkTransformBase_ServerTeleport_mEF9ECFA0FBCF2D0448284B511692CB61AF0CCCCE,
	NetworkTransformBase_ServerTeleport_m4E8F9CA127E8100055D926476AA8C1ACDE46B6B5,
	NetworkTransformBase_DoTeleport_m263D5F57F8117BBBBFFFEE420D210B9ECE708021,
	NetworkTransformBase_RpcTeleport_m2E06E42230A147CB253024D7DFBBF3AD357F8F27,
	NetworkTransformBase_CmdTeleportFinished_m050A5E005BC56812AD1B546651AC723C40BAC482,
	NetworkTransformBase_OnDrawGizmos_m201BD602C906DF0CAB4062EDD55D72F5ECCB04E3,
	NetworkTransformBase_DrawDataPointGizmo_mFD807536E36BE9F41548EEE3A54BB47E2DF5D243,
	NetworkTransformBase_DrawLineBetweenDataPoints_mE3DDE940CB2BE44F00945DEEF485DED039EC6CF1,
	NetworkTransformBase__ctor_m7F1BEF03D49DB00ED74D85970D5E30A8B14E377E,
	NetworkTransformBase_MirrorProcessed_m355817808C7A2B0B14201E075702C86BDDD964C6,
	NetworkTransformBase_get_NetworkclientAuthority_m9657103DC3F3AE77A3E1A2B8405051D1A9280AAD,
	NetworkTransformBase_set_NetworkclientAuthority_m329724D832B6923D591BC62EF06ADD3DE0CE9BF4,
	NetworkTransformBase_get_NetworkexcludeOwnerUpdate_m0BF4BA43B8F9BF3778B24090EE3E466E90B3F790,
	NetworkTransformBase_set_NetworkexcludeOwnerUpdate_m293E54A64E1605CCF0EAD5AEE835EBD65D5C1355,
	NetworkTransformBase_get_NetworksyncPosition_m7CB08BAC1947C7E530679774D7CC134291001397,
	NetworkTransformBase_set_NetworksyncPosition_m43212BC29791176F5B832D0256D311E96D608ED1,
	NetworkTransformBase_get_NetworksyncRotation_m7BD9DAE6B4A679FF60B4749B8EEBE2A23797A418,
	NetworkTransformBase_set_NetworksyncRotation_m50AE877EA7ABEE8E9860568BAFD9E94C08CA1CEE,
	NetworkTransformBase_get_NetworksyncScale_m332806A5D48BD3671C71ED9D6D6C9644FBC95A63,
	NetworkTransformBase_set_NetworksyncScale_m22C987C09CBDF8B8EFBD0C0AC52915621ED75ADA,
	NetworkTransformBase_get_NetworkinterpolatePosition_m9819D816848F20F6B1F4796496912EC5826EA9F1,
	NetworkTransformBase_set_NetworkinterpolatePosition_mB0A6D4026D0AC9F0BDC3C23A333CB1C7AC49D277,
	NetworkTransformBase_get_NetworkinterpolateRotation_mCCE3E9053877A3165C8F66F2B7C540D191AA0473,
	NetworkTransformBase_set_NetworkinterpolateRotation_m415B821B6054A740529F1F57D5F79764B19FD3E6,
	NetworkTransformBase_get_NetworkinterpolateScale_m6C81E85950710AD4ADCA563F4A67BCAB89AA2515,
	NetworkTransformBase_set_NetworkinterpolateScale_m325A8BBBB5416AE0AD5A1BD3B2AEF7EB66F192A1,
	NetworkTransformBase_get_NetworklocalPositionSensitivity_mB732054AE7D1FC2EB57BFEC50917438EE5F5888E,
	NetworkTransformBase_set_NetworklocalPositionSensitivity_mD0E30BE1A2BB1F78C732AF07B6F548B96B17D3D1,
	NetworkTransformBase_get_NetworklocalRotationSensitivity_mC67052F68DE8FB87906A31FF66D9903A0AAB6F7B,
	NetworkTransformBase_set_NetworklocalRotationSensitivity_m93017DBC9971A0CF2532C055B1E77B6DCF076290,
	NetworkTransformBase_get_NetworklocalScaleSensitivity_m79A978CC85991F22D95908A2417A747ACD075050,
	NetworkTransformBase_set_NetworklocalScaleSensitivity_mEC39778F89A07F616CCDC8D8DA673BA0A7FE46E7,
	NetworkTransformBase_UserCode_CmdClientToServerSync__Vector3__UInt32__Vector3_m6B3CBC07E6BBD2FA67BB803498D9D77C8DF1D667,
	NetworkTransformBase_InvokeUserCode_CmdClientToServerSync__Vector3__UInt32__Vector3_m38353087C429C28FD201C851FA101BF6148F1731,
	NetworkTransformBase_UserCode_RpcMove__Vector3__UInt32__Vector3_m3B1B2B596A04210D4BE04A4D090FEDB888E4BA5F,
	NetworkTransformBase_InvokeUserCode_RpcMove__Vector3__UInt32__Vector3_m545822EAC9837C55ACF715DE38CEAE8473A0C769,
	NetworkTransformBase_UserCode_RpcTeleport__Vector3__UInt32__Boolean_mBA38B249F8B595ED0BD57F06677C107D61FD6DC8,
	NetworkTransformBase_InvokeUserCode_RpcTeleport__Vector3__UInt32__Boolean_mE89958C01A2AD882B144A1AE65A84617D79478C6,
	NetworkTransformBase_UserCode_CmdTeleportFinished_m0430E546F2CCA58FF782AD2F02DAD796AE1A4C9B,
	NetworkTransformBase_InvokeUserCode_CmdTeleportFinished_m26EEA3E6F568579CCB33A52023CBABA036D384B0,
	NetworkTransformBase__cctor_mF6F411DFE380188412E290AF41D132A68497C104,
	NetworkTransformBase_SerializeSyncVars_m14AB6A526E35FCDAD98B8826B4194BA380AB2EF3,
	NetworkTransformBase_DeserializeSyncVars_m8597B9457BE7674B1510C9A39AACC29FC0DF80B5,
	DataPoint_get_isValid_m305A8F1E2800210EEF2C12A7532FCF8602E0C020,
	NetworkTransformChild_get_targetTransform_mA2551232703D05BBE8BCBA0AA4D93AAF41721C19,
	NetworkTransformChild__ctor_m251A86F312AD587906D460237502D9DCD3469837,
	NetworkTransformChild_MirrorProcessed_mF330A0B9E63FB0D5E6DCB670ED01F3635BE4CA3A,
	ServerFoundUnityEvent__ctor_m98F0934D285A0B199816BC0EF56A9110F7B499A7,
	NetworkDiscovery_get_ServerId_m0393F83D7872351E0B51CBD9B006BE12F7DAB0F2,
	NetworkDiscovery_set_ServerId_mCC20225E540A4D49A13E318D99D4315916C9D3EC,
	NetworkDiscovery_Start_m0BC76A9176B2FFDE299369A1568353EA1112D2FE,
	NetworkDiscovery_ProcessRequest_m2696E7DB1F2A3B37D35445E47A1159BDA1610804,
	NetworkDiscovery_GetRequest_m5191C16B4967C44C1788414977BE7680B6AD3132,
	NetworkDiscovery_ProcessResponse_m56B63DE07F7D1958AC06B0DE2D3526B167717858,
	NetworkDiscovery__ctor_mCF2DB5CFEADF8AD55A37AF5A49F1CF8699FC45DF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkDiscoveryHUD_OnGUI_m8B99F305D5025EEBFA37CC6378421EBB434075C5,
	NetworkDiscoveryHUD_DrawGUI_mF090ACD1398D168C60FBDFED821DF47AEEE3230F,
	NetworkDiscoveryHUD_StopButtons_mE0D1594EFFD82FB28D3CAF1080E7C46BEA95DD9F,
	NetworkDiscoveryHUD_Connect_mD5FA85FD6866BA8F180A4A94E37F163EFA9379D6,
	NetworkDiscoveryHUD_OnDiscoveredServer_mA1F4D2C18D68A00ECA3980FC66E2239FC269E38A,
	NetworkDiscoveryHUD__ctor_mD529124AEEA197386427DFC7013E4481E9FEE668,
	ServerResponse_get_EndPoint_m6DAD50F1785A1577B00C8DAADB1E2446B934CA0C,
	ServerResponse_set_EndPoint_mE2370B1D263800DD50A7463C05FFD93351E9526F,
	GeneratedNetworkCode__Read_Mirror_ReadyMessage_m048541B81A0BEAA714842B3F8B0F22DCA3E279AD,
	GeneratedNetworkCode__Write_Mirror_ReadyMessage_m01DC51135AB81BEDB7BED2EC7583182F840A7A09,
	GeneratedNetworkCode__Read_Mirror_NotReadyMessage_m95725AD95EBC2F4EF2827593C36AF574D365E882,
	GeneratedNetworkCode__Write_Mirror_NotReadyMessage_mDC8D762F7E74327CDC8E61B81F44966CFB33F007,
	GeneratedNetworkCode__Read_Mirror_AddPlayerMessage_m25FB29225CACDDC16B1DCE207D44A58A6E14965B,
	GeneratedNetworkCode__Write_Mirror_AddPlayerMessage_m35841C4087A1F65F31027390600EA3D20ABD888C,
	GeneratedNetworkCode__Read_Mirror_SceneMessage_m5DBBB5CE0FC989769FB20788A1A2939BFF406583,
	GeneratedNetworkCode__Read_Mirror_SceneOperation_mF63BDCDC7391D3FD37A384BDAB94CF80E8AE38BD,
	GeneratedNetworkCode__Write_Mirror_SceneMessage_mDC09FE790B84739BD6BB39063A42674B8D20B439,
	GeneratedNetworkCode__Write_Mirror_SceneOperation_m898374C0384EA970E800ABE11A9F3791BFAFD2D2,
	GeneratedNetworkCode__Read_Mirror_CommandMessage_m9137F757D3CCC1B2724A8FAD5383405026D4E15E,
	GeneratedNetworkCode__Write_Mirror_CommandMessage_mC010353643CA9E4283D1D728CA8B5FBDEC9CFC92,
	GeneratedNetworkCode__Read_Mirror_RpcMessage_m76904F48A4AE93E3EC3811BC8F7D7222986CBC91,
	GeneratedNetworkCode__Write_Mirror_RpcMessage_m96E555184BB3884A376BA0E5FE220C0F69997A2A,
	GeneratedNetworkCode__Read_Mirror_SpawnMessage_mA4DC66896E815468C7F7D1ED0AC1DDD46DCA6BEF,
	GeneratedNetworkCode__Write_Mirror_SpawnMessage_mCBC229687B354E7CB4C31F44857299BF9AF276B9,
	GeneratedNetworkCode__Read_Mirror_ChangeOwnerMessage_m59235FBB767E6D6787BE24D4305AF42C3F5DD153,
	GeneratedNetworkCode__Write_Mirror_ChangeOwnerMessage_m6F109C3E92E45BCF85BAC47927A0BF432F65ADD3,
	GeneratedNetworkCode__Read_Mirror_ObjectSpawnStartedMessage_m780E1A3DDDD191BF46EFC0855892B4F4A89693CB,
	GeneratedNetworkCode__Write_Mirror_ObjectSpawnStartedMessage_m52E2D9BE0E2DF414E34B2E020683E5B5894C52CE,
	GeneratedNetworkCode__Read_Mirror_ObjectSpawnFinishedMessage_m7A049EC00BF8FF56B643ECD135C828C8B185C00B,
	GeneratedNetworkCode__Write_Mirror_ObjectSpawnFinishedMessage_mAE0290DB02063E7F5176F88D9B089A4E4B073C29,
	GeneratedNetworkCode__Read_Mirror_ObjectDestroyMessage_m0253F7D176EB837EFE401C21E44861CA9EAA0C9A,
	GeneratedNetworkCode__Write_Mirror_ObjectDestroyMessage_m533403A395951F44DC2902A716CD4350CF6C8A13,
	GeneratedNetworkCode__Read_Mirror_ObjectHideMessage_m4FCABC4227993CB9C8DCDA36724031BDC5598575,
	GeneratedNetworkCode__Write_Mirror_ObjectHideMessage_mEFDA269E469770B02ADCBF4BFCBE5E4905D4D9F4,
	GeneratedNetworkCode__Read_Mirror_EntityStateMessage_m335FEDD5888CDEC91C115AA10A5A9829702E259B,
	GeneratedNetworkCode__Write_Mirror_EntityStateMessage_m2774697DC9F0FEEC721EF000A1721F90385C26ED,
	GeneratedNetworkCode__Read_Mirror_NetworkPingMessage_m79DF5D2AA749DB6C49943DF2D340EC0F74EA410B,
	GeneratedNetworkCode__Write_Mirror_NetworkPingMessage_mFA89195274AC46E8EC8F56362EF22E9238363B50,
	GeneratedNetworkCode__Read_Mirror_NetworkPongMessage_m8F5A8B068367AE323991AE95DD10ABC62825F3EA,
	GeneratedNetworkCode__Write_Mirror_NetworkPongMessage_mA43DBF3C08FFB1AEC273A541063BE71BF4D4019B,
	GeneratedNetworkCode__Read_Mirror_Discovery_ServerRequest_m440F755BC5692190A3584036CFC6D6C14BA81C41,
	GeneratedNetworkCode__Write_Mirror_Discovery_ServerRequest_m7C4ED38FD32CA46F6CF6FEE281405BEEBF470646,
	GeneratedNetworkCode__Read_Mirror_Discovery_ServerResponse_m54E9E54AF32F787BCA642B8242BD86FC7C5F9968,
	GeneratedNetworkCode__Write_Mirror_Discovery_ServerResponse_mC5CA823A98102D4EE3318EA6FC2D9EF989B13133,
	GeneratedNetworkCode_InitReadWriters_mE5C95525D6231DCC2495AB04E9E14AC26D72BECD,
};
extern void LogEntry__ctor_mC9E2A56B134438A361389D9C84CDC08CA73DED4E_AdjustorThunk (void);
extern void NTSnapshot_get_remoteTimestamp_m84004F6F50E1788176856F60EA4152548B1D1059_AdjustorThunk (void);
extern void NTSnapshot_set_remoteTimestamp_m688B88A63C5830FCE38ECFA9C4E6C28EEBF35050_AdjustorThunk (void);
extern void NTSnapshot_get_localTimestamp_mA92EBFA6E7DD2511FC4E505F1A017930FBC13010_AdjustorThunk (void);
extern void NTSnapshot_set_localTimestamp_m0872B0F9AF15F1379212A1F42337FE444D5B487F_AdjustorThunk (void);
extern void NTSnapshot__ctor_mC1D447ECCD119ECE48D2664C9B98ED9D7BD78E48_AdjustorThunk (void);
extern void DataPoint_get_isValid_m305A8F1E2800210EEF2C12A7532FCF8602E0C020_AdjustorThunk (void);
extern void ServerResponse_get_EndPoint_m6DAD50F1785A1577B00C8DAADB1E2446B934CA0C_AdjustorThunk (void);
extern void ServerResponse_set_EndPoint_mE2370B1D263800DD50A7463C05FFD93351E9526F_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[9] = 
{
	{ 0x06000003, LogEntry__ctor_mC9E2A56B134438A361389D9C84CDC08CA73DED4E_AdjustorThunk },
	{ 0x060000F7, NTSnapshot_get_remoteTimestamp_m84004F6F50E1788176856F60EA4152548B1D1059_AdjustorThunk },
	{ 0x060000F8, NTSnapshot_set_remoteTimestamp_m688B88A63C5830FCE38ECFA9C4E6C28EEBF35050_AdjustorThunk },
	{ 0x060000F9, NTSnapshot_get_localTimestamp_mA92EBFA6E7DD2511FC4E505F1A017930FBC13010_AdjustorThunk },
	{ 0x060000FA, NTSnapshot_set_localTimestamp_m0872B0F9AF15F1379212A1F42337FE444D5B487F_AdjustorThunk },
	{ 0x060000FB, NTSnapshot__ctor_mC1D447ECCD119ECE48D2664C9B98ED9D7BD78E48_AdjustorThunk },
	{ 0x060001B8, DataPoint_get_isValid_m305A8F1E2800210EEF2C12A7532FCF8602E0C020_AdjustorThunk },
	{ 0x060001E8, ServerResponse_get_EndPoint_m6DAD50F1785A1577B00C8DAADB1E2446B934CA0C_AdjustorThunk },
	{ 0x060001E9, ServerResponse_set_EndPoint_mE2370B1D263800DD50A7463C05FFD93351E9526F_AdjustorThunk },
};
static const int32_t s_InvokerIndices[526] = 
{
	5508,
	5508,
	2554,
	5508,
	1556,
	5508,
	5508,
	5508,
	3171,
	5508,
	1964,
	2565,
	5508,
	5508,
	5508,
	5508,
	4453,
	4453,
	5508,
	2125,
	1537,
	4404,
	1964,
	2565,
	5508,
	5508,
	5508,
	4453,
	4453,
	5508,
	4495,
	1964,
	2565,
	5508,
	-1,
	-1,
	-1,
	-1,
	-1,
	5367,
	4140,
	1964,
	2565,
	5508,
	5508,
	5508,
	5508,
	5508,
	4453,
	4453,
	5508,
	2565,
	1559,
	4453,
	1964,
	2565,
	4453,
	5508,
	5442,
	5508,
	5508,
	5508,
	2639,
	1352,
	5508,
	451,
	4453,
	451,
	4453,
	4423,
	4423,
	5368,
	1968,
	4453,
	1968,
	2581,
	4453,
	4423,
	4453,
	4423,
	451,
	4453,
	4423,
	4423,
	4505,
	451,
	4453,
	4423,
	4423,
	5508,
	3842,
	5508,
	5454,
	4505,
	451,
	7009,
	4453,
	7009,
	4423,
	7009,
	4423,
	7009,
	4505,
	7009,
	451,
	7009,
	4453,
	7009,
	4423,
	7009,
	4423,
	7009,
	8500,
	1968,
	2581,
	5508,
	5508,
	5508,
	5508,
	5508,
	5442,
	4494,
	5508,
	5508,
	4453,
	2565,
	5508,
	5508,
	5508,
	4453,
	4453,
	4453,
	5508,
	4453,
	4453,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	4453,
	4453,
	4453,
	3448,
	1843,
	4453,
	1398,
	5508,
	5508,
	5508,
	5508,
	5508,
	4453,
	5508,
	4453,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	8500,
	5508,
	3596,
	5508,
	5508,
	4494,
	2280,
	2623,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5442,
	4494,
	5367,
	4423,
	4494,
	7009,
	8500,
	1968,
	2581,
	5398,
	5508,
	5508,
	5442,
	5398,
	5454,
	5391,
	1520,
	3838,
	1437,
	1437,
	1437,
	1437,
	5508,
	5508,
	5508,
	4552,
	2654,
	4552,
	2654,
	2654,
	4552,
	2654,
	2654,
	5508,
	5508,
	5508,
	5508,
	1968,
	2581,
	5508,
	5508,
	1437,
	7009,
	1437,
	7009,
	4552,
	7009,
	2654,
	7009,
	2654,
	7009,
	4552,
	7009,
	2654,
	7009,
	2654,
	7009,
	8500,
	5398,
	5508,
	5508,
	5328,
	4386,
	5328,
	4386,
	427,
	6737,
	5442,
	5442,
	5508,
	5508,
	5508,
	5508,
	2657,
	5508,
	5508,
	5508,
	5502,
	4552,
	5502,
	4552,
	2657,
	7009,
	8500,
	1968,
	2581,
	5508,
	5442,
	5442,
	2657,
	2657,
	2623,
	2623,
	2639,
	2639,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	4552,
	2657,
	4494,
	4494,
	4505,
	4505,
	5508,
	5508,
	5502,
	4552,
	5502,
	4552,
	5442,
	4494,
	5442,
	4494,
	5454,
	4505,
	5454,
	4505,
	4552,
	7009,
	2657,
	7009,
	4494,
	7009,
	4494,
	7009,
	4505,
	7009,
	4505,
	7009,
	8500,
	1968,
	2581,
	5508,
	5508,
	5442,
	5442,
	2650,
	2639,
	2623,
	2639,
	2639,
	2639,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	4550,
	2649,
	4494,
	4505,
	4505,
	4505,
	5508,
	5508,
	5500,
	4550,
	5454,
	4505,
	5442,
	4494,
	5454,
	4505,
	5454,
	4505,
	5454,
	4505,
	4550,
	7009,
	2649,
	7009,
	4494,
	7009,
	4505,
	7009,
	4505,
	7009,
	4505,
	7009,
	8500,
	1968,
	2581,
	5508,
	5398,
	5508,
	5508,
	5398,
	5442,
	5508,
	5508,
	5508,
	5508,
	5442,
	5442,
	5442,
	5442,
	5442,
	1639,
	1639,
	1640,
	6521,
	1640,
	1434,
	1344,
	1434,
	7464,
	4552,
	2654,
	2654,
	1638,
	5508,
	5508,
	7731,
	7046,
	5508,
	5508,
	5442,
	4494,
	5442,
	4494,
	5442,
	4494,
	5442,
	4494,
	5442,
	4494,
	5442,
	4494,
	5442,
	4494,
	5442,
	4494,
	5454,
	4505,
	5454,
	4505,
	5454,
	4505,
	1639,
	7009,
	1639,
	7009,
	1638,
	7009,
	5508,
	7009,
	8500,
	1968,
	2581,
	5442,
	5398,
	5508,
	5508,
	5508,
	5368,
	4424,
	5508,
	2053,
	5448,
	2634,
	5508,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5508,
	5508,
	5508,
	4500,
	4500,
	5508,
	5398,
	4453,
	8171,
	7670,
	8038,
	7660,
	7887,
	7634,
	8209,
	8194,
	7677,
	7675,
	7913,
	7639,
	8181,
	7673,
	8232,
	7682,
	7903,
	7635,
	8141,
	7665,
	8140,
	7664,
	8138,
	7662,
	8139,
	7663,
	7951,
	7643,
	8036,
	7658,
	8037,
	7659,
	8212,
	7679,
	8213,
	7680,
	8500,
};
static const Il2CppTokenRangePair s_rgctxIndices[6] = 
{
	{ 0x0200000B, { 0, 18 } },
	{ 0x02000027, { 18, 16 } },
	{ 0x02000028, { 34, 3 } },
	{ 0x02000029, { 37, 3 } },
	{ 0x0200002A, { 40, 2 } },
	{ 0x0200002B, { 42, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[45] = 
{
	{ (Il2CppRGCTXDataType)3, 7681 },
	{ (Il2CppRGCTXDataType)2, 3555 },
	{ (Il2CppRGCTXDataType)3, 15512 },
	{ (Il2CppRGCTXDataType)3, 7683 },
	{ (Il2CppRGCTXDataType)3, 15513 },
	{ (Il2CppRGCTXDataType)3, 15515 },
	{ (Il2CppRGCTXDataType)3, 10449 },
	{ (Il2CppRGCTXDataType)3, 10448 },
	{ (Il2CppRGCTXDataType)2, 2844 },
	{ (Il2CppRGCTXDataType)3, 15514 },
	{ (Il2CppRGCTXDataType)3, 15469 },
	{ (Il2CppRGCTXDataType)3, 7682 },
	{ (Il2CppRGCTXDataType)3, 35711 },
	{ (Il2CppRGCTXDataType)3, 12535 },
	{ (Il2CppRGCTXDataType)3, 12534 },
	{ (Il2CppRGCTXDataType)2, 2890 },
	{ (Il2CppRGCTXDataType)2, 2737 },
	{ (Il2CppRGCTXDataType)3, 7680 },
	{ (Il2CppRGCTXDataType)3, 26839 },
	{ (Il2CppRGCTXDataType)3, 26835 },
	{ (Il2CppRGCTXDataType)3, 26841 },
	{ (Il2CppRGCTXDataType)2, 5528 },
	{ (Il2CppRGCTXDataType)3, 26840 },
	{ (Il2CppRGCTXDataType)3, 26838 },
	{ (Il2CppRGCTXDataType)3, 41666 },
	{ (Il2CppRGCTXDataType)3, 41663 },
	{ (Il2CppRGCTXDataType)3, 26837 },
	{ (Il2CppRGCTXDataType)2, 1260 },
	{ (Il2CppRGCTXDataType)3, 43556 },
	{ (Il2CppRGCTXDataType)3, 26834 },
	{ (Il2CppRGCTXDataType)3, 41657 },
	{ (Il2CppRGCTXDataType)3, 26836 },
	{ (Il2CppRGCTXDataType)3, 43551 },
	{ (Il2CppRGCTXDataType)3, 41660 },
	{ (Il2CppRGCTXDataType)3, 26845 },
	{ (Il2CppRGCTXDataType)3, 26846 },
	{ (Il2CppRGCTXDataType)3, 41631 },
	{ (Il2CppRGCTXDataType)3, 41624 },
	{ (Il2CppRGCTXDataType)3, 43440 },
	{ (Il2CppRGCTXDataType)3, 26844 },
	{ (Il2CppRGCTXDataType)3, 26842 },
	{ (Il2CppRGCTXDataType)3, 41628 },
	{ (Il2CppRGCTXDataType)3, 41621 },
	{ (Il2CppRGCTXDataType)3, 43442 },
	{ (Il2CppRGCTXDataType)3, 26843 },
};
extern const CustomAttributesCacheGenerator g_Mirror_Components_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Mirror_Components_CodeGenModule;
const Il2CppCodeGenModule g_Mirror_Components_CodeGenModule = 
{
	"Mirror.Components.dll",
	526,
	s_methodPointers,
	9,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	6,
	s_rgctxIndices,
	45,
	s_rgctxValues,
	NULL,
	g_Mirror_Components_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
