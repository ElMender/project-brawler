﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// Mirror.SyncVarAttribute
struct SyncVarAttribute_t68608CC0E0E751346276F1A6ABC18F4414E60305;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CAllErc1155U3Ed__11_tA168E7380E452F6602375A9173BC179B94FC4953_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAllErc721U3Ed__10_t0745493CAF65FB4DF6B59F47EAC92C305AA41C89_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBalanceOfBatchU3Ed__2_t60644C76ADC9E5B161B500CB35FEBCA663D9EAF3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBalanceOfU3Ed__1_t596B95FA265D5B86832E6DC75F164259A19E82BC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBalanceOfU3Ed__1_tB473BB3392EB81C7699C038E4F719E2403237CF9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBalanceOfU3Ed__1_tDC23312279CD10EC9C92EDA7B353808B3F7E87FE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBalanceOfU3Ed__2_t68638B7AF8B246775112352588CC72C561347885_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBlockNumberU3Ed__7_t706EE349544B8B7491CA7F3F882FBE0E2AF3E9D8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBroadcastTransactionU3Ed__16_tA29C760B3B0769972DD4485195B59F7C54E69868_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBurnU3Ed__11_t57A44B35853BAEE9C5D70339914EF7D880A488C6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCallU3Ed__4_t57CA061C0ACBEC239ADBE086F5510845CDAB5F95_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CChainIdU3Ed__14_t01BB10AB132A673B66ED82C5BDC825A957E3B042_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CChangeNameU3Ed__8_t6C75EF60C6E66B9BB035E70BB9907A2A7D7553F8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCountDownU3Ed__6_tE8E3E327359764B263938683CA50A40694172FBA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateContractDataU3Ed__9_t8E75059EF4B0F6C3DE517A4AF94B8F24E8FF8043_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateTransactionU3Ed__15_t937D21D1ADE37224525F3F71AF50134AE63B7474_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDecimalsU3Ed__4_tB98D99A8A8CF0A91CDD2EC24910036603A583104_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGasLimitU3Ed__13_tB59F6B110FC57A64542891D9CE84ADC2CB4BDBE1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGasPriceU3Ed__12_tCA0EA62CF30ED62EEF1902A4C2D5FA332773FD35_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetNftU3Ed__10_t9EB229B03086CFC54263810ABEE900FCBA6FC85B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CMintU3Ed__6_tE9A6EE8F8DB7219D7A1127A28A907B7BBB826F9C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CMultiCallU3Ed__5_t0621A0209591B34AF876B719A10D05583E968D0A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CNameU3Ed__2_tC680925E6D11B7981763BED04F1D122A139990F1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CNonceU3Ed__8_tEA3B84C8E2FFF958D8C9AD0C080BA0997375135C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnConnectedU3Ed__6_tEFC9E3F446B694229C87DDAF8E60AD510F0C5F1A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnLoginU3Ed__2_t566FE4C2C513D969330BA5B1E33B3C977A5C7522_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnSend1155U3Ed__0_t7C1EF487A95CFD9199044CEAF7184F39BD8EB846_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnSend20U3Ed__0_t19D4015AA5A24D7279A104C542D5B7F63B20E830_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnSend721U3Ed__0_t3CC7BEDD0A3A00D6359571BDB9098BA2565172D0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnSendContractU3Ed__0_t15F35AD460E7321938C2FD75A065307A93876F9C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnSendTransactionU3Ed__0_t17D92BBC0E0FF116EE91EBA514E78D309F704DF2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnSendTransactionU3Ed__0_t2A7D6CDF5773E00BAEA1859E778C6BC14638EA50_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnSendTransactionU3Ed__0_tAC21FA78A9334357CD3676F1B36A89938F0526AB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnSignMessageU3Ed__0_t53C2F5D040A39F6A271091DF3DCEF85F1D9D150A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnSignMessageU3Ed__0_tB5523A25CE45CFCAD0F6637DD8C3B64F6E00AB42_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnTransfer1155U3Ed__0_t3825AF7253AF38599E73188003F68FD536E30C1A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnTransfer20U3Ed__0_t3CD34BBD71FF80DD78C25D0FE992ED9D1353E8F4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnTransfer721U3Ed__0_t19C7B441C34CB2F433CAD229877558815977CDCC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COwnerOfBatchU3Ed__3_tCBB2433BB34B3DABB1D9A161C46FDBC0039AEE87_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COwnerOfU3Ed__2_t31BF925729D400CFF07A1A743984FE04A1432167_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CReLoadU3Ed__9_tC6FAF560B8A13260A85E593B82E7660D7792F4AB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSafeTransferFromU3Ed__4_t3CA8E5B1F971C292BEA3561C1036CA365AC1ECFA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSafeTransferFromU3Ed__5_t7DECABE00F0A217B61D3EBE9046646C3A3C2B0AE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSendContractU3Ed__10_tAF6DDE291FA77D4A2E25DF8CA6735982A5DB53AF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSendTransactionU3Ed__11_tB60AB0245CE1C4FDBCB87250537366F097BE2873_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSendTransactionU3Ed__1_t4CDF5AD9BA9B4AF6F817B240705BB2224D0C98B3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSignU3Ed__12_t4804C297784B13A88FD729B8A08FE6D404F3E13B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSignU3Ed__2_t0540A44BE5A31AB48B727FE4B61366B5186F08F3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_t066787A9AC67CB267EF2129A0DF34AA9B4B21043_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_t35FD1115B0BC8E36B985418F68FF8380BC1C0E47_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_t3B0BE786C61C58EE0A8283BC4FBCEAE538C03D52_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_t479A6E16097648A0B10B0738414EAA50C0BF9293_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_t4C79D410A6C828EF826DC1FDC1B980242584FB34_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_t63A6A6BA4375C09E642416F98697963B5F104775_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_t64D04A640EF7B7D42ECBFEDD79906834E402A483_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_t7536771EC913216A48637DC08A6230B38924DB2A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_t764D1A87698DFB37F6819D1127531AAACCFD6BD4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_t7925DC9BEAFEB29EB7181BD3B7DF9CBEBEC20FD9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_t7C1BAAAA07BFB53E3766837DFCF5BC7A3489A733_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_t895329D5ABFF19DBE587C0A6208CE8E3B3005492_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_t99EA47DEF2B18A525F2C23CF25D697879CB47FDF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_tAE2F75CCF52DC3789B48F5B1A2F666C4CF49CDB1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_tB8B66B371E65C03F064F71E432E101E7258A08F6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_tBEA5CFB545680D57980B2DB70A29312D13795D50_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_tD23CE53FD3B127402C1A7B73A29C6AF1CAB1C3E8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_tD57E14EDF0D69957DE35D69036FE23E61F69DBA1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__0_tFB2C2E6151A430EC6EA1BC8B0F3539D9FDC5344E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__1_t1B368FB4F5A05F99FFFEAAC39C443FDA0DDEEFE0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__1_t69C675AB72ACBBEC9F2AE65937A9BAF7F7F0002B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__1_t9F1246F65A99648E11CF70FD4A03AC34D963741E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__3_tD11F74ED0C86C8C74ECB51A28C9E277FBE93327F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSymbolU3Ed__3_tF808464A58E53DB372CC85620EE1D0D90241B5B8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTotalSupplyU3Ed__5_t7308A33FAC257AD518D84B19A3FCAB415CE71D47_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTransferU3Ed__4_t4EB7258C238E19FF60A86837B4E6620F284C8779_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTxStatusU3Ed__6_tD683ED7E193CE34112C120522E9F294244B46E8A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CURIU3Ed__3_tDA723E745AD248E1E1036527792AB1C993E3F182_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CURIU3Ed__4_tDB8FC11B83C01A3F00B4B4E522A83AA0BAC5E67F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CVerifyU3Ed__3_t31E1C48BC886D4D42666C1483040ACB8058C4A72_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RuntimeInitializeLoadType
struct RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5 
{
public:
	// System.Int32 UnityEngine.RuntimeInitializeLoadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// Mirror.SyncVarAttribute
struct SyncVarAttribute_t68608CC0E0E751346276F1A6ABC18F4414E60305  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String Mirror.SyncVarAttribute::hook
	String_t* ___hook_0;

public:
	inline static int32_t get_offset_of_hook_0() { return static_cast<int32_t>(offsetof(SyncVarAttribute_t68608CC0E0E751346276F1A6ABC18F4414E60305, ___hook_0)); }
	inline String_t* get_hook_0() const { return ___hook_0; }
	inline String_t** get_address_of_hook_0() { return &___hook_0; }
	inline void set_hook_0(String_t* value)
	{
		___hook_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hook_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D  : public PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948
{
public:
	// UnityEngine.RuntimeInitializeLoadType UnityEngine.RuntimeInitializeOnLoadMethodAttribute::m_LoadType
	int32_t ___m_LoadType_0;

public:
	inline static int32_t get_offset_of_m_LoadType_0() { return static_cast<int32_t>(offsetof(RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D, ___m_LoadType_0)); }
	inline int32_t get_m_LoadType_0() const { return ___m_LoadType_0; }
	inline int32_t* get_address_of_m_LoadType_0() { return &___m_LoadType_0; }
	inline void set_m_LoadType_0(int32_t value)
	{
		___m_LoadType_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void Mirror.SyncVarAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SyncVarAttribute__ctor_m4E99C2B45DDA311F1D9B684B967978541348C875 (SyncVarAttribute_t68608CC0E0E751346276F1A6ABC18F4414E60305 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516 (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * __this, int32_t ___loadType0, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void Controlador_tCBF1E76EC0EDC20B5276019371D9C1E65DB1084C_CustomAttributesCacheGenerator_U3CassetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Controlador_tCBF1E76EC0EDC20B5276019371D9C1E65DB1084C_CustomAttributesCacheGenerator_Controlador_get_asset_m365D9F2CF8A3FA0841A4E58BB93A6DA4CFBDFD85(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_vida(CustomAttributesCache* cache)
{
	{
		SyncVarAttribute_t68608CC0E0E751346276F1A6ABC18F4414E60305 * tmp = (SyncVarAttribute_t68608CC0E0E751346276F1A6ABC18F4414E60305 *)cache->attributes[0];
		SyncVarAttribute__ctor_m4E99C2B45DDA311F1D9B684B967978541348C875(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_Stock(CustomAttributesCache* cache)
{
	{
		SyncVarAttribute_t68608CC0E0E751346276F1A6ABC18F4414E60305 * tmp = (SyncVarAttribute_t68608CC0E0E751346276F1A6ABC18F4414E60305 *)cache->attributes[0];
		SyncVarAttribute__ctor_m4E99C2B45DDA311F1D9B684B967978541348C875(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_winner(CustomAttributesCache* cache)
{
	{
		SyncVarAttribute_t68608CC0E0E751346276F1A6ABC18F4414E60305 * tmp = (SyncVarAttribute_t68608CC0E0E751346276F1A6ABC18F4414E60305 *)cache->attributes[0];
		SyncVarAttribute__ctor_m4E99C2B45DDA311F1D9B684B967978541348C875(tmp, NULL);
	}
}
static void gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_TimeToStart(CustomAttributesCache* cache)
{
	{
		SyncVarAttribute_t68608CC0E0E751346276F1A6ABC18F4414E60305 * tmp = (SyncVarAttribute_t68608CC0E0E751346276F1A6ABC18F4414E60305 *)cache->attributes[0];
		SyncVarAttribute__ctor_m4E99C2B45DDA311F1D9B684B967978541348C875(tmp, NULL);
	}
}
static void gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_GameStarted(CustomAttributesCache* cache)
{
	{
		SyncVarAttribute_t68608CC0E0E751346276F1A6ABC18F4414E60305 * tmp = (SyncVarAttribute_t68608CC0E0E751346276F1A6ABC18F4414E60305 *)cache->attributes[0];
		SyncVarAttribute__ctor_m4E99C2B45DDA311F1D9B684B967978541348C875(tmp, NULL);
	}
}
static void gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_gameManager_Start_m169616D522718461F2522FC60796972765513C0B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__3_tD11F74ED0C86C8C74ECB51A28C9E277FBE93327F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__3_tD11F74ED0C86C8C74ECB51A28C9E277FBE93327F_0_0_0_var), NULL);
	}
}
static void gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_gameManager_CountDown_mD4C9C89C32375A504A19CDF76CD14CF08E839C5F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCountDownU3Ed__6_tE8E3E327359764B263938683CA50A40694172FBA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCountDownU3Ed__6_tE8E3E327359764B263938683CA50A40694172FBA_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__3_tD11F74ED0C86C8C74ECB51A28C9E277FBE93327F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__3_tD11F74ED0C86C8C74ECB51A28C9E277FBE93327F_CustomAttributesCacheGenerator_U3CStartU3Ed__3__ctor_mB48F4FAA9459FC08D7816D6B374F86776F614EF7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__3_tD11F74ED0C86C8C74ECB51A28C9E277FBE93327F_CustomAttributesCacheGenerator_U3CStartU3Ed__3_System_IDisposable_Dispose_m93BAFC6EC8543E89F44F2387FB1A74F02B9BD27C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__3_tD11F74ED0C86C8C74ECB51A28C9E277FBE93327F_CustomAttributesCacheGenerator_U3CStartU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2B347A5018C26527FE70DD242A8E9AE4FB19ACA5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__3_tD11F74ED0C86C8C74ECB51A28C9E277FBE93327F_CustomAttributesCacheGenerator_U3CStartU3Ed__3_System_Collections_IEnumerator_Reset_mD0096DF92FFDA797AEEE4E72E2129471B234813D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__3_tD11F74ED0C86C8C74ECB51A28C9E277FBE93327F_CustomAttributesCacheGenerator_U3CStartU3Ed__3_System_Collections_IEnumerator_get_Current_mCC8C0917AA7FAC87174852D1E5818DA0DB4416C0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCountDownU3Ed__6_tE8E3E327359764B263938683CA50A40694172FBA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCountDownU3Ed__6_tE8E3E327359764B263938683CA50A40694172FBA_CustomAttributesCacheGenerator_U3CCountDownU3Ed__6__ctor_mC57A48E6DDD09DA6E118A55129926012973577CA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCountDownU3Ed__6_tE8E3E327359764B263938683CA50A40694172FBA_CustomAttributesCacheGenerator_U3CCountDownU3Ed__6_System_IDisposable_Dispose_m2C308B8820BEC4319EBD920D21012B4FD9150C86(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCountDownU3Ed__6_tE8E3E327359764B263938683CA50A40694172FBA_CustomAttributesCacheGenerator_U3CCountDownU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8389CB86F7B02C0EEA1C0B9AB4153727D26C6DCF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCountDownU3Ed__6_tE8E3E327359764B263938683CA50A40694172FBA_CustomAttributesCacheGenerator_U3CCountDownU3Ed__6_System_Collections_IEnumerator_Reset_mDCCF81548FBBEB13791A835C0C3D5A9DC02DC371(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCountDownU3Ed__6_tE8E3E327359764B263938683CA50A40694172FBA_CustomAttributesCacheGenerator_U3CCountDownU3Ed__6_System_Collections_IEnumerator_get_Current_m9D45A9FB927FF4F09F7A7D3CF3E0310A8444C439(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ImportNFTTextureExample_tEB4BB9B467014822B3E60E0FB79FDBFDD7ADE317_CustomAttributesCacheGenerator_ImportNFTTextureExample_Start_mBBAA36C72E5675E8A026135493CC4EC689D13C28(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__1_t1B368FB4F5A05F99FFFEAAC39C443FDA0DDEEFE0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__1_t1B368FB4F5A05F99FFFEAAC39C443FDA0DDEEFE0_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__1_t1B368FB4F5A05F99FFFEAAC39C443FDA0DDEEFE0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__1_t1B368FB4F5A05F99FFFEAAC39C443FDA0DDEEFE0_CustomAttributesCacheGenerator_U3CStartU3Ed__1_SetStateMachine_m72770C78EEEB2DFDB98D0015DFA3CB7FE724D5E3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERC1155_tB77D8C560189DC5911A80997D6230820826F50F0_CustomAttributesCacheGenerator_ERC1155_BalanceOf_m6E609734004053402859FA732EB12BD113F5BBC9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBalanceOfU3Ed__1_tB473BB3392EB81C7699C038E4F719E2403237CF9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CBalanceOfU3Ed__1_tB473BB3392EB81C7699C038E4F719E2403237CF9_0_0_0_var), NULL);
	}
}
static void ERC1155_tB77D8C560189DC5911A80997D6230820826F50F0_CustomAttributesCacheGenerator_ERC1155_BalanceOfBatch_m976C311032DE4CAB615A4BC0FC2F57669A5D04DA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBalanceOfBatchU3Ed__2_t60644C76ADC9E5B161B500CB35FEBCA663D9EAF3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CBalanceOfBatchU3Ed__2_t60644C76ADC9E5B161B500CB35FEBCA663D9EAF3_0_0_0_var), NULL);
	}
}
static void ERC1155_tB77D8C560189DC5911A80997D6230820826F50F0_CustomAttributesCacheGenerator_ERC1155_URI_mA6C8E3B39B71865362F93F5641353858238A7341(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CURIU3Ed__3_tDA723E745AD248E1E1036527792AB1C993E3F182_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CURIU3Ed__3_tDA723E745AD248E1E1036527792AB1C993E3F182_0_0_0_var), NULL);
	}
}
static void U3CBalanceOfU3Ed__1_tB473BB3392EB81C7699C038E4F719E2403237CF9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBalanceOfU3Ed__1_tB473BB3392EB81C7699C038E4F719E2403237CF9_CustomAttributesCacheGenerator_U3CBalanceOfU3Ed__1_SetStateMachine_m6AF4E4155ABC37A3A94695D78B0A266B91662C55(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBalanceOfBatchU3Ed__2_t60644C76ADC9E5B161B500CB35FEBCA663D9EAF3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBalanceOfBatchU3Ed__2_t60644C76ADC9E5B161B500CB35FEBCA663D9EAF3_CustomAttributesCacheGenerator_U3CBalanceOfBatchU3Ed__2_SetStateMachine_m34749E2FCC62C6089CA7D14ECC5008103D62EB8E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CURIU3Ed__3_tDA723E745AD248E1E1036527792AB1C993E3F182_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CURIU3Ed__3_tDA723E745AD248E1E1036527792AB1C993E3F182_CustomAttributesCacheGenerator_U3CURIU3Ed__3_SetStateMachine_mF1974E58996F7E03BED741B0EE6CB3416B2ACE72(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERC20_tAA7C8F643B8DC535D3E94C1C7C99A6E6B105D689_CustomAttributesCacheGenerator_ERC20_BalanceOf_mEC315C4821D0098701B261B0BEBC1A2C5B6653D6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBalanceOfU3Ed__1_tDC23312279CD10EC9C92EDA7B353808B3F7E87FE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CBalanceOfU3Ed__1_tDC23312279CD10EC9C92EDA7B353808B3F7E87FE_0_0_0_var), NULL);
	}
}
static void ERC20_tAA7C8F643B8DC535D3E94C1C7C99A6E6B105D689_CustomAttributesCacheGenerator_ERC20_Name_m5535F0329E289B03EA6922420FCFA008C077831C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CNameU3Ed__2_tC680925E6D11B7981763BED04F1D122A139990F1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CNameU3Ed__2_tC680925E6D11B7981763BED04F1D122A139990F1_0_0_0_var), NULL);
	}
}
static void ERC20_tAA7C8F643B8DC535D3E94C1C7C99A6E6B105D689_CustomAttributesCacheGenerator_ERC20_Symbol_m1AD978D77261C364EF258C1FB93BE8071F00B026(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSymbolU3Ed__3_tF808464A58E53DB372CC85620EE1D0D90241B5B8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSymbolU3Ed__3_tF808464A58E53DB372CC85620EE1D0D90241B5B8_0_0_0_var), NULL);
	}
}
static void ERC20_tAA7C8F643B8DC535D3E94C1C7C99A6E6B105D689_CustomAttributesCacheGenerator_ERC20_Decimals_m3F46DAA1CEDFE86A488FCF1A2BC10AE4E7FFB4FB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDecimalsU3Ed__4_tB98D99A8A8CF0A91CDD2EC24910036603A583104_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CDecimalsU3Ed__4_tB98D99A8A8CF0A91CDD2EC24910036603A583104_0_0_0_var), NULL);
	}
}
static void ERC20_tAA7C8F643B8DC535D3E94C1C7C99A6E6B105D689_CustomAttributesCacheGenerator_ERC20_TotalSupply_mF278916C040D2B71EC160E87AC1853B0EBEB245B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTotalSupplyU3Ed__5_t7308A33FAC257AD518D84B19A3FCAB415CE71D47_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CTotalSupplyU3Ed__5_t7308A33FAC257AD518D84B19A3FCAB415CE71D47_0_0_0_var), NULL);
	}
}
static void U3CBalanceOfU3Ed__1_tDC23312279CD10EC9C92EDA7B353808B3F7E87FE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBalanceOfU3Ed__1_tDC23312279CD10EC9C92EDA7B353808B3F7E87FE_CustomAttributesCacheGenerator_U3CBalanceOfU3Ed__1_SetStateMachine_m8699DFD19EEB93AED6E7AA39FDB723211AE5BCB9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNameU3Ed__2_tC680925E6D11B7981763BED04F1D122A139990F1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CNameU3Ed__2_tC680925E6D11B7981763BED04F1D122A139990F1_CustomAttributesCacheGenerator_U3CNameU3Ed__2_SetStateMachine_m49C47D68B3C839B84D14583B1166CE6CF3CCA7B7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSymbolU3Ed__3_tF808464A58E53DB372CC85620EE1D0D90241B5B8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSymbolU3Ed__3_tF808464A58E53DB372CC85620EE1D0D90241B5B8_CustomAttributesCacheGenerator_U3CSymbolU3Ed__3_SetStateMachine_mB4F3CE1D6541161B57F0FF7DC4AD7695C09BD8E1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDecimalsU3Ed__4_tB98D99A8A8CF0A91CDD2EC24910036603A583104_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDecimalsU3Ed__4_tB98D99A8A8CF0A91CDD2EC24910036603A583104_CustomAttributesCacheGenerator_U3CDecimalsU3Ed__4_SetStateMachine_mE9819F1B7E958708201C635D9DA17E66020F1BC0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTotalSupplyU3Ed__5_t7308A33FAC257AD518D84B19A3FCAB415CE71D47_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTotalSupplyU3Ed__5_t7308A33FAC257AD518D84B19A3FCAB415CE71D47_CustomAttributesCacheGenerator_U3CTotalSupplyU3Ed__5_SetStateMachine_m2B452102301436127D184A33B558C339BE18146B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERC721_tB84643377A52CC25F7A1340587AEE43033B9E8FE_CustomAttributesCacheGenerator_ERC721_BalanceOf_mF1062240718E051ABFE8F82957509C43F8DFD912(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBalanceOfU3Ed__1_t596B95FA265D5B86832E6DC75F164259A19E82BC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CBalanceOfU3Ed__1_t596B95FA265D5B86832E6DC75F164259A19E82BC_0_0_0_var), NULL);
	}
}
static void ERC721_tB84643377A52CC25F7A1340587AEE43033B9E8FE_CustomAttributesCacheGenerator_ERC721_OwnerOf_m7860416DDCB041D40D7AFB7D06D6CDC1F4841EE1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COwnerOfU3Ed__2_t31BF925729D400CFF07A1A743984FE04A1432167_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COwnerOfU3Ed__2_t31BF925729D400CFF07A1A743984FE04A1432167_0_0_0_var), NULL);
	}
}
static void ERC721_tB84643377A52CC25F7A1340587AEE43033B9E8FE_CustomAttributesCacheGenerator_ERC721_OwnerOfBatch_mE72D33FAF1E47F507ECDD68E91388EF9031C385C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COwnerOfBatchU3Ed__3_tCBB2433BB34B3DABB1D9A161C46FDBC0039AEE87_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COwnerOfBatchU3Ed__3_tCBB2433BB34B3DABB1D9A161C46FDBC0039AEE87_0_0_0_var), NULL);
	}
}
static void ERC721_tB84643377A52CC25F7A1340587AEE43033B9E8FE_CustomAttributesCacheGenerator_ERC721_URI_mA39E5DFAE098BA075A61C93AF8FC02BE5E29FB75(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CURIU3Ed__4_tDB8FC11B83C01A3F00B4B4E522A83AA0BAC5E67F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CURIU3Ed__4_tDB8FC11B83C01A3F00B4B4E522A83AA0BAC5E67F_0_0_0_var), NULL);
	}
}
static void U3CBalanceOfU3Ed__1_t596B95FA265D5B86832E6DC75F164259A19E82BC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBalanceOfU3Ed__1_t596B95FA265D5B86832E6DC75F164259A19E82BC_CustomAttributesCacheGenerator_U3CBalanceOfU3Ed__1_SetStateMachine_mD5B12D1AB4C0AE02E3EFC2E56D0E9A69C4A7C0C7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COwnerOfU3Ed__2_t31BF925729D400CFF07A1A743984FE04A1432167_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COwnerOfU3Ed__2_t31BF925729D400CFF07A1A743984FE04A1432167_CustomAttributesCacheGenerator_U3COwnerOfU3Ed__2_SetStateMachine_mB9A361356148E8EC7BB543DF7E2F58122EF5685C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COwnerOfBatchU3Ed__3_tCBB2433BB34B3DABB1D9A161C46FDBC0039AEE87_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COwnerOfBatchU3Ed__3_tCBB2433BB34B3DABB1D9A161C46FDBC0039AEE87_CustomAttributesCacheGenerator_U3COwnerOfBatchU3Ed__3_SetStateMachine_m94E80236D24084C606B00DAC402B69E5AB05B06A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CURIU3Ed__4_tDB8FC11B83C01A3F00B4B4E522A83AA0BAC5E67F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CURIU3Ed__4_tDB8FC11B83C01A3F00B4B4E522A83AA0BAC5E67F_CustomAttributesCacheGenerator_U3CURIU3Ed__4_SetStateMachine_m83FEB5116327CB0CD5BD2F04AF3E440AA2E52DEA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_BalanceOf_m6AF5C0DA67C45C9977B4AAC2C249A36FD84D559E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBalanceOfU3Ed__2_t68638B7AF8B246775112352588CC72C561347885_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CBalanceOfU3Ed__2_t68638B7AF8B246775112352588CC72C561347885_0_0_0_var), NULL);
	}
}
static void EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_Verify_mA8C92E16EF841C6356A849C6E29765534B0EA2CB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CVerifyU3Ed__3_t31E1C48BC886D4D42666C1483040ACB8058C4A72_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CVerifyU3Ed__3_t31E1C48BC886D4D42666C1483040ACB8058C4A72_0_0_0_var), NULL);
	}
}
static void EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_Call_m8760C42E4C727A97B7DF290D887E292460F142C4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCallU3Ed__4_t57CA061C0ACBEC239ADBE086F5510845CDAB5F95_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CCallU3Ed__4_t57CA061C0ACBEC239ADBE086F5510845CDAB5F95_0_0_0_var), NULL);
	}
}
static void EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_MultiCall_m45EA95D0FC82EFF50087446F4ED7C4D407F0B43D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CMultiCallU3Ed__5_t0621A0209591B34AF876B719A10D05583E968D0A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CMultiCallU3Ed__5_t0621A0209591B34AF876B719A10D05583E968D0A_0_0_0_var), NULL);
	}
}
static void EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_TxStatus_mB28FBAF141DDCD343020787DD04064C0C5993E95(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTxStatusU3Ed__6_tD683ED7E193CE34112C120522E9F294244B46E8A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CTxStatusU3Ed__6_tD683ED7E193CE34112C120522E9F294244B46E8A_0_0_0_var), NULL);
	}
}
static void EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_BlockNumber_m70A74E4A04CAE5B3B50FFA11AFEE0DF6A6A7EC4A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBlockNumberU3Ed__7_t706EE349544B8B7491CA7F3F882FBE0E2AF3E9D8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CBlockNumberU3Ed__7_t706EE349544B8B7491CA7F3F882FBE0E2AF3E9D8_0_0_0_var), NULL);
	}
}
static void EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_Nonce_m47224BD603FC6ADE4473C6E4499791AC55A46BFF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CNonceU3Ed__8_tEA3B84C8E2FFF958D8C9AD0C080BA0997375135C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CNonceU3Ed__8_tEA3B84C8E2FFF958D8C9AD0C080BA0997375135C_0_0_0_var), NULL);
	}
}
static void EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_CreateContractData_m0A69CC06452FBB76AC259D74C32CCC81DBC02A92(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateContractDataU3Ed__9_t8E75059EF4B0F6C3DE517A4AF94B8F24E8FF8043_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CCreateContractDataU3Ed__9_t8E75059EF4B0F6C3DE517A4AF94B8F24E8FF8043_0_0_0_var), NULL);
	}
}
static void EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_AllErc721_m0556274DB1637D6A25E1414A24D33CBA8AD95379(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAllErc721U3Ed__10_t0745493CAF65FB4DF6B59F47EAC92C305AA41C89_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CAllErc721U3Ed__10_t0745493CAF65FB4DF6B59F47EAC92C305AA41C89_0_0_0_var), NULL);
	}
}
static void EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_AllErc1155_mF8CC99C847366BCE65363096D2018749003520D6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAllErc1155U3Ed__11_tA168E7380E452F6602375A9173BC179B94FC4953_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CAllErc1155U3Ed__11_tA168E7380E452F6602375A9173BC179B94FC4953_0_0_0_var), NULL);
	}
}
static void EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_GasPrice_mFFCC58B0047F8FABEE42963AEB67979F814C23A6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGasPriceU3Ed__12_tCA0EA62CF30ED62EEF1902A4C2D5FA332773FD35_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CGasPriceU3Ed__12_tCA0EA62CF30ED62EEF1902A4C2D5FA332773FD35_0_0_0_var), NULL);
	}
}
static void EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_GasLimit_mC9C0175B2B65072058C8DA29E1656FD3676E14B3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGasLimitU3Ed__13_tB59F6B110FC57A64542891D9CE84ADC2CB4BDBE1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CGasLimitU3Ed__13_tB59F6B110FC57A64542891D9CE84ADC2CB4BDBE1_0_0_0_var), NULL);
	}
}
static void EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_ChainId_m5A37B376BE531801A4C4984896B593413935FEA1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CChainIdU3Ed__14_t01BB10AB132A673B66ED82C5BDC825A957E3B042_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CChainIdU3Ed__14_t01BB10AB132A673B66ED82C5BDC825A957E3B042_0_0_0_var), NULL);
	}
}
static void EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_CreateTransaction_m9DB2168641491273CE7C782CA1DC042EFD38A6A8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateTransactionU3Ed__15_t937D21D1ADE37224525F3F71AF50134AE63B7474_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CCreateTransactionU3Ed__15_t937D21D1ADE37224525F3F71AF50134AE63B7474_0_0_0_var), NULL);
	}
}
static void EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_BroadcastTransaction_mC3C2718C12BABE0A104C2D9FE68099FF1DD14227(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBroadcastTransactionU3Ed__16_tA29C760B3B0769972DD4485195B59F7C54E69868_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CBroadcastTransactionU3Ed__16_tA29C760B3B0769972DD4485195B59F7C54E69868_0_0_0_var), NULL);
	}
}
static void U3CBalanceOfU3Ed__2_t68638B7AF8B246775112352588CC72C561347885_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBalanceOfU3Ed__2_t68638B7AF8B246775112352588CC72C561347885_CustomAttributesCacheGenerator_U3CBalanceOfU3Ed__2_SetStateMachine_mD8F5C1681185A6E89A41F1980CE6AABD3050AF10(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CVerifyU3Ed__3_t31E1C48BC886D4D42666C1483040ACB8058C4A72_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CVerifyU3Ed__3_t31E1C48BC886D4D42666C1483040ACB8058C4A72_CustomAttributesCacheGenerator_U3CVerifyU3Ed__3_SetStateMachine_m477762C3E935EB66E24F0F2FF4564273024EC780(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallU3Ed__4_t57CA061C0ACBEC239ADBE086F5510845CDAB5F95_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCallU3Ed__4_t57CA061C0ACBEC239ADBE086F5510845CDAB5F95_CustomAttributesCacheGenerator_U3CCallU3Ed__4_SetStateMachine_m2B4C2DDF94B74ACCA651A9AC53F1CA67853C059F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMultiCallU3Ed__5_t0621A0209591B34AF876B719A10D05583E968D0A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CMultiCallU3Ed__5_t0621A0209591B34AF876B719A10D05583E968D0A_CustomAttributesCacheGenerator_U3CMultiCallU3Ed__5_SetStateMachine_m77A035AB7DEC5D4342A4617899A02C20AB0E8733(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTxStatusU3Ed__6_tD683ED7E193CE34112C120522E9F294244B46E8A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTxStatusU3Ed__6_tD683ED7E193CE34112C120522E9F294244B46E8A_CustomAttributesCacheGenerator_U3CTxStatusU3Ed__6_SetStateMachine_m6A59F5AB3D2A6077CF1BA0D1E8353FDEADC0D754(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBlockNumberU3Ed__7_t706EE349544B8B7491CA7F3F882FBE0E2AF3E9D8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBlockNumberU3Ed__7_t706EE349544B8B7491CA7F3F882FBE0E2AF3E9D8_CustomAttributesCacheGenerator_U3CBlockNumberU3Ed__7_SetStateMachine_mBFBF55F1191EB7F3D04C204A5FFC7053EF8C8200(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNonceU3Ed__8_tEA3B84C8E2FFF958D8C9AD0C080BA0997375135C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CNonceU3Ed__8_tEA3B84C8E2FFF958D8C9AD0C080BA0997375135C_CustomAttributesCacheGenerator_U3CNonceU3Ed__8_SetStateMachine_m6C0E5BDD185012D9EE78EC7E73F9DB5448B13C1D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateContractDataU3Ed__9_t8E75059EF4B0F6C3DE517A4AF94B8F24E8FF8043_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateContractDataU3Ed__9_t8E75059EF4B0F6C3DE517A4AF94B8F24E8FF8043_CustomAttributesCacheGenerator_U3CCreateContractDataU3Ed__9_SetStateMachine_mC2A74C0432A6E8621E3D510878A7EBB59BA77B96(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAllErc721U3Ed__10_t0745493CAF65FB4DF6B59F47EAC92C305AA41C89_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAllErc721U3Ed__10_t0745493CAF65FB4DF6B59F47EAC92C305AA41C89_CustomAttributesCacheGenerator_U3CAllErc721U3Ed__10_SetStateMachine_mC7F79A8B56C459462E982BFB2007442CB5E77327(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAllErc1155U3Ed__11_tA168E7380E452F6602375A9173BC179B94FC4953_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAllErc1155U3Ed__11_tA168E7380E452F6602375A9173BC179B94FC4953_CustomAttributesCacheGenerator_U3CAllErc1155U3Ed__11_SetStateMachine_m09EBEC3D74A283E912739821BBC146F9A9306B00(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGasPriceU3Ed__12_tCA0EA62CF30ED62EEF1902A4C2D5FA332773FD35_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGasPriceU3Ed__12_tCA0EA62CF30ED62EEF1902A4C2D5FA332773FD35_CustomAttributesCacheGenerator_U3CGasPriceU3Ed__12_SetStateMachine_mD8A5388148CA31A2177C494B2BF4ACAE993566C7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGasLimitU3Ed__13_tB59F6B110FC57A64542891D9CE84ADC2CB4BDBE1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGasLimitU3Ed__13_tB59F6B110FC57A64542891D9CE84ADC2CB4BDBE1_CustomAttributesCacheGenerator_U3CGasLimitU3Ed__13_SetStateMachine_mAAB510EC434AE95BC2910728023F47955EA53564(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChainIdU3Ed__14_t01BB10AB132A673B66ED82C5BDC825A957E3B042_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CChainIdU3Ed__14_t01BB10AB132A673B66ED82C5BDC825A957E3B042_CustomAttributesCacheGenerator_U3CChainIdU3Ed__14_SetStateMachine_mC3320A62EFF445C636E3E0CBE73AA0C5272DA035(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateTransactionU3Ed__15_t937D21D1ADE37224525F3F71AF50134AE63B7474_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateTransactionU3Ed__15_t937D21D1ADE37224525F3F71AF50134AE63B7474_CustomAttributesCacheGenerator_U3CCreateTransactionU3Ed__15_SetStateMachine_m07800DBAA5A71F467B51548B88E44168704663BD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBroadcastTransactionU3Ed__16_tA29C760B3B0769972DD4485195B59F7C54E69868_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBroadcastTransactionU3Ed__16_tA29C760B3B0769972DD4485195B59F7C54E69868_CustomAttributesCacheGenerator_U3CBroadcastTransactionU3Ed__16_SetStateMachine_m6534FEE9D7F78D07C18917E9259F894B414C1E0E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Web3Wallet_t981201C98C5E889969444984CC19BAF5B46A4913_CustomAttributesCacheGenerator_Web3Wallet_SendTransaction_mF2426B50D053EF9F8A0D3A6BD8C5A054015D3510(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSendTransactionU3Ed__1_t4CDF5AD9BA9B4AF6F817B240705BB2224D0C98B3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSendTransactionU3Ed__1_t4CDF5AD9BA9B4AF6F817B240705BB2224D0C98B3_0_0_0_var), NULL);
	}
}
static void Web3Wallet_t981201C98C5E889969444984CC19BAF5B46A4913_CustomAttributesCacheGenerator_Web3Wallet_Sign_m9097546A29A5FBA1F0114EC95EF7097EC0FD8D8A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSignU3Ed__2_t0540A44BE5A31AB48B727FE4B61366B5186F08F3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSignU3Ed__2_t0540A44BE5A31AB48B727FE4B61366B5186F08F3_0_0_0_var), NULL);
	}
}
static void U3CSendTransactionU3Ed__1_t4CDF5AD9BA9B4AF6F817B240705BB2224D0C98B3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSendTransactionU3Ed__1_t4CDF5AD9BA9B4AF6F817B240705BB2224D0C98B3_CustomAttributesCacheGenerator_U3CSendTransactionU3Ed__1_SetStateMachine_m6C58AE8B37667F289A5318FDC6D296A10C72431B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSignU3Ed__2_t0540A44BE5A31AB48B727FE4B61366B5186F08F3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSignU3Ed__2_t0540A44BE5A31AB48B727FE4B61366B5186F08F3_CustomAttributesCacheGenerator_U3CSignU3Ed__2_SetStateMachine_m80954B22F85344CA5FC5AA3D47EB652446A8DA40(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Web3GL_tD6EF7423AA9552FE71D1B906BD23FF96D9634662_CustomAttributesCacheGenerator_Web3GL_SendContract_m6675BF158272EAC923D284D5CC10C7F09558E173(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSendContractU3Ed__10_tAF6DDE291FA77D4A2E25DF8CA6735982A5DB53AF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSendContractU3Ed__10_tAF6DDE291FA77D4A2E25DF8CA6735982A5DB53AF_0_0_0_var), NULL);
	}
}
static void Web3GL_tD6EF7423AA9552FE71D1B906BD23FF96D9634662_CustomAttributesCacheGenerator_Web3GL_SendTransaction_mD40DBCA64188F5159A42BE864CA19D5B61C7F5D9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSendTransactionU3Ed__11_tB60AB0245CE1C4FDBCB87250537366F097BE2873_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSendTransactionU3Ed__11_tB60AB0245CE1C4FDBCB87250537366F097BE2873_0_0_0_var), NULL);
	}
}
static void Web3GL_tD6EF7423AA9552FE71D1B906BD23FF96D9634662_CustomAttributesCacheGenerator_Web3GL_Sign_mFEAE31488340360AB008F8E3FAE799D1E10871C7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSignU3Ed__12_t4804C297784B13A88FD729B8A08FE6D404F3E13B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSignU3Ed__12_t4804C297784B13A88FD729B8A08FE6D404F3E13B_0_0_0_var), NULL);
	}
}
static void U3CSendContractU3Ed__10_tAF6DDE291FA77D4A2E25DF8CA6735982A5DB53AF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSendContractU3Ed__10_tAF6DDE291FA77D4A2E25DF8CA6735982A5DB53AF_CustomAttributesCacheGenerator_U3CSendContractU3Ed__10_SetStateMachine_mDB924150509FEA19525D41521D4EFFBB8CA8C99C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSendTransactionU3Ed__11_tB60AB0245CE1C4FDBCB87250537366F097BE2873_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSendTransactionU3Ed__11_tB60AB0245CE1C4FDBCB87250537366F097BE2873_CustomAttributesCacheGenerator_U3CSendTransactionU3Ed__11_SetStateMachine_m324E10096F6699BD39FB4AB1847403016F77A827(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSignU3Ed__12_t4804C297784B13A88FD729B8A08FE6D404F3E13B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSignU3Ed__12_t4804C297784B13A88FD729B8A08FE6D404F3E13B_CustomAttributesCacheGenerator_U3CSignU3Ed__12_SetStateMachine_mBAA214792AF8C334AC1EDA56E1C0A9B805A5C267(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERC1155BalanceOfBatchExample_tA8D88B5A9FAF21F12EFE356CEBFDD07E78C229A7_CustomAttributesCacheGenerator_ERC1155BalanceOfBatchExample_Start_mB2D18D5D03FFF898BCCD553ED52C25472DDA46A2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_tD23CE53FD3B127402C1A7B73A29C6AF1CAB1C3E8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_tD23CE53FD3B127402C1A7B73A29C6AF1CAB1C3E8_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_tD23CE53FD3B127402C1A7B73A29C6AF1CAB1C3E8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_tD23CE53FD3B127402C1A7B73A29C6AF1CAB1C3E8_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m76E05730082FD3FB893CFE4ED257BFD0E1146F2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERC1155BalanceOfExample_t2B8ED4BB8CCBFB2EA55003C1E6A0858133B56B5B_CustomAttributesCacheGenerator_ERC1155BalanceOfExample_Start_mF1D7FC086FAF079516A7463211759E19659E3E19(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_t066787A9AC67CB267EF2129A0DF34AA9B4B21043_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_t066787A9AC67CB267EF2129A0DF34AA9B4B21043_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_t066787A9AC67CB267EF2129A0DF34AA9B4B21043_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_t066787A9AC67CB267EF2129A0DF34AA9B4B21043_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_mACD6D05861661B7259297843E6FE635E542144F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERC1155URIExample_t5E72CC882B99CF7E440D0BD9E78FC79D9D27EA70_CustomAttributesCacheGenerator_ERC1155URIExample_Start_m47316F5E9063BA95AB5038A40203C130AE315332(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_t35FD1115B0BC8E36B985418F68FF8380BC1C0E47_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_t35FD1115B0BC8E36B985418F68FF8380BC1C0E47_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_t35FD1115B0BC8E36B985418F68FF8380BC1C0E47_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_t35FD1115B0BC8E36B985418F68FF8380BC1C0E47_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m32F010E483167AED04DE742EB8940EDE54A25941(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERC20BalanceOfExample_t03CAD8603573B5038D9911953021BBB6C3CF17E3_CustomAttributesCacheGenerator_ERC20BalanceOfExample_Start_mFF41E5445321315891D6684596EFF7494EB4CCEA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_tAE2F75CCF52DC3789B48F5B1A2F666C4CF49CDB1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_tAE2F75CCF52DC3789B48F5B1A2F666C4CF49CDB1_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_tAE2F75CCF52DC3789B48F5B1A2F666C4CF49CDB1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_tAE2F75CCF52DC3789B48F5B1A2F666C4CF49CDB1_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m8B9F75B4229353D82C1C4D7F5D9B3D0608653F2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERC20DecimalsExample_t95297FF191243BDC695DC4FB021EFEF97B82B5BF_CustomAttributesCacheGenerator_ERC20DecimalsExample_Start_mF8A5B405AC67AC0E354341E76FD1674DC451DE85(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_t7C1BAAAA07BFB53E3766837DFCF5BC7A3489A733_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_t7C1BAAAA07BFB53E3766837DFCF5BC7A3489A733_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_t7C1BAAAA07BFB53E3766837DFCF5BC7A3489A733_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_t7C1BAAAA07BFB53E3766837DFCF5BC7A3489A733_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m0E9F22BDB96721E7BD11F85CE5F248ABFF1D4399(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERC20NameExample_tF4DC09F14C4B9BF427DBEDC6278C0C791EC286C5_CustomAttributesCacheGenerator_ERC20NameExample_Start_m5E0090E513011383498C5799D006C408A0549FFD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_t3B0BE786C61C58EE0A8283BC4FBCEAE538C03D52_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_t3B0BE786C61C58EE0A8283BC4FBCEAE538C03D52_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_t3B0BE786C61C58EE0A8283BC4FBCEAE538C03D52_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_t3B0BE786C61C58EE0A8283BC4FBCEAE538C03D52_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m5C52A6DA998FC196467CC1A8D7DA3F1B2680784A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERC20SymbolExample_t69754E14BF859CD08B6932B35413B76D0E1A7EB7_CustomAttributesCacheGenerator_ERC20SymbolExample_Start_m44F3237CCD3B3C48CF8216DAADC25FB4C817870A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_t895329D5ABFF19DBE587C0A6208CE8E3B3005492_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_t895329D5ABFF19DBE587C0A6208CE8E3B3005492_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_t895329D5ABFF19DBE587C0A6208CE8E3B3005492_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_t895329D5ABFF19DBE587C0A6208CE8E3B3005492_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m8ECA709670FDBBB8CF6CE3BE68AC77CEE55FEF6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERC20TotalSupplyExample_t840EDDCF049B9AF63D73A64761E35F7045FC60EF_CustomAttributesCacheGenerator_ERC20TotalSupplyExample_Start_mAD965D99BF2EEA5EF43F8B15CC691404AD553AE2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_t764D1A87698DFB37F6819D1127531AAACCFD6BD4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_t764D1A87698DFB37F6819D1127531AAACCFD6BD4_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_t764D1A87698DFB37F6819D1127531AAACCFD6BD4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_t764D1A87698DFB37F6819D1127531AAACCFD6BD4_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m90AE0209DA8EBB3AE0D980935EAEC3783FB842AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERC721BalanceOfExample_tC3B9AF4E7458FB2447E72577DCE748B769F3D1DC_CustomAttributesCacheGenerator_ERC721BalanceOfExample_Start_m5C514F94087DD8AFF72E9D283FF33BD6175B2EF2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_tBEA5CFB545680D57980B2DB70A29312D13795D50_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_tBEA5CFB545680D57980B2DB70A29312D13795D50_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_tBEA5CFB545680D57980B2DB70A29312D13795D50_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_tBEA5CFB545680D57980B2DB70A29312D13795D50_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_mE60DC6B0816FFFA80349E8D40C3CFC426F8001B7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERC721OwnerOfBatchExample_t70FD33F4468647E2AFAF89489B7F61B93EF8F262_CustomAttributesCacheGenerator_ERC721OwnerOfBatchExample_Start_m8CF7A69B7BFA086DD735B18E5A2395F422414DA6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_t7536771EC913216A48637DC08A6230B38924DB2A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_t7536771EC913216A48637DC08A6230B38924DB2A_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_t7536771EC913216A48637DC08A6230B38924DB2A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_t7536771EC913216A48637DC08A6230B38924DB2A_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_mE6B01D55F9D6F1B641203AD045DA10D399ABC770(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERC721OwnerOfExample_tCD51B537726B5EB4294B117FEB90330F4758344F_CustomAttributesCacheGenerator_ERC721OwnerOfExample_Start_m9D7F5468A89DEC19DEF62AD36B527F91BF6792C1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_t99EA47DEF2B18A525F2C23CF25D697879CB47FDF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_t99EA47DEF2B18A525F2C23CF25D697879CB47FDF_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_t99EA47DEF2B18A525F2C23CF25D697879CB47FDF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_t99EA47DEF2B18A525F2C23CF25D697879CB47FDF_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m0ECA16AF5C9507944D8F09DD555E373FC01AF5F7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERC721URIExample_t11F22B29756DA94FBCE2EE709A3755E8D72F73B5_CustomAttributesCacheGenerator_ERC721URIExample_Start_m0EACF752E96AE14675BB65815ACEBA99F6494291(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_t63A6A6BA4375C09E642416F98697963B5F104775_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_t63A6A6BA4375C09E642416F98697963B5F104775_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_t63A6A6BA4375C09E642416F98697963B5F104775_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_t63A6A6BA4375C09E642416F98697963B5F104775_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_mB281CC760C5BC7236B4D097976C68D5CF60EF754(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void AllErc1155Example_tA97B863F2CEFEC5A67625870DB133D572EC856BA_CustomAttributesCacheGenerator_AllErc1155Example_Start_m98450BC383F06E18DD3E997CA40678AA77B3ED45(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__1_t9F1246F65A99648E11CF70FD4A03AC34D963741E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__1_t9F1246F65A99648E11CF70FD4A03AC34D963741E_0_0_0_var), NULL);
	}
}
static void NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_U3CcontractU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_U3CtokenIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_U3CuriU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_U3CbalanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_NFTs_get_contract_m132FBACD650BBB034871A7BEAA4A8A5E69890D65(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_NFTs_set_contract_m5806E56C6DAE7193AB5CD81D9F474D07A35B851E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_NFTs_get_tokenId_m82B87799B5788DA9B1406347C6E7BE76CC00907F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_NFTs_set_tokenId_mEED2527C52CB89056919196F37BB93FC91703C8B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_NFTs_get_uri_m24476236E60F7F31C888F8E68D053AF7D9B88C0A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_NFTs_set_uri_mA7C27EE09FE391D53EA831A4ECE9B915DAE66CBB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_NFTs_get_balance_mCBFB0194B7ABA4A6B584E176B16282A5F2109424(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_NFTs_set_balance_m4045F162A6C0E6616F9AEE022F3204229347A37C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__1_t9F1246F65A99648E11CF70FD4A03AC34D963741E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__1_t9F1246F65A99648E11CF70FD4A03AC34D963741E_CustomAttributesCacheGenerator_U3CStartU3Ed__1_SetStateMachine_mF4BABB9B81366714D470D287C46918A5C6D7EB6A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void AllErc721Example_t2320DACFCCCC75350179C8D5F6DEFEA9EFDB92AB_CustomAttributesCacheGenerator_AllErc721Example_Start_m1173828869CE0CA1368EC96797AB780B26007185(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__1_t69C675AB72ACBBEC9F2AE65937A9BAF7F7F0002B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__1_t69C675AB72ACBBEC9F2AE65937A9BAF7F7F0002B_0_0_0_var), NULL);
	}
}
static void NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_U3CcontractU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_U3CtokenIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_U3CuriU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_U3CbalanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_NFTs_get_contract_mEF0AF77D33D8D1793D68D7BD429C144F2DBECC0D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_NFTs_set_contract_mD64E897A626ADCEE21998CE15BFF69AD5F762634(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_NFTs_get_tokenId_m9EFD2AC01B73E801C3BC8B45FB7268099F8979EF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_NFTs_set_tokenId_m24AE780605FE57573BC4ED1FF58B1890B9C98686(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_NFTs_get_uri_mC235611E28E2B8ACC01BA87194FC197DB55AD436(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_NFTs_set_uri_mAD2F72E336F6B548906B4D9A68BB7CF28FDA325B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_NFTs_get_balance_m9815F7685CFBDFB187A2EBC45307D1353622B4FA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_NFTs_set_balance_m343CFEB6DFF4A498F2127634C5EA9041E505AF37(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__1_t69C675AB72ACBBEC9F2AE65937A9BAF7F7F0002B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__1_t69C675AB72ACBBEC9F2AE65937A9BAF7F7F0002B_CustomAttributesCacheGenerator_U3CStartU3Ed__1_SetStateMachine_mCD7FDC90AA399D1DB295620538CA230DEAB95A1F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BlockNumberExample_t021EE1F41AF9ADCA74474C76FE8A46DE92057BD0_CustomAttributesCacheGenerator_BlockNumberExample_Start_mAE8F8CFCAE848DAA2C9FC0E971741520E7BA3EFD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_tD57E14EDF0D69957DE35D69036FE23E61F69DBA1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_tD57E14EDF0D69957DE35D69036FE23E61F69DBA1_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_tD57E14EDF0D69957DE35D69036FE23E61F69DBA1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_tD57E14EDF0D69957DE35D69036FE23E61F69DBA1_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m1B759B91C18F303ECF8AA29965839045F5FB49C9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CreateContractDataExample_t6D7EBFC5C9DC361C3E0F58B343D2DD451BE42879_CustomAttributesCacheGenerator_CreateContractDataExample_Start_m975FDA0E1478E7283C9B3F95C5D4A825CFCC500E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_t4C79D410A6C828EF826DC1FDC1B980242584FB34_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_t4C79D410A6C828EF826DC1FDC1B980242584FB34_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_t4C79D410A6C828EF826DC1FDC1B980242584FB34_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_t4C79D410A6C828EF826DC1FDC1B980242584FB34_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m90FCB1CDB53828A20360A302E41321FA2915A8B7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CustomCallExample_tEFB7F5467D3F9DAAA06AA730518F596FAA51C956_CustomAttributesCacheGenerator_CustomCallExample_Mint_mA9AF3C5B8DC09D051DB0DB7EE46FC8F3F1ECFB34(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CMintU3Ed__6_tE9A6EE8F8DB7219D7A1127A28A907B7BBB826F9C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CMintU3Ed__6_tE9A6EE8F8DB7219D7A1127A28A907B7BBB826F9C_0_0_0_var), NULL);
	}
}
static void CustomCallExample_tEFB7F5467D3F9DAAA06AA730518F596FAA51C956_CustomAttributesCacheGenerator_CustomCallExample_ChangeName_m11A572AD5E78FF9DBC799B37BB3C7DFE0EE95A34(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CChangeNameU3Ed__8_t6C75EF60C6E66B9BB035E70BB9907A2A7D7553F8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CChangeNameU3Ed__8_t6C75EF60C6E66B9BB035E70BB9907A2A7D7553F8_0_0_0_var), NULL);
	}
}
static void CustomCallExample_tEFB7F5467D3F9DAAA06AA730518F596FAA51C956_CustomAttributesCacheGenerator_CustomCallExample_ReLoad_m9ED3FF4E3B3388BC59DD339CF4E2ADE587DB03FF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CReLoadU3Ed__9_tC6FAF560B8A13260A85E593B82E7660D7792F4AB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CReLoadU3Ed__9_tC6FAF560B8A13260A85E593B82E7660D7792F4AB_0_0_0_var), NULL);
	}
}
static void CustomCallExample_tEFB7F5467D3F9DAAA06AA730518F596FAA51C956_CustomAttributesCacheGenerator_CustomCallExample_GetNft_mB0ABD70FEE9F630BB81163A45BE6C18B72674981(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetNftU3Ed__10_t9EB229B03086CFC54263810ABEE900FCBA6FC85B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CGetNftU3Ed__10_t9EB229B03086CFC54263810ABEE900FCBA6FC85B_0_0_0_var), NULL);
	}
}
static void CustomCallExample_tEFB7F5467D3F9DAAA06AA730518F596FAA51C956_CustomAttributesCacheGenerator_CustomCallExample_Burn_mF950FC3007B36EB9AF4C968F7FFCDDF6B31B09C8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBurnU3Ed__11_t57A44B35853BAEE9C5D70339914EF7D880A488C6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CBurnU3Ed__11_t57A44B35853BAEE9C5D70339914EF7D880A488C6_0_0_0_var), NULL);
	}
}
static void NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_U3CcontractU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_U3CtokenIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_U3CuriU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_U3CbalanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_NFTs_get_contract_mF6232E648620A06789385C55D8180A8CF7464944(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_NFTs_set_contract_m4F35AAB24C4FD78BF470D1F797C126463E38FB60(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_NFTs_get_tokenId_mDF28A5F14194E236C2FF36FF1822BB37FA5F1774(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_NFTs_set_tokenId_m240F331C0FD015FEFCD9B9C8AAF3ABBFD9D38D71(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_NFTs_get_uri_mFFE0E2FAC27B394D476A8ED8A87D3D75F58EA390(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_NFTs_set_uri_mF53EB20E483FA1467C8BCC0764A02EC97B64FDED(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_NFTs_get_balance_m53BCCA62B6C840C1D66DDA2AE8E9D8BB6AFAD2AC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_NFTs_set_balance_m32CAB14E10B0A94AA9A0093C4133AC20E4775BAA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CMintU3Ed__6_tE9A6EE8F8DB7219D7A1127A28A907B7BBB826F9C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CMintU3Ed__6_tE9A6EE8F8DB7219D7A1127A28A907B7BBB826F9C_CustomAttributesCacheGenerator_U3CMintU3Ed__6_SetStateMachine_mE34E1011DA6AE67062B3638D5CA080CB82EE7586(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeNameU3Ed__8_t6C75EF60C6E66B9BB035E70BB9907A2A7D7553F8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CChangeNameU3Ed__8_t6C75EF60C6E66B9BB035E70BB9907A2A7D7553F8_CustomAttributesCacheGenerator_U3CChangeNameU3Ed__8_SetStateMachine_m2F1CDC60D482E2AFCE6FBEA95ECA5648BBE31409(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CReLoadU3Ed__9_tC6FAF560B8A13260A85E593B82E7660D7792F4AB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CReLoadU3Ed__9_tC6FAF560B8A13260A85E593B82E7660D7792F4AB_CustomAttributesCacheGenerator_U3CReLoadU3Ed__9_SetStateMachine_m89009A42D7FAC4B90BC171A10997A50E3AFDE867(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNftU3Ed__10_t9EB229B03086CFC54263810ABEE900FCBA6FC85B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetNftU3Ed__10_t9EB229B03086CFC54263810ABEE900FCBA6FC85B_CustomAttributesCacheGenerator_U3CGetNftU3Ed__10_SetStateMachine_m110A5F8CA3599AE1E562A01BAE0B1194F8801D48(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBurnU3Ed__11_t57A44B35853BAEE9C5D70339914EF7D880A488C6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBurnU3Ed__11_t57A44B35853BAEE9C5D70339914EF7D880A488C6_CustomAttributesCacheGenerator_U3CBurnU3Ed__11_SetStateMachine_m1F8C91D9B818F4B52BF56BBC71398F27DFFF3417(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CustomRPCExample_t84D78666093B2375A29EFD0AC8938A233DD9AF70_CustomAttributesCacheGenerator_CustomRPCExample_Start_mA3CCBB955CEA57395864A8F32683C8763BEFDC33(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_t479A6E16097648A0B10B0738414EAA50C0BF9293_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_t479A6E16097648A0B10B0738414EAA50C0BF9293_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_t479A6E16097648A0B10B0738414EAA50C0BF9293_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_t479A6E16097648A0B10B0738414EAA50C0BF9293_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_mA01B28EE49B96A3BB28545E7375E2E4823E4F158(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void EthBalanceOfExample_tB6A55A007578F79961335084F784DF7EA94301C5_CustomAttributesCacheGenerator_EthBalanceOfExample_Start_m38948E53166841C031BB586EE77C4CC412FAA71F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_tFB2C2E6151A430EC6EA1BC8B0F3539D9FDC5344E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_tFB2C2E6151A430EC6EA1BC8B0F3539D9FDC5344E_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_tFB2C2E6151A430EC6EA1BC8B0F3539D9FDC5344E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_tFB2C2E6151A430EC6EA1BC8B0F3539D9FDC5344E_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m8763265A5E48878995143FE2211C1135DD40A431(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void NonceExample_t3B378A322C855CFF7EAF91F4FDFDE06E54D368A0_CustomAttributesCacheGenerator_NonceExample_Start_mE7130950BE1D260F4460F2F6DD25E5ADF4209C0D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_t64D04A640EF7B7D42ECBFEDD79906834E402A483_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_t64D04A640EF7B7D42ECBFEDD79906834E402A483_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_t64D04A640EF7B7D42ECBFEDD79906834E402A483_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_t64D04A640EF7B7D42ECBFEDD79906834E402A483_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_mE36D9D616EF594199062DEBE0273AC423B4D8347(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TxStatus_tA5AF89A303ED9A45B7F408C4487DC05E1D878820_CustomAttributesCacheGenerator_TxStatus_Start_mFA223A713B485F7F2E598A7EB56054D55BB28DB9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_tB8B66B371E65C03F064F71E432E101E7258A08F6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_tB8B66B371E65C03F064F71E432E101E7258A08F6_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_tB8B66B371E65C03F064F71E432E101E7258A08F6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_tB8B66B371E65C03F064F71E432E101E7258A08F6_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_mF482F4B6CA65515CF4469D1B6BCD7C43F86BB665(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VerifyExample_t6D959548B51F544244727DCD742D28D5DC2C5129_CustomAttributesCacheGenerator_VerifyExample_Start_m00DC30EC005ED55D223D40563C7E5BFA50D78296(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__0_t7925DC9BEAFEB29EB7181BD3B7DF9CBEBEC20FD9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__0_t7925DC9BEAFEB29EB7181BD3B7DF9CBEBEC20FD9_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__0_t7925DC9BEAFEB29EB7181BD3B7DF9CBEBEC20FD9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__0_t7925DC9BEAFEB29EB7181BD3B7DF9CBEBEC20FD9_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m8B1480939675F9433880643CD4EA0C534881209D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Web3PrivateKeySend1155Example_t5E81F3DA30966A941F8836C66FE74FA4E991007B_CustomAttributesCacheGenerator_Web3PrivateKeySend1155Example_OnSend1155_m244501F1769E054C6D39622BD3439BFBA32416CD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnSend1155U3Ed__0_t7C1EF487A95CFD9199044CEAF7184F39BD8EB846_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COnSend1155U3Ed__0_t7C1EF487A95CFD9199044CEAF7184F39BD8EB846_0_0_0_var), NULL);
	}
}
static void U3COnSend1155U3Ed__0_t7C1EF487A95CFD9199044CEAF7184F39BD8EB846_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnSend1155U3Ed__0_t7C1EF487A95CFD9199044CEAF7184F39BD8EB846_CustomAttributesCacheGenerator_U3COnSend1155U3Ed__0_SetStateMachine_m6044558A9818E8F112CEFE3BF401EDE447831294(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Web3PrivateKeySend20Example_t5C28A29420AEA17AB8C3684CF8055680F39E108A_CustomAttributesCacheGenerator_Web3PrivateKeySend20Example_OnSend20_m25112C041C89E4656FB9F97A75A0F4D65E827540(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnSend20U3Ed__0_t19D4015AA5A24D7279A104C542D5B7F63B20E830_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COnSend20U3Ed__0_t19D4015AA5A24D7279A104C542D5B7F63B20E830_0_0_0_var), NULL);
	}
}
static void U3COnSend20U3Ed__0_t19D4015AA5A24D7279A104C542D5B7F63B20E830_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnSend20U3Ed__0_t19D4015AA5A24D7279A104C542D5B7F63B20E830_CustomAttributesCacheGenerator_U3COnSend20U3Ed__0_SetStateMachine_m2459E52710C5D0C1641F042AA1246E79DB87C834(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Web3PrivateKeySend721Example_t7C5B412BD1183BA68937A2567C92210EF0C1FDD3_CustomAttributesCacheGenerator_Web3PrivateKeySend721Example_OnSend721_m7DF532A7644D27B2CBBC1EDDF024613064342487(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnSend721U3Ed__0_t3CC7BEDD0A3A00D6359571BDB9098BA2565172D0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COnSend721U3Ed__0_t3CC7BEDD0A3A00D6359571BDB9098BA2565172D0_0_0_0_var), NULL);
	}
}
static void U3COnSend721U3Ed__0_t3CC7BEDD0A3A00D6359571BDB9098BA2565172D0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnSend721U3Ed__0_t3CC7BEDD0A3A00D6359571BDB9098BA2565172D0_CustomAttributesCacheGenerator_U3COnSend721U3Ed__0_SetStateMachine_m2CDC6C3A888109DBFB4B350C5D3969E5E9805EF3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Web3PrivateKeySendTransactionExample_t51C2B5F484B55FC913465DB5810EFFFFF77F9293_CustomAttributesCacheGenerator_Web3PrivateKeySendTransactionExample_OnSendTransaction_m096857E458B9BFEB768B0BA0088D8C396C14FD63(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnSendTransactionU3Ed__0_tAC21FA78A9334357CD3676F1B36A89938F0526AB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COnSendTransactionU3Ed__0_tAC21FA78A9334357CD3676F1B36A89938F0526AB_0_0_0_var), NULL);
	}
}
static void U3COnSendTransactionU3Ed__0_tAC21FA78A9334357CD3676F1B36A89938F0526AB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnSendTransactionU3Ed__0_tAC21FA78A9334357CD3676F1B36A89938F0526AB_CustomAttributesCacheGenerator_U3COnSendTransactionU3Ed__0_SetStateMachine_m75CF9DD43E6B9372BE208D6144B6D3D389EDD257(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Web3WalletSendTransactionExample_tBACBB9A580022148FAE9EF695B23DA972B8580C1_CustomAttributesCacheGenerator_Web3WalletSendTransactionExample_OnSendTransaction_m4FA8621476FD0880D88D0F52337D40760ED02D06(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnSendTransactionU3Ed__0_t17D92BBC0E0FF116EE91EBA514E78D309F704DF2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COnSendTransactionU3Ed__0_t17D92BBC0E0FF116EE91EBA514E78D309F704DF2_0_0_0_var), NULL);
	}
}
static void U3COnSendTransactionU3Ed__0_t17D92BBC0E0FF116EE91EBA514E78D309F704DF2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnSendTransactionU3Ed__0_t17D92BBC0E0FF116EE91EBA514E78D309F704DF2_CustomAttributesCacheGenerator_U3COnSendTransactionU3Ed__0_SetStateMachine_mA1354644175F2D58C90730B1FDB4869FA7BDFC48(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Web3WalletSignMessageExample_t35AC222FEDCB211ABF34B56B54C9C908CD82426D_CustomAttributesCacheGenerator_Web3WalletSignMessageExample_OnSignMessage_m6A3BA73D6F59E338F04E01A2E3912198F487BBDA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnSignMessageU3Ed__0_tB5523A25CE45CFCAD0F6637DD8C3B64F6E00AB42_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COnSignMessageU3Ed__0_tB5523A25CE45CFCAD0F6637DD8C3B64F6E00AB42_0_0_0_var), NULL);
	}
}
static void U3COnSignMessageU3Ed__0_tB5523A25CE45CFCAD0F6637DD8C3B64F6E00AB42_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnSignMessageU3Ed__0_tB5523A25CE45CFCAD0F6637DD8C3B64F6E00AB42_CustomAttributesCacheGenerator_U3COnSignMessageU3Ed__0_SetStateMachine_m4338BE5F3F9B7C77682650825239AC6C7138A6B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Web3WalletTransfer1155Example_t9A9B8A3CC0FDEEB4EF5607782DC838AC4F4A40F6_CustomAttributesCacheGenerator_Web3WalletTransfer1155Example_OnTransfer1155_m6CC73312B266A7E1B7974E6CB782503B376758E3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnTransfer1155U3Ed__0_t3825AF7253AF38599E73188003F68FD536E30C1A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COnTransfer1155U3Ed__0_t3825AF7253AF38599E73188003F68FD536E30C1A_0_0_0_var), NULL);
	}
}
static void U3COnTransfer1155U3Ed__0_t3825AF7253AF38599E73188003F68FD536E30C1A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnTransfer1155U3Ed__0_t3825AF7253AF38599E73188003F68FD536E30C1A_CustomAttributesCacheGenerator_U3COnTransfer1155U3Ed__0_SetStateMachine_mE5364F3E8E033AA906ACE7657884281076A015C5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Web3WalletTransfer20Example_t6AC6BEA2A4CBFF994913184A51A5E9896DE707CD_CustomAttributesCacheGenerator_Web3WalletTransfer20Example_OnTransfer20_mD19680408F257116D802637BCAFA6AE2D4C049D1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnTransfer20U3Ed__0_t3CD34BBD71FF80DD78C25D0FE992ED9D1353E8F4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COnTransfer20U3Ed__0_t3CD34BBD71FF80DD78C25D0FE992ED9D1353E8F4_0_0_0_var), NULL);
	}
}
static void U3COnTransfer20U3Ed__0_t3CD34BBD71FF80DD78C25D0FE992ED9D1353E8F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnTransfer20U3Ed__0_t3CD34BBD71FF80DD78C25D0FE992ED9D1353E8F4_CustomAttributesCacheGenerator_U3COnTransfer20U3Ed__0_SetStateMachine_mF8F73F2742E7641DF16BA8DAD19AB3A74142E61C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Web3WalletTransfer721Example_t61B0589D45F611520C9C466C30FDE293F7EB4AAC_CustomAttributesCacheGenerator_Web3WalletTransfer721Example_OnTransfer721_mF62FEAA1F0ED77EA1D20ABDDA5F08A19908BD314(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnTransfer721U3Ed__0_t19C7B441C34CB2F433CAD229877558815977CDCC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COnTransfer721U3Ed__0_t19C7B441C34CB2F433CAD229877558815977CDCC_0_0_0_var), NULL);
	}
}
static void U3COnTransfer721U3Ed__0_t19C7B441C34CB2F433CAD229877558815977CDCC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnTransfer721U3Ed__0_t19C7B441C34CB2F433CAD229877558815977CDCC_CustomAttributesCacheGenerator_U3COnTransfer721U3Ed__0_SetStateMachine_mF885565CD1B11CCD1E3057612D11154B7629129D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WebGLSendContractExample_t69A3686A5C09CD04184278A40805D54481117CFB_CustomAttributesCacheGenerator_WebGLSendContractExample_OnSendContract_m1A4B193810A8347EB809FAA076CE10CE962713C2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnSendContractU3Ed__0_t15F35AD460E7321938C2FD75A065307A93876F9C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COnSendContractU3Ed__0_t15F35AD460E7321938C2FD75A065307A93876F9C_0_0_0_var), NULL);
	}
}
static void U3COnSendContractU3Ed__0_t15F35AD460E7321938C2FD75A065307A93876F9C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnSendContractU3Ed__0_t15F35AD460E7321938C2FD75A065307A93876F9C_CustomAttributesCacheGenerator_U3COnSendContractU3Ed__0_SetStateMachine_m7A097F8F1BB131B8A785D2F22D1D28D7527646EA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WebGLSendTransactionExample_tEDB304B208287C836BD9E41249EF21CEE555B62E_CustomAttributesCacheGenerator_WebGLSendTransactionExample_OnSendTransaction_mE9BD097CBD52F83717726BFBB8B55265A7BEA7F8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnSendTransactionU3Ed__0_t2A7D6CDF5773E00BAEA1859E778C6BC14638EA50_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COnSendTransactionU3Ed__0_t2A7D6CDF5773E00BAEA1859E778C6BC14638EA50_0_0_0_var), NULL);
	}
}
static void U3COnSendTransactionU3Ed__0_t2A7D6CDF5773E00BAEA1859E778C6BC14638EA50_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnSendTransactionU3Ed__0_t2A7D6CDF5773E00BAEA1859E778C6BC14638EA50_CustomAttributesCacheGenerator_U3COnSendTransactionU3Ed__0_SetStateMachine_mC12443C49C60F7DCAA5CDA5E716B32E16BD74723(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WebGLSignMessageExample_t6B1C913E8904B2D53E392604E2E19ACCB04905D2_CustomAttributesCacheGenerator_WebGLSignMessageExample_OnSignMessage_mEDA9C904A9601BA4E7A4EBD354297A6DB24A5279(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnSignMessageU3Ed__0_t53C2F5D040A39F6A271091DF3DCEF85F1D9D150A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COnSignMessageU3Ed__0_t53C2F5D040A39F6A271091DF3DCEF85F1D9D150A_0_0_0_var), NULL);
	}
}
static void U3COnSignMessageU3Ed__0_t53C2F5D040A39F6A271091DF3DCEF85F1D9D150A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnSignMessageU3Ed__0_t53C2F5D040A39F6A271091DF3DCEF85F1D9D150A_CustomAttributesCacheGenerator_U3COnSignMessageU3Ed__0_SetStateMachine_m3F0DC2AEF8F20669EFA57A92C67E1F9A5F4B6802(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WebGLTransfer1155_t4B7D5BD7F1B768A116719F1D39FEA9B3276BCB80_CustomAttributesCacheGenerator_contract(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WebGLTransfer1155_t4B7D5BD7F1B768A116719F1D39FEA9B3276BCB80_CustomAttributesCacheGenerator_toAccount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WebGLTransfer1155_t4B7D5BD7F1B768A116719F1D39FEA9B3276BCB80_CustomAttributesCacheGenerator_tokenId(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WebGLTransfer1155_t4B7D5BD7F1B768A116719F1D39FEA9B3276BCB80_CustomAttributesCacheGenerator_amount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WebGLTransfer1155_t4B7D5BD7F1B768A116719F1D39FEA9B3276BCB80_CustomAttributesCacheGenerator_WebGLTransfer1155_SafeTransferFrom_m60027B3FC9E5908D1DAC6D38F279E525F9E69282(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSafeTransferFromU3Ed__5_t7DECABE00F0A217B61D3EBE9046646C3A3C2B0AE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSafeTransferFromU3Ed__5_t7DECABE00F0A217B61D3EBE9046646C3A3C2B0AE_0_0_0_var), NULL);
	}
}
static void U3CSafeTransferFromU3Ed__5_t7DECABE00F0A217B61D3EBE9046646C3A3C2B0AE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSafeTransferFromU3Ed__5_t7DECABE00F0A217B61D3EBE9046646C3A3C2B0AE_CustomAttributesCacheGenerator_U3CSafeTransferFromU3Ed__5_SetStateMachine_m357DFCF039FEA8B6C3395C25DFC5753549AD9156(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WebGLTransfer20_t081FD59CDBAF6AE4314056B8B7B6249964572A0A_CustomAttributesCacheGenerator_contract(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WebGLTransfer20_t081FD59CDBAF6AE4314056B8B7B6249964572A0A_CustomAttributesCacheGenerator_toAccount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WebGLTransfer20_t081FD59CDBAF6AE4314056B8B7B6249964572A0A_CustomAttributesCacheGenerator_amount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WebGLTransfer20_t081FD59CDBAF6AE4314056B8B7B6249964572A0A_CustomAttributesCacheGenerator_WebGLTransfer20_Transfer_mC189BF2DC9623CEED6D1285FFF55B985A9F95FB8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTransferU3Ed__4_t4EB7258C238E19FF60A86837B4E6620F284C8779_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CTransferU3Ed__4_t4EB7258C238E19FF60A86837B4E6620F284C8779_0_0_0_var), NULL);
	}
}
static void U3CTransferU3Ed__4_t4EB7258C238E19FF60A86837B4E6620F284C8779_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTransferU3Ed__4_t4EB7258C238E19FF60A86837B4E6620F284C8779_CustomAttributesCacheGenerator_U3CTransferU3Ed__4_SetStateMachine_mA32D0A2CF0FC0E65AD5EA5081D9B5860C23936DE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WebGLTransfer721_t3DF4C3057637769675C1EFA29FAAF75CE9AA314C_CustomAttributesCacheGenerator_contract(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WebGLTransfer721_t3DF4C3057637769675C1EFA29FAAF75CE9AA314C_CustomAttributesCacheGenerator_toAccount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WebGLTransfer721_t3DF4C3057637769675C1EFA29FAAF75CE9AA314C_CustomAttributesCacheGenerator_tokenId(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WebGLTransfer721_t3DF4C3057637769675C1EFA29FAAF75CE9AA314C_CustomAttributesCacheGenerator_WebGLTransfer721_SafeTransferFrom_m48A1C85515A84E75A7ADB433BD7D6011D307848B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSafeTransferFromU3Ed__4_t3CA8E5B1F971C292BEA3561C1036CA365AC1ECFA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSafeTransferFromU3Ed__4_t3CA8E5B1F971C292BEA3561C1036CA365AC1ECFA_0_0_0_var), NULL);
	}
}
static void U3CSafeTransferFromU3Ed__4_t3CA8E5B1F971C292BEA3561C1036CA365AC1ECFA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSafeTransferFromU3Ed__4_t3CA8E5B1F971C292BEA3561C1036CA365AC1ECFA_CustomAttributesCacheGenerator_U3CSafeTransferFromU3Ed__4_SetStateMachine_m72420EB4DE07C850B47609F4910E992BFAA0CC10(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WalletLogin_t610F7E3F09279AAA049091289432790647D92F5F_CustomAttributesCacheGenerator_WalletLogin_OnLogin_mADEC61B177A2617C428988B3EC727ED17679DE7D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnLoginU3Ed__2_t566FE4C2C513D969330BA5B1E33B3C977A5C7522_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COnLoginU3Ed__2_t566FE4C2C513D969330BA5B1E33B3C977A5C7522_0_0_0_var), NULL);
	}
}
static void U3COnLoginU3Ed__2_t566FE4C2C513D969330BA5B1E33B3C977A5C7522_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnLoginU3Ed__2_t566FE4C2C513D969330BA5B1E33B3C977A5C7522_CustomAttributesCacheGenerator_U3COnLoginU3Ed__2_SetStateMachine_mB0FF2BA206BB13E75568802ED92D9C2A4609EBC1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WebLogin_tF88C4CA5F48CD5D6A1973176EAB073A5AD819BBD_CustomAttributesCacheGenerator_WebLogin_OnConnected_mBE42B60A0B735E4C2ABB34614D0D194F987BCD8B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnConnectedU3Ed__6_tEFC9E3F446B694229C87DDAF8E60AD510F0C5F1A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COnConnectedU3Ed__6_tEFC9E3F446B694229C87DDAF8E60AD510F0C5F1A_0_0_0_var), NULL);
	}
}
static void U3COnConnectedU3Ed__6_tEFC9E3F446B694229C87DDAF8E60AD510F0C5F1A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnConnectedU3Ed__6_tEFC9E3F446B694229C87DDAF8E60AD510F0C5F1A_CustomAttributesCacheGenerator_U3COnConnectedU3Ed__6_SetStateMachine_mCDC0D60876FA7F6803612569D8BCB444418EC9D9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GeneratedNetworkCode_tA5DCDE73EABE64446CC3985FE891D3CA9B55A51E_CustomAttributesCacheGenerator_GeneratedNetworkCode_InitReadWriters_mF3F676E480CD76ED8819C76E79234EED4737891B(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 1LL, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[297] = 
{
	U3CStartU3Ed__3_tD11F74ED0C86C8C74ECB51A28C9E277FBE93327F_CustomAttributesCacheGenerator,
	U3CCountDownU3Ed__6_tE8E3E327359764B263938683CA50A40694172FBA_CustomAttributesCacheGenerator,
	U3CStartU3Ed__1_t1B368FB4F5A05F99FFFEAAC39C443FDA0DDEEFE0_CustomAttributesCacheGenerator,
	U3CBalanceOfU3Ed__1_tB473BB3392EB81C7699C038E4F719E2403237CF9_CustomAttributesCacheGenerator,
	U3CBalanceOfBatchU3Ed__2_t60644C76ADC9E5B161B500CB35FEBCA663D9EAF3_CustomAttributesCacheGenerator,
	U3CURIU3Ed__3_tDA723E745AD248E1E1036527792AB1C993E3F182_CustomAttributesCacheGenerator,
	U3CBalanceOfU3Ed__1_tDC23312279CD10EC9C92EDA7B353808B3F7E87FE_CustomAttributesCacheGenerator,
	U3CNameU3Ed__2_tC680925E6D11B7981763BED04F1D122A139990F1_CustomAttributesCacheGenerator,
	U3CSymbolU3Ed__3_tF808464A58E53DB372CC85620EE1D0D90241B5B8_CustomAttributesCacheGenerator,
	U3CDecimalsU3Ed__4_tB98D99A8A8CF0A91CDD2EC24910036603A583104_CustomAttributesCacheGenerator,
	U3CTotalSupplyU3Ed__5_t7308A33FAC257AD518D84B19A3FCAB415CE71D47_CustomAttributesCacheGenerator,
	U3CBalanceOfU3Ed__1_t596B95FA265D5B86832E6DC75F164259A19E82BC_CustomAttributesCacheGenerator,
	U3COwnerOfU3Ed__2_t31BF925729D400CFF07A1A743984FE04A1432167_CustomAttributesCacheGenerator,
	U3COwnerOfBatchU3Ed__3_tCBB2433BB34B3DABB1D9A161C46FDBC0039AEE87_CustomAttributesCacheGenerator,
	U3CURIU3Ed__4_tDB8FC11B83C01A3F00B4B4E522A83AA0BAC5E67F_CustomAttributesCacheGenerator,
	U3CBalanceOfU3Ed__2_t68638B7AF8B246775112352588CC72C561347885_CustomAttributesCacheGenerator,
	U3CVerifyU3Ed__3_t31E1C48BC886D4D42666C1483040ACB8058C4A72_CustomAttributesCacheGenerator,
	U3CCallU3Ed__4_t57CA061C0ACBEC239ADBE086F5510845CDAB5F95_CustomAttributesCacheGenerator,
	U3CMultiCallU3Ed__5_t0621A0209591B34AF876B719A10D05583E968D0A_CustomAttributesCacheGenerator,
	U3CTxStatusU3Ed__6_tD683ED7E193CE34112C120522E9F294244B46E8A_CustomAttributesCacheGenerator,
	U3CBlockNumberU3Ed__7_t706EE349544B8B7491CA7F3F882FBE0E2AF3E9D8_CustomAttributesCacheGenerator,
	U3CNonceU3Ed__8_tEA3B84C8E2FFF958D8C9AD0C080BA0997375135C_CustomAttributesCacheGenerator,
	U3CCreateContractDataU3Ed__9_t8E75059EF4B0F6C3DE517A4AF94B8F24E8FF8043_CustomAttributesCacheGenerator,
	U3CAllErc721U3Ed__10_t0745493CAF65FB4DF6B59F47EAC92C305AA41C89_CustomAttributesCacheGenerator,
	U3CAllErc1155U3Ed__11_tA168E7380E452F6602375A9173BC179B94FC4953_CustomAttributesCacheGenerator,
	U3CGasPriceU3Ed__12_tCA0EA62CF30ED62EEF1902A4C2D5FA332773FD35_CustomAttributesCacheGenerator,
	U3CGasLimitU3Ed__13_tB59F6B110FC57A64542891D9CE84ADC2CB4BDBE1_CustomAttributesCacheGenerator,
	U3CChainIdU3Ed__14_t01BB10AB132A673B66ED82C5BDC825A957E3B042_CustomAttributesCacheGenerator,
	U3CCreateTransactionU3Ed__15_t937D21D1ADE37224525F3F71AF50134AE63B7474_CustomAttributesCacheGenerator,
	U3CBroadcastTransactionU3Ed__16_tA29C760B3B0769972DD4485195B59F7C54E69868_CustomAttributesCacheGenerator,
	U3CSendTransactionU3Ed__1_t4CDF5AD9BA9B4AF6F817B240705BB2224D0C98B3_CustomAttributesCacheGenerator,
	U3CSignU3Ed__2_t0540A44BE5A31AB48B727FE4B61366B5186F08F3_CustomAttributesCacheGenerator,
	U3CSendContractU3Ed__10_tAF6DDE291FA77D4A2E25DF8CA6735982A5DB53AF_CustomAttributesCacheGenerator,
	U3CSendTransactionU3Ed__11_tB60AB0245CE1C4FDBCB87250537366F097BE2873_CustomAttributesCacheGenerator,
	U3CSignU3Ed__12_t4804C297784B13A88FD729B8A08FE6D404F3E13B_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_tD23CE53FD3B127402C1A7B73A29C6AF1CAB1C3E8_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_t066787A9AC67CB267EF2129A0DF34AA9B4B21043_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_t35FD1115B0BC8E36B985418F68FF8380BC1C0E47_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_tAE2F75CCF52DC3789B48F5B1A2F666C4CF49CDB1_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_t7C1BAAAA07BFB53E3766837DFCF5BC7A3489A733_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_t3B0BE786C61C58EE0A8283BC4FBCEAE538C03D52_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_t895329D5ABFF19DBE587C0A6208CE8E3B3005492_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_t764D1A87698DFB37F6819D1127531AAACCFD6BD4_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_tBEA5CFB545680D57980B2DB70A29312D13795D50_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_t7536771EC913216A48637DC08A6230B38924DB2A_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_t99EA47DEF2B18A525F2C23CF25D697879CB47FDF_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_t63A6A6BA4375C09E642416F98697963B5F104775_CustomAttributesCacheGenerator,
	U3CStartU3Ed__1_t9F1246F65A99648E11CF70FD4A03AC34D963741E_CustomAttributesCacheGenerator,
	U3CStartU3Ed__1_t69C675AB72ACBBEC9F2AE65937A9BAF7F7F0002B_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_tD57E14EDF0D69957DE35D69036FE23E61F69DBA1_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_t4C79D410A6C828EF826DC1FDC1B980242584FB34_CustomAttributesCacheGenerator,
	U3CMintU3Ed__6_tE9A6EE8F8DB7219D7A1127A28A907B7BBB826F9C_CustomAttributesCacheGenerator,
	U3CChangeNameU3Ed__8_t6C75EF60C6E66B9BB035E70BB9907A2A7D7553F8_CustomAttributesCacheGenerator,
	U3CReLoadU3Ed__9_tC6FAF560B8A13260A85E593B82E7660D7792F4AB_CustomAttributesCacheGenerator,
	U3CGetNftU3Ed__10_t9EB229B03086CFC54263810ABEE900FCBA6FC85B_CustomAttributesCacheGenerator,
	U3CBurnU3Ed__11_t57A44B35853BAEE9C5D70339914EF7D880A488C6_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_t479A6E16097648A0B10B0738414EAA50C0BF9293_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_tFB2C2E6151A430EC6EA1BC8B0F3539D9FDC5344E_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_t64D04A640EF7B7D42ECBFEDD79906834E402A483_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_tB8B66B371E65C03F064F71E432E101E7258A08F6_CustomAttributesCacheGenerator,
	U3CStartU3Ed__0_t7925DC9BEAFEB29EB7181BD3B7DF9CBEBEC20FD9_CustomAttributesCacheGenerator,
	U3COnSend1155U3Ed__0_t7C1EF487A95CFD9199044CEAF7184F39BD8EB846_CustomAttributesCacheGenerator,
	U3COnSend20U3Ed__0_t19D4015AA5A24D7279A104C542D5B7F63B20E830_CustomAttributesCacheGenerator,
	U3COnSend721U3Ed__0_t3CC7BEDD0A3A00D6359571BDB9098BA2565172D0_CustomAttributesCacheGenerator,
	U3COnSendTransactionU3Ed__0_tAC21FA78A9334357CD3676F1B36A89938F0526AB_CustomAttributesCacheGenerator,
	U3COnSendTransactionU3Ed__0_t17D92BBC0E0FF116EE91EBA514E78D309F704DF2_CustomAttributesCacheGenerator,
	U3COnSignMessageU3Ed__0_tB5523A25CE45CFCAD0F6637DD8C3B64F6E00AB42_CustomAttributesCacheGenerator,
	U3COnTransfer1155U3Ed__0_t3825AF7253AF38599E73188003F68FD536E30C1A_CustomAttributesCacheGenerator,
	U3COnTransfer20U3Ed__0_t3CD34BBD71FF80DD78C25D0FE992ED9D1353E8F4_CustomAttributesCacheGenerator,
	U3COnTransfer721U3Ed__0_t19C7B441C34CB2F433CAD229877558815977CDCC_CustomAttributesCacheGenerator,
	U3COnSendContractU3Ed__0_t15F35AD460E7321938C2FD75A065307A93876F9C_CustomAttributesCacheGenerator,
	U3COnSendTransactionU3Ed__0_t2A7D6CDF5773E00BAEA1859E778C6BC14638EA50_CustomAttributesCacheGenerator,
	U3COnSignMessageU3Ed__0_t53C2F5D040A39F6A271091DF3DCEF85F1D9D150A_CustomAttributesCacheGenerator,
	U3CSafeTransferFromU3Ed__5_t7DECABE00F0A217B61D3EBE9046646C3A3C2B0AE_CustomAttributesCacheGenerator,
	U3CTransferU3Ed__4_t4EB7258C238E19FF60A86837B4E6620F284C8779_CustomAttributesCacheGenerator,
	U3CSafeTransferFromU3Ed__4_t3CA8E5B1F971C292BEA3561C1036CA365AC1ECFA_CustomAttributesCacheGenerator,
	U3COnLoginU3Ed__2_t566FE4C2C513D969330BA5B1E33B3C977A5C7522_CustomAttributesCacheGenerator,
	U3COnConnectedU3Ed__6_tEFC9E3F446B694229C87DDAF8E60AD510F0C5F1A_CustomAttributesCacheGenerator,
	Controlador_tCBF1E76EC0EDC20B5276019371D9C1E65DB1084C_CustomAttributesCacheGenerator_U3CassetU3Ek__BackingField,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_vida,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_Stock,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_winner,
	gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_TimeToStart,
	gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_GameStarted,
	NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_U3CcontractU3Ek__BackingField,
	NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_U3CtokenIdU3Ek__BackingField,
	NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_U3CuriU3Ek__BackingField,
	NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_U3CbalanceU3Ek__BackingField,
	NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_U3CcontractU3Ek__BackingField,
	NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_U3CtokenIdU3Ek__BackingField,
	NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_U3CuriU3Ek__BackingField,
	NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_U3CbalanceU3Ek__BackingField,
	NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_U3CcontractU3Ek__BackingField,
	NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_U3CtokenIdU3Ek__BackingField,
	NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_U3CuriU3Ek__BackingField,
	NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_U3CbalanceU3Ek__BackingField,
	WebGLTransfer1155_t4B7D5BD7F1B768A116719F1D39FEA9B3276BCB80_CustomAttributesCacheGenerator_contract,
	WebGLTransfer1155_t4B7D5BD7F1B768A116719F1D39FEA9B3276BCB80_CustomAttributesCacheGenerator_toAccount,
	WebGLTransfer1155_t4B7D5BD7F1B768A116719F1D39FEA9B3276BCB80_CustomAttributesCacheGenerator_tokenId,
	WebGLTransfer1155_t4B7D5BD7F1B768A116719F1D39FEA9B3276BCB80_CustomAttributesCacheGenerator_amount,
	WebGLTransfer20_t081FD59CDBAF6AE4314056B8B7B6249964572A0A_CustomAttributesCacheGenerator_contract,
	WebGLTransfer20_t081FD59CDBAF6AE4314056B8B7B6249964572A0A_CustomAttributesCacheGenerator_toAccount,
	WebGLTransfer20_t081FD59CDBAF6AE4314056B8B7B6249964572A0A_CustomAttributesCacheGenerator_amount,
	WebGLTransfer721_t3DF4C3057637769675C1EFA29FAAF75CE9AA314C_CustomAttributesCacheGenerator_contract,
	WebGLTransfer721_t3DF4C3057637769675C1EFA29FAAF75CE9AA314C_CustomAttributesCacheGenerator_toAccount,
	WebGLTransfer721_t3DF4C3057637769675C1EFA29FAAF75CE9AA314C_CustomAttributesCacheGenerator_tokenId,
	Controlador_tCBF1E76EC0EDC20B5276019371D9C1E65DB1084C_CustomAttributesCacheGenerator_Controlador_get_asset_m365D9F2CF8A3FA0841A4E58BB93A6DA4CFBDFD85,
	gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_gameManager_Start_m169616D522718461F2522FC60796972765513C0B,
	gameManager_t2A9919D7CB9773B3211A72C5DA02B305DE63B8AB_CustomAttributesCacheGenerator_gameManager_CountDown_mD4C9C89C32375A504A19CDF76CD14CF08E839C5F,
	U3CStartU3Ed__3_tD11F74ED0C86C8C74ECB51A28C9E277FBE93327F_CustomAttributesCacheGenerator_U3CStartU3Ed__3__ctor_mB48F4FAA9459FC08D7816D6B374F86776F614EF7,
	U3CStartU3Ed__3_tD11F74ED0C86C8C74ECB51A28C9E277FBE93327F_CustomAttributesCacheGenerator_U3CStartU3Ed__3_System_IDisposable_Dispose_m93BAFC6EC8543E89F44F2387FB1A74F02B9BD27C,
	U3CStartU3Ed__3_tD11F74ED0C86C8C74ECB51A28C9E277FBE93327F_CustomAttributesCacheGenerator_U3CStartU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2B347A5018C26527FE70DD242A8E9AE4FB19ACA5,
	U3CStartU3Ed__3_tD11F74ED0C86C8C74ECB51A28C9E277FBE93327F_CustomAttributesCacheGenerator_U3CStartU3Ed__3_System_Collections_IEnumerator_Reset_mD0096DF92FFDA797AEEE4E72E2129471B234813D,
	U3CStartU3Ed__3_tD11F74ED0C86C8C74ECB51A28C9E277FBE93327F_CustomAttributesCacheGenerator_U3CStartU3Ed__3_System_Collections_IEnumerator_get_Current_mCC8C0917AA7FAC87174852D1E5818DA0DB4416C0,
	U3CCountDownU3Ed__6_tE8E3E327359764B263938683CA50A40694172FBA_CustomAttributesCacheGenerator_U3CCountDownU3Ed__6__ctor_mC57A48E6DDD09DA6E118A55129926012973577CA,
	U3CCountDownU3Ed__6_tE8E3E327359764B263938683CA50A40694172FBA_CustomAttributesCacheGenerator_U3CCountDownU3Ed__6_System_IDisposable_Dispose_m2C308B8820BEC4319EBD920D21012B4FD9150C86,
	U3CCountDownU3Ed__6_tE8E3E327359764B263938683CA50A40694172FBA_CustomAttributesCacheGenerator_U3CCountDownU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8389CB86F7B02C0EEA1C0B9AB4153727D26C6DCF,
	U3CCountDownU3Ed__6_tE8E3E327359764B263938683CA50A40694172FBA_CustomAttributesCacheGenerator_U3CCountDownU3Ed__6_System_Collections_IEnumerator_Reset_mDCCF81548FBBEB13791A835C0C3D5A9DC02DC371,
	U3CCountDownU3Ed__6_tE8E3E327359764B263938683CA50A40694172FBA_CustomAttributesCacheGenerator_U3CCountDownU3Ed__6_System_Collections_IEnumerator_get_Current_m9D45A9FB927FF4F09F7A7D3CF3E0310A8444C439,
	ImportNFTTextureExample_tEB4BB9B467014822B3E60E0FB79FDBFDD7ADE317_CustomAttributesCacheGenerator_ImportNFTTextureExample_Start_mBBAA36C72E5675E8A026135493CC4EC689D13C28,
	U3CStartU3Ed__1_t1B368FB4F5A05F99FFFEAAC39C443FDA0DDEEFE0_CustomAttributesCacheGenerator_U3CStartU3Ed__1_SetStateMachine_m72770C78EEEB2DFDB98D0015DFA3CB7FE724D5E3,
	ERC1155_tB77D8C560189DC5911A80997D6230820826F50F0_CustomAttributesCacheGenerator_ERC1155_BalanceOf_m6E609734004053402859FA732EB12BD113F5BBC9,
	ERC1155_tB77D8C560189DC5911A80997D6230820826F50F0_CustomAttributesCacheGenerator_ERC1155_BalanceOfBatch_m976C311032DE4CAB615A4BC0FC2F57669A5D04DA,
	ERC1155_tB77D8C560189DC5911A80997D6230820826F50F0_CustomAttributesCacheGenerator_ERC1155_URI_mA6C8E3B39B71865362F93F5641353858238A7341,
	U3CBalanceOfU3Ed__1_tB473BB3392EB81C7699C038E4F719E2403237CF9_CustomAttributesCacheGenerator_U3CBalanceOfU3Ed__1_SetStateMachine_m6AF4E4155ABC37A3A94695D78B0A266B91662C55,
	U3CBalanceOfBatchU3Ed__2_t60644C76ADC9E5B161B500CB35FEBCA663D9EAF3_CustomAttributesCacheGenerator_U3CBalanceOfBatchU3Ed__2_SetStateMachine_m34749E2FCC62C6089CA7D14ECC5008103D62EB8E,
	U3CURIU3Ed__3_tDA723E745AD248E1E1036527792AB1C993E3F182_CustomAttributesCacheGenerator_U3CURIU3Ed__3_SetStateMachine_mF1974E58996F7E03BED741B0EE6CB3416B2ACE72,
	ERC20_tAA7C8F643B8DC535D3E94C1C7C99A6E6B105D689_CustomAttributesCacheGenerator_ERC20_BalanceOf_mEC315C4821D0098701B261B0BEBC1A2C5B6653D6,
	ERC20_tAA7C8F643B8DC535D3E94C1C7C99A6E6B105D689_CustomAttributesCacheGenerator_ERC20_Name_m5535F0329E289B03EA6922420FCFA008C077831C,
	ERC20_tAA7C8F643B8DC535D3E94C1C7C99A6E6B105D689_CustomAttributesCacheGenerator_ERC20_Symbol_m1AD978D77261C364EF258C1FB93BE8071F00B026,
	ERC20_tAA7C8F643B8DC535D3E94C1C7C99A6E6B105D689_CustomAttributesCacheGenerator_ERC20_Decimals_m3F46DAA1CEDFE86A488FCF1A2BC10AE4E7FFB4FB,
	ERC20_tAA7C8F643B8DC535D3E94C1C7C99A6E6B105D689_CustomAttributesCacheGenerator_ERC20_TotalSupply_mF278916C040D2B71EC160E87AC1853B0EBEB245B,
	U3CBalanceOfU3Ed__1_tDC23312279CD10EC9C92EDA7B353808B3F7E87FE_CustomAttributesCacheGenerator_U3CBalanceOfU3Ed__1_SetStateMachine_m8699DFD19EEB93AED6E7AA39FDB723211AE5BCB9,
	U3CNameU3Ed__2_tC680925E6D11B7981763BED04F1D122A139990F1_CustomAttributesCacheGenerator_U3CNameU3Ed__2_SetStateMachine_m49C47D68B3C839B84D14583B1166CE6CF3CCA7B7,
	U3CSymbolU3Ed__3_tF808464A58E53DB372CC85620EE1D0D90241B5B8_CustomAttributesCacheGenerator_U3CSymbolU3Ed__3_SetStateMachine_mB4F3CE1D6541161B57F0FF7DC4AD7695C09BD8E1,
	U3CDecimalsU3Ed__4_tB98D99A8A8CF0A91CDD2EC24910036603A583104_CustomAttributesCacheGenerator_U3CDecimalsU3Ed__4_SetStateMachine_mE9819F1B7E958708201C635D9DA17E66020F1BC0,
	U3CTotalSupplyU3Ed__5_t7308A33FAC257AD518D84B19A3FCAB415CE71D47_CustomAttributesCacheGenerator_U3CTotalSupplyU3Ed__5_SetStateMachine_m2B452102301436127D184A33B558C339BE18146B,
	ERC721_tB84643377A52CC25F7A1340587AEE43033B9E8FE_CustomAttributesCacheGenerator_ERC721_BalanceOf_mF1062240718E051ABFE8F82957509C43F8DFD912,
	ERC721_tB84643377A52CC25F7A1340587AEE43033B9E8FE_CustomAttributesCacheGenerator_ERC721_OwnerOf_m7860416DDCB041D40D7AFB7D06D6CDC1F4841EE1,
	ERC721_tB84643377A52CC25F7A1340587AEE43033B9E8FE_CustomAttributesCacheGenerator_ERC721_OwnerOfBatch_mE72D33FAF1E47F507ECDD68E91388EF9031C385C,
	ERC721_tB84643377A52CC25F7A1340587AEE43033B9E8FE_CustomAttributesCacheGenerator_ERC721_URI_mA39E5DFAE098BA075A61C93AF8FC02BE5E29FB75,
	U3CBalanceOfU3Ed__1_t596B95FA265D5B86832E6DC75F164259A19E82BC_CustomAttributesCacheGenerator_U3CBalanceOfU3Ed__1_SetStateMachine_mD5B12D1AB4C0AE02E3EFC2E56D0E9A69C4A7C0C7,
	U3COwnerOfU3Ed__2_t31BF925729D400CFF07A1A743984FE04A1432167_CustomAttributesCacheGenerator_U3COwnerOfU3Ed__2_SetStateMachine_mB9A361356148E8EC7BB543DF7E2F58122EF5685C,
	U3COwnerOfBatchU3Ed__3_tCBB2433BB34B3DABB1D9A161C46FDBC0039AEE87_CustomAttributesCacheGenerator_U3COwnerOfBatchU3Ed__3_SetStateMachine_m94E80236D24084C606B00DAC402B69E5AB05B06A,
	U3CURIU3Ed__4_tDB8FC11B83C01A3F00B4B4E522A83AA0BAC5E67F_CustomAttributesCacheGenerator_U3CURIU3Ed__4_SetStateMachine_m83FEB5116327CB0CD5BD2F04AF3E440AA2E52DEA,
	EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_BalanceOf_m6AF5C0DA67C45C9977B4AAC2C249A36FD84D559E,
	EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_Verify_mA8C92E16EF841C6356A849C6E29765534B0EA2CB,
	EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_Call_m8760C42E4C727A97B7DF290D887E292460F142C4,
	EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_MultiCall_m45EA95D0FC82EFF50087446F4ED7C4D407F0B43D,
	EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_TxStatus_mB28FBAF141DDCD343020787DD04064C0C5993E95,
	EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_BlockNumber_m70A74E4A04CAE5B3B50FFA11AFEE0DF6A6A7EC4A,
	EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_Nonce_m47224BD603FC6ADE4473C6E4499791AC55A46BFF,
	EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_CreateContractData_m0A69CC06452FBB76AC259D74C32CCC81DBC02A92,
	EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_AllErc721_m0556274DB1637D6A25E1414A24D33CBA8AD95379,
	EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_AllErc1155_mF8CC99C847366BCE65363096D2018749003520D6,
	EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_GasPrice_mFFCC58B0047F8FABEE42963AEB67979F814C23A6,
	EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_GasLimit_mC9C0175B2B65072058C8DA29E1656FD3676E14B3,
	EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_ChainId_m5A37B376BE531801A4C4984896B593413935FEA1,
	EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_CreateTransaction_m9DB2168641491273CE7C782CA1DC042EFD38A6A8,
	EVM_t4CB9F43860ADE16A97838AAEA17067F24EB140AF_CustomAttributesCacheGenerator_EVM_BroadcastTransaction_mC3C2718C12BABE0A104C2D9FE68099FF1DD14227,
	U3CBalanceOfU3Ed__2_t68638B7AF8B246775112352588CC72C561347885_CustomAttributesCacheGenerator_U3CBalanceOfU3Ed__2_SetStateMachine_mD8F5C1681185A6E89A41F1980CE6AABD3050AF10,
	U3CVerifyU3Ed__3_t31E1C48BC886D4D42666C1483040ACB8058C4A72_CustomAttributesCacheGenerator_U3CVerifyU3Ed__3_SetStateMachine_m477762C3E935EB66E24F0F2FF4564273024EC780,
	U3CCallU3Ed__4_t57CA061C0ACBEC239ADBE086F5510845CDAB5F95_CustomAttributesCacheGenerator_U3CCallU3Ed__4_SetStateMachine_m2B4C2DDF94B74ACCA651A9AC53F1CA67853C059F,
	U3CMultiCallU3Ed__5_t0621A0209591B34AF876B719A10D05583E968D0A_CustomAttributesCacheGenerator_U3CMultiCallU3Ed__5_SetStateMachine_m77A035AB7DEC5D4342A4617899A02C20AB0E8733,
	U3CTxStatusU3Ed__6_tD683ED7E193CE34112C120522E9F294244B46E8A_CustomAttributesCacheGenerator_U3CTxStatusU3Ed__6_SetStateMachine_m6A59F5AB3D2A6077CF1BA0D1E8353FDEADC0D754,
	U3CBlockNumberU3Ed__7_t706EE349544B8B7491CA7F3F882FBE0E2AF3E9D8_CustomAttributesCacheGenerator_U3CBlockNumberU3Ed__7_SetStateMachine_mBFBF55F1191EB7F3D04C204A5FFC7053EF8C8200,
	U3CNonceU3Ed__8_tEA3B84C8E2FFF958D8C9AD0C080BA0997375135C_CustomAttributesCacheGenerator_U3CNonceU3Ed__8_SetStateMachine_m6C0E5BDD185012D9EE78EC7E73F9DB5448B13C1D,
	U3CCreateContractDataU3Ed__9_t8E75059EF4B0F6C3DE517A4AF94B8F24E8FF8043_CustomAttributesCacheGenerator_U3CCreateContractDataU3Ed__9_SetStateMachine_mC2A74C0432A6E8621E3D510878A7EBB59BA77B96,
	U3CAllErc721U3Ed__10_t0745493CAF65FB4DF6B59F47EAC92C305AA41C89_CustomAttributesCacheGenerator_U3CAllErc721U3Ed__10_SetStateMachine_mC7F79A8B56C459462E982BFB2007442CB5E77327,
	U3CAllErc1155U3Ed__11_tA168E7380E452F6602375A9173BC179B94FC4953_CustomAttributesCacheGenerator_U3CAllErc1155U3Ed__11_SetStateMachine_m09EBEC3D74A283E912739821BBC146F9A9306B00,
	U3CGasPriceU3Ed__12_tCA0EA62CF30ED62EEF1902A4C2D5FA332773FD35_CustomAttributesCacheGenerator_U3CGasPriceU3Ed__12_SetStateMachine_mD8A5388148CA31A2177C494B2BF4ACAE993566C7,
	U3CGasLimitU3Ed__13_tB59F6B110FC57A64542891D9CE84ADC2CB4BDBE1_CustomAttributesCacheGenerator_U3CGasLimitU3Ed__13_SetStateMachine_mAAB510EC434AE95BC2910728023F47955EA53564,
	U3CChainIdU3Ed__14_t01BB10AB132A673B66ED82C5BDC825A957E3B042_CustomAttributesCacheGenerator_U3CChainIdU3Ed__14_SetStateMachine_mC3320A62EFF445C636E3E0CBE73AA0C5272DA035,
	U3CCreateTransactionU3Ed__15_t937D21D1ADE37224525F3F71AF50134AE63B7474_CustomAttributesCacheGenerator_U3CCreateTransactionU3Ed__15_SetStateMachine_m07800DBAA5A71F467B51548B88E44168704663BD,
	U3CBroadcastTransactionU3Ed__16_tA29C760B3B0769972DD4485195B59F7C54E69868_CustomAttributesCacheGenerator_U3CBroadcastTransactionU3Ed__16_SetStateMachine_m6534FEE9D7F78D07C18917E9259F894B414C1E0E,
	Web3Wallet_t981201C98C5E889969444984CC19BAF5B46A4913_CustomAttributesCacheGenerator_Web3Wallet_SendTransaction_mF2426B50D053EF9F8A0D3A6BD8C5A054015D3510,
	Web3Wallet_t981201C98C5E889969444984CC19BAF5B46A4913_CustomAttributesCacheGenerator_Web3Wallet_Sign_m9097546A29A5FBA1F0114EC95EF7097EC0FD8D8A,
	U3CSendTransactionU3Ed__1_t4CDF5AD9BA9B4AF6F817B240705BB2224D0C98B3_CustomAttributesCacheGenerator_U3CSendTransactionU3Ed__1_SetStateMachine_m6C58AE8B37667F289A5318FDC6D296A10C72431B,
	U3CSignU3Ed__2_t0540A44BE5A31AB48B727FE4B61366B5186F08F3_CustomAttributesCacheGenerator_U3CSignU3Ed__2_SetStateMachine_m80954B22F85344CA5FC5AA3D47EB652446A8DA40,
	Web3GL_tD6EF7423AA9552FE71D1B906BD23FF96D9634662_CustomAttributesCacheGenerator_Web3GL_SendContract_m6675BF158272EAC923D284D5CC10C7F09558E173,
	Web3GL_tD6EF7423AA9552FE71D1B906BD23FF96D9634662_CustomAttributesCacheGenerator_Web3GL_SendTransaction_mD40DBCA64188F5159A42BE864CA19D5B61C7F5D9,
	Web3GL_tD6EF7423AA9552FE71D1B906BD23FF96D9634662_CustomAttributesCacheGenerator_Web3GL_Sign_mFEAE31488340360AB008F8E3FAE799D1E10871C7,
	U3CSendContractU3Ed__10_tAF6DDE291FA77D4A2E25DF8CA6735982A5DB53AF_CustomAttributesCacheGenerator_U3CSendContractU3Ed__10_SetStateMachine_mDB924150509FEA19525D41521D4EFFBB8CA8C99C,
	U3CSendTransactionU3Ed__11_tB60AB0245CE1C4FDBCB87250537366F097BE2873_CustomAttributesCacheGenerator_U3CSendTransactionU3Ed__11_SetStateMachine_m324E10096F6699BD39FB4AB1847403016F77A827,
	U3CSignU3Ed__12_t4804C297784B13A88FD729B8A08FE6D404F3E13B_CustomAttributesCacheGenerator_U3CSignU3Ed__12_SetStateMachine_mBAA214792AF8C334AC1EDA56E1C0A9B805A5C267,
	ERC1155BalanceOfBatchExample_tA8D88B5A9FAF21F12EFE356CEBFDD07E78C229A7_CustomAttributesCacheGenerator_ERC1155BalanceOfBatchExample_Start_mB2D18D5D03FFF898BCCD553ED52C25472DDA46A2,
	U3CStartU3Ed__0_tD23CE53FD3B127402C1A7B73A29C6AF1CAB1C3E8_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m76E05730082FD3FB893CFE4ED257BFD0E1146F2A,
	ERC1155BalanceOfExample_t2B8ED4BB8CCBFB2EA55003C1E6A0858133B56B5B_CustomAttributesCacheGenerator_ERC1155BalanceOfExample_Start_mF1D7FC086FAF079516A7463211759E19659E3E19,
	U3CStartU3Ed__0_t066787A9AC67CB267EF2129A0DF34AA9B4B21043_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_mACD6D05861661B7259297843E6FE635E542144F8,
	ERC1155URIExample_t5E72CC882B99CF7E440D0BD9E78FC79D9D27EA70_CustomAttributesCacheGenerator_ERC1155URIExample_Start_m47316F5E9063BA95AB5038A40203C130AE315332,
	U3CStartU3Ed__0_t35FD1115B0BC8E36B985418F68FF8380BC1C0E47_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m32F010E483167AED04DE742EB8940EDE54A25941,
	ERC20BalanceOfExample_t03CAD8603573B5038D9911953021BBB6C3CF17E3_CustomAttributesCacheGenerator_ERC20BalanceOfExample_Start_mFF41E5445321315891D6684596EFF7494EB4CCEA,
	U3CStartU3Ed__0_tAE2F75CCF52DC3789B48F5B1A2F666C4CF49CDB1_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m8B9F75B4229353D82C1C4D7F5D9B3D0608653F2A,
	ERC20DecimalsExample_t95297FF191243BDC695DC4FB021EFEF97B82B5BF_CustomAttributesCacheGenerator_ERC20DecimalsExample_Start_mF8A5B405AC67AC0E354341E76FD1674DC451DE85,
	U3CStartU3Ed__0_t7C1BAAAA07BFB53E3766837DFCF5BC7A3489A733_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m0E9F22BDB96721E7BD11F85CE5F248ABFF1D4399,
	ERC20NameExample_tF4DC09F14C4B9BF427DBEDC6278C0C791EC286C5_CustomAttributesCacheGenerator_ERC20NameExample_Start_m5E0090E513011383498C5799D006C408A0549FFD,
	U3CStartU3Ed__0_t3B0BE786C61C58EE0A8283BC4FBCEAE538C03D52_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m5C52A6DA998FC196467CC1A8D7DA3F1B2680784A,
	ERC20SymbolExample_t69754E14BF859CD08B6932B35413B76D0E1A7EB7_CustomAttributesCacheGenerator_ERC20SymbolExample_Start_m44F3237CCD3B3C48CF8216DAADC25FB4C817870A,
	U3CStartU3Ed__0_t895329D5ABFF19DBE587C0A6208CE8E3B3005492_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m8ECA709670FDBBB8CF6CE3BE68AC77CEE55FEF6B,
	ERC20TotalSupplyExample_t840EDDCF049B9AF63D73A64761E35F7045FC60EF_CustomAttributesCacheGenerator_ERC20TotalSupplyExample_Start_mAD965D99BF2EEA5EF43F8B15CC691404AD553AE2,
	U3CStartU3Ed__0_t764D1A87698DFB37F6819D1127531AAACCFD6BD4_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m90AE0209DA8EBB3AE0D980935EAEC3783FB842AD,
	ERC721BalanceOfExample_tC3B9AF4E7458FB2447E72577DCE748B769F3D1DC_CustomAttributesCacheGenerator_ERC721BalanceOfExample_Start_m5C514F94087DD8AFF72E9D283FF33BD6175B2EF2,
	U3CStartU3Ed__0_tBEA5CFB545680D57980B2DB70A29312D13795D50_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_mE60DC6B0816FFFA80349E8D40C3CFC426F8001B7,
	ERC721OwnerOfBatchExample_t70FD33F4468647E2AFAF89489B7F61B93EF8F262_CustomAttributesCacheGenerator_ERC721OwnerOfBatchExample_Start_m8CF7A69B7BFA086DD735B18E5A2395F422414DA6,
	U3CStartU3Ed__0_t7536771EC913216A48637DC08A6230B38924DB2A_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_mE6B01D55F9D6F1B641203AD045DA10D399ABC770,
	ERC721OwnerOfExample_tCD51B537726B5EB4294B117FEB90330F4758344F_CustomAttributesCacheGenerator_ERC721OwnerOfExample_Start_m9D7F5468A89DEC19DEF62AD36B527F91BF6792C1,
	U3CStartU3Ed__0_t99EA47DEF2B18A525F2C23CF25D697879CB47FDF_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m0ECA16AF5C9507944D8F09DD555E373FC01AF5F7,
	ERC721URIExample_t11F22B29756DA94FBCE2EE709A3755E8D72F73B5_CustomAttributesCacheGenerator_ERC721URIExample_Start_m0EACF752E96AE14675BB65815ACEBA99F6494291,
	U3CStartU3Ed__0_t63A6A6BA4375C09E642416F98697963B5F104775_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_mB281CC760C5BC7236B4D097976C68D5CF60EF754,
	AllErc1155Example_tA97B863F2CEFEC5A67625870DB133D572EC856BA_CustomAttributesCacheGenerator_AllErc1155Example_Start_m98450BC383F06E18DD3E997CA40678AA77B3ED45,
	NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_NFTs_get_contract_m132FBACD650BBB034871A7BEAA4A8A5E69890D65,
	NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_NFTs_set_contract_m5806E56C6DAE7193AB5CD81D9F474D07A35B851E,
	NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_NFTs_get_tokenId_m82B87799B5788DA9B1406347C6E7BE76CC00907F,
	NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_NFTs_set_tokenId_mEED2527C52CB89056919196F37BB93FC91703C8B,
	NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_NFTs_get_uri_m24476236E60F7F31C888F8E68D053AF7D9B88C0A,
	NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_NFTs_set_uri_mA7C27EE09FE391D53EA831A4ECE9B915DAE66CBB,
	NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_NFTs_get_balance_mCBFB0194B7ABA4A6B584E176B16282A5F2109424,
	NFTs_t27182D745697B046EEEFBC3DED2D44CB3F9D7BD9_CustomAttributesCacheGenerator_NFTs_set_balance_m4045F162A6C0E6616F9AEE022F3204229347A37C,
	U3CStartU3Ed__1_t9F1246F65A99648E11CF70FD4A03AC34D963741E_CustomAttributesCacheGenerator_U3CStartU3Ed__1_SetStateMachine_mF4BABB9B81366714D470D287C46918A5C6D7EB6A,
	AllErc721Example_t2320DACFCCCC75350179C8D5F6DEFEA9EFDB92AB_CustomAttributesCacheGenerator_AllErc721Example_Start_m1173828869CE0CA1368EC96797AB780B26007185,
	NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_NFTs_get_contract_mEF0AF77D33D8D1793D68D7BD429C144F2DBECC0D,
	NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_NFTs_set_contract_mD64E897A626ADCEE21998CE15BFF69AD5F762634,
	NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_NFTs_get_tokenId_m9EFD2AC01B73E801C3BC8B45FB7268099F8979EF,
	NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_NFTs_set_tokenId_m24AE780605FE57573BC4ED1FF58B1890B9C98686,
	NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_NFTs_get_uri_mC235611E28E2B8ACC01BA87194FC197DB55AD436,
	NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_NFTs_set_uri_mAD2F72E336F6B548906B4D9A68BB7CF28FDA325B,
	NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_NFTs_get_balance_m9815F7685CFBDFB187A2EBC45307D1353622B4FA,
	NFTs_t8605EEC14309B15424C32C40736E0CF88E115225_CustomAttributesCacheGenerator_NFTs_set_balance_m343CFEB6DFF4A498F2127634C5EA9041E505AF37,
	U3CStartU3Ed__1_t69C675AB72ACBBEC9F2AE65937A9BAF7F7F0002B_CustomAttributesCacheGenerator_U3CStartU3Ed__1_SetStateMachine_mCD7FDC90AA399D1DB295620538CA230DEAB95A1F,
	BlockNumberExample_t021EE1F41AF9ADCA74474C76FE8A46DE92057BD0_CustomAttributesCacheGenerator_BlockNumberExample_Start_mAE8F8CFCAE848DAA2C9FC0E971741520E7BA3EFD,
	U3CStartU3Ed__0_tD57E14EDF0D69957DE35D69036FE23E61F69DBA1_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m1B759B91C18F303ECF8AA29965839045F5FB49C9,
	CreateContractDataExample_t6D7EBFC5C9DC361C3E0F58B343D2DD451BE42879_CustomAttributesCacheGenerator_CreateContractDataExample_Start_m975FDA0E1478E7283C9B3F95C5D4A825CFCC500E,
	U3CStartU3Ed__0_t4C79D410A6C828EF826DC1FDC1B980242584FB34_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m90FCB1CDB53828A20360A302E41321FA2915A8B7,
	CustomCallExample_tEFB7F5467D3F9DAAA06AA730518F596FAA51C956_CustomAttributesCacheGenerator_CustomCallExample_Mint_mA9AF3C5B8DC09D051DB0DB7EE46FC8F3F1ECFB34,
	CustomCallExample_tEFB7F5467D3F9DAAA06AA730518F596FAA51C956_CustomAttributesCacheGenerator_CustomCallExample_ChangeName_m11A572AD5E78FF9DBC799B37BB3C7DFE0EE95A34,
	CustomCallExample_tEFB7F5467D3F9DAAA06AA730518F596FAA51C956_CustomAttributesCacheGenerator_CustomCallExample_ReLoad_m9ED3FF4E3B3388BC59DD339CF4E2ADE587DB03FF,
	CustomCallExample_tEFB7F5467D3F9DAAA06AA730518F596FAA51C956_CustomAttributesCacheGenerator_CustomCallExample_GetNft_mB0ABD70FEE9F630BB81163A45BE6C18B72674981,
	CustomCallExample_tEFB7F5467D3F9DAAA06AA730518F596FAA51C956_CustomAttributesCacheGenerator_CustomCallExample_Burn_mF950FC3007B36EB9AF4C968F7FFCDDF6B31B09C8,
	NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_NFTs_get_contract_mF6232E648620A06789385C55D8180A8CF7464944,
	NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_NFTs_set_contract_m4F35AAB24C4FD78BF470D1F797C126463E38FB60,
	NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_NFTs_get_tokenId_mDF28A5F14194E236C2FF36FF1822BB37FA5F1774,
	NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_NFTs_set_tokenId_m240F331C0FD015FEFCD9B9C8AAF3ABBFD9D38D71,
	NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_NFTs_get_uri_mFFE0E2FAC27B394D476A8ED8A87D3D75F58EA390,
	NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_NFTs_set_uri_mF53EB20E483FA1467C8BCC0764A02EC97B64FDED,
	NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_NFTs_get_balance_m53BCCA62B6C840C1D66DDA2AE8E9D8BB6AFAD2AC,
	NFTs_tC1CCDA6EA2651D32DBA2C4AFD66353481C20E054_CustomAttributesCacheGenerator_NFTs_set_balance_m32CAB14E10B0A94AA9A0093C4133AC20E4775BAA,
	U3CMintU3Ed__6_tE9A6EE8F8DB7219D7A1127A28A907B7BBB826F9C_CustomAttributesCacheGenerator_U3CMintU3Ed__6_SetStateMachine_mE34E1011DA6AE67062B3638D5CA080CB82EE7586,
	U3CChangeNameU3Ed__8_t6C75EF60C6E66B9BB035E70BB9907A2A7D7553F8_CustomAttributesCacheGenerator_U3CChangeNameU3Ed__8_SetStateMachine_m2F1CDC60D482E2AFCE6FBEA95ECA5648BBE31409,
	U3CReLoadU3Ed__9_tC6FAF560B8A13260A85E593B82E7660D7792F4AB_CustomAttributesCacheGenerator_U3CReLoadU3Ed__9_SetStateMachine_m89009A42D7FAC4B90BC171A10997A50E3AFDE867,
	U3CGetNftU3Ed__10_t9EB229B03086CFC54263810ABEE900FCBA6FC85B_CustomAttributesCacheGenerator_U3CGetNftU3Ed__10_SetStateMachine_m110A5F8CA3599AE1E562A01BAE0B1194F8801D48,
	U3CBurnU3Ed__11_t57A44B35853BAEE9C5D70339914EF7D880A488C6_CustomAttributesCacheGenerator_U3CBurnU3Ed__11_SetStateMachine_m1F8C91D9B818F4B52BF56BBC71398F27DFFF3417,
	CustomRPCExample_t84D78666093B2375A29EFD0AC8938A233DD9AF70_CustomAttributesCacheGenerator_CustomRPCExample_Start_mA3CCBB955CEA57395864A8F32683C8763BEFDC33,
	U3CStartU3Ed__0_t479A6E16097648A0B10B0738414EAA50C0BF9293_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_mA01B28EE49B96A3BB28545E7375E2E4823E4F158,
	EthBalanceOfExample_tB6A55A007578F79961335084F784DF7EA94301C5_CustomAttributesCacheGenerator_EthBalanceOfExample_Start_m38948E53166841C031BB586EE77C4CC412FAA71F,
	U3CStartU3Ed__0_tFB2C2E6151A430EC6EA1BC8B0F3539D9FDC5344E_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m8763265A5E48878995143FE2211C1135DD40A431,
	NonceExample_t3B378A322C855CFF7EAF91F4FDFDE06E54D368A0_CustomAttributesCacheGenerator_NonceExample_Start_mE7130950BE1D260F4460F2F6DD25E5ADF4209C0D,
	U3CStartU3Ed__0_t64D04A640EF7B7D42ECBFEDD79906834E402A483_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_mE36D9D616EF594199062DEBE0273AC423B4D8347,
	TxStatus_tA5AF89A303ED9A45B7F408C4487DC05E1D878820_CustomAttributesCacheGenerator_TxStatus_Start_mFA223A713B485F7F2E598A7EB56054D55BB28DB9,
	U3CStartU3Ed__0_tB8B66B371E65C03F064F71E432E101E7258A08F6_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_mF482F4B6CA65515CF4469D1B6BCD7C43F86BB665,
	VerifyExample_t6D959548B51F544244727DCD742D28D5DC2C5129_CustomAttributesCacheGenerator_VerifyExample_Start_m00DC30EC005ED55D223D40563C7E5BFA50D78296,
	U3CStartU3Ed__0_t7925DC9BEAFEB29EB7181BD3B7DF9CBEBEC20FD9_CustomAttributesCacheGenerator_U3CStartU3Ed__0_SetStateMachine_m8B1480939675F9433880643CD4EA0C534881209D,
	Web3PrivateKeySend1155Example_t5E81F3DA30966A941F8836C66FE74FA4E991007B_CustomAttributesCacheGenerator_Web3PrivateKeySend1155Example_OnSend1155_m244501F1769E054C6D39622BD3439BFBA32416CD,
	U3COnSend1155U3Ed__0_t7C1EF487A95CFD9199044CEAF7184F39BD8EB846_CustomAttributesCacheGenerator_U3COnSend1155U3Ed__0_SetStateMachine_m6044558A9818E8F112CEFE3BF401EDE447831294,
	Web3PrivateKeySend20Example_t5C28A29420AEA17AB8C3684CF8055680F39E108A_CustomAttributesCacheGenerator_Web3PrivateKeySend20Example_OnSend20_m25112C041C89E4656FB9F97A75A0F4D65E827540,
	U3COnSend20U3Ed__0_t19D4015AA5A24D7279A104C542D5B7F63B20E830_CustomAttributesCacheGenerator_U3COnSend20U3Ed__0_SetStateMachine_m2459E52710C5D0C1641F042AA1246E79DB87C834,
	Web3PrivateKeySend721Example_t7C5B412BD1183BA68937A2567C92210EF0C1FDD3_CustomAttributesCacheGenerator_Web3PrivateKeySend721Example_OnSend721_m7DF532A7644D27B2CBBC1EDDF024613064342487,
	U3COnSend721U3Ed__0_t3CC7BEDD0A3A00D6359571BDB9098BA2565172D0_CustomAttributesCacheGenerator_U3COnSend721U3Ed__0_SetStateMachine_m2CDC6C3A888109DBFB4B350C5D3969E5E9805EF3,
	Web3PrivateKeySendTransactionExample_t51C2B5F484B55FC913465DB5810EFFFFF77F9293_CustomAttributesCacheGenerator_Web3PrivateKeySendTransactionExample_OnSendTransaction_m096857E458B9BFEB768B0BA0088D8C396C14FD63,
	U3COnSendTransactionU3Ed__0_tAC21FA78A9334357CD3676F1B36A89938F0526AB_CustomAttributesCacheGenerator_U3COnSendTransactionU3Ed__0_SetStateMachine_m75CF9DD43E6B9372BE208D6144B6D3D389EDD257,
	Web3WalletSendTransactionExample_tBACBB9A580022148FAE9EF695B23DA972B8580C1_CustomAttributesCacheGenerator_Web3WalletSendTransactionExample_OnSendTransaction_m4FA8621476FD0880D88D0F52337D40760ED02D06,
	U3COnSendTransactionU3Ed__0_t17D92BBC0E0FF116EE91EBA514E78D309F704DF2_CustomAttributesCacheGenerator_U3COnSendTransactionU3Ed__0_SetStateMachine_mA1354644175F2D58C90730B1FDB4869FA7BDFC48,
	Web3WalletSignMessageExample_t35AC222FEDCB211ABF34B56B54C9C908CD82426D_CustomAttributesCacheGenerator_Web3WalletSignMessageExample_OnSignMessage_m6A3BA73D6F59E338F04E01A2E3912198F487BBDA,
	U3COnSignMessageU3Ed__0_tB5523A25CE45CFCAD0F6637DD8C3B64F6E00AB42_CustomAttributesCacheGenerator_U3COnSignMessageU3Ed__0_SetStateMachine_m4338BE5F3F9B7C77682650825239AC6C7138A6B1,
	Web3WalletTransfer1155Example_t9A9B8A3CC0FDEEB4EF5607782DC838AC4F4A40F6_CustomAttributesCacheGenerator_Web3WalletTransfer1155Example_OnTransfer1155_m6CC73312B266A7E1B7974E6CB782503B376758E3,
	U3COnTransfer1155U3Ed__0_t3825AF7253AF38599E73188003F68FD536E30C1A_CustomAttributesCacheGenerator_U3COnTransfer1155U3Ed__0_SetStateMachine_mE5364F3E8E033AA906ACE7657884281076A015C5,
	Web3WalletTransfer20Example_t6AC6BEA2A4CBFF994913184A51A5E9896DE707CD_CustomAttributesCacheGenerator_Web3WalletTransfer20Example_OnTransfer20_mD19680408F257116D802637BCAFA6AE2D4C049D1,
	U3COnTransfer20U3Ed__0_t3CD34BBD71FF80DD78C25D0FE992ED9D1353E8F4_CustomAttributesCacheGenerator_U3COnTransfer20U3Ed__0_SetStateMachine_mF8F73F2742E7641DF16BA8DAD19AB3A74142E61C,
	Web3WalletTransfer721Example_t61B0589D45F611520C9C466C30FDE293F7EB4AAC_CustomAttributesCacheGenerator_Web3WalletTransfer721Example_OnTransfer721_mF62FEAA1F0ED77EA1D20ABDDA5F08A19908BD314,
	U3COnTransfer721U3Ed__0_t19C7B441C34CB2F433CAD229877558815977CDCC_CustomAttributesCacheGenerator_U3COnTransfer721U3Ed__0_SetStateMachine_mF885565CD1B11CCD1E3057612D11154B7629129D,
	WebGLSendContractExample_t69A3686A5C09CD04184278A40805D54481117CFB_CustomAttributesCacheGenerator_WebGLSendContractExample_OnSendContract_m1A4B193810A8347EB809FAA076CE10CE962713C2,
	U3COnSendContractU3Ed__0_t15F35AD460E7321938C2FD75A065307A93876F9C_CustomAttributesCacheGenerator_U3COnSendContractU3Ed__0_SetStateMachine_m7A097F8F1BB131B8A785D2F22D1D28D7527646EA,
	WebGLSendTransactionExample_tEDB304B208287C836BD9E41249EF21CEE555B62E_CustomAttributesCacheGenerator_WebGLSendTransactionExample_OnSendTransaction_mE9BD097CBD52F83717726BFBB8B55265A7BEA7F8,
	U3COnSendTransactionU3Ed__0_t2A7D6CDF5773E00BAEA1859E778C6BC14638EA50_CustomAttributesCacheGenerator_U3COnSendTransactionU3Ed__0_SetStateMachine_mC12443C49C60F7DCAA5CDA5E716B32E16BD74723,
	WebGLSignMessageExample_t6B1C913E8904B2D53E392604E2E19ACCB04905D2_CustomAttributesCacheGenerator_WebGLSignMessageExample_OnSignMessage_mEDA9C904A9601BA4E7A4EBD354297A6DB24A5279,
	U3COnSignMessageU3Ed__0_t53C2F5D040A39F6A271091DF3DCEF85F1D9D150A_CustomAttributesCacheGenerator_U3COnSignMessageU3Ed__0_SetStateMachine_m3F0DC2AEF8F20669EFA57A92C67E1F9A5F4B6802,
	WebGLTransfer1155_t4B7D5BD7F1B768A116719F1D39FEA9B3276BCB80_CustomAttributesCacheGenerator_WebGLTransfer1155_SafeTransferFrom_m60027B3FC9E5908D1DAC6D38F279E525F9E69282,
	U3CSafeTransferFromU3Ed__5_t7DECABE00F0A217B61D3EBE9046646C3A3C2B0AE_CustomAttributesCacheGenerator_U3CSafeTransferFromU3Ed__5_SetStateMachine_m357DFCF039FEA8B6C3395C25DFC5753549AD9156,
	WebGLTransfer20_t081FD59CDBAF6AE4314056B8B7B6249964572A0A_CustomAttributesCacheGenerator_WebGLTransfer20_Transfer_mC189BF2DC9623CEED6D1285FFF55B985A9F95FB8,
	U3CTransferU3Ed__4_t4EB7258C238E19FF60A86837B4E6620F284C8779_CustomAttributesCacheGenerator_U3CTransferU3Ed__4_SetStateMachine_mA32D0A2CF0FC0E65AD5EA5081D9B5860C23936DE,
	WebGLTransfer721_t3DF4C3057637769675C1EFA29FAAF75CE9AA314C_CustomAttributesCacheGenerator_WebGLTransfer721_SafeTransferFrom_m48A1C85515A84E75A7ADB433BD7D6011D307848B,
	U3CSafeTransferFromU3Ed__4_t3CA8E5B1F971C292BEA3561C1036CA365AC1ECFA_CustomAttributesCacheGenerator_U3CSafeTransferFromU3Ed__4_SetStateMachine_m72420EB4DE07C850B47609F4910E992BFAA0CC10,
	WalletLogin_t610F7E3F09279AAA049091289432790647D92F5F_CustomAttributesCacheGenerator_WalletLogin_OnLogin_mADEC61B177A2617C428988B3EC727ED17679DE7D,
	U3COnLoginU3Ed__2_t566FE4C2C513D969330BA5B1E33B3C977A5C7522_CustomAttributesCacheGenerator_U3COnLoginU3Ed__2_SetStateMachine_mB0FF2BA206BB13E75568802ED92D9C2A4609EBC1,
	WebLogin_tF88C4CA5F48CD5D6A1973176EAB073A5AD819BBD_CustomAttributesCacheGenerator_WebLogin_OnConnected_mBE42B60A0B735E4C2ABB34614D0D194F987BCD8B,
	U3COnConnectedU3Ed__6_tEFC9E3F446B694229C87DDAF8E60AD510F0C5F1A_CustomAttributesCacheGenerator_U3COnConnectedU3Ed__6_SetStateMachine_mCDC0D60876FA7F6803612569D8BCB444418EC9D9,
	GeneratedNetworkCode_tA5DCDE73EABE64446CC3985FE891D3CA9B55A51E_CustomAttributesCacheGenerator_GeneratedNetworkCode_InitReadWriters_mF3F676E480CD76ED8819C76E79234EED4737891B,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
