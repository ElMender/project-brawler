﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_mBAD8A67721F7598BE1151A9142ACE8229337284E (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m2EA1DDCC3375C83D2922C167CD0BA462DA85B421 (void);
// 0x00000003 System.Void System.Runtime.CompilerServices.IsUnmanagedAttribute::.ctor()
extern void IsUnmanagedAttribute__ctor_m674339F10E3980C7645B182B747EC74E4E3B0EDD (void);
// 0x00000004 System.Int32 kcp2k.KcpTransport::FromKcpChannel(kcp2k.KcpChannel)
extern void KcpTransport_FromKcpChannel_mB64EDD825967DF4381967DB2EC31A44AB52D6E37 (void);
// 0x00000005 kcp2k.KcpChannel kcp2k.KcpTransport::ToKcpChannel(System.Int32)
extern void KcpTransport_ToKcpChannel_m2570D7CD3090359062C0D6852A9820A70FC164DA (void);
// 0x00000006 System.Void kcp2k.KcpTransport::Awake()
extern void KcpTransport_Awake_m62B40AF0A488B3ECE15089856A16BE4623A51BEE (void);
// 0x00000007 System.Void kcp2k.KcpTransport::OnValidate()
extern void KcpTransport_OnValidate_mD3A91915F6D032B947493DE1E7F6DD52C3B033BF (void);
// 0x00000008 System.Boolean kcp2k.KcpTransport::Available()
extern void KcpTransport_Available_mC28A19601D78A80E81F13231A059929D2E118C54 (void);
// 0x00000009 System.Boolean kcp2k.KcpTransport::ClientConnected()
extern void KcpTransport_ClientConnected_m4370351A8E8B89BABF8AC5BF2423F05BEC959358 (void);
// 0x0000000A System.Void kcp2k.KcpTransport::ClientConnect(System.String)
extern void KcpTransport_ClientConnect_mDB9736490D95A3F288721B88A33DC79FCEFB98DD (void);
// 0x0000000B System.Void kcp2k.KcpTransport::ClientConnect(System.Uri)
extern void KcpTransport_ClientConnect_m5C61BED925B65A67E6F1F48EC651F42D1B3C138C (void);
// 0x0000000C System.Void kcp2k.KcpTransport::ClientSend(System.ArraySegment`1<System.Byte>,System.Int32)
extern void KcpTransport_ClientSend_m561B652876E5E71B9509A5675BE66B763DF07DD2 (void);
// 0x0000000D System.Void kcp2k.KcpTransport::ClientDisconnect()
extern void KcpTransport_ClientDisconnect_mADDC6439D972EC6CA801E57F55FE211B755093B1 (void);
// 0x0000000E System.Void kcp2k.KcpTransport::ClientEarlyUpdate()
extern void KcpTransport_ClientEarlyUpdate_m4AE48F297CFB0B6ED62E6C89FF7AD5B5BE04B1CF (void);
// 0x0000000F System.Void kcp2k.KcpTransport::ClientLateUpdate()
extern void KcpTransport_ClientLateUpdate_mFCA6124C57C7836AE664951F986CCB11F3206D75 (void);
// 0x00000010 System.Uri kcp2k.KcpTransport::ServerUri()
extern void KcpTransport_ServerUri_mB32E80AE3973B31A562FA9BE8954A9EC833B3E2D (void);
// 0x00000011 System.Boolean kcp2k.KcpTransport::ServerActive()
extern void KcpTransport_ServerActive_mFFF858F3F362B80BFBB8DA38870901126AD33D0F (void);
// 0x00000012 System.Void kcp2k.KcpTransport::ServerStart()
extern void KcpTransport_ServerStart_mB19F14CED645C0AF8BEE683B2911105972CB4B17 (void);
// 0x00000013 System.Void kcp2k.KcpTransport::ServerSend(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void KcpTransport_ServerSend_m3759F5AAAD385E594FBEBBA4DC5C220A7C21DF7F (void);
// 0x00000014 System.Void kcp2k.KcpTransport::ServerDisconnect(System.Int32)
extern void KcpTransport_ServerDisconnect_mEEC7A07982EBC1EFCFA2B07E189DD6E0A7FEABDD (void);
// 0x00000015 System.String kcp2k.KcpTransport::ServerGetClientAddress(System.Int32)
extern void KcpTransport_ServerGetClientAddress_m0DC394B917B355C3FD4798EE0C0022B438188BCA (void);
// 0x00000016 System.Void kcp2k.KcpTransport::ServerStop()
extern void KcpTransport_ServerStop_m3EE5CD2DE9953E2EFE10720382DC9D5DC4F13E3C (void);
// 0x00000017 System.Void kcp2k.KcpTransport::ServerEarlyUpdate()
extern void KcpTransport_ServerEarlyUpdate_m79B9CC4F151CD8FCE544D2A8F549B54A049BCEEF (void);
// 0x00000018 System.Void kcp2k.KcpTransport::ServerLateUpdate()
extern void KcpTransport_ServerLateUpdate_m06B80D50F2CC1A33AC95FAE891E4E95F5B2DF97B (void);
// 0x00000019 System.Void kcp2k.KcpTransport::Shutdown()
extern void KcpTransport_Shutdown_m6CF63A2AC8B565B3411F462352420A5427A0A3BC (void);
// 0x0000001A System.Int32 kcp2k.KcpTransport::GetMaxPacketSize(System.Int32)
extern void KcpTransport_GetMaxPacketSize_mEB9B1E5DE977F2A6225576122ECE6C505B8DC257 (void);
// 0x0000001B System.Int32 kcp2k.KcpTransport::GetBatchThreshold(System.Int32)
extern void KcpTransport_GetBatchThreshold_mA0D07FCF893E5D932F4745CFB87262CBBACCCE91 (void);
// 0x0000001C System.Int64 kcp2k.KcpTransport::GetAverageMaxSendRate()
extern void KcpTransport_GetAverageMaxSendRate_mDBE93910418A930FB9D2E01BC11519CC227D42FE (void);
// 0x0000001D System.Int64 kcp2k.KcpTransport::GetAverageMaxReceiveRate()
extern void KcpTransport_GetAverageMaxReceiveRate_m7E2FDCBA723BFEA986CC6C268E0F1BFC6F2D0834 (void);
// 0x0000001E System.Int64 kcp2k.KcpTransport::GetTotalSendQueue()
extern void KcpTransport_GetTotalSendQueue_mA939D84C1BA0CF741A8266D880E0D8D7A1FA7EFA (void);
// 0x0000001F System.Int64 kcp2k.KcpTransport::GetTotalReceiveQueue()
extern void KcpTransport_GetTotalReceiveQueue_m60E09827BA5ACFF1BC3FC0EAC6ED2E05BF27948C (void);
// 0x00000020 System.Int64 kcp2k.KcpTransport::GetTotalSendBuffer()
extern void KcpTransport_GetTotalSendBuffer_m02B88C25F1D16A343D4F55DA6F8C2E526468BA03 (void);
// 0x00000021 System.Int64 kcp2k.KcpTransport::GetTotalReceiveBuffer()
extern void KcpTransport_GetTotalReceiveBuffer_m90FB51580EAB4CB8C00C8D3074474EF70C5BB401 (void);
// 0x00000022 System.String kcp2k.KcpTransport::PrettyBytes(System.Int64)
extern void KcpTransport_PrettyBytes_m60CBBB4165C1D52DD392C9D6E4F7359F28345CA4 (void);
// 0x00000023 System.Void kcp2k.KcpTransport::OnLogStatistics()
extern void KcpTransport_OnLogStatistics_m6251A4F09FBD2348C24444E46A89E385F32D6693 (void);
// 0x00000024 System.String kcp2k.KcpTransport::ToString()
extern void KcpTransport_ToString_mAE9B5329A0035FC14BD3ECB6BFAA3E3B9C0911E5 (void);
// 0x00000025 System.Void kcp2k.KcpTransport::.ctor()
extern void KcpTransport__ctor_m0CF4CC09A52CDE5F1CB9A576C0650B0FA7A21075 (void);
// 0x00000026 System.Void kcp2k.KcpTransport::<Awake>b__22_1()
extern void KcpTransport_U3CAwakeU3Eb__22_1_m4160E26B7C11B7BD8713CCB11E77D94BE8741BB4 (void);
// 0x00000027 System.Void kcp2k.KcpTransport::<Awake>b__22_2(System.ArraySegment`1<System.Byte>,kcp2k.KcpChannel)
extern void KcpTransport_U3CAwakeU3Eb__22_2_m0A99F222B220E478453B6E21AFC8AA70BF6B00B7 (void);
// 0x00000028 System.Void kcp2k.KcpTransport::<Awake>b__22_3()
extern void KcpTransport_U3CAwakeU3Eb__22_3_mE2B91D22FE19CD7AB46E4F14044FB9AE5EB81F92 (void);
// 0x00000029 System.Void kcp2k.KcpTransport::<Awake>b__22_4()
extern void KcpTransport_U3CAwakeU3Eb__22_4_m3BBA4A0DA66024030F80A1230DA6FAA6D1A52E82 (void);
// 0x0000002A System.Void kcp2k.KcpTransport::<Awake>b__22_5(System.ArraySegment`1<System.Byte>,kcp2k.KcpChannel)
extern void KcpTransport_U3CAwakeU3Eb__22_5_mD4B2EB78505906D0B36C782D73C07117A6DB6A7C (void);
// 0x0000002B System.Void kcp2k.KcpTransport::<Awake>b__22_6()
extern void KcpTransport_U3CAwakeU3Eb__22_6_m1E6D5CF6B0D536FF63427CC1D9A5D4111BC00037 (void);
// 0x0000002C System.Void kcp2k.KcpTransport::<Awake>b__22_7(System.Int32)
extern void KcpTransport_U3CAwakeU3Eb__22_7_mAC8BB1D9022778C0E4AECEC7B7069EAF1E191C8C (void);
// 0x0000002D System.Void kcp2k.KcpTransport::<Awake>b__22_8(System.Int32,System.ArraySegment`1<System.Byte>,kcp2k.KcpChannel)
extern void KcpTransport_U3CAwakeU3Eb__22_8_mA2493E67FBA0274A3DF39257B8968EB2A1782C4D (void);
// 0x0000002E System.Void kcp2k.KcpTransport::<Awake>b__22_9(System.Int32)
extern void KcpTransport_U3CAwakeU3Eb__22_9_m3718FC146E7392A34DCA376A4CC907EE79107268 (void);
// 0x0000002F System.Void kcp2k.KcpTransport::<Awake>b__22_10(System.Int32)
extern void KcpTransport_U3CAwakeU3Eb__22_10_m7CB524232C17580E7298C82025B29923A8390BD8 (void);
// 0x00000030 System.Void kcp2k.KcpTransport::<Awake>b__22_11(System.Int32,System.ArraySegment`1<System.Byte>,kcp2k.KcpChannel)
extern void KcpTransport_U3CAwakeU3Eb__22_11_m10FF31C37971098EE568E7C10F124EAD479169E4 (void);
// 0x00000031 System.Void kcp2k.KcpTransport::<Awake>b__22_12(System.Int32)
extern void KcpTransport_U3CAwakeU3Eb__22_12_mE8B84593620E6985DEAF203E6D772C838237B1B0 (void);
// 0x00000032 System.Void kcp2k.KcpTransport/<>c::.cctor()
extern void U3CU3Ec__cctor_m74DDBDACB2E47803B0ECB09111C9BCD8E1FBC2FF (void);
// 0x00000033 System.Void kcp2k.KcpTransport/<>c::.ctor()
extern void U3CU3Ec__ctor_m198B95BF0E13C9FF9E4CF6A86E7F21801E5234E0 (void);
// 0x00000034 System.Void kcp2k.KcpTransport/<>c::<Awake>b__22_0(System.String)
extern void U3CU3Ec_U3CAwakeU3Eb__22_0_mED3F2C0F388BE01226F6CAD5C450E4A03EFA7CD0 (void);
// 0x00000035 System.Int64 kcp2k.KcpTransport/<>c::<GetAverageMaxSendRate>b__44_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetAverageMaxSendRateU3Eb__44_0_m670B20C66040756AA43B5A2DA26A525DA0E3CE8E (void);
// 0x00000036 System.Int64 kcp2k.KcpTransport/<>c::<GetAverageMaxReceiveRate>b__45_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetAverageMaxReceiveRateU3Eb__45_0_mACB59A3EB68421543A8950069A9FE511D6B267F5 (void);
// 0x00000037 System.Int32 kcp2k.KcpTransport/<>c::<GetTotalSendQueue>b__46_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetTotalSendQueueU3Eb__46_0_mB0E14CFFAE3F3AF53A176175F1147A390FF6E62D (void);
// 0x00000038 System.Int32 kcp2k.KcpTransport/<>c::<GetTotalReceiveQueue>b__47_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetTotalReceiveQueueU3Eb__47_0_m200BD3BD374C50D7097269491FBCC80E8A0FBF10 (void);
// 0x00000039 System.Int32 kcp2k.KcpTransport/<>c::<GetTotalSendBuffer>b__48_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetTotalSendBufferU3Eb__48_0_m7DAD5A18EDF682BB34E55F3459DCC4F8081A1A2B (void);
// 0x0000003A System.Int32 kcp2k.KcpTransport/<>c::<GetTotalReceiveBuffer>b__49_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetTotalReceiveBufferU3Eb__49_0_mD64AE0406D1DD066CA9C742C80E57C4495645292 (void);
// 0x0000003B System.Void Mirror.SyncVarAttribute::.ctor()
extern void SyncVarAttribute__ctor_m4E99C2B45DDA311F1D9B684B967978541348C875 (void);
// 0x0000003C System.Void Mirror.CommandAttribute::.ctor()
extern void CommandAttribute__ctor_m583940E69A6305050FEF54E57CBAF339A8E77FFC (void);
// 0x0000003D System.Void Mirror.ClientRpcAttribute::.ctor()
extern void ClientRpcAttribute__ctor_mCE8A483D94F06D1FA29EA2342BE860018B29BCA7 (void);
// 0x0000003E System.Void Mirror.TargetRpcAttribute::.ctor()
extern void TargetRpcAttribute__ctor_m5954E4349796FC0A7AA8BE4857791C38E867CA8C (void);
// 0x0000003F System.Void Mirror.ServerAttribute::.ctor()
extern void ServerAttribute__ctor_m413F135143ED76C9B8C3F41B2DB6597E4382E0A6 (void);
// 0x00000040 System.Void Mirror.ServerCallbackAttribute::.ctor()
extern void ServerCallbackAttribute__ctor_m7258C253CBA5BF5C92BAAB6345240F9418C0B61D (void);
// 0x00000041 System.Void Mirror.ClientAttribute::.ctor()
extern void ClientAttribute__ctor_mD3E93370918D30F063F1CBCA1CF1BC46B90D9065 (void);
// 0x00000042 System.Void Mirror.ClientCallbackAttribute::.ctor()
extern void ClientCallbackAttribute__ctor_m1CCA6B90CA8E84DD7A6C6AC9C862434AA9560703 (void);
// 0x00000043 System.Void Mirror.SceneAttribute::.ctor()
extern void SceneAttribute__ctor_m7F8095E94F9D74BDB458EE8B5CF10781C8EEEB62 (void);
// 0x00000044 System.Void Mirror.ShowInInspectorAttribute::.ctor()
extern void ShowInInspectorAttribute__ctor_mFA3EF79FD7E9F573D31F156DE62A6CBDBE254688 (void);
// 0x00000045 System.Void Mirror.Batcher::.ctor(System.Int32)
extern void Batcher__ctor_m09EE7AAA6B64C24CC33BDA106A986C6EE2FDE530 (void);
// 0x00000046 System.Void Mirror.Batcher::AddMessage(System.ArraySegment`1<System.Byte>)
extern void Batcher_AddMessage_m7A8979D02D56CFBDE123E1D1736937E42F8ACFFC (void);
// 0x00000047 System.Boolean Mirror.Batcher::MakeNextBatch(Mirror.NetworkWriter,System.Double)
extern void Batcher_MakeNextBatch_m7E84834E9BA080217B7EABE42EEA34448B2DFDB2 (void);
// 0x00000048 System.Int32 Mirror.Unbatcher::get_BatchesCount()
extern void Unbatcher_get_BatchesCount_m94ED4CD606FD0612C11C0BDA995E14684BCB3892 (void);
// 0x00000049 System.Void Mirror.Unbatcher::StartReadingBatch(Mirror.PooledNetworkWriter)
extern void Unbatcher_StartReadingBatch_m7609F8C4E60E92DAF00646BBE3897A3028CAC1B5 (void);
// 0x0000004A System.Boolean Mirror.Unbatcher::AddBatch(System.ArraySegment`1<System.Byte>)
extern void Unbatcher_AddBatch_mF699FFD9FDCFE1CF312EC5A1DBA87B95886BABF9 (void);
// 0x0000004B System.Boolean Mirror.Unbatcher::GetNextMessage(Mirror.NetworkReader&,System.Double&)
extern void Unbatcher_GetNextMessage_m0A09E7F86E5677699469D0C2782DB81ADCEF836B (void);
// 0x0000004C System.Void Mirror.Unbatcher::.ctor()
extern void Unbatcher__ctor_m6374EC7D7E8440A13444BD921C524C49227B43B0 (void);
// 0x0000004D System.Int32 Mirror.Compression::LargestAbsoluteComponentIndex(UnityEngine.Vector4,System.Single&,UnityEngine.Vector3&)
extern void Compression_LargestAbsoluteComponentIndex_mB176E3E544821911CE8E10C6E766E73063A18F8F (void);
// 0x0000004E System.UInt16 Mirror.Compression::ScaleFloatToUShort(System.Single,System.Single,System.Single,System.UInt16,System.UInt16)
extern void Compression_ScaleFloatToUShort_mAFB60A1D68E5F3B953184D66C1DE4A57E9D21A3F (void);
// 0x0000004F System.Single Mirror.Compression::ScaleUShortToFloat(System.UInt16,System.UInt16,System.UInt16,System.Single,System.Single)
extern void Compression_ScaleUShortToFloat_mAB1609B5E7B4C38EAA298BBF3249C06A272B5865 (void);
// 0x00000050 System.Single Mirror.Compression::QuaternionElement(UnityEngine.Quaternion,System.Int32)
extern void Compression_QuaternionElement_m60537C4CF1CA1B44907E0F6D49F7A88E6CF4E604 (void);
// 0x00000051 System.UInt32 Mirror.Compression::CompressQuaternion(UnityEngine.Quaternion)
extern void Compression_CompressQuaternion_mC1DD43E3FA152477293A562CA2713DAC89B89CFE (void);
// 0x00000052 UnityEngine.Quaternion Mirror.Compression::QuaternionNormalizeSafe(UnityEngine.Quaternion)
extern void Compression_QuaternionNormalizeSafe_mD1517F3E0392C60CCEF02EAC2857A64312F83F09 (void);
// 0x00000053 UnityEngine.Quaternion Mirror.Compression::DecompressQuaternion(System.UInt32)
extern void Compression_DecompressQuaternion_m235BB50DA4C0504C27545528ED733C1E0CD246EC (void);
// 0x00000054 System.Void Mirror.Compression::CompressVarUInt(Mirror.NetworkWriter,System.UInt64)
extern void Compression_CompressVarUInt_m3D7658FE25B3349CADD478CF0527A78A4D852731 (void);
// 0x00000055 System.Void Mirror.Compression::CompressVarInt(Mirror.NetworkWriter,System.Int64)
extern void Compression_CompressVarInt_m7B1A3B99D742DA9BD7A53463E04DA4D2BC8E85BB (void);
// 0x00000056 System.UInt64 Mirror.Compression::DecompressVarUInt(Mirror.NetworkReader)
extern void Compression_DecompressVarUInt_m3A4757642159D857F4A7CE4BCFFFA9DBE60A71D7 (void);
// 0x00000057 System.Int64 Mirror.Compression::DecompressVarInt(Mirror.NetworkReader)
extern void Compression_DecompressVarInt_mF00C15FC25DD9D9CD5DAAA63C8E55305049AA9F9 (void);
// 0x00000058 System.Double Mirror.ExponentialMovingAverage::get_Value()
extern void ExponentialMovingAverage_get_Value_mB278B5333872C5EAEAF519E1F95BF4B437F1A00E (void);
// 0x00000059 System.Void Mirror.ExponentialMovingAverage::set_Value(System.Double)
extern void ExponentialMovingAverage_set_Value_m7839F5E48C1B64C159F98A9D14213DD5C390EEA7 (void);
// 0x0000005A System.Double Mirror.ExponentialMovingAverage::get_Var()
extern void ExponentialMovingAverage_get_Var_m9ED5A56A0D2B778547F20E4D762562F0F927D8BD (void);
// 0x0000005B System.Void Mirror.ExponentialMovingAverage::set_Var(System.Double)
extern void ExponentialMovingAverage_set_Var_m58941EF0646BC5D3EF2CD0EDCAC7C6D539AF0D54 (void);
// 0x0000005C System.Void Mirror.ExponentialMovingAverage::.ctor(System.Int32)
extern void ExponentialMovingAverage__ctor_m3EB10AAA23643AF85E68D21E0EEDE15219287268 (void);
// 0x0000005D System.Void Mirror.ExponentialMovingAverage::Add(System.Double)
extern void ExponentialMovingAverage_Add_mC19B600AC4A4ABB827290B157B0A830103D54F11 (void);
// 0x0000005E System.Int32 Mirror.Extensions::GetStableHashCode(System.String)
extern void Extensions_GetStableHashCode_m0592E21267B4D12BAF6A3124018FFDB85AA1EDB9 (void);
// 0x0000005F System.String Mirror.Extensions::GetMethodName(System.Delegate)
extern void Extensions_GetMethodName_mBD201B7767E472A8E4F2116F18AD20D437783E0F (void);
// 0x00000060 System.Void Mirror.Extensions::CopyTo(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.List`1<T>)
// 0x00000061 System.Void Mirror.InterestManagement::Awake()
extern void InterestManagement_Awake_m0020EDD8ABA4470A17089BC2A660147FA087F3D9 (void);
// 0x00000062 System.Void Mirror.InterestManagement::Reset()
extern void InterestManagement_Reset_mA592DDE988AB634D50B583EBF5FABF427C1C0F07 (void);
// 0x00000063 System.Boolean Mirror.InterestManagement::OnCheckObserver(Mirror.NetworkIdentity,Mirror.NetworkConnectionToClient)
// 0x00000064 System.Void Mirror.InterestManagement::OnRebuildObservers(Mirror.NetworkIdentity,System.Collections.Generic.HashSet`1<Mirror.NetworkConnectionToClient>)
// 0x00000065 System.Void Mirror.InterestManagement::RebuildAll()
extern void InterestManagement_RebuildAll_m143CB3DE5E99AF95FA1285E5E2D91E5FE2DEA1AD (void);
// 0x00000066 System.Void Mirror.InterestManagement::SetHostVisibility(Mirror.NetworkIdentity,System.Boolean)
extern void InterestManagement_SetHostVisibility_m17A390210930533FAEB21EE83C4EF32675DBE8F9 (void);
// 0x00000067 System.Void Mirror.InterestManagement::OnSpawned(Mirror.NetworkIdentity)
extern void InterestManagement_OnSpawned_m0D03EF9591FEB7D27C28D5BB9B8A0C43BF9F7262 (void);
// 0x00000068 System.Void Mirror.InterestManagement::OnDestroyed(Mirror.NetworkIdentity)
extern void InterestManagement_OnDestroyed_m780A052B4A97A1C03EBA0A0F2780680D5C74CEA1 (void);
// 0x00000069 System.Void Mirror.InterestManagement::.ctor()
extern void InterestManagement__ctor_mC2CB6AFC383E02ACBABDD728A970658A1C3CCFF4 (void);
// 0x0000006A System.Void Mirror.LocalConnectionToClient::.ctor()
extern void LocalConnectionToClient__ctor_m428D1E3E51E34E982DF40E0DCE961195AA003337 (void);
// 0x0000006B System.String Mirror.LocalConnectionToClient::get_address()
extern void LocalConnectionToClient_get_address_m2D7FB00302E2915434C1E5DB9797D4A5A4E33059 (void);
// 0x0000006C System.Void Mirror.LocalConnectionToClient::Send(System.ArraySegment`1<System.Byte>,System.Int32)
extern void LocalConnectionToClient_Send_m2D2251FEA3914828444ED06315BFBE2643AF019E (void);
// 0x0000006D System.Boolean Mirror.LocalConnectionToClient::IsAlive(System.Single)
extern void LocalConnectionToClient_IsAlive_m7A9507EB1C35012CFF5DB54FC6B30C58232A73D6 (void);
// 0x0000006E System.Void Mirror.LocalConnectionToClient::DisconnectInternal()
extern void LocalConnectionToClient_DisconnectInternal_m703C5C7C8D1B2C4C30BF993266497288D47016C2 (void);
// 0x0000006F System.Void Mirror.LocalConnectionToClient::Disconnect()
extern void LocalConnectionToClient_Disconnect_m62485E44133767987CFE31DC010CE7281AE84A92 (void);
// 0x00000070 System.String Mirror.LocalConnectionToServer::get_address()
extern void LocalConnectionToServer_get_address_m0C6B7946CCE095F5AD805DBD421620B031AA6953 (void);
// 0x00000071 System.Void Mirror.LocalConnectionToServer::QueueConnectedEvent()
extern void LocalConnectionToServer_QueueConnectedEvent_m5248D0F3CB749C8DAB1C8D2F8067D0C4291A03BB (void);
// 0x00000072 System.Void Mirror.LocalConnectionToServer::QueueDisconnectedEvent()
extern void LocalConnectionToServer_QueueDisconnectedEvent_m5199F8C95A1A01F7367E0C1B32B2B8E08D673168 (void);
// 0x00000073 System.Void Mirror.LocalConnectionToServer::Send(System.ArraySegment`1<System.Byte>,System.Int32)
extern void LocalConnectionToServer_Send_m7101806705A74ECC702860F1DA86A8E61A3FCC7E (void);
// 0x00000074 System.Void Mirror.LocalConnectionToServer::Update()
extern void LocalConnectionToServer_Update_mD992865B7AB76B7164B41CE40EB7ABBD978805D8 (void);
// 0x00000075 System.Void Mirror.LocalConnectionToServer::DisconnectInternal()
extern void LocalConnectionToServer_DisconnectInternal_m9F0657A0BEBDCCA743A4F3BBCF6AA4E624A9B89B (void);
// 0x00000076 System.Void Mirror.LocalConnectionToServer::Disconnect()
extern void LocalConnectionToServer_Disconnect_m702169BB2063D9E348421A2EB47E83FF04D31E4D (void);
// 0x00000077 System.Boolean Mirror.LocalConnectionToServer::IsAlive(System.Single)
extern void LocalConnectionToServer_IsAlive_m249A5454FEE2D49C444B5D2F181EBB9E773E906D (void);
// 0x00000078 System.Void Mirror.LocalConnectionToServer::.ctor()
extern void LocalConnectionToServer__ctor_m958A380BF9B110F4AD9C701DE3E61583FE6D58AA (void);
// 0x00000079 System.Double Mirror.Mathd::LerpUnclamped(System.Double,System.Double,System.Double)
extern void Mathd_LerpUnclamped_m13CC466F1D9A8E1A3A5DC71D67E5D77893300F12 (void);
// 0x0000007A System.Double Mirror.Mathd::Clamp01(System.Double)
extern void Mathd_Clamp01_mC0705D2095AADDFCA9535C68AAA71737869470E8 (void);
// 0x0000007B System.Double Mirror.Mathd::InverseLerp(System.Double,System.Double,System.Double)
extern void Mathd_InverseLerp_m5EE751E947817D17D845E932A91C5EA5A109DC24 (void);
// 0x0000007C System.Int32 Mirror.MessagePacking::get_MaxContentSize()
extern void MessagePacking_get_MaxContentSize_m661410D0975A474E1CBF705D2FB155C2DE4BEB90 (void);
// 0x0000007D System.UInt16 Mirror.MessagePacking::GetId()
// 0x0000007E System.Void Mirror.MessagePacking::Pack(T,Mirror.NetworkWriter)
// 0x0000007F System.Boolean Mirror.MessagePacking::Unpack(Mirror.NetworkReader,System.UInt16&)
extern void MessagePacking_Unpack_m96DB2B52AAB7CB3F845AE3D940630217DFED1543 (void);
// 0x00000080 Mirror.NetworkMessageDelegate Mirror.MessagePacking::WrapHandler(System.Action`3<C,T,System.Int32>,System.Boolean)
// 0x00000081 Mirror.NetworkMessageDelegate Mirror.MessagePacking::WrapHandler(System.Action`2<C,T>,System.Boolean)
// 0x00000082 System.Void Mirror.MessagePacking/<>c__DisplayClass6_0`2::.ctor()
// 0x00000083 System.Void Mirror.MessagePacking/<>c__DisplayClass6_0`2::<WrapHandler>b__0(Mirror.NetworkConnection,Mirror.NetworkReader,System.Int32)
// 0x00000084 System.Void Mirror.MessagePacking/<>c__DisplayClass7_0`2::.ctor()
// 0x00000085 System.Void Mirror.MessagePacking/<>c__DisplayClass7_0`2::<WrapHandler>g__Wrapped|0(C,T,System.Int32)
// 0x00000086 System.Void Mirror.NetworkPingMessage::.ctor(System.Double)
extern void NetworkPingMessage__ctor_mF340573CD5E74C9BECF60F7789C8170559A95C5D (void);
// 0x00000087 System.Void Mirror.UnityEventNetworkConnection::.ctor()
extern void UnityEventNetworkConnection__ctor_m2E143132E593982075D6CFB3ACE9FC22A2E66F55 (void);
// 0x00000088 System.Void Mirror.NetworkAuthenticator::OnStartServer()
extern void NetworkAuthenticator_OnStartServer_m846E62361157BE3A9A6280B0314DA5D22BDF11B8 (void);
// 0x00000089 System.Void Mirror.NetworkAuthenticator::OnStopServer()
extern void NetworkAuthenticator_OnStopServer_m41CE4A338AE0CA96A3BAB3034A5A56A35A4BF72A (void);
// 0x0000008A System.Void Mirror.NetworkAuthenticator::OnServerAuthenticate(Mirror.NetworkConnectionToClient)
extern void NetworkAuthenticator_OnServerAuthenticate_mEBF420F94B6BE8065D4712B6DEC43BA0F51D8B4B (void);
// 0x0000008B System.Void Mirror.NetworkAuthenticator::ServerAccept(Mirror.NetworkConnectionToClient)
extern void NetworkAuthenticator_ServerAccept_m221411BDD939B7B9832825EB80CA3F44A5A44D14 (void);
// 0x0000008C System.Void Mirror.NetworkAuthenticator::ServerReject(Mirror.NetworkConnectionToClient)
extern void NetworkAuthenticator_ServerReject_mE1AAE10EB8217BE0E7F81F529A1C12804D37C96E (void);
// 0x0000008D System.Void Mirror.NetworkAuthenticator::OnStartClient()
extern void NetworkAuthenticator_OnStartClient_m5C32E06D0A5DD87660C6B25EDD8FBDB30D9BC644 (void);
// 0x0000008E System.Void Mirror.NetworkAuthenticator::OnStopClient()
extern void NetworkAuthenticator_OnStopClient_m1679E3E9ECFA23353E52BABF80DB494ADE6B5B62 (void);
// 0x0000008F System.Void Mirror.NetworkAuthenticator::OnClientAuthenticate()
extern void NetworkAuthenticator_OnClientAuthenticate_mC74560EC6347743730397A477A8B2639A9D9BF28 (void);
// 0x00000090 System.Void Mirror.NetworkAuthenticator::ClientAccept()
extern void NetworkAuthenticator_ClientAccept_m8B68015CCD5DF4A222DB0222EDF3B9A3CD2862E1 (void);
// 0x00000091 System.Void Mirror.NetworkAuthenticator::ClientReject()
extern void NetworkAuthenticator_ClientReject_m923193A3EAFC73EBA13502B73FE1EAB5FA5FC730 (void);
// 0x00000092 System.Void Mirror.NetworkAuthenticator::Reset()
extern void NetworkAuthenticator_Reset_m595DBEB9E798D750D98554E1850D5931558B3FCD (void);
// 0x00000093 System.Void Mirror.NetworkAuthenticator::.ctor()
extern void NetworkAuthenticator__ctor_m1EFE9D58997FA1E95FE79D6196AEBDE5CEC414E5 (void);
// 0x00000094 System.Boolean Mirror.NetworkBehaviour::get_isServer()
extern void NetworkBehaviour_get_isServer_m6CF3499812C1F2679BB924165AA79C59E6D2EBCF (void);
// 0x00000095 System.Boolean Mirror.NetworkBehaviour::get_isClient()
extern void NetworkBehaviour_get_isClient_m87FF41CC03AD448B05627F3711F8E27C63D5C615 (void);
// 0x00000096 System.Boolean Mirror.NetworkBehaviour::get_isLocalPlayer()
extern void NetworkBehaviour_get_isLocalPlayer_mFA35EE97B42DEEE92E4FD5562C8C6A1717607DE0 (void);
// 0x00000097 System.Boolean Mirror.NetworkBehaviour::get_isServerOnly()
extern void NetworkBehaviour_get_isServerOnly_m6247E79FF74BBEC947D4D605095A8A24E5E16FD8 (void);
// 0x00000098 System.Boolean Mirror.NetworkBehaviour::get_isClientOnly()
extern void NetworkBehaviour_get_isClientOnly_m1082A236ADF41204D352DAEE63F652244950C3B1 (void);
// 0x00000099 System.Boolean Mirror.NetworkBehaviour::get_hasAuthority()
extern void NetworkBehaviour_get_hasAuthority_m8C249EBFD9F083DE67F38A74E3245727C6035167 (void);
// 0x0000009A System.UInt32 Mirror.NetworkBehaviour::get_netId()
extern void NetworkBehaviour_get_netId_mFD41F9D183B23443AA528BC0244E0835CCC94826 (void);
// 0x0000009B Mirror.NetworkConnection Mirror.NetworkBehaviour::get_connectionToServer()
extern void NetworkBehaviour_get_connectionToServer_m269C003EE53BFA1EE6A2DBA16A975723C8829421 (void);
// 0x0000009C Mirror.NetworkConnectionToClient Mirror.NetworkBehaviour::get_connectionToClient()
extern void NetworkBehaviour_get_connectionToClient_m76BDC3A8CB43E9A9434569A7C0CA2EF6A428EC77 (void);
// 0x0000009D System.Boolean Mirror.NetworkBehaviour::HasSyncObjects()
extern void NetworkBehaviour_HasSyncObjects_mDD3B1CE94285E413CA1862D945D74156D6510D18 (void);
// 0x0000009E Mirror.NetworkIdentity Mirror.NetworkBehaviour::get_netIdentity()
extern void NetworkBehaviour_get_netIdentity_m67CAF485E29AFA1CB0540A7ADBE68FCE2326151D (void);
// 0x0000009F System.Void Mirror.NetworkBehaviour::set_netIdentity(Mirror.NetworkIdentity)
extern void NetworkBehaviour_set_netIdentity_m4FB00584A78A461AC087E6A31F8A57AE40E05BC0 (void);
// 0x000000A0 System.Int32 Mirror.NetworkBehaviour::get_ComponentIndex()
extern void NetworkBehaviour_get_ComponentIndex_mACCD123A66C72A3D062535CF936618C2158E9D76 (void);
// 0x000000A1 System.Void Mirror.NetworkBehaviour::set_ComponentIndex(System.Int32)
extern void NetworkBehaviour_set_ComponentIndex_mC14387FE3B3E56D945F6B126E08AD618443368DE (void);
// 0x000000A2 System.UInt64 Mirror.NetworkBehaviour::get_syncVarDirtyBits()
extern void NetworkBehaviour_get_syncVarDirtyBits_m1690C7716CF78A4CC6360088FB682385EE76C9E3 (void);
// 0x000000A3 System.Void Mirror.NetworkBehaviour::set_syncVarDirtyBits(System.UInt64)
extern void NetworkBehaviour_set_syncVarDirtyBits_m1FA90B23C70060A48A39E1284A9516B24F996824 (void);
// 0x000000A4 System.Boolean Mirror.NetworkBehaviour::GetSyncVarHookGuard(System.UInt64)
extern void NetworkBehaviour_GetSyncVarHookGuard_m69E27CD31431FD5BBDDA23D06E99E18CF7E2590A (void);
// 0x000000A5 System.Boolean Mirror.NetworkBehaviour::getSyncVarHookGuard(System.UInt64)
extern void NetworkBehaviour_getSyncVarHookGuard_m04C7E518D055E786F2EDD648D995543282812D09 (void);
// 0x000000A6 System.Void Mirror.NetworkBehaviour::SetSyncVarHookGuard(System.UInt64,System.Boolean)
extern void NetworkBehaviour_SetSyncVarHookGuard_mF38BC0B793D2A923C63A3A17CA9CA704B5588756 (void);
// 0x000000A7 System.Void Mirror.NetworkBehaviour::setSyncVarHookGuard(System.UInt64,System.Boolean)
extern void NetworkBehaviour_setSyncVarHookGuard_m44C276207929797CD147789B1E2D5EFF0E8D0B98 (void);
// 0x000000A8 System.Void Mirror.NetworkBehaviour::SetSyncVarDirtyBit(System.UInt64)
extern void NetworkBehaviour_SetSyncVarDirtyBit_mBE9A36805B048D3150A08DFFEC6ADC9CE0239457 (void);
// 0x000000A9 System.Void Mirror.NetworkBehaviour::SetDirtyBit(System.UInt64)
extern void NetworkBehaviour_SetDirtyBit_mDE4C6DE9ECC6E882CBDC4BF66BB65CC3B92A49E5 (void);
// 0x000000AA System.Boolean Mirror.NetworkBehaviour::IsDirty()
extern void NetworkBehaviour_IsDirty_m50CC8C762BF12BBF3D796B0266BBF1E0D90BA1EA (void);
// 0x000000AB System.Void Mirror.NetworkBehaviour::ClearAllDirtyBits()
extern void NetworkBehaviour_ClearAllDirtyBits_mD37CF3E0D4864DA51521989A6D3F4D012BC5E3BE (void);
// 0x000000AC System.Void Mirror.NetworkBehaviour::InitSyncObject(Mirror.SyncObject)
extern void NetworkBehaviour_InitSyncObject_m627E07D5152DFE01AC855565E09043BA70692CDF (void);
// 0x000000AD System.Void Mirror.NetworkBehaviour::SendCommandInternal(System.String,Mirror.NetworkWriter,System.Int32,System.Boolean)
extern void NetworkBehaviour_SendCommandInternal_mEC82E2864E0EFF7FE5D8B1B92DC9151B5AA61220 (void);
// 0x000000AE System.Void Mirror.NetworkBehaviour::SendRPCInternal(System.String,Mirror.NetworkWriter,System.Int32,System.Boolean)
extern void NetworkBehaviour_SendRPCInternal_m1F6F594A7FBDC846223DAD77E2F8362C05DB18B2 (void);
// 0x000000AF System.Void Mirror.NetworkBehaviour::SendTargetRPCInternal(Mirror.NetworkConnection,System.String,Mirror.NetworkWriter,System.Int32)
extern void NetworkBehaviour_SendTargetRPCInternal_m3F79098819821955796385EC5013BDC83652121F (void);
// 0x000000B0 System.Void Mirror.NetworkBehaviour::GeneratedSyncVarSetter(T,T&,System.UInt64,System.Action`2<T,T>)
// 0x000000B1 System.Void Mirror.NetworkBehaviour::GeneratedSyncVarSetter_GameObject(UnityEngine.GameObject,UnityEngine.GameObject&,System.UInt64,System.Action`2<UnityEngine.GameObject,UnityEngine.GameObject>,System.UInt32&)
extern void NetworkBehaviour_GeneratedSyncVarSetter_GameObject_mFE124DB3DDEED7ADD0811521B936716AF3946B85 (void);
// 0x000000B2 System.Void Mirror.NetworkBehaviour::GeneratedSyncVarSetter_NetworkIdentity(Mirror.NetworkIdentity,Mirror.NetworkIdentity&,System.UInt64,System.Action`2<Mirror.NetworkIdentity,Mirror.NetworkIdentity>,System.UInt32&)
extern void NetworkBehaviour_GeneratedSyncVarSetter_NetworkIdentity_m96D20403FEA8419277C8CD4AAD85D460C3A7C47A (void);
// 0x000000B3 System.Void Mirror.NetworkBehaviour::GeneratedSyncVarSetter_NetworkBehaviour(T,T&,System.UInt64,System.Action`2<T,T>,Mirror.NetworkBehaviour/NetworkBehaviourSyncVar&)
// 0x000000B4 System.Boolean Mirror.NetworkBehaviour::SyncVarGameObjectEqual(UnityEngine.GameObject,System.UInt32)
extern void NetworkBehaviour_SyncVarGameObjectEqual_m538EA781DEC2D9D0DF307035D039850460DC5690 (void);
// 0x000000B5 System.Void Mirror.NetworkBehaviour::SetSyncVarGameObject(UnityEngine.GameObject,UnityEngine.GameObject&,System.UInt64,System.UInt32&)
extern void NetworkBehaviour_SetSyncVarGameObject_m58A39D6DE471F63603EDDA893781EACE528246AF (void);
// 0x000000B6 UnityEngine.GameObject Mirror.NetworkBehaviour::GetSyncVarGameObject(System.UInt32,UnityEngine.GameObject&)
extern void NetworkBehaviour_GetSyncVarGameObject_mBD2A0B361900E77AF101885A491B0921FDA4AE3F (void);
// 0x000000B7 System.Boolean Mirror.NetworkBehaviour::SyncVarNetworkIdentityEqual(Mirror.NetworkIdentity,System.UInt32)
extern void NetworkBehaviour_SyncVarNetworkIdentityEqual_mBF505C08B55B2AA9120AE69F365A0C1D7EC092E2 (void);
// 0x000000B8 System.Void Mirror.NetworkBehaviour::GeneratedSyncVarDeserialize(T&,System.Action`2<T,T>,T)
// 0x000000B9 System.Void Mirror.NetworkBehaviour::GeneratedSyncVarDeserialize_GameObject(UnityEngine.GameObject&,System.Action`2<UnityEngine.GameObject,UnityEngine.GameObject>,Mirror.NetworkReader,System.UInt32&)
extern void NetworkBehaviour_GeneratedSyncVarDeserialize_GameObject_m8FA704F8E268520F840C8026BD1365E2B1294840 (void);
// 0x000000BA System.Void Mirror.NetworkBehaviour::GeneratedSyncVarDeserialize_NetworkIdentity(Mirror.NetworkIdentity&,System.Action`2<Mirror.NetworkIdentity,Mirror.NetworkIdentity>,Mirror.NetworkReader,System.UInt32&)
extern void NetworkBehaviour_GeneratedSyncVarDeserialize_NetworkIdentity_m992D1F385125B3D39924FC712A450093DCE28ACC (void);
// 0x000000BB System.Void Mirror.NetworkBehaviour::GeneratedSyncVarDeserialize_NetworkBehaviour(T&,System.Action`2<T,T>,Mirror.NetworkReader,Mirror.NetworkBehaviour/NetworkBehaviourSyncVar&)
// 0x000000BC System.Void Mirror.NetworkBehaviour::SetSyncVarNetworkIdentity(Mirror.NetworkIdentity,Mirror.NetworkIdentity&,System.UInt64,System.UInt32&)
extern void NetworkBehaviour_SetSyncVarNetworkIdentity_m662595FF753AE6B0E87DBC7E72517250A4244339 (void);
// 0x000000BD Mirror.NetworkIdentity Mirror.NetworkBehaviour::GetSyncVarNetworkIdentity(System.UInt32,Mirror.NetworkIdentity&)
extern void NetworkBehaviour_GetSyncVarNetworkIdentity_m7C2CF1934178FA42C8D93CD008F4E174FD944927 (void);
// 0x000000BE System.Boolean Mirror.NetworkBehaviour::SyncVarNetworkBehaviourEqual(T,Mirror.NetworkBehaviour/NetworkBehaviourSyncVar)
// 0x000000BF System.Void Mirror.NetworkBehaviour::SetSyncVarNetworkBehaviour(T,T&,System.UInt64,Mirror.NetworkBehaviour/NetworkBehaviourSyncVar&)
// 0x000000C0 T Mirror.NetworkBehaviour::GetSyncVarNetworkBehaviour(Mirror.NetworkBehaviour/NetworkBehaviourSyncVar,T&)
// 0x000000C1 System.Boolean Mirror.NetworkBehaviour::SyncVarEqual(T,T&)
// 0x000000C2 System.Void Mirror.NetworkBehaviour::SetSyncVar(T,T&,System.UInt64)
// 0x000000C3 System.Boolean Mirror.NetworkBehaviour::OnSerialize(Mirror.NetworkWriter,System.Boolean)
extern void NetworkBehaviour_OnSerialize_mBE2870081075933577F25C4760A3758FC5A73391 (void);
// 0x000000C4 System.Void Mirror.NetworkBehaviour::OnDeserialize(Mirror.NetworkReader,System.Boolean)
extern void NetworkBehaviour_OnDeserialize_mDA83841054FF1970D8C87C246CB56F426195768A (void);
// 0x000000C5 System.Boolean Mirror.NetworkBehaviour::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void NetworkBehaviour_SerializeSyncVars_m4AF8BE097726BA018E3762C14E33BDBF3A5C4819 (void);
// 0x000000C6 System.Void Mirror.NetworkBehaviour::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void NetworkBehaviour_DeserializeSyncVars_mC87ECFD6F9C2D5C1306ABDF2CF6498864C84677A (void);
// 0x000000C7 System.Boolean Mirror.NetworkBehaviour::SerializeObjectsAll(Mirror.NetworkWriter)
extern void NetworkBehaviour_SerializeObjectsAll_mE77BC9DA6B1BA7417C5FF4AA5CB0F31F1882ABF9 (void);
// 0x000000C8 System.Boolean Mirror.NetworkBehaviour::SerializeObjectsDelta(Mirror.NetworkWriter)
extern void NetworkBehaviour_SerializeObjectsDelta_mF7FA7AF00832E10CB3A3A9E9E18A9DFEF315C8FF (void);
// 0x000000C9 System.Void Mirror.NetworkBehaviour::DeSerializeObjectsAll(Mirror.NetworkReader)
extern void NetworkBehaviour_DeSerializeObjectsAll_m6597B6F683F3DCD0FF34B30AE24A95B5CD004C4F (void);
// 0x000000CA System.Void Mirror.NetworkBehaviour::DeSerializeObjectsDelta(Mirror.NetworkReader)
extern void NetworkBehaviour_DeSerializeObjectsDelta_m33229FDA09E4E1EB32F3432C26DFF174563D57CA (void);
// 0x000000CB System.Void Mirror.NetworkBehaviour::ResetSyncObjects()
extern void NetworkBehaviour_ResetSyncObjects_mCC577FB1959F7BE96220152489E55AD993C98D14 (void);
// 0x000000CC System.Void Mirror.NetworkBehaviour::OnStartServer()
extern void NetworkBehaviour_OnStartServer_m7E498D9B74008DCD512C4A01C847795893A5ED2D (void);
// 0x000000CD System.Void Mirror.NetworkBehaviour::OnStopServer()
extern void NetworkBehaviour_OnStopServer_m3553611E0C2705734BD005AF6396A6A7C4994EF1 (void);
// 0x000000CE System.Void Mirror.NetworkBehaviour::OnStartClient()
extern void NetworkBehaviour_OnStartClient_mBE0F27A3CDF5C4F76C777885B2D21AB60A9647E8 (void);
// 0x000000CF System.Void Mirror.NetworkBehaviour::OnStopClient()
extern void NetworkBehaviour_OnStopClient_m6D76DD7B8DC4A4E29D1F58DD69CA6C84E0CC9B81 (void);
// 0x000000D0 System.Void Mirror.NetworkBehaviour::OnStartLocalPlayer()
extern void NetworkBehaviour_OnStartLocalPlayer_m884B22DB458ECACC0E00809576CC2DD2F41DE2B4 (void);
// 0x000000D1 System.Void Mirror.NetworkBehaviour::OnStopLocalPlayer()
extern void NetworkBehaviour_OnStopLocalPlayer_mACF50DFFD20E147C9616BB416F0ECBDF236E4B21 (void);
// 0x000000D2 System.Void Mirror.NetworkBehaviour::OnStartAuthority()
extern void NetworkBehaviour_OnStartAuthority_mC224FDE4601A24F19B2B4A085010764783BB2CED (void);
// 0x000000D3 System.Void Mirror.NetworkBehaviour::OnStopAuthority()
extern void NetworkBehaviour_OnStopAuthority_m3040A6FF682924E89E26C06E7B629D024D87D376 (void);
// 0x000000D4 System.Void Mirror.NetworkBehaviour::.ctor()
extern void NetworkBehaviour__ctor_mB98FF8F52DCEBEB3BC7679DE03FA50785207EE78 (void);
// 0x000000D5 System.Void Mirror.NetworkBehaviour/NetworkBehaviourSyncVar::.ctor(System.UInt32,System.Int32)
extern void NetworkBehaviourSyncVar__ctor_mFB219E84A8139BFFCC899543F4057E03D963B0FF (void);
// 0x000000D6 System.Boolean Mirror.NetworkBehaviour/NetworkBehaviourSyncVar::Equals(Mirror.NetworkBehaviour/NetworkBehaviourSyncVar)
extern void NetworkBehaviourSyncVar_Equals_m2EE0BD631368A1EA28D3E4422A9F14C43B9B0A27 (void);
// 0x000000D7 System.Boolean Mirror.NetworkBehaviour/NetworkBehaviourSyncVar::Equals(System.UInt32,System.Int32)
extern void NetworkBehaviourSyncVar_Equals_m2EE82223A7405A371A233BCED2C78292E69AA85F (void);
// 0x000000D8 System.String Mirror.NetworkBehaviour/NetworkBehaviourSyncVar::ToString()
extern void NetworkBehaviourSyncVar_ToString_m4DE9A94D17F9F28FCDB963AC8665144C37076363 (void);
// 0x000000D9 System.Void Mirror.NetworkBehaviour/<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_m962006B4E384E919173C557496E93594671CB77D (void);
// 0x000000DA System.Void Mirror.NetworkBehaviour/<>c__DisplayClass45_0::<InitSyncObject>b__0()
extern void U3CU3Ec__DisplayClass45_0_U3CInitSyncObjectU3Eb__0_m5C6E3A0D209D6E49E000D11013E633990795C5E5 (void);
// 0x000000DB System.Boolean Mirror.NetworkBehaviour/<>c__DisplayClass45_0::<InitSyncObject>b__1()
extern void U3CU3Ec__DisplayClass45_0_U3CInitSyncObjectU3Eb__1_m136A4E517A3C9D631B9CD6EF8E537B3D110B3719 (void);
// 0x000000DC Mirror.NetworkConnection Mirror.NetworkClient::get_connection()
extern void NetworkClient_get_connection_m5439CD4BADA80C781B783F013464517F581F9557 (void);
// 0x000000DD System.Void Mirror.NetworkClient::set_connection(Mirror.NetworkConnection)
extern void NetworkClient_set_connection_mED66882874FA7C5891570DFDC873B4EC5B678AFC (void);
// 0x000000DE Mirror.NetworkIdentity Mirror.NetworkClient::get_localPlayer()
extern void NetworkClient_get_localPlayer_mFA6B51032C92C1B18EAA3B7FCD0369A089A0020C (void);
// 0x000000DF System.Void Mirror.NetworkClient::set_localPlayer(Mirror.NetworkIdentity)
extern void NetworkClient_set_localPlayer_mCBB4FBB1B611801ABEB2B2BBD6A0FD03365D534F (void);
// 0x000000E0 System.String Mirror.NetworkClient::get_serverIp()
extern void NetworkClient_get_serverIp_m3952EAE51FB2B2067755FDFFE7481BB95A4E146B (void);
// 0x000000E1 System.Boolean Mirror.NetworkClient::get_active()
extern void NetworkClient_get_active_m80C7ACA728DE7F6F6B5DC6C0E80B5AD0D287EA37 (void);
// 0x000000E2 System.Boolean Mirror.NetworkClient::get_isConnecting()
extern void NetworkClient_get_isConnecting_mEFCFA8E2F8F117CED0643652F62F0683B2350C72 (void);
// 0x000000E3 System.Boolean Mirror.NetworkClient::get_isConnected()
extern void NetworkClient_get_isConnected_m58D71078B898D6DA731CDC3D4EDA737E7B04A4E6 (void);
// 0x000000E4 System.Boolean Mirror.NetworkClient::get_isHostClient()
extern void NetworkClient_get_isHostClient_m1A44178EF65BA31D2F9E5469FD739270E1DC18CA (void);
// 0x000000E5 System.Void Mirror.NetworkClient::AddTransportHandlers()
extern void NetworkClient_AddTransportHandlers_mF0A64666B0628413F9F37D72B169C8A7C9DBC920 (void);
// 0x000000E6 System.Void Mirror.NetworkClient::RegisterSystemHandlers(System.Boolean)
extern void NetworkClient_RegisterSystemHandlers_m5BF1C0E926656B42C70C67A146B65FFF8378AB55 (void);
// 0x000000E7 System.Void Mirror.NetworkClient::Connect(System.String)
extern void NetworkClient_Connect_m1012B6CB8305CF83DE993CB961DCBAC2E5366489 (void);
// 0x000000E8 System.Void Mirror.NetworkClient::Connect(System.Uri)
extern void NetworkClient_Connect_mD685E75AAE9CF8F6765C91640C84437777546925 (void);
// 0x000000E9 System.Void Mirror.NetworkClient::ConnectHost()
extern void NetworkClient_ConnectHost_m2359D0E1763DEE430B6920B2C7B4D67A270927C9 (void);
// 0x000000EA System.Void Mirror.NetworkClient::ConnectLocalServer()
extern void NetworkClient_ConnectLocalServer_mCCDFBA08C7B898756DF7812127468B9F68966C7E (void);
// 0x000000EB System.Void Mirror.NetworkClient::Disconnect()
extern void NetworkClient_Disconnect_mD13276168C810E45CA0ED75ABDC3DBF7CBC2F439 (void);
// 0x000000EC System.Void Mirror.NetworkClient::OnTransportConnected()
extern void NetworkClient_OnTransportConnected_m1B39F7AAF7474EC0246ABE6CCEC5BB00BC91EDE4 (void);
// 0x000000ED System.Boolean Mirror.NetworkClient::UnpackAndInvoke(Mirror.NetworkReader,System.Int32)
extern void NetworkClient_UnpackAndInvoke_m6F5034DE78530E962B29E497844B81AF99E889FF (void);
// 0x000000EE System.Void Mirror.NetworkClient::OnTransportData(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkClient_OnTransportData_m424C221EE24669E9D8F3A6B1B8B126453B733B67 (void);
// 0x000000EF System.Void Mirror.NetworkClient::OnTransportDisconnected()
extern void NetworkClient_OnTransportDisconnected_mB728EAB36784C12FE25C8DBF472A733A59C9A207 (void);
// 0x000000F0 System.Void Mirror.NetworkClient::OnError(System.Exception)
extern void NetworkClient_OnError_m3CBE84BB09E2B3B7D4741ECA4F9F2DDAAF5184A1 (void);
// 0x000000F1 System.Void Mirror.NetworkClient::Send(T,System.Int32)
// 0x000000F2 System.Void Mirror.NetworkClient::RegisterHandler(System.Action`1<T>,System.Boolean)
// 0x000000F3 System.Void Mirror.NetworkClient::ReplaceHandler(System.Action`2<Mirror.NetworkConnection,T>,System.Boolean)
// 0x000000F4 System.Void Mirror.NetworkClient::ReplaceHandler(System.Action`1<T>,System.Boolean)
// 0x000000F5 System.Boolean Mirror.NetworkClient::UnregisterHandler()
// 0x000000F6 System.Boolean Mirror.NetworkClient::GetPrefab(System.Guid,UnityEngine.GameObject&)
extern void NetworkClient_GetPrefab_mB24435E12D19697A887BFADE8F8B3D586742999E (void);
// 0x000000F7 System.Void Mirror.NetworkClient::RegisterPrefabIdentity(Mirror.NetworkIdentity)
extern void NetworkClient_RegisterPrefabIdentity_m70F93AE7E4C64AA218627822BB68FFA632E40238 (void);
// 0x000000F8 System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject,System.Guid)
extern void NetworkClient_RegisterPrefab_mDDF91101ACDA56F5CF4F69D165E33040C587BA80 (void);
// 0x000000F9 System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject)
extern void NetworkClient_RegisterPrefab_mD3E596F19CF1A608C524A4A95CEF56BE6EBC5FF4 (void);
// 0x000000FA System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject,System.Guid,Mirror.SpawnDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterPrefab_mADEC9110EDEDFC7D2769EDFBC5AABE4BA782D24A (void);
// 0x000000FB System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject,Mirror.SpawnDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterPrefab_m83A8C1F431CC6E48040A642CB71B3B8E2ED91E2B (void);
// 0x000000FC System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject,System.Guid,Mirror.SpawnHandlerDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterPrefab_mFBD356840D4DD5EF716B650E3C09D5A6CA0A23BB (void);
// 0x000000FD System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject,Mirror.SpawnHandlerDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterPrefab_mA11E07E698EDFC41138E2F9FD3A9CCD94ADAEF05 (void);
// 0x000000FE System.Void Mirror.NetworkClient::UnregisterPrefab(UnityEngine.GameObject)
extern void NetworkClient_UnregisterPrefab_m624DDE9ADA9681E06079404F2B9A80F9D16023E6 (void);
// 0x000000FF System.Void Mirror.NetworkClient::RegisterSpawnHandler(System.Guid,Mirror.SpawnDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterSpawnHandler_m5246FEC89A9B3C563F54F2775BA6CFFC3BA00D9D (void);
// 0x00000100 System.Void Mirror.NetworkClient::RegisterSpawnHandler(System.Guid,Mirror.SpawnHandlerDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterSpawnHandler_m1391B1F148D5E508A90FCC69B39B4A3913A98AB9 (void);
// 0x00000101 System.Void Mirror.NetworkClient::UnregisterSpawnHandler(System.Guid)
extern void NetworkClient_UnregisterSpawnHandler_mFCBC83634E281D816898E45D25C41337B3B69AAE (void);
// 0x00000102 System.Void Mirror.NetworkClient::ClearSpawners()
extern void NetworkClient_ClearSpawners_m92F7ADAD3F0327103A44F3D132D9F89E60F917A7 (void);
// 0x00000103 System.Boolean Mirror.NetworkClient::InvokeUnSpawnHandler(System.Guid,UnityEngine.GameObject)
extern void NetworkClient_InvokeUnSpawnHandler_m303A55418A1FBA4403512BF5B0D7F037850999DB (void);
// 0x00000104 System.Boolean Mirror.NetworkClient::Ready()
extern void NetworkClient_Ready_m0290F8ED5D23A2C8DC4DCEAE68295AAC9644B926 (void);
// 0x00000105 System.Void Mirror.NetworkClient::InternalAddPlayer(Mirror.NetworkIdentity)
extern void NetworkClient_InternalAddPlayer_m53CB34CF7951291A275A589AAB770D9668363B70 (void);
// 0x00000106 System.Boolean Mirror.NetworkClient::AddPlayer()
extern void NetworkClient_AddPlayer_m0C6015BEBE42CDDEB5AE5C5F787108737505B107 (void);
// 0x00000107 System.Void Mirror.NetworkClient::ApplySpawnPayload(Mirror.NetworkIdentity,Mirror.SpawnMessage)
extern void NetworkClient_ApplySpawnPayload_mEB9CA6042E62F380DA7B3F49BAB4538448FF84FF (void);
// 0x00000108 System.Boolean Mirror.NetworkClient::FindOrSpawnObject(Mirror.SpawnMessage,Mirror.NetworkIdentity&)
extern void NetworkClient_FindOrSpawnObject_m73DBFF69AAEC9489D2B8AD54381DB5B39E35A189 (void);
// 0x00000109 Mirror.NetworkIdentity Mirror.NetworkClient::GetExistingObject(System.UInt32)
extern void NetworkClient_GetExistingObject_m8B4FB7CA960913040F4AD6B2082D6106A2E875E2 (void);
// 0x0000010A Mirror.NetworkIdentity Mirror.NetworkClient::SpawnPrefab(Mirror.SpawnMessage)
extern void NetworkClient_SpawnPrefab_m31B1515BF10E16933F18CA5930B06BDD0EEF6595 (void);
// 0x0000010B Mirror.NetworkIdentity Mirror.NetworkClient::SpawnSceneObject(System.UInt64)
extern void NetworkClient_SpawnSceneObject_m9D4871EDCD945BF8DBFBC0C9411F7175A845BE20 (void);
// 0x0000010C Mirror.NetworkIdentity Mirror.NetworkClient::GetAndRemoveSceneObject(System.UInt64)
extern void NetworkClient_GetAndRemoveSceneObject_mF4E2C3C8F607D9848D5D449AAD77D90A6551CD42 (void);
// 0x0000010D System.Boolean Mirror.NetworkClient::ConsiderForSpawning(Mirror.NetworkIdentity)
extern void NetworkClient_ConsiderForSpawning_m775C21D2195F17002BF0F7F60F0C052AAA6934B2 (void);
// 0x0000010E System.Void Mirror.NetworkClient::PrepareToSpawnSceneObjects()
extern void NetworkClient_PrepareToSpawnSceneObjects_m1F51AE31B33D48FD9DDBF1CEC08AA546AEB50D30 (void);
// 0x0000010F System.Void Mirror.NetworkClient::OnObjectSpawnStarted(Mirror.ObjectSpawnStartedMessage)
extern void NetworkClient_OnObjectSpawnStarted_m2F1A04282BE3688D0750D225100B003AE5FE7658 (void);
// 0x00000110 System.Void Mirror.NetworkClient::OnObjectSpawnFinished(Mirror.ObjectSpawnFinishedMessage)
extern void NetworkClient_OnObjectSpawnFinished_m08BC661EAE49DB5C7955421AB8D43CBA1CD78B90 (void);
// 0x00000111 System.Void Mirror.NetworkClient::ClearNullFromSpawned()
extern void NetworkClient_ClearNullFromSpawned_m294A3CF498EC00F34F3E5D27E07E42DB6CF120A6 (void);
// 0x00000112 System.Void Mirror.NetworkClient::OnHostClientObjectDestroy(Mirror.ObjectDestroyMessage)
extern void NetworkClient_OnHostClientObjectDestroy_m821445496B743A10A13697A519B1B7BE76D7154B (void);
// 0x00000113 System.Void Mirror.NetworkClient::OnHostClientObjectHide(Mirror.ObjectHideMessage)
extern void NetworkClient_OnHostClientObjectHide_m9C1D529B105CAD107D472163C75E52130C5CB8BB (void);
// 0x00000114 System.Void Mirror.NetworkClient::OnHostClientSpawn(Mirror.SpawnMessage)
extern void NetworkClient_OnHostClientSpawn_m0B461537BFDC210A94C92142EF85F18E772FCAA5 (void);
// 0x00000115 System.Void Mirror.NetworkClient::OnEntityStateMessage(Mirror.EntityStateMessage)
extern void NetworkClient_OnEntityStateMessage_mB2D5A9AA816B88D1CFECD9C52C65D428ADE3C475 (void);
// 0x00000116 System.Void Mirror.NetworkClient::OnRPCMessage(Mirror.RpcMessage)
extern void NetworkClient_OnRPCMessage_mDB1DC6D93DA81BCFC6604686FBF20951BD087242 (void);
// 0x00000117 System.Void Mirror.NetworkClient::OnObjectHide(Mirror.ObjectHideMessage)
extern void NetworkClient_OnObjectHide_mECE8726EE3F62C50CB3541A558AB0A478995D0E7 (void);
// 0x00000118 System.Void Mirror.NetworkClient::OnObjectDestroy(Mirror.ObjectDestroyMessage)
extern void NetworkClient_OnObjectDestroy_mCD8DDAF1FD872969E841C2C7E9946E8FA5299766 (void);
// 0x00000119 System.Void Mirror.NetworkClient::OnSpawn(Mirror.SpawnMessage)
extern void NetworkClient_OnSpawn_m757BA45D182CA43B9FFBF7E03120358BC0836C98 (void);
// 0x0000011A System.Void Mirror.NetworkClient::OnChangeOwner(Mirror.ChangeOwnerMessage)
extern void NetworkClient_OnChangeOwner_m7735A531EC4EA93EDB59F3FE839B5030097AB364 (void);
// 0x0000011B System.Void Mirror.NetworkClient::ChangeOwner(Mirror.NetworkIdentity,Mirror.ChangeOwnerMessage)
extern void NetworkClient_ChangeOwner_mE0005ED3EC2D4400854784AAF0E13B54B37D06D9 (void);
// 0x0000011C System.Void Mirror.NetworkClient::CheckForLocalPlayer(Mirror.NetworkIdentity)
extern void NetworkClient_CheckForLocalPlayer_m9AA764A2840876F38D3CC123AE637365A9A025C8 (void);
// 0x0000011D System.Void Mirror.NetworkClient::DestroyObject(System.UInt32)
extern void NetworkClient_DestroyObject_m58E70E859CF6E27F0018B060C2990757BFC5DEFC (void);
// 0x0000011E System.Void Mirror.NetworkClient::NetworkEarlyUpdate()
extern void NetworkClient_NetworkEarlyUpdate_m4BB991D8FD06E909BDE076B87895113E9A510F94 (void);
// 0x0000011F System.Void Mirror.NetworkClient::NetworkLateUpdate()
extern void NetworkClient_NetworkLateUpdate_mD996BE78A2574AA44050056BAC9E3F7AC4FFDBDB (void);
// 0x00000120 System.Void Mirror.NetworkClient::DestroyAllClientObjects()
extern void NetworkClient_DestroyAllClientObjects_m895C02874F863A82A1FDB4015F223CF4DDBAD324 (void);
// 0x00000121 System.Void Mirror.NetworkClient::Shutdown()
extern void NetworkClient_Shutdown_m362C444833B9302098E6CC73EFA59309FC7B3D4D (void);
// 0x00000122 System.Void Mirror.NetworkClient::.cctor()
extern void NetworkClient__cctor_m051C6F073FEAF3D99E7E41C2F5A72AACBA005313 (void);
// 0x00000123 System.Void Mirror.NetworkClient/<>c::.cctor()
extern void U3CU3Ec__cctor_mBED38AB857BA061D80B1B06261A24C9C56744C10 (void);
// 0x00000124 System.Void Mirror.NetworkClient/<>c::.ctor()
extern void U3CU3Ec__ctor_m66C0ED4876EBA658BD0CF465BFA73ECA9EBDA408 (void);
// 0x00000125 System.Void Mirror.NetworkClient/<>c::<RegisterSystemHandlers>b__34_0(Mirror.NetworkPongMessage)
extern void U3CU3Ec_U3CRegisterSystemHandlersU3Eb__34_0_m92497DB7FC6650C4647A5C4DCCA5D9087A1B833D (void);
// 0x00000126 System.Void Mirror.NetworkClient/<>c::<RegisterSystemHandlers>b__34_1(Mirror.ObjectSpawnStartedMessage)
extern void U3CU3Ec_U3CRegisterSystemHandlersU3Eb__34_1_m35C43719C0947907FD5081589A8CA58BE835634C (void);
// 0x00000127 System.Void Mirror.NetworkClient/<>c::<RegisterSystemHandlers>b__34_2(Mirror.ObjectSpawnFinishedMessage)
extern void U3CU3Ec_U3CRegisterSystemHandlersU3Eb__34_2_m6FF355334D71914A677670E1EE9530E921CA22F1 (void);
// 0x00000128 System.Void Mirror.NetworkClient/<>c::<RegisterSystemHandlers>b__34_3(Mirror.EntityStateMessage)
extern void U3CU3Ec_U3CRegisterSystemHandlersU3Eb__34_3_m91EB283DB4D9E498D6261615691CB196C940B4B7 (void);
// 0x00000129 System.UInt32 Mirror.NetworkClient/<>c::<OnObjectSpawnFinished>b__76_0(Mirror.NetworkIdentity)
extern void U3CU3Ec_U3COnObjectSpawnFinishedU3Eb__76_0_m9650D466911A0B71616F89A878B302721616A9FC (void);
// 0x0000012A System.Void Mirror.NetworkClient/<>c__DisplayClass46_0`1::.ctor()
// 0x0000012B System.Void Mirror.NetworkClient/<>c__DisplayClass46_0`1::<RegisterHandler>g__HandlerWrapped|0(Mirror.NetworkConnection,T)
// 0x0000012C System.Void Mirror.NetworkClient/<>c__DisplayClass48_0`1::.ctor()
// 0x0000012D System.Void Mirror.NetworkClient/<>c__DisplayClass48_0`1::<ReplaceHandler>b__0(Mirror.NetworkConnection,T)
// 0x0000012E System.Void Mirror.NetworkClient/<>c__DisplayClass54_0::.ctor()
extern void U3CU3Ec__DisplayClass54_0__ctor_mD023BF815BEDF3FBCF83F53AE522428A1687B60C (void);
// 0x0000012F UnityEngine.GameObject Mirror.NetworkClient/<>c__DisplayClass54_0::<RegisterPrefab>b__0(Mirror.SpawnMessage)
extern void U3CU3Ec__DisplayClass54_0_U3CRegisterPrefabU3Eb__0_mAFEF885E19651E91D5D6B62F2E260A3427AD6C5C (void);
// 0x00000130 System.Void Mirror.NetworkClient/<>c__DisplayClass55_0::.ctor()
extern void U3CU3Ec__DisplayClass55_0__ctor_mB7594B3AFDBD832B2444954BB314FD2A52B91F5F (void);
// 0x00000131 UnityEngine.GameObject Mirror.NetworkClient/<>c__DisplayClass55_0::<RegisterPrefab>b__0(Mirror.SpawnMessage)
extern void U3CU3Ec__DisplayClass55_0_U3CRegisterPrefabU3Eb__0_m8705332689EA30149F1594815E659A922E62445B (void);
// 0x00000132 System.Void Mirror.NetworkClient/<>c__DisplayClass59_0::.ctor()
extern void U3CU3Ec__DisplayClass59_0__ctor_m822CBD9D445088BAA0AE8CC07D967875449CA774 (void);
// 0x00000133 UnityEngine.GameObject Mirror.NetworkClient/<>c__DisplayClass59_0::<RegisterSpawnHandler>b__0(Mirror.SpawnMessage)
extern void U3CU3Ec__DisplayClass59_0_U3CRegisterSpawnHandlerU3Eb__0_m52627ED4B00407B509E6C079FFFA49B1520611E7 (void);
// 0x00000134 System.Collections.Generic.HashSet`1<Mirror.NetworkIdentity> Mirror.NetworkConnection::get_observing()
extern void NetworkConnection_get_observing_m8B3F6C988707FAED9B086C8FB7324EFA11C703AD (void);
// 0x00000135 System.String Mirror.NetworkConnection::get_address()
// 0x00000136 Mirror.NetworkIdentity Mirror.NetworkConnection::get_identity()
extern void NetworkConnection_get_identity_mF8F7D1AA28117C2F53450E6697D2966DB1B16F45 (void);
// 0x00000137 System.Void Mirror.NetworkConnection::set_identity(Mirror.NetworkIdentity)
extern void NetworkConnection_set_identity_m6BD8F3D5B3542256F7BB47F17C18AC51B6EB5CC9 (void);
// 0x00000138 System.Collections.Generic.HashSet`1<Mirror.NetworkIdentity> Mirror.NetworkConnection::get_clientOwnedObjects()
extern void NetworkConnection_get_clientOwnedObjects_m6EC6D84417588BE723FBD92B91F6E37F0562373F (void);
// 0x00000139 System.Double Mirror.NetworkConnection::get_remoteTimeStamp()
extern void NetworkConnection_get_remoteTimeStamp_mD1FBDB75191518C80B665E0FFFEA7FF2ECEB3EF3 (void);
// 0x0000013A System.Void Mirror.NetworkConnection::set_remoteTimeStamp(System.Double)
extern void NetworkConnection_set_remoteTimeStamp_m96492955B3F2E2218AE1D47406082A64F0AA5B54 (void);
// 0x0000013B System.Void Mirror.NetworkConnection::.ctor()
extern void NetworkConnection__ctor_mA4169F8A1B0EF786615F138B3395E621AC733E3D (void);
// 0x0000013C System.Void Mirror.NetworkConnection::.ctor(System.Int32)
extern void NetworkConnection__ctor_m617332B65E39A90B5D3AC66B867A5338BDFEB85E (void);
// 0x0000013D Mirror.Batcher Mirror.NetworkConnection::GetBatchForChannelId(System.Int32)
extern void NetworkConnection_GetBatchForChannelId_mDC908F676511D9C6E5E3940FB0A269A4F351D123 (void);
// 0x0000013E System.Boolean Mirror.NetworkConnection::ValidatePacketSize(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkConnection_ValidatePacketSize_m623E054D7EACF8628D11D6CC96CBF18D5575F42E (void);
// 0x0000013F System.Void Mirror.NetworkConnection::Send(T,System.Int32)
// 0x00000140 System.Void Mirror.NetworkConnection::Send(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkConnection_Send_mB0574477C6D5B9CDF9B660239051B76E40AC3B68 (void);
// 0x00000141 System.Void Mirror.NetworkConnection::SendToTransport(System.ArraySegment`1<System.Byte>,System.Int32)
// 0x00000142 System.Void Mirror.NetworkConnection::Update()
extern void NetworkConnection_Update_m25D98F1DD1E6D7AF386E81ED32202D3FD0B5C369 (void);
// 0x00000143 System.Boolean Mirror.NetworkConnection::IsAlive(System.Single)
extern void NetworkConnection_IsAlive_mB23FF1F3CB3DCF3ECE7533A1179FC1977B9524FC (void);
// 0x00000144 System.Void Mirror.NetworkConnection::Disconnect()
// 0x00000145 System.String Mirror.NetworkConnection::ToString()
extern void NetworkConnection_ToString_m15D8D27A4FE3967B3BE72F9B5BFC08D4EF2D0CDB (void);
// 0x00000146 System.String Mirror.NetworkConnectionToClient::get_address()
extern void NetworkConnectionToClient_get_address_mB7234483EEF04B6B7E5F3837918B606ACA8F77BD (void);
// 0x00000147 System.Void Mirror.NetworkConnectionToClient::.ctor(System.Int32)
extern void NetworkConnectionToClient__ctor_m5301FC38234CB2FD85ABCE37EE92D51179572776 (void);
// 0x00000148 System.Void Mirror.NetworkConnectionToClient::SendToTransport(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkConnectionToClient_SendToTransport_m550D67F19E3780187B2913566ED3955B12F0A83B (void);
// 0x00000149 System.Void Mirror.NetworkConnectionToClient::Disconnect()
extern void NetworkConnectionToClient_Disconnect_mF451E9202AE6F4664F777BBE2A6CCA497EB9E7AC (void);
// 0x0000014A System.Void Mirror.NetworkConnectionToClient::AddToObserving(Mirror.NetworkIdentity)
extern void NetworkConnectionToClient_AddToObserving_m46B6A4674F416CC9B7F87FD03002B86D79D453AC (void);
// 0x0000014B System.Void Mirror.NetworkConnectionToClient::RemoveFromObserving(Mirror.NetworkIdentity,System.Boolean)
extern void NetworkConnectionToClient_RemoveFromObserving_mC760146C9A79307754A8A3A2FEE93BF4C7D7559A (void);
// 0x0000014C System.Void Mirror.NetworkConnectionToClient::RemoveFromObservingsObservers()
extern void NetworkConnectionToClient_RemoveFromObservingsObservers_m0CFE675A58F545498CF275EDE63322E4AD88B4D4 (void);
// 0x0000014D System.Void Mirror.NetworkConnectionToClient::AddOwnedObject(Mirror.NetworkIdentity)
extern void NetworkConnectionToClient_AddOwnedObject_m6AD1D341C35E579CCB2B8469018C1F04DBD3F93B (void);
// 0x0000014E System.Void Mirror.NetworkConnectionToClient::RemoveOwnedObject(Mirror.NetworkIdentity)
extern void NetworkConnectionToClient_RemoveOwnedObject_m9181B973ECD712FA5729803CD18DA59DE756E216 (void);
// 0x0000014F System.Void Mirror.NetworkConnectionToClient::DestroyOwnedObjects()
extern void NetworkConnectionToClient_DestroyOwnedObjects_m7AB4B6AAFD8244B596C02C303DFDCDD9543235C5 (void);
// 0x00000150 System.String Mirror.NetworkConnectionToServer::get_address()
extern void NetworkConnectionToServer_get_address_m93A6B095B543ECB8D3E8D7C62FD04ED19B9DF3F2 (void);
// 0x00000151 System.Void Mirror.NetworkConnectionToServer::SendToTransport(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkConnectionToServer_SendToTransport_m1DFF5D04A40CEF55768D68F1DC598427CB0CB179 (void);
// 0x00000152 System.Void Mirror.NetworkConnectionToServer::Disconnect()
extern void NetworkConnectionToServer_Disconnect_mAF11CAB599FAAA6907A6A53DF0FF35E34903EDC1 (void);
// 0x00000153 System.Void Mirror.NetworkConnectionToServer::.ctor()
extern void NetworkConnectionToServer__ctor_m59A1F6CDD13D875CDC80886DF418F29331FD12F4 (void);
// 0x00000154 System.Void Mirror.NetworkDiagnostics::add_OutMessageEvent(System.Action`1<Mirror.NetworkDiagnostics/MessageInfo>)
extern void NetworkDiagnostics_add_OutMessageEvent_m509EFD06CA54100CA6D78D553E686D705AA7E14B (void);
// 0x00000155 System.Void Mirror.NetworkDiagnostics::remove_OutMessageEvent(System.Action`1<Mirror.NetworkDiagnostics/MessageInfo>)
extern void NetworkDiagnostics_remove_OutMessageEvent_m1FE3832B053A95A4B9DE0775797F009BEA175AB2 (void);
// 0x00000156 System.Void Mirror.NetworkDiagnostics::add_InMessageEvent(System.Action`1<Mirror.NetworkDiagnostics/MessageInfo>)
extern void NetworkDiagnostics_add_InMessageEvent_mC6E41C981DFCF20D9FAA9C2CB603D15F543E2729 (void);
// 0x00000157 System.Void Mirror.NetworkDiagnostics::remove_InMessageEvent(System.Action`1<Mirror.NetworkDiagnostics/MessageInfo>)
extern void NetworkDiagnostics_remove_InMessageEvent_m548727ACDD2579D73D281D4422E1DFC561FE96FB (void);
// 0x00000158 System.Void Mirror.NetworkDiagnostics::ResetStatics()
extern void NetworkDiagnostics_ResetStatics_m8823AD524923C5DD10B928ED8C36D3346BEC7972 (void);
// 0x00000159 System.Void Mirror.NetworkDiagnostics::OnSend(T,System.Int32,System.Int32,System.Int32)
// 0x0000015A System.Void Mirror.NetworkDiagnostics::OnReceive(T,System.Int32,System.Int32)
// 0x0000015B System.Void Mirror.NetworkDiagnostics/MessageInfo::.ctor(Mirror.NetworkMessage,System.Int32,System.Int32,System.Int32)
extern void MessageInfo__ctor_mAC67988BF4FBFD84F5201A8DBFEA962167787671 (void);
// 0x0000015C System.Boolean Mirror.NetworkIdentity::get_isClient()
extern void NetworkIdentity_get_isClient_mA664D866C9B791BA0D554F5F1373FCB0FA1A571C (void);
// 0x0000015D System.Void Mirror.NetworkIdentity::set_isClient(System.Boolean)
extern void NetworkIdentity_set_isClient_m6B0DD90AF225C53612890CD949C344DC3AF17CB6 (void);
// 0x0000015E System.Boolean Mirror.NetworkIdentity::get_isServer()
extern void NetworkIdentity_get_isServer_m4BE8C60D704E40109BC7A11007F77178A557B6F2 (void);
// 0x0000015F System.Void Mirror.NetworkIdentity::set_isServer(System.Boolean)
extern void NetworkIdentity_set_isServer_mE11EFCB6F277961CF8BF2FF363F3D4ACA936C2B3 (void);
// 0x00000160 System.Boolean Mirror.NetworkIdentity::get_isLocalPlayer()
extern void NetworkIdentity_get_isLocalPlayer_m9E8156906B197598DD5E56CCB463BFD80EF0BC76 (void);
// 0x00000161 System.Void Mirror.NetworkIdentity::set_isLocalPlayer(System.Boolean)
extern void NetworkIdentity_set_isLocalPlayer_mC5BD33CD0B88ED65D5B55FA49FF86F7635291193 (void);
// 0x00000162 System.Boolean Mirror.NetworkIdentity::get_isServerOnly()
extern void NetworkIdentity_get_isServerOnly_m7FE05E43DDC8562E9C848BE93EA80827F8B2431C (void);
// 0x00000163 System.Boolean Mirror.NetworkIdentity::get_isClientOnly()
extern void NetworkIdentity_get_isClientOnly_m315291F7D383386522570D0E3455CBA9A8DD2871 (void);
// 0x00000164 System.Boolean Mirror.NetworkIdentity::get_hasAuthority()
extern void NetworkIdentity_get_hasAuthority_mC0FA6F347408FB65CC629E407CA2DBE9EB4B520B (void);
// 0x00000165 System.Void Mirror.NetworkIdentity::set_hasAuthority(System.Boolean)
extern void NetworkIdentity_set_hasAuthority_mB13BDE976C8B19F28C6F8219BBC28D426CB92EE8 (void);
// 0x00000166 System.UInt32 Mirror.NetworkIdentity::get_netId()
extern void NetworkIdentity_get_netId_m3FF02B719B8AE0B6A3483063A373AFFB2489C0FA (void);
// 0x00000167 System.Void Mirror.NetworkIdentity::set_netId(System.UInt32)
extern void NetworkIdentity_set_netId_m7DCA5820ABD4B361D3B6B08A93429BB932A610A9 (void);
// 0x00000168 Mirror.NetworkConnection Mirror.NetworkIdentity::get_connectionToServer()
extern void NetworkIdentity_get_connectionToServer_m5B5462ECC017128A99205EDEF095B3FFDBF4EF39 (void);
// 0x00000169 System.Void Mirror.NetworkIdentity::set_connectionToServer(Mirror.NetworkConnection)
extern void NetworkIdentity_set_connectionToServer_m4E36D6FCC72BB5F99830549C9DE19DD350535ADF (void);
// 0x0000016A Mirror.NetworkConnectionToClient Mirror.NetworkIdentity::get_connectionToClient()
extern void NetworkIdentity_get_connectionToClient_mF85737F2CC90FC7E77FE8385F35F2FF2E692D82A (void);
// 0x0000016B System.Void Mirror.NetworkIdentity::set_connectionToClient(Mirror.NetworkConnectionToClient)
extern void NetworkIdentity_set_connectionToClient_mD0DBB925AA6F5BEFD9D79C0BB1EE4E0362971129 (void);
// 0x0000016C System.Collections.Generic.Dictionary`2<System.UInt32,Mirror.NetworkIdentity> Mirror.NetworkIdentity::get_spawned()
extern void NetworkIdentity_get_spawned_mFD19D87CF5E290D78893B6ED88611ACC1860D139 (void);
// 0x0000016D Mirror.NetworkBehaviour[] Mirror.NetworkIdentity::get_NetworkBehaviours()
extern void NetworkIdentity_get_NetworkBehaviours_m7D27CEBD20ABC0925B9C7187E04E43CAC99AE8B0 (void);
// 0x0000016E System.Void Mirror.NetworkIdentity::set_NetworkBehaviours(Mirror.NetworkBehaviour[])
extern void NetworkIdentity_set_NetworkBehaviours_mEEBC35C794AB5516F94F1D9F6D67A7A4A93DB51A (void);
// 0x0000016F System.Guid Mirror.NetworkIdentity::get_assetId()
extern void NetworkIdentity_get_assetId_m3285D38FE7DE4D1F3640E5E50B2D2C88D44BA2F6 (void);
// 0x00000170 System.Void Mirror.NetworkIdentity::set_assetId(System.Guid)
extern void NetworkIdentity_set_assetId_m695FA033D0740CBF83F1EEDE8B29FDF5AA8D83AF (void);
// 0x00000171 System.Void Mirror.NetworkIdentity::ResetStatics()
extern void NetworkIdentity_ResetStatics_m1CE994D4F7672894D26295DD98DCEE63AE46B171 (void);
// 0x00000172 Mirror.NetworkIdentity Mirror.NetworkIdentity::GetSceneIdentity(System.UInt64)
extern void NetworkIdentity_GetSceneIdentity_mD960C920AAEA6C0BF5BD16A3DEDED4A80F7DFF05 (void);
// 0x00000173 System.Void Mirror.NetworkIdentity::SetClientOwner(Mirror.NetworkConnectionToClient)
extern void NetworkIdentity_SetClientOwner_m4A4C5A2B4F8BFA439EC88A39161776B435C7CDEA (void);
// 0x00000174 System.UInt32 Mirror.NetworkIdentity::GetNextNetworkId()
extern void NetworkIdentity_GetNextNetworkId_m4B5B89CE5A7B120E0F1AC8092103D2A7C959D3B3 (void);
// 0x00000175 System.Void Mirror.NetworkIdentity::ResetNextNetworkId()
extern void NetworkIdentity_ResetNextNetworkId_m53391AEF97823B5133DB4F523A957998AF349014 (void);
// 0x00000176 System.Void Mirror.NetworkIdentity::add_clientAuthorityCallback(Mirror.NetworkIdentity/ClientAuthorityCallback)
extern void NetworkIdentity_add_clientAuthorityCallback_m633CD9516DED0EB53EB4E1E87BB9EAA15AD40308 (void);
// 0x00000177 System.Void Mirror.NetworkIdentity::remove_clientAuthorityCallback(Mirror.NetworkIdentity/ClientAuthorityCallback)
extern void NetworkIdentity_remove_clientAuthorityCallback_mB13DCC6DA39776FA5286B9A836FB6EE302740AE0 (void);
// 0x00000178 System.Boolean Mirror.NetworkIdentity::get_SpawnedFromInstantiate()
extern void NetworkIdentity_get_SpawnedFromInstantiate_mF9CCC6D13CD07F8D25FD2D4762D3A2E82F12E2CC (void);
// 0x00000179 System.Void Mirror.NetworkIdentity::set_SpawnedFromInstantiate(System.Boolean)
extern void NetworkIdentity_set_SpawnedFromInstantiate_mF6F78EE5B73A8AD10495BE8EF356812CE273D469 (void);
// 0x0000017A System.Void Mirror.NetworkIdentity::InitializeNetworkBehaviours()
extern void NetworkIdentity_InitializeNetworkBehaviours_m0DC83CA536354E0EF1FB35B2839CD1AF68000B19 (void);
// 0x0000017B System.Void Mirror.NetworkIdentity::Awake()
extern void NetworkIdentity_Awake_m5278295D326C59B7A020FD899C7BC97922D05FE6 (void);
// 0x0000017C System.Void Mirror.NetworkIdentity::OnValidate()
extern void NetworkIdentity_OnValidate_m226702DAC2E56D685492FD4AC1C6F06A6C1B22E4 (void);
// 0x0000017D System.Void Mirror.NetworkIdentity::OnDestroy()
extern void NetworkIdentity_OnDestroy_mBE43AC5BCEC90EA29BEF95B400892C62439E4BF0 (void);
// 0x0000017E System.Void Mirror.NetworkIdentity::OnStartServer()
extern void NetworkIdentity_OnStartServer_m0F2FCCC99FA0C984742DED2406C184A9B883226A (void);
// 0x0000017F System.Void Mirror.NetworkIdentity::OnStopServer()
extern void NetworkIdentity_OnStopServer_m256B4D399B052376C17A63ED5866B9B1AD22F2EF (void);
// 0x00000180 System.Void Mirror.NetworkIdentity::OnStartClient()
extern void NetworkIdentity_OnStartClient_m32E83A6A595E3F99CCC214FC71AB952D4B409541 (void);
// 0x00000181 System.Void Mirror.NetworkIdentity::OnStopClient()
extern void NetworkIdentity_OnStopClient_m0BAA5DBA51A92275C387D90D17A54D8033733D82 (void);
// 0x00000182 System.Void Mirror.NetworkIdentity::OnStartLocalPlayer()
extern void NetworkIdentity_OnStartLocalPlayer_m8BB0D60CFE045D1D76E78C71BB12287275F1195E (void);
// 0x00000183 System.Void Mirror.NetworkIdentity::OnStopLocalPlayer()
extern void NetworkIdentity_OnStopLocalPlayer_m420155E3121A79F98489C82238CD9D23A3343006 (void);
// 0x00000184 System.Void Mirror.NetworkIdentity::NotifyAuthority()
extern void NetworkIdentity_NotifyAuthority_mABE2E3765FF6730E79FFA54064DA772FF2F185F3 (void);
// 0x00000185 System.Void Mirror.NetworkIdentity::OnStartAuthority()
extern void NetworkIdentity_OnStartAuthority_mF7593974D2A81557BE023303A33B724590D8E194 (void);
// 0x00000186 System.Void Mirror.NetworkIdentity::OnStopAuthority()
extern void NetworkIdentity_OnStopAuthority_m6124985322A401EFFAA46CB41C4B5141E6DBA592 (void);
// 0x00000187 System.Boolean Mirror.NetworkIdentity::OnSerializeSafely(Mirror.NetworkBehaviour,Mirror.NetworkWriter,System.Boolean)
extern void NetworkIdentity_OnSerializeSafely_m5612BD0E29030C4166F0BF245E7F4253C0EC1A46 (void);
// 0x00000188 System.Void Mirror.NetworkIdentity::OnSerializeAllSafely(System.Boolean,Mirror.NetworkWriter,Mirror.NetworkWriter)
extern void NetworkIdentity_OnSerializeAllSafely_mED5DEB32511CC049B8062A96A1012F3EFE92E789 (void);
// 0x00000189 Mirror.NetworkIdentitySerialization Mirror.NetworkIdentity::GetSerializationAtTick(System.Int32)
extern void NetworkIdentity_GetSerializationAtTick_m441032D3E28D2384CA2C55A4D7217764B0B038E7 (void);
// 0x0000018A System.Void Mirror.NetworkIdentity::OnDeserializeSafely(Mirror.NetworkBehaviour,Mirror.NetworkReader,System.Boolean)
extern void NetworkIdentity_OnDeserializeSafely_m930E63C2BA46D9E0678F3A7F9AA98E347EDD6BBA (void);
// 0x0000018B System.Void Mirror.NetworkIdentity::OnDeserializeAllSafely(Mirror.NetworkReader,System.Boolean)
extern void NetworkIdentity_OnDeserializeAllSafely_m09CD02B14342498B755DCEA0B498E4085CE45699 (void);
// 0x0000018C System.Void Mirror.NetworkIdentity::HandleRemoteCall(System.Int32,System.Int32,Mirror.RemoteCalls.RemoteCallType,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkIdentity_HandleRemoteCall_m57CBE918D43018F5C3DE87BB9E898513737B13FC (void);
// 0x0000018D System.Void Mirror.NetworkIdentity::AddObserver(Mirror.NetworkConnectionToClient)
extern void NetworkIdentity_AddObserver_m9E79C7D6A2BCFE1CC2064FBE5DAED6067E80EA10 (void);
// 0x0000018E System.Void Mirror.NetworkIdentity::RemoveObserver(Mirror.NetworkConnection)
extern void NetworkIdentity_RemoveObserver_m13F3F65F59B18EC0B46395A9AA514795E950EC1C (void);
// 0x0000018F System.Void Mirror.NetworkIdentity::ClearObservers()
extern void NetworkIdentity_ClearObservers_m989F20077372E5E0E1248D5DB8865BF1BED11216 (void);
// 0x00000190 System.Boolean Mirror.NetworkIdentity::AssignClientAuthority(Mirror.NetworkConnectionToClient)
extern void NetworkIdentity_AssignClientAuthority_m4C2EF50FBDF63CBF594B9E89A56BA04D9F44B5AD (void);
// 0x00000191 System.Void Mirror.NetworkIdentity::RemoveClientAuthority()
extern void NetworkIdentity_RemoveClientAuthority_mB0A56953931D826CA1ACF66DF53F98B961674E8C (void);
// 0x00000192 System.Void Mirror.NetworkIdentity::Reset()
extern void NetworkIdentity_Reset_m883C3F9B8DE395014DB12FE2787BA4975BA3E50E (void);
// 0x00000193 System.Void Mirror.NetworkIdentity::ClearAllComponentsDirtyBits()
extern void NetworkIdentity_ClearAllComponentsDirtyBits_m98279299BDD30A3A319544445922E128065FCFEB (void);
// 0x00000194 System.Void Mirror.NetworkIdentity::ClearDirtyComponentsDirtyBits()
extern void NetworkIdentity_ClearDirtyComponentsDirtyBits_m02EBEB1FF48E0892B9E33863D1ABE6200B916FD9 (void);
// 0x00000195 System.Void Mirror.NetworkIdentity::ResetSyncObjects()
extern void NetworkIdentity_ResetSyncObjects_m01A06B2CED1EFF81D34B22058741D54AADDCB2BC (void);
// 0x00000196 System.Void Mirror.NetworkIdentity::.ctor()
extern void NetworkIdentity__ctor_m4105C071BA18EBD036577EB3F22D2D346A512A28 (void);
// 0x00000197 System.Void Mirror.NetworkIdentity::.cctor()
extern void NetworkIdentity__cctor_m0715F33040D20006B1E16209699F5A4BA7F5FE5E (void);
// 0x00000198 System.Void Mirror.NetworkIdentity/ClientAuthorityCallback::.ctor(System.Object,System.IntPtr)
extern void ClientAuthorityCallback__ctor_m5CB257AFC43DEA6B8CF7BDC3B8F05DB42B6BD7C7 (void);
// 0x00000199 System.Void Mirror.NetworkIdentity/ClientAuthorityCallback::Invoke(Mirror.NetworkConnectionToClient,Mirror.NetworkIdentity,System.Boolean)
extern void ClientAuthorityCallback_Invoke_mAA82FAC912867AFA1817E8CD00F3DD42F94BBDD1 (void);
// 0x0000019A System.IAsyncResult Mirror.NetworkIdentity/ClientAuthorityCallback::BeginInvoke(Mirror.NetworkConnectionToClient,Mirror.NetworkIdentity,System.Boolean,System.AsyncCallback,System.Object)
extern void ClientAuthorityCallback_BeginInvoke_mA2DCE99677B40D7A76BF9DEE2E5B0A3CC6DD2609 (void);
// 0x0000019B System.Void Mirror.NetworkIdentity/ClientAuthorityCallback::EndInvoke(System.IAsyncResult)
extern void ClientAuthorityCallback_EndInvoke_m9D51E32557679438EA41FEE710455EBF916C4C97 (void);
// 0x0000019C System.Void Mirror.NetworkLoop::ResetStatics()
extern void NetworkLoop_ResetStatics_mA141100B577B33CE8A63EDDED1840641DA47FC75 (void);
// 0x0000019D System.Int32 Mirror.NetworkLoop::FindPlayerLoopEntryIndex(UnityEngine.LowLevel.PlayerLoopSystem/UpdateFunction,UnityEngine.LowLevel.PlayerLoopSystem,System.Type)
extern void NetworkLoop_FindPlayerLoopEntryIndex_m8401214D75A51D4212D332E8E5CB5D9337B27FEE (void);
// 0x0000019E System.Boolean Mirror.NetworkLoop::AddToPlayerLoop(UnityEngine.LowLevel.PlayerLoopSystem/UpdateFunction,System.Type,UnityEngine.LowLevel.PlayerLoopSystem&,System.Type,Mirror.NetworkLoop/AddMode)
extern void NetworkLoop_AddToPlayerLoop_mA203DBCE1BA2E03091F1A59F981936532FFC3101 (void);
// 0x0000019F System.Void Mirror.NetworkLoop::RuntimeInitializeOnLoad()
extern void NetworkLoop_RuntimeInitializeOnLoad_m9DCD6DC6C58A6A3E1D750A2BE7B910D9C4C013D8 (void);
// 0x000001A0 System.Void Mirror.NetworkLoop::NetworkEarlyUpdate()
extern void NetworkLoop_NetworkEarlyUpdate_m45238F04DFD59A6D6937BCE61DEA4EB5C512141F (void);
// 0x000001A1 System.Void Mirror.NetworkLoop::NetworkLateUpdate()
extern void NetworkLoop_NetworkLateUpdate_m5A9039F8703B7D81FC5CD97674E42F9EFE90007E (void);
// 0x000001A2 System.Void Mirror.NetworkLoop/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mB3B89587C70507BEEEA8D237FA662BA8A2833F61 (void);
// 0x000001A3 System.Boolean Mirror.NetworkLoop/<>c__DisplayClass4_0::<FindPlayerLoopEntryIndex>b__0(UnityEngine.LowLevel.PlayerLoopSystem)
extern void U3CU3Ec__DisplayClass4_0_U3CFindPlayerLoopEntryIndexU3Eb__0_m8BDAB8183595358E52F81332F37E34612B8B66B9 (void);
// 0x000001A4 Mirror.NetworkManager Mirror.NetworkManager::get_singleton()
extern void NetworkManager_get_singleton_m3687E70BBF51C41E6F20B606EF0E0E40D13E7641 (void);
// 0x000001A5 System.Void Mirror.NetworkManager::set_singleton(Mirror.NetworkManager)
extern void NetworkManager_set_singleton_m3E3F67766BFA7EFD8A3360377DEABEAAFCC271F9 (void);
// 0x000001A6 System.Int32 Mirror.NetworkManager::get_numPlayers()
extern void NetworkManager_get_numPlayers_mA4CC3D7D3A2C4967F21E4EC6554CADB2EB239816 (void);
// 0x000001A7 System.Boolean Mirror.NetworkManager::get_isNetworkActive()
extern void NetworkManager_get_isNetworkActive_m9FF273CFD5738832BFA8542B51BBC6F8A3C1A2EA (void);
// 0x000001A8 Mirror.NetworkManagerMode Mirror.NetworkManager::get_mode()
extern void NetworkManager_get_mode_m7DDDC4AFB7EC130F64E6BA4E916235B46C338337 (void);
// 0x000001A9 System.Void Mirror.NetworkManager::set_mode(Mirror.NetworkManagerMode)
extern void NetworkManager_set_mode_mFD99BDFC3978E7B505C6062BBB31BFDE7A3751FF (void);
// 0x000001AA System.Void Mirror.NetworkManager::OnValidate()
extern void NetworkManager_OnValidate_m1E5FDC67423166450D021672C314C8346B0CECA0 (void);
// 0x000001AB System.Void Mirror.NetworkManager::Reset()
extern void NetworkManager_Reset_m1BA9D62BA16187C79D148D5919186292D2C82E6F (void);
// 0x000001AC System.Void Mirror.NetworkManager::Awake()
extern void NetworkManager_Awake_mED76ED7E0D4767AD1F7ADB7E845C2679E84E6B8A (void);
// 0x000001AD System.Void Mirror.NetworkManager::Start()
extern void NetworkManager_Start_mD2069EBE9124584CAF85D88EEC28A841F386A4F0 (void);
// 0x000001AE System.Void Mirror.NetworkManager::LateUpdate()
extern void NetworkManager_LateUpdate_m5A729F6E28B3C3D49254D02A1A6BACFC9255A8F2 (void);
// 0x000001AF System.Boolean Mirror.NetworkManager::IsServerOnlineSceneChangeNeeded()
extern void NetworkManager_IsServerOnlineSceneChangeNeeded_m635D78E9B5D3883BC0DB0526DC8BE88F4C9FB9C4 (void);
// 0x000001B0 System.Boolean Mirror.NetworkManager::IsSceneActive(System.String)
extern void NetworkManager_IsSceneActive_mA50E21AD716E48D445A23383FFD249D21149F3E2 (void);
// 0x000001B1 System.Void Mirror.NetworkManager::SetupServer()
extern void NetworkManager_SetupServer_m9D6BCC8D75D3F91E34218B4DCFA6EE3E813CD2F0 (void);
// 0x000001B2 System.Void Mirror.NetworkManager::StartServer()
extern void NetworkManager_StartServer_m7F3D716905D518790D749225F100D0841051B017 (void);
// 0x000001B3 System.Void Mirror.NetworkManager::StartClient()
extern void NetworkManager_StartClient_m76D46DACCCD2C70BB78014AD376FD99D91EC36C3 (void);
// 0x000001B4 System.Void Mirror.NetworkManager::StartClient(System.Uri)
extern void NetworkManager_StartClient_mC4B472F3B09F109813AD8B7AC133D0475BBCECAE (void);
// 0x000001B5 System.Void Mirror.NetworkManager::StartHost()
extern void NetworkManager_StartHost_m075A9DF45CC23B3CE251028E7258583AB32B5710 (void);
// 0x000001B6 System.Void Mirror.NetworkManager::FinishStartHost()
extern void NetworkManager_FinishStartHost_mA7C04417B8D8E5FCF593CC8BDAD1B7A1316B0B6E (void);
// 0x000001B7 System.Void Mirror.NetworkManager::StartHostClient()
extern void NetworkManager_StartHostClient_m63E4184E021964794FB3F6BD67C3C15036203F4D (void);
// 0x000001B8 System.Void Mirror.NetworkManager::StopHost()
extern void NetworkManager_StopHost_m9BE1DF0275204D698472DCB8B1DBF62FC83994CF (void);
// 0x000001B9 System.Void Mirror.NetworkManager::StopServer()
extern void NetworkManager_StopServer_m796F0F4CA3CA42289EBD5F1F6CA6975C9993D988 (void);
// 0x000001BA System.Void Mirror.NetworkManager::StopClient()
extern void NetworkManager_StopClient_m749293B1F5FA88A3CBD079B8A5A45B6400F3FE47 (void);
// 0x000001BB System.Void Mirror.NetworkManager::OnApplicationQuit()
extern void NetworkManager_OnApplicationQuit_m720A6D0FE0EC77E009A55942C2FB006CE9253DA0 (void);
// 0x000001BC System.Void Mirror.NetworkManager::ConfigureHeadlessFrameRate()
extern void NetworkManager_ConfigureHeadlessFrameRate_mE2F10667D1299D0761FAF1F0E6476BBD3EA9085F (void);
// 0x000001BD System.Boolean Mirror.NetworkManager::InitializeSingleton()
extern void NetworkManager_InitializeSingleton_m2C2E16FAF3FC4D0F4246B344C83E2F864DB21F71 (void);
// 0x000001BE System.Void Mirror.NetworkManager::RegisterServerMessages()
extern void NetworkManager_RegisterServerMessages_m62B2385AE088F6F8C8708221989D9E8601FB025C (void);
// 0x000001BF System.Void Mirror.NetworkManager::RegisterClientMessages()
extern void NetworkManager_RegisterClientMessages_m59E1AB1EC3667065785D052268B8DC5115B18091 (void);
// 0x000001C0 System.Void Mirror.NetworkManager::ResetStatics()
extern void NetworkManager_ResetStatics_m601A79C8C2920AEFAC7525351CEA601BB2CFECB9 (void);
// 0x000001C1 System.Void Mirror.NetworkManager::OnDestroy()
extern void NetworkManager_OnDestroy_m4BA90829754CAA8DBC58D177E18D6AE8E4F0842D (void);
// 0x000001C2 System.String Mirror.NetworkManager::get_networkSceneName()
extern void NetworkManager_get_networkSceneName_m80DB5F4473BF7D3CCD9F5A5F3E0F2DDCEA7D76BD (void);
// 0x000001C3 System.Void Mirror.NetworkManager::set_networkSceneName(System.String)
extern void NetworkManager_set_networkSceneName_mBEC371D12F3669C066AC523EB7FD6A9885BB3685 (void);
// 0x000001C4 System.Void Mirror.NetworkManager::ServerChangeScene(System.String)
extern void NetworkManager_ServerChangeScene_mE7B47AD1D7BA092859F28E4500046C8B6BF52D8B (void);
// 0x000001C5 System.Void Mirror.NetworkManager::ClientChangeScene(System.String,Mirror.SceneOperation,System.Boolean)
extern void NetworkManager_ClientChangeScene_m6F6D640C2E6D989B53DA115B766555057D279B23 (void);
// 0x000001C6 System.Void Mirror.NetworkManager::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void NetworkManager_OnSceneLoaded_mB69266EA76D21E1DA2CC2364C9EA4D4D1D2971F2 (void);
// 0x000001C7 System.Void Mirror.NetworkManager::UpdateScene()
extern void NetworkManager_UpdateScene_m0DC87E5D4A8881A2363E79F42C71C14C0B0E31E0 (void);
// 0x000001C8 System.Void Mirror.NetworkManager::FinishLoadScene()
extern void NetworkManager_FinishLoadScene_m2EF8C54A639E7BE29E9FC587B3CE651C96602AF4 (void);
// 0x000001C9 System.Void Mirror.NetworkManager::FinishLoadSceneHost()
extern void NetworkManager_FinishLoadSceneHost_m055894F1A6AA4D805395C8302D7861744A9FC931 (void);
// 0x000001CA System.Void Mirror.NetworkManager::FinishLoadSceneServerOnly()
extern void NetworkManager_FinishLoadSceneServerOnly_mBF8ABAF755EE1A2887DBF1B9E0D8946BEF6B6EB9 (void);
// 0x000001CB System.Void Mirror.NetworkManager::FinishLoadSceneClientOnly()
extern void NetworkManager_FinishLoadSceneClientOnly_mDC883960B695D08D84046B1584F4D1766D7FEEF2 (void);
// 0x000001CC System.Void Mirror.NetworkManager::RegisterStartPosition(UnityEngine.Transform)
extern void NetworkManager_RegisterStartPosition_mDE79A6E850F2627C193B28E33307C73C120B7C8E (void);
// 0x000001CD System.Void Mirror.NetworkManager::UnRegisterStartPosition(UnityEngine.Transform)
extern void NetworkManager_UnRegisterStartPosition_mC51E52EFA2E8B98D0077FA9D654C7548F12CED28 (void);
// 0x000001CE UnityEngine.Transform Mirror.NetworkManager::GetStartPosition()
extern void NetworkManager_GetStartPosition_m25078697FA64905AF7618A26E75ED10750B1A754 (void);
// 0x000001CF System.Void Mirror.NetworkManager::OnServerConnectInternal(Mirror.NetworkConnectionToClient)
extern void NetworkManager_OnServerConnectInternal_m4F66944807317BF72ECEF1DFA28B49626FA25B94 (void);
// 0x000001D0 System.Void Mirror.NetworkManager::OnServerAuthenticated(Mirror.NetworkConnectionToClient)
extern void NetworkManager_OnServerAuthenticated_m322AA42F54B7853A9903412830AAB76ABED91853 (void);
// 0x000001D1 System.Void Mirror.NetworkManager::OnServerReadyMessageInternal(Mirror.NetworkConnectionToClient,Mirror.ReadyMessage)
extern void NetworkManager_OnServerReadyMessageInternal_m084C79624BCE4A9D7164F5E5739641C82F4768FC (void);
// 0x000001D2 System.Void Mirror.NetworkManager::OnServerAddPlayerInternal(Mirror.NetworkConnectionToClient,Mirror.AddPlayerMessage)
extern void NetworkManager_OnServerAddPlayerInternal_m3939653708D4869B12BDECE790525CF0895C7F0F (void);
// 0x000001D3 System.Void Mirror.NetworkManager::OnClientConnectInternal()
extern void NetworkManager_OnClientConnectInternal_m3598A44D5ED77226B51464DE553F99537ED7085A (void);
// 0x000001D4 System.Void Mirror.NetworkManager::OnClientAuthenticated()
extern void NetworkManager_OnClientAuthenticated_m2EC1BD4F0C6188208EBBF3F5D333E87D191785B2 (void);
// 0x000001D5 System.Void Mirror.NetworkManager::OnClientDisconnectInternal()
extern void NetworkManager_OnClientDisconnectInternal_mBABA2400A07727928C4DAFD31AD0F6C518CC516D (void);
// 0x000001D6 System.Void Mirror.NetworkManager::OnClientNotReadyMessageInternal(Mirror.NotReadyMessage)
extern void NetworkManager_OnClientNotReadyMessageInternal_m59761D864BB5B6E5AB9E8773F5B3084B7BF3C097 (void);
// 0x000001D7 System.Void Mirror.NetworkManager::OnClientSceneInternal(Mirror.SceneMessage)
extern void NetworkManager_OnClientSceneInternal_m577FA355F23D1BBA446A69CD950CF45FCAD34019 (void);
// 0x000001D8 System.Void Mirror.NetworkManager::OnServerConnect(Mirror.NetworkConnectionToClient)
extern void NetworkManager_OnServerConnect_m4FE95629249187EC364DFC99157CC31896BB9032 (void);
// 0x000001D9 System.Void Mirror.NetworkManager::OnServerDisconnect(Mirror.NetworkConnectionToClient)
extern void NetworkManager_OnServerDisconnect_m03AAB57C35A00C437AEF6619441C3E6F90DFF082 (void);
// 0x000001DA System.Void Mirror.NetworkManager::OnServerReady(Mirror.NetworkConnectionToClient)
extern void NetworkManager_OnServerReady_mB1C4FFB5CAE5D60A7BCA30EDB3CC5A16215EA62E (void);
// 0x000001DB System.Void Mirror.NetworkManager::OnServerAddPlayer(Mirror.NetworkConnectionToClient)
extern void NetworkManager_OnServerAddPlayer_m5A0967A4E13E54B8D76F19E124373769E66DA557 (void);
// 0x000001DC System.Void Mirror.NetworkManager::OnServerError(Mirror.NetworkConnectionToClient,System.Exception)
extern void NetworkManager_OnServerError_m3E6942651A79EB4C71B47BC3BCA7AD192F88160F (void);
// 0x000001DD System.Void Mirror.NetworkManager::OnServerChangeScene(System.String)
extern void NetworkManager_OnServerChangeScene_m78A746FB47504CEFACC6AF32A27B7C22C21ECC0A (void);
// 0x000001DE System.Void Mirror.NetworkManager::OnServerSceneChanged(System.String)
extern void NetworkManager_OnServerSceneChanged_m3A6AD198811FBCA588A5B4429A003180AEE8A08B (void);
// 0x000001DF System.Void Mirror.NetworkManager::OnClientConnect()
extern void NetworkManager_OnClientConnect_m9254EB4CD1F095FC7A3712C59F268770E426EF1E (void);
// 0x000001E0 System.Void Mirror.NetworkManager::OnClientConnect(Mirror.NetworkConnection)
extern void NetworkManager_OnClientConnect_m23A364AC2D7C3AEE64B43C25453A7BC2378CCC1A (void);
// 0x000001E1 System.Void Mirror.NetworkManager::OnClientDisconnect()
extern void NetworkManager_OnClientDisconnect_m3990DBEB7C16E8CE24F4F46490AB52EE9B7BE77D (void);
// 0x000001E2 System.Void Mirror.NetworkManager::OnClientDisconnect(Mirror.NetworkConnection)
extern void NetworkManager_OnClientDisconnect_m02FA88A6123126271FF5EB97903A4149A0D5D69B (void);
// 0x000001E3 System.Void Mirror.NetworkManager::OnClientError(System.Exception)
extern void NetworkManager_OnClientError_m3E98D9E3FEAC96795CA4F44CC614694C176F4AAC (void);
// 0x000001E4 System.Void Mirror.NetworkManager::OnClientNotReady()
extern void NetworkManager_OnClientNotReady_mA2B3332324E4BFCBD3D1F75C7C41AAFC128B72A7 (void);
// 0x000001E5 System.Void Mirror.NetworkManager::OnClientNotReady(Mirror.NetworkConnection)
extern void NetworkManager_OnClientNotReady_m1378C1AD4DCB54A286F0A35EFC73287C8A2A2C7F (void);
// 0x000001E6 System.Void Mirror.NetworkManager::OnClientChangeScene(System.String,Mirror.SceneOperation,System.Boolean)
extern void NetworkManager_OnClientChangeScene_mA38EC559CA0D4CFA58ACD65B6E0568683748F411 (void);
// 0x000001E7 System.Void Mirror.NetworkManager::OnClientSceneChanged()
extern void NetworkManager_OnClientSceneChanged_m5A252B98B09BB83BA45EEE697CBD3C6C5A54A152 (void);
// 0x000001E8 System.Void Mirror.NetworkManager::OnClientSceneChanged(Mirror.NetworkConnection)
extern void NetworkManager_OnClientSceneChanged_mE627B70A20ABD429D2D8F8C3D0E32F70323442AE (void);
// 0x000001E9 System.Void Mirror.NetworkManager::OnStartHost()
extern void NetworkManager_OnStartHost_m377F4A7C84B04AB4F90A786995899EA7EE38B3CE (void);
// 0x000001EA System.Void Mirror.NetworkManager::OnStartServer()
extern void NetworkManager_OnStartServer_m05E1F97124AFA01D00BC671476F834089724C97A (void);
// 0x000001EB System.Void Mirror.NetworkManager::OnStartClient()
extern void NetworkManager_OnStartClient_m6C90B4D7405831A55142A14BB05BD89154116C24 (void);
// 0x000001EC System.Void Mirror.NetworkManager::OnStopServer()
extern void NetworkManager_OnStopServer_mBE7B67DF4F17E2302A423B6BAAF6C3A137DCA77D (void);
// 0x000001ED System.Void Mirror.NetworkManager::OnStopClient()
extern void NetworkManager_OnStopClient_m84A76651AF99B829787A74F417B32D44688681BD (void);
// 0x000001EE System.Void Mirror.NetworkManager::OnStopHost()
extern void NetworkManager_OnStopHost_mF7DF15C4F927F247E625E3F9B97962190AF08F0F (void);
// 0x000001EF System.Void Mirror.NetworkManager::.ctor()
extern void NetworkManager__ctor_m831DF4BB5F616C10CC2B272AD9DA2C660B3A1924 (void);
// 0x000001F0 System.Void Mirror.NetworkManager::.cctor()
extern void NetworkManager__cctor_mB48BA1AD4B577F594D044976E4F190A877F0198F (void);
// 0x000001F1 System.Void Mirror.NetworkManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m37D13BDF4CBED4CBCBB1249A2281187B67BA788E (void);
// 0x000001F2 System.Void Mirror.NetworkManager/<>c::.ctor()
extern void U3CU3Ec__ctor_m450CE0D75EBA856B0B9F8B7F888C07156E680F20 (void);
// 0x000001F3 System.Boolean Mirror.NetworkManager/<>c::<get_numPlayers>b__21_0(System.Collections.Generic.KeyValuePair`2<System.Int32,Mirror.NetworkConnectionToClient>)
extern void U3CU3Ec_U3Cget_numPlayersU3Eb__21_0_m55CB9833DC205969711E6F01706B4D0B67BA0800 (void);
// 0x000001F4 System.Boolean Mirror.NetworkManager/<>c::<RegisterClientMessages>b__52_0(UnityEngine.GameObject)
extern void U3CU3Ec_U3CRegisterClientMessagesU3Eb__52_0_mEB1B22CBE8BAF3792D502817A90E67A8E23ED0EF (void);
// 0x000001F5 System.Int32 Mirror.NetworkManager/<>c::<RegisterStartPosition>b__69_0(UnityEngine.Transform)
extern void U3CU3Ec_U3CRegisterStartPositionU3Eb__69_0_m6BD47C3FB43CDA3ED895BAB78A77008B68741335 (void);
// 0x000001F6 System.Boolean Mirror.NetworkManager/<>c::<GetStartPosition>b__71_0(UnityEngine.Transform)
extern void U3CU3Ec_U3CGetStartPositionU3Eb__71_0_mEEC6E272C3FECCB3466B5E9081A7A4DB358A0D1E (void);
// 0x000001F7 System.Void Mirror.NetworkManagerHUD::Awake()
extern void NetworkManagerHUD_Awake_m4EA9A71E137E6FB2CF2F69CD1355B8FFAEB2C91A (void);
// 0x000001F8 System.Void Mirror.NetworkManagerHUD::OnGUI()
extern void NetworkManagerHUD_OnGUI_mF1BD1A2A022586475B91CC9491377150601855E4 (void);
// 0x000001F9 System.Void Mirror.NetworkManagerHUD::StartButtons()
extern void NetworkManagerHUD_StartButtons_m77E114195F788922578CAEAD16F04C8529070859 (void);
// 0x000001FA System.Void Mirror.NetworkManagerHUD::StatusLabels()
extern void NetworkManagerHUD_StatusLabels_m5BC7A80BFA34A4CDEB4F8DFF207766A36CFD6B56 (void);
// 0x000001FB System.Void Mirror.NetworkManagerHUD::StopButtons()
extern void NetworkManagerHUD_StopButtons_mCDF2E1FC08A56AF9904AFD9CCC18172E7EFEFF50 (void);
// 0x000001FC System.Void Mirror.NetworkManagerHUD::.ctor()
extern void NetworkManagerHUD__ctor_mB218F6DAB2DD70F06B61CE59E7A6B40160BE5146 (void);
// 0x000001FD System.Int32 Mirror.NetworkReader::get_Length()
extern void NetworkReader_get_Length_mD4443E7D7B3E49740A8D400B1CA1FC6D13D26D4B (void);
// 0x000001FE System.Int32 Mirror.NetworkReader::get_Remaining()
extern void NetworkReader_get_Remaining_mF954AAEEA5A5137C0A5C71CC0A239E00242D5F0D (void);
// 0x000001FF System.Void Mirror.NetworkReader::.ctor(System.Byte[])
extern void NetworkReader__ctor_m7472356275E51A91FEE8FEF118A6AF2240F6417D (void);
// 0x00000200 System.Void Mirror.NetworkReader::.ctor(System.ArraySegment`1<System.Byte>)
extern void NetworkReader__ctor_m73E59DA6A9DC39831B7EAC72DD9F9B29BA0918C0 (void);
// 0x00000201 System.Void Mirror.NetworkReader::SetBuffer(System.Byte[])
extern void NetworkReader_SetBuffer_m637B9FB7E70FFAA1E7D1CBD92B3C162D2B647775 (void);
// 0x00000202 System.Void Mirror.NetworkReader::SetBuffer(System.ArraySegment`1<System.Byte>)
extern void NetworkReader_SetBuffer_m0A5DA4D026405BE2057842706FC25EC994750700 (void);
// 0x00000203 T Mirror.NetworkReader::ReadBlittable()
// 0x00000204 System.Nullable`1<T> Mirror.NetworkReader::ReadBlittableNullable()
// 0x00000205 System.Byte Mirror.NetworkReader::ReadByte()
extern void NetworkReader_ReadByte_m5CEA57059C34AA6DC218D6F09207C5188AEA68A8 (void);
// 0x00000206 System.Byte[] Mirror.NetworkReader::ReadBytes(System.Byte[],System.Int32)
extern void NetworkReader_ReadBytes_m8A6348412F267570291A4BD93F4DF1715B35077D (void);
// 0x00000207 System.ArraySegment`1<System.Byte> Mirror.NetworkReader::ReadBytesSegment(System.Int32)
extern void NetworkReader_ReadBytesSegment_mD99D152684B83B9AF600E28FB6642FAB3B4B4CCF (void);
// 0x00000208 System.String Mirror.NetworkReader::ToString()
extern void NetworkReader_ToString_mFEA276025738C2F5F0D5BEE70952B19D278380A0 (void);
// 0x00000209 T Mirror.NetworkReader::Read()
// 0x0000020A System.Byte Mirror.NetworkReaderExtensions::ReadByte(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadByte_m86E3D12D2C1E2C447A4C2F20B808117DAA69571C (void);
// 0x0000020B System.Nullable`1<System.Byte> Mirror.NetworkReaderExtensions::ReadByteNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadByteNullable_m46B384BB9A670691AD0B2D53ED2BCFBA00652997 (void);
// 0x0000020C System.SByte Mirror.NetworkReaderExtensions::ReadSByte(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadSByte_m8D3F2D88094079D5E0495FBBB3815DBAECB71CC1 (void);
// 0x0000020D System.Nullable`1<System.SByte> Mirror.NetworkReaderExtensions::ReadSByteNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadSByteNullable_mE4F005C9E051CA4913FF491D48EE45C270694A0E (void);
// 0x0000020E System.Char Mirror.NetworkReaderExtensions::ReadChar(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadChar_m1CD7BF565346230DFE2B29A62818C0DF4ED89AFD (void);
// 0x0000020F System.Nullable`1<System.Char> Mirror.NetworkReaderExtensions::ReadCharNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadCharNullable_mBA02C8B069942BDBDFD8278379C001746572A840 (void);
// 0x00000210 System.Boolean Mirror.NetworkReaderExtensions::ReadBool(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadBool_mB302D18DCEDDEDB917F1417AA91C6C3D0FF05FA7 (void);
// 0x00000211 System.Nullable`1<System.Boolean> Mirror.NetworkReaderExtensions::ReadBoolNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadBoolNullable_m0E59A2846A3630932D55CF5257CBA500340252F1 (void);
// 0x00000212 System.Int16 Mirror.NetworkReaderExtensions::ReadShort(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadShort_m2F10D1E755DA0CE0B6DC9616874769ED37236613 (void);
// 0x00000213 System.Nullable`1<System.Int16> Mirror.NetworkReaderExtensions::ReadShortNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadShortNullable_mB3647FD80F5864268B3063BF1DE391FECDB33DA0 (void);
// 0x00000214 System.UInt16 Mirror.NetworkReaderExtensions::ReadUShort(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadUShort_m8D2B92163641A77AC2996A4AB42C3B726E67EF0F (void);
// 0x00000215 System.Nullable`1<System.UInt16> Mirror.NetworkReaderExtensions::ReadUShortNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadUShortNullable_mD6DA147C35E1113279521FFC0599F2BB4FEAEC12 (void);
// 0x00000216 System.Int32 Mirror.NetworkReaderExtensions::ReadInt(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadInt_m7A9A4138EDF745302020D1404A8A73C5BA0F0597 (void);
// 0x00000217 System.Nullable`1<System.Int32> Mirror.NetworkReaderExtensions::ReadIntNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadIntNullable_m232030B7EC07DB0F07809D4D6C4505FB3BE175FD (void);
// 0x00000218 System.UInt32 Mirror.NetworkReaderExtensions::ReadUInt(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadUInt_mFA3BF8869BCF94137E3F40CFF79B3CE4B93C3D90 (void);
// 0x00000219 System.Nullable`1<System.UInt32> Mirror.NetworkReaderExtensions::ReadUIntNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadUIntNullable_mC372F7D0272500AC161F80934B107216C0ED1889 (void);
// 0x0000021A System.Int64 Mirror.NetworkReaderExtensions::ReadLong(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadLong_m5ADF67BD501F629D2B2779848BF9B4CFB6BC4DCD (void);
// 0x0000021B System.Nullable`1<System.Int64> Mirror.NetworkReaderExtensions::ReadLongNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadLongNullable_mE7D7D972269097D6D2BB0E902900E15C513E8DE9 (void);
// 0x0000021C System.UInt64 Mirror.NetworkReaderExtensions::ReadULong(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadULong_m01F59C890722632901DC6C16D461CF39E5EEFDAC (void);
// 0x0000021D System.Nullable`1<System.UInt64> Mirror.NetworkReaderExtensions::ReadULongNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadULongNullable_m0DA6A493DC13F36AEAF356579B2D4848A719DA43 (void);
// 0x0000021E System.Single Mirror.NetworkReaderExtensions::ReadFloat(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadFloat_m4DFFE1E95D7BEF889745B16F7963B32FD839FA64 (void);
// 0x0000021F System.Nullable`1<System.Single> Mirror.NetworkReaderExtensions::ReadFloatNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadFloatNullable_m5EADF2936FFE0963063AA5CA3617ACF285CB5997 (void);
// 0x00000220 System.Double Mirror.NetworkReaderExtensions::ReadDouble(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadDouble_m5800AF1FD52D7DB9047FE7B58A0BD38E23C94281 (void);
// 0x00000221 System.Nullable`1<System.Double> Mirror.NetworkReaderExtensions::ReadDoubleNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadDoubleNullable_m8EECE714A0396E02FF250AAA2024DBB28D5C42AB (void);
// 0x00000222 System.Decimal Mirror.NetworkReaderExtensions::ReadDecimal(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadDecimal_m0AE7D5057B858DB3A6C838CF3DF716707F3FA1EC (void);
// 0x00000223 System.Nullable`1<System.Decimal> Mirror.NetworkReaderExtensions::ReadDecimalNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadDecimalNullable_mF95C4F7A1E4D229852BC05966CA011B7681A0D24 (void);
// 0x00000224 System.String Mirror.NetworkReaderExtensions::ReadString(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadString_mA74E4612F529683A4AEEDB55C369CDEA32D222CC (void);
// 0x00000225 System.Byte[] Mirror.NetworkReaderExtensions::ReadBytesAndSize(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadBytesAndSize_m2EED0B100EFEDF9E1A4D2E9A429DBF3D4F8EA623 (void);
// 0x00000226 System.Byte[] Mirror.NetworkReaderExtensions::ReadBytes(Mirror.NetworkReader,System.Int32)
extern void NetworkReaderExtensions_ReadBytes_m256AB25CABB792B276088B8929BBD2BC972719F0 (void);
// 0x00000227 System.ArraySegment`1<System.Byte> Mirror.NetworkReaderExtensions::ReadBytesAndSizeSegment(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadBytesAndSizeSegment_m1750D52A666869DA2EC3C3A25E767834B9DA9E2E (void);
// 0x00000228 UnityEngine.Vector2 Mirror.NetworkReaderExtensions::ReadVector2(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector2_mADCA4D375C619EE39C7ED83BC352C870897FEF6F (void);
// 0x00000229 System.Nullable`1<UnityEngine.Vector2> Mirror.NetworkReaderExtensions::ReadVector2Nullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector2Nullable_mF103F71AF6F6C8F2A65A46EFB2C00BB34FFFB3DB (void);
// 0x0000022A UnityEngine.Vector3 Mirror.NetworkReaderExtensions::ReadVector3(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector3_mD9D5C83CB3177994353C71C8BC71495A79288416 (void);
// 0x0000022B System.Nullable`1<UnityEngine.Vector3> Mirror.NetworkReaderExtensions::ReadVector3Nullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector3Nullable_mDEA1185C82A9735529800863A1AECB50FD21801A (void);
// 0x0000022C UnityEngine.Vector4 Mirror.NetworkReaderExtensions::ReadVector4(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector4_m26621D658FD76A6A4D8EE73CE81739620A706997 (void);
// 0x0000022D System.Nullable`1<UnityEngine.Vector4> Mirror.NetworkReaderExtensions::ReadVector4Nullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector4Nullable_m4EE393E4CFF0EEF34831707DFD4372AFA61CE392 (void);
// 0x0000022E UnityEngine.Vector2Int Mirror.NetworkReaderExtensions::ReadVector2Int(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector2Int_m0B0397F7BA2F4DE1E1A555E9EFEE676C38ECE40C (void);
// 0x0000022F System.Nullable`1<UnityEngine.Vector2Int> Mirror.NetworkReaderExtensions::ReadVector2IntNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector2IntNullable_m3779FA8E7B35AAC67C7B02128E4DE4DBB8727AF3 (void);
// 0x00000230 UnityEngine.Vector3Int Mirror.NetworkReaderExtensions::ReadVector3Int(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector3Int_mD9A574478D0EC900DB9C50B000AE39148A429F68 (void);
// 0x00000231 System.Nullable`1<UnityEngine.Vector3Int> Mirror.NetworkReaderExtensions::ReadVector3IntNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector3IntNullable_mCC634E519B105A61E6A6D174518C022C2774976C (void);
// 0x00000232 UnityEngine.Color Mirror.NetworkReaderExtensions::ReadColor(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadColor_m21E0B16FA466A4FDAEC8BE1C80E47C9BAE3217A7 (void);
// 0x00000233 System.Nullable`1<UnityEngine.Color> Mirror.NetworkReaderExtensions::ReadColorNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadColorNullable_mF0F6F745B6BEFF123450CA83C75492C0EE01FD88 (void);
// 0x00000234 UnityEngine.Color32 Mirror.NetworkReaderExtensions::ReadColor32(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadColor32_m7926AFB64529E5FC7C51D390468AAB86722D01F7 (void);
// 0x00000235 System.Nullable`1<UnityEngine.Color32> Mirror.NetworkReaderExtensions::ReadColor32Nullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadColor32Nullable_mC5FC03E7B558C4DFA8FC0C0488A1754B2FF976D4 (void);
// 0x00000236 UnityEngine.Quaternion Mirror.NetworkReaderExtensions::ReadQuaternion(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadQuaternion_m2946D7DD55E1ED8EDEF82536F0D4246D42A22E19 (void);
// 0x00000237 System.Nullable`1<UnityEngine.Quaternion> Mirror.NetworkReaderExtensions::ReadQuaternionNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadQuaternionNullable_mFC1F82DAEE714675362CE50ACF588A125A274509 (void);
// 0x00000238 UnityEngine.Rect Mirror.NetworkReaderExtensions::ReadRect(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadRect_m75BCEF76E5C1CFEBE794A2FE5F52AA55DFBF3F85 (void);
// 0x00000239 System.Nullable`1<UnityEngine.Rect> Mirror.NetworkReaderExtensions::ReadRectNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadRectNullable_m2DC5EA005A10F7D2ED892AB04592292327EDF064 (void);
// 0x0000023A UnityEngine.Plane Mirror.NetworkReaderExtensions::ReadPlane(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadPlane_m5D8E59014B3ADD6108143CBB2C25DAA1DDD291C5 (void);
// 0x0000023B System.Nullable`1<UnityEngine.Plane> Mirror.NetworkReaderExtensions::ReadPlaneNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadPlaneNullable_m2183EBA24F2D938E841A82638C814DA61AC8A2FE (void);
// 0x0000023C UnityEngine.Ray Mirror.NetworkReaderExtensions::ReadRay(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadRay_mD2A5A24032CB890240D7F5D94E8399E2766C05EA (void);
// 0x0000023D System.Nullable`1<UnityEngine.Ray> Mirror.NetworkReaderExtensions::ReadRayNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadRayNullable_m83A2D617CECCC13DF92443BEC58F863EE59BEE9C (void);
// 0x0000023E UnityEngine.Matrix4x4 Mirror.NetworkReaderExtensions::ReadMatrix4x4(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadMatrix4x4_m8C9D3E72F015488DA355F6E26FDCF6E0C52FC7D8 (void);
// 0x0000023F System.Nullable`1<UnityEngine.Matrix4x4> Mirror.NetworkReaderExtensions::ReadMatrix4x4Nullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadMatrix4x4Nullable_m5905D3251EECDC7F61B23772F105298B1AA0B6F5 (void);
// 0x00000240 System.Guid Mirror.NetworkReaderExtensions::ReadGuid(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadGuid_m4BEDE3B6CE956D9B64BFF779754151F49B7A82CE (void);
// 0x00000241 System.Nullable`1<System.Guid> Mirror.NetworkReaderExtensions::ReadGuidNullable(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadGuidNullable_mA45DC71F13BA89949EDDB1948554D3CED60EC111 (void);
// 0x00000242 Mirror.NetworkIdentity Mirror.NetworkReaderExtensions::ReadNetworkIdentity(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadNetworkIdentity_m52CEFB599F366ADD367C0C23CFA163EBB0F6C764 (void);
// 0x00000243 Mirror.NetworkBehaviour Mirror.NetworkReaderExtensions::ReadNetworkBehaviour(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadNetworkBehaviour_mAEDC6923777D5F941348F24C73586BB93353E492 (void);
// 0x00000244 T Mirror.NetworkReaderExtensions::ReadNetworkBehaviour(Mirror.NetworkReader)
// 0x00000245 Mirror.NetworkBehaviour/NetworkBehaviourSyncVar Mirror.NetworkReaderExtensions::ReadNetworkBehaviourSyncVar(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadNetworkBehaviourSyncVar_mC735015AC984ED517FB5EC9469C0A70181E6838F (void);
// 0x00000246 UnityEngine.Transform Mirror.NetworkReaderExtensions::ReadTransform(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadTransform_m81B5B4E6C64D5C011B52262BDCDB1F34E0B50034 (void);
// 0x00000247 UnityEngine.GameObject Mirror.NetworkReaderExtensions::ReadGameObject(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadGameObject_m50C52A705CA8F13E6B80746CB3F001CD82D42404 (void);
// 0x00000248 System.Collections.Generic.List`1<T> Mirror.NetworkReaderExtensions::ReadList(Mirror.NetworkReader)
// 0x00000249 T[] Mirror.NetworkReaderExtensions::ReadArray(Mirror.NetworkReader)
// 0x0000024A System.Uri Mirror.NetworkReaderExtensions::ReadUri(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadUri_m4D5B6761CC4A07C3E7F223B2DDF88EF4CACC3A3B (void);
// 0x0000024B UnityEngine.Texture2D Mirror.NetworkReaderExtensions::ReadTexture2D(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadTexture2D_mE2072E327B0B6F70025162BD7273DA4CE93C6D2D (void);
// 0x0000024C UnityEngine.Sprite Mirror.NetworkReaderExtensions::ReadSprite(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadSprite_m1F27B4B0A5D5D62A72E452BCC8A193BE0D372DB0 (void);
// 0x0000024D System.Void Mirror.NetworkReaderExtensions::.cctor()
extern void NetworkReaderExtensions__cctor_m40E45B62701A8A5F9F8DAA0544C08D5A4FF20A29 (void);
// 0x0000024E System.Void Mirror.PooledNetworkReader::.ctor(System.Byte[])
extern void PooledNetworkReader__ctor_mEAE1DCDFBAC4CCFA4D40970DE4D20B36931D656E (void);
// 0x0000024F System.Void Mirror.PooledNetworkReader::.ctor(System.ArraySegment`1<System.Byte>)
extern void PooledNetworkReader__ctor_m576FB263D8FEA8501D96F220390DC912FEB24762 (void);
// 0x00000250 System.Void Mirror.PooledNetworkReader::Dispose()
extern void PooledNetworkReader_Dispose_mC35EBC46EDE36DB1EBCF9AFDDCE4375618D32A15 (void);
// 0x00000251 Mirror.PooledNetworkReader Mirror.NetworkReaderPool::GetReader(System.Byte[])
extern void NetworkReaderPool_GetReader_m951A0F2DA2D9D9B6DB00A2F9540BEC414D2DE21C (void);
// 0x00000252 Mirror.PooledNetworkReader Mirror.NetworkReaderPool::GetReader(System.ArraySegment`1<System.Byte>)
extern void NetworkReaderPool_GetReader_mE32ACECE58AE2B54D448549DCA0D920D84DAF79E (void);
// 0x00000253 System.Void Mirror.NetworkReaderPool::Recycle(Mirror.PooledNetworkReader)
extern void NetworkReaderPool_Recycle_mAD1BB938E53E18CF168736FB5256680DBBFD1535 (void);
// 0x00000254 System.Void Mirror.NetworkReaderPool::.cctor()
extern void NetworkReaderPool__cctor_m7818A15817331E4274A35067A7FC010DD07871F6 (void);
// 0x00000255 System.Void Mirror.NetworkReaderPool/<>c::.cctor()
extern void U3CU3Ec__cctor_m312E11FBBE279F10D9D2B17EFB882AA1B0920D12 (void);
// 0x00000256 System.Void Mirror.NetworkReaderPool/<>c::.ctor()
extern void U3CU3Ec__ctor_mDC5278341D19B9BC6F238AF069FB094885AB02D5 (void);
// 0x00000257 Mirror.PooledNetworkReader Mirror.NetworkReaderPool/<>c::<.cctor>b__4_0()
extern void U3CU3Ec_U3C_cctorU3Eb__4_0_m7B2E2E667B272E9C7BD040EDDA22D56ABB94241D (void);
// 0x00000258 Mirror.NetworkConnectionToClient Mirror.NetworkServer::get_localConnection()
extern void NetworkServer_get_localConnection_m5CC8941C493528A76850E9CA0EE95BFAE18C2B7C (void);
// 0x00000259 System.Void Mirror.NetworkServer::set_localConnection(Mirror.NetworkConnectionToClient)
extern void NetworkServer_set_localConnection_m121D988179415A2FEA40ADA6C77986A764A56CB8 (void);
// 0x0000025A System.Boolean Mirror.NetworkServer::get_localClientActive()
extern void NetworkServer_get_localClientActive_m9C4F4B8848458BB4A2EE6552EDE1E39792C2B3BA (void);
// 0x0000025B System.Boolean Mirror.NetworkServer::get_active()
extern void NetworkServer_get_active_mF055B10F741C963266FE30D5667E781DBF44DEEE (void);
// 0x0000025C System.Void Mirror.NetworkServer::set_active(System.Boolean)
extern void NetworkServer_set_active_m964AD097B146622B9B4439D01DE79A734F658CD6 (void);
// 0x0000025D System.Void Mirror.NetworkServer::Initialize()
extern void NetworkServer_Initialize_m3ACC3E103BE21A4A4911B20175B6D05B11A95525 (void);
// 0x0000025E System.Void Mirror.NetworkServer::AddTransportHandlers()
extern void NetworkServer_AddTransportHandlers_m978B1B0770E285B67A248262C957CD4E1605984A (void);
// 0x0000025F System.Void Mirror.NetworkServer::ActivateHostScene()
extern void NetworkServer_ActivateHostScene_m457CCCEFB3E319567D22F5AFC7209B6A153DE976 (void);
// 0x00000260 System.Void Mirror.NetworkServer::RegisterMessageHandlers()
extern void NetworkServer_RegisterMessageHandlers_mA9D72F9E0CB28A698139D5BEBC3D4764627802B9 (void);
// 0x00000261 System.Void Mirror.NetworkServer::Listen(System.Int32)
extern void NetworkServer_Listen_m0A131ED708A5EE255FC8A2B79C35017D78C9BF18 (void);
// 0x00000262 System.Void Mirror.NetworkServer::CleanupSpawned()
extern void NetworkServer_CleanupSpawned_m8234F6BBF27B185E179E2C23F57C80DA752F1513 (void);
// 0x00000263 System.Void Mirror.NetworkServer::Shutdown()
extern void NetworkServer_Shutdown_mBEA3CA658B95006C84A0A81D5D1D3E47EF564EAF (void);
// 0x00000264 System.Boolean Mirror.NetworkServer::AddConnection(Mirror.NetworkConnectionToClient)
extern void NetworkServer_AddConnection_m765F75E8AF029F8D9D9E9A115663C37AA7C6C9CF (void);
// 0x00000265 System.Boolean Mirror.NetworkServer::RemoveConnection(System.Int32)
extern void NetworkServer_RemoveConnection_m1492C8648F00DB2D4C3F3DC9ABC690BF27C9299B (void);
// 0x00000266 System.Void Mirror.NetworkServer::SetLocalConnection(Mirror.LocalConnectionToClient)
extern void NetworkServer_SetLocalConnection_mEDC58E853ADC35700140B17F47106B5CCF7568AF (void);
// 0x00000267 System.Void Mirror.NetworkServer::RemoveLocalConnection()
extern void NetworkServer_RemoveLocalConnection_m378F55B5F91FCC504A7100491C3832C006EED402 (void);
// 0x00000268 System.Boolean Mirror.NetworkServer::NoExternalConnections()
extern void NetworkServer_NoExternalConnections_mFA58D0B4E2C7D1D227BBCE0815863D71B3AAF785 (void);
// 0x00000269 System.Boolean Mirror.NetworkServer::HasExternalConnections()
extern void NetworkServer_HasExternalConnections_m584C09C6807F4388BE584212A555A8C80EA94D6A (void);
// 0x0000026A System.Void Mirror.NetworkServer::SendToAll(T,System.Int32,System.Boolean)
// 0x0000026B System.Void Mirror.NetworkServer::SendToReady(T,System.Int32)
// 0x0000026C System.Void Mirror.NetworkServer::SendToObservers(Mirror.NetworkIdentity,T,System.Int32)
// 0x0000026D System.Void Mirror.NetworkServer::SendToReadyObservers(Mirror.NetworkIdentity,T,System.Boolean,System.Int32)
// 0x0000026E System.Void Mirror.NetworkServer::SendToReady(Mirror.NetworkIdentity,T,System.Boolean,System.Int32)
// 0x0000026F System.Void Mirror.NetworkServer::SendToReadyObservers(Mirror.NetworkIdentity,T,System.Int32)
// 0x00000270 System.Void Mirror.NetworkServer::SendToReady(Mirror.NetworkIdentity,T,System.Int32)
// 0x00000271 System.Void Mirror.NetworkServer::OnTransportConnected(System.Int32)
extern void NetworkServer_OnTransportConnected_m662E1418EE78E47302A2B40D6363323F21A0CAE3 (void);
// 0x00000272 System.Void Mirror.NetworkServer::OnConnected(Mirror.NetworkConnectionToClient)
extern void NetworkServer_OnConnected_m92037D3CFABD7D7514108F39242084325D9B8DD6 (void);
// 0x00000273 System.Boolean Mirror.NetworkServer::UnpackAndInvoke(Mirror.NetworkConnectionToClient,Mirror.NetworkReader,System.Int32)
extern void NetworkServer_UnpackAndInvoke_m202013D7033D71E38A0E83C4884CD21717C00CFA (void);
// 0x00000274 System.Void Mirror.NetworkServer::OnTransportData(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkServer_OnTransportData_m44765B391CB6A066F3081BC65781B024B4070AFD (void);
// 0x00000275 System.Void Mirror.NetworkServer::OnTransportDisconnected(System.Int32)
extern void NetworkServer_OnTransportDisconnected_mC27AD3B742C5E8F72F6C8329A7B2545AAAA4293A (void);
// 0x00000276 System.Void Mirror.NetworkServer::OnError(System.Int32,System.Exception)
extern void NetworkServer_OnError_m3C684B7F8A69D5F77520F7F867288C34CEB52510 (void);
// 0x00000277 System.Void Mirror.NetworkServer::RegisterHandler(System.Action`2<Mirror.NetworkConnectionToClient,T>,System.Boolean)
// 0x00000278 System.Void Mirror.NetworkServer::RegisterHandler(System.Action`3<Mirror.NetworkConnectionToClient,T,System.Int32>,System.Boolean)
// 0x00000279 System.Void Mirror.NetworkServer::ReplaceHandler(System.Action`2<Mirror.NetworkConnectionToClient,T>,System.Boolean)
// 0x0000027A System.Void Mirror.NetworkServer::ReplaceHandler(System.Action`1<T>,System.Boolean)
// 0x0000027B System.Void Mirror.NetworkServer::UnregisterHandler()
// 0x0000027C System.Void Mirror.NetworkServer::ClearHandlers()
extern void NetworkServer_ClearHandlers_m9CA5A2CC1467B94753025BDA3186A20249D1DD52 (void);
// 0x0000027D System.Boolean Mirror.NetworkServer::GetNetworkIdentity(UnityEngine.GameObject,Mirror.NetworkIdentity&)
extern void NetworkServer_GetNetworkIdentity_mC661B7E843BFE80D1D590A10671B8E9D5C4A98B6 (void);
// 0x0000027E System.Void Mirror.NetworkServer::DisconnectAll()
extern void NetworkServer_DisconnectAll_mCD86E876F1AEE03A2E835C13C0C977A937CD8A4B (void);
// 0x0000027F System.Boolean Mirror.NetworkServer::AddPlayerForConnection(Mirror.NetworkConnectionToClient,UnityEngine.GameObject)
extern void NetworkServer_AddPlayerForConnection_mC2F0A5B518D848C12E42D427DAA8FD0BFC4E9C56 (void);
// 0x00000280 System.Boolean Mirror.NetworkServer::AddPlayerForConnection(Mirror.NetworkConnectionToClient,UnityEngine.GameObject,System.Guid)
extern void NetworkServer_AddPlayerForConnection_mA88DC3FDC9F32EF84D510CC44C4E03B2C09DEEA8 (void);
// 0x00000281 System.Boolean Mirror.NetworkServer::ReplacePlayerForConnection(Mirror.NetworkConnectionToClient,UnityEngine.GameObject,System.Boolean)
extern void NetworkServer_ReplacePlayerForConnection_m5500FF147E5894EC95AD5A1BFAE72DD69F89B0E8 (void);
// 0x00000282 System.Boolean Mirror.NetworkServer::ReplacePlayerForConnection(Mirror.NetworkConnectionToClient,UnityEngine.GameObject,System.Guid,System.Boolean)
extern void NetworkServer_ReplacePlayerForConnection_mE8CC0EE5D8074A327338BA6980B53BC9A57FEEEE (void);
// 0x00000283 System.Void Mirror.NetworkServer::SetClientReady(Mirror.NetworkConnectionToClient)
extern void NetworkServer_SetClientReady_mAB57FA4FC627929E203E77FAF03E99C7D0890DEE (void);
// 0x00000284 System.Void Mirror.NetworkServer::SetClientNotReady(Mirror.NetworkConnectionToClient)
extern void NetworkServer_SetClientNotReady_m8C9CE076987FC6D3A107C56A6028A6B8292825F5 (void);
// 0x00000285 System.Void Mirror.NetworkServer::SetAllClientsNotReady()
extern void NetworkServer_SetAllClientsNotReady_mCD791365FA6BDA777EB2FB2865B63B0A6BEE4926 (void);
// 0x00000286 System.Void Mirror.NetworkServer::OnClientReadyMessage(Mirror.NetworkConnectionToClient,Mirror.ReadyMessage)
extern void NetworkServer_OnClientReadyMessage_mA653969EC458916D6B3870A5B59B1455BEEBC466 (void);
// 0x00000287 System.Void Mirror.NetworkServer::ShowForConnection(Mirror.NetworkIdentity,Mirror.NetworkConnection)
extern void NetworkServer_ShowForConnection_m65FE936D0D166AA00A3D8EFB5AAA2120F54C2CAE (void);
// 0x00000288 System.Void Mirror.NetworkServer::HideForConnection(Mirror.NetworkIdentity,Mirror.NetworkConnection)
extern void NetworkServer_HideForConnection_mD1B712AA6BF00CFC2DAAF3EAFE3A9C777AE43ED4 (void);
// 0x00000289 System.Void Mirror.NetworkServer::RemovePlayerForConnection(Mirror.NetworkConnection,System.Boolean)
extern void NetworkServer_RemovePlayerForConnection_mE52A009050437EC470A9BF46165473ACF841A5D9 (void);
// 0x0000028A System.Void Mirror.NetworkServer::OnCommandMessage(Mirror.NetworkConnectionToClient,Mirror.CommandMessage,System.Int32)
extern void NetworkServer_OnCommandMessage_mCC9C792BC26F41538359A854D37641AF52CFCC33 (void);
// 0x0000028B System.ArraySegment`1<System.Byte> Mirror.NetworkServer::CreateSpawnMessagePayload(System.Boolean,Mirror.NetworkIdentity,Mirror.PooledNetworkWriter,Mirror.PooledNetworkWriter)
extern void NetworkServer_CreateSpawnMessagePayload_m607B37EC828537F1A87CE12B8A077C0B861CBF34 (void);
// 0x0000028C System.Void Mirror.NetworkServer::SendSpawnMessage(Mirror.NetworkIdentity,Mirror.NetworkConnection)
extern void NetworkServer_SendSpawnMessage_mBCD386FE79E43D7C4DC964A4D539A857C5D5D102 (void);
// 0x0000028D System.Void Mirror.NetworkServer::SendChangeOwnerMessage(Mirror.NetworkIdentity,Mirror.NetworkConnectionToClient)
extern void NetworkServer_SendChangeOwnerMessage_mEEA8A44F0B9EC0BD743D520CAF2C20BBB53E35CD (void);
// 0x0000028E System.Void Mirror.NetworkServer::SpawnObject(UnityEngine.GameObject,Mirror.NetworkConnection)
extern void NetworkServer_SpawnObject_mB3AD303BED0C815C723891DD4583AA2C289CB163 (void);
// 0x0000028F System.Void Mirror.NetworkServer::Spawn(UnityEngine.GameObject,Mirror.NetworkConnection)
extern void NetworkServer_Spawn_mE71DE744EA836EE5C4EA51B6FCDDCDB83C1D7846 (void);
// 0x00000290 System.Void Mirror.NetworkServer::Spawn(UnityEngine.GameObject,UnityEngine.GameObject)
extern void NetworkServer_Spawn_mC82932E22819C7C7682841AB3300F26151AC6D15 (void);
// 0x00000291 System.Void Mirror.NetworkServer::Spawn(UnityEngine.GameObject,System.Guid,Mirror.NetworkConnection)
extern void NetworkServer_Spawn_m14592C4F1111BE2C02BEFDBE75F31AE54150924D (void);
// 0x00000292 System.Boolean Mirror.NetworkServer::ValidateSceneObject(Mirror.NetworkIdentity)
extern void NetworkServer_ValidateSceneObject_m4ED8F63B17AAA36BAA4CB4DA2BA2E70A838EFD71 (void);
// 0x00000293 System.Boolean Mirror.NetworkServer::SpawnObjects()
extern void NetworkServer_SpawnObjects_m9C61D31EB90F43E1219F7C228B38BB01CA7F7A12 (void);
// 0x00000294 System.Void Mirror.NetworkServer::Respawn(Mirror.NetworkIdentity)
extern void NetworkServer_Respawn_mEDAC1931E0963B50A235D2A66D481898B7AE1B07 (void);
// 0x00000295 System.Void Mirror.NetworkServer::SpawnObserversForConnection(Mirror.NetworkConnectionToClient)
extern void NetworkServer_SpawnObserversForConnection_mB30B555FBED825E2B0B8EE579F51CF71F55FA9F0 (void);
// 0x00000296 System.Void Mirror.NetworkServer::UnSpawn(UnityEngine.GameObject)
extern void NetworkServer_UnSpawn_m1584C6BCEE95BA77A803C51B77E166E4F94FBCC9 (void);
// 0x00000297 System.Void Mirror.NetworkServer::DestroyPlayerForConnection(Mirror.NetworkConnectionToClient)
extern void NetworkServer_DestroyPlayerForConnection_mC6114493615209566C55E11CF4823B999EA9796A (void);
// 0x00000298 System.Void Mirror.NetworkServer::DestroyObject(Mirror.NetworkIdentity,Mirror.NetworkServer/DestroyMode)
extern void NetworkServer_DestroyObject_m4D81EAD44ABA120EDB93BEB38C8E3F6F8C41798C (void);
// 0x00000299 System.Void Mirror.NetworkServer::DestroyObject(UnityEngine.GameObject,Mirror.NetworkServer/DestroyMode)
extern void NetworkServer_DestroyObject_m774FF3CB9D73D6CB140F8090CA05F5B1C070AF76 (void);
// 0x0000029A System.Void Mirror.NetworkServer::Destroy(UnityEngine.GameObject)
extern void NetworkServer_Destroy_m113ADC5542A4627A397C75DC3ECC9BC87F404FFB (void);
// 0x0000029B System.Void Mirror.NetworkServer::AddAllReadyServerConnectionsToObservers(Mirror.NetworkIdentity)
extern void NetworkServer_AddAllReadyServerConnectionsToObservers_mDCCA7E9B82CA04C30C3ABA187C3306DE6939C8A1 (void);
// 0x0000029C System.Void Mirror.NetworkServer::RebuildObserversDefault(Mirror.NetworkIdentity,System.Boolean)
extern void NetworkServer_RebuildObserversDefault_mE396735085539EB07B92E6A9CBBCB33277C0BAFF (void);
// 0x0000029D System.Void Mirror.NetworkServer::RebuildObserversCustom(Mirror.NetworkIdentity,System.Boolean)
extern void NetworkServer_RebuildObserversCustom_mBB971C93348EACA1C5977913D7A7E5533043E022 (void);
// 0x0000029E System.Void Mirror.NetworkServer::RebuildObservers(Mirror.NetworkIdentity,System.Boolean)
extern void NetworkServer_RebuildObservers_m2E49CB20FF4AF0D1EAC08AF0B0271BBEFCDCDABC (void);
// 0x0000029F Mirror.NetworkWriter Mirror.NetworkServer::GetEntitySerializationForConnection(Mirror.NetworkIdentity,Mirror.NetworkConnectionToClient)
extern void NetworkServer_GetEntitySerializationForConnection_m1540A07C348D470AEDE16F3A9968BAD8098FBF73 (void);
// 0x000002A0 System.Void Mirror.NetworkServer::BroadcastToConnection(Mirror.NetworkConnectionToClient)
extern void NetworkServer_BroadcastToConnection_m7482A9022FA26FC120EA40F9E19FF498581792A4 (void);
// 0x000002A1 System.Void Mirror.NetworkServer::Broadcast()
extern void NetworkServer_Broadcast_mF3905B75CAA7F9C63C37CE19552CAB614628A009 (void);
// 0x000002A2 System.Void Mirror.NetworkServer::NetworkEarlyUpdate()
extern void NetworkServer_NetworkEarlyUpdate_m9990AD91F269B79E8AB54FE45A4EC6251F7C457A (void);
// 0x000002A3 System.Void Mirror.NetworkServer::NetworkLateUpdate()
extern void NetworkServer_NetworkLateUpdate_mAC035ABA4BB49EF98A547B2A96F3AA54F2E5D63E (void);
// 0x000002A4 System.Void Mirror.NetworkServer::.cctor()
extern void NetworkServer__cctor_mC0DCCC43EE4E77463F04C3FECCBB84579DAA6C9A (void);
// 0x000002A5 System.Void Mirror.NetworkServer/<>c__DisplayClass50_0`1::.ctor()
// 0x000002A6 System.Void Mirror.NetworkServer/<>c__DisplayClass50_0`1::<ReplaceHandler>b__0(Mirror.NetworkConnectionToClient,T)
// 0x000002A7 System.Void Mirror.NetworkStartPosition::Awake()
extern void NetworkStartPosition_Awake_m957E26CD377C729F2F9442F0C737CC4729A72C47 (void);
// 0x000002A8 System.Void Mirror.NetworkStartPosition::OnDestroy()
extern void NetworkStartPosition_OnDestroy_m00BD40ED4641A481ACE5967C211E6EBF84585FC8 (void);
// 0x000002A9 System.Void Mirror.NetworkStartPosition::.ctor()
extern void NetworkStartPosition__ctor_m0630A7917AC91935D77BF5755391FB6EA6DFBFD0 (void);
// 0x000002AA System.Double Mirror.NetworkTime::get_localTime()
extern void NetworkTime_get_localTime_mE9F343542C384B1D15D2541759FAD163D00946D6 (void);
// 0x000002AB System.Double Mirror.NetworkTime::get_time()
extern void NetworkTime_get_time_m8DE6EBB4100FF44B7A282881FD69798D737C24C6 (void);
// 0x000002AC System.Double Mirror.NetworkTime::get_timeVariance()
extern void NetworkTime_get_timeVariance_m34EB4F607490CA48E1896BE2075B9564AF922BFE (void);
// 0x000002AD System.Double Mirror.NetworkTime::get_timeStandardDeviation()
extern void NetworkTime_get_timeStandardDeviation_m0BA80800A296887072DF1191307EB274579081A7 (void);
// 0x000002AE System.Double Mirror.NetworkTime::get_offset()
extern void NetworkTime_get_offset_m962DCC4FAA7B83F01518096E0BD17F34EEA84CBF (void);
// 0x000002AF System.Double Mirror.NetworkTime::get_rtt()
extern void NetworkTime_get_rtt_m9E4EC224EB95FBB7B8329E9A225D3275D0770C66 (void);
// 0x000002B0 System.Double Mirror.NetworkTime::get_rttVariance()
extern void NetworkTime_get_rttVariance_m7E2A5F27F0741B45B69848E26ED1D03A33F0A037 (void);
// 0x000002B1 System.Double Mirror.NetworkTime::get_rttStandardDeviation()
extern void NetworkTime_get_rttStandardDeviation_m557D817D40153F321D01AB0D272E620525F2CA38 (void);
// 0x000002B2 System.Void Mirror.NetworkTime::ResetStatics()
extern void NetworkTime_ResetStatics_m178218DA64630690EADF332FDB4DB878FAC88BDF (void);
// 0x000002B3 System.Void Mirror.NetworkTime::UpdateClient()
extern void NetworkTime_UpdateClient_m852C8649B8BDB874079CAD542EC213D2165D53DD (void);
// 0x000002B4 System.Void Mirror.NetworkTime::OnServerPing(Mirror.NetworkConnectionToClient,Mirror.NetworkPingMessage)
extern void NetworkTime_OnServerPing_m0313EC1EE6F416509525C9C1471260E6AFD1BEE6 (void);
// 0x000002B5 System.Void Mirror.NetworkTime::OnClientPong(Mirror.NetworkPongMessage)
extern void NetworkTime_OnClientPong_m44B93D45F64A2A215C28274C23018A9CADBDC253 (void);
// 0x000002B6 System.Void Mirror.NetworkTime::.cctor()
extern void NetworkTime__cctor_m747E3CF7CD3F32E4BFCA9975961B4C76A4737770 (void);
// 0x000002B7 System.Void Mirror.NetworkWriter::Reset()
extern void NetworkWriter_Reset_m9F34945A36E170550D63E41D2CF9C26ACB03FF77 (void);
// 0x000002B8 System.Void Mirror.NetworkWriter::EnsureCapacity(System.Int32)
extern void NetworkWriter_EnsureCapacity_m8686EBB645C1892C928AB87D8FD73148B547BDC9 (void);
// 0x000002B9 System.Byte[] Mirror.NetworkWriter::ToArray()
extern void NetworkWriter_ToArray_mC9A28117C639BA09B09A43343D3D3A3488A187E7 (void);
// 0x000002BA System.ArraySegment`1<System.Byte> Mirror.NetworkWriter::ToArraySegment()
extern void NetworkWriter_ToArraySegment_m20B3969C06F4320029AD66A820B0523A8D6641EF (void);
// 0x000002BB System.Void Mirror.NetworkWriter::WriteBlittable(T)
// 0x000002BC System.Void Mirror.NetworkWriter::WriteBlittableNullable(System.Nullable`1<T>)
// 0x000002BD System.Void Mirror.NetworkWriter::WriteByte(System.Byte)
extern void NetworkWriter_WriteByte_mC22A6863F21D47005ECAD28F5AB6FC5248B23D0E (void);
// 0x000002BE System.Void Mirror.NetworkWriter::WriteBytes(System.Byte[],System.Int32,System.Int32)
extern void NetworkWriter_WriteBytes_m1EF0FDBA6E07D885F57CB524710524715BB7E374 (void);
// 0x000002BF System.Void Mirror.NetworkWriter::Write(T)
// 0x000002C0 System.Void Mirror.NetworkWriter::.ctor()
extern void NetworkWriter__ctor_mD3E7B77EDCCE633CFEE83EBC1DB06355D0460A5F (void);
// 0x000002C1 System.Void Mirror.NetworkWriterExtensions::WriteByte(Mirror.NetworkWriter,System.Byte)
extern void NetworkWriterExtensions_WriteByte_mFFEFC20C5AB176841BBBE10C1E4A866EAF369B96 (void);
// 0x000002C2 System.Void Mirror.NetworkWriterExtensions::WriteByteNullable(Mirror.NetworkWriter,System.Nullable`1<System.Byte>)
extern void NetworkWriterExtensions_WriteByteNullable_mF9B242CDD70D6377003A16616017AE7E7ECF828C (void);
// 0x000002C3 System.Void Mirror.NetworkWriterExtensions::WriteSByte(Mirror.NetworkWriter,System.SByte)
extern void NetworkWriterExtensions_WriteSByte_m9AA9E2E5390BD4AC9CEA8A05EAE1234D9C48B8C8 (void);
// 0x000002C4 System.Void Mirror.NetworkWriterExtensions::WriteSByteNullable(Mirror.NetworkWriter,System.Nullable`1<System.SByte>)
extern void NetworkWriterExtensions_WriteSByteNullable_m31024FEF67A2D39E6942456902E0609F86D86BBD (void);
// 0x000002C5 System.Void Mirror.NetworkWriterExtensions::WriteChar(Mirror.NetworkWriter,System.Char)
extern void NetworkWriterExtensions_WriteChar_m14899B6388B37CA68D9BA49CF0BE0C3EE956E49E (void);
// 0x000002C6 System.Void Mirror.NetworkWriterExtensions::WriteCharNullable(Mirror.NetworkWriter,System.Nullable`1<System.Char>)
extern void NetworkWriterExtensions_WriteCharNullable_mFC4447120353234274E133070B57CF79A00D19DD (void);
// 0x000002C7 System.Void Mirror.NetworkWriterExtensions::WriteBool(Mirror.NetworkWriter,System.Boolean)
extern void NetworkWriterExtensions_WriteBool_mF0B4A6ADF594071A21F89B4AC0FD3594F5D38157 (void);
// 0x000002C8 System.Void Mirror.NetworkWriterExtensions::WriteBoolNullable(Mirror.NetworkWriter,System.Nullable`1<System.Boolean>)
extern void NetworkWriterExtensions_WriteBoolNullable_m0827D39D922053803E6913AD577C1D6FD59184C1 (void);
// 0x000002C9 System.Void Mirror.NetworkWriterExtensions::WriteShort(Mirror.NetworkWriter,System.Int16)
extern void NetworkWriterExtensions_WriteShort_mF005FFDEF8C55047B7EC7BCA61241AEB294BE7DE (void);
// 0x000002CA System.Void Mirror.NetworkWriterExtensions::WriteShortNullable(Mirror.NetworkWriter,System.Nullable`1<System.Int16>)
extern void NetworkWriterExtensions_WriteShortNullable_m47963A4259643D24DE1C2CBA482F093FABE3E581 (void);
// 0x000002CB System.Void Mirror.NetworkWriterExtensions::WriteUShort(Mirror.NetworkWriter,System.UInt16)
extern void NetworkWriterExtensions_WriteUShort_m12CCA0CE8DA51CAD6A67CDC3680393119557AAB3 (void);
// 0x000002CC System.Void Mirror.NetworkWriterExtensions::WriteUShortNullable(Mirror.NetworkWriter,System.Nullable`1<System.UInt16>)
extern void NetworkWriterExtensions_WriteUShortNullable_mAC2A24B07CE4482BA3F167EC7622609CAD3A2B94 (void);
// 0x000002CD System.Void Mirror.NetworkWriterExtensions::WriteInt(Mirror.NetworkWriter,System.Int32)
extern void NetworkWriterExtensions_WriteInt_m131D043593351682FEA1F18B455B80F78A24FD12 (void);
// 0x000002CE System.Void Mirror.NetworkWriterExtensions::WriteIntNullable(Mirror.NetworkWriter,System.Nullable`1<System.Int32>)
extern void NetworkWriterExtensions_WriteIntNullable_m543C5968EB92A9A79A703C2E48F11C57F3FE8A38 (void);
// 0x000002CF System.Void Mirror.NetworkWriterExtensions::WriteUInt(Mirror.NetworkWriter,System.UInt32)
extern void NetworkWriterExtensions_WriteUInt_mFDC458AC35F7EF0D09E7A0D779ACF37D12346D64 (void);
// 0x000002D0 System.Void Mirror.NetworkWriterExtensions::WriteUIntNullable(Mirror.NetworkWriter,System.Nullable`1<System.UInt32>)
extern void NetworkWriterExtensions_WriteUIntNullable_m045E0D347FF8BA92E1884FC666419FF015395DFA (void);
// 0x000002D1 System.Void Mirror.NetworkWriterExtensions::WriteLong(Mirror.NetworkWriter,System.Int64)
extern void NetworkWriterExtensions_WriteLong_m60818DB28C20076BB7B90A357DF8F7D58CD2AD5E (void);
// 0x000002D2 System.Void Mirror.NetworkWriterExtensions::WriteLongNullable(Mirror.NetworkWriter,System.Nullable`1<System.Int64>)
extern void NetworkWriterExtensions_WriteLongNullable_m36E40C95D550FC603346B3F598D01A0141D4B566 (void);
// 0x000002D3 System.Void Mirror.NetworkWriterExtensions::WriteULong(Mirror.NetworkWriter,System.UInt64)
extern void NetworkWriterExtensions_WriteULong_mF3B9637FA1C3EA31551616D085A5A3AF041E6963 (void);
// 0x000002D4 System.Void Mirror.NetworkWriterExtensions::WriteULongNullable(Mirror.NetworkWriter,System.Nullable`1<System.UInt64>)
extern void NetworkWriterExtensions_WriteULongNullable_mB436FDD19B8677D5E98E0C55848DAE698E7906FB (void);
// 0x000002D5 System.Void Mirror.NetworkWriterExtensions::WriteFloat(Mirror.NetworkWriter,System.Single)
extern void NetworkWriterExtensions_WriteFloat_mCC353B70D9FBACD2D2CECA565EC7B2B30400A6A5 (void);
// 0x000002D6 System.Void Mirror.NetworkWriterExtensions::WriteFloatNullable(Mirror.NetworkWriter,System.Nullable`1<System.Single>)
extern void NetworkWriterExtensions_WriteFloatNullable_m05BBFDF4558D76CD3D615EC8293D46C5EAAE5F43 (void);
// 0x000002D7 System.Void Mirror.NetworkWriterExtensions::WriteDouble(Mirror.NetworkWriter,System.Double)
extern void NetworkWriterExtensions_WriteDouble_m40D19C266720079063B22A53B6940E1CF6482BFC (void);
// 0x000002D8 System.Void Mirror.NetworkWriterExtensions::WriteDoubleNullable(Mirror.NetworkWriter,System.Nullable`1<System.Double>)
extern void NetworkWriterExtensions_WriteDoubleNullable_m6199C6C58FCCBDE4C4D4B5A184FC037749609666 (void);
// 0x000002D9 System.Void Mirror.NetworkWriterExtensions::WriteDecimal(Mirror.NetworkWriter,System.Decimal)
extern void NetworkWriterExtensions_WriteDecimal_m1F5400E8F7D91C15538FC724A4B430A818479431 (void);
// 0x000002DA System.Void Mirror.NetworkWriterExtensions::WriteDecimalNullable(Mirror.NetworkWriter,System.Nullable`1<System.Decimal>)
extern void NetworkWriterExtensions_WriteDecimalNullable_mFD5228BF1FFBB3A62C4876029B853D560FB29B17 (void);
// 0x000002DB System.Void Mirror.NetworkWriterExtensions::WriteString(Mirror.NetworkWriter,System.String)
extern void NetworkWriterExtensions_WriteString_m31C762A5BBACB77129E085AB7D7A9AEEB9ACA95F (void);
// 0x000002DC System.Void Mirror.NetworkWriterExtensions::WriteBytesAndSizeSegment(Mirror.NetworkWriter,System.ArraySegment`1<System.Byte>)
extern void NetworkWriterExtensions_WriteBytesAndSizeSegment_mAD214D7E8A080A5615D400A6C3F2F5218F3AD11B (void);
// 0x000002DD System.Void Mirror.NetworkWriterExtensions::WriteBytesAndSize(Mirror.NetworkWriter,System.Byte[])
extern void NetworkWriterExtensions_WriteBytesAndSize_mB11A8C12622432C14420D616F763F130DD1A91C9 (void);
// 0x000002DE System.Void Mirror.NetworkWriterExtensions::WriteBytesAndSize(Mirror.NetworkWriter,System.Byte[],System.Int32,System.Int32)
extern void NetworkWriterExtensions_WriteBytesAndSize_m75E79671D06A8D7C3CA09E02844E6316F3A3E56B (void);
// 0x000002DF System.Void Mirror.NetworkWriterExtensions::WriteArraySegment(Mirror.NetworkWriter,System.ArraySegment`1<T>)
// 0x000002E0 System.Void Mirror.NetworkWriterExtensions::WriteVector2(Mirror.NetworkWriter,UnityEngine.Vector2)
extern void NetworkWriterExtensions_WriteVector2_m52B333B1242AC5E0490DA980D5954157FDF9949F (void);
// 0x000002E1 System.Void Mirror.NetworkWriterExtensions::WriteVector2Nullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector2>)
extern void NetworkWriterExtensions_WriteVector2Nullable_m2178D4AC2AD94F0C4D69B7329701D73F7F769662 (void);
// 0x000002E2 System.Void Mirror.NetworkWriterExtensions::WriteVector3(Mirror.NetworkWriter,UnityEngine.Vector3)
extern void NetworkWriterExtensions_WriteVector3_m2895E336F7B2F52819808DC35F574B10D3317609 (void);
// 0x000002E3 System.Void Mirror.NetworkWriterExtensions::WriteVector3Nullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector3>)
extern void NetworkWriterExtensions_WriteVector3Nullable_m015585A48318B31C257AB9DB240306359004EDA5 (void);
// 0x000002E4 System.Void Mirror.NetworkWriterExtensions::WriteVector4(Mirror.NetworkWriter,UnityEngine.Vector4)
extern void NetworkWriterExtensions_WriteVector4_mE75893C683F8C655244CE272B0B050662E751414 (void);
// 0x000002E5 System.Void Mirror.NetworkWriterExtensions::WriteVector4Nullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector4>)
extern void NetworkWriterExtensions_WriteVector4Nullable_m006747750FDE41EDD60C58BFA8E9219AA93813E7 (void);
// 0x000002E6 System.Void Mirror.NetworkWriterExtensions::WriteVector2Int(Mirror.NetworkWriter,UnityEngine.Vector2Int)
extern void NetworkWriterExtensions_WriteVector2Int_mDA0A622A19099A741DE393233204CD6FB89FF75A (void);
// 0x000002E7 System.Void Mirror.NetworkWriterExtensions::WriteVector2IntNullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector2Int>)
extern void NetworkWriterExtensions_WriteVector2IntNullable_m32D79441D84ADCF6C22C4A05A3E7A74576EF8FF9 (void);
// 0x000002E8 System.Void Mirror.NetworkWriterExtensions::WriteVector3Int(Mirror.NetworkWriter,UnityEngine.Vector3Int)
extern void NetworkWriterExtensions_WriteVector3Int_m4911D397D68D855E386699BB99ECF4BD0F5F7060 (void);
// 0x000002E9 System.Void Mirror.NetworkWriterExtensions::WriteVector3IntNullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Vector3Int>)
extern void NetworkWriterExtensions_WriteVector3IntNullable_m5907EAB248F53ACCB09E9062ECA7F82A2F0A3A79 (void);
// 0x000002EA System.Void Mirror.NetworkWriterExtensions::WriteColor(Mirror.NetworkWriter,UnityEngine.Color)
extern void NetworkWriterExtensions_WriteColor_m7EA61C4CEE7C0F49BCAF7339D1A76F4ECA50626E (void);
// 0x000002EB System.Void Mirror.NetworkWriterExtensions::WriteColorNullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Color>)
extern void NetworkWriterExtensions_WriteColorNullable_m9B0C40392853593AF7A9DA61A91B59C7641BF154 (void);
// 0x000002EC System.Void Mirror.NetworkWriterExtensions::WriteColor32(Mirror.NetworkWriter,UnityEngine.Color32)
extern void NetworkWriterExtensions_WriteColor32_mC430555DAD65FA8A1ACA7117122960F6412B3079 (void);
// 0x000002ED System.Void Mirror.NetworkWriterExtensions::WriteColor32Nullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Color32>)
extern void NetworkWriterExtensions_WriteColor32Nullable_m7EEE0A8672EBD42EBDC68D52F6C1BF745997D54A (void);
// 0x000002EE System.Void Mirror.NetworkWriterExtensions::WriteQuaternion(Mirror.NetworkWriter,UnityEngine.Quaternion)
extern void NetworkWriterExtensions_WriteQuaternion_m96A981CD95861D3F23358155CD1CCBCDF185D909 (void);
// 0x000002EF System.Void Mirror.NetworkWriterExtensions::WriteQuaternionNullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Quaternion>)
extern void NetworkWriterExtensions_WriteQuaternionNullable_m6138C62C7B32C2858D59488B953583FE82854AC4 (void);
// 0x000002F0 System.Void Mirror.NetworkWriterExtensions::WriteRect(Mirror.NetworkWriter,UnityEngine.Rect)
extern void NetworkWriterExtensions_WriteRect_m1C462FFD3B7FE7B73B6F733CDECEDCBFFF9F3A05 (void);
// 0x000002F1 System.Void Mirror.NetworkWriterExtensions::WriteRectNullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Rect>)
extern void NetworkWriterExtensions_WriteRectNullable_m25B1C832F6F6027DAD898FD6F7D37014459CF91D (void);
// 0x000002F2 System.Void Mirror.NetworkWriterExtensions::WritePlane(Mirror.NetworkWriter,UnityEngine.Plane)
extern void NetworkWriterExtensions_WritePlane_m75F27F25C9E50A60C635D376D4C1D993D1376A97 (void);
// 0x000002F3 System.Void Mirror.NetworkWriterExtensions::WritePlaneNullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Plane>)
extern void NetworkWriterExtensions_WritePlaneNullable_m61AD793E53710488E94AB3932CBFA3EB43AA00E5 (void);
// 0x000002F4 System.Void Mirror.NetworkWriterExtensions::WriteRay(Mirror.NetworkWriter,UnityEngine.Ray)
extern void NetworkWriterExtensions_WriteRay_mF907AD59F7170BBB74CF49611FBBAF069DD6D1F2 (void);
// 0x000002F5 System.Void Mirror.NetworkWriterExtensions::WriteRayNullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Ray>)
extern void NetworkWriterExtensions_WriteRayNullable_m14DEFB4AA69F614EFF381EEBD7D6D19BB862E30A (void);
// 0x000002F6 System.Void Mirror.NetworkWriterExtensions::WriteMatrix4x4(Mirror.NetworkWriter,UnityEngine.Matrix4x4)
extern void NetworkWriterExtensions_WriteMatrix4x4_m40B7941CFBB2912FA11AFB35F5729E4F2383BEAD (void);
// 0x000002F7 System.Void Mirror.NetworkWriterExtensions::WriteMatrix4x4Nullable(Mirror.NetworkWriter,System.Nullable`1<UnityEngine.Matrix4x4>)
extern void NetworkWriterExtensions_WriteMatrix4x4Nullable_m792F404CB27BEA99EEE8797CFFE4BD76B4209253 (void);
// 0x000002F8 System.Void Mirror.NetworkWriterExtensions::WriteGuid(Mirror.NetworkWriter,System.Guid)
extern void NetworkWriterExtensions_WriteGuid_m1700462B08DC1D6B80FF00521C50C5A85E781AFB (void);
// 0x000002F9 System.Void Mirror.NetworkWriterExtensions::WriteGuidNullable(Mirror.NetworkWriter,System.Nullable`1<System.Guid>)
extern void NetworkWriterExtensions_WriteGuidNullable_mAE2592EC889F49BCC5C6C264C2DBAE743C2B618D (void);
// 0x000002FA System.Void Mirror.NetworkWriterExtensions::WriteNetworkIdentity(Mirror.NetworkWriter,Mirror.NetworkIdentity)
extern void NetworkWriterExtensions_WriteNetworkIdentity_m6B7BF775A09551E47F1E98F2603E0C3B8358FC64 (void);
// 0x000002FB System.Void Mirror.NetworkWriterExtensions::WriteNetworkBehaviour(Mirror.NetworkWriter,Mirror.NetworkBehaviour)
extern void NetworkWriterExtensions_WriteNetworkBehaviour_mCA7167729B04CFA80D5F7AB5D9C43FC24B4DF20D (void);
// 0x000002FC System.Void Mirror.NetworkWriterExtensions::WriteTransform(Mirror.NetworkWriter,UnityEngine.Transform)
extern void NetworkWriterExtensions_WriteTransform_m3118EFCE291DE2182B785E099FE30B3D68F36830 (void);
// 0x000002FD System.Void Mirror.NetworkWriterExtensions::WriteGameObject(Mirror.NetworkWriter,UnityEngine.GameObject)
extern void NetworkWriterExtensions_WriteGameObject_m461FDFA27D93D42B1C6C1B5F84EC2314E479E45B (void);
// 0x000002FE System.Void Mirror.NetworkWriterExtensions::WriteList(Mirror.NetworkWriter,System.Collections.Generic.List`1<T>)
// 0x000002FF System.Void Mirror.NetworkWriterExtensions::WriteArray(Mirror.NetworkWriter,T[])
// 0x00000300 System.Void Mirror.NetworkWriterExtensions::WriteUri(Mirror.NetworkWriter,System.Uri)
extern void NetworkWriterExtensions_WriteUri_mECC3E974CD9C900CEBB24E5E4743DF2D41E3533F (void);
// 0x00000301 System.Void Mirror.NetworkWriterExtensions::WriteTexture2D(Mirror.NetworkWriter,UnityEngine.Texture2D)
extern void NetworkWriterExtensions_WriteTexture2D_mD912047E3473A437DC8C57D552A778CC00A5AB7A (void);
// 0x00000302 System.Void Mirror.NetworkWriterExtensions::WriteSprite(Mirror.NetworkWriter,UnityEngine.Sprite)
extern void NetworkWriterExtensions_WriteSprite_mDCBD674842A4AB01AB69507B9C7AFEAA12DD67A1 (void);
// 0x00000303 System.Void Mirror.NetworkWriterExtensions::.cctor()
extern void NetworkWriterExtensions__cctor_mEF540B4CE4511D89773F16F2D0E8092600455D59 (void);
// 0x00000304 System.Void Mirror.PooledNetworkWriter::Dispose()
extern void PooledNetworkWriter_Dispose_m4D1350BAFF1AE5BE1A2C6540EFACB15F02511A18 (void);
// 0x00000305 System.Void Mirror.PooledNetworkWriter::.ctor()
extern void PooledNetworkWriter__ctor_m0575E62AABFA81E2F757C76EA455757C8F27B165 (void);
// 0x00000306 Mirror.PooledNetworkWriter Mirror.NetworkWriterPool::GetWriter()
extern void NetworkWriterPool_GetWriter_m53506C8016911951C82F2F83E45592CE2F9A85AE (void);
// 0x00000307 System.Void Mirror.NetworkWriterPool::Recycle(Mirror.PooledNetworkWriter)
extern void NetworkWriterPool_Recycle_m8E7D8C8ED6F0856380CE750DBEF1D0EC5BAF8FB3 (void);
// 0x00000308 System.Void Mirror.NetworkWriterPool::.cctor()
extern void NetworkWriterPool__cctor_m9274ABE8EE4582DC7D9DF184F7F73F2DF3FC7CF3 (void);
// 0x00000309 System.Void Mirror.NetworkWriterPool/<>c::.cctor()
extern void U3CU3Ec__cctor_m3FD838FF3EDCED87947D48608D148699344D8A93 (void);
// 0x0000030A System.Void Mirror.NetworkWriterPool/<>c::.ctor()
extern void U3CU3Ec__ctor_m54EF1D3A06EA9F18684008AB6E69268AF8AD7A9A (void);
// 0x0000030B Mirror.PooledNetworkWriter Mirror.NetworkWriterPool/<>c::<.cctor>b__3_0()
extern void U3CU3Ec_U3C_cctorU3Eb__3_0_mB0A5B91C0E7EC65AD20C53B230FD500DECA1A061 (void);
// 0x0000030C System.Void Mirror.Pool`1::.ctor(System.Func`1<T>,System.Int32)
// 0x0000030D T Mirror.Pool`1::Take()
// 0x0000030E System.Void Mirror.Pool`1::Return(T)
// 0x0000030F System.Int32 Mirror.Pool`1::get_Count()
// 0x00000310 System.Double Mirror.Snapshot::get_remoteTimestamp()
// 0x00000311 System.Void Mirror.Snapshot::set_remoteTimestamp(System.Double)
// 0x00000312 System.Double Mirror.Snapshot::get_localTimestamp()
// 0x00000313 System.Void Mirror.Snapshot::set_localTimestamp(System.Double)
// 0x00000314 System.Void Mirror.SnapshotInterpolation::InsertIfNewEnough(T,System.Collections.Generic.SortedList`2<System.Double,T>)
// 0x00000315 System.Boolean Mirror.SnapshotInterpolation::HasAmountOlderThan(System.Collections.Generic.SortedList`2<System.Double,T>,System.Double,System.Int32)
// 0x00000316 System.Boolean Mirror.SnapshotInterpolation::HasEnough(System.Collections.Generic.SortedList`2<System.Double,T>,System.Double,System.Double)
// 0x00000317 System.Boolean Mirror.SnapshotInterpolation::HasEnoughWithoutFirst(System.Collections.Generic.SortedList`2<System.Double,T>,System.Double,System.Double)
// 0x00000318 System.Double Mirror.SnapshotInterpolation::CalculateCatchup(System.Collections.Generic.SortedList`2<System.Double,T>,System.Int32,System.Double)
// 0x00000319 System.Void Mirror.SnapshotInterpolation::GetFirstSecondAndDelta(System.Collections.Generic.SortedList`2<System.Double,T>,T&,T&,System.Double&)
// 0x0000031A System.Boolean Mirror.SnapshotInterpolation::Compute(System.Double,System.Double,System.Double&,System.Double,System.Collections.Generic.SortedList`2<System.Double,T>,System.Int32,System.Single,System.Func`4<T,T,System.Double,T>,T&)
// 0x0000031B System.Int32 Mirror.SyncIDictionary`2::get_Count()
// 0x0000031C System.Boolean Mirror.SyncIDictionary`2::get_IsReadOnly()
// 0x0000031D System.Void Mirror.SyncIDictionary`2::set_IsReadOnly(System.Boolean)
// 0x0000031E System.Void Mirror.SyncIDictionary`2::add_Callback(Mirror.SyncIDictionary`2/SyncDictionaryChanged<TKey,TValue>)
// 0x0000031F System.Void Mirror.SyncIDictionary`2::remove_Callback(Mirror.SyncIDictionary`2/SyncDictionaryChanged<TKey,TValue>)
// 0x00000320 System.Void Mirror.SyncIDictionary`2::Reset()
// 0x00000321 System.Collections.Generic.ICollection`1<TKey> Mirror.SyncIDictionary`2::get_Keys()
// 0x00000322 System.Collections.Generic.ICollection`1<TValue> Mirror.SyncIDictionary`2::get_Values()
// 0x00000323 System.Collections.Generic.IEnumerable`1<TKey> Mirror.SyncIDictionary`2::System.Collections.Generic.IReadOnlyDictionary<TKey,TValue>.get_Keys()
// 0x00000324 System.Collections.Generic.IEnumerable`1<TValue> Mirror.SyncIDictionary`2::System.Collections.Generic.IReadOnlyDictionary<TKey,TValue>.get_Values()
// 0x00000325 System.Void Mirror.SyncIDictionary`2::ClearChanges()
// 0x00000326 System.Void Mirror.SyncIDictionary`2::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
// 0x00000327 System.Void Mirror.SyncIDictionary`2::AddOperation(Mirror.SyncIDictionary`2/Operation<TKey,TValue>,TKey,TValue)
// 0x00000328 System.Void Mirror.SyncIDictionary`2::OnSerializeAll(Mirror.NetworkWriter)
// 0x00000329 System.Void Mirror.SyncIDictionary`2::OnSerializeDelta(Mirror.NetworkWriter)
// 0x0000032A System.Void Mirror.SyncIDictionary`2::OnDeserializeAll(Mirror.NetworkReader)
// 0x0000032B System.Void Mirror.SyncIDictionary`2::OnDeserializeDelta(Mirror.NetworkReader)
// 0x0000032C System.Void Mirror.SyncIDictionary`2::Clear()
// 0x0000032D System.Boolean Mirror.SyncIDictionary`2::ContainsKey(TKey)
// 0x0000032E System.Boolean Mirror.SyncIDictionary`2::Remove(TKey)
// 0x0000032F TValue Mirror.SyncIDictionary`2::get_Item(TKey)
// 0x00000330 System.Void Mirror.SyncIDictionary`2::set_Item(TKey,TValue)
// 0x00000331 System.Boolean Mirror.SyncIDictionary`2::TryGetValue(TKey,TValue&)
// 0x00000332 System.Void Mirror.SyncIDictionary`2::Add(TKey,TValue)
// 0x00000333 System.Void Mirror.SyncIDictionary`2::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000334 System.Boolean Mirror.SyncIDictionary`2::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000335 System.Void Mirror.SyncIDictionary`2::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
// 0x00000336 System.Boolean Mirror.SyncIDictionary`2::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000337 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> Mirror.SyncIDictionary`2::GetEnumerator()
// 0x00000338 System.Collections.IEnumerator Mirror.SyncIDictionary`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000339 System.Void Mirror.SyncIDictionary`2/SyncDictionaryChanged::.ctor(System.Object,System.IntPtr)
// 0x0000033A System.Void Mirror.SyncIDictionary`2/SyncDictionaryChanged::Invoke(Mirror.SyncIDictionary`2/Operation<TKey,TValue>,TKey,TValue)
// 0x0000033B System.IAsyncResult Mirror.SyncIDictionary`2/SyncDictionaryChanged::BeginInvoke(Mirror.SyncIDictionary`2/Operation<TKey,TValue>,TKey,TValue,System.AsyncCallback,System.Object)
// 0x0000033C System.Void Mirror.SyncIDictionary`2/SyncDictionaryChanged::EndInvoke(System.IAsyncResult)
// 0x0000033D System.Void Mirror.SyncDictionary`2::.ctor()
// 0x0000033E System.Void Mirror.SyncDictionary`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000033F System.Void Mirror.SyncDictionary`2::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
// 0x00000340 System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> Mirror.SyncDictionary`2::get_Values()
// 0x00000341 System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> Mirror.SyncDictionary`2::get_Keys()
// 0x00000342 System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> Mirror.SyncDictionary`2::GetEnumerator()
// 0x00000343 System.Int32 Mirror.SyncList`1::get_Count()
// 0x00000344 System.Boolean Mirror.SyncList`1::get_IsReadOnly()
// 0x00000345 System.Void Mirror.SyncList`1::set_IsReadOnly(System.Boolean)
// 0x00000346 System.Void Mirror.SyncList`1::add_Callback(Mirror.SyncList`1/SyncListChanged<T>)
// 0x00000347 System.Void Mirror.SyncList`1::remove_Callback(Mirror.SyncList`1/SyncListChanged<T>)
// 0x00000348 System.Void Mirror.SyncList`1::.ctor()
// 0x00000349 System.Void Mirror.SyncList`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000034A System.Void Mirror.SyncList`1::.ctor(System.Collections.Generic.IList`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000034B System.Void Mirror.SyncList`1::ClearChanges()
// 0x0000034C System.Void Mirror.SyncList`1::Reset()
// 0x0000034D System.Void Mirror.SyncList`1::AddOperation(Mirror.SyncList`1/Operation<T>,System.Int32,T,T)
// 0x0000034E System.Void Mirror.SyncList`1::OnSerializeAll(Mirror.NetworkWriter)
// 0x0000034F System.Void Mirror.SyncList`1::OnSerializeDelta(Mirror.NetworkWriter)
// 0x00000350 System.Void Mirror.SyncList`1::OnDeserializeAll(Mirror.NetworkReader)
// 0x00000351 System.Void Mirror.SyncList`1::OnDeserializeDelta(Mirror.NetworkReader)
// 0x00000352 System.Void Mirror.SyncList`1::Add(T)
// 0x00000353 System.Void Mirror.SyncList`1::AddRange(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000354 System.Void Mirror.SyncList`1::Clear()
// 0x00000355 System.Boolean Mirror.SyncList`1::Contains(T)
// 0x00000356 System.Void Mirror.SyncList`1::CopyTo(T[],System.Int32)
// 0x00000357 System.Int32 Mirror.SyncList`1::IndexOf(T)
// 0x00000358 System.Int32 Mirror.SyncList`1::FindIndex(System.Predicate`1<T>)
// 0x00000359 T Mirror.SyncList`1::Find(System.Predicate`1<T>)
// 0x0000035A System.Collections.Generic.List`1<T> Mirror.SyncList`1::FindAll(System.Predicate`1<T>)
// 0x0000035B System.Void Mirror.SyncList`1::Insert(System.Int32,T)
// 0x0000035C System.Void Mirror.SyncList`1::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
// 0x0000035D System.Boolean Mirror.SyncList`1::Remove(T)
// 0x0000035E System.Void Mirror.SyncList`1::RemoveAt(System.Int32)
// 0x0000035F System.Int32 Mirror.SyncList`1::RemoveAll(System.Predicate`1<T>)
// 0x00000360 T Mirror.SyncList`1::get_Item(System.Int32)
// 0x00000361 System.Void Mirror.SyncList`1::set_Item(System.Int32,T)
// 0x00000362 Mirror.SyncList`1/Enumerator<T> Mirror.SyncList`1::GetEnumerator()
// 0x00000363 System.Collections.Generic.IEnumerator`1<T> Mirror.SyncList`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000364 System.Collections.IEnumerator Mirror.SyncList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000365 System.Void Mirror.SyncList`1/SyncListChanged::.ctor(System.Object,System.IntPtr)
// 0x00000366 System.Void Mirror.SyncList`1/SyncListChanged::Invoke(Mirror.SyncList`1/Operation<T>,System.Int32,T,T)
// 0x00000367 System.IAsyncResult Mirror.SyncList`1/SyncListChanged::BeginInvoke(Mirror.SyncList`1/Operation<T>,System.Int32,T,T,System.AsyncCallback,System.Object)
// 0x00000368 System.Void Mirror.SyncList`1/SyncListChanged::EndInvoke(System.IAsyncResult)
// 0x00000369 T Mirror.SyncList`1/Enumerator::get_Current()
// 0x0000036A System.Void Mirror.SyncList`1/Enumerator::set_Current(T)
// 0x0000036B System.Void Mirror.SyncList`1/Enumerator::.ctor(Mirror.SyncList`1<T>)
// 0x0000036C System.Boolean Mirror.SyncList`1/Enumerator::MoveNext()
// 0x0000036D System.Void Mirror.SyncList`1/Enumerator::Reset()
// 0x0000036E System.Object Mirror.SyncList`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x0000036F System.Void Mirror.SyncList`1/Enumerator::Dispose()
// 0x00000370 System.Void Mirror.SyncObject::ClearChanges()
// 0x00000371 System.Void Mirror.SyncObject::Flush()
extern void SyncObject_Flush_m7B8340A5FD92A0A1C092F115976457A1B84AD8EF (void);
// 0x00000372 System.Void Mirror.SyncObject::OnSerializeAll(Mirror.NetworkWriter)
// 0x00000373 System.Void Mirror.SyncObject::OnSerializeDelta(Mirror.NetworkWriter)
// 0x00000374 System.Void Mirror.SyncObject::OnDeserializeAll(Mirror.NetworkReader)
// 0x00000375 System.Void Mirror.SyncObject::OnDeserializeDelta(Mirror.NetworkReader)
// 0x00000376 System.Void Mirror.SyncObject::Reset()
// 0x00000377 System.Void Mirror.SyncObject::.ctor()
extern void SyncObject__ctor_mCE98D127674428340204B86B247E70CEE3352E0B (void);
// 0x00000378 System.Void Mirror.SyncObject/<>c::.cctor()
extern void U3CU3Ec__cctor_m0CE42A80DD1706EAE2A8B0A4852DE8A5818F48DA (void);
// 0x00000379 System.Void Mirror.SyncObject/<>c::.ctor()
extern void U3CU3Ec__ctor_mFC9B0D0EE6169B2BE6911DFCA57A995E26ADB768 (void);
// 0x0000037A System.Boolean Mirror.SyncObject/<>c::<.ctor>b__9_0()
extern void U3CU3Ec_U3C_ctorU3Eb__9_0_m5132968FC2EF9EEB668A486A2F97DAEB1DAE1312 (void);
// 0x0000037B System.Int32 Mirror.SyncSet`1::get_Count()
// 0x0000037C System.Boolean Mirror.SyncSet`1::get_IsReadOnly()
// 0x0000037D System.Void Mirror.SyncSet`1::set_IsReadOnly(System.Boolean)
// 0x0000037E System.Void Mirror.SyncSet`1::add_Callback(Mirror.SyncSet`1/SyncSetChanged<T>)
// 0x0000037F System.Void Mirror.SyncSet`1::remove_Callback(Mirror.SyncSet`1/SyncSetChanged<T>)
// 0x00000380 System.Void Mirror.SyncSet`1::.ctor(System.Collections.Generic.ISet`1<T>)
// 0x00000381 System.Void Mirror.SyncSet`1::Reset()
// 0x00000382 System.Void Mirror.SyncSet`1::ClearChanges()
// 0x00000383 System.Void Mirror.SyncSet`1::AddOperation(Mirror.SyncSet`1/Operation<T>,T)
// 0x00000384 System.Void Mirror.SyncSet`1::AddOperation(Mirror.SyncSet`1/Operation<T>)
// 0x00000385 System.Void Mirror.SyncSet`1::OnSerializeAll(Mirror.NetworkWriter)
// 0x00000386 System.Void Mirror.SyncSet`1::OnSerializeDelta(Mirror.NetworkWriter)
// 0x00000387 System.Void Mirror.SyncSet`1::OnDeserializeAll(Mirror.NetworkReader)
// 0x00000388 System.Void Mirror.SyncSet`1::OnDeserializeDelta(Mirror.NetworkReader)
// 0x00000389 System.Boolean Mirror.SyncSet`1::Add(T)
// 0x0000038A System.Void Mirror.SyncSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000038B System.Void Mirror.SyncSet`1::Clear()
// 0x0000038C System.Boolean Mirror.SyncSet`1::Contains(T)
// 0x0000038D System.Void Mirror.SyncSet`1::CopyTo(T[],System.Int32)
// 0x0000038E System.Boolean Mirror.SyncSet`1::Remove(T)
// 0x0000038F System.Collections.Generic.IEnumerator`1<T> Mirror.SyncSet`1::GetEnumerator()
// 0x00000390 System.Collections.IEnumerator Mirror.SyncSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000391 System.Void Mirror.SyncSet`1::ExceptWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000392 System.Void Mirror.SyncSet`1::IntersectWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000393 System.Void Mirror.SyncSet`1::IntersectWithSet(System.Collections.Generic.ISet`1<T>)
// 0x00000394 System.Boolean Mirror.SyncSet`1::IsProperSubsetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000395 System.Boolean Mirror.SyncSet`1::IsProperSupersetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000396 System.Boolean Mirror.SyncSet`1::IsSubsetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000397 System.Boolean Mirror.SyncSet`1::IsSupersetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000398 System.Boolean Mirror.SyncSet`1::Overlaps(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000399 System.Boolean Mirror.SyncSet`1::SetEquals(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000039A System.Void Mirror.SyncSet`1::SymmetricExceptWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000039B System.Void Mirror.SyncSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000039C System.Void Mirror.SyncSet`1/SyncSetChanged::.ctor(System.Object,System.IntPtr)
// 0x0000039D System.Void Mirror.SyncSet`1/SyncSetChanged::Invoke(Mirror.SyncSet`1/Operation<T>,T)
// 0x0000039E System.IAsyncResult Mirror.SyncSet`1/SyncSetChanged::BeginInvoke(Mirror.SyncSet`1/Operation<T>,T,System.AsyncCallback,System.Object)
// 0x0000039F System.Void Mirror.SyncSet`1/SyncSetChanged::EndInvoke(System.IAsyncResult)
// 0x000003A0 System.Void Mirror.SyncHashSet`1::.ctor()
// 0x000003A1 System.Void Mirror.SyncHashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000003A2 System.Collections.Generic.HashSet`1/Enumerator<T> Mirror.SyncHashSet`1::GetEnumerator()
// 0x000003A3 System.Void Mirror.SyncSortedSet`1::.ctor()
// 0x000003A4 System.Void Mirror.SyncSortedSet`1::.ctor(System.Collections.Generic.IComparer`1<T>)
// 0x000003A5 System.Collections.Generic.SortedSet`1/Enumerator<T> Mirror.SyncSortedSet`1::GetEnumerator()
// 0x000003A6 T Mirror.SyncVar`1::get_Value()
// 0x000003A7 System.Void Mirror.SyncVar`1::set_Value(T)
// 0x000003A8 System.Void Mirror.SyncVar`1::add_Callback(System.Action`2<T,T>)
// 0x000003A9 System.Void Mirror.SyncVar`1::remove_Callback(System.Action`2<T,T>)
// 0x000003AA System.Void Mirror.SyncVar`1::InvokeCallback(T,T)
// 0x000003AB System.Void Mirror.SyncVar`1::ClearChanges()
// 0x000003AC System.Void Mirror.SyncVar`1::Reset()
// 0x000003AD System.Void Mirror.SyncVar`1::.ctor(T)
// 0x000003AE T Mirror.SyncVar`1::op_Implicit(Mirror.SyncVar`1<T>)
// 0x000003AF Mirror.SyncVar`1<T> Mirror.SyncVar`1::op_Implicit(T)
// 0x000003B0 System.Void Mirror.SyncVar`1::OnSerializeAll(Mirror.NetworkWriter)
// 0x000003B1 System.Void Mirror.SyncVar`1::OnSerializeDelta(Mirror.NetworkWriter)
// 0x000003B2 System.Void Mirror.SyncVar`1::OnDeserializeAll(Mirror.NetworkReader)
// 0x000003B3 System.Void Mirror.SyncVar`1::OnDeserializeDelta(Mirror.NetworkReader)
// 0x000003B4 System.Boolean Mirror.SyncVar`1::Equals(T)
// 0x000003B5 System.String Mirror.SyncVar`1::ToString()
// 0x000003B6 UnityEngine.GameObject Mirror.SyncVarGameObject::get_Value()
extern void SyncVarGameObject_get_Value_mE65AFF3C6639B6AB7C0F38060DD8B8C235FAEDB6 (void);
// 0x000003B7 System.Void Mirror.SyncVarGameObject::set_Value(UnityEngine.GameObject)
extern void SyncVarGameObject_set_Value_mC8BB9614EEB3E3C3398260C0A046B0BF3720F3F7 (void);
// 0x000003B8 System.Void Mirror.SyncVarGameObject::add_Callback(System.Action`2<UnityEngine.GameObject,UnityEngine.GameObject>)
extern void SyncVarGameObject_add_Callback_m4665D2E0E039D093CBDCC74C53163CA3175039CD (void);
// 0x000003B9 System.Void Mirror.SyncVarGameObject::remove_Callback(System.Action`2<UnityEngine.GameObject,UnityEngine.GameObject>)
extern void SyncVarGameObject_remove_Callback_mFFFFF221B06FBE7DA9A3379CC467664F0391317D (void);
// 0x000003BA System.Void Mirror.SyncVarGameObject::InvokeCallback(System.UInt32,System.UInt32)
extern void SyncVarGameObject_InvokeCallback_mB3619997E27EB3129A67673A31E6C4703D8083E7 (void);
// 0x000003BB System.Void Mirror.SyncVarGameObject::.ctor(UnityEngine.GameObject)
extern void SyncVarGameObject__ctor_mB04849C010A23C4DA5D5C888E549306AD764450A (void);
// 0x000003BC System.UInt32 Mirror.SyncVarGameObject::GetNetId(UnityEngine.GameObject)
extern void SyncVarGameObject_GetNetId_m57ED9EB23EC30C59E33A0941AC55D1A3ADCB3DB1 (void);
// 0x000003BD UnityEngine.GameObject Mirror.SyncVarGameObject::GetGameObject(System.UInt32)
extern void SyncVarGameObject_GetGameObject_m8066BA7A21591654063C99AF4EB3AA9718C87EEB (void);
// 0x000003BE UnityEngine.GameObject Mirror.SyncVarGameObject::op_Implicit(Mirror.SyncVarGameObject)
extern void SyncVarGameObject_op_Implicit_mE581FC095E6AF11A3DA0512548C56A140A75443F (void);
// 0x000003BF Mirror.SyncVarGameObject Mirror.SyncVarGameObject::op_Implicit(UnityEngine.GameObject)
extern void SyncVarGameObject_op_Implicit_mD0E1C1226C73A8A837C045965234D7AF1423B6EE (void);
// 0x000003C0 System.Boolean Mirror.SyncVarGameObject::op_Equality(Mirror.SyncVarGameObject,Mirror.SyncVarGameObject)
extern void SyncVarGameObject_op_Equality_m73BFF9908CB976703889892036BD22F5040F9CDA (void);
// 0x000003C1 System.Boolean Mirror.SyncVarGameObject::op_Inequality(Mirror.SyncVarGameObject,Mirror.SyncVarGameObject)
extern void SyncVarGameObject_op_Inequality_mF89450FF6FEF63BA1E386073EF9CEA5A2704CCF8 (void);
// 0x000003C2 System.Boolean Mirror.SyncVarGameObject::op_Equality(Mirror.SyncVarGameObject,UnityEngine.GameObject)
extern void SyncVarGameObject_op_Equality_m10BF041FD59A41511A31FC153850BDC542259816 (void);
// 0x000003C3 System.Boolean Mirror.SyncVarGameObject::op_Inequality(Mirror.SyncVarGameObject,UnityEngine.GameObject)
extern void SyncVarGameObject_op_Inequality_m1A4F171A99D45F75E4CFA7EA5A68BE17B6517529 (void);
// 0x000003C4 System.Boolean Mirror.SyncVarGameObject::op_Equality(UnityEngine.GameObject,Mirror.SyncVarGameObject)
extern void SyncVarGameObject_op_Equality_m6BFF6D527A8F7A1BA2763B7E413790723D1D68E7 (void);
// 0x000003C5 System.Boolean Mirror.SyncVarGameObject::op_Inequality(UnityEngine.GameObject,Mirror.SyncVarGameObject)
extern void SyncVarGameObject_op_Inequality_m2F33424297230D22C6105DDC26A6E1E3AB4A4298 (void);
// 0x000003C6 System.Boolean Mirror.SyncVarGameObject::Equals(System.Object)
extern void SyncVarGameObject_Equals_m34C64231D0EA8B97E7FC51381A26633BA6E07D55 (void);
// 0x000003C7 System.Int32 Mirror.SyncVarGameObject::GetHashCode()
extern void SyncVarGameObject_GetHashCode_mF0EAFF0E15E80F2E221DC3FF59D2CFE04DF3B07A (void);
// 0x000003C8 T Mirror.SyncVarNetworkBehaviour`1::get_Value()
// 0x000003C9 System.Void Mirror.SyncVarNetworkBehaviour`1::set_Value(T)
// 0x000003CA System.Void Mirror.SyncVarNetworkBehaviour`1::add_Callback(System.Action`2<T,T>)
// 0x000003CB System.Void Mirror.SyncVarNetworkBehaviour`1::remove_Callback(System.Action`2<T,T>)
// 0x000003CC System.Void Mirror.SyncVarNetworkBehaviour`1::InvokeCallback(System.UInt64,System.UInt64)
// 0x000003CD System.Void Mirror.SyncVarNetworkBehaviour`1::.ctor(T)
// 0x000003CE T Mirror.SyncVarNetworkBehaviour`1::op_Implicit(Mirror.SyncVarNetworkBehaviour`1<T>)
// 0x000003CF Mirror.SyncVarNetworkBehaviour`1<T> Mirror.SyncVarNetworkBehaviour`1::op_Implicit(T)
// 0x000003D0 System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Equality(Mirror.SyncVarNetworkBehaviour`1<T>,Mirror.SyncVarNetworkBehaviour`1<T>)
// 0x000003D1 System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Inequality(Mirror.SyncVarNetworkBehaviour`1<T>,Mirror.SyncVarNetworkBehaviour`1<T>)
// 0x000003D2 System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Equality(Mirror.SyncVarNetworkBehaviour`1<T>,Mirror.NetworkBehaviour)
// 0x000003D3 System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Inequality(Mirror.SyncVarNetworkBehaviour`1<T>,Mirror.NetworkBehaviour)
// 0x000003D4 System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Equality(Mirror.SyncVarNetworkBehaviour`1<T>,T)
// 0x000003D5 System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Inequality(Mirror.SyncVarNetworkBehaviour`1<T>,T)
// 0x000003D6 System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Equality(Mirror.NetworkBehaviour,Mirror.SyncVarNetworkBehaviour`1<T>)
// 0x000003D7 System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Inequality(Mirror.NetworkBehaviour,Mirror.SyncVarNetworkBehaviour`1<T>)
// 0x000003D8 System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Equality(T,Mirror.SyncVarNetworkBehaviour`1<T>)
// 0x000003D9 System.Boolean Mirror.SyncVarNetworkBehaviour`1::op_Inequality(T,Mirror.SyncVarNetworkBehaviour`1<T>)
// 0x000003DA System.Boolean Mirror.SyncVarNetworkBehaviour`1::Equals(System.Object)
// 0x000003DB System.Int32 Mirror.SyncVarNetworkBehaviour`1::GetHashCode()
// 0x000003DC System.UInt64 Mirror.SyncVarNetworkBehaviour`1::Pack(System.UInt32,System.Byte)
// 0x000003DD System.Void Mirror.SyncVarNetworkBehaviour`1::Unpack(System.UInt64,System.UInt32&,System.Byte&)
// 0x000003DE T Mirror.SyncVarNetworkBehaviour`1::ULongToNetworkBehaviour(System.UInt64)
// 0x000003DF System.UInt64 Mirror.SyncVarNetworkBehaviour`1::NetworkBehaviourToULong(T)
// 0x000003E0 System.Void Mirror.SyncVarNetworkBehaviour`1::OnSerializeAll(Mirror.NetworkWriter)
// 0x000003E1 System.Void Mirror.SyncVarNetworkBehaviour`1::OnSerializeDelta(Mirror.NetworkWriter)
// 0x000003E2 System.Void Mirror.SyncVarNetworkBehaviour`1::OnDeserializeAll(Mirror.NetworkReader)
// 0x000003E3 System.Void Mirror.SyncVarNetworkBehaviour`1::OnDeserializeDelta(Mirror.NetworkReader)
// 0x000003E4 Mirror.NetworkIdentity Mirror.SyncVarNetworkIdentity::get_Value()
extern void SyncVarNetworkIdentity_get_Value_m777B8CFC8DAA96885BB1C305D0AEB486F2FF8413 (void);
// 0x000003E5 System.Void Mirror.SyncVarNetworkIdentity::set_Value(Mirror.NetworkIdentity)
extern void SyncVarNetworkIdentity_set_Value_m6BC7FA7E6EF557B7307781890F419CF973E1A2B5 (void);
// 0x000003E6 System.Void Mirror.SyncVarNetworkIdentity::add_Callback(System.Action`2<Mirror.NetworkIdentity,Mirror.NetworkIdentity>)
extern void SyncVarNetworkIdentity_add_Callback_m71EA63626C4AEB46E49E6FF03F0ECF253CA00FD3 (void);
// 0x000003E7 System.Void Mirror.SyncVarNetworkIdentity::remove_Callback(System.Action`2<Mirror.NetworkIdentity,Mirror.NetworkIdentity>)
extern void SyncVarNetworkIdentity_remove_Callback_mD9822968C88796117217CC09181809478066F64B (void);
// 0x000003E8 System.Void Mirror.SyncVarNetworkIdentity::InvokeCallback(System.UInt32,System.UInt32)
extern void SyncVarNetworkIdentity_InvokeCallback_m6B0BE6A3FECA596F98865EDE165754DE9C7B2E26 (void);
// 0x000003E9 System.Void Mirror.SyncVarNetworkIdentity::.ctor(Mirror.NetworkIdentity)
extern void SyncVarNetworkIdentity__ctor_m1FDDAF32DA124DFCA5BA034E2F6B4428D91BFD49 (void);
// 0x000003EA Mirror.NetworkIdentity Mirror.SyncVarNetworkIdentity::op_Implicit(Mirror.SyncVarNetworkIdentity)
extern void SyncVarNetworkIdentity_op_Implicit_m78CF163D678CA7D6D127D5C6E2E55835B310428F (void);
// 0x000003EB Mirror.SyncVarNetworkIdentity Mirror.SyncVarNetworkIdentity::op_Implicit(Mirror.NetworkIdentity)
extern void SyncVarNetworkIdentity_op_Implicit_m8F58E47D4FDC73AC11C1C31D45828E404AE4C21D (void);
// 0x000003EC System.Boolean Mirror.SyncVarNetworkIdentity::op_Equality(Mirror.SyncVarNetworkIdentity,Mirror.SyncVarNetworkIdentity)
extern void SyncVarNetworkIdentity_op_Equality_m388AF3AD37E0B41F48F4F58DD80CEB0336B76064 (void);
// 0x000003ED System.Boolean Mirror.SyncVarNetworkIdentity::op_Inequality(Mirror.SyncVarNetworkIdentity,Mirror.SyncVarNetworkIdentity)
extern void SyncVarNetworkIdentity_op_Inequality_mDB84F11552998D1C74AE4E28CBD3BA7CFDAAE32A (void);
// 0x000003EE System.Boolean Mirror.SyncVarNetworkIdentity::op_Equality(Mirror.SyncVarNetworkIdentity,Mirror.NetworkIdentity)
extern void SyncVarNetworkIdentity_op_Equality_m09A02E3285097BF64AFD4484BA2EB94527124279 (void);
// 0x000003EF System.Boolean Mirror.SyncVarNetworkIdentity::op_Inequality(Mirror.SyncVarNetworkIdentity,Mirror.NetworkIdentity)
extern void SyncVarNetworkIdentity_op_Inequality_m332E8B21430C8195C6F80A55DCAE641769159F77 (void);
// 0x000003F0 System.Boolean Mirror.SyncVarNetworkIdentity::op_Equality(Mirror.NetworkIdentity,Mirror.SyncVarNetworkIdentity)
extern void SyncVarNetworkIdentity_op_Equality_mAA1610C1CFC76F7974D9C9790DF47B864ACC442C (void);
// 0x000003F1 System.Boolean Mirror.SyncVarNetworkIdentity::op_Inequality(Mirror.NetworkIdentity,Mirror.SyncVarNetworkIdentity)
extern void SyncVarNetworkIdentity_op_Inequality_mAF0E505A71340F2547CBC44D52CB183FCFB8643E (void);
// 0x000003F2 System.Boolean Mirror.SyncVarNetworkIdentity::Equals(System.Object)
extern void SyncVarNetworkIdentity_Equals_mF95A3D92C3CA69A16A1AB018BA623604AB1CAFE3 (void);
// 0x000003F3 System.Int32 Mirror.SyncVarNetworkIdentity::GetHashCode()
extern void SyncVarNetworkIdentity_GetHashCode_mF09764959B3EF41924CD4AF803F0855C3BCD7084 (void);
// 0x000003F4 System.Void Mirror.LatencySimulation::Awake()
extern void LatencySimulation_Awake_mEAC826A25C58752C0C69564A5348BD01CA9CB7C1 (void);
// 0x000003F5 System.Void Mirror.LatencySimulation::OnEnable()
extern void LatencySimulation_OnEnable_mD242DDC8C43D5466149B69B85EAE0DE562756A9C (void);
// 0x000003F6 System.Void Mirror.LatencySimulation::OnDisable()
extern void LatencySimulation_OnDisable_m199817612D997387B3E911992B686D05DB2E07B1 (void);
// 0x000003F7 System.Single Mirror.LatencySimulation::Noise(System.Single)
extern void LatencySimulation_Noise_m1A3BC7F8CD1202484C1A868B4D23FC9791AC5CF5 (void);
// 0x000003F8 System.Single Mirror.LatencySimulation::SimulateLatency(System.Int32)
extern void LatencySimulation_SimulateLatency_mAF38F234D096858BDBD02E0437436E71EF41CB7C (void);
// 0x000003F9 System.Void Mirror.LatencySimulation::SimulateSend(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32,System.Single,System.Collections.Generic.List`1<Mirror.QueuedMessage>,System.Collections.Generic.List`1<Mirror.QueuedMessage>)
extern void LatencySimulation_SimulateSend_mDF97FC1EECFEC05ADB7531989317B5295595CEBB (void);
// 0x000003FA System.Boolean Mirror.LatencySimulation::Available()
extern void LatencySimulation_Available_mB4E68E0825607F4005173F6C7D09373DA21ECD03 (void);
// 0x000003FB System.Void Mirror.LatencySimulation::ClientConnect(System.String)
extern void LatencySimulation_ClientConnect_m58CFCBC09F36A3BF4B4AFACDE77C3DDBE813A75F (void);
// 0x000003FC System.Void Mirror.LatencySimulation::ClientConnect(System.Uri)
extern void LatencySimulation_ClientConnect_mA949A0B2766FB09E9D5DFC5739F9BCD2702FFC5F (void);
// 0x000003FD System.Boolean Mirror.LatencySimulation::ClientConnected()
extern void LatencySimulation_ClientConnected_mDF44EB7042EF3BC3ED0B44748C4B8839D64C6715 (void);
// 0x000003FE System.Void Mirror.LatencySimulation::ClientDisconnect()
extern void LatencySimulation_ClientDisconnect_m24DA6CF3C4FADC708864D354670ABC2E7B2A5EA9 (void);
// 0x000003FF System.Void Mirror.LatencySimulation::ClientSend(System.ArraySegment`1<System.Byte>,System.Int32)
extern void LatencySimulation_ClientSend_mE46A5530B7A922BEED635E7F1AA40F4AF109C353 (void);
// 0x00000400 System.Uri Mirror.LatencySimulation::ServerUri()
extern void LatencySimulation_ServerUri_mE307B5DFAA51ACE4E586BC9F9E3F46D72A3CEE9E (void);
// 0x00000401 System.Boolean Mirror.LatencySimulation::ServerActive()
extern void LatencySimulation_ServerActive_mEC3859D604A254EBBE0613A38D095DFC70C9FBB3 (void);
// 0x00000402 System.String Mirror.LatencySimulation::ServerGetClientAddress(System.Int32)
extern void LatencySimulation_ServerGetClientAddress_m49F80F36FAF18A175ABD6AFC040E1916F433AD40 (void);
// 0x00000403 System.Void Mirror.LatencySimulation::ServerDisconnect(System.Int32)
extern void LatencySimulation_ServerDisconnect_m1C5804F3662C09822C4E111BAB2314537BCD83D3 (void);
// 0x00000404 System.Void Mirror.LatencySimulation::ServerSend(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void LatencySimulation_ServerSend_m13439312B8DA75B034DCB22DE7AB5D804F1A5629 (void);
// 0x00000405 System.Void Mirror.LatencySimulation::ServerStart()
extern void LatencySimulation_ServerStart_m1EA1DA984B406C9EF1ED2B70010DB9DF20F51C01 (void);
// 0x00000406 System.Void Mirror.LatencySimulation::ServerStop()
extern void LatencySimulation_ServerStop_m42D319CCA21268E27974470ADBA90C0D478EF316 (void);
// 0x00000407 System.Void Mirror.LatencySimulation::ClientEarlyUpdate()
extern void LatencySimulation_ClientEarlyUpdate_m53492BCFAB22CBE97F2AD567B5DC1C6E82C851FA (void);
// 0x00000408 System.Void Mirror.LatencySimulation::ServerEarlyUpdate()
extern void LatencySimulation_ServerEarlyUpdate_mBA19D74A9BB375B315A174B428E691ADD4A30952 (void);
// 0x00000409 System.Void Mirror.LatencySimulation::ClientLateUpdate()
extern void LatencySimulation_ClientLateUpdate_mF0C4A41CEA86EFB8A86E97633249B22E59122924 (void);
// 0x0000040A System.Void Mirror.LatencySimulation::ServerLateUpdate()
extern void LatencySimulation_ServerLateUpdate_m374D47AD51A3EE64CA2889A03B508F9A3BFB87E0 (void);
// 0x0000040B System.Int32 Mirror.LatencySimulation::GetBatchThreshold(System.Int32)
extern void LatencySimulation_GetBatchThreshold_m8E605B0BD4403AF800FBC4BEBB9FCEBE10378BBE (void);
// 0x0000040C System.Int32 Mirror.LatencySimulation::GetMaxPacketSize(System.Int32)
extern void LatencySimulation_GetMaxPacketSize_m9F0DCF83D8965FB56EE3BAD9CB384BE9EF0CF3B6 (void);
// 0x0000040D System.Void Mirror.LatencySimulation::Shutdown()
extern void LatencySimulation_Shutdown_mC7DD8B6818B0076AD69EEFDB07C8E4E62A548471 (void);
// 0x0000040E System.String Mirror.LatencySimulation::ToString()
extern void LatencySimulation_ToString_m2F0B4FAF301FC22A8093E73308E047FC919C5DAB (void);
// 0x0000040F System.Void Mirror.LatencySimulation::.ctor()
extern void LatencySimulation__ctor_mEBBCAAD0D6369E89B44B1C8F9636D88BD7EABDD4 (void);
// 0x00000410 System.Boolean Mirror.MiddlewareTransport::Available()
extern void MiddlewareTransport_Available_mF191BD5C8896806E9F28BA6FEC9FE1DDDC2695EB (void);
// 0x00000411 System.Int32 Mirror.MiddlewareTransport::GetMaxPacketSize(System.Int32)
extern void MiddlewareTransport_GetMaxPacketSize_mA0E7EED841343BAF79A3ADB599A1FD4A7EE601B7 (void);
// 0x00000412 System.Int32 Mirror.MiddlewareTransport::GetBatchThreshold(System.Int32)
extern void MiddlewareTransport_GetBatchThreshold_m143CE6F44716CF4698567F495FD23C5BB9D2C3B2 (void);
// 0x00000413 System.Void Mirror.MiddlewareTransport::Shutdown()
extern void MiddlewareTransport_Shutdown_m194A579B64BFD146048A8EEE1E9FF244D76F0B3B (void);
// 0x00000414 System.Void Mirror.MiddlewareTransport::ClientConnect(System.String)
extern void MiddlewareTransport_ClientConnect_m5CA5664667B11CFB4938EEB37AC81ED33749C5E1 (void);
// 0x00000415 System.Boolean Mirror.MiddlewareTransport::ClientConnected()
extern void MiddlewareTransport_ClientConnected_mA0F69BF09D2BC66AE0D356E4F9D14AF85CC22932 (void);
// 0x00000416 System.Void Mirror.MiddlewareTransport::ClientDisconnect()
extern void MiddlewareTransport_ClientDisconnect_m0D3A2544636DEF9926928276F00D6265FC970B53 (void);
// 0x00000417 System.Void Mirror.MiddlewareTransport::ClientSend(System.ArraySegment`1<System.Byte>,System.Int32)
extern void MiddlewareTransport_ClientSend_mD163AB15E518761572B6ED2CB1D0FCF73EF9D8F5 (void);
// 0x00000418 System.Void Mirror.MiddlewareTransport::ClientEarlyUpdate()
extern void MiddlewareTransport_ClientEarlyUpdate_m871E0E3C136A4525828038C57A789FD780C2C1F2 (void);
// 0x00000419 System.Void Mirror.MiddlewareTransport::ClientLateUpdate()
extern void MiddlewareTransport_ClientLateUpdate_m29921876427F25B125507E1F0F91C2E733A8DFD1 (void);
// 0x0000041A System.Boolean Mirror.MiddlewareTransport::ServerActive()
extern void MiddlewareTransport_ServerActive_m6D3FEA6629AAC8A1F7DC552699B5E73BAFE0B129 (void);
// 0x0000041B System.Void Mirror.MiddlewareTransport::ServerStart()
extern void MiddlewareTransport_ServerStart_m0D85BB7EA451FD775587EDF01BBAF13E4DF687A1 (void);
// 0x0000041C System.Void Mirror.MiddlewareTransport::ServerStop()
extern void MiddlewareTransport_ServerStop_m2211C5FF5EB34510A2D4BA3A8B43426FC12FE3E9 (void);
// 0x0000041D System.Void Mirror.MiddlewareTransport::ServerSend(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void MiddlewareTransport_ServerSend_mD53F86C744C1E2953E9B893682627A97167BCF1D (void);
// 0x0000041E System.Void Mirror.MiddlewareTransport::ServerDisconnect(System.Int32)
extern void MiddlewareTransport_ServerDisconnect_m5BBABDD2A495A780623AF5955B084B1AAD24B071 (void);
// 0x0000041F System.String Mirror.MiddlewareTransport::ServerGetClientAddress(System.Int32)
extern void MiddlewareTransport_ServerGetClientAddress_m489B2ACBA1E4572FDFBCC3C58F9B4C345E08BE5D (void);
// 0x00000420 System.Uri Mirror.MiddlewareTransport::ServerUri()
extern void MiddlewareTransport_ServerUri_m4811255EF1EE5048D396701A580ED4BDEC892991 (void);
// 0x00000421 System.Void Mirror.MiddlewareTransport::ServerEarlyUpdate()
extern void MiddlewareTransport_ServerEarlyUpdate_mF4BF126B0A54A1A9A00E9F5F86E64F54F8B9C1E5 (void);
// 0x00000422 System.Void Mirror.MiddlewareTransport::ServerLateUpdate()
extern void MiddlewareTransport_ServerLateUpdate_m20E19AB75FC9CB57E0C57C05FBE51F4A6BF90142 (void);
// 0x00000423 System.Void Mirror.MiddlewareTransport::.ctor()
extern void MiddlewareTransport__ctor_mCB190AEE11176640125AF0F7F62842471D92186B (void);
// 0x00000424 System.Void Mirror.MultiplexTransport::Awake()
extern void MultiplexTransport_Awake_m26A9019ED0ABDDE43CA027533F27F34A39BC5BB9 (void);
// 0x00000425 System.Void Mirror.MultiplexTransport::ClientEarlyUpdate()
extern void MultiplexTransport_ClientEarlyUpdate_m25E35E39DD3ED68AFA0F1829474734D1D6B7D50B (void);
// 0x00000426 System.Void Mirror.MultiplexTransport::ServerEarlyUpdate()
extern void MultiplexTransport_ServerEarlyUpdate_m2A91E85584D6897FC0F495DF270220129EB2D2CC (void);
// 0x00000427 System.Void Mirror.MultiplexTransport::ClientLateUpdate()
extern void MultiplexTransport_ClientLateUpdate_m2FF3203709B7766A969E34724CD4ECD393672084 (void);
// 0x00000428 System.Void Mirror.MultiplexTransport::ServerLateUpdate()
extern void MultiplexTransport_ServerLateUpdate_m8B4417CEAC63EBFB5754F5BA315B32329B84F1AD (void);
// 0x00000429 System.Void Mirror.MultiplexTransport::OnEnable()
extern void MultiplexTransport_OnEnable_mB1A69C2F6625B2623669BF8E158A298E3F598E17 (void);
// 0x0000042A System.Void Mirror.MultiplexTransport::OnDisable()
extern void MultiplexTransport_OnDisable_m3D0FB1850D725883E1B012BE2E191B2C4E4E4C24 (void);
// 0x0000042B System.Boolean Mirror.MultiplexTransport::Available()
extern void MultiplexTransport_Available_mD1A9553AAFEA47B7243E432217D1F097BA949DAE (void);
// 0x0000042C System.Void Mirror.MultiplexTransport::ClientConnect(System.String)
extern void MultiplexTransport_ClientConnect_m100247467A74FBD5B85B3053895265A3CD969804 (void);
// 0x0000042D System.Void Mirror.MultiplexTransport::ClientConnect(System.Uri)
extern void MultiplexTransport_ClientConnect_m68EE415F6344B5B068F38B4E9665626B814C3FC7 (void);
// 0x0000042E System.Boolean Mirror.MultiplexTransport::ClientConnected()
extern void MultiplexTransport_ClientConnected_m9A02C200A00E63BEA58542D9FF631389991B0C06 (void);
// 0x0000042F System.Void Mirror.MultiplexTransport::ClientDisconnect()
extern void MultiplexTransport_ClientDisconnect_mC5E015E98AB02572E650F30319414B7344856E2A (void);
// 0x00000430 System.Void Mirror.MultiplexTransport::ClientSend(System.ArraySegment`1<System.Byte>,System.Int32)
extern void MultiplexTransport_ClientSend_m02AC6BC2BE30B2493FC7EADA175CAE5531E16942 (void);
// 0x00000431 System.Int32 Mirror.MultiplexTransport::FromBaseId(System.Int32,System.Int32)
extern void MultiplexTransport_FromBaseId_m48D22B4B10606A37A35893E55EA3F38CF11F24E4 (void);
// 0x00000432 System.Int32 Mirror.MultiplexTransport::ToBaseId(System.Int32)
extern void MultiplexTransport_ToBaseId_m4483DB4464A8A0664EE667278437C9D6C50E4D26 (void);
// 0x00000433 System.Int32 Mirror.MultiplexTransport::ToTransportId(System.Int32)
extern void MultiplexTransport_ToTransportId_m0750AEEA94DA55CDFFAD5D9C93346686C466C4BD (void);
// 0x00000434 System.Void Mirror.MultiplexTransport::AddServerCallbacks()
extern void MultiplexTransport_AddServerCallbacks_m83C3D43FDF8C41DDC100792514769D8420AA95F3 (void);
// 0x00000435 System.Uri Mirror.MultiplexTransport::ServerUri()
extern void MultiplexTransport_ServerUri_m880F88499ECEAF9A081AEE2B4536FCF3A65AFD87 (void);
// 0x00000436 System.Boolean Mirror.MultiplexTransport::ServerActive()
extern void MultiplexTransport_ServerActive_m01DD6DAD3AC8A8A9D981CB9E345BDA698599453C (void);
// 0x00000437 System.String Mirror.MultiplexTransport::ServerGetClientAddress(System.Int32)
extern void MultiplexTransport_ServerGetClientAddress_mF215E0A6812B259249283C8189D6706AA63DA7C8 (void);
// 0x00000438 System.Void Mirror.MultiplexTransport::ServerDisconnect(System.Int32)
extern void MultiplexTransport_ServerDisconnect_m2FE63F8AF3F91CEFDD5958C366500125E02014BF (void);
// 0x00000439 System.Void Mirror.MultiplexTransport::ServerSend(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void MultiplexTransport_ServerSend_mCAC22B32D358D90BB60C39E785571B3CEB4E29BB (void);
// 0x0000043A System.Void Mirror.MultiplexTransport::ServerStart()
extern void MultiplexTransport_ServerStart_m30F65C9378DDB1B84E9C9C25D69F8E2EE1D5A887 (void);
// 0x0000043B System.Void Mirror.MultiplexTransport::ServerStop()
extern void MultiplexTransport_ServerStop_mC1038F61612FC37D90432BEB8E5F2B50672FB447 (void);
// 0x0000043C System.Int32 Mirror.MultiplexTransport::GetMaxPacketSize(System.Int32)
extern void MultiplexTransport_GetMaxPacketSize_m8E1ED512A8C33A0583C506866631DA35575CF644 (void);
// 0x0000043D System.Void Mirror.MultiplexTransport::Shutdown()
extern void MultiplexTransport_Shutdown_mCD3B2370DD52E5F859EC07816FA6BC662A62631A (void);
// 0x0000043E System.String Mirror.MultiplexTransport::ToString()
extern void MultiplexTransport_ToString_m322B81C3495ED803F44376B5E317216B4BA2E441 (void);
// 0x0000043F System.Void Mirror.MultiplexTransport::.ctor()
extern void MultiplexTransport__ctor_mEE455ED05170C5F0676EB3F47E5529D8D3637A4B (void);
// 0x00000440 System.Void Mirror.MultiplexTransport/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m5F63E485307DE66FF8DF6DA73B14BD0A8C596679 (void);
// 0x00000441 System.Void Mirror.MultiplexTransport/<>c__DisplayClass18_0::<AddServerCallbacks>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__0_m71C260E9F663345051F4ED0DBE5FD51CB051374E (void);
// 0x00000442 System.Void Mirror.MultiplexTransport/<>c__DisplayClass18_0::<AddServerCallbacks>b__1(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__1_mAEB5E470043FEFE9ACA5895E6903C43283DB85D1 (void);
// 0x00000443 System.Void Mirror.MultiplexTransport/<>c__DisplayClass18_0::<AddServerCallbacks>b__2(System.Int32,System.Exception)
extern void U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__2_m20358BC676721F4AF2C4443B36DF2E03E7DDAFD3 (void);
// 0x00000444 System.Void Mirror.MultiplexTransport/<>c__DisplayClass18_0::<AddServerCallbacks>b__3(System.Int32)
extern void U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__3_m3D94991394A01162B4B35D5D975273EE53A43152 (void);
// 0x00000445 System.Void Mirror.TelepathyTransport::Awake()
extern void TelepathyTransport_Awake_m602F5715334063DC6405856C3AD1FC29C7B7E5A5 (void);
// 0x00000446 System.Boolean Mirror.TelepathyTransport::Available()
extern void TelepathyTransport_Available_mF8EDA3882E796A7C821F5205051E549927AFEB80 (void);
// 0x00000447 System.Void Mirror.TelepathyTransport::CreateClient()
extern void TelepathyTransport_CreateClient_mA2500F52E85A569E7732413CE58F46E7F0C9502B (void);
// 0x00000448 System.Boolean Mirror.TelepathyTransport::ClientConnected()
extern void TelepathyTransport_ClientConnected_m8B5228FFC261001AAFD1F4C1DACEAB32FB728BFA (void);
// 0x00000449 System.Void Mirror.TelepathyTransport::ClientConnect(System.String)
extern void TelepathyTransport_ClientConnect_mEC8326A6AE0253B05D929D98B37E774134A8320D (void);
// 0x0000044A System.Void Mirror.TelepathyTransport::ClientConnect(System.Uri)
extern void TelepathyTransport_ClientConnect_m27B5C086CB7ED8E51EDF0D922FACFBEE88CB2BB5 (void);
// 0x0000044B System.Void Mirror.TelepathyTransport::ClientSend(System.ArraySegment`1<System.Byte>,System.Int32)
extern void TelepathyTransport_ClientSend_m3783EA93B7C2367DA6BEFAC62A797AFE3386ABD5 (void);
// 0x0000044C System.Void Mirror.TelepathyTransport::ClientDisconnect()
extern void TelepathyTransport_ClientDisconnect_m0A27B27ECCDAF5C60DC6777E95D6BA075FCEA4AC (void);
// 0x0000044D System.Void Mirror.TelepathyTransport::ClientEarlyUpdate()
extern void TelepathyTransport_ClientEarlyUpdate_m03B06C08025B8191282BEE1F6A9EC25C0EE47D34 (void);
// 0x0000044E System.Uri Mirror.TelepathyTransport::ServerUri()
extern void TelepathyTransport_ServerUri_mD3896EBAB74A7FE4BE2970ACB5B8930A1C064D0B (void);
// 0x0000044F System.Boolean Mirror.TelepathyTransport::ServerActive()
extern void TelepathyTransport_ServerActive_m7E31D881004FB4A68CE8E0C7F5C2D3C18ED9909E (void);
// 0x00000450 System.Void Mirror.TelepathyTransport::ServerStart()
extern void TelepathyTransport_ServerStart_m6F3609E2583811D26FB3ED5DF916A40EF848F84D (void);
// 0x00000451 System.Void Mirror.TelepathyTransport::ServerSend(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void TelepathyTransport_ServerSend_m52FD3C622C1459DE5492FF5383A0BD7B6EF3909B (void);
// 0x00000452 System.Void Mirror.TelepathyTransport::ServerDisconnect(System.Int32)
extern void TelepathyTransport_ServerDisconnect_m4D145DFAB3459FCD5FA07DB9B023E3783CF9394C (void);
// 0x00000453 System.String Mirror.TelepathyTransport::ServerGetClientAddress(System.Int32)
extern void TelepathyTransport_ServerGetClientAddress_mCAAE5E776410D1E8F8CB92D5AE547207F4F32EEE (void);
// 0x00000454 System.Void Mirror.TelepathyTransport::ServerStop()
extern void TelepathyTransport_ServerStop_m029E82E1FC85B297A37C856ACBB2471C6410E2FB (void);
// 0x00000455 System.Void Mirror.TelepathyTransport::ServerEarlyUpdate()
extern void TelepathyTransport_ServerEarlyUpdate_m970EF7D651ED0FC5FD4898CEDC3878892DC820E0 (void);
// 0x00000456 System.Void Mirror.TelepathyTransport::Shutdown()
extern void TelepathyTransport_Shutdown_mA4F85A710C7C52BB890A74C4942EC7BA64F9FB40 (void);
// 0x00000457 System.Int32 Mirror.TelepathyTransport::GetMaxPacketSize(System.Int32)
extern void TelepathyTransport_GetMaxPacketSize_m81DCD6F50C02F566D328EB5097BEE7081C544A2B (void);
// 0x00000458 System.String Mirror.TelepathyTransport::ToString()
extern void TelepathyTransport_ToString_m528636DD99B5B7A9346BC0B4E628E3C07CAA954C (void);
// 0x00000459 System.Void Mirror.TelepathyTransport::.ctor()
extern void TelepathyTransport__ctor_mA4061E623CDE35A23989AE27570AB68007F43F9D (void);
// 0x0000045A System.Boolean Mirror.TelepathyTransport::<Awake>b__16_0()
extern void TelepathyTransport_U3CAwakeU3Eb__16_0_mE8A62C170D1D06FC33DB1D05296F685B47563116 (void);
// 0x0000045B System.Void Mirror.TelepathyTransport::<CreateClient>b__18_0()
extern void TelepathyTransport_U3CCreateClientU3Eb__18_0_m2191BB18AFC53FAD8E79AB7DF6817986ADDE4091 (void);
// 0x0000045C System.Void Mirror.TelepathyTransport::<CreateClient>b__18_1(System.ArraySegment`1<System.Byte>)
extern void TelepathyTransport_U3CCreateClientU3Eb__18_1_m048D4A07C6DCDD9FF3B6CB39F01A4A43F0A553FE (void);
// 0x0000045D System.Void Mirror.TelepathyTransport::<CreateClient>b__18_2()
extern void TelepathyTransport_U3CCreateClientU3Eb__18_2_m90BB987518058CFF67D49CE28C6C9B38D07C364B (void);
// 0x0000045E System.Void Mirror.TelepathyTransport::<ServerStart>b__27_0(System.Int32)
extern void TelepathyTransport_U3CServerStartU3Eb__27_0_m4FA1834F3E693A569EB3C31B4F25513901F555B5 (void);
// 0x0000045F System.Void Mirror.TelepathyTransport::<ServerStart>b__27_1(System.Int32,System.ArraySegment`1<System.Byte>)
extern void TelepathyTransport_U3CServerStartU3Eb__27_1_mF9AA67BA5E56DE1BB322D9064283B455CC12E16C (void);
// 0x00000460 System.Void Mirror.TelepathyTransport::<ServerStart>b__27_2(System.Int32)
extern void TelepathyTransport_U3CServerStartU3Eb__27_2_m7F10B8399AA1AC077200DA03220BEAD54C01F930 (void);
// 0x00000461 System.Boolean Mirror.Transport::Available()
// 0x00000462 System.Boolean Mirror.Transport::ClientConnected()
// 0x00000463 System.Void Mirror.Transport::ClientConnect(System.String)
// 0x00000464 System.Void Mirror.Transport::ClientConnect(System.Uri)
extern void Transport_ClientConnect_mBA9A630061B0F73E6B0383A762693985C3AEEA6D (void);
// 0x00000465 System.Void Mirror.Transport::ClientSend(System.ArraySegment`1<System.Byte>,System.Int32)
// 0x00000466 System.Void Mirror.Transport::ClientDisconnect()
// 0x00000467 System.Uri Mirror.Transport::ServerUri()
// 0x00000468 System.Boolean Mirror.Transport::ServerActive()
// 0x00000469 System.Void Mirror.Transport::ServerStart()
// 0x0000046A System.Void Mirror.Transport::ServerSend(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
// 0x0000046B System.Void Mirror.Transport::ServerDisconnect(System.Int32)
// 0x0000046C System.String Mirror.Transport::ServerGetClientAddress(System.Int32)
// 0x0000046D System.Void Mirror.Transport::ServerStop()
// 0x0000046E System.Int32 Mirror.Transport::GetMaxPacketSize(System.Int32)
// 0x0000046F System.Int32 Mirror.Transport::GetBatchThreshold(System.Int32)
extern void Transport_GetBatchThreshold_mDCB4EF6DD7FB35DAF8E7A40E3DACE7484CE04ACA (void);
// 0x00000470 System.Void Mirror.Transport::Update()
extern void Transport_Update_mE9D7C9DCA9E54CA74C6D1B47C38834ABE4C7D796 (void);
// 0x00000471 System.Void Mirror.Transport::LateUpdate()
extern void Transport_LateUpdate_m458595D25E89D7222C3424D8A95AE08BF0D646FD (void);
// 0x00000472 System.Void Mirror.Transport::ClientEarlyUpdate()
extern void Transport_ClientEarlyUpdate_m0722DB6D5B6FC924657EAEC9A7E0DAE15497AEBE (void);
// 0x00000473 System.Void Mirror.Transport::ServerEarlyUpdate()
extern void Transport_ServerEarlyUpdate_mFD20DA364D3891A0C0F053D96FECF8E79E717D80 (void);
// 0x00000474 System.Void Mirror.Transport::ClientLateUpdate()
extern void Transport_ClientLateUpdate_mD6FF8477B2174BD8B8DB9EA6F2CB02BDD473A135 (void);
// 0x00000475 System.Void Mirror.Transport::ServerLateUpdate()
extern void Transport_ServerLateUpdate_m0FC3D74ECF4029F06B92999BB34025E58F34CC8C (void);
// 0x00000476 System.Void Mirror.Transport::Shutdown()
// 0x00000477 System.Void Mirror.Transport::OnApplicationQuit()
extern void Transport_OnApplicationQuit_mC51DBEB57BAB46E2B780F49420F5E205D79675BB (void);
// 0x00000478 System.Void Mirror.Transport::.ctor()
extern void Transport__ctor_m18499670B379DE249BA0A4D8D978335F0C1E0376 (void);
// 0x00000479 System.Void Mirror.Transport/<>c::.cctor()
extern void U3CU3Ec__cctor_m304817F15C080D3EEC191046CF1E9890B95CC53C (void);
// 0x0000047A System.Void Mirror.Transport/<>c::.ctor()
extern void U3CU3Ec__ctor_m5310ACAAE5FAD5B20FD9F10931FAB299C78950EC (void);
// 0x0000047B System.Void Mirror.Transport/<>c::<.ctor>b__32_0()
extern void U3CU3Ec_U3C_ctorU3Eb__32_0_mF7FDB3F2F0DFF66E559442E14A7D4464D0E14D79 (void);
// 0x0000047C System.Void Mirror.Transport/<>c::<.ctor>b__32_1(System.ArraySegment`1<System.Byte>,System.Int32)
extern void U3CU3Ec_U3C_ctorU3Eb__32_1_m54E5403E2D3851EA7C6A6D343300323AC7110927 (void);
// 0x0000047D System.Void Mirror.Transport/<>c::<.ctor>b__32_2(System.Exception)
extern void U3CU3Ec_U3C_ctorU3Eb__32_2_mEE5BBB94A91E911340820E2237772AFDD1872CD7 (void);
// 0x0000047E System.Void Mirror.Transport/<>c::<.ctor>b__32_3()
extern void U3CU3Ec_U3C_ctorU3Eb__32_3_mC6026CC78089FE13D779CC367A1D87FFCE82D604 (void);
// 0x0000047F System.Void Mirror.Transport/<>c::<.ctor>b__32_4(System.Int32)
extern void U3CU3Ec_U3C_ctorU3Eb__32_4_m195B635652A49A8FC937C05492F1610FAAC6F0BB (void);
// 0x00000480 System.Void Mirror.Transport/<>c::<.ctor>b__32_5(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void U3CU3Ec_U3C_ctorU3Eb__32_5_m76101E9DC27C90650C0BA52687E86B115CA542A2 (void);
// 0x00000481 System.Void Mirror.Transport/<>c::<.ctor>b__32_6(System.Int32,System.Exception)
extern void U3CU3Ec_U3C_ctorU3Eb__32_6_m324D78CD5F370CACCC239A8ACAB0E17767C01C1C (void);
// 0x00000482 System.Void Mirror.Transport/<>c::<.ctor>b__32_7(System.Int32)
extern void U3CU3Ec_U3C_ctorU3Eb__32_7_m3976B6943CBB65C428F9FE631793BB65EA539C8D (void);
// 0x00000483 System.Void Mirror.NetworkMessageDelegate::.ctor(System.Object,System.IntPtr)
extern void NetworkMessageDelegate__ctor_m8A66FF4EABB5ED3BEDBA1E5F5BDEF8582502FFEF (void);
// 0x00000484 System.Void Mirror.NetworkMessageDelegate::Invoke(Mirror.NetworkConnection,Mirror.NetworkReader,System.Int32)
extern void NetworkMessageDelegate_Invoke_mAEE9C9A0218BA47AA1BAED072D5042DB7C28E7F1 (void);
// 0x00000485 System.IAsyncResult Mirror.NetworkMessageDelegate::BeginInvoke(Mirror.NetworkConnection,Mirror.NetworkReader,System.Int32,System.AsyncCallback,System.Object)
extern void NetworkMessageDelegate_BeginInvoke_m72BB2CC861D8E6F765FF70B2EA5DDB947385613F (void);
// 0x00000486 System.Void Mirror.NetworkMessageDelegate::EndInvoke(System.IAsyncResult)
extern void NetworkMessageDelegate_EndInvoke_mE3EFA49B7DE1173FC104A1CCEEFCC0DE10114520 (void);
// 0x00000487 System.Void Mirror.SpawnDelegate::.ctor(System.Object,System.IntPtr)
extern void SpawnDelegate__ctor_m85296496F410FDA1A8F167B0E65E39B176491618 (void);
// 0x00000488 UnityEngine.GameObject Mirror.SpawnDelegate::Invoke(UnityEngine.Vector3,System.Guid)
extern void SpawnDelegate_Invoke_m647484B13C3CB741C8D7D910D355FEC904FBFBA3 (void);
// 0x00000489 System.IAsyncResult Mirror.SpawnDelegate::BeginInvoke(UnityEngine.Vector3,System.Guid,System.AsyncCallback,System.Object)
extern void SpawnDelegate_BeginInvoke_m6BDC7169A26F222C731A4CC93B5D2C1A9A5DCF7B (void);
// 0x0000048A UnityEngine.GameObject Mirror.SpawnDelegate::EndInvoke(System.IAsyncResult)
extern void SpawnDelegate_EndInvoke_m3DC601F0E76C04C3159289012B2B4AC40BDA0AB5 (void);
// 0x0000048B System.Void Mirror.SpawnHandlerDelegate::.ctor(System.Object,System.IntPtr)
extern void SpawnHandlerDelegate__ctor_m5D9B376DBB3C85A6B66ED69AB8B7E7010A3130C9 (void);
// 0x0000048C UnityEngine.GameObject Mirror.SpawnHandlerDelegate::Invoke(Mirror.SpawnMessage)
extern void SpawnHandlerDelegate_Invoke_m8DCF7DA5325363F4860AF5A135A7D9318A8C21D0 (void);
// 0x0000048D System.IAsyncResult Mirror.SpawnHandlerDelegate::BeginInvoke(Mirror.SpawnMessage,System.AsyncCallback,System.Object)
extern void SpawnHandlerDelegate_BeginInvoke_mA8C0FE11CC475B4F433E7CE81A0D91EDC68631C4 (void);
// 0x0000048E UnityEngine.GameObject Mirror.SpawnHandlerDelegate::EndInvoke(System.IAsyncResult)
extern void SpawnHandlerDelegate_EndInvoke_m20D6E4B8796890F64D0FDF48450469B8FF716965 (void);
// 0x0000048F System.Void Mirror.UnSpawnDelegate::.ctor(System.Object,System.IntPtr)
extern void UnSpawnDelegate__ctor_mB9B060274806A21287BA156858B167BDCC028AE1 (void);
// 0x00000490 System.Void Mirror.UnSpawnDelegate::Invoke(UnityEngine.GameObject)
extern void UnSpawnDelegate_Invoke_m253B17666AEEE399A94FF14D3B4FA5AFD7078D57 (void);
// 0x00000491 System.IAsyncResult Mirror.UnSpawnDelegate::BeginInvoke(UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern void UnSpawnDelegate_BeginInvoke_m281EC91CBC5162D34ACF38393B3881A800CE3058 (void);
// 0x00000492 System.Void Mirror.UnSpawnDelegate::EndInvoke(System.IAsyncResult)
extern void UnSpawnDelegate_EndInvoke_mECA5B4C0F9415708A4E4D354020E3F4F48927B57 (void);
// 0x00000493 System.UInt32 Mirror.Utils::GetTrueRandomUInt()
extern void Utils_GetTrueRandomUInt_m42E9457ACD0D68D5B81A8B127F0F5B3D68C8A60E (void);
// 0x00000494 System.Boolean Mirror.Utils::IsPrefab(UnityEngine.GameObject)
extern void Utils_IsPrefab_mF9AE489EFFD6D206956CB7C33F27A6515578F502 (void);
// 0x00000495 System.Boolean Mirror.Utils::IsSceneObjectWithPrefabParent(UnityEngine.GameObject,UnityEngine.GameObject&)
extern void Utils_IsSceneObjectWithPrefabParent_mF68AE61BD662AC25BD9EA4DDA45CF00D5D5666BE (void);
// 0x00000496 System.Boolean Mirror.Utils::IsPointInScreen(UnityEngine.Vector2)
extern void Utils_IsPointInScreen_m9F2EB8E51CA512D11B8FAE3B122BB2C91A2B6DFB (void);
// 0x00000497 Mirror.NetworkIdentity Mirror.Utils::GetSpawnedInServerOrClient(System.UInt32)
extern void Utils_GetSpawnedInServerOrClient_mC3BDF9A39C1BE50284978EFDD7415EEB91C07C57 (void);
// 0x00000498 System.Void Mirror.RemoteCalls.RemoteCallDelegate::.ctor(System.Object,System.IntPtr)
extern void RemoteCallDelegate__ctor_m3F67716E53766A6A7E680E0C77F7B9D5F1F2D0E1 (void);
// 0x00000499 System.Void Mirror.RemoteCalls.RemoteCallDelegate::Invoke(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void RemoteCallDelegate_Invoke_m848344D03E6489904F3B3B459EDBC240D7CEBFF5 (void);
// 0x0000049A System.IAsyncResult Mirror.RemoteCalls.RemoteCallDelegate::BeginInvoke(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient,System.AsyncCallback,System.Object)
extern void RemoteCallDelegate_BeginInvoke_m34CD029992B84D41DE926CCE439D737E4B1B7F66 (void);
// 0x0000049B System.Void Mirror.RemoteCalls.RemoteCallDelegate::EndInvoke(System.IAsyncResult)
extern void RemoteCallDelegate_EndInvoke_m1DA48390145766F9235DBC67382C7DDA05941673 (void);
// 0x0000049C System.Boolean Mirror.RemoteCalls.Invoker::AreEqual(System.Type,Mirror.RemoteCalls.RemoteCallType,Mirror.RemoteCalls.RemoteCallDelegate)
extern void Invoker_AreEqual_m00338067ECE838CD927419F73EE365A9CECB7C47 (void);
// 0x0000049D System.Void Mirror.RemoteCalls.Invoker::.ctor()
extern void Invoker__ctor_mAB4C1E969716FCF8DB4257BCF45F152446555DF4 (void);
// 0x0000049E System.Boolean Mirror.RemoteCalls.RemoteProcedureCalls::CheckIfDelegateExists(System.Type,Mirror.RemoteCalls.RemoteCallType,Mirror.RemoteCalls.RemoteCallDelegate,System.Int32)
extern void RemoteProcedureCalls_CheckIfDelegateExists_mD2EB732C127114EEA5BB6B8905A6E64EE0214EC7 (void);
// 0x0000049F System.Int32 Mirror.RemoteCalls.RemoteProcedureCalls::RegisterDelegate(System.Type,System.String,Mirror.RemoteCalls.RemoteCallType,Mirror.RemoteCalls.RemoteCallDelegate,System.Boolean)
extern void RemoteProcedureCalls_RegisterDelegate_m90390C79C2172ECECDF9E7E88BF19D9BF8E0C4EA (void);
// 0x000004A0 System.Void Mirror.RemoteCalls.RemoteProcedureCalls::RegisterCommand(System.Type,System.String,Mirror.RemoteCalls.RemoteCallDelegate,System.Boolean)
extern void RemoteProcedureCalls_RegisterCommand_m3FBF93ACE360E2DE9C986F943698A29A916C3143 (void);
// 0x000004A1 System.Void Mirror.RemoteCalls.RemoteProcedureCalls::RegisterRpc(System.Type,System.String,Mirror.RemoteCalls.RemoteCallDelegate)
extern void RemoteProcedureCalls_RegisterRpc_mD6EE0378898117C330E0D463F1BDA5A7A3F6FD19 (void);
// 0x000004A2 System.Void Mirror.RemoteCalls.RemoteProcedureCalls::RemoveDelegate(System.Int32)
extern void RemoteProcedureCalls_RemoveDelegate_m1A927E7F25452473796651139255BEFA578A0F92 (void);
// 0x000004A3 System.Boolean Mirror.RemoteCalls.RemoteProcedureCalls::GetInvokerForHash(System.Int32,Mirror.RemoteCalls.RemoteCallType,Mirror.RemoteCalls.Invoker&)
extern void RemoteProcedureCalls_GetInvokerForHash_m24BF900C2F8908F325BC490BEC0E74EED1514A4C (void);
// 0x000004A4 System.Boolean Mirror.RemoteCalls.RemoteProcedureCalls::Invoke(System.Int32,Mirror.RemoteCalls.RemoteCallType,Mirror.NetworkReader,Mirror.NetworkBehaviour,Mirror.NetworkConnectionToClient)
extern void RemoteProcedureCalls_Invoke_m6F5DD31CF8DE5CED460EE534BA481C2EE8954B40 (void);
// 0x000004A5 System.Boolean Mirror.RemoteCalls.RemoteProcedureCalls::CommandRequiresAuthority(System.Int32)
extern void RemoteProcedureCalls_CommandRequiresAuthority_m2E4FC4E955F7210D16B72C1C878E4EB2C6BCA899 (void);
// 0x000004A6 Mirror.RemoteCalls.RemoteCallDelegate Mirror.RemoteCalls.RemoteProcedureCalls::GetDelegate(System.Int32)
extern void RemoteProcedureCalls_GetDelegate_mD574382502F91E9E4263ADBC334B3AD1A4F76BFB (void);
// 0x000004A7 System.Void Mirror.RemoteCalls.RemoteProcedureCalls::.cctor()
extern void RemoteProcedureCalls__cctor_m1B17F0ABAC30E2C7BAB0CDA5819611D8F42BA244 (void);
// 0x000004A8 Mirror.ReadyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ReadyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ReadyMessage_m4E344D67949293187D126E9DB4B66E68BBFBAEE1 (void);
// 0x000004A9 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ReadyMessage(Mirror.NetworkWriter,Mirror.ReadyMessage)
extern void GeneratedNetworkCode__Write_Mirror_ReadyMessage_mB14840AF0A29896AD4CB80BB6D2D14690EAB90C4 (void);
// 0x000004AA Mirror.NotReadyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NotReadyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NotReadyMessage_m73EBD81A27C74BC3C248C6E040547A02CC284741 (void);
// 0x000004AB System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NotReadyMessage(Mirror.NetworkWriter,Mirror.NotReadyMessage)
extern void GeneratedNetworkCode__Write_Mirror_NotReadyMessage_mFFD85ECAD655A6A6428AB2C3257A298BF82D32D3 (void);
// 0x000004AC Mirror.AddPlayerMessage Mirror.GeneratedNetworkCode::_Read_Mirror.AddPlayerMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_AddPlayerMessage_m516FA38D7454305B8A27E0E53C232B46C1342E3A (void);
// 0x000004AD System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.AddPlayerMessage(Mirror.NetworkWriter,Mirror.AddPlayerMessage)
extern void GeneratedNetworkCode__Write_Mirror_AddPlayerMessage_m16236FB7277472EC7BAFBF723746B6AB7B5165BA (void);
// 0x000004AE Mirror.SceneMessage Mirror.GeneratedNetworkCode::_Read_Mirror.SceneMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SceneMessage_m5EF5F08305A5ED7336A03141426864B77FB60355 (void);
// 0x000004AF Mirror.SceneOperation Mirror.GeneratedNetworkCode::_Read_Mirror.SceneOperation(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SceneOperation_m6593FF68AA6E260ECEEDFBCF700D58D37BFF3B03 (void);
// 0x000004B0 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SceneMessage(Mirror.NetworkWriter,Mirror.SceneMessage)
extern void GeneratedNetworkCode__Write_Mirror_SceneMessage_m7751BC2A52EDA5209FE4DEE0712F72D9191F48D0 (void);
// 0x000004B1 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SceneOperation(Mirror.NetworkWriter,Mirror.SceneOperation)
extern void GeneratedNetworkCode__Write_Mirror_SceneOperation_m82391DF96778526A6DADB740698AD1D6292CF34A (void);
// 0x000004B2 Mirror.CommandMessage Mirror.GeneratedNetworkCode::_Read_Mirror.CommandMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_CommandMessage_m3D9DFA26219BADC3CA6DE371E3A20CD300638323 (void);
// 0x000004B3 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.CommandMessage(Mirror.NetworkWriter,Mirror.CommandMessage)
extern void GeneratedNetworkCode__Write_Mirror_CommandMessage_m32DB2DBCF1B07F22B92C66EE7539A2A0346C95BF (void);
// 0x000004B4 Mirror.RpcMessage Mirror.GeneratedNetworkCode::_Read_Mirror.RpcMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_RpcMessage_m14C0E321ED76E0EFFECE15BCE81F1605D6B892B8 (void);
// 0x000004B5 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.RpcMessage(Mirror.NetworkWriter,Mirror.RpcMessage)
extern void GeneratedNetworkCode__Write_Mirror_RpcMessage_m22BC6E711B230E2FCA1302CEB10175E90ACD68A1 (void);
// 0x000004B6 Mirror.SpawnMessage Mirror.GeneratedNetworkCode::_Read_Mirror.SpawnMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_SpawnMessage_m6A8028F047F003A82AA07DC1806FDA7EE43E6C27 (void);
// 0x000004B7 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.SpawnMessage(Mirror.NetworkWriter,Mirror.SpawnMessage)
extern void GeneratedNetworkCode__Write_Mirror_SpawnMessage_mFA9126BE7E93B8D881849D7634B3634F441C47ED (void);
// 0x000004B8 Mirror.ChangeOwnerMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ChangeOwnerMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ChangeOwnerMessage_m006F4299A05059BC3BD38152233C279273ECDC36 (void);
// 0x000004B9 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ChangeOwnerMessage(Mirror.NetworkWriter,Mirror.ChangeOwnerMessage)
extern void GeneratedNetworkCode__Write_Mirror_ChangeOwnerMessage_m88E6F40CA2153521DE21DB9E4B56944A3F597D34 (void);
// 0x000004BA Mirror.ObjectSpawnStartedMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectSpawnStartedMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectSpawnStartedMessage_mE8AAD46EB6A6EE89451B41232D74E7D660F6E424 (void);
// 0x000004BB System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectSpawnStartedMessage(Mirror.NetworkWriter,Mirror.ObjectSpawnStartedMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectSpawnStartedMessage_m3FA1297055A6CCE3262B4CC120D0180FA5632F7C (void);
// 0x000004BC Mirror.ObjectSpawnFinishedMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectSpawnFinishedMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectSpawnFinishedMessage_m04FAB9718A074E27C587F366C03660BE3A8707C6 (void);
// 0x000004BD System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectSpawnFinishedMessage(Mirror.NetworkWriter,Mirror.ObjectSpawnFinishedMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectSpawnFinishedMessage_mF70A7E1F66927F844D6EC216369EDB2A7C4455A6 (void);
// 0x000004BE Mirror.ObjectDestroyMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectDestroyMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectDestroyMessage_m2536202895EE8BF458D5519D4C597E09AF18514D (void);
// 0x000004BF System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectDestroyMessage(Mirror.NetworkWriter,Mirror.ObjectDestroyMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectDestroyMessage_m893B1809CFF465913F49259B126D8F3825937C8D (void);
// 0x000004C0 Mirror.ObjectHideMessage Mirror.GeneratedNetworkCode::_Read_Mirror.ObjectHideMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_ObjectHideMessage_mE9D8BE2F8E134D20BF1A8C378D2C3E1405C13DC0 (void);
// 0x000004C1 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.ObjectHideMessage(Mirror.NetworkWriter,Mirror.ObjectHideMessage)
extern void GeneratedNetworkCode__Write_Mirror_ObjectHideMessage_m4CB8EDD46511E3F9EEEEB10C8C598ACFF80D4469 (void);
// 0x000004C2 Mirror.EntityStateMessage Mirror.GeneratedNetworkCode::_Read_Mirror.EntityStateMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_EntityStateMessage_m3B48D3F01B190BC96719E6E1A2AAD415A750CDA2 (void);
// 0x000004C3 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.EntityStateMessage(Mirror.NetworkWriter,Mirror.EntityStateMessage)
extern void GeneratedNetworkCode__Write_Mirror_EntityStateMessage_m04DC9C28FD64679F8C96254B158F44A5711DE1FC (void);
// 0x000004C4 Mirror.NetworkPingMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NetworkPingMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NetworkPingMessage_m79853AE41B6F173C64F24524475A2804CF04326D (void);
// 0x000004C5 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NetworkPingMessage(Mirror.NetworkWriter,Mirror.NetworkPingMessage)
extern void GeneratedNetworkCode__Write_Mirror_NetworkPingMessage_m8855D6C394B429D69304B07BF1E4744F6A1A7891 (void);
// 0x000004C6 Mirror.NetworkPongMessage Mirror.GeneratedNetworkCode::_Read_Mirror.NetworkPongMessage(Mirror.NetworkReader)
extern void GeneratedNetworkCode__Read_Mirror_NetworkPongMessage_m966BA422B175C070879D6DB963F33A8FAEDD65F9 (void);
// 0x000004C7 System.Void Mirror.GeneratedNetworkCode::_Write_Mirror.NetworkPongMessage(Mirror.NetworkWriter,Mirror.NetworkPongMessage)
extern void GeneratedNetworkCode__Write_Mirror_NetworkPongMessage_mAB8F03B31199677482F68FCC05458C2BB7CBCA04 (void);
// 0x000004C8 System.Void Mirror.GeneratedNetworkCode::InitReadWriters()
extern void GeneratedNetworkCode_InitReadWriters_mF874877C924678E4EC340DE14446855B70EBE1E5 (void);
static Il2CppMethodPointer s_methodPointers[1224] = 
{
	EmbeddedAttribute__ctor_mBAD8A67721F7598BE1151A9142ACE8229337284E,
	IsReadOnlyAttribute__ctor_m2EA1DDCC3375C83D2922C167CD0BA462DA85B421,
	IsUnmanagedAttribute__ctor_m674339F10E3980C7645B182B747EC74E4E3B0EDD,
	KcpTransport_FromKcpChannel_mB64EDD825967DF4381967DB2EC31A44AB52D6E37,
	KcpTransport_ToKcpChannel_m2570D7CD3090359062C0D6852A9820A70FC164DA,
	KcpTransport_Awake_m62B40AF0A488B3ECE15089856A16BE4623A51BEE,
	KcpTransport_OnValidate_mD3A91915F6D032B947493DE1E7F6DD52C3B033BF,
	KcpTransport_Available_mC28A19601D78A80E81F13231A059929D2E118C54,
	KcpTransport_ClientConnected_m4370351A8E8B89BABF8AC5BF2423F05BEC959358,
	KcpTransport_ClientConnect_mDB9736490D95A3F288721B88A33DC79FCEFB98DD,
	KcpTransport_ClientConnect_m5C61BED925B65A67E6F1F48EC651F42D1B3C138C,
	KcpTransport_ClientSend_m561B652876E5E71B9509A5675BE66B763DF07DD2,
	KcpTransport_ClientDisconnect_mADDC6439D972EC6CA801E57F55FE211B755093B1,
	KcpTransport_ClientEarlyUpdate_m4AE48F297CFB0B6ED62E6C89FF7AD5B5BE04B1CF,
	KcpTransport_ClientLateUpdate_mFCA6124C57C7836AE664951F986CCB11F3206D75,
	KcpTransport_ServerUri_mB32E80AE3973B31A562FA9BE8954A9EC833B3E2D,
	KcpTransport_ServerActive_mFFF858F3F362B80BFBB8DA38870901126AD33D0F,
	KcpTransport_ServerStart_mB19F14CED645C0AF8BEE683B2911105972CB4B17,
	KcpTransport_ServerSend_m3759F5AAAD385E594FBEBBA4DC5C220A7C21DF7F,
	KcpTransport_ServerDisconnect_mEEC7A07982EBC1EFCFA2B07E189DD6E0A7FEABDD,
	KcpTransport_ServerGetClientAddress_m0DC394B917B355C3FD4798EE0C0022B438188BCA,
	KcpTransport_ServerStop_m3EE5CD2DE9953E2EFE10720382DC9D5DC4F13E3C,
	KcpTransport_ServerEarlyUpdate_m79B9CC4F151CD8FCE544D2A8F549B54A049BCEEF,
	KcpTransport_ServerLateUpdate_m06B80D50F2CC1A33AC95FAE891E4E95F5B2DF97B,
	KcpTransport_Shutdown_m6CF63A2AC8B565B3411F462352420A5427A0A3BC,
	KcpTransport_GetMaxPacketSize_mEB9B1E5DE977F2A6225576122ECE6C505B8DC257,
	KcpTransport_GetBatchThreshold_mA0D07FCF893E5D932F4745CFB87262CBBACCCE91,
	KcpTransport_GetAverageMaxSendRate_mDBE93910418A930FB9D2E01BC11519CC227D42FE,
	KcpTransport_GetAverageMaxReceiveRate_m7E2FDCBA723BFEA986CC6C268E0F1BFC6F2D0834,
	KcpTransport_GetTotalSendQueue_mA939D84C1BA0CF741A8266D880E0D8D7A1FA7EFA,
	KcpTransport_GetTotalReceiveQueue_m60E09827BA5ACFF1BC3FC0EAC6ED2E05BF27948C,
	KcpTransport_GetTotalSendBuffer_m02B88C25F1D16A343D4F55DA6F8C2E526468BA03,
	KcpTransport_GetTotalReceiveBuffer_m90FB51580EAB4CB8C00C8D3074474EF70C5BB401,
	KcpTransport_PrettyBytes_m60CBBB4165C1D52DD392C9D6E4F7359F28345CA4,
	KcpTransport_OnLogStatistics_m6251A4F09FBD2348C24444E46A89E385F32D6693,
	KcpTransport_ToString_mAE9B5329A0035FC14BD3ECB6BFAA3E3B9C0911E5,
	KcpTransport__ctor_m0CF4CC09A52CDE5F1CB9A576C0650B0FA7A21075,
	KcpTransport_U3CAwakeU3Eb__22_1_m4160E26B7C11B7BD8713CCB11E77D94BE8741BB4,
	KcpTransport_U3CAwakeU3Eb__22_2_m0A99F222B220E478453B6E21AFC8AA70BF6B00B7,
	KcpTransport_U3CAwakeU3Eb__22_3_mE2B91D22FE19CD7AB46E4F14044FB9AE5EB81F92,
	KcpTransport_U3CAwakeU3Eb__22_4_m3BBA4A0DA66024030F80A1230DA6FAA6D1A52E82,
	KcpTransport_U3CAwakeU3Eb__22_5_mD4B2EB78505906D0B36C782D73C07117A6DB6A7C,
	KcpTransport_U3CAwakeU3Eb__22_6_m1E6D5CF6B0D536FF63427CC1D9A5D4111BC00037,
	KcpTransport_U3CAwakeU3Eb__22_7_mAC8BB1D9022778C0E4AECEC7B7069EAF1E191C8C,
	KcpTransport_U3CAwakeU3Eb__22_8_mA2493E67FBA0274A3DF39257B8968EB2A1782C4D,
	KcpTransport_U3CAwakeU3Eb__22_9_m3718FC146E7392A34DCA376A4CC907EE79107268,
	KcpTransport_U3CAwakeU3Eb__22_10_m7CB524232C17580E7298C82025B29923A8390BD8,
	KcpTransport_U3CAwakeU3Eb__22_11_m10FF31C37971098EE568E7C10F124EAD479169E4,
	KcpTransport_U3CAwakeU3Eb__22_12_mE8B84593620E6985DEAF203E6D772C838237B1B0,
	U3CU3Ec__cctor_m74DDBDACB2E47803B0ECB09111C9BCD8E1FBC2FF,
	U3CU3Ec__ctor_m198B95BF0E13C9FF9E4CF6A86E7F21801E5234E0,
	U3CU3Ec_U3CAwakeU3Eb__22_0_mED3F2C0F388BE01226F6CAD5C450E4A03EFA7CD0,
	U3CU3Ec_U3CGetAverageMaxSendRateU3Eb__44_0_m670B20C66040756AA43B5A2DA26A525DA0E3CE8E,
	U3CU3Ec_U3CGetAverageMaxReceiveRateU3Eb__45_0_mACB59A3EB68421543A8950069A9FE511D6B267F5,
	U3CU3Ec_U3CGetTotalSendQueueU3Eb__46_0_mB0E14CFFAE3F3AF53A176175F1147A390FF6E62D,
	U3CU3Ec_U3CGetTotalReceiveQueueU3Eb__47_0_m200BD3BD374C50D7097269491FBCC80E8A0FBF10,
	U3CU3Ec_U3CGetTotalSendBufferU3Eb__48_0_m7DAD5A18EDF682BB34E55F3459DCC4F8081A1A2B,
	U3CU3Ec_U3CGetTotalReceiveBufferU3Eb__49_0_mD64AE0406D1DD066CA9C742C80E57C4495645292,
	SyncVarAttribute__ctor_m4E99C2B45DDA311F1D9B684B967978541348C875,
	CommandAttribute__ctor_m583940E69A6305050FEF54E57CBAF339A8E77FFC,
	ClientRpcAttribute__ctor_mCE8A483D94F06D1FA29EA2342BE860018B29BCA7,
	TargetRpcAttribute__ctor_m5954E4349796FC0A7AA8BE4857791C38E867CA8C,
	ServerAttribute__ctor_m413F135143ED76C9B8C3F41B2DB6597E4382E0A6,
	ServerCallbackAttribute__ctor_m7258C253CBA5BF5C92BAAB6345240F9418C0B61D,
	ClientAttribute__ctor_mD3E93370918D30F063F1CBCA1CF1BC46B90D9065,
	ClientCallbackAttribute__ctor_m1CCA6B90CA8E84DD7A6C6AC9C862434AA9560703,
	SceneAttribute__ctor_m7F8095E94F9D74BDB458EE8B5CF10781C8EEEB62,
	ShowInInspectorAttribute__ctor_mFA3EF79FD7E9F573D31F156DE62A6CBDBE254688,
	Batcher__ctor_m09EE7AAA6B64C24CC33BDA106A986C6EE2FDE530,
	Batcher_AddMessage_m7A8979D02D56CFBDE123E1D1736937E42F8ACFFC,
	Batcher_MakeNextBatch_m7E84834E9BA080217B7EABE42EEA34448B2DFDB2,
	Unbatcher_get_BatchesCount_m94ED4CD606FD0612C11C0BDA995E14684BCB3892,
	Unbatcher_StartReadingBatch_m7609F8C4E60E92DAF00646BBE3897A3028CAC1B5,
	Unbatcher_AddBatch_mF699FFD9FDCFE1CF312EC5A1DBA87B95886BABF9,
	Unbatcher_GetNextMessage_m0A09E7F86E5677699469D0C2782DB81ADCEF836B,
	Unbatcher__ctor_m6374EC7D7E8440A13444BD921C524C49227B43B0,
	Compression_LargestAbsoluteComponentIndex_mB176E3E544821911CE8E10C6E766E73063A18F8F,
	Compression_ScaleFloatToUShort_mAFB60A1D68E5F3B953184D66C1DE4A57E9D21A3F,
	Compression_ScaleUShortToFloat_mAB1609B5E7B4C38EAA298BBF3249C06A272B5865,
	Compression_QuaternionElement_m60537C4CF1CA1B44907E0F6D49F7A88E6CF4E604,
	Compression_CompressQuaternion_mC1DD43E3FA152477293A562CA2713DAC89B89CFE,
	Compression_QuaternionNormalizeSafe_mD1517F3E0392C60CCEF02EAC2857A64312F83F09,
	Compression_DecompressQuaternion_m235BB50DA4C0504C27545528ED733C1E0CD246EC,
	Compression_CompressVarUInt_m3D7658FE25B3349CADD478CF0527A78A4D852731,
	Compression_CompressVarInt_m7B1A3B99D742DA9BD7A53463E04DA4D2BC8E85BB,
	Compression_DecompressVarUInt_m3A4757642159D857F4A7CE4BCFFFA9DBE60A71D7,
	Compression_DecompressVarInt_mF00C15FC25DD9D9CD5DAAA63C8E55305049AA9F9,
	ExponentialMovingAverage_get_Value_mB278B5333872C5EAEAF519E1F95BF4B437F1A00E,
	ExponentialMovingAverage_set_Value_m7839F5E48C1B64C159F98A9D14213DD5C390EEA7,
	ExponentialMovingAverage_get_Var_m9ED5A56A0D2B778547F20E4D762562F0F927D8BD,
	ExponentialMovingAverage_set_Var_m58941EF0646BC5D3EF2CD0EDCAC7C6D539AF0D54,
	ExponentialMovingAverage__ctor_m3EB10AAA23643AF85E68D21E0EEDE15219287268,
	ExponentialMovingAverage_Add_mC19B600AC4A4ABB827290B157B0A830103D54F11,
	Extensions_GetStableHashCode_m0592E21267B4D12BAF6A3124018FFDB85AA1EDB9,
	Extensions_GetMethodName_mBD201B7767E472A8E4F2116F18AD20D437783E0F,
	NULL,
	InterestManagement_Awake_m0020EDD8ABA4470A17089BC2A660147FA087F3D9,
	InterestManagement_Reset_mA592DDE988AB634D50B583EBF5FABF427C1C0F07,
	NULL,
	NULL,
	InterestManagement_RebuildAll_m143CB3DE5E99AF95FA1285E5E2D91E5FE2DEA1AD,
	InterestManagement_SetHostVisibility_m17A390210930533FAEB21EE83C4EF32675DBE8F9,
	InterestManagement_OnSpawned_m0D03EF9591FEB7D27C28D5BB9B8A0C43BF9F7262,
	InterestManagement_OnDestroyed_m780A052B4A97A1C03EBA0A0F2780680D5C74CEA1,
	InterestManagement__ctor_mC2CB6AFC383E02ACBABDD728A970658A1C3CCFF4,
	LocalConnectionToClient__ctor_m428D1E3E51E34E982DF40E0DCE961195AA003337,
	LocalConnectionToClient_get_address_m2D7FB00302E2915434C1E5DB9797D4A5A4E33059,
	LocalConnectionToClient_Send_m2D2251FEA3914828444ED06315BFBE2643AF019E,
	LocalConnectionToClient_IsAlive_m7A9507EB1C35012CFF5DB54FC6B30C58232A73D6,
	LocalConnectionToClient_DisconnectInternal_m703C5C7C8D1B2C4C30BF993266497288D47016C2,
	LocalConnectionToClient_Disconnect_m62485E44133767987CFE31DC010CE7281AE84A92,
	LocalConnectionToServer_get_address_m0C6B7946CCE095F5AD805DBD421620B031AA6953,
	LocalConnectionToServer_QueueConnectedEvent_m5248D0F3CB749C8DAB1C8D2F8067D0C4291A03BB,
	LocalConnectionToServer_QueueDisconnectedEvent_m5199F8C95A1A01F7367E0C1B32B2B8E08D673168,
	LocalConnectionToServer_Send_m7101806705A74ECC702860F1DA86A8E61A3FCC7E,
	LocalConnectionToServer_Update_mD992865B7AB76B7164B41CE40EB7ABBD978805D8,
	LocalConnectionToServer_DisconnectInternal_m9F0657A0BEBDCCA743A4F3BBCF6AA4E624A9B89B,
	LocalConnectionToServer_Disconnect_m702169BB2063D9E348421A2EB47E83FF04D31E4D,
	LocalConnectionToServer_IsAlive_m249A5454FEE2D49C444B5D2F181EBB9E773E906D,
	LocalConnectionToServer__ctor_m958A380BF9B110F4AD9C701DE3E61583FE6D58AA,
	Mathd_LerpUnclamped_m13CC466F1D9A8E1A3A5DC71D67E5D77893300F12,
	Mathd_Clamp01_mC0705D2095AADDFCA9535C68AAA71737869470E8,
	Mathd_InverseLerp_m5EE751E947817D17D845E932A91C5EA5A109DC24,
	MessagePacking_get_MaxContentSize_m661410D0975A474E1CBF705D2FB155C2DE4BEB90,
	NULL,
	NULL,
	MessagePacking_Unpack_m96DB2B52AAB7CB3F845AE3D940630217DFED1543,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkPingMessage__ctor_mF340573CD5E74C9BECF60F7789C8170559A95C5D,
	UnityEventNetworkConnection__ctor_m2E143132E593982075D6CFB3ACE9FC22A2E66F55,
	NetworkAuthenticator_OnStartServer_m846E62361157BE3A9A6280B0314DA5D22BDF11B8,
	NetworkAuthenticator_OnStopServer_m41CE4A338AE0CA96A3BAB3034A5A56A35A4BF72A,
	NetworkAuthenticator_OnServerAuthenticate_mEBF420F94B6BE8065D4712B6DEC43BA0F51D8B4B,
	NetworkAuthenticator_ServerAccept_m221411BDD939B7B9832825EB80CA3F44A5A44D14,
	NetworkAuthenticator_ServerReject_mE1AAE10EB8217BE0E7F81F529A1C12804D37C96E,
	NetworkAuthenticator_OnStartClient_m5C32E06D0A5DD87660C6B25EDD8FBDB30D9BC644,
	NetworkAuthenticator_OnStopClient_m1679E3E9ECFA23353E52BABF80DB494ADE6B5B62,
	NetworkAuthenticator_OnClientAuthenticate_mC74560EC6347743730397A477A8B2639A9D9BF28,
	NetworkAuthenticator_ClientAccept_m8B68015CCD5DF4A222DB0222EDF3B9A3CD2862E1,
	NetworkAuthenticator_ClientReject_m923193A3EAFC73EBA13502B73FE1EAB5FA5FC730,
	NetworkAuthenticator_Reset_m595DBEB9E798D750D98554E1850D5931558B3FCD,
	NetworkAuthenticator__ctor_m1EFE9D58997FA1E95FE79D6196AEBDE5CEC414E5,
	NetworkBehaviour_get_isServer_m6CF3499812C1F2679BB924165AA79C59E6D2EBCF,
	NetworkBehaviour_get_isClient_m87FF41CC03AD448B05627F3711F8E27C63D5C615,
	NetworkBehaviour_get_isLocalPlayer_mFA35EE97B42DEEE92E4FD5562C8C6A1717607DE0,
	NetworkBehaviour_get_isServerOnly_m6247E79FF74BBEC947D4D605095A8A24E5E16FD8,
	NetworkBehaviour_get_isClientOnly_m1082A236ADF41204D352DAEE63F652244950C3B1,
	NetworkBehaviour_get_hasAuthority_m8C249EBFD9F083DE67F38A74E3245727C6035167,
	NetworkBehaviour_get_netId_mFD41F9D183B23443AA528BC0244E0835CCC94826,
	NetworkBehaviour_get_connectionToServer_m269C003EE53BFA1EE6A2DBA16A975723C8829421,
	NetworkBehaviour_get_connectionToClient_m76BDC3A8CB43E9A9434569A7C0CA2EF6A428EC77,
	NetworkBehaviour_HasSyncObjects_mDD3B1CE94285E413CA1862D945D74156D6510D18,
	NetworkBehaviour_get_netIdentity_m67CAF485E29AFA1CB0540A7ADBE68FCE2326151D,
	NetworkBehaviour_set_netIdentity_m4FB00584A78A461AC087E6A31F8A57AE40E05BC0,
	NetworkBehaviour_get_ComponentIndex_mACCD123A66C72A3D062535CF936618C2158E9D76,
	NetworkBehaviour_set_ComponentIndex_mC14387FE3B3E56D945F6B126E08AD618443368DE,
	NetworkBehaviour_get_syncVarDirtyBits_m1690C7716CF78A4CC6360088FB682385EE76C9E3,
	NetworkBehaviour_set_syncVarDirtyBits_m1FA90B23C70060A48A39E1284A9516B24F996824,
	NetworkBehaviour_GetSyncVarHookGuard_m69E27CD31431FD5BBDDA23D06E99E18CF7E2590A,
	NetworkBehaviour_getSyncVarHookGuard_m04C7E518D055E786F2EDD648D995543282812D09,
	NetworkBehaviour_SetSyncVarHookGuard_mF38BC0B793D2A923C63A3A17CA9CA704B5588756,
	NetworkBehaviour_setSyncVarHookGuard_m44C276207929797CD147789B1E2D5EFF0E8D0B98,
	NetworkBehaviour_SetSyncVarDirtyBit_mBE9A36805B048D3150A08DFFEC6ADC9CE0239457,
	NetworkBehaviour_SetDirtyBit_mDE4C6DE9ECC6E882CBDC4BF66BB65CC3B92A49E5,
	NetworkBehaviour_IsDirty_m50CC8C762BF12BBF3D796B0266BBF1E0D90BA1EA,
	NetworkBehaviour_ClearAllDirtyBits_mD37CF3E0D4864DA51521989A6D3F4D012BC5E3BE,
	NetworkBehaviour_InitSyncObject_m627E07D5152DFE01AC855565E09043BA70692CDF,
	NetworkBehaviour_SendCommandInternal_mEC82E2864E0EFF7FE5D8B1B92DC9151B5AA61220,
	NetworkBehaviour_SendRPCInternal_m1F6F594A7FBDC846223DAD77E2F8362C05DB18B2,
	NetworkBehaviour_SendTargetRPCInternal_m3F79098819821955796385EC5013BDC83652121F,
	NULL,
	NetworkBehaviour_GeneratedSyncVarSetter_GameObject_mFE124DB3DDEED7ADD0811521B936716AF3946B85,
	NetworkBehaviour_GeneratedSyncVarSetter_NetworkIdentity_m96D20403FEA8419277C8CD4AAD85D460C3A7C47A,
	NULL,
	NetworkBehaviour_SyncVarGameObjectEqual_m538EA781DEC2D9D0DF307035D039850460DC5690,
	NetworkBehaviour_SetSyncVarGameObject_m58A39D6DE471F63603EDDA893781EACE528246AF,
	NetworkBehaviour_GetSyncVarGameObject_mBD2A0B361900E77AF101885A491B0921FDA4AE3F,
	NetworkBehaviour_SyncVarNetworkIdentityEqual_mBF505C08B55B2AA9120AE69F365A0C1D7EC092E2,
	NULL,
	NetworkBehaviour_GeneratedSyncVarDeserialize_GameObject_m8FA704F8E268520F840C8026BD1365E2B1294840,
	NetworkBehaviour_GeneratedSyncVarDeserialize_NetworkIdentity_m992D1F385125B3D39924FC712A450093DCE28ACC,
	NULL,
	NetworkBehaviour_SetSyncVarNetworkIdentity_m662595FF753AE6B0E87DBC7E72517250A4244339,
	NetworkBehaviour_GetSyncVarNetworkIdentity_m7C2CF1934178FA42C8D93CD008F4E174FD944927,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkBehaviour_OnSerialize_mBE2870081075933577F25C4760A3758FC5A73391,
	NetworkBehaviour_OnDeserialize_mDA83841054FF1970D8C87C246CB56F426195768A,
	NetworkBehaviour_SerializeSyncVars_m4AF8BE097726BA018E3762C14E33BDBF3A5C4819,
	NetworkBehaviour_DeserializeSyncVars_mC87ECFD6F9C2D5C1306ABDF2CF6498864C84677A,
	NetworkBehaviour_SerializeObjectsAll_mE77BC9DA6B1BA7417C5FF4AA5CB0F31F1882ABF9,
	NetworkBehaviour_SerializeObjectsDelta_mF7FA7AF00832E10CB3A3A9E9E18A9DFEF315C8FF,
	NetworkBehaviour_DeSerializeObjectsAll_m6597B6F683F3DCD0FF34B30AE24A95B5CD004C4F,
	NetworkBehaviour_DeSerializeObjectsDelta_m33229FDA09E4E1EB32F3432C26DFF174563D57CA,
	NetworkBehaviour_ResetSyncObjects_mCC577FB1959F7BE96220152489E55AD993C98D14,
	NetworkBehaviour_OnStartServer_m7E498D9B74008DCD512C4A01C847795893A5ED2D,
	NetworkBehaviour_OnStopServer_m3553611E0C2705734BD005AF6396A6A7C4994EF1,
	NetworkBehaviour_OnStartClient_mBE0F27A3CDF5C4F76C777885B2D21AB60A9647E8,
	NetworkBehaviour_OnStopClient_m6D76DD7B8DC4A4E29D1F58DD69CA6C84E0CC9B81,
	NetworkBehaviour_OnStartLocalPlayer_m884B22DB458ECACC0E00809576CC2DD2F41DE2B4,
	NetworkBehaviour_OnStopLocalPlayer_mACF50DFFD20E147C9616BB416F0ECBDF236E4B21,
	NetworkBehaviour_OnStartAuthority_mC224FDE4601A24F19B2B4A085010764783BB2CED,
	NetworkBehaviour_OnStopAuthority_m3040A6FF682924E89E26C06E7B629D024D87D376,
	NetworkBehaviour__ctor_mB98FF8F52DCEBEB3BC7679DE03FA50785207EE78,
	NetworkBehaviourSyncVar__ctor_mFB219E84A8139BFFCC899543F4057E03D963B0FF,
	NetworkBehaviourSyncVar_Equals_m2EE0BD631368A1EA28D3E4422A9F14C43B9B0A27,
	NetworkBehaviourSyncVar_Equals_m2EE82223A7405A371A233BCED2C78292E69AA85F,
	NetworkBehaviourSyncVar_ToString_m4DE9A94D17F9F28FCDB963AC8665144C37076363,
	U3CU3Ec__DisplayClass45_0__ctor_m962006B4E384E919173C557496E93594671CB77D,
	U3CU3Ec__DisplayClass45_0_U3CInitSyncObjectU3Eb__0_m5C6E3A0D209D6E49E000D11013E633990795C5E5,
	U3CU3Ec__DisplayClass45_0_U3CInitSyncObjectU3Eb__1_m136A4E517A3C9D631B9CD6EF8E537B3D110B3719,
	NetworkClient_get_connection_m5439CD4BADA80C781B783F013464517F581F9557,
	NetworkClient_set_connection_mED66882874FA7C5891570DFDC873B4EC5B678AFC,
	NetworkClient_get_localPlayer_mFA6B51032C92C1B18EAA3B7FCD0369A089A0020C,
	NetworkClient_set_localPlayer_mCBB4FBB1B611801ABEB2B2BBD6A0FD03365D534F,
	NetworkClient_get_serverIp_m3952EAE51FB2B2067755FDFFE7481BB95A4E146B,
	NetworkClient_get_active_m80C7ACA728DE7F6F6B5DC6C0E80B5AD0D287EA37,
	NetworkClient_get_isConnecting_mEFCFA8E2F8F117CED0643652F62F0683B2350C72,
	NetworkClient_get_isConnected_m58D71078B898D6DA731CDC3D4EDA737E7B04A4E6,
	NetworkClient_get_isHostClient_m1A44178EF65BA31D2F9E5469FD739270E1DC18CA,
	NetworkClient_AddTransportHandlers_mF0A64666B0628413F9F37D72B169C8A7C9DBC920,
	NetworkClient_RegisterSystemHandlers_m5BF1C0E926656B42C70C67A146B65FFF8378AB55,
	NetworkClient_Connect_m1012B6CB8305CF83DE993CB961DCBAC2E5366489,
	NetworkClient_Connect_mD685E75AAE9CF8F6765C91640C84437777546925,
	NetworkClient_ConnectHost_m2359D0E1763DEE430B6920B2C7B4D67A270927C9,
	NetworkClient_ConnectLocalServer_mCCDFBA08C7B898756DF7812127468B9F68966C7E,
	NetworkClient_Disconnect_mD13276168C810E45CA0ED75ABDC3DBF7CBC2F439,
	NetworkClient_OnTransportConnected_m1B39F7AAF7474EC0246ABE6CCEC5BB00BC91EDE4,
	NetworkClient_UnpackAndInvoke_m6F5034DE78530E962B29E497844B81AF99E889FF,
	NetworkClient_OnTransportData_m424C221EE24669E9D8F3A6B1B8B126453B733B67,
	NetworkClient_OnTransportDisconnected_mB728EAB36784C12FE25C8DBF472A733A59C9A207,
	NetworkClient_OnError_m3CBE84BB09E2B3B7D4741ECA4F9F2DDAAF5184A1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkClient_GetPrefab_mB24435E12D19697A887BFADE8F8B3D586742999E,
	NetworkClient_RegisterPrefabIdentity_m70F93AE7E4C64AA218627822BB68FFA632E40238,
	NetworkClient_RegisterPrefab_mDDF91101ACDA56F5CF4F69D165E33040C587BA80,
	NetworkClient_RegisterPrefab_mD3E596F19CF1A608C524A4A95CEF56BE6EBC5FF4,
	NetworkClient_RegisterPrefab_mADEC9110EDEDFC7D2769EDFBC5AABE4BA782D24A,
	NetworkClient_RegisterPrefab_m83A8C1F431CC6E48040A642CB71B3B8E2ED91E2B,
	NetworkClient_RegisterPrefab_mFBD356840D4DD5EF716B650E3C09D5A6CA0A23BB,
	NetworkClient_RegisterPrefab_mA11E07E698EDFC41138E2F9FD3A9CCD94ADAEF05,
	NetworkClient_UnregisterPrefab_m624DDE9ADA9681E06079404F2B9A80F9D16023E6,
	NetworkClient_RegisterSpawnHandler_m5246FEC89A9B3C563F54F2775BA6CFFC3BA00D9D,
	NetworkClient_RegisterSpawnHandler_m1391B1F148D5E508A90FCC69B39B4A3913A98AB9,
	NetworkClient_UnregisterSpawnHandler_mFCBC83634E281D816898E45D25C41337B3B69AAE,
	NetworkClient_ClearSpawners_m92F7ADAD3F0327103A44F3D132D9F89E60F917A7,
	NetworkClient_InvokeUnSpawnHandler_m303A55418A1FBA4403512BF5B0D7F037850999DB,
	NetworkClient_Ready_m0290F8ED5D23A2C8DC4DCEAE68295AAC9644B926,
	NetworkClient_InternalAddPlayer_m53CB34CF7951291A275A589AAB770D9668363B70,
	NetworkClient_AddPlayer_m0C6015BEBE42CDDEB5AE5C5F787108737505B107,
	NetworkClient_ApplySpawnPayload_mEB9CA6042E62F380DA7B3F49BAB4538448FF84FF,
	NetworkClient_FindOrSpawnObject_m73DBFF69AAEC9489D2B8AD54381DB5B39E35A189,
	NetworkClient_GetExistingObject_m8B4FB7CA960913040F4AD6B2082D6106A2E875E2,
	NetworkClient_SpawnPrefab_m31B1515BF10E16933F18CA5930B06BDD0EEF6595,
	NetworkClient_SpawnSceneObject_m9D4871EDCD945BF8DBFBC0C9411F7175A845BE20,
	NetworkClient_GetAndRemoveSceneObject_mF4E2C3C8F607D9848D5D449AAD77D90A6551CD42,
	NetworkClient_ConsiderForSpawning_m775C21D2195F17002BF0F7F60F0C052AAA6934B2,
	NetworkClient_PrepareToSpawnSceneObjects_m1F51AE31B33D48FD9DDBF1CEC08AA546AEB50D30,
	NetworkClient_OnObjectSpawnStarted_m2F1A04282BE3688D0750D225100B003AE5FE7658,
	NetworkClient_OnObjectSpawnFinished_m08BC661EAE49DB5C7955421AB8D43CBA1CD78B90,
	NetworkClient_ClearNullFromSpawned_m294A3CF498EC00F34F3E5D27E07E42DB6CF120A6,
	NetworkClient_OnHostClientObjectDestroy_m821445496B743A10A13697A519B1B7BE76D7154B,
	NetworkClient_OnHostClientObjectHide_m9C1D529B105CAD107D472163C75E52130C5CB8BB,
	NetworkClient_OnHostClientSpawn_m0B461537BFDC210A94C92142EF85F18E772FCAA5,
	NetworkClient_OnEntityStateMessage_mB2D5A9AA816B88D1CFECD9C52C65D428ADE3C475,
	NetworkClient_OnRPCMessage_mDB1DC6D93DA81BCFC6604686FBF20951BD087242,
	NetworkClient_OnObjectHide_mECE8726EE3F62C50CB3541A558AB0A478995D0E7,
	NetworkClient_OnObjectDestroy_mCD8DDAF1FD872969E841C2C7E9946E8FA5299766,
	NetworkClient_OnSpawn_m757BA45D182CA43B9FFBF7E03120358BC0836C98,
	NetworkClient_OnChangeOwner_m7735A531EC4EA93EDB59F3FE839B5030097AB364,
	NetworkClient_ChangeOwner_mE0005ED3EC2D4400854784AAF0E13B54B37D06D9,
	NetworkClient_CheckForLocalPlayer_m9AA764A2840876F38D3CC123AE637365A9A025C8,
	NetworkClient_DestroyObject_m58E70E859CF6E27F0018B060C2990757BFC5DEFC,
	NetworkClient_NetworkEarlyUpdate_m4BB991D8FD06E909BDE076B87895113E9A510F94,
	NetworkClient_NetworkLateUpdate_mD996BE78A2574AA44050056BAC9E3F7AC4FFDBDB,
	NetworkClient_DestroyAllClientObjects_m895C02874F863A82A1FDB4015F223CF4DDBAD324,
	NetworkClient_Shutdown_m362C444833B9302098E6CC73EFA59309FC7B3D4D,
	NetworkClient__cctor_m051C6F073FEAF3D99E7E41C2F5A72AACBA005313,
	U3CU3Ec__cctor_mBED38AB857BA061D80B1B06261A24C9C56744C10,
	U3CU3Ec__ctor_m66C0ED4876EBA658BD0CF465BFA73ECA9EBDA408,
	U3CU3Ec_U3CRegisterSystemHandlersU3Eb__34_0_m92497DB7FC6650C4647A5C4DCCA5D9087A1B833D,
	U3CU3Ec_U3CRegisterSystemHandlersU3Eb__34_1_m35C43719C0947907FD5081589A8CA58BE835634C,
	U3CU3Ec_U3CRegisterSystemHandlersU3Eb__34_2_m6FF355334D71914A677670E1EE9530E921CA22F1,
	U3CU3Ec_U3CRegisterSystemHandlersU3Eb__34_3_m91EB283DB4D9E498D6261615691CB196C940B4B7,
	U3CU3Ec_U3COnObjectSpawnFinishedU3Eb__76_0_m9650D466911A0B71616F89A878B302721616A9FC,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass54_0__ctor_mD023BF815BEDF3FBCF83F53AE522428A1687B60C,
	U3CU3Ec__DisplayClass54_0_U3CRegisterPrefabU3Eb__0_mAFEF885E19651E91D5D6B62F2E260A3427AD6C5C,
	U3CU3Ec__DisplayClass55_0__ctor_mB7594B3AFDBD832B2444954BB314FD2A52B91F5F,
	U3CU3Ec__DisplayClass55_0_U3CRegisterPrefabU3Eb__0_m8705332689EA30149F1594815E659A922E62445B,
	U3CU3Ec__DisplayClass59_0__ctor_m822CBD9D445088BAA0AE8CC07D967875449CA774,
	U3CU3Ec__DisplayClass59_0_U3CRegisterSpawnHandlerU3Eb__0_m52627ED4B00407B509E6C079FFFA49B1520611E7,
	NetworkConnection_get_observing_m8B3F6C988707FAED9B086C8FB7324EFA11C703AD,
	NULL,
	NetworkConnection_get_identity_mF8F7D1AA28117C2F53450E6697D2966DB1B16F45,
	NetworkConnection_set_identity_m6BD8F3D5B3542256F7BB47F17C18AC51B6EB5CC9,
	NetworkConnection_get_clientOwnedObjects_m6EC6D84417588BE723FBD92B91F6E37F0562373F,
	NetworkConnection_get_remoteTimeStamp_mD1FBDB75191518C80B665E0FFFEA7FF2ECEB3EF3,
	NetworkConnection_set_remoteTimeStamp_m96492955B3F2E2218AE1D47406082A64F0AA5B54,
	NetworkConnection__ctor_mA4169F8A1B0EF786615F138B3395E621AC733E3D,
	NetworkConnection__ctor_m617332B65E39A90B5D3AC66B867A5338BDFEB85E,
	NetworkConnection_GetBatchForChannelId_mDC908F676511D9C6E5E3940FB0A269A4F351D123,
	NetworkConnection_ValidatePacketSize_m623E054D7EACF8628D11D6CC96CBF18D5575F42E,
	NULL,
	NetworkConnection_Send_mB0574477C6D5B9CDF9B660239051B76E40AC3B68,
	NULL,
	NetworkConnection_Update_m25D98F1DD1E6D7AF386E81ED32202D3FD0B5C369,
	NetworkConnection_IsAlive_mB23FF1F3CB3DCF3ECE7533A1179FC1977B9524FC,
	NULL,
	NetworkConnection_ToString_m15D8D27A4FE3967B3BE72F9B5BFC08D4EF2D0CDB,
	NetworkConnectionToClient_get_address_mB7234483EEF04B6B7E5F3837918B606ACA8F77BD,
	NetworkConnectionToClient__ctor_m5301FC38234CB2FD85ABCE37EE92D51179572776,
	NetworkConnectionToClient_SendToTransport_m550D67F19E3780187B2913566ED3955B12F0A83B,
	NetworkConnectionToClient_Disconnect_mF451E9202AE6F4664F777BBE2A6CCA497EB9E7AC,
	NetworkConnectionToClient_AddToObserving_m46B6A4674F416CC9B7F87FD03002B86D79D453AC,
	NetworkConnectionToClient_RemoveFromObserving_mC760146C9A79307754A8A3A2FEE93BF4C7D7559A,
	NetworkConnectionToClient_RemoveFromObservingsObservers_m0CFE675A58F545498CF275EDE63322E4AD88B4D4,
	NetworkConnectionToClient_AddOwnedObject_m6AD1D341C35E579CCB2B8469018C1F04DBD3F93B,
	NetworkConnectionToClient_RemoveOwnedObject_m9181B973ECD712FA5729803CD18DA59DE756E216,
	NetworkConnectionToClient_DestroyOwnedObjects_m7AB4B6AAFD8244B596C02C303DFDCDD9543235C5,
	NetworkConnectionToServer_get_address_m93A6B095B543ECB8D3E8D7C62FD04ED19B9DF3F2,
	NetworkConnectionToServer_SendToTransport_m1DFF5D04A40CEF55768D68F1DC598427CB0CB179,
	NetworkConnectionToServer_Disconnect_mAF11CAB599FAAA6907A6A53DF0FF35E34903EDC1,
	NetworkConnectionToServer__ctor_m59A1F6CDD13D875CDC80886DF418F29331FD12F4,
	NetworkDiagnostics_add_OutMessageEvent_m509EFD06CA54100CA6D78D553E686D705AA7E14B,
	NetworkDiagnostics_remove_OutMessageEvent_m1FE3832B053A95A4B9DE0775797F009BEA175AB2,
	NetworkDiagnostics_add_InMessageEvent_mC6E41C981DFCF20D9FAA9C2CB603D15F543E2729,
	NetworkDiagnostics_remove_InMessageEvent_m548727ACDD2579D73D281D4422E1DFC561FE96FB,
	NetworkDiagnostics_ResetStatics_m8823AD524923C5DD10B928ED8C36D3346BEC7972,
	NULL,
	NULL,
	MessageInfo__ctor_mAC67988BF4FBFD84F5201A8DBFEA962167787671,
	NetworkIdentity_get_isClient_mA664D866C9B791BA0D554F5F1373FCB0FA1A571C,
	NetworkIdentity_set_isClient_m6B0DD90AF225C53612890CD949C344DC3AF17CB6,
	NetworkIdentity_get_isServer_m4BE8C60D704E40109BC7A11007F77178A557B6F2,
	NetworkIdentity_set_isServer_mE11EFCB6F277961CF8BF2FF363F3D4ACA936C2B3,
	NetworkIdentity_get_isLocalPlayer_m9E8156906B197598DD5E56CCB463BFD80EF0BC76,
	NetworkIdentity_set_isLocalPlayer_mC5BD33CD0B88ED65D5B55FA49FF86F7635291193,
	NetworkIdentity_get_isServerOnly_m7FE05E43DDC8562E9C848BE93EA80827F8B2431C,
	NetworkIdentity_get_isClientOnly_m315291F7D383386522570D0E3455CBA9A8DD2871,
	NetworkIdentity_get_hasAuthority_mC0FA6F347408FB65CC629E407CA2DBE9EB4B520B,
	NetworkIdentity_set_hasAuthority_mB13BDE976C8B19F28C6F8219BBC28D426CB92EE8,
	NetworkIdentity_get_netId_m3FF02B719B8AE0B6A3483063A373AFFB2489C0FA,
	NetworkIdentity_set_netId_m7DCA5820ABD4B361D3B6B08A93429BB932A610A9,
	NetworkIdentity_get_connectionToServer_m5B5462ECC017128A99205EDEF095B3FFDBF4EF39,
	NetworkIdentity_set_connectionToServer_m4E36D6FCC72BB5F99830549C9DE19DD350535ADF,
	NetworkIdentity_get_connectionToClient_mF85737F2CC90FC7E77FE8385F35F2FF2E692D82A,
	NetworkIdentity_set_connectionToClient_mD0DBB925AA6F5BEFD9D79C0BB1EE4E0362971129,
	NetworkIdentity_get_spawned_mFD19D87CF5E290D78893B6ED88611ACC1860D139,
	NetworkIdentity_get_NetworkBehaviours_m7D27CEBD20ABC0925B9C7187E04E43CAC99AE8B0,
	NetworkIdentity_set_NetworkBehaviours_mEEBC35C794AB5516F94F1D9F6D67A7A4A93DB51A,
	NetworkIdentity_get_assetId_m3285D38FE7DE4D1F3640E5E50B2D2C88D44BA2F6,
	NetworkIdentity_set_assetId_m695FA033D0740CBF83F1EEDE8B29FDF5AA8D83AF,
	NetworkIdentity_ResetStatics_m1CE994D4F7672894D26295DD98DCEE63AE46B171,
	NetworkIdentity_GetSceneIdentity_mD960C920AAEA6C0BF5BD16A3DEDED4A80F7DFF05,
	NetworkIdentity_SetClientOwner_m4A4C5A2B4F8BFA439EC88A39161776B435C7CDEA,
	NetworkIdentity_GetNextNetworkId_m4B5B89CE5A7B120E0F1AC8092103D2A7C959D3B3,
	NetworkIdentity_ResetNextNetworkId_m53391AEF97823B5133DB4F523A957998AF349014,
	NetworkIdentity_add_clientAuthorityCallback_m633CD9516DED0EB53EB4E1E87BB9EAA15AD40308,
	NetworkIdentity_remove_clientAuthorityCallback_mB13DCC6DA39776FA5286B9A836FB6EE302740AE0,
	NetworkIdentity_get_SpawnedFromInstantiate_mF9CCC6D13CD07F8D25FD2D4762D3A2E82F12E2CC,
	NetworkIdentity_set_SpawnedFromInstantiate_mF6F78EE5B73A8AD10495BE8EF356812CE273D469,
	NetworkIdentity_InitializeNetworkBehaviours_m0DC83CA536354E0EF1FB35B2839CD1AF68000B19,
	NetworkIdentity_Awake_m5278295D326C59B7A020FD899C7BC97922D05FE6,
	NetworkIdentity_OnValidate_m226702DAC2E56D685492FD4AC1C6F06A6C1B22E4,
	NetworkIdentity_OnDestroy_mBE43AC5BCEC90EA29BEF95B400892C62439E4BF0,
	NetworkIdentity_OnStartServer_m0F2FCCC99FA0C984742DED2406C184A9B883226A,
	NetworkIdentity_OnStopServer_m256B4D399B052376C17A63ED5866B9B1AD22F2EF,
	NetworkIdentity_OnStartClient_m32E83A6A595E3F99CCC214FC71AB952D4B409541,
	NetworkIdentity_OnStopClient_m0BAA5DBA51A92275C387D90D17A54D8033733D82,
	NetworkIdentity_OnStartLocalPlayer_m8BB0D60CFE045D1D76E78C71BB12287275F1195E,
	NetworkIdentity_OnStopLocalPlayer_m420155E3121A79F98489C82238CD9D23A3343006,
	NetworkIdentity_NotifyAuthority_mABE2E3765FF6730E79FFA54064DA772FF2F185F3,
	NetworkIdentity_OnStartAuthority_mF7593974D2A81557BE023303A33B724590D8E194,
	NetworkIdentity_OnStopAuthority_m6124985322A401EFFAA46CB41C4B5141E6DBA592,
	NetworkIdentity_OnSerializeSafely_m5612BD0E29030C4166F0BF245E7F4253C0EC1A46,
	NetworkIdentity_OnSerializeAllSafely_mED5DEB32511CC049B8062A96A1012F3EFE92E789,
	NetworkIdentity_GetSerializationAtTick_m441032D3E28D2384CA2C55A4D7217764B0B038E7,
	NetworkIdentity_OnDeserializeSafely_m930E63C2BA46D9E0678F3A7F9AA98E347EDD6BBA,
	NetworkIdentity_OnDeserializeAllSafely_m09CD02B14342498B755DCEA0B498E4085CE45699,
	NetworkIdentity_HandleRemoteCall_m57CBE918D43018F5C3DE87BB9E898513737B13FC,
	NetworkIdentity_AddObserver_m9E79C7D6A2BCFE1CC2064FBE5DAED6067E80EA10,
	NetworkIdentity_RemoveObserver_m13F3F65F59B18EC0B46395A9AA514795E950EC1C,
	NetworkIdentity_ClearObservers_m989F20077372E5E0E1248D5DB8865BF1BED11216,
	NetworkIdentity_AssignClientAuthority_m4C2EF50FBDF63CBF594B9E89A56BA04D9F44B5AD,
	NetworkIdentity_RemoveClientAuthority_mB0A56953931D826CA1ACF66DF53F98B961674E8C,
	NetworkIdentity_Reset_m883C3F9B8DE395014DB12FE2787BA4975BA3E50E,
	NetworkIdentity_ClearAllComponentsDirtyBits_m98279299BDD30A3A319544445922E128065FCFEB,
	NetworkIdentity_ClearDirtyComponentsDirtyBits_m02EBEB1FF48E0892B9E33863D1ABE6200B916FD9,
	NetworkIdentity_ResetSyncObjects_m01A06B2CED1EFF81D34B22058741D54AADDCB2BC,
	NetworkIdentity__ctor_m4105C071BA18EBD036577EB3F22D2D346A512A28,
	NetworkIdentity__cctor_m0715F33040D20006B1E16209699F5A4BA7F5FE5E,
	ClientAuthorityCallback__ctor_m5CB257AFC43DEA6B8CF7BDC3B8F05DB42B6BD7C7,
	ClientAuthorityCallback_Invoke_mAA82FAC912867AFA1817E8CD00F3DD42F94BBDD1,
	ClientAuthorityCallback_BeginInvoke_mA2DCE99677B40D7A76BF9DEE2E5B0A3CC6DD2609,
	ClientAuthorityCallback_EndInvoke_m9D51E32557679438EA41FEE710455EBF916C4C97,
	NetworkLoop_ResetStatics_mA141100B577B33CE8A63EDDED1840641DA47FC75,
	NetworkLoop_FindPlayerLoopEntryIndex_m8401214D75A51D4212D332E8E5CB5D9337B27FEE,
	NetworkLoop_AddToPlayerLoop_mA203DBCE1BA2E03091F1A59F981936532FFC3101,
	NetworkLoop_RuntimeInitializeOnLoad_m9DCD6DC6C58A6A3E1D750A2BE7B910D9C4C013D8,
	NetworkLoop_NetworkEarlyUpdate_m45238F04DFD59A6D6937BCE61DEA4EB5C512141F,
	NetworkLoop_NetworkLateUpdate_m5A9039F8703B7D81FC5CD97674E42F9EFE90007E,
	U3CU3Ec__DisplayClass4_0__ctor_mB3B89587C70507BEEEA8D237FA662BA8A2833F61,
	U3CU3Ec__DisplayClass4_0_U3CFindPlayerLoopEntryIndexU3Eb__0_m8BDAB8183595358E52F81332F37E34612B8B66B9,
	NetworkManager_get_singleton_m3687E70BBF51C41E6F20B606EF0E0E40D13E7641,
	NetworkManager_set_singleton_m3E3F67766BFA7EFD8A3360377DEABEAAFCC271F9,
	NetworkManager_get_numPlayers_mA4CC3D7D3A2C4967F21E4EC6554CADB2EB239816,
	NetworkManager_get_isNetworkActive_m9FF273CFD5738832BFA8542B51BBC6F8A3C1A2EA,
	NetworkManager_get_mode_m7DDDC4AFB7EC130F64E6BA4E916235B46C338337,
	NetworkManager_set_mode_mFD99BDFC3978E7B505C6062BBB31BFDE7A3751FF,
	NetworkManager_OnValidate_m1E5FDC67423166450D021672C314C8346B0CECA0,
	NetworkManager_Reset_m1BA9D62BA16187C79D148D5919186292D2C82E6F,
	NetworkManager_Awake_mED76ED7E0D4767AD1F7ADB7E845C2679E84E6B8A,
	NetworkManager_Start_mD2069EBE9124584CAF85D88EEC28A841F386A4F0,
	NetworkManager_LateUpdate_m5A729F6E28B3C3D49254D02A1A6BACFC9255A8F2,
	NetworkManager_IsServerOnlineSceneChangeNeeded_m635D78E9B5D3883BC0DB0526DC8BE88F4C9FB9C4,
	NetworkManager_IsSceneActive_mA50E21AD716E48D445A23383FFD249D21149F3E2,
	NetworkManager_SetupServer_m9D6BCC8D75D3F91E34218B4DCFA6EE3E813CD2F0,
	NetworkManager_StartServer_m7F3D716905D518790D749225F100D0841051B017,
	NetworkManager_StartClient_m76D46DACCCD2C70BB78014AD376FD99D91EC36C3,
	NetworkManager_StartClient_mC4B472F3B09F109813AD8B7AC133D0475BBCECAE,
	NetworkManager_StartHost_m075A9DF45CC23B3CE251028E7258583AB32B5710,
	NetworkManager_FinishStartHost_mA7C04417B8D8E5FCF593CC8BDAD1B7A1316B0B6E,
	NetworkManager_StartHostClient_m63E4184E021964794FB3F6BD67C3C15036203F4D,
	NetworkManager_StopHost_m9BE1DF0275204D698472DCB8B1DBF62FC83994CF,
	NetworkManager_StopServer_m796F0F4CA3CA42289EBD5F1F6CA6975C9993D988,
	NetworkManager_StopClient_m749293B1F5FA88A3CBD079B8A5A45B6400F3FE47,
	NetworkManager_OnApplicationQuit_m720A6D0FE0EC77E009A55942C2FB006CE9253DA0,
	NetworkManager_ConfigureHeadlessFrameRate_mE2F10667D1299D0761FAF1F0E6476BBD3EA9085F,
	NetworkManager_InitializeSingleton_m2C2E16FAF3FC4D0F4246B344C83E2F864DB21F71,
	NetworkManager_RegisterServerMessages_m62B2385AE088F6F8C8708221989D9E8601FB025C,
	NetworkManager_RegisterClientMessages_m59E1AB1EC3667065785D052268B8DC5115B18091,
	NetworkManager_ResetStatics_m601A79C8C2920AEFAC7525351CEA601BB2CFECB9,
	NetworkManager_OnDestroy_m4BA90829754CAA8DBC58D177E18D6AE8E4F0842D,
	NetworkManager_get_networkSceneName_m80DB5F4473BF7D3CCD9F5A5F3E0F2DDCEA7D76BD,
	NetworkManager_set_networkSceneName_mBEC371D12F3669C066AC523EB7FD6A9885BB3685,
	NetworkManager_ServerChangeScene_mE7B47AD1D7BA092859F28E4500046C8B6BF52D8B,
	NetworkManager_ClientChangeScene_m6F6D640C2E6D989B53DA115B766555057D279B23,
	NetworkManager_OnSceneLoaded_mB69266EA76D21E1DA2CC2364C9EA4D4D1D2971F2,
	NetworkManager_UpdateScene_m0DC87E5D4A8881A2363E79F42C71C14C0B0E31E0,
	NetworkManager_FinishLoadScene_m2EF8C54A639E7BE29E9FC587B3CE651C96602AF4,
	NetworkManager_FinishLoadSceneHost_m055894F1A6AA4D805395C8302D7861744A9FC931,
	NetworkManager_FinishLoadSceneServerOnly_mBF8ABAF755EE1A2887DBF1B9E0D8946BEF6B6EB9,
	NetworkManager_FinishLoadSceneClientOnly_mDC883960B695D08D84046B1584F4D1766D7FEEF2,
	NetworkManager_RegisterStartPosition_mDE79A6E850F2627C193B28E33307C73C120B7C8E,
	NetworkManager_UnRegisterStartPosition_mC51E52EFA2E8B98D0077FA9D654C7548F12CED28,
	NetworkManager_GetStartPosition_m25078697FA64905AF7618A26E75ED10750B1A754,
	NetworkManager_OnServerConnectInternal_m4F66944807317BF72ECEF1DFA28B49626FA25B94,
	NetworkManager_OnServerAuthenticated_m322AA42F54B7853A9903412830AAB76ABED91853,
	NetworkManager_OnServerReadyMessageInternal_m084C79624BCE4A9D7164F5E5739641C82F4768FC,
	NetworkManager_OnServerAddPlayerInternal_m3939653708D4869B12BDECE790525CF0895C7F0F,
	NetworkManager_OnClientConnectInternal_m3598A44D5ED77226B51464DE553F99537ED7085A,
	NetworkManager_OnClientAuthenticated_m2EC1BD4F0C6188208EBBF3F5D333E87D191785B2,
	NetworkManager_OnClientDisconnectInternal_mBABA2400A07727928C4DAFD31AD0F6C518CC516D,
	NetworkManager_OnClientNotReadyMessageInternal_m59761D864BB5B6E5AB9E8773F5B3084B7BF3C097,
	NetworkManager_OnClientSceneInternal_m577FA355F23D1BBA446A69CD950CF45FCAD34019,
	NetworkManager_OnServerConnect_m4FE95629249187EC364DFC99157CC31896BB9032,
	NetworkManager_OnServerDisconnect_m03AAB57C35A00C437AEF6619441C3E6F90DFF082,
	NetworkManager_OnServerReady_mB1C4FFB5CAE5D60A7BCA30EDB3CC5A16215EA62E,
	NetworkManager_OnServerAddPlayer_m5A0967A4E13E54B8D76F19E124373769E66DA557,
	NetworkManager_OnServerError_m3E6942651A79EB4C71B47BC3BCA7AD192F88160F,
	NetworkManager_OnServerChangeScene_m78A746FB47504CEFACC6AF32A27B7C22C21ECC0A,
	NetworkManager_OnServerSceneChanged_m3A6AD198811FBCA588A5B4429A003180AEE8A08B,
	NetworkManager_OnClientConnect_m9254EB4CD1F095FC7A3712C59F268770E426EF1E,
	NetworkManager_OnClientConnect_m23A364AC2D7C3AEE64B43C25453A7BC2378CCC1A,
	NetworkManager_OnClientDisconnect_m3990DBEB7C16E8CE24F4F46490AB52EE9B7BE77D,
	NetworkManager_OnClientDisconnect_m02FA88A6123126271FF5EB97903A4149A0D5D69B,
	NetworkManager_OnClientError_m3E98D9E3FEAC96795CA4F44CC614694C176F4AAC,
	NetworkManager_OnClientNotReady_mA2B3332324E4BFCBD3D1F75C7C41AAFC128B72A7,
	NetworkManager_OnClientNotReady_m1378C1AD4DCB54A286F0A35EFC73287C8A2A2C7F,
	NetworkManager_OnClientChangeScene_mA38EC559CA0D4CFA58ACD65B6E0568683748F411,
	NetworkManager_OnClientSceneChanged_m5A252B98B09BB83BA45EEE697CBD3C6C5A54A152,
	NetworkManager_OnClientSceneChanged_mE627B70A20ABD429D2D8F8C3D0E32F70323442AE,
	NetworkManager_OnStartHost_m377F4A7C84B04AB4F90A786995899EA7EE38B3CE,
	NetworkManager_OnStartServer_m05E1F97124AFA01D00BC671476F834089724C97A,
	NetworkManager_OnStartClient_m6C90B4D7405831A55142A14BB05BD89154116C24,
	NetworkManager_OnStopServer_mBE7B67DF4F17E2302A423B6BAAF6C3A137DCA77D,
	NetworkManager_OnStopClient_m84A76651AF99B829787A74F417B32D44688681BD,
	NetworkManager_OnStopHost_mF7DF15C4F927F247E625E3F9B97962190AF08F0F,
	NetworkManager__ctor_m831DF4BB5F616C10CC2B272AD9DA2C660B3A1924,
	NetworkManager__cctor_mB48BA1AD4B577F594D044976E4F190A877F0198F,
	U3CU3Ec__cctor_m37D13BDF4CBED4CBCBB1249A2281187B67BA788E,
	U3CU3Ec__ctor_m450CE0D75EBA856B0B9F8B7F888C07156E680F20,
	U3CU3Ec_U3Cget_numPlayersU3Eb__21_0_m55CB9833DC205969711E6F01706B4D0B67BA0800,
	U3CU3Ec_U3CRegisterClientMessagesU3Eb__52_0_mEB1B22CBE8BAF3792D502817A90E67A8E23ED0EF,
	U3CU3Ec_U3CRegisterStartPositionU3Eb__69_0_m6BD47C3FB43CDA3ED895BAB78A77008B68741335,
	U3CU3Ec_U3CGetStartPositionU3Eb__71_0_mEEC6E272C3FECCB3466B5E9081A7A4DB358A0D1E,
	NetworkManagerHUD_Awake_m4EA9A71E137E6FB2CF2F69CD1355B8FFAEB2C91A,
	NetworkManagerHUD_OnGUI_mF1BD1A2A022586475B91CC9491377150601855E4,
	NetworkManagerHUD_StartButtons_m77E114195F788922578CAEAD16F04C8529070859,
	NetworkManagerHUD_StatusLabels_m5BC7A80BFA34A4CDEB4F8DFF207766A36CFD6B56,
	NetworkManagerHUD_StopButtons_mCDF2E1FC08A56AF9904AFD9CCC18172E7EFEFF50,
	NetworkManagerHUD__ctor_mB218F6DAB2DD70F06B61CE59E7A6B40160BE5146,
	NetworkReader_get_Length_mD4443E7D7B3E49740A8D400B1CA1FC6D13D26D4B,
	NetworkReader_get_Remaining_mF954AAEEA5A5137C0A5C71CC0A239E00242D5F0D,
	NetworkReader__ctor_m7472356275E51A91FEE8FEF118A6AF2240F6417D,
	NetworkReader__ctor_m73E59DA6A9DC39831B7EAC72DD9F9B29BA0918C0,
	NetworkReader_SetBuffer_m637B9FB7E70FFAA1E7D1CBD92B3C162D2B647775,
	NetworkReader_SetBuffer_m0A5DA4D026405BE2057842706FC25EC994750700,
	NULL,
	NULL,
	NetworkReader_ReadByte_m5CEA57059C34AA6DC218D6F09207C5188AEA68A8,
	NetworkReader_ReadBytes_m8A6348412F267570291A4BD93F4DF1715B35077D,
	NetworkReader_ReadBytesSegment_mD99D152684B83B9AF600E28FB6642FAB3B4B4CCF,
	NetworkReader_ToString_mFEA276025738C2F5F0D5BEE70952B19D278380A0,
	NULL,
	NetworkReaderExtensions_ReadByte_m86E3D12D2C1E2C447A4C2F20B808117DAA69571C,
	NetworkReaderExtensions_ReadByteNullable_m46B384BB9A670691AD0B2D53ED2BCFBA00652997,
	NetworkReaderExtensions_ReadSByte_m8D3F2D88094079D5E0495FBBB3815DBAECB71CC1,
	NetworkReaderExtensions_ReadSByteNullable_mE4F005C9E051CA4913FF491D48EE45C270694A0E,
	NetworkReaderExtensions_ReadChar_m1CD7BF565346230DFE2B29A62818C0DF4ED89AFD,
	NetworkReaderExtensions_ReadCharNullable_mBA02C8B069942BDBDFD8278379C001746572A840,
	NetworkReaderExtensions_ReadBool_mB302D18DCEDDEDB917F1417AA91C6C3D0FF05FA7,
	NetworkReaderExtensions_ReadBoolNullable_m0E59A2846A3630932D55CF5257CBA500340252F1,
	NetworkReaderExtensions_ReadShort_m2F10D1E755DA0CE0B6DC9616874769ED37236613,
	NetworkReaderExtensions_ReadShortNullable_mB3647FD80F5864268B3063BF1DE391FECDB33DA0,
	NetworkReaderExtensions_ReadUShort_m8D2B92163641A77AC2996A4AB42C3B726E67EF0F,
	NetworkReaderExtensions_ReadUShortNullable_mD6DA147C35E1113279521FFC0599F2BB4FEAEC12,
	NetworkReaderExtensions_ReadInt_m7A9A4138EDF745302020D1404A8A73C5BA0F0597,
	NetworkReaderExtensions_ReadIntNullable_m232030B7EC07DB0F07809D4D6C4505FB3BE175FD,
	NetworkReaderExtensions_ReadUInt_mFA3BF8869BCF94137E3F40CFF79B3CE4B93C3D90,
	NetworkReaderExtensions_ReadUIntNullable_mC372F7D0272500AC161F80934B107216C0ED1889,
	NetworkReaderExtensions_ReadLong_m5ADF67BD501F629D2B2779848BF9B4CFB6BC4DCD,
	NetworkReaderExtensions_ReadLongNullable_mE7D7D972269097D6D2BB0E902900E15C513E8DE9,
	NetworkReaderExtensions_ReadULong_m01F59C890722632901DC6C16D461CF39E5EEFDAC,
	NetworkReaderExtensions_ReadULongNullable_m0DA6A493DC13F36AEAF356579B2D4848A719DA43,
	NetworkReaderExtensions_ReadFloat_m4DFFE1E95D7BEF889745B16F7963B32FD839FA64,
	NetworkReaderExtensions_ReadFloatNullable_m5EADF2936FFE0963063AA5CA3617ACF285CB5997,
	NetworkReaderExtensions_ReadDouble_m5800AF1FD52D7DB9047FE7B58A0BD38E23C94281,
	NetworkReaderExtensions_ReadDoubleNullable_m8EECE714A0396E02FF250AAA2024DBB28D5C42AB,
	NetworkReaderExtensions_ReadDecimal_m0AE7D5057B858DB3A6C838CF3DF716707F3FA1EC,
	NetworkReaderExtensions_ReadDecimalNullable_mF95C4F7A1E4D229852BC05966CA011B7681A0D24,
	NetworkReaderExtensions_ReadString_mA74E4612F529683A4AEEDB55C369CDEA32D222CC,
	NetworkReaderExtensions_ReadBytesAndSize_m2EED0B100EFEDF9E1A4D2E9A429DBF3D4F8EA623,
	NetworkReaderExtensions_ReadBytes_m256AB25CABB792B276088B8929BBD2BC972719F0,
	NetworkReaderExtensions_ReadBytesAndSizeSegment_m1750D52A666869DA2EC3C3A25E767834B9DA9E2E,
	NetworkReaderExtensions_ReadVector2_mADCA4D375C619EE39C7ED83BC352C870897FEF6F,
	NetworkReaderExtensions_ReadVector2Nullable_mF103F71AF6F6C8F2A65A46EFB2C00BB34FFFB3DB,
	NetworkReaderExtensions_ReadVector3_mD9D5C83CB3177994353C71C8BC71495A79288416,
	NetworkReaderExtensions_ReadVector3Nullable_mDEA1185C82A9735529800863A1AECB50FD21801A,
	NetworkReaderExtensions_ReadVector4_m26621D658FD76A6A4D8EE73CE81739620A706997,
	NetworkReaderExtensions_ReadVector4Nullable_m4EE393E4CFF0EEF34831707DFD4372AFA61CE392,
	NetworkReaderExtensions_ReadVector2Int_m0B0397F7BA2F4DE1E1A555E9EFEE676C38ECE40C,
	NetworkReaderExtensions_ReadVector2IntNullable_m3779FA8E7B35AAC67C7B02128E4DE4DBB8727AF3,
	NetworkReaderExtensions_ReadVector3Int_mD9A574478D0EC900DB9C50B000AE39148A429F68,
	NetworkReaderExtensions_ReadVector3IntNullable_mCC634E519B105A61E6A6D174518C022C2774976C,
	NetworkReaderExtensions_ReadColor_m21E0B16FA466A4FDAEC8BE1C80E47C9BAE3217A7,
	NetworkReaderExtensions_ReadColorNullable_mF0F6F745B6BEFF123450CA83C75492C0EE01FD88,
	NetworkReaderExtensions_ReadColor32_m7926AFB64529E5FC7C51D390468AAB86722D01F7,
	NetworkReaderExtensions_ReadColor32Nullable_mC5FC03E7B558C4DFA8FC0C0488A1754B2FF976D4,
	NetworkReaderExtensions_ReadQuaternion_m2946D7DD55E1ED8EDEF82536F0D4246D42A22E19,
	NetworkReaderExtensions_ReadQuaternionNullable_mFC1F82DAEE714675362CE50ACF588A125A274509,
	NetworkReaderExtensions_ReadRect_m75BCEF76E5C1CFEBE794A2FE5F52AA55DFBF3F85,
	NetworkReaderExtensions_ReadRectNullable_m2DC5EA005A10F7D2ED892AB04592292327EDF064,
	NetworkReaderExtensions_ReadPlane_m5D8E59014B3ADD6108143CBB2C25DAA1DDD291C5,
	NetworkReaderExtensions_ReadPlaneNullable_m2183EBA24F2D938E841A82638C814DA61AC8A2FE,
	NetworkReaderExtensions_ReadRay_mD2A5A24032CB890240D7F5D94E8399E2766C05EA,
	NetworkReaderExtensions_ReadRayNullable_m83A2D617CECCC13DF92443BEC58F863EE59BEE9C,
	NetworkReaderExtensions_ReadMatrix4x4_m8C9D3E72F015488DA355F6E26FDCF6E0C52FC7D8,
	NetworkReaderExtensions_ReadMatrix4x4Nullable_m5905D3251EECDC7F61B23772F105298B1AA0B6F5,
	NetworkReaderExtensions_ReadGuid_m4BEDE3B6CE956D9B64BFF779754151F49B7A82CE,
	NetworkReaderExtensions_ReadGuidNullable_mA45DC71F13BA89949EDDB1948554D3CED60EC111,
	NetworkReaderExtensions_ReadNetworkIdentity_m52CEFB599F366ADD367C0C23CFA163EBB0F6C764,
	NetworkReaderExtensions_ReadNetworkBehaviour_mAEDC6923777D5F941348F24C73586BB93353E492,
	NULL,
	NetworkReaderExtensions_ReadNetworkBehaviourSyncVar_mC735015AC984ED517FB5EC9469C0A70181E6838F,
	NetworkReaderExtensions_ReadTransform_m81B5B4E6C64D5C011B52262BDCDB1F34E0B50034,
	NetworkReaderExtensions_ReadGameObject_m50C52A705CA8F13E6B80746CB3F001CD82D42404,
	NULL,
	NULL,
	NetworkReaderExtensions_ReadUri_m4D5B6761CC4A07C3E7F223B2DDF88EF4CACC3A3B,
	NetworkReaderExtensions_ReadTexture2D_mE2072E327B0B6F70025162BD7273DA4CE93C6D2D,
	NetworkReaderExtensions_ReadSprite_m1F27B4B0A5D5D62A72E452BCC8A193BE0D372DB0,
	NetworkReaderExtensions__cctor_m40E45B62701A8A5F9F8DAA0544C08D5A4FF20A29,
	PooledNetworkReader__ctor_mEAE1DCDFBAC4CCFA4D40970DE4D20B36931D656E,
	PooledNetworkReader__ctor_m576FB263D8FEA8501D96F220390DC912FEB24762,
	PooledNetworkReader_Dispose_mC35EBC46EDE36DB1EBCF9AFDDCE4375618D32A15,
	NetworkReaderPool_GetReader_m951A0F2DA2D9D9B6DB00A2F9540BEC414D2DE21C,
	NetworkReaderPool_GetReader_mE32ACECE58AE2B54D448549DCA0D920D84DAF79E,
	NetworkReaderPool_Recycle_mAD1BB938E53E18CF168736FB5256680DBBFD1535,
	NetworkReaderPool__cctor_m7818A15817331E4274A35067A7FC010DD07871F6,
	U3CU3Ec__cctor_m312E11FBBE279F10D9D2B17EFB882AA1B0920D12,
	U3CU3Ec__ctor_mDC5278341D19B9BC6F238AF069FB094885AB02D5,
	U3CU3Ec_U3C_cctorU3Eb__4_0_m7B2E2E667B272E9C7BD040EDDA22D56ABB94241D,
	NetworkServer_get_localConnection_m5CC8941C493528A76850E9CA0EE95BFAE18C2B7C,
	NetworkServer_set_localConnection_m121D988179415A2FEA40ADA6C77986A764A56CB8,
	NetworkServer_get_localClientActive_m9C4F4B8848458BB4A2EE6552EDE1E39792C2B3BA,
	NetworkServer_get_active_mF055B10F741C963266FE30D5667E781DBF44DEEE,
	NetworkServer_set_active_m964AD097B146622B9B4439D01DE79A734F658CD6,
	NetworkServer_Initialize_m3ACC3E103BE21A4A4911B20175B6D05B11A95525,
	NetworkServer_AddTransportHandlers_m978B1B0770E285B67A248262C957CD4E1605984A,
	NetworkServer_ActivateHostScene_m457CCCEFB3E319567D22F5AFC7209B6A153DE976,
	NetworkServer_RegisterMessageHandlers_mA9D72F9E0CB28A698139D5BEBC3D4764627802B9,
	NetworkServer_Listen_m0A131ED708A5EE255FC8A2B79C35017D78C9BF18,
	NetworkServer_CleanupSpawned_m8234F6BBF27B185E179E2C23F57C80DA752F1513,
	NetworkServer_Shutdown_mBEA3CA658B95006C84A0A81D5D1D3E47EF564EAF,
	NetworkServer_AddConnection_m765F75E8AF029F8D9D9E9A115663C37AA7C6C9CF,
	NetworkServer_RemoveConnection_m1492C8648F00DB2D4C3F3DC9ABC690BF27C9299B,
	NetworkServer_SetLocalConnection_mEDC58E853ADC35700140B17F47106B5CCF7568AF,
	NetworkServer_RemoveLocalConnection_m378F55B5F91FCC504A7100491C3832C006EED402,
	NetworkServer_NoExternalConnections_mFA58D0B4E2C7D1D227BBCE0815863D71B3AAF785,
	NetworkServer_HasExternalConnections_m584C09C6807F4388BE584212A555A8C80EA94D6A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkServer_OnTransportConnected_m662E1418EE78E47302A2B40D6363323F21A0CAE3,
	NetworkServer_OnConnected_m92037D3CFABD7D7514108F39242084325D9B8DD6,
	NetworkServer_UnpackAndInvoke_m202013D7033D71E38A0E83C4884CD21717C00CFA,
	NetworkServer_OnTransportData_m44765B391CB6A066F3081BC65781B024B4070AFD,
	NetworkServer_OnTransportDisconnected_mC27AD3B742C5E8F72F6C8329A7B2545AAAA4293A,
	NetworkServer_OnError_m3C684B7F8A69D5F77520F7F867288C34CEB52510,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkServer_ClearHandlers_m9CA5A2CC1467B94753025BDA3186A20249D1DD52,
	NetworkServer_GetNetworkIdentity_mC661B7E843BFE80D1D590A10671B8E9D5C4A98B6,
	NetworkServer_DisconnectAll_mCD86E876F1AEE03A2E835C13C0C977A937CD8A4B,
	NetworkServer_AddPlayerForConnection_mC2F0A5B518D848C12E42D427DAA8FD0BFC4E9C56,
	NetworkServer_AddPlayerForConnection_mA88DC3FDC9F32EF84D510CC44C4E03B2C09DEEA8,
	NetworkServer_ReplacePlayerForConnection_m5500FF147E5894EC95AD5A1BFAE72DD69F89B0E8,
	NetworkServer_ReplacePlayerForConnection_mE8CC0EE5D8074A327338BA6980B53BC9A57FEEEE,
	NetworkServer_SetClientReady_mAB57FA4FC627929E203E77FAF03E99C7D0890DEE,
	NetworkServer_SetClientNotReady_m8C9CE076987FC6D3A107C56A6028A6B8292825F5,
	NetworkServer_SetAllClientsNotReady_mCD791365FA6BDA777EB2FB2865B63B0A6BEE4926,
	NetworkServer_OnClientReadyMessage_mA653969EC458916D6B3870A5B59B1455BEEBC466,
	NetworkServer_ShowForConnection_m65FE936D0D166AA00A3D8EFB5AAA2120F54C2CAE,
	NetworkServer_HideForConnection_mD1B712AA6BF00CFC2DAAF3EAFE3A9C777AE43ED4,
	NetworkServer_RemovePlayerForConnection_mE52A009050437EC470A9BF46165473ACF841A5D9,
	NetworkServer_OnCommandMessage_mCC9C792BC26F41538359A854D37641AF52CFCC33,
	NetworkServer_CreateSpawnMessagePayload_m607B37EC828537F1A87CE12B8A077C0B861CBF34,
	NetworkServer_SendSpawnMessage_mBCD386FE79E43D7C4DC964A4D539A857C5D5D102,
	NetworkServer_SendChangeOwnerMessage_mEEA8A44F0B9EC0BD743D520CAF2C20BBB53E35CD,
	NetworkServer_SpawnObject_mB3AD303BED0C815C723891DD4583AA2C289CB163,
	NetworkServer_Spawn_mE71DE744EA836EE5C4EA51B6FCDDCDB83C1D7846,
	NetworkServer_Spawn_mC82932E22819C7C7682841AB3300F26151AC6D15,
	NetworkServer_Spawn_m14592C4F1111BE2C02BEFDBE75F31AE54150924D,
	NetworkServer_ValidateSceneObject_m4ED8F63B17AAA36BAA4CB4DA2BA2E70A838EFD71,
	NetworkServer_SpawnObjects_m9C61D31EB90F43E1219F7C228B38BB01CA7F7A12,
	NetworkServer_Respawn_mEDAC1931E0963B50A235D2A66D481898B7AE1B07,
	NetworkServer_SpawnObserversForConnection_mB30B555FBED825E2B0B8EE579F51CF71F55FA9F0,
	NetworkServer_UnSpawn_m1584C6BCEE95BA77A803C51B77E166E4F94FBCC9,
	NetworkServer_DestroyPlayerForConnection_mC6114493615209566C55E11CF4823B999EA9796A,
	NetworkServer_DestroyObject_m4D81EAD44ABA120EDB93BEB38C8E3F6F8C41798C,
	NetworkServer_DestroyObject_m774FF3CB9D73D6CB140F8090CA05F5B1C070AF76,
	NetworkServer_Destroy_m113ADC5542A4627A397C75DC3ECC9BC87F404FFB,
	NetworkServer_AddAllReadyServerConnectionsToObservers_mDCCA7E9B82CA04C30C3ABA187C3306DE6939C8A1,
	NetworkServer_RebuildObserversDefault_mE396735085539EB07B92E6A9CBBCB33277C0BAFF,
	NetworkServer_RebuildObserversCustom_mBB971C93348EACA1C5977913D7A7E5533043E022,
	NetworkServer_RebuildObservers_m2E49CB20FF4AF0D1EAC08AF0B0271BBEFCDCDABC,
	NetworkServer_GetEntitySerializationForConnection_m1540A07C348D470AEDE16F3A9968BAD8098FBF73,
	NetworkServer_BroadcastToConnection_m7482A9022FA26FC120EA40F9E19FF498581792A4,
	NetworkServer_Broadcast_mF3905B75CAA7F9C63C37CE19552CAB614628A009,
	NetworkServer_NetworkEarlyUpdate_m9990AD91F269B79E8AB54FE45A4EC6251F7C457A,
	NetworkServer_NetworkLateUpdate_mAC035ABA4BB49EF98A547B2A96F3AA54F2E5D63E,
	NetworkServer__cctor_mC0DCCC43EE4E77463F04C3FECCBB84579DAA6C9A,
	NULL,
	NULL,
	NetworkStartPosition_Awake_m957E26CD377C729F2F9442F0C737CC4729A72C47,
	NetworkStartPosition_OnDestroy_m00BD40ED4641A481ACE5967C211E6EBF84585FC8,
	NetworkStartPosition__ctor_m0630A7917AC91935D77BF5755391FB6EA6DFBFD0,
	NetworkTime_get_localTime_mE9F343542C384B1D15D2541759FAD163D00946D6,
	NetworkTime_get_time_m8DE6EBB4100FF44B7A282881FD69798D737C24C6,
	NetworkTime_get_timeVariance_m34EB4F607490CA48E1896BE2075B9564AF922BFE,
	NetworkTime_get_timeStandardDeviation_m0BA80800A296887072DF1191307EB274579081A7,
	NetworkTime_get_offset_m962DCC4FAA7B83F01518096E0BD17F34EEA84CBF,
	NetworkTime_get_rtt_m9E4EC224EB95FBB7B8329E9A225D3275D0770C66,
	NetworkTime_get_rttVariance_m7E2A5F27F0741B45B69848E26ED1D03A33F0A037,
	NetworkTime_get_rttStandardDeviation_m557D817D40153F321D01AB0D272E620525F2CA38,
	NetworkTime_ResetStatics_m178218DA64630690EADF332FDB4DB878FAC88BDF,
	NetworkTime_UpdateClient_m852C8649B8BDB874079CAD542EC213D2165D53DD,
	NetworkTime_OnServerPing_m0313EC1EE6F416509525C9C1471260E6AFD1BEE6,
	NetworkTime_OnClientPong_m44B93D45F64A2A215C28274C23018A9CADBDC253,
	NetworkTime__cctor_m747E3CF7CD3F32E4BFCA9975961B4C76A4737770,
	NetworkWriter_Reset_m9F34945A36E170550D63E41D2CF9C26ACB03FF77,
	NetworkWriter_EnsureCapacity_m8686EBB645C1892C928AB87D8FD73148B547BDC9,
	NetworkWriter_ToArray_mC9A28117C639BA09B09A43343D3D3A3488A187E7,
	NetworkWriter_ToArraySegment_m20B3969C06F4320029AD66A820B0523A8D6641EF,
	NULL,
	NULL,
	NetworkWriter_WriteByte_mC22A6863F21D47005ECAD28F5AB6FC5248B23D0E,
	NetworkWriter_WriteBytes_m1EF0FDBA6E07D885F57CB524710524715BB7E374,
	NULL,
	NetworkWriter__ctor_mD3E7B77EDCCE633CFEE83EBC1DB06355D0460A5F,
	NetworkWriterExtensions_WriteByte_mFFEFC20C5AB176841BBBE10C1E4A866EAF369B96,
	NetworkWriterExtensions_WriteByteNullable_mF9B242CDD70D6377003A16616017AE7E7ECF828C,
	NetworkWriterExtensions_WriteSByte_m9AA9E2E5390BD4AC9CEA8A05EAE1234D9C48B8C8,
	NetworkWriterExtensions_WriteSByteNullable_m31024FEF67A2D39E6942456902E0609F86D86BBD,
	NetworkWriterExtensions_WriteChar_m14899B6388B37CA68D9BA49CF0BE0C3EE956E49E,
	NetworkWriterExtensions_WriteCharNullable_mFC4447120353234274E133070B57CF79A00D19DD,
	NetworkWriterExtensions_WriteBool_mF0B4A6ADF594071A21F89B4AC0FD3594F5D38157,
	NetworkWriterExtensions_WriteBoolNullable_m0827D39D922053803E6913AD577C1D6FD59184C1,
	NetworkWriterExtensions_WriteShort_mF005FFDEF8C55047B7EC7BCA61241AEB294BE7DE,
	NetworkWriterExtensions_WriteShortNullable_m47963A4259643D24DE1C2CBA482F093FABE3E581,
	NetworkWriterExtensions_WriteUShort_m12CCA0CE8DA51CAD6A67CDC3680393119557AAB3,
	NetworkWriterExtensions_WriteUShortNullable_mAC2A24B07CE4482BA3F167EC7622609CAD3A2B94,
	NetworkWriterExtensions_WriteInt_m131D043593351682FEA1F18B455B80F78A24FD12,
	NetworkWriterExtensions_WriteIntNullable_m543C5968EB92A9A79A703C2E48F11C57F3FE8A38,
	NetworkWriterExtensions_WriteUInt_mFDC458AC35F7EF0D09E7A0D779ACF37D12346D64,
	NetworkWriterExtensions_WriteUIntNullable_m045E0D347FF8BA92E1884FC666419FF015395DFA,
	NetworkWriterExtensions_WriteLong_m60818DB28C20076BB7B90A357DF8F7D58CD2AD5E,
	NetworkWriterExtensions_WriteLongNullable_m36E40C95D550FC603346B3F598D01A0141D4B566,
	NetworkWriterExtensions_WriteULong_mF3B9637FA1C3EA31551616D085A5A3AF041E6963,
	NetworkWriterExtensions_WriteULongNullable_mB436FDD19B8677D5E98E0C55848DAE698E7906FB,
	NetworkWriterExtensions_WriteFloat_mCC353B70D9FBACD2D2CECA565EC7B2B30400A6A5,
	NetworkWriterExtensions_WriteFloatNullable_m05BBFDF4558D76CD3D615EC8293D46C5EAAE5F43,
	NetworkWriterExtensions_WriteDouble_m40D19C266720079063B22A53B6940E1CF6482BFC,
	NetworkWriterExtensions_WriteDoubleNullable_m6199C6C58FCCBDE4C4D4B5A184FC037749609666,
	NetworkWriterExtensions_WriteDecimal_m1F5400E8F7D91C15538FC724A4B430A818479431,
	NetworkWriterExtensions_WriteDecimalNullable_mFD5228BF1FFBB3A62C4876029B853D560FB29B17,
	NetworkWriterExtensions_WriteString_m31C762A5BBACB77129E085AB7D7A9AEEB9ACA95F,
	NetworkWriterExtensions_WriteBytesAndSizeSegment_mAD214D7E8A080A5615D400A6C3F2F5218F3AD11B,
	NetworkWriterExtensions_WriteBytesAndSize_mB11A8C12622432C14420D616F763F130DD1A91C9,
	NetworkWriterExtensions_WriteBytesAndSize_m75E79671D06A8D7C3CA09E02844E6316F3A3E56B,
	NULL,
	NetworkWriterExtensions_WriteVector2_m52B333B1242AC5E0490DA980D5954157FDF9949F,
	NetworkWriterExtensions_WriteVector2Nullable_m2178D4AC2AD94F0C4D69B7329701D73F7F769662,
	NetworkWriterExtensions_WriteVector3_m2895E336F7B2F52819808DC35F574B10D3317609,
	NetworkWriterExtensions_WriteVector3Nullable_m015585A48318B31C257AB9DB240306359004EDA5,
	NetworkWriterExtensions_WriteVector4_mE75893C683F8C655244CE272B0B050662E751414,
	NetworkWriterExtensions_WriteVector4Nullable_m006747750FDE41EDD60C58BFA8E9219AA93813E7,
	NetworkWriterExtensions_WriteVector2Int_mDA0A622A19099A741DE393233204CD6FB89FF75A,
	NetworkWriterExtensions_WriteVector2IntNullable_m32D79441D84ADCF6C22C4A05A3E7A74576EF8FF9,
	NetworkWriterExtensions_WriteVector3Int_m4911D397D68D855E386699BB99ECF4BD0F5F7060,
	NetworkWriterExtensions_WriteVector3IntNullable_m5907EAB248F53ACCB09E9062ECA7F82A2F0A3A79,
	NetworkWriterExtensions_WriteColor_m7EA61C4CEE7C0F49BCAF7339D1A76F4ECA50626E,
	NetworkWriterExtensions_WriteColorNullable_m9B0C40392853593AF7A9DA61A91B59C7641BF154,
	NetworkWriterExtensions_WriteColor32_mC430555DAD65FA8A1ACA7117122960F6412B3079,
	NetworkWriterExtensions_WriteColor32Nullable_m7EEE0A8672EBD42EBDC68D52F6C1BF745997D54A,
	NetworkWriterExtensions_WriteQuaternion_m96A981CD95861D3F23358155CD1CCBCDF185D909,
	NetworkWriterExtensions_WriteQuaternionNullable_m6138C62C7B32C2858D59488B953583FE82854AC4,
	NetworkWriterExtensions_WriteRect_m1C462FFD3B7FE7B73B6F733CDECEDCBFFF9F3A05,
	NetworkWriterExtensions_WriteRectNullable_m25B1C832F6F6027DAD898FD6F7D37014459CF91D,
	NetworkWriterExtensions_WritePlane_m75F27F25C9E50A60C635D376D4C1D993D1376A97,
	NetworkWriterExtensions_WritePlaneNullable_m61AD793E53710488E94AB3932CBFA3EB43AA00E5,
	NetworkWriterExtensions_WriteRay_mF907AD59F7170BBB74CF49611FBBAF069DD6D1F2,
	NetworkWriterExtensions_WriteRayNullable_m14DEFB4AA69F614EFF381EEBD7D6D19BB862E30A,
	NetworkWriterExtensions_WriteMatrix4x4_m40B7941CFBB2912FA11AFB35F5729E4F2383BEAD,
	NetworkWriterExtensions_WriteMatrix4x4Nullable_m792F404CB27BEA99EEE8797CFFE4BD76B4209253,
	NetworkWriterExtensions_WriteGuid_m1700462B08DC1D6B80FF00521C50C5A85E781AFB,
	NetworkWriterExtensions_WriteGuidNullable_mAE2592EC889F49BCC5C6C264C2DBAE743C2B618D,
	NetworkWriterExtensions_WriteNetworkIdentity_m6B7BF775A09551E47F1E98F2603E0C3B8358FC64,
	NetworkWriterExtensions_WriteNetworkBehaviour_mCA7167729B04CFA80D5F7AB5D9C43FC24B4DF20D,
	NetworkWriterExtensions_WriteTransform_m3118EFCE291DE2182B785E099FE30B3D68F36830,
	NetworkWriterExtensions_WriteGameObject_m461FDFA27D93D42B1C6C1B5F84EC2314E479E45B,
	NULL,
	NULL,
	NetworkWriterExtensions_WriteUri_mECC3E974CD9C900CEBB24E5E4743DF2D41E3533F,
	NetworkWriterExtensions_WriteTexture2D_mD912047E3473A437DC8C57D552A778CC00A5AB7A,
	NetworkWriterExtensions_WriteSprite_mDCBD674842A4AB01AB69507B9C7AFEAA12DD67A1,
	NetworkWriterExtensions__cctor_mEF540B4CE4511D89773F16F2D0E8092600455D59,
	PooledNetworkWriter_Dispose_m4D1350BAFF1AE5BE1A2C6540EFACB15F02511A18,
	PooledNetworkWriter__ctor_m0575E62AABFA81E2F757C76EA455757C8F27B165,
	NetworkWriterPool_GetWriter_m53506C8016911951C82F2F83E45592CE2F9A85AE,
	NetworkWriterPool_Recycle_m8E7D8C8ED6F0856380CE750DBEF1D0EC5BAF8FB3,
	NetworkWriterPool__cctor_m9274ABE8EE4582DC7D9DF184F7F73F2DF3FC7CF3,
	U3CU3Ec__cctor_m3FD838FF3EDCED87947D48608D148699344D8A93,
	U3CU3Ec__ctor_m54EF1D3A06EA9F18684008AB6E69268AF8AD7A9A,
	U3CU3Ec_U3C_cctorU3Eb__3_0_mB0A5B91C0E7EC65AD20C53B230FD500DECA1A061,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SyncObject_Flush_m7B8340A5FD92A0A1C092F115976457A1B84AD8EF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SyncObject__ctor_mCE98D127674428340204B86B247E70CEE3352E0B,
	U3CU3Ec__cctor_m0CE42A80DD1706EAE2A8B0A4852DE8A5818F48DA,
	U3CU3Ec__ctor_mFC9B0D0EE6169B2BE6911DFCA57A995E26ADB768,
	U3CU3Ec_U3C_ctorU3Eb__9_0_m5132968FC2EF9EEB668A486A2F97DAEB1DAE1312,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SyncVarGameObject_get_Value_mE65AFF3C6639B6AB7C0F38060DD8B8C235FAEDB6,
	SyncVarGameObject_set_Value_mC8BB9614EEB3E3C3398260C0A046B0BF3720F3F7,
	SyncVarGameObject_add_Callback_m4665D2E0E039D093CBDCC74C53163CA3175039CD,
	SyncVarGameObject_remove_Callback_mFFFFF221B06FBE7DA9A3379CC467664F0391317D,
	SyncVarGameObject_InvokeCallback_mB3619997E27EB3129A67673A31E6C4703D8083E7,
	SyncVarGameObject__ctor_mB04849C010A23C4DA5D5C888E549306AD764450A,
	SyncVarGameObject_GetNetId_m57ED9EB23EC30C59E33A0941AC55D1A3ADCB3DB1,
	SyncVarGameObject_GetGameObject_m8066BA7A21591654063C99AF4EB3AA9718C87EEB,
	SyncVarGameObject_op_Implicit_mE581FC095E6AF11A3DA0512548C56A140A75443F,
	SyncVarGameObject_op_Implicit_mD0E1C1226C73A8A837C045965234D7AF1423B6EE,
	SyncVarGameObject_op_Equality_m73BFF9908CB976703889892036BD22F5040F9CDA,
	SyncVarGameObject_op_Inequality_mF89450FF6FEF63BA1E386073EF9CEA5A2704CCF8,
	SyncVarGameObject_op_Equality_m10BF041FD59A41511A31FC153850BDC542259816,
	SyncVarGameObject_op_Inequality_m1A4F171A99D45F75E4CFA7EA5A68BE17B6517529,
	SyncVarGameObject_op_Equality_m6BFF6D527A8F7A1BA2763B7E413790723D1D68E7,
	SyncVarGameObject_op_Inequality_m2F33424297230D22C6105DDC26A6E1E3AB4A4298,
	SyncVarGameObject_Equals_m34C64231D0EA8B97E7FC51381A26633BA6E07D55,
	SyncVarGameObject_GetHashCode_mF0EAFF0E15E80F2E221DC3FF59D2CFE04DF3B07A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SyncVarNetworkIdentity_get_Value_m777B8CFC8DAA96885BB1C305D0AEB486F2FF8413,
	SyncVarNetworkIdentity_set_Value_m6BC7FA7E6EF557B7307781890F419CF973E1A2B5,
	SyncVarNetworkIdentity_add_Callback_m71EA63626C4AEB46E49E6FF03F0ECF253CA00FD3,
	SyncVarNetworkIdentity_remove_Callback_mD9822968C88796117217CC09181809478066F64B,
	SyncVarNetworkIdentity_InvokeCallback_m6B0BE6A3FECA596F98865EDE165754DE9C7B2E26,
	SyncVarNetworkIdentity__ctor_m1FDDAF32DA124DFCA5BA034E2F6B4428D91BFD49,
	SyncVarNetworkIdentity_op_Implicit_m78CF163D678CA7D6D127D5C6E2E55835B310428F,
	SyncVarNetworkIdentity_op_Implicit_m8F58E47D4FDC73AC11C1C31D45828E404AE4C21D,
	SyncVarNetworkIdentity_op_Equality_m388AF3AD37E0B41F48F4F58DD80CEB0336B76064,
	SyncVarNetworkIdentity_op_Inequality_mDB84F11552998D1C74AE4E28CBD3BA7CFDAAE32A,
	SyncVarNetworkIdentity_op_Equality_m09A02E3285097BF64AFD4484BA2EB94527124279,
	SyncVarNetworkIdentity_op_Inequality_m332E8B21430C8195C6F80A55DCAE641769159F77,
	SyncVarNetworkIdentity_op_Equality_mAA1610C1CFC76F7974D9C9790DF47B864ACC442C,
	SyncVarNetworkIdentity_op_Inequality_mAF0E505A71340F2547CBC44D52CB183FCFB8643E,
	SyncVarNetworkIdentity_Equals_mF95A3D92C3CA69A16A1AB018BA623604AB1CAFE3,
	SyncVarNetworkIdentity_GetHashCode_mF09764959B3EF41924CD4AF803F0855C3BCD7084,
	LatencySimulation_Awake_mEAC826A25C58752C0C69564A5348BD01CA9CB7C1,
	LatencySimulation_OnEnable_mD242DDC8C43D5466149B69B85EAE0DE562756A9C,
	LatencySimulation_OnDisable_m199817612D997387B3E911992B686D05DB2E07B1,
	LatencySimulation_Noise_m1A3BC7F8CD1202484C1A868B4D23FC9791AC5CF5,
	LatencySimulation_SimulateLatency_mAF38F234D096858BDBD02E0437436E71EF41CB7C,
	LatencySimulation_SimulateSend_mDF97FC1EECFEC05ADB7531989317B5295595CEBB,
	LatencySimulation_Available_mB4E68E0825607F4005173F6C7D09373DA21ECD03,
	LatencySimulation_ClientConnect_m58CFCBC09F36A3BF4B4AFACDE77C3DDBE813A75F,
	LatencySimulation_ClientConnect_mA949A0B2766FB09E9D5DFC5739F9BCD2702FFC5F,
	LatencySimulation_ClientConnected_mDF44EB7042EF3BC3ED0B44748C4B8839D64C6715,
	LatencySimulation_ClientDisconnect_m24DA6CF3C4FADC708864D354670ABC2E7B2A5EA9,
	LatencySimulation_ClientSend_mE46A5530B7A922BEED635E7F1AA40F4AF109C353,
	LatencySimulation_ServerUri_mE307B5DFAA51ACE4E586BC9F9E3F46D72A3CEE9E,
	LatencySimulation_ServerActive_mEC3859D604A254EBBE0613A38D095DFC70C9FBB3,
	LatencySimulation_ServerGetClientAddress_m49F80F36FAF18A175ABD6AFC040E1916F433AD40,
	LatencySimulation_ServerDisconnect_m1C5804F3662C09822C4E111BAB2314537BCD83D3,
	LatencySimulation_ServerSend_m13439312B8DA75B034DCB22DE7AB5D804F1A5629,
	LatencySimulation_ServerStart_m1EA1DA984B406C9EF1ED2B70010DB9DF20F51C01,
	LatencySimulation_ServerStop_m42D319CCA21268E27974470ADBA90C0D478EF316,
	LatencySimulation_ClientEarlyUpdate_m53492BCFAB22CBE97F2AD567B5DC1C6E82C851FA,
	LatencySimulation_ServerEarlyUpdate_mBA19D74A9BB375B315A174B428E691ADD4A30952,
	LatencySimulation_ClientLateUpdate_mF0C4A41CEA86EFB8A86E97633249B22E59122924,
	LatencySimulation_ServerLateUpdate_m374D47AD51A3EE64CA2889A03B508F9A3BFB87E0,
	LatencySimulation_GetBatchThreshold_m8E605B0BD4403AF800FBC4BEBB9FCEBE10378BBE,
	LatencySimulation_GetMaxPacketSize_m9F0DCF83D8965FB56EE3BAD9CB384BE9EF0CF3B6,
	LatencySimulation_Shutdown_mC7DD8B6818B0076AD69EEFDB07C8E4E62A548471,
	LatencySimulation_ToString_m2F0B4FAF301FC22A8093E73308E047FC919C5DAB,
	LatencySimulation__ctor_mEBBCAAD0D6369E89B44B1C8F9636D88BD7EABDD4,
	MiddlewareTransport_Available_mF191BD5C8896806E9F28BA6FEC9FE1DDDC2695EB,
	MiddlewareTransport_GetMaxPacketSize_mA0E7EED841343BAF79A3ADB599A1FD4A7EE601B7,
	MiddlewareTransport_GetBatchThreshold_m143CE6F44716CF4698567F495FD23C5BB9D2C3B2,
	MiddlewareTransport_Shutdown_m194A579B64BFD146048A8EEE1E9FF244D76F0B3B,
	MiddlewareTransport_ClientConnect_m5CA5664667B11CFB4938EEB37AC81ED33749C5E1,
	MiddlewareTransport_ClientConnected_mA0F69BF09D2BC66AE0D356E4F9D14AF85CC22932,
	MiddlewareTransport_ClientDisconnect_m0D3A2544636DEF9926928276F00D6265FC970B53,
	MiddlewareTransport_ClientSend_mD163AB15E518761572B6ED2CB1D0FCF73EF9D8F5,
	MiddlewareTransport_ClientEarlyUpdate_m871E0E3C136A4525828038C57A789FD780C2C1F2,
	MiddlewareTransport_ClientLateUpdate_m29921876427F25B125507E1F0F91C2E733A8DFD1,
	MiddlewareTransport_ServerActive_m6D3FEA6629AAC8A1F7DC552699B5E73BAFE0B129,
	MiddlewareTransport_ServerStart_m0D85BB7EA451FD775587EDF01BBAF13E4DF687A1,
	MiddlewareTransport_ServerStop_m2211C5FF5EB34510A2D4BA3A8B43426FC12FE3E9,
	MiddlewareTransport_ServerSend_mD53F86C744C1E2953E9B893682627A97167BCF1D,
	MiddlewareTransport_ServerDisconnect_m5BBABDD2A495A780623AF5955B084B1AAD24B071,
	MiddlewareTransport_ServerGetClientAddress_m489B2ACBA1E4572FDFBCC3C58F9B4C345E08BE5D,
	MiddlewareTransport_ServerUri_m4811255EF1EE5048D396701A580ED4BDEC892991,
	MiddlewareTransport_ServerEarlyUpdate_mF4BF126B0A54A1A9A00E9F5F86E64F54F8B9C1E5,
	MiddlewareTransport_ServerLateUpdate_m20E19AB75FC9CB57E0C57C05FBE51F4A6BF90142,
	MiddlewareTransport__ctor_mCB190AEE11176640125AF0F7F62842471D92186B,
	MultiplexTransport_Awake_m26A9019ED0ABDDE43CA027533F27F34A39BC5BB9,
	MultiplexTransport_ClientEarlyUpdate_m25E35E39DD3ED68AFA0F1829474734D1D6B7D50B,
	MultiplexTransport_ServerEarlyUpdate_m2A91E85584D6897FC0F495DF270220129EB2D2CC,
	MultiplexTransport_ClientLateUpdate_m2FF3203709B7766A969E34724CD4ECD393672084,
	MultiplexTransport_ServerLateUpdate_m8B4417CEAC63EBFB5754F5BA315B32329B84F1AD,
	MultiplexTransport_OnEnable_mB1A69C2F6625B2623669BF8E158A298E3F598E17,
	MultiplexTransport_OnDisable_m3D0FB1850D725883E1B012BE2E191B2C4E4E4C24,
	MultiplexTransport_Available_mD1A9553AAFEA47B7243E432217D1F097BA949DAE,
	MultiplexTransport_ClientConnect_m100247467A74FBD5B85B3053895265A3CD969804,
	MultiplexTransport_ClientConnect_m68EE415F6344B5B068F38B4E9665626B814C3FC7,
	MultiplexTransport_ClientConnected_m9A02C200A00E63BEA58542D9FF631389991B0C06,
	MultiplexTransport_ClientDisconnect_mC5E015E98AB02572E650F30319414B7344856E2A,
	MultiplexTransport_ClientSend_m02AC6BC2BE30B2493FC7EADA175CAE5531E16942,
	MultiplexTransport_FromBaseId_m48D22B4B10606A37A35893E55EA3F38CF11F24E4,
	MultiplexTransport_ToBaseId_m4483DB4464A8A0664EE667278437C9D6C50E4D26,
	MultiplexTransport_ToTransportId_m0750AEEA94DA55CDFFAD5D9C93346686C466C4BD,
	MultiplexTransport_AddServerCallbacks_m83C3D43FDF8C41DDC100792514769D8420AA95F3,
	MultiplexTransport_ServerUri_m880F88499ECEAF9A081AEE2B4536FCF3A65AFD87,
	MultiplexTransport_ServerActive_m01DD6DAD3AC8A8A9D981CB9E345BDA698599453C,
	MultiplexTransport_ServerGetClientAddress_mF215E0A6812B259249283C8189D6706AA63DA7C8,
	MultiplexTransport_ServerDisconnect_m2FE63F8AF3F91CEFDD5958C366500125E02014BF,
	MultiplexTransport_ServerSend_mCAC22B32D358D90BB60C39E785571B3CEB4E29BB,
	MultiplexTransport_ServerStart_m30F65C9378DDB1B84E9C9C25D69F8E2EE1D5A887,
	MultiplexTransport_ServerStop_mC1038F61612FC37D90432BEB8E5F2B50672FB447,
	MultiplexTransport_GetMaxPacketSize_m8E1ED512A8C33A0583C506866631DA35575CF644,
	MultiplexTransport_Shutdown_mCD3B2370DD52E5F859EC07816FA6BC662A62631A,
	MultiplexTransport_ToString_m322B81C3495ED803F44376B5E317216B4BA2E441,
	MultiplexTransport__ctor_mEE455ED05170C5F0676EB3F47E5529D8D3637A4B,
	U3CU3Ec__DisplayClass18_0__ctor_m5F63E485307DE66FF8DF6DA73B14BD0A8C596679,
	U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__0_m71C260E9F663345051F4ED0DBE5FD51CB051374E,
	U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__1_mAEB5E470043FEFE9ACA5895E6903C43283DB85D1,
	U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__2_m20358BC676721F4AF2C4443B36DF2E03E7DDAFD3,
	U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__3_m3D94991394A01162B4B35D5D975273EE53A43152,
	TelepathyTransport_Awake_m602F5715334063DC6405856C3AD1FC29C7B7E5A5,
	TelepathyTransport_Available_mF8EDA3882E796A7C821F5205051E549927AFEB80,
	TelepathyTransport_CreateClient_mA2500F52E85A569E7732413CE58F46E7F0C9502B,
	TelepathyTransport_ClientConnected_m8B5228FFC261001AAFD1F4C1DACEAB32FB728BFA,
	TelepathyTransport_ClientConnect_mEC8326A6AE0253B05D929D98B37E774134A8320D,
	TelepathyTransport_ClientConnect_m27B5C086CB7ED8E51EDF0D922FACFBEE88CB2BB5,
	TelepathyTransport_ClientSend_m3783EA93B7C2367DA6BEFAC62A797AFE3386ABD5,
	TelepathyTransport_ClientDisconnect_m0A27B27ECCDAF5C60DC6777E95D6BA075FCEA4AC,
	TelepathyTransport_ClientEarlyUpdate_m03B06C08025B8191282BEE1F6A9EC25C0EE47D34,
	TelepathyTransport_ServerUri_mD3896EBAB74A7FE4BE2970ACB5B8930A1C064D0B,
	TelepathyTransport_ServerActive_m7E31D881004FB4A68CE8E0C7F5C2D3C18ED9909E,
	TelepathyTransport_ServerStart_m6F3609E2583811D26FB3ED5DF916A40EF848F84D,
	TelepathyTransport_ServerSend_m52FD3C622C1459DE5492FF5383A0BD7B6EF3909B,
	TelepathyTransport_ServerDisconnect_m4D145DFAB3459FCD5FA07DB9B023E3783CF9394C,
	TelepathyTransport_ServerGetClientAddress_mCAAE5E776410D1E8F8CB92D5AE547207F4F32EEE,
	TelepathyTransport_ServerStop_m029E82E1FC85B297A37C856ACBB2471C6410E2FB,
	TelepathyTransport_ServerEarlyUpdate_m970EF7D651ED0FC5FD4898CEDC3878892DC820E0,
	TelepathyTransport_Shutdown_mA4F85A710C7C52BB890A74C4942EC7BA64F9FB40,
	TelepathyTransport_GetMaxPacketSize_m81DCD6F50C02F566D328EB5097BEE7081C544A2B,
	TelepathyTransport_ToString_m528636DD99B5B7A9346BC0B4E628E3C07CAA954C,
	TelepathyTransport__ctor_mA4061E623CDE35A23989AE27570AB68007F43F9D,
	TelepathyTransport_U3CAwakeU3Eb__16_0_mE8A62C170D1D06FC33DB1D05296F685B47563116,
	TelepathyTransport_U3CCreateClientU3Eb__18_0_m2191BB18AFC53FAD8E79AB7DF6817986ADDE4091,
	TelepathyTransport_U3CCreateClientU3Eb__18_1_m048D4A07C6DCDD9FF3B6CB39F01A4A43F0A553FE,
	TelepathyTransport_U3CCreateClientU3Eb__18_2_m90BB987518058CFF67D49CE28C6C9B38D07C364B,
	TelepathyTransport_U3CServerStartU3Eb__27_0_m4FA1834F3E693A569EB3C31B4F25513901F555B5,
	TelepathyTransport_U3CServerStartU3Eb__27_1_mF9AA67BA5E56DE1BB322D9064283B455CC12E16C,
	TelepathyTransport_U3CServerStartU3Eb__27_2_m7F10B8399AA1AC077200DA03220BEAD54C01F930,
	NULL,
	NULL,
	NULL,
	Transport_ClientConnect_mBA9A630061B0F73E6B0383A762693985C3AEEA6D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Transport_GetBatchThreshold_mDCB4EF6DD7FB35DAF8E7A40E3DACE7484CE04ACA,
	Transport_Update_mE9D7C9DCA9E54CA74C6D1B47C38834ABE4C7D796,
	Transport_LateUpdate_m458595D25E89D7222C3424D8A95AE08BF0D646FD,
	Transport_ClientEarlyUpdate_m0722DB6D5B6FC924657EAEC9A7E0DAE15497AEBE,
	Transport_ServerEarlyUpdate_mFD20DA364D3891A0C0F053D96FECF8E79E717D80,
	Transport_ClientLateUpdate_mD6FF8477B2174BD8B8DB9EA6F2CB02BDD473A135,
	Transport_ServerLateUpdate_m0FC3D74ECF4029F06B92999BB34025E58F34CC8C,
	NULL,
	Transport_OnApplicationQuit_mC51DBEB57BAB46E2B780F49420F5E205D79675BB,
	Transport__ctor_m18499670B379DE249BA0A4D8D978335F0C1E0376,
	U3CU3Ec__cctor_m304817F15C080D3EEC191046CF1E9890B95CC53C,
	U3CU3Ec__ctor_m5310ACAAE5FAD5B20FD9F10931FAB299C78950EC,
	U3CU3Ec_U3C_ctorU3Eb__32_0_mF7FDB3F2F0DFF66E559442E14A7D4464D0E14D79,
	U3CU3Ec_U3C_ctorU3Eb__32_1_m54E5403E2D3851EA7C6A6D343300323AC7110927,
	U3CU3Ec_U3C_ctorU3Eb__32_2_mEE5BBB94A91E911340820E2237772AFDD1872CD7,
	U3CU3Ec_U3C_ctorU3Eb__32_3_mC6026CC78089FE13D779CC367A1D87FFCE82D604,
	U3CU3Ec_U3C_ctorU3Eb__32_4_m195B635652A49A8FC937C05492F1610FAAC6F0BB,
	U3CU3Ec_U3C_ctorU3Eb__32_5_m76101E9DC27C90650C0BA52687E86B115CA542A2,
	U3CU3Ec_U3C_ctorU3Eb__32_6_m324D78CD5F370CACCC239A8ACAB0E17767C01C1C,
	U3CU3Ec_U3C_ctorU3Eb__32_7_m3976B6943CBB65C428F9FE631793BB65EA539C8D,
	NetworkMessageDelegate__ctor_m8A66FF4EABB5ED3BEDBA1E5F5BDEF8582502FFEF,
	NetworkMessageDelegate_Invoke_mAEE9C9A0218BA47AA1BAED072D5042DB7C28E7F1,
	NetworkMessageDelegate_BeginInvoke_m72BB2CC861D8E6F765FF70B2EA5DDB947385613F,
	NetworkMessageDelegate_EndInvoke_mE3EFA49B7DE1173FC104A1CCEEFCC0DE10114520,
	SpawnDelegate__ctor_m85296496F410FDA1A8F167B0E65E39B176491618,
	SpawnDelegate_Invoke_m647484B13C3CB741C8D7D910D355FEC904FBFBA3,
	SpawnDelegate_BeginInvoke_m6BDC7169A26F222C731A4CC93B5D2C1A9A5DCF7B,
	SpawnDelegate_EndInvoke_m3DC601F0E76C04C3159289012B2B4AC40BDA0AB5,
	SpawnHandlerDelegate__ctor_m5D9B376DBB3C85A6B66ED69AB8B7E7010A3130C9,
	SpawnHandlerDelegate_Invoke_m8DCF7DA5325363F4860AF5A135A7D9318A8C21D0,
	SpawnHandlerDelegate_BeginInvoke_mA8C0FE11CC475B4F433E7CE81A0D91EDC68631C4,
	SpawnHandlerDelegate_EndInvoke_m20D6E4B8796890F64D0FDF48450469B8FF716965,
	UnSpawnDelegate__ctor_mB9B060274806A21287BA156858B167BDCC028AE1,
	UnSpawnDelegate_Invoke_m253B17666AEEE399A94FF14D3B4FA5AFD7078D57,
	UnSpawnDelegate_BeginInvoke_m281EC91CBC5162D34ACF38393B3881A800CE3058,
	UnSpawnDelegate_EndInvoke_mECA5B4C0F9415708A4E4D354020E3F4F48927B57,
	Utils_GetTrueRandomUInt_m42E9457ACD0D68D5B81A8B127F0F5B3D68C8A60E,
	Utils_IsPrefab_mF9AE489EFFD6D206956CB7C33F27A6515578F502,
	Utils_IsSceneObjectWithPrefabParent_mF68AE61BD662AC25BD9EA4DDA45CF00D5D5666BE,
	Utils_IsPointInScreen_m9F2EB8E51CA512D11B8FAE3B122BB2C91A2B6DFB,
	Utils_GetSpawnedInServerOrClient_mC3BDF9A39C1BE50284978EFDD7415EEB91C07C57,
	RemoteCallDelegate__ctor_m3F67716E53766A6A7E680E0C77F7B9D5F1F2D0E1,
	RemoteCallDelegate_Invoke_m848344D03E6489904F3B3B459EDBC240D7CEBFF5,
	RemoteCallDelegate_BeginInvoke_m34CD029992B84D41DE926CCE439D737E4B1B7F66,
	RemoteCallDelegate_EndInvoke_m1DA48390145766F9235DBC67382C7DDA05941673,
	Invoker_AreEqual_m00338067ECE838CD927419F73EE365A9CECB7C47,
	Invoker__ctor_mAB4C1E969716FCF8DB4257BCF45F152446555DF4,
	RemoteProcedureCalls_CheckIfDelegateExists_mD2EB732C127114EEA5BB6B8905A6E64EE0214EC7,
	RemoteProcedureCalls_RegisterDelegate_m90390C79C2172ECECDF9E7E88BF19D9BF8E0C4EA,
	RemoteProcedureCalls_RegisterCommand_m3FBF93ACE360E2DE9C986F943698A29A916C3143,
	RemoteProcedureCalls_RegisterRpc_mD6EE0378898117C330E0D463F1BDA5A7A3F6FD19,
	RemoteProcedureCalls_RemoveDelegate_m1A927E7F25452473796651139255BEFA578A0F92,
	RemoteProcedureCalls_GetInvokerForHash_m24BF900C2F8908F325BC490BEC0E74EED1514A4C,
	RemoteProcedureCalls_Invoke_m6F5DD31CF8DE5CED460EE534BA481C2EE8954B40,
	RemoteProcedureCalls_CommandRequiresAuthority_m2E4FC4E955F7210D16B72C1C878E4EB2C6BCA899,
	RemoteProcedureCalls_GetDelegate_mD574382502F91E9E4263ADBC334B3AD1A4F76BFB,
	RemoteProcedureCalls__cctor_m1B17F0ABAC30E2C7BAB0CDA5819611D8F42BA244,
	GeneratedNetworkCode__Read_Mirror_ReadyMessage_m4E344D67949293187D126E9DB4B66E68BBFBAEE1,
	GeneratedNetworkCode__Write_Mirror_ReadyMessage_mB14840AF0A29896AD4CB80BB6D2D14690EAB90C4,
	GeneratedNetworkCode__Read_Mirror_NotReadyMessage_m73EBD81A27C74BC3C248C6E040547A02CC284741,
	GeneratedNetworkCode__Write_Mirror_NotReadyMessage_mFFD85ECAD655A6A6428AB2C3257A298BF82D32D3,
	GeneratedNetworkCode__Read_Mirror_AddPlayerMessage_m516FA38D7454305B8A27E0E53C232B46C1342E3A,
	GeneratedNetworkCode__Write_Mirror_AddPlayerMessage_m16236FB7277472EC7BAFBF723746B6AB7B5165BA,
	GeneratedNetworkCode__Read_Mirror_SceneMessage_m5EF5F08305A5ED7336A03141426864B77FB60355,
	GeneratedNetworkCode__Read_Mirror_SceneOperation_m6593FF68AA6E260ECEEDFBCF700D58D37BFF3B03,
	GeneratedNetworkCode__Write_Mirror_SceneMessage_m7751BC2A52EDA5209FE4DEE0712F72D9191F48D0,
	GeneratedNetworkCode__Write_Mirror_SceneOperation_m82391DF96778526A6DADB740698AD1D6292CF34A,
	GeneratedNetworkCode__Read_Mirror_CommandMessage_m3D9DFA26219BADC3CA6DE371E3A20CD300638323,
	GeneratedNetworkCode__Write_Mirror_CommandMessage_m32DB2DBCF1B07F22B92C66EE7539A2A0346C95BF,
	GeneratedNetworkCode__Read_Mirror_RpcMessage_m14C0E321ED76E0EFFECE15BCE81F1605D6B892B8,
	GeneratedNetworkCode__Write_Mirror_RpcMessage_m22BC6E711B230E2FCA1302CEB10175E90ACD68A1,
	GeneratedNetworkCode__Read_Mirror_SpawnMessage_m6A8028F047F003A82AA07DC1806FDA7EE43E6C27,
	GeneratedNetworkCode__Write_Mirror_SpawnMessage_mFA9126BE7E93B8D881849D7634B3634F441C47ED,
	GeneratedNetworkCode__Read_Mirror_ChangeOwnerMessage_m006F4299A05059BC3BD38152233C279273ECDC36,
	GeneratedNetworkCode__Write_Mirror_ChangeOwnerMessage_m88E6F40CA2153521DE21DB9E4B56944A3F597D34,
	GeneratedNetworkCode__Read_Mirror_ObjectSpawnStartedMessage_mE8AAD46EB6A6EE89451B41232D74E7D660F6E424,
	GeneratedNetworkCode__Write_Mirror_ObjectSpawnStartedMessage_m3FA1297055A6CCE3262B4CC120D0180FA5632F7C,
	GeneratedNetworkCode__Read_Mirror_ObjectSpawnFinishedMessage_m04FAB9718A074E27C587F366C03660BE3A8707C6,
	GeneratedNetworkCode__Write_Mirror_ObjectSpawnFinishedMessage_mF70A7E1F66927F844D6EC216369EDB2A7C4455A6,
	GeneratedNetworkCode__Read_Mirror_ObjectDestroyMessage_m2536202895EE8BF458D5519D4C597E09AF18514D,
	GeneratedNetworkCode__Write_Mirror_ObjectDestroyMessage_m893B1809CFF465913F49259B126D8F3825937C8D,
	GeneratedNetworkCode__Read_Mirror_ObjectHideMessage_mE9D8BE2F8E134D20BF1A8C378D2C3E1405C13DC0,
	GeneratedNetworkCode__Write_Mirror_ObjectHideMessage_m4CB8EDD46511E3F9EEEEB10C8C598ACFF80D4469,
	GeneratedNetworkCode__Read_Mirror_EntityStateMessage_m3B48D3F01B190BC96719E6E1A2AAD415A750CDA2,
	GeneratedNetworkCode__Write_Mirror_EntityStateMessage_m04DC9C28FD64679F8C96254B158F44A5711DE1FC,
	GeneratedNetworkCode__Read_Mirror_NetworkPingMessage_m79853AE41B6F173C64F24524475A2804CF04326D,
	GeneratedNetworkCode__Write_Mirror_NetworkPingMessage_m8855D6C394B429D69304B07BF1E4744F6A1A7891,
	GeneratedNetworkCode__Read_Mirror_NetworkPongMessage_m966BA422B175C070879D6DB963F33A8FAEDD65F9,
	GeneratedNetworkCode__Write_Mirror_NetworkPongMessage_mAB8F03B31199677482F68FCC05458C2BB7CBCA04,
	GeneratedNetworkCode_InitReadWriters_mF874877C924678E4EC340DE14446855B70EBE1E5,
};
extern void NetworkPingMessage__ctor_mF340573CD5E74C9BECF60F7789C8170559A95C5D_AdjustorThunk (void);
extern void NetworkBehaviourSyncVar__ctor_mFB219E84A8139BFFCC899543F4057E03D963B0FF_AdjustorThunk (void);
extern void NetworkBehaviourSyncVar_Equals_m2EE0BD631368A1EA28D3E4422A9F14C43B9B0A27_AdjustorThunk (void);
extern void NetworkBehaviourSyncVar_Equals_m2EE82223A7405A371A233BCED2C78292E69AA85F_AdjustorThunk (void);
extern void NetworkBehaviourSyncVar_ToString_m4DE9A94D17F9F28FCDB963AC8665144C37076363_AdjustorThunk (void);
extern void MessageInfo__ctor_mAC67988BF4FBFD84F5201A8DBFEA962167787671_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[6] = 
{
	{ 0x06000086, NetworkPingMessage__ctor_mF340573CD5E74C9BECF60F7789C8170559A95C5D_AdjustorThunk },
	{ 0x060000D5, NetworkBehaviourSyncVar__ctor_mFB219E84A8139BFFCC899543F4057E03D963B0FF_AdjustorThunk },
	{ 0x060000D6, NetworkBehaviourSyncVar_Equals_m2EE0BD631368A1EA28D3E4422A9F14C43B9B0A27_AdjustorThunk },
	{ 0x060000D7, NetworkBehaviourSyncVar_Equals_m2EE82223A7405A371A233BCED2C78292E69AA85F_AdjustorThunk },
	{ 0x060000D8, NetworkBehaviourSyncVar_ToString_m4DE9A94D17F9F28FCDB963AC8665144C37076363_AdjustorThunk },
	{ 0x0600015B, MessageInfo__ctor_mAC67988BF4FBFD84F5201A8DBFEA962167787671_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1224] = 
{
	5508,
	5508,
	5508,
	7994,
	8191,
	5508,
	5508,
	5442,
	5442,
	4453,
	4453,
	2082,
	5508,
	5508,
	5508,
	5398,
	5442,
	5508,
	1471,
	4423,
	3441,
	5508,
	5508,
	5508,
	5508,
	3146,
	3146,
	5368,
	5368,
	5368,
	5368,
	5368,
	5368,
	8113,
	5508,
	5398,
	5508,
	5508,
	2083,
	5508,
	5508,
	2083,
	5508,
	4423,
	1472,
	4423,
	4423,
	1472,
	4423,
	8500,
	5508,
	4453,
	3357,
	3357,
	3171,
	3171,
	3171,
	3171,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	4423,
	4155,
	1958,
	5367,
	4453,
	3540,
	1895,
	5508,
	6720,
	5905,
	6103,
	7456,
	7990,
	8166,
	8164,
	7651,
	7651,
	8012,
	8012,
	5328,
	4386,
	5328,
	4386,
	4423,
	4386,
	7988,
	8116,
	-1,
	5508,
	5508,
	1964,
	2565,
	5508,
	2581,
	4453,
	4453,
	5508,
	5508,
	5398,
	2082,
	3888,
	5508,
	5508,
	5398,
	5508,
	5508,
	2082,
	5508,
	5508,
	5508,
	3888,
	5508,
	6673,
	7942,
	6673,
	8455,
	-1,
	-1,
	7381,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4386,
	5508,
	5508,
	5508,
	4453,
	4453,
	4453,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5442,
	5442,
	5442,
	5442,
	5442,
	5442,
	5367,
	5398,
	5398,
	5442,
	5398,
	4453,
	5367,
	4423,
	5368,
	4424,
	3814,
	3814,
	2486,
	2486,
	4424,
	4424,
	5442,
	5508,
	4453,
	1049,
	1049,
	1052,
	-1,
	458,
	458,
	-1,
	7386,
	1018,
	1828,
	7386,
	-1,
	961,
	961,
	-1,
	1018,
	1828,
	-1,
	-1,
	-1,
	-1,
	-1,
	1968,
	2581,
	1968,
	2581,
	3842,
	3842,
	4453,
	4453,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	2280,
	4005,
	1932,
	5398,
	5508,
	5508,
	5442,
	8462,
	8341,
	8462,
	8341,
	8462,
	8485,
	8485,
	8485,
	8485,
	8500,
	8351,
	8341,
	8341,
	8500,
	8500,
	8500,
	8500,
	7386,
	7522,
	8500,
	8341,
	-1,
	-1,
	-1,
	-1,
	-1,
	7347,
	8341,
	7644,
	8341,
	6572,
	7009,
	6572,
	7009,
	8341,
	6953,
	6953,
	8335,
	8500,
	7349,
	8485,
	8341,
	8485,
	7682,
	7421,
	8112,
	8126,
	8113,
	8113,
	8194,
	8500,
	8345,
	8344,
	8500,
	8342,
	8343,
	8355,
	8334,
	8350,
	8343,
	8342,
	8355,
	8331,
	7635,
	8341,
	8338,
	8500,
	8500,
	8500,
	8500,
	8500,
	8500,
	5508,
	4451,
	4457,
	4456,
	4387,
	3171,
	-1,
	-1,
	-1,
	-1,
	5508,
	3454,
	5508,
	3454,
	5508,
	3454,
	5398,
	5398,
	5398,
	4453,
	5398,
	5328,
	4386,
	5508,
	4423,
	3441,
	7309,
	-1,
	2082,
	2082,
	5508,
	3888,
	5508,
	5398,
	5398,
	4423,
	2082,
	5508,
	4453,
	2581,
	5508,
	4453,
	4453,
	5508,
	5398,
	2082,
	5508,
	5508,
	8341,
	8341,
	8341,
	8341,
	8500,
	-1,
	-1,
	1028,
	5442,
	4494,
	5442,
	4494,
	5442,
	4494,
	5442,
	5442,
	5442,
	4494,
	5367,
	4423,
	5398,
	4453,
	5398,
	4453,
	8462,
	5398,
	4453,
	5346,
	4404,
	8500,
	8113,
	4453,
	8455,
	8500,
	8341,
	8341,
	5442,
	4494,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	1399,
	1614,
	3410,
	1561,
	2581,
	441,
	4453,
	4453,
	5508,
	3842,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	8500,
	2556,
	1561,
	371,
	4453,
	8500,
	6711,
	6087,
	8500,
	8500,
	8500,
	5508,
	3853,
	8462,
	8341,
	5367,
	5442,
	5367,
	4423,
	5508,
	5508,
	5508,
	5508,
	5508,
	5442,
	8194,
	5508,
	5508,
	5508,
	4453,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5442,
	5508,
	5508,
	8500,
	5508,
	8462,
	8341,
	4453,
	1578,
	2625,
	5508,
	5508,
	5508,
	5508,
	5508,
	8341,
	8341,
	5398,
	4453,
	4453,
	2575,
	2537,
	5508,
	5508,
	5508,
	4452,
	4496,
	4453,
	4453,
	4453,
	4453,
	2565,
	4453,
	4453,
	5508,
	4453,
	5508,
	4453,
	4453,
	5508,
	4453,
	1578,
	5508,
	4453,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	8500,
	8500,
	5508,
	3596,
	3842,
	3171,
	3842,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5367,
	5367,
	4453,
	4155,
	4453,
	4155,
	-1,
	-1,
	5442,
	1842,
	2686,
	5398,
	-1,
	8194,
	7796,
	8194,
	7826,
	7967,
	7797,
	8194,
	7795,
	7967,
	7813,
	7967,
	7831,
	7988,
	7814,
	7988,
	7832,
	8012,
	7816,
	8012,
	7833,
	8224,
	7827,
	7946,
	7804,
	7931,
	7803,
	8116,
	8116,
	7258,
	7788,
	8310,
	7834,
	8316,
	7836,
	8324,
	7838,
	8314,
	7835,
	8322,
	7837,
	7908,
	7798,
	7912,
	7799,
	8165,
	7821,
	8173,
	7823,
	8144,
	7820,
	8169,
	7822,
	8029,
	7819,
	7956,
	7806,
	8116,
	8116,
	-1,
	8398,
	8116,
	8116,
	-1,
	-1,
	8116,
	8116,
	8116,
	8500,
	4453,
	4155,
	5508,
	8116,
	8040,
	8341,
	8500,
	8500,
	5508,
	5398,
	8462,
	8341,
	8485,
	8485,
	8351,
	8500,
	8500,
	8500,
	8500,
	8338,
	8500,
	8500,
	8194,
	8191,
	8341,
	8500,
	8485,
	8485,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	8338,
	8341,
	6841,
	6957,
	8338,
	7569,
	-1,
	-1,
	-1,
	-1,
	-1,
	8500,
	7381,
	8500,
	7389,
	6839,
	6843,
	6491,
	8341,
	8341,
	8500,
	7670,
	7661,
	7661,
	7675,
	6989,
	6227,
	7661,
	7661,
	7661,
	7661,
	7661,
	6990,
	8194,
	8485,
	8341,
	8341,
	8341,
	8341,
	7650,
	7650,
	8341,
	8341,
	7675,
	7675,
	7675,
	7263,
	8341,
	8500,
	8500,
	8500,
	8500,
	-1,
	-1,
	5508,
	5508,
	5508,
	8442,
	8442,
	8442,
	8442,
	8442,
	8442,
	8442,
	8442,
	8500,
	8500,
	7658,
	8340,
	8500,
	5508,
	4423,
	5398,
	4851,
	-1,
	-1,
	4494,
	1542,
	-1,
	5508,
	7675,
	7606,
	7675,
	7622,
	7649,
	7607,
	7675,
	7605,
	7649,
	7614,
	7649,
	7624,
	7650,
	7615,
	7650,
	7625,
	7651,
	7616,
	7651,
	7626,
	7681,
	7623,
	7642,
	7611,
	7641,
	7610,
	7661,
	7583,
	7661,
	6590,
	-1,
	7685,
	7627,
	7687,
	7629,
	7689,
	7631,
	7686,
	7628,
	7688,
	7630,
	7637,
	7608,
	7638,
	7609,
	7668,
	7619,
	7671,
	7621,
	7666,
	7618,
	7669,
	7620,
	7657,
	7617,
	7644,
	7612,
	7661,
	7661,
	7661,
	7661,
	-1,
	-1,
	7661,
	7661,
	7661,
	8500,
	5508,
	5508,
	8462,
	8341,
	8500,
	8500,
	5508,
	5398,
	-1,
	-1,
	-1,
	-1,
	5328,
	4386,
	5328,
	4386,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5508,
	5508,
	4453,
	4453,
	4453,
	4453,
	5508,
	5508,
	8500,
	5508,
	5442,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5398,
	4453,
	4453,
	4453,
	2280,
	4453,
	7988,
	8112,
	8116,
	8116,
	7389,
	7389,
	7389,
	7389,
	7389,
	7389,
	3842,
	5367,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5398,
	4453,
	4453,
	4453,
	2280,
	4453,
	8116,
	8116,
	7389,
	7389,
	7389,
	7389,
	7389,
	7389,
	3842,
	5367,
	5508,
	5508,
	5508,
	4071,
	4068,
	239,
	5442,
	4453,
	4453,
	5442,
	5508,
	2082,
	5398,
	5442,
	3441,
	4423,
	1471,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	3146,
	3146,
	5508,
	5398,
	5508,
	5442,
	3146,
	3146,
	5508,
	4453,
	5442,
	5508,
	2082,
	5508,
	5508,
	5442,
	5508,
	5508,
	1471,
	4423,
	3441,
	5398,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5442,
	4453,
	4453,
	5442,
	5508,
	2082,
	1719,
	3146,
	3146,
	5508,
	5398,
	5442,
	3441,
	4423,
	1471,
	5508,
	5508,
	3146,
	5508,
	5398,
	5508,
	5508,
	4423,
	1471,
	2305,
	4423,
	5508,
	5442,
	5508,
	5442,
	4453,
	4453,
	2082,
	5508,
	5508,
	5398,
	5442,
	5508,
	1471,
	4423,
	3441,
	5508,
	5508,
	5508,
	3146,
	5398,
	5508,
	5442,
	5508,
	4155,
	5508,
	4423,
	2134,
	4423,
	5442,
	5442,
	4453,
	4453,
	2082,
	5508,
	5398,
	5442,
	5508,
	1471,
	4423,
	3441,
	5508,
	3146,
	3146,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	5508,
	8500,
	5508,
	5508,
	2082,
	4453,
	5508,
	4423,
	1471,
	2305,
	4423,
	2556,
	1556,
	366,
	4453,
	2556,
	1855,
	852,
	3448,
	2556,
	3454,
	1288,
	3448,
	2556,
	4453,
	1255,
	4453,
	8455,
	8194,
	7381,
	8203,
	8112,
	2556,
	1559,
	368,
	4453,
	1393,
	5508,
	6486,
	6010,
	6597,
	7009,
	8338,
	6815,
	6073,
	8191,
	8112,
	8500,
	8171,
	7670,
	8038,
	7660,
	7887,
	7634,
	8209,
	8194,
	7677,
	7675,
	7913,
	7639,
	8181,
	7673,
	8232,
	7682,
	7903,
	7635,
	8141,
	7665,
	8140,
	7664,
	8138,
	7662,
	8139,
	7663,
	7951,
	7643,
	8036,
	7658,
	8037,
	7659,
	8500,
};
static const Il2CppTokenRangePair s_rgctxIndices[67] = 
{
	{ 0x0200001B, { 13, 5 } },
	{ 0x0200001C, { 18, 1 } },
	{ 0x02000036, { 53, 1 } },
	{ 0x02000037, { 54, 1 } },
	{ 0x02000055, { 99, 1 } },
	{ 0x0200005F, { 115, 6 } },
	{ 0x02000062, { 144, 29 } },
	{ 0x02000066, { 173, 9 } },
	{ 0x02000067, { 182, 39 } },
	{ 0x0200006B, { 221, 5 } },
	{ 0x0200006E, { 226, 32 } },
	{ 0x02000072, { 258, 8 } },
	{ 0x02000073, { 266, 8 } },
	{ 0x02000074, { 274, 15 } },
	{ 0x02000076, { 289, 17 } },
	{ 0x06000060, { 0, 1 } },
	{ 0x0600007D, { 1, 1 } },
	{ 0x0600007E, { 2, 2 } },
	{ 0x06000080, { 4, 3 } },
	{ 0x06000081, { 7, 6 } },
	{ 0x060000B0, { 19, 3 } },
	{ 0x060000B3, { 22, 3 } },
	{ 0x060000B8, { 25, 2 } },
	{ 0x060000BB, { 27, 2 } },
	{ 0x060000BE, { 29, 1 } },
	{ 0x060000BF, { 30, 1 } },
	{ 0x060000C0, { 31, 1 } },
	{ 0x060000C1, { 32, 3 } },
	{ 0x060000F1, { 35, 1 } },
	{ 0x060000F2, { 36, 8 } },
	{ 0x060000F3, { 44, 2 } },
	{ 0x060000F4, { 46, 6 } },
	{ 0x060000F5, { 52, 1 } },
	{ 0x0600013F, { 55, 2 } },
	{ 0x06000159, { 57, 1 } },
	{ 0x0600015A, { 58, 1 } },
	{ 0x06000203, { 59, 2 } },
	{ 0x06000204, { 61, 3 } },
	{ 0x06000209, { 64, 3 } },
	{ 0x06000244, { 67, 1 } },
	{ 0x06000248, { 68, 4 } },
	{ 0x06000249, { 72, 2 } },
	{ 0x0600026A, { 74, 2 } },
	{ 0x0600026B, { 76, 1 } },
	{ 0x0600026C, { 77, 2 } },
	{ 0x0600026D, { 79, 2 } },
	{ 0x0600026E, { 81, 1 } },
	{ 0x0600026F, { 82, 1 } },
	{ 0x06000270, { 83, 1 } },
	{ 0x06000277, { 84, 3 } },
	{ 0x06000278, { 87, 3 } },
	{ 0x06000279, { 90, 2 } },
	{ 0x0600027A, { 92, 6 } },
	{ 0x0600027B, { 98, 1 } },
	{ 0x060002BB, { 100, 1 } },
	{ 0x060002BC, { 101, 3 } },
	{ 0x060002BF, { 104, 3 } },
	{ 0x060002DF, { 107, 4 } },
	{ 0x060002FE, { 111, 3 } },
	{ 0x060002FF, { 114, 1 } },
	{ 0x06000314, { 121, 6 } },
	{ 0x06000315, { 127, 4 } },
	{ 0x06000316, { 131, 1 } },
	{ 0x06000317, { 132, 1 } },
	{ 0x06000318, { 133, 1 } },
	{ 0x06000319, { 134, 3 } },
	{ 0x0600031A, { 137, 7 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[306] = 
{
	{ (Il2CppRGCTXDataType)3, 20286 },
	{ (Il2CppRGCTXDataType)1, 379 },
	{ (Il2CppRGCTXDataType)3, 43127 },
	{ (Il2CppRGCTXDataType)3, 43547 },
	{ (Il2CppRGCTXDataType)2, 1717 },
	{ (Il2CppRGCTXDataType)3, 311 },
	{ (Il2CppRGCTXDataType)3, 312 },
	{ (Il2CppRGCTXDataType)2, 1719 },
	{ (Il2CppRGCTXDataType)3, 363 },
	{ (Il2CppRGCTXDataType)3, 364 },
	{ (Il2CppRGCTXDataType)2, 2090 },
	{ (Il2CppRGCTXDataType)3, 1989 },
	{ (Il2CppRGCTXDataType)3, 43190 },
	{ (Il2CppRGCTXDataType)1, 1034 },
	{ (Il2CppRGCTXDataType)3, 43439 },
	{ (Il2CppRGCTXDataType)3, 43382 },
	{ (Il2CppRGCTXDataType)2, 1319 },
	{ (Il2CppRGCTXDataType)3, 1990 },
	{ (Il2CppRGCTXDataType)3, 1418 },
	{ (Il2CppRGCTXDataType)3, 43324 },
	{ (Il2CppRGCTXDataType)3, 43309 },
	{ (Il2CppRGCTXDataType)3, 1411 },
	{ (Il2CppRGCTXDataType)3, 43337 },
	{ (Il2CppRGCTXDataType)3, 43321 },
	{ (Il2CppRGCTXDataType)3, 1412 },
	{ (Il2CppRGCTXDataType)3, 43323 },
	{ (Il2CppRGCTXDataType)3, 1409 },
	{ (Il2CppRGCTXDataType)3, 43307 },
	{ (Il2CppRGCTXDataType)3, 1410 },
	{ (Il2CppRGCTXDataType)2, 390 },
	{ (Il2CppRGCTXDataType)2, 396 },
	{ (Il2CppRGCTXDataType)2, 397 },
	{ (Il2CppRGCTXDataType)3, 12687 },
	{ (Il2CppRGCTXDataType)2, 2903 },
	{ (Il2CppRGCTXDataType)3, 12686 },
	{ (Il2CppRGCTXDataType)3, 43359 },
	{ (Il2CppRGCTXDataType)2, 1704 },
	{ (Il2CppRGCTXDataType)3, 221 },
	{ (Il2CppRGCTXDataType)3, 43129 },
	{ (Il2CppRGCTXDataType)1, 399 },
	{ (Il2CppRGCTXDataType)3, 222 },
	{ (Il2CppRGCTXDataType)2, 1951 },
	{ (Il2CppRGCTXDataType)3, 1481 },
	{ (Il2CppRGCTXDataType)3, 43185 },
	{ (Il2CppRGCTXDataType)3, 43130 },
	{ (Il2CppRGCTXDataType)3, 43186 },
	{ (Il2CppRGCTXDataType)2, 1705 },
	{ (Il2CppRGCTXDataType)3, 253 },
	{ (Il2CppRGCTXDataType)3, 254 },
	{ (Il2CppRGCTXDataType)2, 1952 },
	{ (Il2CppRGCTXDataType)3, 1482 },
	{ (Il2CppRGCTXDataType)3, 43354 },
	{ (Il2CppRGCTXDataType)3, 43128 },
	{ (Il2CppRGCTXDataType)3, 874 },
	{ (Il2CppRGCTXDataType)3, 875 },
	{ (Il2CppRGCTXDataType)3, 43158 },
	{ (Il2CppRGCTXDataType)3, 43406 },
	{ (Il2CppRGCTXDataType)2, 405 },
	{ (Il2CppRGCTXDataType)2, 404 },
	{ (Il2CppRGCTXDataType)2, 408 },
	{ (Il2CppRGCTXDataType)1, 408 },
	{ (Il2CppRGCTXDataType)3, 43475 },
	{ (Il2CppRGCTXDataType)2, 5566 },
	{ (Il2CppRGCTXDataType)3, 26980 },
	{ (Il2CppRGCTXDataType)2, 5966 },
	{ (Il2CppRGCTXDataType)1, 407 },
	{ (Il2CppRGCTXDataType)3, 14375 },
	{ (Il2CppRGCTXDataType)2, 410 },
	{ (Il2CppRGCTXDataType)2, 4968 },
	{ (Il2CppRGCTXDataType)3, 20292 },
	{ (Il2CppRGCTXDataType)3, 43433 },
	{ (Il2CppRGCTXDataType)3, 20293 },
	{ (Il2CppRGCTXDataType)2, 7500 },
	{ (Il2CppRGCTXDataType)3, 43434 },
	{ (Il2CppRGCTXDataType)3, 43159 },
	{ (Il2CppRGCTXDataType)3, 43407 },
	{ (Il2CppRGCTXDataType)3, 43537 },
	{ (Il2CppRGCTXDataType)3, 43160 },
	{ (Il2CppRGCTXDataType)3, 43408 },
	{ (Il2CppRGCTXDataType)3, 43161 },
	{ (Il2CppRGCTXDataType)3, 43409 },
	{ (Il2CppRGCTXDataType)3, 43541 },
	{ (Il2CppRGCTXDataType)3, 43542 },
	{ (Il2CppRGCTXDataType)3, 43540 },
	{ (Il2CppRGCTXDataType)3, 43131 },
	{ (Il2CppRGCTXDataType)1, 412 },
	{ (Il2CppRGCTXDataType)3, 43187 },
	{ (Il2CppRGCTXDataType)3, 43132 },
	{ (Il2CppRGCTXDataType)1, 413 },
	{ (Il2CppRGCTXDataType)3, 43188 },
	{ (Il2CppRGCTXDataType)3, 43133 },
	{ (Il2CppRGCTXDataType)3, 43189 },
	{ (Il2CppRGCTXDataType)2, 1708 },
	{ (Il2CppRGCTXDataType)3, 265 },
	{ (Il2CppRGCTXDataType)3, 266 },
	{ (Il2CppRGCTXDataType)2, 1955 },
	{ (Il2CppRGCTXDataType)3, 1483 },
	{ (Il2CppRGCTXDataType)3, 43535 },
	{ (Il2CppRGCTXDataType)3, 43134 },
	{ (Il2CppRGCTXDataType)3, 876 },
	{ (Il2CppRGCTXDataType)2, 425 },
	{ (Il2CppRGCTXDataType)3, 26981 },
	{ (Il2CppRGCTXDataType)3, 26982 },
	{ (Il2CppRGCTXDataType)3, 43591 },
	{ (Il2CppRGCTXDataType)2, 6613 },
	{ (Il2CppRGCTXDataType)1, 424 },
	{ (Il2CppRGCTXDataType)3, 1495 },
	{ (Il2CppRGCTXDataType)3, 2206 },
	{ (Il2CppRGCTXDataType)3, 2205 },
	{ (Il2CppRGCTXDataType)3, 2207 },
	{ (Il2CppRGCTXDataType)3, 43549 },
	{ (Il2CppRGCTXDataType)3, 20294 },
	{ (Il2CppRGCTXDataType)3, 20295 },
	{ (Il2CppRGCTXDataType)3, 43550 },
	{ (Il2CppRGCTXDataType)3, 43548 },
	{ (Il2CppRGCTXDataType)2, 6197 },
	{ (Il2CppRGCTXDataType)3, 33557 },
	{ (Il2CppRGCTXDataType)3, 13923 },
	{ (Il2CppRGCTXDataType)3, 33559 },
	{ (Il2CppRGCTXDataType)3, 33560 },
	{ (Il2CppRGCTXDataType)3, 33558 },
	{ (Il2CppRGCTXDataType)2, 512 },
	{ (Il2CppRGCTXDataType)3, 33260 },
	{ (Il2CppRGCTXDataType)3, 33261 },
	{ (Il2CppRGCTXDataType)2, 4498 },
	{ (Il2CppRGCTXDataType)3, 33259 },
	{ (Il2CppRGCTXDataType)3, 33258 },
	{ (Il2CppRGCTXDataType)3, 33254 },
	{ (Il2CppRGCTXDataType)3, 33255 },
	{ (Il2CppRGCTXDataType)2, 4496 },
	{ (Il2CppRGCTXDataType)2, 507 },
	{ (Il2CppRGCTXDataType)3, 43917 },
	{ (Il2CppRGCTXDataType)3, 43918 },
	{ (Il2CppRGCTXDataType)3, 33256 },
	{ (Il2CppRGCTXDataType)3, 33257 },
	{ (Il2CppRGCTXDataType)2, 4497 },
	{ (Il2CppRGCTXDataType)2, 511 },
	{ (Il2CppRGCTXDataType)3, 43921 },
	{ (Il2CppRGCTXDataType)3, 43909 },
	{ (Il2CppRGCTXDataType)3, 43914 },
	{ (Il2CppRGCTXDataType)3, 33253 },
	{ (Il2CppRGCTXDataType)3, 43924 },
	{ (Il2CppRGCTXDataType)2, 506 },
	{ (Il2CppRGCTXDataType)3, 14981 },
	{ (Il2CppRGCTXDataType)2, 3668 },
	{ (Il2CppRGCTXDataType)2, 6241 },
	{ (Il2CppRGCTXDataType)3, 42973 },
	{ (Il2CppRGCTXDataType)3, 33761 },
	{ (Il2CppRGCTXDataType)3, 20505 },
	{ (Il2CppRGCTXDataType)2, 3789 },
	{ (Il2CppRGCTXDataType)2, 5016 },
	{ (Il2CppRGCTXDataType)3, 20503 },
	{ (Il2CppRGCTXDataType)3, 33760 },
	{ (Il2CppRGCTXDataType)3, 20504 },
	{ (Il2CppRGCTXDataType)3, 33701 },
	{ (Il2CppRGCTXDataType)2, 3976 },
	{ (Il2CppRGCTXDataType)2, 4176 },
	{ (Il2CppRGCTXDataType)3, 19679 },
	{ (Il2CppRGCTXDataType)3, 43552 },
	{ (Il2CppRGCTXDataType)3, 19680 },
	{ (Il2CppRGCTXDataType)3, 43557 },
	{ (Il2CppRGCTXDataType)3, 20506 },
	{ (Il2CppRGCTXDataType)3, 20507 },
	{ (Il2CppRGCTXDataType)3, 43435 },
	{ (Il2CppRGCTXDataType)3, 43441 },
	{ (Il2CppRGCTXDataType)3, 33756 },
	{ (Il2CppRGCTXDataType)3, 33757 },
	{ (Il2CppRGCTXDataType)3, 33755 },
	{ (Il2CppRGCTXDataType)3, 33758 },
	{ (Il2CppRGCTXDataType)3, 12752 },
	{ (Il2CppRGCTXDataType)2, 2934 },
	{ (Il2CppRGCTXDataType)3, 12751 },
	{ (Il2CppRGCTXDataType)3, 33759 },
	{ (Il2CppRGCTXDataType)2, 2457 },
	{ (Il2CppRGCTXDataType)3, 5544 },
	{ (Il2CppRGCTXDataType)3, 33729 },
	{ (Il2CppRGCTXDataType)2, 6246 },
	{ (Il2CppRGCTXDataType)3, 5546 },
	{ (Il2CppRGCTXDataType)3, 5545 },
	{ (Il2CppRGCTXDataType)3, 5549 },
	{ (Il2CppRGCTXDataType)3, 5548 },
	{ (Il2CppRGCTXDataType)3, 5547 },
	{ (Il2CppRGCTXDataType)2, 3645 },
	{ (Il2CppRGCTXDataType)2, 6247 },
	{ (Il2CppRGCTXDataType)3, 42974 },
	{ (Il2CppRGCTXDataType)3, 12732 },
	{ (Il2CppRGCTXDataType)2, 2924 },
	{ (Il2CppRGCTXDataType)3, 33831 },
	{ (Il2CppRGCTXDataType)2, 5012 },
	{ (Il2CppRGCTXDataType)3, 20385 },
	{ (Il2CppRGCTXDataType)2, 4994 },
	{ (Il2CppRGCTXDataType)3, 20359 },
	{ (Il2CppRGCTXDataType)3, 20387 },
	{ (Il2CppRGCTXDataType)3, 33840 },
	{ (Il2CppRGCTXDataType)3, 33839 },
	{ (Il2CppRGCTXDataType)3, 20386 },
	{ (Il2CppRGCTXDataType)3, 33826 },
	{ (Il2CppRGCTXDataType)2, 4510 },
	{ (Il2CppRGCTXDataType)3, 43553 },
	{ (Il2CppRGCTXDataType)3, 20388 },
	{ (Il2CppRGCTXDataType)3, 20389 },
	{ (Il2CppRGCTXDataType)3, 43436 },
	{ (Il2CppRGCTXDataType)3, 33833 },
	{ (Il2CppRGCTXDataType)2, 3911 },
	{ (Il2CppRGCTXDataType)2, 4104 },
	{ (Il2CppRGCTXDataType)3, 33832 },
	{ (Il2CppRGCTXDataType)3, 33835 },
	{ (Il2CppRGCTXDataType)2, 4281 },
	{ (Il2CppRGCTXDataType)3, 29039 },
	{ (Il2CppRGCTXDataType)3, 33834 },
	{ (Il2CppRGCTXDataType)3, 20360 },
	{ (Il2CppRGCTXDataType)3, 33836 },
	{ (Il2CppRGCTXDataType)3, 33838 },
	{ (Il2CppRGCTXDataType)3, 20361 },
	{ (Il2CppRGCTXDataType)3, 10466 },
	{ (Il2CppRGCTXDataType)3, 33837 },
	{ (Il2CppRGCTXDataType)3, 10465 },
	{ (Il2CppRGCTXDataType)2, 2858 },
	{ (Il2CppRGCTXDataType)3, 20362 },
	{ (Il2CppRGCTXDataType)2, 2859 },
	{ (Il2CppRGCTXDataType)3, 10467 },
	{ (Il2CppRGCTXDataType)3, 10492 },
	{ (Il2CppRGCTXDataType)3, 33841 },
	{ (Il2CppRGCTXDataType)3, 33842 },
	{ (Il2CppRGCTXDataType)3, 10491 },
	{ (Il2CppRGCTXDataType)2, 1097 },
	{ (Il2CppRGCTXDataType)2, 3646 },
	{ (Il2CppRGCTXDataType)2, 6251 },
	{ (Il2CppRGCTXDataType)3, 42975 },
	{ (Il2CppRGCTXDataType)2, 5014 },
	{ (Il2CppRGCTXDataType)3, 20390 },
	{ (Il2CppRGCTXDataType)3, 33917 },
	{ (Il2CppRGCTXDataType)3, 20392 },
	{ (Il2CppRGCTXDataType)3, 33916 },
	{ (Il2CppRGCTXDataType)3, 20391 },
	{ (Il2CppRGCTXDataType)3, 33877 },
	{ (Il2CppRGCTXDataType)3, 33911 },
	{ (Il2CppRGCTXDataType)2, 3912 },
	{ (Il2CppRGCTXDataType)2, 4105 },
	{ (Il2CppRGCTXDataType)3, 43554 },
	{ (Il2CppRGCTXDataType)3, 20393 },
	{ (Il2CppRGCTXDataType)3, 20394 },
	{ (Il2CppRGCTXDataType)3, 43437 },
	{ (Il2CppRGCTXDataType)2, 4638 },
	{ (Il2CppRGCTXDataType)3, 33910 },
	{ (Il2CppRGCTXDataType)3, 33913 },
	{ (Il2CppRGCTXDataType)3, 33912 },
	{ (Il2CppRGCTXDataType)3, 33915 },
	{ (Il2CppRGCTXDataType)3, 33914 },
	{ (Il2CppRGCTXDataType)2, 3559 },
	{ (Il2CppRGCTXDataType)3, 15540 },
	{ (Il2CppRGCTXDataType)2, 4995 },
	{ (Il2CppRGCTXDataType)3, 20363 },
	{ (Il2CppRGCTXDataType)3, 20364 },
	{ (Il2CppRGCTXDataType)3, 10469 },
	{ (Il2CppRGCTXDataType)3, 10468 },
	{ (Il2CppRGCTXDataType)2, 2860 },
	{ (Il2CppRGCTXDataType)3, 33909 },
	{ (Il2CppRGCTXDataType)3, 12731 },
	{ (Il2CppRGCTXDataType)2, 2923 },
	{ (Il2CppRGCTXDataType)3, 33725 },
	{ (Il2CppRGCTXDataType)2, 3558 },
	{ (Il2CppRGCTXDataType)3, 15538 },
	{ (Il2CppRGCTXDataType)3, 33882 },
	{ (Il2CppRGCTXDataType)2, 6253 },
	{ (Il2CppRGCTXDataType)3, 15539 },
	{ (Il2CppRGCTXDataType)3, 4180 },
	{ (Il2CppRGCTXDataType)2, 2274 },
	{ (Il2CppRGCTXDataType)3, 33978 },
	{ (Il2CppRGCTXDataType)2, 6171 },
	{ (Il2CppRGCTXDataType)3, 33413 },
	{ (Il2CppRGCTXDataType)3, 33918 },
	{ (Il2CppRGCTXDataType)2, 6254 },
	{ (Il2CppRGCTXDataType)3, 33414 },
	{ (Il2CppRGCTXDataType)3, 34022 },
	{ (Il2CppRGCTXDataType)3, 34023 },
	{ (Il2CppRGCTXDataType)2, 1910 },
	{ (Il2CppRGCTXDataType)3, 42969 },
	{ (Il2CppRGCTXDataType)3, 1416 },
	{ (Il2CppRGCTXDataType)3, 34024 },
	{ (Il2CppRGCTXDataType)2, 6256 },
	{ (Il2CppRGCTXDataType)3, 34021 },
	{ (Il2CppRGCTXDataType)3, 43555 },
	{ (Il2CppRGCTXDataType)3, 43438 },
	{ (Il2CppRGCTXDataType)3, 34025 },
	{ (Il2CppRGCTXDataType)3, 12734 },
	{ (Il2CppRGCTXDataType)2, 2925 },
	{ (Il2CppRGCTXDataType)3, 12733 },
	{ (Il2CppRGCTXDataType)2, 881 },
	{ (Il2CppRGCTXDataType)3, 33985 },
	{ (Il2CppRGCTXDataType)2, 6255 },
	{ (Il2CppRGCTXDataType)3, 33983 },
	{ (Il2CppRGCTXDataType)2, 1908 },
	{ (Il2CppRGCTXDataType)3, 42968 },
	{ (Il2CppRGCTXDataType)3, 1415 },
	{ (Il2CppRGCTXDataType)3, 33987 },
	{ (Il2CppRGCTXDataType)2, 6255 },
	{ (Il2CppRGCTXDataType)3, 33982 },
	{ (Il2CppRGCTXDataType)2, 879 },
	{ (Il2CppRGCTXDataType)3, 33990 },
	{ (Il2CppRGCTXDataType)3, 33991 },
	{ (Il2CppRGCTXDataType)3, 33989 },
	{ (Il2CppRGCTXDataType)3, 33992 },
	{ (Il2CppRGCTXDataType)3, 33988 },
	{ (Il2CppRGCTXDataType)3, 33986 },
	{ (Il2CppRGCTXDataType)3, 33984 },
};
extern const CustomAttributesCacheGenerator g_Mirror_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Mirror_CodeGenModule;
const Il2CppCodeGenModule g_Mirror_CodeGenModule = 
{
	"Mirror.dll",
	1224,
	s_methodPointers,
	6,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	67,
	s_rgctxIndices,
	306,
	s_rgctxValues,
	NULL,
	g_Mirror_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
